<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use Validator;
use Auth ;
use Session ;

use DB ;

class LoginController extends Controller
{
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email'    => 'required|email',
            'password' => 'required|min:6',
        ]);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
        $login = $this->loginByEmailPassword($request->all()) ;
        var_dump($login);
        // dd(Auth::user()) ;
        if ($login)
        {

                return redirect(route('plataforma')) ;
        }
        else
        {
            return redirect(route('login'))
                    ->withInput()
                    ->withErrors('Usuario Incorrecto, Verificar email o password');
            // return Redirect::to('login')->with('msg','Te has registrado correctamente');
        }


    }


    public function loginByEmailPassword(array $data)
    {
        $user = Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'estado' => 1]) ;
        return $user ;
    }


}
