<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Control;

use App\Traits\TreeMenu ;

class ControlesController extends Controller
{
	use TreeMenu;

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function getControles()
    {


        $data = Control::where('estado',1)
                ->get() ;

        if (count($data) > 0)
        {
            $data = $this->buildTreeMenu($data->toArray(), null) ;
        }
        // dd($data) ;

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }
}
