<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Persona ;

class PersonasController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }


    public function getPersonasJuridicas()
    {
    	$data = Persona::where('per_tipo',2)
    					->with('perJuridica')
    					->get() ;



    	return response()->json(
	            [
	                'error' => false,
	                'data' => $data,
	            ]
	        );
    }

    # si queremso mostrar personas que solo pertenescas debemos modificar SQL
    public function getPersonasInfoBasica(Request $request)
    {
         $per_tipo = $request->input('per_tipo') ;

        if ($per_tipo == 0 || empty($per_tipo) )
        {
            $per_tipos = array(1,2) ;
        }
        else  if($per_tipo == 1 )
        {
            $per_tipos = array(1) ;
        }
        else  if($per_tipo == 2 )
        {
            $per_tipos = array(2) ;
        }

        $user = \Auth::user();
        $rol_id_user = $user->rol_id ;
        $persona_id_padre = $user->persona_id_padre ;

        if ($rol_id_user != 1)
        {
            $data = Persona::where('persona.estado',1)
                            ->whereIn('persona.per_tipo',$per_tipos)
                        ->join('per_relacion', function($join) use ($persona_id_padre){
                              $join->on('persona.id','=', 'per_relacion.persona_id')
                              ->where('per_relacion.estado','=',1)
                              ->where('per_relacion.tipo_relacion_id','=',2)
                            ->where('per_relacion.persona_id_padre','=',$persona_id_padre) ;
                            })
                        ->join('per_documento', function($join) {
                              $join->on('persona.id','=', 'per_documento.persona_id')
                              ->whereIn('per_documento.tipo_documento_id',[1,2])
                              ->where('per_documento.estado','=',1)
                              ;
                            })
                        ->leftJoin('per_mail', function($join) {
                              $join->on('persona.id','=', 'per_mail.persona_id')
                              ->where('per_mail.item','=',1);
                            })
                        ->select([
                            'persona.id as persona_id',
                            'persona.per_nombre',
                            'persona.per_apellidos',
                            'persona.per_fecha_nac',
                            'per_documento.numero as documento',
                            'per_documento.tipo_documento_id',
                            'per_mail.id as per_mail_id',
                            'per_mail.mail',
                            \DB::raw('CONCAT(persona.per_apellidos, " ", persona.per_nombre) AS full_name') ,
                         ])
                        ->orderBy('persona.per_apellidos','asc')
                        ->get();
                        // dd($data->toArray());
        }else
        { # usuarario sudo
            $data = Persona::where('persona.estado',1)
                        ->whereIn('persona.per_tipo',$per_tipos)
                        ->join('per_relacion', function($join)  {
                              $join->on('persona.id','=', 'per_relacion.persona_id')
                              ->where('per_relacion.estado','=',1)
                              ->where('per_relacion.tipo_relacion_id','=',2) ;
                            })
                        ->join('per_documento', function($join) {
                              $join->on('persona.id','=', 'per_documento.persona_id')
                              ->whereIn('per_documento.tipo_documento_id',[1,2])
                              ->where('per_documento.estado','=',1)
                              ;
                            })
                        ->leftJoin('per_mail', function($join) {
                              $join->on('persona.id','=', 'per_mail.persona_id')
                              ->where('per_mail.item','=',1);
                            })
                        ->select([
                            'persona.id as persona_id',
                            'persona.per_nombre',
                            'persona.per_apellidos',
                            'persona.per_fecha_nac',
                            'per_documento.numero as documento',
                            'per_documento.tipo_documento_id',
                            'per_mail.id as per_mail_id',
                            'per_mail.mail',
                            \DB::raw('CONCAT(persona.per_apellidos, " ", persona.per_nombre) AS full_name') ,
                         ])
                        ->orderBy('persona.per_apellidos','asc')
                        ->get();
                        // dd($data->toArray());

        }
        return \Response::json([
                            'message' => 'Operación Correcta',
                            'error'=> false ,
                            'data' => $data ,
                            'datassss' => 'full'
                        ]) ;
    }

}
