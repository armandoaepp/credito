<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Traits\GeneretorCodes ;


use App\Models\Cliente ;
use App\Models\AgenteComercial ;
use App\Models\PerRelacion ;

class ClientesController extends Controller
{
    use GeneretorCodes ;

	public function getClientes(Request $request)
	{
        $per_tipo = $request->input('per_tipo') ;

        if ($per_tipo == 0 || empty($per_tipo) )
        {
            $per_tipos = array(1,2) ;
        }
        else  if($per_tipo == 1 )
        {
            $per_tipos = array(1) ;
        }
        else  if($per_tipo == 2 )
        {
            $per_tipos = array(2) ;
        }

        $user = \Auth::user();

        $rol_id_user      = $user->rol_id ;
        $persona_id_padre = $user->persona_id_padre ;


    	if ($rol_id_user > 1)
    	{
    		$data = Cliente::where('cliente.estado',1)
    					->where('cliente.persona_id_padre',$persona_id_padre )
                        ->join('agente_comercial', function($join) {
                              $join->on('agente_comercial.cliente_id','=', 'cliente.id')
                              ->where('agente_comercial.estado','=',1);
                            })
    					->join('persona', function($join) use ($per_tipos) {
                              $join->on('persona.id','=', 'cliente.persona_id')
                              ->where('persona.estado','=',1)
                              ->whereIn('persona.per_tipo',$per_tipos)
                              ;
                            })
                        ->join('per_documento', function($join) {
                              $join->on('persona.id','=', 'per_documento.persona_id')
                              ->whereIn('per_documento.tipo_documento_id',[1,2])
                              ->where('per_documento.estado','=',1)
                              ;
                            })
                        ->leftJoin('per_telefono', function($join) {
                              $join->on('persona.id','=', 'per_telefono.persona_id')
                              ->where('per_telefono.item','=',1);
                            })
                        ->leftJoin('per_mail', function($join) {
                              $join->on('persona.id','=', 'per_mail.persona_id')
                              ->where('per_mail.item','=',1);
                            })
                        ->select([
                            'cliente.id',
                            'cliente.codigo',
                            'persona.id as persona_id',
                            'persona.per_fecha_nac',
                            'per_documento.numero as documento',
                            'per_documento.tipo_documento_id',
                            'persona.per_nombre',
                            'persona.per_apellidos',
                            'per_telefono.id as per_telefono_id',
                            'per_telefono.telefono',
                            'per_mail.id as per_mail_id',
                            'per_mail.mail',
                            'agente_comercial.id as agente_comercial_id',
                            'agente_comercial.empleado_id as empleado_id',
                         ])
                        ->orderBy('cliente.id','desc')
    					->get();
    	}else
    	{
    		# cuando ingrese como sudo
            $data = Cliente::where('cliente.estado',1)
                        ->join('agente_comercial', function($join) {
                              $join->on('agente_comercial.cliente_id','=', 'cliente.id')
                              ->where('agente_comercial.estado','=',1);
                            })
                        ->join('persona', function($join) use ($per_tipos) {
                              $join->on('persona.id','=', 'cliente.persona_id')
                              ->where('persona.estado','=',1)
                              ->whereIn('persona.per_tipo',$per_tipos)
                              ;
                            })
                        ->join('per_documento', function($join) {
                              $join->on('persona.id','=', 'per_documento.persona_id')
                              ->whereIn('per_documento.tipo_documento_id',[1,2])
                              ->where('per_documento.estado','=',1)
                              ;
                            })
                        ->leftJoin('per_telefono', function($join) {
                              $join->on('persona.id','=', 'per_telefono.persona_id')
                              ->where('per_telefono.item','=',1);
                            })
                        ->leftJoin('per_mail', function($join) {
                              $join->on('persona.id','=', 'per_mail.persona_id')
                              ->where('per_mail.item','=',1);
                            })
                        ->select([
                            'cliente.id',
                            'cliente.codigo',
                            'persona.id as persona_id',
                            'persona.per_fecha_nac',
                            'per_documento.numero as documento',
                            'per_documento.tipo_documento_id',
                            'persona.per_nombre',
                            'persona.per_apellidos',
                            'per_telefono.id as per_telefono_id',
                            'per_telefono.telefono',
                            'per_mail.id as per_mail_id',
                            'per_mail.mail',
                            'agente_comercial.id as agente_comercial_id',
                            'agente_comercial.empleado_id as empleado_id',
                         ])
                        ->orderBy('cliente.id','desc')
                        ->get();
    	}

    	return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}

    public function save(Request $request)
    {
        $persona_id  = $request->input('persona_id') ;
        $empleado_id = $request->input('empleado_id') ;

        $user = \Auth::user() ;
        $persona_id_padre = $user->persona_id_padre ;


        $error = false;
        $data = '' ;

            $cliente =  Cliente::where('persona_id',$persona_id)
                                ->where('persona_id_padre',$persona_id_padre)
                                ->first();
            if (!$cliente)
            {
                # save data
                    $cliente =  new Cliente() ;
                    $cliente->persona_id       =  $persona_id ;
                    $cliente->persona_id_padre =  $persona_id_padre ;
                    $cliente->estado           =  1 ;
                    $cliente->save() ;

                    $cliente_id = $cliente->id ;

                    $codigo = $this->generateCodeCliente($cliente_id);

                    $cliente_upd =  Cliente::find($cliente_id);
                    $cliente_upd->codigo = $codigo ;
                    $cliente_upd->save() ;

                    $agente_comercial =  new AgenteComercial() ;
                    $agente_comercial->empleado_id = $empleado_id ;
                    $agente_comercial->cliente_id  = $cliente_id ;
                    $agente_comercial->estado      = 1 ;
                    $agente_comercial->save() ;

                    # ==== ASIGNAR RELACION PERSONA(EMPRESA) -> CLIENTE ======================
                        $per_relacion = new PerRelacion() ;
                        $per_relacion->persona_id_padre = $persona_id_padre ;
                        $per_relacion->tipo_relacion_id = 3 ;
                        $per_relacion->persona_id       = $persona_id;
                        $per_relacion->referencia       = 'Cliente - Empresa' ;
                        $per_relacion->estado           = 1 ;
                        $per_relacion->save();
            }
            else
            {
                $cliente->estado =  1 ;
                $cliente->save() ;
            }

            $data = 'Registro Correcto' ;

            return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }

    public function updateNewAgenteComercial(Request $request)
    {
        $empleado_id_new     = $request->input('empleado_id_new');
        $cliente_id          = $request->input('cliente_id');
        $agente_comercial_id = $request->input('agente_comercial_id');


        $agente_comercial = AgenteComercial::find($agente_comercial_id);
        $agente_comercial->estado = 0 ;
        $agente_comercial->save() ;

        $agente_comercial_new = new AgenteComercial() ;
        $agente_comercial_new->cliente_id = $cliente_id  ;
        $agente_comercial_new->empleado_id = $empleado_id_new  ;
        $agente_comercial_new->estado = 1 ;
        $agente_comercial_new->save();

        $data = "OK" ;
        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }

    public function updateEstado(Request $request)
    {
        $cliente_id = $request->input('cliente_id');
        $estado      = $request->input('estado');

        $cliente = Cliente::find($cliente_id);
        $cliente->estado = $estado ;
        $cliente->save() ;

        $data = "OK" ;
        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }

    public function getClientesInfoBasica(Request $request)
    {
        $per_tipo = $request->input('per_tipo') ;

        if ($per_tipo == 0 || empty($per_tipo) )
        {
            $per_tipos = array(1,2) ;
        }
        else  if($per_tipo == 1 )
        {
            $per_tipos = array(1) ;
        }
        else  if($per_tipo == 2 )
        {
            $per_tipos = array(2) ;
        }

        $user = \Auth::user();

        $rol_id_user      = $user->rol_id ;
        $persona_id_padre = $user->persona_id_padre ;


        if ($rol_id_user > 1)
        {
            $data = Cliente::where('cliente.estado',1)
                        ->where('cliente.persona_id_padre',$persona_id_padre )
                        ->join('agente_comercial', function($join) {
                              $join->on('agente_comercial.cliente_id','=', 'cliente.id')
                              ->where('agente_comercial.estado','=',1);
                            })
                        ->join('persona', function($join) use ($per_tipos) {
                              $join->on('persona.id','=', 'cliente.persona_id')
                              ->where('persona.estado','=',1)
                              ->whereIn('persona.per_tipo',$per_tipos)
                              ;
                            })
                        ->join('per_documento', function($join) {
                              $join->on('persona.id','=', 'per_documento.persona_id')
                              ->whereIn('per_documento.tipo_documento_id',[1,2])
                              ->where('per_documento.estado','=',1)
                              ;
                            })
                        ->select([
                            'cliente.id',
                            'cliente.codigo',
                            'persona.id as persona_id',
                            'per_documento.numero as documento',
                            'per_documento.tipo_documento_id',
                            'persona.per_nombre',
                            'persona.per_apellidos',
                            'agente_comercial.id as agente_comercial_id',
                            'agente_comercial.empleado_id as empleado_id',
                            \DB::raw('CONCAT(persona.per_apellidos, " ", persona.per_nombre) AS full_name') ,

                         ])
                        ->orderBy('cliente.id','desc')
                        ->get();
        }else
        {
            # cuando ingrese como sudo
            $data = Cliente::where('cliente.estado',1)
                        ->join('agente_comercial', function($join) {
                              $join->on('agente_comercial.cliente_id','=', 'cliente.id')
                              ->where('agente_comercial.estado','=',1);
                            })
                        ->join('persona', function($join) use ($per_tipos) {
                              $join->on('persona.id','=', 'cliente.persona_id')
                              ->where('persona.estado','=',1)
                              ->whereIn('persona.per_tipo',$per_tipos)
                              ;
                            })
                        ->join('per_documento', function($join) {
                              $join->on('persona.id','=', 'per_documento.persona_id')
                              ->whereIn('per_documento.tipo_documento_id',[1,2])
                              ->where('per_documento.estado','=',1)
                              ;
                            })
                        ->select([
                             'cliente.id',
                            'cliente.codigo',
                            'persona.id as persona_id',
                            'per_documento.numero as documento',
                            'per_documento.tipo_documento_id',
                            'persona.per_nombre',
                            'persona.per_apellidos',
                            'agente_comercial.id as agente_comercial_id',
                            'agente_comercial.empleado_id as empleado_id',
                            \DB::raw('CONCAT(persona.per_apellidos, " ", persona.per_nombre) AS full_name') ,

                         ])
                        ->orderBy('persona.per_apellidos','asc')
                        ->get();
        }

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }

}
