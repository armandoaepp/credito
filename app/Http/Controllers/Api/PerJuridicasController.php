<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth ;
use App\Models\Persona ;
use App\Models\PerJuridica ;

use App\Models\PerDocumento;
use App\Models\PerMail;
use App\Models\PerTelefono;
use App\Models\PerRelacion;
use App\Models\PerWeb;

class PerJuridicasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function getPerJuridicas()
    {
    	$user = Auth::user();

    	$rol_id_user = $user->rol_id ;
    	$persona_id_padre = $user->persona_id_padre ;

    	if ($rol_id_user != 1)
    	{
    		$data = Persona::where('per_tipo',2)
    					->where('persona.estado',1)
    					->join('per_juridica', function($join) {
						      $join->on('persona.id','=', 'per_juridica.persona_id')
						      ->where('per_juridica.estado','=',1);
						    })
    					->join('per_relacion', function($join) use ($persona_id_padre) {
						      $join->on('persona.id','=', 'per_relacion.persona_id')
						      ->where('per_relacion.estado','=',1)
						      ->where('per_relacion.tipo_relacion_id','=',2)
						      ->where('per_relacion.persona_id_padre','=',$persona_id_padre) ;
						    })
    					->leftJoin('per_telefono', function($join) {
						      $join->on('persona.id','=', 'per_telefono.persona_id')
						      ->where('per_telefono.item','=',1);
						    })
                        ->leftJoin('per_mail', function($join) {
                              $join->on('persona.id','=', 'per_mail.persona_id')
                              ->where('per_mail.item','=',1);
                            })
    					->select([
    						'persona.id as persona_id',
    						'persona.per_fecha_nac',
    						'per_juridica.id as per_juridica_id',
    						'per_juridica.ruc',
    						'per_juridica.razon_social',
    						'per_juridica.nombre_comercial',
    						'per_telefono.id as per_telefono_id',
    						'per_telefono.telefono',
                            'per_mail.id as per_mail_id',
                            'per_mail.mail'
    					 ])
    					->orderBy('persona_id','desc')
    					->get() ;
    					// dd($data);
    	}else
    	{ # cuando ingrese como sudo

    		$data = Persona::where('per_tipo',2)
    					->where('persona.estado',1)
    					->join('per_juridica', function($join) {
						      $join->on('persona.id','=', 'per_juridica.persona_id')
						      ->where('per_juridica.estado','=',1);
						    })
    					->join('per_relacion', function($join) {
						      $join->on('persona.id','=', 'per_relacion.persona_id')
						      ->where('per_relacion.estado','=',1)
						      ->where('per_relacion.tipo_relacion_id','=',2) ;
						    })
    					->leftJoin('per_telefono', function($join) {
						      $join->on('persona.id','=', 'per_telefono.persona_id')
						      ->where('per_telefono.item','=',1);
						    })
                        ->leftJoin('per_mail', function($join) {
                              $join->on('persona.id','=', 'per_mail.persona_id')
                              ->where('per_mail.item','=',1);
                            })
    					->select([
    						'persona.id as persona_id',
    						'persona.per_fecha_nac',
    						'per_juridica.id as per_juridica_id',
    						'per_juridica.ruc',
    						'per_juridica.razon_social',
    						'per_juridica.nombre_comercial',
    						'per_telefono.id as per_telefono_id',
    						'per_telefono.telefono',
                            'per_mail.id as per_mail_id',
                            'per_mail.mail'
    					 ])
    					->orderBy('per_juridica_id','desc')
    					->get() ;
    					// dd($data->toArray());
    	}

    	return response()->json(
	            [
	                'error' => false,
	                'data' => $data,
	            ]
	        );
    }

    public function save(Request $request)
    {
        $user = Auth::user() ;
        $persona_id_padre = $user->persona_id_padre ;

        /*$ruc              = '9234959234';
        $razon_social     = 'demos';
        $nombre_comercial = ' prueba ';
        $telefonos        = ["telefono"=>"996393414"];
        $distrito_id      = 3;
        $mails            = ["mail"=>"armando@gmail.com"];*/

        $ruc              = $request->input('ruc');
        $razon_social     = $request->input('razon_social');
        $nombre_comercial = $request->input('nombre_comercial');
        $telefonos        = $request->input('telefonos');
        $distrito_id      = $request->input('distrito_id');
        $mails            = $request->input('mails');
        $sitio_web        = !empty($request->input('sitio_web'))?$request->input('sitio_web') : '' ;

        $error = false;
        $data  = array();

        try {

            \DB::beginTransaction();

            # save data
                $persona =  new Persona() ;
                $persona->per_nombre    =  $razon_social ;
                $persona->per_apellidos =  $nombre_comercial ;
                // $persona->per_fecha_nac =  $fecha_nac ;
                $persona->per_tipo      =  2 ;
                $persona->save() ;

                $persona_id =  $persona->id ;

                $per_juridica =  new PerJuridica() ;
                $per_juridica->persona_id       = $persona_id ;
                $per_juridica->rubro_id         = 0 ;
                $per_juridica->ruc              = $ruc ;
                $per_juridica->razon_social     = $razon_social ;
                $per_juridica->nombre_comercial = $nombre_comercial ;
                $per_juridica->estado           = 1;
                $per_juridica->save() ;

                $per_documento =  new PerDocumento() ;
                $per_documento->persona_id        = $persona_id;
                $per_documento->tipo_documento_id = 2 ;
                $per_documento->numero            = $ruc;
                $per_documento->caducidad         = null;
                $per_documento->imagen            = '';
                $per_documento->estado            = 1;
                $per_documento->save() ;


                # registrar telefonos
                if(count($telefonos) > 0 )
                {
                    $fills =  array() ;
                    for ($i=0; $i < count($telefonos); $i++)
                    {
                        $fill = array(
                                        'persona_id' => $persona_id ,
                                        'tipo_telefono_id' => 1 ,
                                        'telefono' => $telefonos[$i]['telefono'],
                                        'item' => ($i+1)
                                  ) ;
                        array_push($fills,$fill) ;

                    }
                    PerTelefono::insert($fills) ;
                }

                # registrar mails
                if(count($mails) > 0 )
                {
                    $fills =  array() ;
                    for ($i=0; $i < count($mails); $i++)
                    {
                        $fill = array(
                                        'persona_id' => $persona_id ,
                                        'mail' => $mails[$i]['mail'],
                                        'item' => ($i+1)
                                  ) ;
                        array_push($fills,$fill) ;

                    }
                    PerMail::insert($fills) ;
                }


                # ==== ASIGNAR RELACION PERSONA -> EMPRESA ======================
                    $per_relacion = new PerRelacion() ;
                    $per_relacion->persona_id_padre = $persona_id_padre ;
                    $per_relacion->tipo_relacion_id = 2 ;
                    $per_relacion->persona_id       = $persona_id;
                    $per_relacion->referencia       = 'persona(2) - empresa ' ;
                    // $per_relacion->created_at       = $created_at;
                    $per_relacion->estado           = 1 ;
                    $per_relacion->save();

                $per_web = new PerWeb();
                $per_web->persona_id = $persona_id ;
                $per_web->tipo_web_id   = 1 ;
                $per_web->url        = $sitio_web ;
                $per_web->save();



                $error =  false ;
                $data =  array('persona_id'=> $persona_id) ;

            \DB::commit();

        } catch (Exception $e)
        {
            \DB::rollback();
            $error =  true ;
            $data =  'Error: '.$e->getMessage() ;
        }

        return response()->json(
                [
                    'message' => 'Operación Correcta',
                    'error' => $error,
                    'data'  => $data,
                ]
            );
    }

    public function getPerJuridicaByRuc(Request $request)
    {
        $ruc    = $request->input('ruc') ;

        $data = PerJuridica::where('ruc',$ruc)
                    ->get() ;

        return response()->json(
                [
                    'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function getPerJuridicaInfoAll(Request $request)
    {
        $persona_id = $request->input('persona_id') ;
        // $persona_id = 54;

        $data = Persona::where('id',$persona_id)
                    ->with('perJuridica')
                    ->with('perTelefono')
                    ->with('perMail')
                    ->with('perWeb')
                    ->get() ;

        return response()->json(
                [
                    'error' => false,
                    'data'  => $data,
                ]
            );

    }

    public function updateRuc(Request $request)
    {
        $persona_id        = $request->input('persona_id');
        $ruc               = $request->input('ruc');
        $tipo_documento_id = 2;

        $rules = ['ruc'=> 'required|min:10|unique:per_juridica|integer'] ;
        $validator = \Validator::make(['ruc'=> $ruc],  $rules);

        if ($validator->fails())
         {
            $errors = (array)$validator->errors()->toArray();

            return \Response::json([
                'message' => 'Operación Fallida',
                'error'   => true,
                'data'    => $errors ,
            ]);
        }
        else
        {

            $per_juridica = PerJuridica::where('persona_id',$persona_id)->first();
            $per_juridica->ruc = $ruc ;
            $per_juridica->save() ;

            //  per_documento
            $per_documento = PerDocumento::where('persona_id',$persona_id)
                                           ->where('tipo_documento_id',$tipo_documento_id)
                                           ->first();
            $per_documento->numero = $ruc ;
            $per_documento->save() ;

            $data = $per_juridica ;

            return \Response::json([
                'message' => 'Registro Correcto',
                'error'   => false,
                'data'    => $data ,
            ]);
        }
    }

    public function updatePerJuridicaInfo(Request $request)
    {
        $razon_social     = $request->input('razon_social');
        $nombre_comercial = $request->input('nombre_comercial');
        $persona_id       = $request->input('persona_id');
        $per_fecha_nac    = $request->input('fecha_nac');


        try {

            \DB::beginTransaction();

                $persona = Persona::find($persona_id);
                $persona->per_nombre    = $razon_social ;
                $persona->per_apellidos = $nombre_comercial ;
                $persona->per_fecha_nac = $per_fecha_nac ;
                $persona->save() ;
                // dd($persona);

                $per_juridica = PerJuridica::where('persona_id',$persona_id)->first();
                $per_juridica->razon_social   = $razon_social ;
                $per_juridica->nombre_comercial = $nombre_comercial ;
                $per_juridica->save() ;
                // dd($per_juridica);

            \DB::commit();

            $data = Persona::with('perJuridica')->where('id',$persona_id)->first();

            return \Response::json([
                'message' => 'Registro Correcto',
                'error'   => false,
                'data'    => $data ,
            ]);

        } catch (Exception $e)
        {
            \DB::rollback();

            return \Response::json([
                    'message' => 'Operación Faliida',
                    'error'   => true,
                    'data'    => 'Error: '.$e->getMessage()  ,
                ]);
        }

    }

    public function updateEstado(Request $request)
    {

        $persona_id = $request->input('persona_id') ;
        $estado     = $request->input('estado') ;

        $per_juridica =  PerJuridica::where('persona_id',$persona_id)->first() ;

        $per_juridica->estado = $estado ;
        $per_juridica->save() ;

        return response()->json(
                [
                    'error' => false,
                    'data'  => $per_juridica,
                ]
            );
    }
    # si queremso mostrar personas que solo pertenescas debemos modificar SQL
    public function getPerNaturalInfoBasica(Request $request)
    {
        $user = Auth::user();
        $rol_id_user = $user->rol_id ;
        $persona_id_padre = $user->persona_id_padre ;

        if ($rol_id_user != 1)
        {
            $data = Persona::where('per_tipo',1)
                        ->where('persona.estado',1)
                        ->join('per_relacion', function($join) use ($persona_id_padre)   {
                              $join->on('persona.id','=', 'per_relacion.persona_id')
                              ->where('per_relacion.estado','=',1)
                              ->where('per_relacion.tipo_relacion_id','=',2)
                            ->where('per_relacion.persona_id_padre','=',$persona_id_padre) ;
                            })
                        ->join('per_natural', function($join) {
                              $join->on('persona.id','=', 'per_natural.persona_id')
                              ->where('per_natural.estado','=',1);
                            })
                        ->leftJoin('per_mail', function($join) {
                              $join->on('persona.id','=', 'per_mail.persona_id')
                              ->where('per_mail.item','=',1);
                            })
                        ->select([
                            'persona.id as persona_id',
                            'persona.per_nombre',
                            'persona.per_apellidos',
                            'persona.per_fecha_nac',
                            'per_natural.id as per_natural_id',
                            'per_natural.dni',
                            'per_natural.sexo',
                            'per_natural.estado_civil',
                            'per_mail.id as per_mail_id',
                            'per_mail.mail',
                            \DB::raw('CONCAT(persona.per_apellidos, " ", persona.per_nombre) AS full_name') ,
                         ])
                        ->orderBy('persona.per_apellidos','asc')
                        ->get();
                        // dd($data->toArray());
        }else
        { # usuarario sudo
            $data = Persona::where('per_tipo',1)
                        ->where('persona.estado',1)
                        ->join('per_relacion', function($join)  {
                              $join->on('persona.id','=', 'per_relacion.persona_id')
                              ->where('per_relacion.estado','=',1)
                              ->where('per_relacion.tipo_relacion_id','=',2) ;
                            })
                        ->join('per_natural', function($join) {
                              $join->on('persona.id','=', 'per_natural.persona_id')
                              ->where('per_natural.estado','=',1);
                            })
                        ->leftJoin('per_mail', function($join) {
                              $join->on('persona.id','=', 'per_mail.persona_id')
                              ->where('per_mail.item','=',1);
                            })
                        ->select([
                            'persona.id as persona_id',
                            'persona.per_nombre',
                            'persona.per_apellidos',
                            'persona.per_fecha_nac',
                            'per_natural.id as per_natural_id',
                            'per_natural.dni',
                            'per_natural.sexo',
                            'per_natural.estado_civil',
                            'per_mail.id as per_mail_id',
                            'per_mail.mail',
                            \DB::raw('CONCAT(persona.per_apellidos, " ", persona.per_nombre) AS full_name') ,
                         ])
                        ->orderBy('persona.per_apellidos','asc')
                        ->get();
                        // dd($data->toArray());

        }
        return \Response::json([
                            'message' => 'Operación Correcta',
                            'error'=> false ,
                            'data' => $data ,
                        ]) ;
    }


}
