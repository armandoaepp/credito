<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB ;

use App\Models\Persona;
use App\Models\PerNatural;
use App\Models\PerDocumento;
use App\Models\PerMail;
use App\Models\PerTelefono;
use App\Models\PerRelacion;

use Auth;


use App\Http\Controllers\Api\AvalesController;


class PerNaturalesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getPersonasByTipoRelacion(Request $request)
    {
    	$tipo_relacion_id = $request->input('tipo_relacion_id');
    	// $tipo_relacion_id = 2;

		$data = array() ;
    	// todas las personas
		if ($tipo_relacion_id == 2)
		{
			$data = $this->getPerNaturales();
		}
		// cliente
		else if ($tipo_relacion_id == 3)
		{
		}
		// empleados
		else if ($tipo_relacion_id == 4)
		{
		}
		// avales
		else if ($tipo_relacion_id == 5)
		{
			$avales_ctrl = new AvalesController();
			$data = $avales_ctrl->getAvalesTipo();
		}

		return response()->json(
	            [
	             	'message' => 'Operación Correcta',
	                'error' => false,
	                'data' => $data,
	            ]
	        );


    }

    public function getPersonasNaturales()
    {
    	$user = Auth::user();

    	$rol_id_user = $user->rol_id ;
    	$persona_id_padre = $user->persona_id_padre ;

    	if ($rol_id_user != 1)
    	{
    		$data = Persona::where('per_tipo',1)
    					->where('persona.estado',1)
    					->join('per_natural', function($join) {
						      $join->on('persona.id','=', 'per_natural.persona_id')
						      ->where('per_natural.estado','=',1);
						    })
    					->join('per_relacion', function($join) use ($persona_id_padre) {
						      $join->on('persona.id','=', 'per_relacion.persona_id')
						      ->where('per_relacion.estado','=',1)
						      ->where('per_relacion.tipo_relacion_id','=',2)
						      ->where('per_relacion.persona_id_padre','=',$persona_id_padre) ;
						    })
    					->leftJoin('per_telefono', function($join) {
						      $join->on('persona.id','=', 'per_telefono.persona_id')
						      ->where('per_telefono.item','=',1);
						    })
    					->leftJoin('per_mail', function($join) {
						      $join->on('persona.id','=', 'per_mail.persona_id')
						      ->where('per_mail.item','=',1);
						    })
    					->select([
    						'persona.id as persona_id',
    						'persona.per_fecha_nac',
    						'per_natural.id as per_natural_id',
    						'per_natural.dni',
    						'persona.per_nombre',
    						'persona.per_apellidos',
    						'per_natural.sexo',
    						'per_natural.estado_civil',
    						'per_telefono.id as per_telefono_id',
    						'per_telefono.telefono',
    						'per_mail.id as per_mail_id',
    						'per_mail.mail'
    					 ])
    					->orderBy('persona_id','desc')
    					->get() ;
    					// dd($data);
    	}else
    	{ # cuando ingrese como sudo

    		$data = Persona::where('per_tipo',1)
    					->where('persona.estado',1)
    					->join('per_natural', function($join) {
						      $join->on('persona.id','=', 'per_natural.persona_id')
						      ->where('per_natural.estado','=',1);
						    })
    					->join('per_relacion', function($join)   use ($persona_id_padre) {
						      $join->on('persona.id','=', 'per_relacion.persona_id')
						      ->where('per_relacion.estado','=',1)
						      ->where('per_relacion.tipo_relacion_id','=',2) ;
						    })
    					->leftJoin('per_telefono', function($join) {
						      $join->on('persona.id','=', 'per_telefono.persona_id')
						      ->where('per_telefono.item','=',1);
						    })
    					->leftJoin('per_mail', function($join) {
						      $join->on('persona.id','=', 'per_mail.persona_id')
						      ->where('per_mail.item','=',1);
						    })
    					->select([
    						'persona.id as persona_id',
    						'persona.per_fecha_nac',
    						'per_natural.id as per_natural_id',
    						'per_natural.dni',
    						'persona.per_nombre',
    						'persona.per_apellidos',
    						'per_natural.sexo',
    						'per_natural.estado_civil',
    						'per_telefono.id as per_telefono_id',
    						'per_telefono.telefono',
    						'per_mail.id as per_mail_id',
    						'per_mail.mail'
    					 ])
    					->orderBy('persona_id','desc')
    					->get() ;
    					// dd($data->toArray());
    	}

    	return response()->json(
	            [
	             	'message' => 'Operación Correcta',
	                'error' => false,
	                'data' => $data,
	            ]
	        );
    }

    public function getPerNaturales()
    {
    	$user = Auth::user();

    	$rol_id_user = $user->rol_id ;
    	$persona_id_padre = $user->persona_id_padre ;

    	if ($rol_id_user != 1)
    	{
    		$data = Persona::where('per_tipo',1)
    					->where('persona.estado',1)
    					->join('per_natural', function($join) {
						      $join->on('persona.id','=', 'per_natural.persona_id')
						      ->where('per_natural.estado','=',1);
						    })
    					->join('per_relacion', function($join) use ($persona_id_padre) {
						      $join->on('persona.id','=', 'per_relacion.persona_id')
						      ->where('per_relacion.estado','=',1)
						      ->where('per_relacion.tipo_relacion_id','=',2)
						      ->where('per_relacion.persona_id_padre','=',$persona_id_padre) ;
						    })
    					->leftJoin('per_telefono', function($join) {
						      $join->on('persona.id','=', 'per_telefono.persona_id')
						      ->where('per_telefono.item','=',1);
						    })
    					->leftJoin('per_mail', function($join) {
						      $join->on('persona.id','=', 'per_mail.persona_id')
						      ->where('per_mail.item','=',1);
						    })
    					->select([
    						'persona.id as persona_id',
    						'persona.per_fecha_nac',
    						'per_natural.id as per_natural_id',
    						'per_natural.dni',
    						'persona.per_nombre',
    						'persona.per_apellidos',
    						'per_natural.sexo',
    						'per_natural.estado_civil',
    						'per_telefono.id as per_telefono_id',
    						'per_telefono.telefono',
    						'per_mail.id as per_mail_id',
    						'per_mail.mail'
    					 ])
    					->orderBy('persona_id','desc')
    					->get() ;
    					// dd($data);
    	}else
    	{ # cuando ingrese como sudo

    		$data = Persona::where('per_tipo',1)
    					->where('persona.estado',1)
    					->join('per_natural', function($join) {
						      $join->on('persona.id','=', 'per_natural.persona_id')
						      ->where('per_natural.estado','=',1);
						    })
    					->join('per_relacion', function($join)   use ($persona_id_padre) {
						      $join->on('persona.id','=', 'per_relacion.persona_id')
						      ->where('per_relacion.estado','=',1)
						      ->where('per_relacion.tipo_relacion_id','=',2) ;
						    })
    					->leftJoin('per_telefono', function($join) {
						      $join->on('persona.id','=', 'per_telefono.persona_id')
						      ->where('per_telefono.item','=',1);
						    })
    					->leftJoin('per_mail', function($join) {
						      $join->on('persona.id','=', 'per_mail.persona_id')
						      ->where('per_mail.item','=',1);
						    })
    					->select([
    						'persona.id as persona_id',
    						'persona.per_fecha_nac',
    						'per_natural.id as per_natural_id',
    						'per_natural.dni',
    						'persona.per_nombre',
    						'persona.per_apellidos',
    						'per_natural.sexo',
    						'per_natural.estado_civil',
    						'per_telefono.id as per_telefono_id',
    						'per_telefono.telefono',
    						'per_mail.id as per_mail_id',
    						'per_mail.mail'
    					 ])
    					->orderBy('persona_id','desc')
    					->get() ;
    					// dd($data->toArray());
    	}

    	return $data ;
    }

    public function save(Request $request)
    {
    	$user = Auth::user() ;
		$persona_id_padre = $user->persona_id_padre ;
		$created_at       = date('Y-m-d H:m:s') ;

		$dni       = $request->input('dni') ;
		$apellidos = $request->input('apellidos') ;
		$nombres   = $request->input('nombres') ;
		$telefonos = $request->input('telefonos') ;
		$mails     = $request->input('mails') ;
		$sexo      = $request->input('sexo') ;
		$fecha_nac = $request->input('fecha_nac') ;

		$tipo_relacion_id = $request->input('tipo_relacion_id') ;
		// $isRegistrer      = $request->input('isRegistrer') ;
		$persona_id       = $request->input('persona_id') ;

		/*$dni       = '46545564';
		$apellidos = '12334345 sdafsdf ';
		$nombres   = 'ddddddddddddddd';
		$telefonos = '998979889';
		$mails     = 'sss';
		$sexo      = '1';
		$fecha_nac = '2015-09-01';
		$tipo_relacion_id = 5;
		$persona_id = 0 ;*/

		$error = false;
		$data  = array();

		if (empty($persona_id))
		{
			try {

				DB::beginTransaction();


				# save data
					$persona =  new Persona() ;
					$persona->per_nombre    =  $nombres ;
					$persona->per_apellidos =  $apellidos ;
					$persona->per_fecha_nac =  $fecha_nac ;
					$persona->per_tipo      =  1 ;
					$persona->save() ;

					$persona_id =  $persona->id ;

					$per_natural =  new PerNatural() ;
					$per_natural->persona_id   = $persona_id;
					$per_natural->dni          = $dni ;
					$per_natural->apellidos    = $apellidos ;
					$per_natural->nombres      = $nombres;
					$per_natural->sexo         = $sexo ;
					$per_natural->estado_civil = 0;
					$per_natural->estado       = 1;
					$per_natural->save() ;

					$per_documento =  new PerDocumento() ;
					$per_documento->persona_id        = $persona_id;
					$per_documento->tipo_documento_id = 1 ;
					$per_documento->numero            = $dni;
					$per_documento->caducidad         = null;
					$per_documento->imagen            = '';
					$per_documento->estado            = 1;
					$per_documento->save() ;


					# registrar telefonos
					if(count($telefonos) > 0 )
					{
						$fills =  array() ;
						for ($i=0; $i < count($telefonos); $i++)
						{
							$fill = array(
											'persona_id' => $persona_id ,
											'tipo_telefono_id' => 1 ,
											'telefono' => $telefonos[$i]['telefono'],
											'item' => ($i+1)
	                                  ) ;
							array_push($fills,$fill) ;

						}
						PerTelefono::insert($fills) ;
					}

					# registrar mails
					if(count($mails) > 0 )
					{
						$fills =  array() ;
						for ($i=0; $i < count($mails); $i++)
						{
							$fill = array(
											'persona_id' => $persona_id ,
											'mail' => $mails[$i]['mail'],
											'item' => ($i+1)
	                                  ) ;
							array_push($fills,$fill) ;

						}
						PerMail::insert($fills) ;
					}

					# ==== ASIGNAR RELACION PERSONA -> EMPRESA ======================

						$per_relacion = new PerRelacion() ;
						$per_relacion->persona_id_padre = $persona_id_padre ;
						$per_relacion->tipo_relacion_id = 2 ;
						$per_relacion->persona_id       = $persona_id;
						$per_relacion->referencia       = 'persona - empresa' ;
						$per_relacion->estado           = 1 ;
						$per_relacion->save();

						# el tipo de relacion segun persona -> cliente, empleado , aval
						$this->setPersonaTipoRelacion($persona_id, $tipo_relacion_id);

					$error =  false ;
	            	$data =  array('persona_id'=> $persona_id) ;

				DB::commit();

	        } catch (Exception $e)
	        {
	            DB::rollback();
	            $error =  true ;
	            $data =  'Error: '.$e->getMessage() ;
	        }
		}else
		{
	    	$this->setPersonaTipoRelacion($persona_id, $tipo_relacion_id);

	    	$error =  false ;
	        $data =  array('persona_id'=> $persona_id) ;
		}

        return response()->json(
                [
                    'error' => $error,
                    'data'  => $data,
                ]
            );
    }

    public function setPersonaTipoRelacion($persona_id, $tipo_relacion_id)
    {
    	if ($tipo_relacion_id == 3)
		{
			$referencia       = 'cliente - empresa' ;
		}
		else if ($tipo_relacion_id == 4)
		{
			$referencia       = 'empleado - empresa' ;
		}
		else if ($tipo_relacion_id == 5)
		{
			$referencia       = 'aval - empresa' ;

			$inputs_data = array(
						'persona_id'       => $persona_id,
						'tipo_relacion_id' => $tipo_relacion_id,
					) ;
			$avales_ctrl = new AvalesController();
			$data = $avales_ctrl->setAval($inputs_data);

		}
    }

    public function getPerNaturalByDni(Request $request)
    {
        $dni    = $request->input('dni') ;

        $per_natural = PerNatural::where('dni',$dni)
                    ->first() ;

         if ($per_natural)
         {
         	$persona_id = $per_natural->persona_id ;
         	$per_natural = Persona::where('id',$persona_id)
				                    ->with('perNatural')
				                    ->with('perTelefono')
				                    ->with('perMail')
				                    ->get() ;
         }
        // dd($per_natural->toArray());
         $data = $per_natural ;
        return response()->json(
                [
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function getPerNaturalInfoAll(Request $request)
    {
		$persona_id = $request->input('persona_id') ;
		// $persona_id = 54;

        $data = Persona::where('id',$persona_id)
                    ->with('perNatural')
                    ->with('perTelefono')
                    ->with('perMail')
                    ->get() ;

        return response()->json(
                [
                    'error' => false,
                    'data'  => $data,
                ]
            );

    }

    public function updateDni(Request $request)
    {
		$persona_id        = $request->input('persona_id');
		$dni               = $request->input('dni');
		$tipo_documento_id = 1;

    	$rules = ['dni'=> 'required|min:8|unique:per_natural|integer'] ;
    	$validator = \Validator::make(['dni'=> $dni],  $rules);

    	if ($validator->fails())
		 {
		 	$errors = (array)$validator->errors()->toArray();

		 	return \Response::json([
				'message' => 'Operación Fallida',
				'error'   => true,
				'data'    => $errors ,
			]);
        }
        else
        {

			$per_natural = PerNatural::where('persona_id',$persona_id)->first();
			$per_natural->dni = $dni ;
			$per_natural->save() ;

			//  per_documento
			$per_documento = PerDocumento::where('persona_id',$persona_id)
										   ->where('tipo_documento_id',$tipo_documento_id)
										   ->first();
			$per_documento->numero = $dni ;
			$per_documento->save() ;

			$data = perNatural::where('persona_id',$persona_id)->first() ;

            return \Response::json([
				'message' => 'Registro Correcto',
				'error'   => false,
				'data'    => $data ,
			]);
        }
    }

	public function updatePerNatInfo(Request $request)
    {
		$apellidos     = $request->input('apellidos');
		$nombres       = $request->input('nombres');
		$sexo          = $request->input('sexo');
		$persona_id    = $request->input('persona_id');
		$per_fecha_nac = $request->input('fecha_nac');


		try {

			DB::beginTransaction();

				$persona = Persona::find($persona_id);
				$persona->per_nombre    = $nombres ;
				$persona->per_apellidos = $apellidos ;
				$persona->per_fecha_nac = $per_fecha_nac ;
				$persona->save() ;
				// dd($persona);

				$per_natural = PerNatural::where('persona_id',$persona_id)->first();
				$per_natural->nombres   = $nombres ;
				$per_natural->apellidos = $apellidos ;
				$per_natural->sexo      = $sexo ;
				$per_natural->save() ;
				// dd($per_natural);

			DB::commit();

			$data = Persona::with('perNatural')->where('id',$persona_id)->first();

			return \Response::json([
				'message' => 'Registro Correcto',
				'error'   => false,
				'data'    => $data ,
			]);

        } catch (Exception $e)
        {
            DB::rollback();

            return \Response::json([
					'message' => 'Registro Correcto',
					'error'   => true,
					'data'    => 'Error: '.$e->getMessage()  ,
				]);
        }

    }

    public function updateEstado(Request $request)
    {

		$persona_id       = $request->input('persona_id') ;
		$estado           = $request->input('estado') ;
		$tipo_relacion_id = $request->input('tipo_relacion_id') ;

		if ($tipo_relacion_id == 2)
		{
			$persona =  PerNatural::where('persona_id',$persona_id)->first() ;
			$persona->estado = $estado ;
        	$persona->save() ;
        	$data = 'ok' ;
		}
		# cliente
		else if ($tipo_relacion_id == 3)
		{
		}
		# empleados
		else if ($tipo_relacion_id == 4)
		{
		}
		# avales
		else if ($tipo_relacion_id == 5)
		{
			$params = array(
	            'persona_id' => $persona_id,
	            'estado' => $estado,
	        );

			$avales_ctrl = new AvalesController();
			$data = $avales_ctrl->updateEstadoByPersonaId($params);
		}

        return response()->json(
                [
                	'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
    }
    # si queremso mostrar personas que solo pertenescas debemos modificar SQL
    public function getPerNaturalInfoBasica(Request $request)
	{
		$user = Auth::user();
    	$rol_id_user = $user->rol_id ;
    	$persona_id_padre = $user->persona_id_padre ;

    	if ($rol_id_user != 1)
    	{
			$data = Persona::where('per_tipo',1)
    					->where('persona.estado',1)
    					->join('per_relacion', function($join) use ($persona_id_padre)   {
						      $join->on('persona.id','=', 'per_relacion.persona_id')
						      ->where('per_relacion.estado','=',1)
						      ->where('per_relacion.tipo_relacion_id','=',2)
							->where('per_relacion.persona_id_padre','=',$persona_id_padre) ;
						    })
    					->join('per_natural', function($join) {
						      $join->on('persona.id','=', 'per_natural.persona_id')
						      ->where('per_natural.estado','=',1);
						    })
    					->leftJoin('per_mail', function($join) {
						      $join->on('persona.id','=', 'per_mail.persona_id')
						      ->where('per_mail.item','=',1);
						    })
    					->select([
    						'persona.id as persona_id',
    						'persona.per_nombre',
    						'persona.per_apellidos',
    						'persona.per_fecha_nac',
    						'per_natural.id as per_natural_id',
    						'per_natural.dni',
    						'per_natural.sexo',
    						'per_natural.estado_civil',
    						'per_mail.id as per_mail_id',
    						'per_mail.mail',
						 	\DB::raw('CONCAT(persona.per_apellidos, " ", persona.per_nombre) AS full_name') ,
    					 ])
    					->orderBy('persona.per_apellidos','asc')
    					->get();
    					// dd($data->toArray());
    	}else
    	{ # usuarario sudo
    		$data = Persona::where('per_tipo',1)
    					->where('persona.estado',1)
    					->join('per_relacion', function($join)  {
						      $join->on('persona.id','=', 'per_relacion.persona_id')
						      ->where('per_relacion.estado','=',1)
						      ->where('per_relacion.tipo_relacion_id','=',2) ;
						    })
    					->join('per_natural', function($join) {
						      $join->on('persona.id','=', 'per_natural.persona_id')
						      ->where('per_natural.estado','=',1);
						    })
    					->leftJoin('per_mail', function($join) {
						      $join->on('persona.id','=', 'per_mail.persona_id')
						      ->where('per_mail.item','=',1);
						    })
    					->select([
    						'persona.id as persona_id',
    						'persona.per_nombre',
    						'persona.per_apellidos',
    						'persona.per_fecha_nac',
    						'per_natural.id as per_natural_id',
    						'per_natural.dni',
    						'per_natural.sexo',
    						'per_natural.estado_civil',
    						'per_mail.id as per_mail_id',
    						'per_mail.mail',
						 	\DB::raw('CONCAT(persona.per_apellidos, " ", persona.per_nombre) AS full_name') ,
    					 ])
    					->orderBy('persona.per_apellidos','asc')
    					->get();
    					// dd($data->toArray());

       	}
		return \Response::json([
							'message' => 'Operación Correcta',
							'error'=> false ,
							'data' => $data ,
						]) ;
	}


}
