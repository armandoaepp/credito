<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\PerWeb ;
use App\Models\TipoWeb ;

class PerWebsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getWebsByPersonaId(Request $request)
    {
		$persona_id = $request->input('persona_id') ;


        $data = $this->getPerWebsByPersonaId($persona_id) ;

    	return response()->json([
    		'error'=> false ,
    		'data' => $data ,
    		]);

    }

    protected function getPerWebsByPersonaId($persona_id)
    {
        $data = PerWeb::where('persona_id', $persona_id)
                            ->orderBy('item','asc')
                            ->get() ;

        return $data ;

    }

    public function delete(Request $request)
    {
        $per_web_id = $request->input('per_web_id') ;
        // $per_web_id = 1 ;

        $web = PerWeb::find($per_web_id);
        $persona_id = $web->persona_id ;
        $web->delete();

        # recuperamos la data despues de eliminar
        $data = $this->getPerWebsByPersonaId($persona_id) ;

        return response()->json([
            'error'=> false ,
            'data' => $data ,
            ]);
    }

    public function update(Request $request)
    {
		$per_web_id = $request->input('per_web_id') ;
		$url        = $request->input('url') ;

        $per_web = PerWeb::find($per_web_id);
        if ($per_web)
        {
	        $per_web->url = $url  ;
	        $per_web->save();

	        $persona_id = $per_web->persona_id ;

        }

        # recuperamos la data despues de eliminar
        $data = $this->getPerWebsByPersonaId($persona_id) ;

        return response()->json([
            'error'=> false ,
            'data' => $data ,
            ]);
    }

    public function save(Request $request)
    {
		$url         = $request->input('url') ;
		$tipo_web_id = $request->input('tipo_web_id') ;
		$persona_id  = $request->input('persona_id') ;

		/*$url         = 'hasjdfhjaksd ' ;
		$tipo_web_id = 1 ;
		$persona_id  = 1  ;*/

        $item_max = \DB::table('per_web')
                    ->where('persona_id',$persona_id)
                    ->max('item');

        $item_max = (empty($item_max)) ? 0 : $item_max ;

        $per_web =  new PerWeb();
		$per_web->url         = $url ;
		$per_web->tipo_web_id = $tipo_web_id ;
		$per_web->persona_id  = $persona_id ;
		$per_web->item        = $item_max +1 ;
        $per_web->save();

        # recuperamos la data despues de eliminar
        $data = $this->getPerWebsByPersonaId($persona_id) ;


        return response()->json([
            'error'=> false ,
            'data' => $data ,
            ]);
    }

    public function reOrderItems(Request $request)
    {
        $webs = $request->input('webs');
        $persona_id = $request->input('persona_id') ;

        if(count($webs) > 0 )
        {
            for ($i=0; $i < count($webs); $i++)
            {
                $new_item = ($i+1) ;
                PerWeb::where('id', $webs[$i]['id'])
                            ->update(['item' => $new_item]);
            }
        }

        $data = PerWeb::where('persona_id',$persona_id)
                            ->orderBy('item','ASC')
                            ->get() ;

        return response()->json([
            'message' => 'Reordenacion Correcta',
            'error'=> false ,
            'data' => $data ,
            ]);
    }

    public function getTipoWebs()
    {
    	$data = TipoWeb::where('estado',1)
                            ->orderBy('nombre','ASC')
                            ->get() ;

        return response()->json([
            'message' => 'Operación Correcta',
            'error'=> false ,
            'data' => $data ,
            ]);
    }

}
