<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Empleado ;
use App\Models\CargoEmpleado ;
use App\Models\PerRelacion ;

class EmpleadosController extends Controller
{
	public function getEmpleados(Request $request)
	{
		$user = \Auth::user();

    	$rol_id_user = $user->rol_id ;
    	$persona_id_padre = $user->persona_id_padre ;


    	if ($rol_id_user > 1)
    	{
    		$data = Empleado::where('empleado.estado',1)
    					->where('empleado.persona_id_padre',$persona_id_padre )
    					->join('cargo_empleado', function($join) {
						      $join->on('cargo_empleado.empleado_id','=', 'empleado.id')
						      ->where('cargo_empleado.estado','=',1);
						    })
    					->join('cargo', function($join) {
						      $join->on('cargo.id','=', 'cargo_empleado.cargo_id')
						      ->where('cargo.estado','=',1);
						    })
    					->join('persona', function($join) {
						      $join->on('persona.id','=', 'empleado.persona_id')
						      ->where('persona.estado','=',1);
						    })

    					->join('per_natural', function($join) {
						      $join->on('persona.id','=', 'per_natural.persona_id')
						      ->where('per_natural.estado','=',1);
						    })
    					->leftJoin('per_telefono', function($join) {
						      $join->on('persona.id','=', 'per_telefono.persona_id')
						      ->where('per_telefono.item','=',1);
						    })
    					->leftJoin('per_mail', function($join) {
						      $join->on('persona.id','=', 'per_mail.persona_id')
						      ->where('per_mail.item','=',1);
						    })
    					->select([
    						'empleado.id',
    						'persona.id as persona_id',
    						'persona.per_fecha_nac',
    						'per_natural.id as per_natural_id',
    						'per_natural.dni',
    						'persona.per_nombre',
    						'persona.per_apellidos',
    						'per_natural.sexo',
    						'per_natural.estado_civil',
    						'per_telefono.id as per_telefono_id',
    						'per_telefono.telefono',
    						'per_mail.id as per_mail_id',
    						'per_mail.mail',
    						'cargo_empleado.id as cargo_empleado_id',
    						'cargo_empleado.cargo_id',
    						'cargo.area_id',
    						'cargo.nombre as cargo',
    					 ])
    					->orderBy('empleado.id','desc')
    					->get();
    					// dd($data->toArray());
    	}else
    	{
    		# cuando ingrese como sudo
    		$data = Empleado::where('empleado.estado',1)
    					->join('cargo_empleado', function($join) {
						      $join->on('cargo_empleado.empleado_id','=', 'empleado.id')
						      ->where('cargo_empleado.estado','=',1);
						    })
    					->join('cargo', function($join) {
						      $join->on('cargo.id','=', 'cargo_empleado.cargo_id')
						      ->where('cargo.estado','=',1);
						    })
    					->join('persona', function($join) {
						      $join->on('persona.id','=', 'empleado.persona_id') ;
						      // ->where('persona.estado','=',1);
						    })
    					->join('per_natural', function($join) {
						      $join->on('persona.id','=', 'per_natural.persona_id')
						      ->where('per_natural.estado','=',1);
						    })
    					->leftJoin('per_telefono', function($join) {
						      $join->on('persona.id','=', 'per_telefono.persona_id')
						      ->where('per_telefono.item','=',1);
						    })
    					->leftJoin('per_mail', function($join) {
						      $join->on('persona.id','=', 'per_mail.persona_id')
						      ->where('per_mail.item','=',1);
						    })
    					->select([
    						'empleado.id',
    						'persona.id as persona_id',
    						'persona.per_fecha_nac',
    						'per_natural.id as per_natural_id',
    						'per_natural.dni',
    						'persona.per_nombre',
    						'persona.per_apellidos',
    						'per_natural.sexo',
    						'per_natural.estado_civil',
    						'per_telefono.id as per_telefono_id',
    						'per_telefono.telefono',
    						'per_mail.id as per_mail_id',
    						'per_mail.mail',
    						'cargo_empleado.id as cargo_empleado_id',
    						'cargo_empleado.cargo_id',
    						'cargo.area_id',
    						'cargo.nombre as cargo',

    					 ])
    					->orderBy('empleado.id','desc')
    					->get();

    				// dd($data->toArray());
    	}

    	return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}

	public function save(Request $request)
    {
    	$user = \Auth::user() ;

		$persona_id_padre = $user->persona_id_padre ;

		$persona_id = $request->input('persona_id') ;
		$cargo_id   = $request->input('cargo_id') ;

		$error = false;
		$data  = array();

			$empleado =  Empleado::where('persona_id',$persona_id)
								->where('persona_id_padre',$persona_id_padre)
								// ->where('cargo_id',$cargo_id)
								->first();
			if (!$empleado)
			{
				# save data
					$empleado =  new Empleado() ;
					$empleado->persona_id       =  $persona_id ;
					$empleado->persona_id_padre =  $persona_id_padre ;
					$empleado->estado           =  1 ;
					$empleado->save() ;

					$empleado_id = $empleado->id ;

					$cargo_empleado =  new CargoEmpleado() ;
					$cargo_empleado->cargo_id   =  $cargo_id ;
					$cargo_empleado->empleado_id =  $empleado_id ;
					$cargo_empleado->estado     =  1 ;
					$cargo_empleado->save() ;

					# ==== ASIGNAR RELACION PERSONA(EMPRESA) -> EMPLEADO ======================
			    		$per_relacion = new PerRelacion() ;
						$per_relacion->persona_id_padre = $persona_id_padre ;
						$per_relacion->tipo_relacion_id = 4 ;
						$per_relacion->persona_id       = $persona_id;
						$per_relacion->referencia       = 'Empleado - Empresa' ;
						// $per_relacion->created_at       = $created_at;
						$per_relacion->estado           = 1 ;
						$per_relacion->save();
			}
			else
			{
				$empleado->estado =  1 ;
				$empleado->save() ;
			}

			return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }

    public function update(Request $request)
    {
    	// $empleado_id = $request->input('empleado_id');
    	$cargo_empleado_id = $request->input('cargo_empleado_id');
    	$cargo_id = $request->input('cargo_id');

    	$cargo_empleado = CargoEmpleado::find($cargo_empleado_id);
    	$cargo_empleado->cargo_id = $cargo_id ;
    	$cargo_empleado->save() ;

    	$data = "OK" ;
    	return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }

    public function updateEstado(Request $request)
    {
		$empleado_id = $request->input('empleado_id');
		$estado      = $request->input('estado');

    	$empleado = Empleado::find($empleado_id);
    	$empleado->estado = $estado ;
    	$empleado->save() ;

    	$data = "OK" ;
    	return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }

    public function asignarNewCargo(Request $request)
    {
        $cargo_empleado_id = $request->input('cargo_empleado_id');
        $empleado_id       = $request->input('empleado_id');
        $cargo_id          = $request->input('cargo_id');

        $cargo_empleado = CargoEmpleado::find($cargo_empleado_id);
        $cargo_id_actual = $cargo_empleado->cargo_id ;

        $data = "" ;
        $error = false ;
        if ($cargo_id_actual == $cargo_id )
        {
            $data = "Nuevo Cargo es igual al Cargo Actual " ;
            $error = true ;

        }else
        {
            # damos de baja al cargo actual
            $cargo_empleado->estado = 0 ;
            $cargo_empleado->save() ;

            # creamos el nuevo cargo
            $new_cargo_empleado = new CargoEmpleado() ;
            $new_cargo_empleado->empleado_id = $empleado_id ;
            $new_cargo_empleado->cargo_id = $cargo_id ;
            $new_cargo_empleado->save() ;
            $data = "OK" ;

        }



        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => $error,
                        'data'    => $data,
                    ]);

    }

     # empleados con informacion basica generalmente para select(combox )
    public function getEmpleadosInfoBasica(Request $request)
    {
        $user = \Auth::user();
        $rol_id_user = $user->rol_id ;
        $persona_id_padre = $user->persona_id_padre ;

        if ($rol_id_user > 1)
        {
            $data = Empleado::where('empleado.estado',1)
                        ->where('empleado.persona_id_padre',$persona_id_padre )
                        ->join('persona', function($join) {
                              $join->on('persona.id','=', 'empleado.persona_id')
                              ->where('persona.estado','=',1);
                            })
                        ->join('per_natural', function($join) {
                              $join->on('persona.id','=', 'per_natural.persona_id')
                              ->where('per_natural.estado','=',1);
                            })
                        ->leftJoin('per_mail', function($join) {
                              $join->on('persona.id','=', 'per_mail.persona_id')
                              ->where('per_mail.item','=',1);
                            })
                        ->select([
                            'empleado.id',
                            'persona.id as persona_id',
                            'persona.per_fecha_nac',
                            'per_natural.id as per_natural_id',
                            'per_natural.dni',
                            'persona.per_nombre',
                            'persona.per_apellidos',
                            'per_natural.sexo',
                            'per_natural.estado_civil',
                            'per_mail.id as per_mail_id',
                            'per_mail.mail',
                             \DB::raw('CONCAT(persona.per_apellidos, " ", persona.per_nombre) AS full_name') ,
                         ])
                        ->orderBy('persona.per_apellidos','asc')
                        ->get();
        }
        else
        {
            $data = Empleado::where('empleado.estado',1)
                        ->join('persona', function($join) {
                              $join->on('persona.id','=', 'empleado.persona_id')
                              ->where('persona.estado','=',1);
                            })
                        ->join('per_natural', function($join) {
                              $join->on('persona.id','=', 'per_natural.persona_id')
                              ->where('per_natural.estado','=',1);
                            })
                        ->leftJoin('per_mail', function($join) {
                              $join->on('persona.id','=', 'per_mail.persona_id')
                              ->where('per_mail.item','=',1);
                            })
                        ->select([
                            'empleado.id',
                            'persona.id as persona_id',
                            'persona.per_fecha_nac',
                            'per_natural.id as per_natural_id',
                            'per_natural.dni',
                            'persona.per_nombre',
                            'persona.per_apellidos',
                            'per_natural.sexo',
                            'per_natural.estado_civil',
                            'per_mail.id as per_mail_id',
                            'per_mail.mail',
                             \DB::raw('CONCAT(persona.per_apellidos, " ", persona.per_nombre) AS full_name') ,
                         ])
                        ->orderBy('persona.per_apellidos','asc')
                        ->get();

        }

        return \Response::json([
                            'message' => 'Operación Correcta',
                            'error'=> false ,
                            'data' => $data ,
                        ]) ;

    }

}
