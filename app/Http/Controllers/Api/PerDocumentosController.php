<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\PerDocumento ;

class PerDocumentosController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	protected function validator(array $data)
    {
        return \Validator::make($data, [
			'persona_id'        => 'required',
			'numero'            => 'required|min:8|unique:per_documento|integer',
			'tipo_documento_id' => 'required',
        ]);
    }


	public function updateNumero(Request $request)
	{
		$inputs = $request->all() ;
		// $inputs = array('persona_id'=> '53', 'numero'=> '43378409' , 'tipo_documento_id' => '1') ;

		$validator = $this->validator($inputs) ;

		if ($validator->fails())
		 {
		 	$errors = (array)$validator->errors()->toArray();

		 	return \Response::json([
				'message' => 'Registro Fallido',
				'error'   => true,
				'data'    => $errors ,
			]);
        }
        else
        {
        	$persona_id        = $inputs['persona_id'] ;
			$numero            = $inputs['numero'];
			$tipo_documento_id = $inputs['tipo_documento_id'];

			$per_ducumento = PerDocumento::where('persona_id',$persona_id)
										   ->where('tipo_documento_id',$tipo_documento_id)
										   ->first();

			$data = $per_ducumento;

            return \Response::json([
				'message' => 'Registro Correcto',
				'error'   => false,
				'data'    => $data ,
			]);
        }

        /*return $request->json([
        	"error"=> $error ,
        	'data' => $data
        	]);*/


	}
}
