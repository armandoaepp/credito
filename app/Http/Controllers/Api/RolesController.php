<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Rol ;
use App\Models\RolControl ;

class RolesController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRoles()
    {
        $user = \Auth::user();

        $rol_id_user = $user->rol_id ;
        $persona_id_padre = $user->persona_id_padre ;


        if ($rol_id_user > 2)
        {

            $data = Rol::where('estado',1)
                    ->get() ;
        }
        # super user
        else if ($rol_id_user == 2)
        {

            $data = Rol::whereIn('estado', [1, 2])
                    ->where('id','>',1)
                    ->get() ;
        }
        # sudo
        else if ($rol_id_user == 1)
        {

            $data = Rol::whereIn('estado', [1, 2])
                    ->get() ;
        }

    	return response()->json(
	            [
	                'error' => false,
	                'data' => $data,
	            ]
	        );
    	// dd($data) ;
    }

    public function getRolByNombre(Request $request)
    {

        $nombre    = $request->input('nombre') ;

        $data = Rol::where('nombre',$nombre)
                    ->get() ;

        return response()->json(
                [
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function save(Request $request)
    {

        $nombre    = $request->input('nombre') ;

        $data = Rol::firstOrCreate(['nombre' => $nombre]);

        return response()->json(
                [
                    'error' => false,
                    'data'  => $data,
                ]
            );
    }

    public function getRolById(Request $request)
    {

        $rol_id    = $request->input('rol_id') ;

        $data = Rol::where('id',$rol_id)
                    ->first() ;

        return response()->json(
                [
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function update(Request $request)
    {

        $rol_id    = $request->input('rol_id') ;
        $nombre    = $request->input('nombre') ;

        $rol =  Rol::find($rol_id) ;
        $rol->nombre = $nombre ;
        $rol->save() ;


         $data =  Rol::find($rol_id) ;
        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
    }

    public function updateEstado(Request $request)
    {

        $rol_id    = $request->input('codigo') ;
        $estado    = $request->input('estado') ;

        $rol =  Rol::find($rol_id) ;
        $rol->estado = $estado ;
        $rol->save() ;

        return response()->json(
                [
                    'error' => false,
                    'data'  => $estado,
                ]
            );
    }

    public function getAccesosRol(Request $request)
    {
        $rol_id = $request->input('rol_id') ;
        // $rol_id = 1;

        $sql = "SELECT
                    control.id,
                    control.control_padre_id,
                    control.jerarquia,
                    control.nombre,
                    control.valor,
                    control.descripcion,
                    control.tipo_control_id,
                    control.glosa ,
                    (IF ( (SELECT rol_control.id  FROM rol_control
                            WHERE rol_control.control_id = control.id
                            AND rol_control.rol_id = $rol_id
                            AND rol_control.estado = 1) > 0  , 1 , 0)
                    ) AS is_active
                FROM control
                WHERE  control.estado =  1
                ORDER BY control.jerarquia  ASC;" ;

        $data = \DB::select($sql) ;

       return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);

    }

    public function updateAccesosRol(Request $request)
    {
        $rol_id = $request->input('rol_id');
        $data   = $request->input('controles');

        /*return \Response::json([
                'message' => 'Operación Correcta',
                'error' => false ,
                'data' => $data ,
            ]);*/


        /*$rol_id = 2;
        $controles = '[{"id":1,"control_padre_id":null,"jerarquia":"10","nombre":"Gestión de Información","valor":"","descripcion":"Gestión de Información","tipo_control_id":1,"glosa":"","is_active":false,"children":[{"id":2,"control_padre_id":1,"jerarquia":"1001","nombre":"Maestros","valor":"maestros","descripcion":"menu","tipo_control_id":2,"glosa":"","is_active":true,"children":[{"id":3,"control_padre_id":2,"jerarquia":"100101","nombre":"Personas","valor":"maestros.personas","descripcion":"sub menu","tipo_control_id":2,"glosa":"","is_active":true,"children":[{"id":4,"control_padre_id":3,"jerarquia":"10010101","nombre":"Listar","valor":"list","descripcion":"fa-list","tipo_control_id":3,"glosa":"","is_active":true,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":5,"control_padre_id":3,"jerarquia":"10010102","nombre":"Nuevo","valor":"new","descripcion":"fa-plus","tipo_control_id":3,"glosa":"","is_active":true,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":6,"control_padre_id":3,"jerarquia":"10010103","nombre":"Editar","valor":"edit","descripcion":"fa-pencil-square-o","tipo_control_id":3,"glosa":"","is_active":true,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false}],"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false}],"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":8,"control_padre_id":1,"jerarquia":"1002","nombre":"Accesos","valor":"accesos","descripcion":"menu","tipo_control_id":2,"glosa":"","is_active":false,"children":[{"id":9,"control_padre_id":8,"jerarquia":"100201","nombre":"Roles","valor":"accesos.roles","descripcion":"sub menu","tipo_control_id":2,"glosa":"","is_active":false,"children":[{"id":10,"control_padre_id":9,"jerarquia":"10020101","nombre":"Listar","valor":"list","descripcion":"fa-list","tipo_control_id":3,"glosa":"inbarra","is_active":false,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":11,"control_padre_id":9,"jerarquia":"10020102","nombre":"Nuevo","valor":"new","descripcion":"fa-plus","tipo_control_id":3,"glosa":"inbarra","is_active":false,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":12,"control_padre_id":9,"jerarquia":"10020103","nombre":"Editar","valor":"edit","descripcion":"fa-pencil-square-o","tipo_control_id":3,"glosa":"intable","is_active":false,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":13,"control_padre_id":9,"jerarquia":"10020104","nombre":"Eliminar","valor":"delete","descripcion":"fa-trash-o","tipo_control_id":3,"glosa":"intable","is_active":false,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":14,"control_padre_id":9,"jerarquia":"10020105","nombre":"Accesos Rol","valor":"access","descripcion":"fa-check-square-o","tipo_control_id":3,"glosa":"inbarra","is_active":false,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false}],"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":15,"control_padre_id":8,"jerarquia":"100202","nombre":"Usuarios","valor":"accesos.usuarios","descripcion":"sub menu","tipo_control_id":2,"glosa":"","is_active":false,"children":[{"id":16,"control_padre_id":15,"jerarquia":"10020201","nombre":"Listar","valor":"list","descripcion":"fa-list","tipo_control_id":3,"glosa":"","is_active":false,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":17,"control_padre_id":15,"jerarquia":"10020202","nombre":"Nuevo","valor":"new","descripcion":"fa-plus","tipo_control_id":3,"glosa":"","is_active":false,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":18,"control_padre_id":15,"jerarquia":"10020203","nombre":"Editar","valor":"edit","descripcion":"fa-pencil-square-o","tipo_control_id":3,"glosa":"","is_active":false,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":19,"control_padre_id":15,"jerarquia":"10020204","nombre":"Eliminar","valor":"delete","descripcion":"fa-trash-o","tipo_control_id":3,"glosa":"","is_active":false,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":20,"control_padre_id":15,"jerarquia":"10020205","nombre":"accesos","valor":"access","descripcion":"fa-check-square-o","tipo_control_id":3,"glosa":"","is_active":false,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false}],"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false}],"__ivhTreeviewExpanded":false,"__ivhTreeviewIndeterminate":false}],"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":true}] ';
        */
        #

        /*$data = json_decode($controles) ;
        $data = json_decode(json_encode($data), true);
        */

        $update_rol_ctrl = RolControl::where('rol_id', $rol_id)
                                    ->update(['estado' => 0]);

        $data_save = array();
        array_walk_recursive($data, function(&$value, &$uid) use (&$data_save) {
          $data_save[$uid][] = $value;
        });
        // dd($data_save);

        for ($i=0; $i < count($data_save['id']) ; $i++)
        {
            $control_id = $data_save['id'][$i] ;
            $estado     = $data_save['isSelected'][$i] ? 1 : 0 ;
            $referencia = $data_save['nombre'][$i] ;

            $rol_control = RolControl::where('rol_id',$rol_id)
                            ->where('control_id',$control_id)
                            ->first() ;
            if ($rol_control )
            {
                 $rol_control->estado = $estado ;
                 $rol_control->save() ;
            }else
            {
                if ($estado == 1 )
                {
                    RolControl::create(array(
                            'rol_id'     => $rol_id,
                            'control_id' => $control_id,
                            'referencia' => $referencia,
                            'estado'     => $estado,
                        ));
                }

            }
        }

        $data = "registro correcto" ;
        return \Response::json([
                'message' => 'Operación Correcta',
                'error' => false ,
                'data' => $data ,
            ]);

    }


}
