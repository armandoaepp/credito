<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\PerTelefono ;

class PerTelefonosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getTelefonosByPersonaId(Request $request)
    {
		$persona_id = $request->input('persona_id') ;

    	/*$data = PerTelefono::where('persona_id', $persona_id)
                            ->orderBy('item','asc')
                            ->get() ;*/
        $data = $this->getPhonesByPersonaId($persona_id) ;

    	return response()->json([
    		'error'=> false ,
    		'data' => $data ,
    		]);

    }

    protected function getPhonesByPersonaId($persona_id)
    {
        $data = PerTelefono::where('persona_id', $persona_id)
                            ->orderBy('item','asc')
                            ->get() ;

        return $data ;

    }

    public function delete(Request $request)
    {
        $telefono_id = $request->input('telefono_id') ;
        // $telefono_id = 1 ;

        $telefono = PerTelefono::find($telefono_id);
        $persona_id = $telefono->persona_id ;
        $telefono->delete();

        # recuperamos la data despues de eliminar
        // $data = PerTelefono::where('persona_id', $persona_id)->get() ;
        $data = $this->getPhonesByPersonaId($persona_id) ;

        return response()->json([
            'error'=> false ,
            'data' => $data ,
            ]);
    }

    public function update(Request $request)
    {
        $telefono_id = $request->input('telefono_id') ;
        $numero      = $request->input('telefono') ;


        $telefono = PerTelefono::find($telefono_id);
        $telefono->telefono = $numero  ;
        $telefono->save();

        $persona_id = $telefono->persona_id ;

        # recuperamos la data despues de eliminar
        // $data = PerTelefono::where('persona_id', $persona_id)->get() ;
        $data = $this->getPhonesByPersonaId($persona_id) ;

        return response()->json([
            'error'=> false ,
            'data' => $data ,
            ]);
    }

    public function saveMultiples(Request $request)
    {
        $persona_id = $request->input('persona_id') ;
        $telefonos  = $request->input('telefonos') ;

        $item_max = \DB::table('per_telefono')
                    ->where('persona_id',$persona_id)
                    ->max('item');

        $item_max = (empty($item_max)) ? 0 : $item_max ;

        if(count($telefonos) > 0 )
        {
            $fills =  array() ;
            for ($i=0; $i < count($telefonos); $i++)
            {
                // dd($telefonos[$i]['telefono']);
                $fill = array(
                                'persona_id'       => $persona_id ,
                                'tipo_telefono_id' => 1 ,
                                'telefono'         => $telefonos[$i]['telefono'],
                                'item'             => $item_max + ($i+1)
                          ) ;

                array_push($fills,$fill) ;

            }
            PerTelefono::insert($fills) ;
        }

        # recuperamos la data despues de eliminar
        // $data = PerTelefono::where('persona_id', $persona_id)->get() ;
        $data = $this->getPhonesByPersonaId($persona_id) ;

        return response()->json([
            'error'=> false ,
            'data' => $data ,
            ]);
    }

    public function reOrderItems(Request $request)
    {
        $telefonos = $request->input('telefonos');
        $persona_id = $request->input('persona_id') ;

        if(count($telefonos) > 0 )
        {
            for ($i=0; $i < count($telefonos); $i++)
            {
                $new_item = ($i+1) ;
                PerTelefono::where('id', $telefonos[$i]['id'])
                            ->update(['item' => $new_item]);
            }
        }

        $data = PerTelefono::where('persona_id',$persona_id)
                            ->orderBy('item','ASC')
                            ->get() ;

        return response()->json([
            'message' => 'Reordenacion Correcta',
            'error'=> false ,
            'data' => $data ,
            ]);
    }


}
