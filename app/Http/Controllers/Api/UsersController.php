<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Controllers\Api\AccesosUsersController;

use DB ;
use Auth ;
use App\Models\Persona ;
use App\Models\PerImagen ;
use App\Models\Rol ;
use App\User ;
use App\Models\PerRelacion ;
use App\Models\Notificacion ;
use App\Models\PerTelefono ;

class UsersController extends Controller
{

	// use AccesosUsersController ;

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function getUsers(Request $request)
	{
		# para el usuario SUDO
			$user = Auth::user();
			$rol_id_user      = $user->rol_id ;
			$persona_id_padre = $user->persona_id_padre ;

			if ($rol_id_user == 2 )
				$start_rol_id = 1 ;
			if ($rol_id_user > 2 )
				$start_rol_id = 2 ;

	    	if ($rol_id_user > 1 )
	    	{
	    		$data =  User::where('users.estado',1)
	    			->where('users.persona_id_padre',$persona_id_padre)
	    			->where('users.rol_id','>',$start_rol_id)
					->join('persona', 'users.persona_id', '=', 'persona.id')
					->join('per_natural', 'persona.id', '=', 'per_natural.persona_id')
					->join('rol', function($join){
					        $join->on('users.rol_id', '=', 'rol.id') ;
					        	 // ->where('rol.estado','=',1);
					    })
					->select([
						'users.id',
						'users.persona_id',
						'users.persona_id_padre',
						'users.email',
						'users.rol_id',
						'rol.nombre as rol',
						'persona.per_nombre',
						'persona.per_apellidos',
						'per_natural.dni',
						 \DB::raw('concat(persona.per_apellidos, " ", persona.per_nombre) AS full_name') ,
						])
					->orderBy('users.id','desc')
					->get() ;
	    	}
	    	else
	    	{
	    		$data =  User::where('users.estado',1)
					->join('persona', 'users.persona_id', '=', 'persona.id')
					->join('per_natural', 'persona.id', '=', 'per_natural.persona_id')
					->join('rol', function($join){
					        $join->on('users.rol_id', '=', 'rol.id') ;
					        	 // ->where('rol.estado','=',1);
					    })
					->select([
						'users.id',
						'users.persona_id',
						'users.persona_id_padre',
						'users.email',
						'users.rol_id',
						'rol.nombre as rol',
						'persona.per_nombre',
						'persona.per_apellidos',
						'per_natural.dni',
						 \DB::raw('concat(persona.per_apellidos, " ", persona.per_nombre) AS full_name') ,
						])
					->orderBy('users.id','desc')
					->get() ;
	    	}




		return \Response::json([
							'message' => 'Operación Correcta',
							'error'=> false ,
							'data' => $data ,
						]) ;
	}

	public function getInfo()
	{
	    $user = Auth::user() ;
	    $persona_id =  $user->persona_id ;
	    $rol_id = $user->rol_id ;

	    $persona = Persona::where('id',$persona_id)
	    		->first() ;
	    $persona = $persona->toArray() ;

	    $per_imagen = PerImagen::where('persona_id',$persona_id)
	    		->first() ;
	    if (!empty($per_imagen))
	    {
			$avatar = $per_imagen->url ;
	    }else
	    {
	    	$avatar = 'img_avatars/avatar_user.png' ; ;
	    }

	    $rol = Rol::where('id',$rol_id)
	    		->first() ;


	    $info = array(
			'avatar' => $avatar,
			'rol_id' => $rol_id,
			'rol'    => $rol->nombre,
	    	) ;

	    $data =array_merge($persona, $info) ;



	   return response()->json(
	            [
	                'error' => false,
	                'data' => $data,
	            ]
	        );
	}

	public function save(Request $request)
	{
		$inputs = $request->all() ;

		/*$inputs = array(
						'persona_id' => 166,
						'email'      => "Rau.Winfield@Kihn.org",
						'password'   => "dasdfasdfasd",
						'rol_id'     => 1
					);*/

		$validator = \Validator::make($inputs, [
			'persona_id' => 'required|integer',
			'rol_id'     => 'required|integer',
			'email'      => 'required|email|max:255|unique:users',
			'password'   => 'required|min:6',
        ]);

    	// $validator = \Validator::make(['dni'=> $dni],  $rules);

    	if ($validator->fails())
		{
		 	$errors = (array)$validator->errors()->toArray();

		 	return \Response::json([
				'message' => 'Validacion Incorrecta',
				'error'   => true,
				'data'    => $errors ,
			]);
        }
        else
        {
        	# save data
				$user = \Auth::user();
				$persona_id_padre = $user->persona_id_padre ;

				try
				{
					DB::beginTransaction();

						$persona_id = $inputs['persona_id'];
						$mail       = $inputs['email'];
						$password   = $inputs['password'];
						$rol_id     = $inputs['rol_id'];

						$user_alias = explode("@", $mail);
						$user_alias = $user_alias[0];

						$user =  new User();
						$user->persona_id       = $persona_id  ;
						$user->rol_id           = $rol_id;
						$user->email            = $mail ;
						$user->password         = $password;
						$user->alias            = $user_alias ;
						$user->persona_id_padre = $persona_id_padre ;

		                $user->save();
		                $user_id = $user->id;

		                // creamos la relacion de persona usuario
						$per_relacion = new PerRelacion() ;
						$per_relacion->persona_id   = $persona_id;
						$per_relacion->persona_id_padre = $persona_id_padre;
						$per_relacion->tipo_relacion_id = 1 ;
						$per_relacion->referencia       = 'Usuario -- Persona';
						$per_relacion->created_at       = date('Y-m-d H:m:s') ;
						$per_relacion->estado           = 1;
		                $per_relacion->save();

		                # genera accesos del usuario
		                    $accesos = array(
		                                    'user_id'    => $user_id ,
		                                    'rol_id'     => $rol_id,
		                                ) ;
		                    $accesosCtrl = new AccesosUsersController() ;
		                    $datos = $accesosCtrl->createUserAccesos($accesos) ;

		                # notificacion via SMS
							$per_telefono = PerTelefono::where('persona_id', $persona_id)
										->where('item',1)
										->first();

		                    $phone_number = $per_telefono->telefono;
		                    // dd($phone_number);

		                    $notificacion = new Notificacion() ;
							$notificacion->user_id     = $user_id ;
							$notificacion->destino     = $phone_number;
							$notificacion->asunto      = 'Credenciales Usuario';
							$notificacion->mensaje     = 'Usuario: '.$mail.' contraseña: '.$password.'\n mas informacion en prestamos del norte ' ;
							$notificacion->referencia  = '';
							$notificacion->tipo        = 1;
							$notificacion->estado      = 1;
							$notificacion->save() ;


		                    # enviar crednciales
		                   /* $data_mail =  array(
		                            'ful_name' => $nombre ." ". $apellidos ,
		                            'password' => $password,
		                            'email'    => $mail,
		                        ) ;

		                    $this->sendUserCredenciales($data_mail) ;
						*/
					DB::commit();

	                return \Response::json([
						'message' => 'Operación Correcta',
						'error'   => false,
						'data'    => $datos,
					]);


				} catch (Exception $e) {
					 DB::rollback();

					return \Response::json([
						'message' => 'Operación Fallida',
						'error'   => true,
						'data'    => 'Error: '.$e->getMessage() ,
					]);

				}
		}
	}

	public function updateEstado(Request $request)
	{
		$user_id = $request->input('user_id');
		$estado  = $request->input('estado');

		/*$user_id = 523;
		$estado  = 0;*/
		try {

			DB::beginTransaction();

			$user =  user::find($user_id);
			$user->estado = $estado ;
			$user->save() ;

			$accesoCtrl =  new AccesosUsersController();
			$accesoCtrl->updateEstadoByUser($user_id, $estado) ;

			DB::commit();

			$data = $user ;

			return \Response::json([
				'message' => 'Registro Correcto',
				'error'   => false,
				'data'    => $data ,
			]);

        } catch (Exception $e)
        {
            DB::rollback();

            return \Response::json([
					'message' => 'Registro Correcto',
					'error'   => true,
					'data'    => 'Error: '.$e->getMessage()  ,
				]);
        }


	}



}
