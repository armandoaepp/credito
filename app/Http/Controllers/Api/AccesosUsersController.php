<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth ;
use DB ;

use App\User ;
use App\Models\Control ;
use App\Models\Acceso ;
use App\Models\RolControl ;

use App\Traits\TreeMenu ;


class AccesosUsersController extends Controller
{
    use TreeMenu;

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user() ;
        $user_id = $user->id ;
        $rol_id  = $user->rol_id ;

     /*   $sql = "SELECT
                    control.id,
                    control.control_padre_id,
                    control.jerarquia,
                    control.nombre,
                    control.valor,
                    control.descripcion,
                    control.tipo_control_id,
                    control.glosa
                FROM control
                INNER JOIN rol_control ON rol_control.control_id =  control.id
                INNER JOIN accesos ON accesos.control_id =  control.id
                INNER JOIN users ON users.id = accesos.user_id
                WHERE  control.estado =  1
                AND rol_control.estado = 1
                AND accesos.estado = 1
                AND users.estado =  1
                AND users.id = ".$user_id ."

                AND rol_control.rol_id = ".$rol_id ."
                AND users.rol_id =  ".$rol_id ."
                ORDER BY control.jerarquia  ASC;" ;*/

          $sql = "SELECT
                    control.id,
                    control.control_padre_id,
                    control.jerarquia,
                    control.nombre,
                    control.valor,
                    control.descripcion,
                    control.tipo_control_id,
                    control.glosa
                FROM control
                INNER JOIN accesos ON accesos.control_id =  control.id
                INNER JOIN users ON users.id = accesos.user_id
                WHERE  control.estado =  1
                AND accesos.estado = 1
                AND users.estado =  1
                AND users.id = $user_id
                ORDER BY control.jerarquia  ASC;" ;


        $data = DB::select($sql) ;
        // dd($data) ;

        if (count($data) > 0)
        {
            $elements = json_decode(json_encode($data), true) ;
            $data = $this->buildTreeMenu($elements, null) ;
         }

       return response()->json(
                [
                    'error' => false,
                    'data' => $data,
                ]
            );
    }

    # Accesos de Usuario Cuando se registran
    # public function createUserAccesos()
    public function createUserAccesos($data =  array() )
    {
        $rol_id = $data['rol_id'] ;
        $user_id = $data['user_id'] ;
        // $rol_id = 1 ;
        // $user_id = 2 ;
        $rol_control = RolControl::where('rol_id',$rol_id)
                        ->where('estado',1)
                        ->orderBy('control_id', 'ASC')
                        ->get();

        $data_insert = array() ;
        if (count($rol_control) > 0)
        {
            foreach ($rol_control as $row)
            {
                $fill = array(
                                'user_id' => $user_id ,
                                'control_id' => $row->control_id,
                                'referencia' => "",
                             ) ;
                array_push($data_insert, $fill) ;
            }
            # insertarmos los accesso
            Acceso::insert( $data_insert) ;
        }

        // return $rol_control ;
        return "ok" ;

    }

    public function updateEstadoByUser($user_id, $estado)
    {
        Acceso::where('user_id','=',$user_id)
            ->update(['estado'=>$estado]);

        return 'ok' ;

    }

    public function getControlAccesosUser(Request $request)
    {
        $user_id = $request->input('user_id') ;
        // $user_id = 1;

        $sql = "SELECT
                    control.id,
                    control.control_padre_id,
                    control.jerarquia,
                    control.nombre,
                    control.valor,
                    control.descripcion,
                    control.tipo_control_id,
                    control.glosa ,
                    (IF ( (SELECT accesos.id  FROM accesos
                            WHERE accesos.control_id = control.id
                            AND accesos.user_id = $user_id
                            AND accesos.estado = 1) > 0  , 1 , 0)
                    ) AS is_active
                FROM control
                WHERE  control.estado =  1
                ORDER BY control.jerarquia  ASC;" ;

        $data = DB::select($sql) ;

       /* if (count($data) > 0)
        {
            $elements = json_decode(json_encode($data), true) ;
            $data = $this->buildTreeMenu($elements, null) ;
         }*/
         ### el arbol lo contruimos en el template-> controller JS

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);

    }

    public function updateAccesosUser(Request $request)
    {
        $user_id = $request->input('user_id');
        $data    = $request->input('controles');

        // $user_id = 12;
        // $controles = '[{"id":1,"control_padre_id":null,"jerarquia":"10","nombre":"Gestión de Información","valor":"","descripcion":"Gestión de Información","tipo_control_id":1,"glosa":"","is_active":true,"children":[{"id":2,"control_padre_id":1,"jerarquia":"1001","nombre":"Maestros","valor":"maestros","descripcion":"menu","tipo_control_id":2,"glosa":"","is_active":true,"children":[{"id":3,"control_padre_id":2,"jerarquia":"100101","nombre":"Personas","valor":"maestros.personas","descripcion":"sub menu","tipo_control_id":2,"glosa":"","is_active":true,"children":[{"id":4,"control_padre_id":3,"jerarquia":"10010101","nombre":"Listar","valor":"list","descripcion":"fa-list","tipo_control_id":3,"glosa":"","is_active":true,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":5,"control_padre_id":3,"jerarquia":"10010102","nombre":"Nuevo","valor":"new","descripcion":"fa-plus","tipo_control_id":3,"glosa":"","is_active":true,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":6,"control_padre_id":3,"jerarquia":"10010103","nombre":"Editar","valor":"edit","descripcion":"fa-pencil-square-o","tipo_control_id":3,"glosa":"","is_active":true,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false}],"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false}],"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":8,"control_padre_id":1,"jerarquia":"1002","nombre":"Accesos","valor":"accesos","descripcion":"menu","tipo_control_id":2,"glosa":"","is_active":true,"children":[{"id":9,"control_padre_id":8,"jerarquia":"100201","nombre":"Roles","valor":"accesos.roles","descripcion":"sub menu","tipo_control_id":2,"glosa":"","is_active":true,"children":[{"id":10,"control_padre_id":9,"jerarquia":"10020101","nombre":"Listar","valor":"list","descripcion":"fa-list","tipo_control_id":3,"glosa":"inbarra","is_active":true,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":11,"control_padre_id":9,"jerarquia":"10020102","nombre":"Nuevo","valor":"new","descripcion":"fa-plus","tipo_control_id":3,"glosa":"inbarra","is_active":true,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":12,"control_padre_id":9,"jerarquia":"10020103","nombre":"Editar","valor":"edit","descripcion":"fa-pencil-square-o","tipo_control_id":3,"glosa":"intable","is_active":true,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":13,"control_padre_id":9,"jerarquia":"10020104","nombre":"Eliminar","valor":"delete","descripcion":"fa-trash-o","tipo_control_id":3,"glosa":"intable","is_active":true,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false}],"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":14,"control_padre_id":8,"jerarquia":"100202","nombre":"Usuarios","valor":"accesos.usuarios","descripcion":"sub menu","tipo_control_id":2,"glosa":"","is_active":true,"children":[{"id":15,"control_padre_id":14,"jerarquia":"10020201","nombre":"Listar","valor":"list","descripcion":"fa-list","tipo_control_id":3,"glosa":"","is_active":true,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":16,"control_padre_id":14,"jerarquia":"10020202","nombre":"Nuevo","valor":"new","descripcion":"fa-plus","tipo_control_id":3,"glosa":"","is_active":true,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":17,"control_padre_id":14,"jerarquia":"10020203","nombre":"Editar","valor":"edit","descripcion":"fa-pencil-square-o","tipo_control_id":3,"glosa":"","is_active":true,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":18,"control_padre_id":14,"jerarquia":"10020204","nombre":"Eliminar","valor":"delete","descripcion":"fa-trash-o","tipo_control_id":3,"glosa":"","is_active":true,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":19,"control_padre_id":14,"jerarquia":"10020205","nombre":"accesos","valor":"access","descripcion":"fa-check-square-o","tipo_control_id":3,"glosa":"","is_active":true,"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false}],"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false}],"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false}],"__ivhTreeviewExpanded":true,"__ivhTreeviewIndeterminate":false},{"id":20,"control_padre_id":null,"jerarquia":"10","nombre":"Reportes","valor":"","descripcion":"Gestión de Información","tipo_control_id":1,"glosa":"","is_active":false,"children":[{"id":21,"control_padre_id":20,"jerarquia":"1001","nombre":"Maestros","valor":"maestros1","descripcion":"menu","tipo_control_id":2,"glosa":"","is_active":false,"children":[{"id":22,"control_padre_id":21,"jerarquia":"100101","nombre":"Personas","valor":"maestros.personas1","descripcion":"sub menu","tipo_control_id":2,"glosa":"","is_active":false,"children":[{"id":23,"control_padre_id":22,"jerarquia":"10010101","nombre":"Listar","valor":"list","descripcion":"fa-list","tipo_control_id":3,"glosa":"","is_active":false,"__ivhTreeviewExpanded":true},{"id":24,"control_padre_id":22,"jerarquia":"10010102","nombre":"Nuevo","valor":"new","descripcion":"fa-plus","tipo_control_id":3,"glosa":"","is_active":false,"__ivhTreeviewExpanded":true},{"id":25,"control_padre_id":22,"jerarquia":"10010103","nombre":"Editar","valor":"edit","descripcion":"fa-pencil-square-o","tipo_control_id":3,"glosa":"","is_active":false,"__ivhTreeviewExpanded":true}],"__ivhTreeviewExpanded":true}],"__ivhTreeviewExpanded":true}],"__ivhTreeviewExpanded":true}] ';

        #
        // $data = json_decode($controles) ;
        // $data = json_decode(json_encode($data), true);

        $data_save = array();
        array_walk_recursive($data, function(&$value, &$uid) use (&$data_save) {
          $data_save[$uid][] = $value;
        });
        /*return \Response::json([
                'message' => 'Operación Correcta',
                'error' => false ,
                'data' => $request->all() ,
                'data2' => $data_save ,
            ]);
*/
        for ($i=0; $i < count($data_save['id']) ; $i++)
        {
            $control_id = $data_save['id'][$i] ;
            $estado     = $data_save['isSelected'][$i] ? 1 : 0 ;
            // $estado     = 1 ;

            $exits = Acceso::where('user_id',$user_id)
                            ->where('control_id',$control_id)
                            ->first() ;
            if ($exits )
            {
                Acceso::where('user_id',$user_id)
                            ->where('control_id',$control_id)
                            ->update(['estado' => $estado]);
            }else
            {
                Acceso::create(array(
                            'user_id'    => $user_id,
                            'control_id' => $control_id,
                            'estado'     => $estado
                        ));
            }
        }

        $data = "registro correcto" ;
        return \Response::json([
                'message' => 'Operación Correcta',
                'error' => false ,
                'data' => $data ,
            ]);

    }
    public function updateUserRol(Request $request)
    {

        $rol_id  = (int)$request->input('rol_id');
        $user_id = (int)$request->input('user_id') ;

       /* $rol_id = 2;
        $user_id = 13;
*/
        $user = User::find($user_id);
        $user->rol_id = $rol_id ;
        $user->save() ;
        # desactivamos todos los controlles par el usuario
        $update_rol= Acceso::where('user_id', $user_id)
                            ->update(['estado' => 0]);


        $rol_control = RolControl::where('rol_id',$rol_id)
                        ->where('estado',1)
                        ->get();

        $rol_control = $rol_control->toArray() ;

         for ($i=0; $i < count($rol_control) ; $i++)
        {
            $control_id = $rol_control[$i]['id'] ;

            $acceso = Acceso::where('user_id',$user_id)
                            ->where('control_id',$control_id)
                            ->first() ;
            if ($acceso )
            {
                $acceso->estado = 1 ;
                $acceso->save();
            }else
            {
                Acceso::create(array(
                            'user_id'    => $user_id,
                            'control_id' => $control_id,
                            'estado'     => 1
                        ));
            }
        }

        $data = "registro correcto" ;
        return \Response::json([
                'message' => 'Operación Correcta',
                'error' => false ,
                'data' => $data ,
            ]);


    }

}
