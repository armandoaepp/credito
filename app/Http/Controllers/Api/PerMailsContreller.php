<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\PerMail ;

class PerMailsContreller extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    protected function getMailsByPersonaId($persona_id)
    {
        $data = PerMail::where('persona_id', $persona_id)
                            ->orderBy('item','asc')
                            ->get() ;

        return $data ;
    }

    public function getMailsAllByPersonaId(Request $request)
    {
		$persona_id = $request->input('persona_id') ;

        $data = $this->getMailsByPersonaId($persona_id) ;

    	return response()->json([
    		'error'=> false ,
    		'data' => $data ,
    		]);

    }

    public function delete(Request $request)
    {
        $mail_id = $request->input('mail_id') ;

        $mail = PerMail::find($mail_id);
        $persona_id = $mail->persona_id ;
        $mail->delete();

        # recuperamos la data despues de eliminar
        $data = $this->getMailsByPersonaId($persona_id) ;

        return response()->json([
            'error'=> false ,
            'data' => $data ,
            ]);
    }

    public function update(Request $request)
    {
		$mail_id = $request->input('mail_id') ;
		$correo  = $request->input('mail') ;

        $mail = PerMail::find($mail_id);
        $mail->mail = $correo  ;
        $mail->save();

        $persona_id = $mail->persona_id ;

        # recuperamos la data despues de eliminar
        $data = $this->getMailsByPersonaId($persona_id) ;

        return response()->json([
            'error'=> false ,
            'data' => $data ,
            ]);
    }

    public function saveMultiples(Request $request)
    {
		$persona_id = $request->input('persona_id') ;
		$mails      = $request->input('mails') ;

        $item_max = \DB::table('per_mail')
                    ->where('persona_id',$persona_id)
                    ->max('item');

        $item_max = (empty($item_max)) ? 0 : $item_max ;

        if(count($mails) > 0 )
        {
            $fills =  array() ;
            for ($i=0; $i < count($mails); $i++)
            {
                $fill = array(
								'persona_id'   => $persona_id ,
								'mail'         => $mails[$i]['mail'],
								'item'         => $item_max + ($i+1)
                          ) ;

                array_push($fills,$fill) ;

            }
            PerMail::insert($fills) ;
        }

        # recuperamos la data despues de eliminar
        $data = $this->getMailsByPersonaId($persona_id) ;

        return response()->json([
            'error'=> false ,
            'data' => $data ,
            ]);
    }

    public function reOrderItems(Request $request)
    {
        $mails = $request->input('mails');
        $persona_id = $request->input('persona_id') ;

        if(count($mails) > 0 )
        {
            for ($i=0; $i < count($mails); $i++)
            {
                $new_item = ($i+1) ;
                PerMail::where('id', $mails[$i]['id'])
                            ->update(['item' => $new_item]);
            }
        }

        $data = PerMail::where('persona_id',$persona_id)
                            ->orderBy('item','ASC')
                            ->get() ;

        return response()->json([
            'message' => 'Reordenacion Correcta',
            'error'=> false ,
            'data' => $data ,
            ]);
    }

}
