<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Ubigeo ;

class UbigeosController extends Controller
{
	# por defecto devolvemos los distritos
	public function getUbigeos(Request $request)
	{
		$pais_id = 1;
		$data = Ubigeo::where('tipo_ubigeo_id',3)
				->where('pais_id',1)
				->where('estado',1)
				->orderBy('descripcion','asc')
				->get();

		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}

	public function getUbigeosByTipoUbige(Request $request)
	{
		$pais_id = 1;
		$tipo_ubigeo_id = $request->tipo_ubigeo_id;
		$data = Ubigeo::where('tipo_ubigeo_id',$tipo_ubigeo_id)
				->where('pais_id',1)
				->where('estado',1)
				->get();

		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}


}
