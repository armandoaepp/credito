<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Aval ;
use App\Models\PerRelacion ;

class AvalesController extends Controller
{
	public function getAvales(Request $request)
	{
		$data = $this->getAvalesTipo();

		if (\Request::ajax())
		{
    		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
		}else
		{
			// dd($data);
			return $data ;
		}
	}

	public function getAvalesTipo()
	{
		$user = \Auth::user();

    	$rol_id_user = $user->rol_id ;
    	$persona_id_padre = $user->persona_id_padre ;


    	if ($rol_id_user > 1)
    	{
    		$data = Aval::where('aval.estado',1)
    					->where('aval.persona_id_padre',$persona_id_padre )
    					->join('persona', function($join) {
						      $join->on('persona.id','=', 'aval.persona_id')
						      ->where('persona.estado','=',1);
						    })
    					->join('per_natural', function($join) {
						      $join->on('persona.id','=', 'per_natural.persona_id')
						      ->where('per_natural.estado','=',1);
						    })
    					->leftJoin('per_telefono', function($join) {
						      $join->on('persona.id','=', 'per_telefono.persona_id')
						      ->where('per_telefono.item','=',1);
						    })
    					->leftJoin('per_mail', function($join) {
						      $join->on('persona.id','=', 'per_mail.persona_id')
						      ->where('per_mail.item','=',1);
						    })
    					->select([
    						'aval.id',
    						'persona.id as persona_id',
    						'persona.per_fecha_nac',
    						'per_natural.id as per_natural_id',
    						'per_natural.dni',
    						'persona.per_nombre',
    						'persona.per_apellidos',
    						'per_natural.sexo',
    						'per_natural.estado_civil',
    						'per_telefono.id as per_telefono_id',
    						'per_telefono.telefono',
    						'per_mail.id as per_mail_id',
    						'per_mail.mail',
    					 ])


    					->orderBy('aval.id','desc')
    					->get();
    					// dd($data->toArray());
    	}else
    	{
    		# cuando ingrese como sudo
    		$data = Aval::where('aval.estado',1)
    					->join('persona', function($join) {
						      $join->on('persona.id','=', 'aval.persona_id') ;
						      // ->where('persona.estado','=',1);
						    })
    					->join('per_natural', function($join) {
						      $join->on('persona.id','=', 'per_natural.persona_id')
						      ->where('per_natural.estado','=',1);
						    })
    					->leftJoin('per_telefono', function($join) {
						      $join->on('persona.id','=', 'per_telefono.persona_id')
						      ->where('per_telefono.item','=',1);
						    })
    					->leftJoin('per_mail', function($join) {
						      $join->on('persona.id','=', 'per_mail.persona_id')
						      ->where('per_mail.item','=',1);
						    })
    					->select([
    						'aval.id',
    						'persona.id as persona_id',
    						'persona.per_fecha_nac',
    						'per_natural.id as per_natural_id',
    						'per_natural.dni',
    						'persona.per_nombre',
    						'persona.per_apellidos',
    						'per_natural.sexo',
    						'per_natural.estado_civil',
    						'per_telefono.id as per_telefono_id',
    						'per_telefono.telefono',
    						'per_mail.id as per_mail_id',
    						'per_mail.mail',

    					 ])
    					->orderBy('aval.id','desc')
    					->get();
    	}

			return $data ;

	}

	public function save(Request $request)
    {


            $data = $this->setAval($request->all()) ;

            return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }

	public function setAval(array $data)
    {
		$persona_id = $data['persona_id'] ;
		$tipo_relacion_id = $data['tipo_relacion_id'] ;

        $user = \Auth::user() ;
        $persona_id_padre = $user->persona_id_padre ;


        $error = false;
        $data = '' ;

            $aval =  Aval::where('persona_id',$persona_id)
                                ->where('persona_id_padre',$persona_id_padre)
                                ->first();
            if (!$aval)
            {
                # save data
                    $aval =  new Aval() ;
                    $aval->persona_id       =  $persona_id ;
                    $aval->persona_id_padre =  $persona_id_padre ;
                    $aval->estado           =  1 ;
                    $aval->save() ;

                    $aval_id = $aval->id ;

                    # ==== ASIGNAR RELACION PERSONA(EMPRESA) -> CLIENTE ======================
                        $per_relacion = new PerRelacion() ;
                        $per_relacion->persona_id_padre = $persona_id_padre ;
                        $per_relacion->tipo_relacion_id = $tipo_relacion_id ;
                        $per_relacion->persona_id       = $persona_id;
                        $per_relacion->referencia       = 'Aval - Empresa' ;
                        $per_relacion->estado           = 1 ;
                        $per_relacion->save();
            }
            else
            {
                $aval->estado =  1 ;
                $aval->save() ;
            }

            $data = 'Registro Correcto' ;

            return $data ;
    }

    public function updateEstado(Request $request)
    {
        $aval_id = $request->input('aval_id');
        $estado  = $request->input('estado');

        $params = array(
            'aval_id' => $aval_id,
            'estado' => $estado,
            );

        $data = $this->setUpdateEstado($params);
        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }

    public function setUpdateEstado(array $data)
    {
        $aval_id = $data['aval_id'];
        $estado  = $data['estado'];

        $aval = Aval::find($aval_id);
        $aval->estado = $estado ;
        $aval->save() ;

        return "ok" ;
    }
    public function updateEstadoByPersonaId(array $params)
    {
        $persona_id = $params['persona_id'];
        $estado     = $params['estado'];

        $aval = Aval::where('persona_id',$persona_id)->first();
        $aval->estado = $estado ;
        $aval->save() ;

        return "ok" ;
    }

    public function getAvalesInfoBasica()
    {
        $user = \Auth::user();
        $persona_id_padre = $user->persona_id_padre ;

        $data = Aval::where('aval.estado',1)
                    ->where('aval.persona_id_padre',$persona_id_padre )
                    ->join('persona', function($join) {
                          $join->on('persona.id','=', 'aval.persona_id')
                          ->where('persona.estado','=',1);
                        })
                    ->join('per_natural', function($join) {
                          $join->on('persona.id','=', 'per_natural.persona_id')
                          ->where('per_natural.estado','=',1);
                        })
                    ->select([
                        'aval.id',
                        'persona.id as persona_id',
                        'per_natural.id as per_natural_id',
                        'per_natural.dni',
                        'persona.per_nombre',
                        'persona.per_apellidos',
                        \DB::raw('CONCAT(persona.per_apellidos, " ", persona.per_nombre) AS full_name') ,
                     ])
                    ->orderBy('persona.per_apellidos','asc')
                    ->get();

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }

}
