<?php

namespace App\Http\Controllers\Registros;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\TipoPrestamo;

class TipoPrestamosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function getTipoPrestamos()
	{
		$data = TipoPrestamo::where('estado',1)
					->get() ;

		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}

	public function save(Request $request)
    {

		$nombre      = $request->input('nombre') ;
		$descripcion = $request->input('descripcion') ;

        $tipo_prestamo = TipoPrestamo::where(['nombre' => $nombre])->first();

        if (!$tipo_prestamo)
        {
        	$tipo_prestamo = new TipoPrestamo() ;
			$tipo_prestamo->nombre      = $nombre ;
			$tipo_prestamo->descripcion = $descripcion ;
        	$tipo_prestamo->save() ;
        }
        else
        {
        	$tipo_prestamo->estado = 1 ;
        	$tipo_prestamo->save() ;
        }

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => "OK",
                    ]);
    }

    public function getTipoPrestamoById(Request $request)
    {

        $tipo_prestamo_id    = $request->input('tipo_prestamo_id') ;

        $data = TipoPrestamo::where('id',$tipo_prestamo_id)
                    		->first() ;

        return response()->json(
                [
                	'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function update(Request $request)
    {

		$tipo_prestamo_id = $request->input('tipo_prestamo_id') ;
		$nombre           = $request->input('nombre') ;
		$descripcion      = $request->input('descripcion') ;

        $tipo_pago =  TipoPrestamo::find($tipo_prestamo_id) ;
        $tipo_pago->nombre      = $nombre ;
		$tipo_pago->descripcion = $descripcion ;
        $tipo_pago->save() ;

        $data =  TipoPrestamo::find($tipo_prestamo_id) ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
    }

    public function updateEstado(Request $request)
    {

		$tipo_prestamo_id = $request->input('codigo') ;
		$estado           = $request->input('estado') ;

        $tipo_pago =  TipoPrestamo::find($tipo_prestamo_id) ;
        $tipo_pago->estado = $estado ;
        $tipo_pago->save() ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $estado,
                ]
            );
    }

}
