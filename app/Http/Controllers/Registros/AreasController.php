<?php

namespace App\Http\Controllers\Registros;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Area ;

class AreasController extends Controller
{

	public function getAreas()
	{
		$data = Area::where('estado',1)
					->get() ;

		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}

	public function save(Request $request)
    {

		$nombre      = $request->input('nombre') ;
		$descripcion = $request->input('descripcion') ;

        $area = Area::where(['nombre' => $nombre])->first();

        if (!$area)
        {
        	$area = new Area() ;
			$area->nombre      = $nombre ;
			$area->descripcion = $descripcion ;
        	$area->save() ;
        }
        else
        {
        	$area->estado = 1 ;
			$area->descripcion = $descripcion ;
        	$area->save() ;
        }

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => "OK",
                    ]);
    }

    public function getAreaById(Request $request)
    {

        $area_id    = $request->input('area_id') ;

        $data = Area::where('id',$area_id)
                    ->first() ;

        return response()->json(
                [
                	'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function update(Request $request)
    {

		$area_id     = $request->input('area_id') ;
		$nombre      = $request->input('nombre') ;
		$descripcion = $request->input('descripcion') ;

        $area =  Area::find($area_id) ;
        $area->nombre      = $nombre ;
		$area->descripcion = $descripcion ;
        $area->save() ;

        $data =  Area::find($area_id) ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
    }

    public function updateEstado(Request $request)
    {

        $area_id    = $request->input('codigo') ;
        $estado    = $request->input('estado') ;

        $area =  Area::find($area_id) ;
        $area->estado = $estado ;
        $area->save() ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $estado,
                ]
            );
    }


}
