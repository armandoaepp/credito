<?php

namespace App\Http\Controllers\Registros;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\TipoCambio;

class TipoCambiosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function getTipoCambios()
	{
		$data = TipoCambio::where('estado',1)
					->orderBy('dia','desc')
					->get() ;

		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}

	public function save(Request $request)
    {

		$valor  = $request->input('valor') ;
		$compra = $request->input('compra') ;
		$venta  = $request->input('venta') ;

		/*$valor  = '3.65' ;
		$compra = 0 ;
		$venta  = 0 ;*/

		$dia    = !empty($request->input('dia') ) ? $request->input('dia') : date('Y-m-d') ;

        $tipo_cambio = TipoCambio::where(['dia' => $dia])
        						->where(['estado'=>1])
        						->first();
		$error =  false ;
		$msj   = "" ;
        if (!$tipo_cambio)
        {
        	$tipo_cambio = new TipoCambio() ;
			$tipo_cambio->valor  = $valor ;
			$tipo_cambio->compra = $compra ;
			$tipo_cambio->venta  = $venta ;
			$tipo_cambio->dia    = $dia ;
        	$tipo_cambio->save() ;

        	$msj =  'Operación Correcta' ;
        }else
        {
			$error =  true ;
			$msj =  'Ya existe un registro' ;
        }

        return \Response::json([
                        'message' => $msj,
                        'error'   => $error,
                        'data'    => [],
                    ]);
    }

    public function getTipoCambioById(Request $request)
    {

        $tipo_cambio_id    = $request->input('tipo_cambio_id') ;

        $data = TipoCambio::where('id',$tipo_cambio_id)
                    		->first() ;

        return response()->json(
                [
                	'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function update(Request $request)
    {

		$tipo_cambio_id = $request->input('tipo_cambio_id') ;
		$valor          = $request->input('valor') ;
		$compra         = $request->input('compra') ;
		$venta          = $request->input('venta') ;
		$dia            = $request->input('dia') ;

		$tipo_cambio = new TipoCambio() ;
		$tipo_cambio->valor  = $valor ;
		$tipo_cambio->compra = $compra ;
		$tipo_cambio->venta  = $venta ;
		$tipo_cambio->dia    = $dia ;
		$tipo_cambio->save() ;

        $data =  TipoCambio::find($tipo_cambio_id) ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
    }

    public function updateEstado(Request $request)
    {

		$tipo_cambio_id = $request->input('codigo') ;
		$estado           = $request->input('estado') ;

        $tipo_cambio =  TipoCambio::find($tipo_cambio_id) ;
        $tipo_cambio->estado = $estado ;
        $tipo_cambio->save() ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $estado,
                ]
            );
    }
}
