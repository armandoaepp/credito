<?php

namespace App\Http\Controllers\Registros;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Operaciones\AcuerdoPago;
class AcuerdoPagosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function getAcuerdoPagos()
	{
		$data = AcuerdoPago::where('estado',1)
					->with('tipoPeriodo')
                    ->get() ;

		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}

    public function getAcuerdoPagosByTipoPeriodoId(Request $request)
    {
        $tipo_periodo_id = $request->input('tipo_periodo_id') ;
        // $tipo_periodo_id = 4 ;

        $data = AcuerdoPago::where('estado',1)
                    ->where('tipo_periodo_id', $tipo_periodo_id)
                    ->get() ;

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }

	public function save(Request $request)
    {

        $nombre          = $request->input('nombre') ;
        $descripcion     = $request->input('descripcion') ;
        $rango           = $request->input('rango') ;
        $partes          = $request->input('partes') ;
        $tipo_periodo_id = $request->input('tipo_periodo_id') ;

        $acuerdo_pago = AcuerdoPago::where(['nombre' => $nombre])
                            ->where(['tipo_periodo_id' => $tipo_periodo_id])
                            ->first();

        if (!$acuerdo_pago)
        {
        	$acuerdo_pago = new AcuerdoPago() ;
            $acuerdo_pago->nombre          = $nombre ;
            $acuerdo_pago->descripcion     = $descripcion ;
            $acuerdo_pago->tipo_periodo_id = $tipo_periodo_id ;
            $acuerdo_pago->rango           = $rango ;
            $acuerdo_pago->partes          = $partes ;
        	$acuerdo_pago->save() ;
        }
        else
        {
        	$acuerdo_pago->estado = 1 ;
        	$acuerdo_pago->save() ;
        }

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => "OK",
                    ]);
    }

    public function getAcuerdoPagoById(Request $request)
    {

        $acuerdo_pago_id    = $request->input('acuerdo_pago_id') ;

        $data = AcuerdoPago::where('id',$acuerdo_pago_id)
                    		->first() ;

        return response()->json(
                [
                	'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function update(Request $request)
    {

        $acuerdo_pago_id = $request->input('acuerdo_pago_id') ;
        $nombre          = $request->input('nombre') ;
        $descripcion     = $request->input('descripcion') ;
        $tipo_periodo_id = $request->input('tipo_periodo_id') ;
        $rango           = $request->input('rango') ;
        $partes          = $request->input('partes') ;

   /*     $descripcion     = "Cuota Mensual programada para un sólo pago por mes, manejado Bajo Autorización." ;
        $nombre          =         "MENSUAL" ;
        $partes          =         0 ;
        $rango           =         0 ;
        $acuerdo_pago_id    =         1 ;
        $tipo_periodo_id =         3 ;*/


        $acuerdo_pago =  AcuerdoPago::find($acuerdo_pago_id) ;
        // dd($acuerdo_pago);
        $acuerdo_pago->nombre          = $nombre ;
        $acuerdo_pago->descripcion     = $descripcion ;
        $acuerdo_pago->rango           = $rango ;
        $acuerdo_pago->partes          = $partes ;
        $acuerdo_pago->tipo_periodo_id = $tipo_periodo_id ;
        $acuerdo_pago->save() ;

        $data =  AcuerdoPago::find($acuerdo_pago_id) ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
    }

    public function updateEstado(Request $request)
    {

        $acuerdo_pago_id = $request->input('codigo') ;
        $estado          = $request->input('estado') ;

        $acuerdo_pago =  AcuerdoPago::find($acuerdo_pago_id) ;
        $acuerdo_pago->estado = $estado ;
        $acuerdo_pago->save() ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $estado,
                ]
            );
    }
}
