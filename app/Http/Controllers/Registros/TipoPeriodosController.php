<?php

namespace App\Http\Controllers\Registros;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\TipoPeriodo ;

class TipoPeriodosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function getTipoPeriodos()
	{
		$data = TipoPeriodo::where('estado',1)
					->get() ;

		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}

	public function save(Request $request)
    {

		$nombre      = $request->input('nombre') ;
        $rango = $request->input('rango') ;
		$descripcion = $request->input('descripcion') ;

        $tipo_periodo = TipoPeriodo::where(['nombre' => $nombre])->first();

        if (!$tipo_periodo)
        {
        	$tipo_periodo = new TipoPeriodo() ;
            $tipo_periodo->nombre      = $nombre ;
            $tipo_periodo->rango       = $rango ;
            $tipo_periodo->descripcion = $descripcion ;
        	$tipo_periodo->save() ;
        }
        else
        {
        	$tipo_periodo->estado = 1 ;
        	$tipo_periodo->save() ;
        }

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => "OK",
                    ]);
    }

    public function getTipoPeriodoById(Request $request)
    {

        $tipo_periodo_id    = $request->input('tipo_periodo_id') ;

        $data = TipoPeriodo::where('id',$tipo_periodo_id)
                    		->first() ;

        return response()->json(
                [
                	'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function update(Request $request)
    {

        $tipo_periodo_id = $request->input('tipo_periodo_id') ;
        $nombre          = $request->input('nombre') ;
        $rango           = $request->input('rango') ;
        $descripcion     = $request->input('descripcion') ;

        $tipo_periodo =  TipoPeriodo::find($tipo_periodo_id) ;
        $tipo_periodo->nombre      = $nombre ;
        $tipo_periodo->rango       = $rango ;
        $tipo_periodo->descripcion = $descripcion ;
        $tipo_periodo->save() ;

        $data =  TipoPeriodo::find($tipo_periodo_id) ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
    }

    public function updateEstado(Request $request)
    {

        $tipo_periodo_id = $request->input('codigo') ;
        $estado          = $request->input('estado') ;

        $tipo_periodo = TipoPeriodo::find($tipo_periodo_id) ;
        $tipo_periodo->estado = $estado ;
        $tipo_periodo->save() ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $estado,
                ]
            );
    }

    public function getTipoPeriodoByPeriodoId( $tipo_periodo_id = 0 )
    {
        $data = TipoPeriodo::find($tipo_periodo_id);
        return  $data ;

    }
}
