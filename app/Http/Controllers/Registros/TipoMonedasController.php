<?php

namespace App\Http\Controllers\Registros;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\TipoMoneda;

class TipoMonedasController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function getTipoMonedas()
	{
		$data = TipoMoneda::where('estado',1)
					->get() ;

		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}

	public function save(Request $request)
    {

		$nombre      = $request->input('nombre') ;
		$simbolo = $request->input('simbolo') ;

        $tipo_moneda = TipoMoneda::where(['nombre' => $nombre])->first();

        if (!$tipo_moneda)
        {
        	$tipo_moneda = new TipoMoneda() ;
			$tipo_moneda->nombre      = $nombre ;
			$tipo_moneda->simbolo = $simbolo ;
        	$tipo_moneda->save() ;
        }
        else
        {
        	$tipo_moneda->estado = 1 ;
        	$tipo_moneda->save() ;
        }

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => "OK",
                    ]);
    }

    public function getTipoMonedaById(Request $request)
    {

        $tipo_moneda_id    = $request->input('tipo_moneda_id') ;

        $data = TipoMoneda::where('id',$tipo_moneda_id)
                    		->first() ;

        return response()->json(
                [
                	'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function update(Request $request)
    {

		$tipo_moneda_id = $request->input('tipo_moneda_id') ;
		$nombre         = $request->input('nombre') ;
		$simbolo    = $request->input('simbolo') ;

        $tipo_moneda =  TipoMoneda::find($tipo_moneda_id) ;
        $tipo_moneda->nombre      = $nombre ;
		$tipo_moneda->simbolo = $simbolo ;
        $tipo_moneda->save() ;

        $data =  TipoMoneda::find($tipo_moneda_id) ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
    }

    public function updateEstado(Request $request)
    {

		$tipo_moneda_id = $request->input('codigo') ;
		$estado           = $request->input('estado') ;

        $tipo_moneda =  TipoMoneda::find($tipo_moneda_id) ;
        $tipo_moneda->estado = $estado ;
        $tipo_moneda->save() ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $estado,
                ]
            );
    }
}
