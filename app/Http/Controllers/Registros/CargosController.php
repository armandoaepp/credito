<?php

namespace App\Http\Controllers\Registros;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Area ;
use App\Models\Cargo ;

class CargosController extends Controller
{

	public function getCargos()
	{
		$data = Cargo::where('estado',1)
                    ->with('area')
					->get() ;
		// dd($data->toArray());

		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}

    public function getCargosAll()
    {
        $data = Cargo::where('estado',1)
                    ->get() ;
        // dd($data->toArray());

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }

    public function getCargosByAreaId()
    {
        $area_id = $request->input('area_id');

        $data = Cargo::where('estado',1)
                    ->where('area_id',$area_id)
                    ->get() ;

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }

	public function save(Request $request)
    {

		$nombre      = $request->input('nombre') ;
		$descripcion = $request->input('descripcion') ;
		$area_id     = $request->input('area_id') ;

		/*$nombre      = 'nombres ';
		$descripcion = 'asdasd ';
		$area_id     = 1;*/

        $cargo = Cargo::where(['nombre' => $nombre, 'area_id' => $area_id])->first();

        if (!$cargo)
        {
        	$cargo = new Cargo() ;
			$cargo->nombre       = $nombre ;
			$cargo->descripcion = $descripcion ;
			$cargo->area_id     = $area_id ;
        	$cargo->save() ;
        }
        else
        {
        	$cargo->estado = 1 ;
        	$cargo->save() ;
        }

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => "OK",
                    ]);
    }

    public function getAreaById(Request $request)
    {

        $cargo_id    = $request->input('area_id') ;

        $data = Cargo::where('id',$cargo_id)
                    ->first() ;

        return response()->json(
                [
                	'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function update(Request $request)
    {

		$cargo_id    = $request->input('cargo_id') ;
		$nombre      = $request->input('nombre') ;
		$descripcion = $request->input('descripcion') ;
		$area_id     = $request->input('area_id') ;

        $cargo =  Cargo::find($cargo_id) ;
		$cargo->nombre      = $nombre ;
		$cargo->descripcion = $descripcion ;
		$cargo->area_id     = $area_id ;
        $cargo->save() ;

        $data =  Cargo::find($cargo_id) ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
    }

    public function updateEstado(Request $request)
    {

        $cargo_id    = $request->input('codigo') ;
        $estado    = $request->input('estado') ;

        $cargo =  Cargo::find($cargo_id) ;
        $cargo->estado = $estado ;
        $cargo->save() ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $estado,
                ]
            );
    }
}
