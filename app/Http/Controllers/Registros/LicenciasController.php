<?php

namespace App\Http\Controllers\Registros;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

Use App\Models\Licencia ;

class LicenciasController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function getLicencias()
	{
		$data = Licencia::where('estado',1)
					->get() ;

		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}

	public function save(Request $request)
    {

		$nombre      = $request->input('nombre') ;
		$descripcion = $request->input('descripcion') ;

        $licencia = Licencia::where(['nombre' => $nombre])->first();

        if (!$licencia)
        {
        	$licencia = new Licencia() ;
			$licencia->nombre      = $nombre ;
			$licencia->descripcion = $descripcion ;
        	$licencia->save() ;
        }
        else
        {
        	$licencia->estado = 1 ;
        	$licencia->save() ;
        }

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => "OK",
                    ]);
    }

    public function getLicenciaById(Request $request)
    {

        $licencia_id    = $request->input('licencia_id') ;

        $data = Licencia::where('id',$licencia_id)
                    		->first() ;

        return response()->json(
                [
                	'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function update(Request $request)
    {

		$licencia_id = $request->input('licencia_id') ;
		$nombre      = $request->input('nombre') ;
		$descripcion = $request->input('descripcion') ;

        $licencia =  Licencia::find($licencia_id) ;
        $licencia->nombre      = $nombre ;
		$licencia->descripcion = $descripcion ;
        $licencia->save() ;

        $data =  Licencia::find($licencia_id) ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
    }

    public function updateEstado(Request $request)
    {

		$licencia_id = $request->input('codigo') ;
		$estado           = $request->input('estado') ;

        $licencia =  Licencia::find($licencia_id) ;
        $licencia->estado = $estado ;
        $licencia->save() ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $estado,
                ]
            );
    }
}
