<?php

namespace App\Http\Controllers\Registros;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\TipoGarantia ;

class TipoGarantiasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function getTipoGarantias()
	{
		$data = TipoGarantia::where('estado',1)
					->get() ;

		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}

	public function save(Request $request)
    {

		$nombre      = $request->input('nombre') ;
		$descripcion = $request->input('descripcion') ;

        $tipo_garantia = TipoGarantia::where(['nombre' => $nombre])->first();

        if (!$tipo_garantia)
        {
        	$tipo_garantia = new TipoGarantia() ;
			$tipo_garantia->nombre      = $nombre ;
			$tipo_garantia->descripcion = $descripcion ;
        	$tipo_garantia->save() ;
        }
        else
        {
        	$tipo_garantia->estado = 1 ;
        	$tipo_garantia->save() ;
        }

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => "OK",
                    ]);
    }

    public function getTipoGarantiaById(Request $request)
    {

        $tipo_garantia_id    = $request->input('tipo_garantia_id') ;

        $data = TipoGarantia::where('id',$tipo_garantia_id)
                    		->first() ;

        return response()->json(
                [
                	'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function update(Request $request)
    {

		$tipo_garantia_id = $request->input('tipo_garantia_id') ;
		$nombre           = $request->input('nombre') ;
		$descripcion      = $request->input('descripcion') ;

        $tipo_garantia =  TipoGarantia::find($tipo_garantia_id) ;
        $tipo_garantia->nombre      = $nombre ;
		$tipo_garantia->descripcion = $descripcion ;
        $tipo_garantia->save() ;

        $data =  TipoGarantia::find($tipo_garantia_id) ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
    }

    public function updateEstado(Request $request)
    {

		$tipo_garantia_id = $request->input('codigo') ;
		$estado           = $request->input('estado') ;

        $tipo_garantia =  TipoGarantia::find($tipo_garantia_id) ;
        $tipo_garantia->estado = $estado ;
        $tipo_garantia->save() ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $estado,
                ]
            );
    }
}
