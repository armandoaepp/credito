<?php

namespace App\Http\Controllers\Registros;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\TipoPago ;
class TipoPagosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function getTipoPagos()
	{
		$data = TipoPago::where('estado',1)
					->with('tipoPrestamo')
                    ->get() ;

		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}

    public function getTipoPagosByTipoPrestamoId(Request $request)
    {
        $tipo_prestamo_id = $request->input('tipo_prestamo_id') ;

        $data = TipoPago::where('estado',1)
                    ->where('tipo_prestamo_id', $tipo_prestamo_id)
                    ->get() ;

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }

	public function save(Request $request)
    {

        $nombre           = $request->input('nombre') ;
        $descripcion      = $request->input('descripcion') ;
        $tipo_prestamo_id = $request->input('tipo_prestamo_id') ;

        $tipo_pago = TipoPago::where(['nombre' => $nombre])
                            ->where(['tipo_prestamo_id' => $tipo_prestamo_id])
                            ->first();

        if (!$tipo_pago)
        {
        	$tipo_pago = new TipoPago() ;
            $tipo_pago->nombre          = $nombre ;
            $tipo_pago->descripcion     = $descripcion ;
            $tipo_pago->tipo_prestamo_id = $tipo_prestamo_id ;
        	$tipo_pago->save() ;
        }
        else
        {
        	$tipo_pago->estado = 1 ;
        	$tipo_pago->save() ;
        }

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => "OK",
                    ]);
    }

    public function getTipoPagoById(Request $request)
    {

        $tipo_pago_id    = $request->input('tipo_pago_id') ;

        $data = TipoPago::where('id',$tipo_pago_id)
                    		->first() ;

        return response()->json(
                [
                	'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function update(Request $request)
    {

        $tipo_pago_id     = $request->input('tipo_pago_id') ;
        $nombre           = $request->input('nombre') ;
        $descripcion      = $request->input('descripcion') ;
        $tipo_prestamo_id = $request->input('tipo_prestamo_id') ;

        $tipo_pago =  TipoPago::find($tipo_pago_id) ;
        $tipo_pago->nombre           = $nombre ;
        $tipo_pago->descripcion      = $descripcion ;
        $tipo_pago->tipo_prestamo_id = $tipo_prestamo_id ;
        $tipo_pago->save() ;

        $data =  TipoPago::find($tipo_pago_id) ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
    }

    public function updateEstado(Request $request)
    {

        $tipo_pago_id = $request->input('codigo') ;
        $estado       = $request->input('estado') ;

        $tipo_pago =  TipoPago::find($tipo_pago_id) ;
        $tipo_pago->estado = $estado ;
        $tipo_pago->save() ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $estado,
                ]
            );
    }
}
