<?php

namespace App\Http\Controllers\Vehiculos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Vehiculos\TipoVehiculo ;

class TipoVehiculosController extends Controller
{
    public function getTipoVehiculos()
    {
        $data = TipoVehiculo::where('estado',1)
                    ->with('claseVehiculo')
                    ->get() ;

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }

    public function getTipoVehiculosAll()
    {
        $data = TipoVehiculo::where('estado',1)
                    ->get() ;
        // dd($data->toArray());

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }

    public function getTipoVehiculosByClaseVehiculoId(Request $request)
    {
        $clase_vehiculo_id = $request->input('clase_vehiculo_id');

        $data = TipoVehiculo::where('estado',1)
                    ->where('clase_vehiculo_id',$clase_vehiculo_id)
                    ->get() ;

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }

    public function save(Request $request)
    {

        $nombre      = $request->input('nombre') ;
        $descripcion = $request->input('descripcion') ;
        $clase_vehiculo_id    = $request->input('clase_vehiculo_id') ;

        /*$nombre      = 'nombres ';
        $descripcion = 'asdasd ';
        $clase_vehiculo_id     = 1;*/

        $tipo_vehiculo = TipoVehiculo::where(['nombre' => $nombre, 'clase_vehiculo_id' => $clase_vehiculo_id])->first();

        if (!$tipo_vehiculo)
        {
            $tipo_vehiculo = new TipoVehiculo() ;
            $tipo_vehiculo->nombre            = $nombre ;
            $tipo_vehiculo->descripcion       = $descripcion ;
            $tipo_vehiculo->clase_vehiculo_id = $clase_vehiculo_id ;
            $tipo_vehiculo->save() ;
        }
        else
        {
            $tipo_vehiculo->estado = 1 ;
            $tipo_vehiculo->save() ;
        }

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => "OK",
                    ]);
    }

    public function getTipoVehiculoById(Request $request)
    {

        $tipo_vehiculo_id    = $request->input('tipo_vehiculo_id') ;

        $data = TipoVehiculo::where('id',$tipo_vehiculo_id)
                    ->first() ;

        return response()->json(
                [
                    'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function update(Request $request)
    {

        $tipo_vehiculo_id  = $request->input('tipo_vehiculo_id') ;
        $nombre            = $request->input('nombre') ;
        $descripcion       = $request->input('descripcion') ;
        $clase_vehiculo_id = $request->input('clase_vehiculo_id') ;

        $tipo_vehiculo =  TipoVehiculo::find($tipo_vehiculo_id) ;
        $tipo_vehiculo->nombre            = $nombre ;
        $tipo_vehiculo->descripcion       = $descripcion ;
        $tipo_vehiculo->clase_vehiculo_id = $clase_vehiculo_id ;
        $tipo_vehiculo->save() ;


        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => 'ok',
                ]
            );
    }

    public function updateEstado(Request $request)
    {

        $tipo_vehiculo_id    = $request->input('codigo') ;
        $estado    = $request->input('estado') ;

        $tipo_vehiculo =  TipoVehiculo::find($tipo_vehiculo_id) ;
        $tipo_vehiculo->estado = $estado ;
        $tipo_vehiculo->save() ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $estado,
                ]
            );
    }


}
