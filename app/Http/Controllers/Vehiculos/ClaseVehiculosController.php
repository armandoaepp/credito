<?php

namespace App\Http\Controllers\Vehiculos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Vehiculos\ClaseVehiculo ;

class ClaseVehiculosController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function getClaseVehiculos()
	{
		$data = ClaseVehiculo::where('estado',1)
					->get() ;

		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}

	public function save(Request $request)
    {

		$nombre      = $request->input('nombre') ;
		$descripcion = $request->input('descripcion') ;

        $clase_vehiculo = ClaseVehiculo::where(['nombre' => $nombre])->first();

        if (!$clase_vehiculo)
        {
        	$clase_vehiculo = new ClaseVehiculo() ;
			$clase_vehiculo->nombre      = $nombre ;
			$clase_vehiculo->descripcion = $descripcion ;
        	$clase_vehiculo->save() ;
        }
        else
        {
        	$clase_vehiculo->estado = 1 ;
        	$clase_vehiculo->save() ;
        }

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => "OK",
                    ]);
    }

    public function getClaseVehiculoById(Request $request)
    {

        $clase_vehiculo_id    = $request->input('clase_vehiculo_id') ;

        $data = ClaseVehiculo::where('id',$clase_vehiculo_id)
                    		->first() ;

        return response()->json(
                [
                	'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function update(Request $request)
    {

		$clase_vehiculo_id = $request->input('clase_vehiculo_id') ;
		$nombre           = $request->input('nombre') ;
		$descripcion      = $request->input('descripcion') ;

        $tipo_pago =  ClaseVehiculo::find($clase_vehiculo_id) ;
        $tipo_pago->nombre      = $nombre ;
		$tipo_pago->descripcion = $descripcion ;
        $tipo_pago->save() ;

        $data =  ClaseVehiculo::find($clase_vehiculo_id) ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
    }

    public function updateEstado(Request $request)
    {

		$clase_vehiculo_id = $request->input('codigo') ;
		$estado           = $request->input('estado') ;

        $tipo_pago =  ClaseVehiculo::find($clase_vehiculo_id) ;
        $tipo_pago->estado = $estado ;
        $tipo_pago->save() ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $estado,
                ]
            );
    }
}
