<?php

namespace App\Http\Controllers\Vehiculos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Vehiculos\Vehiculo;

class VehiculosController extends Controller
{

	public function getVehiculos()
	{

    		$data = Vehiculo::where('vehiculo.estado',1)
    					->join('modelo', function($join) {
						      $join->on('modelo.id','=', 'vehiculo.modelo_id');
						      // ->where('modelo.estado','=',1);
						    })
    					->join('marca', function($join) {
						      $join->on('marca.id','=', 'modelo.marca_id');
						      // ->where('marca.estado','=',1);
						    })
    					->join('tipo_vehiculo', function($join) {
						      $join->on('tipo_vehiculo.id','=', 'vehiculo.tipo_vehiculo_id');
						      // ->where('tipo_vehiculo.estado','=',1);
						    })
    					->join('clase_vehiculo', function($join) {
						      $join->on('clase_vehiculo.id','=', 'tipo_vehiculo.clase_vehiculo_id');
						      // ->where('clase_vehiculo.estado','=',1);
						    })
    					->select([
    						'vehiculo.id',
    						'vehiculo.placa',
    						'vehiculo.serie',
    						'vehiculo.descripcion',
    						'vehiculo.tipo_vehiculo_id',
    						'tipo_vehiculo.nombre as tipo_vehiculo',
    						'clase_vehiculo.id as clase_vehiculo_id',
    						'clase_vehiculo.nombre as clase_vehiculo',
    						'vehiculo.modelo_id',
    						'modelo.nombre as modelo',
    						'marca.id as marca_id',
    						'marca.nombre as marca',
    					 ])
    					->orderBy('vehiculo.id','desc')
    					->get();
    					// dd($data->toArray());
    			return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}

	public function getVehiculosForSelect()
	{

    		$data = Vehiculo::where('vehiculo.estado',1)
    					->join('modelo', function($join) {
						      $join->on('modelo.id','=', 'vehiculo.modelo_id');
						      // ->where('modelo.estado','=',1);
						    })
    					->join('tipo_vehiculo', function($join) {
						      $join->on('tipo_vehiculo.id','=', 'vehiculo.tipo_vehiculo_id');
						      // ->where('tipo_vehiculo.estado','=',1);
						    })
    					->select([
    						'vehiculo.id',
    						'vehiculo.placa',
    						'vehiculo.serie',
    						'vehiculo.tipo_vehiculo_id',
    						'tipo_vehiculo.nombre as tipo_vehiculo',
    						// 'clase_vehiculo.id as clase_vehiculo_id',
    						// 'clase_vehiculo.nombre as clase_vehiculo',
    						'vehiculo.modelo_id',
    						'modelo.nombre as modelo',
    						// 'marca.id as marca_id',
    						// 'marca.nombre as marca',
    					 ])
    					->orderBy('vehiculo.placa','asc')
    					->get();
    					// dd($data->toArray());
    			return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}


	public function save(Request $request)
    {
		$placa            = $request->input('placa');
		$serie            = $request->input('serie');
		$tipo_vehiculo_id = $request->input('tipo_vehiculo_id');
		$modelo_id        = $request->input('modelo_id');
		$descripcion      = $request->input('descripcion');

    	$rules = [
    		'tipo_vehiculo_id'=> 'required|integer',
    		'modelo_id'=> 'required|integer',
    		'placa'=> 'required|min:5'
    	] ;

    	$validator = \Validator::make([
										'tipo_vehiculo_id' => $tipo_vehiculo_id,
										'modelo_id'        => $modelo_id,
										'placa'        => $placa,
						    		],$rules);

    	if ($validator->fails())
		 {
		 	$errors = (array)$validator->errors()->toArray();

		 	return \Response::json([
				'message' => 'Operación Fallida',
				'error'   => true,
				'data'    => $errors ,
			]);
        }
        else
        {
        	$msj = "" ;
			$vehiculo = Vehiculo::where('placa',$placa)->first();

			if ($vehiculo)
			{
				$msj = "Registro ya Existe" ;
			}else
			{
				$vehiculo = new Vehiculo();
				$vehiculo->serie            = $serie ;
				$vehiculo->placa            = $placa ;
				$vehiculo->tipo_vehiculo_id = $tipo_vehiculo_id ;
				$vehiculo->modelo_id        = $modelo_id ;
				$vehiculo->descripcion      = $descripcion ;
				$vehiculo->save() ;
				$msj = "Registro Correcto" ;

			}



            return \Response::json([
				'message' => 'Operación',
				'error'   => false,
				'data'    => array('mensaje'=>$msj) ,
			]);
        }
    }

    public function update(Request $request)
    {
		$vehiculo_id      = $request->input('vehiculo_id');
		$placa            = $request->input('placa');
		$serie            = $request->input('serie');
		$tipo_vehiculo_id = $request->input('tipo_vehiculo_id');
		$modelo_id        = $request->input('modelo_id');
		$descripcion      = $request->input('descripcion');

    	$rules = [
			'tipo_vehiculo_id' => 'required|integer',
			'modelo_id'        => 'required|integer',
			'placa'            => 'required|min:5'
    	] ;

    	$validator = \Validator::make([
										'tipo_vehiculo_id' => $tipo_vehiculo_id,
										'modelo_id'        => $modelo_id,
										'placa'        => $placa,
						    		],$rules);

    	if ($validator->fails())
		 {
		 	$errors = (array)$validator->errors()->toArray();

		 	return \Response::json([
				'message' => 'Operación Fallida',
				'error'   => true,
				'data'    => $errors ,
			]);
        }
        else
        {
        	$msj = "" ;
			$vehiculo = Vehiculo::find($vehiculo_id);
			$vehiculo->serie            = $serie ;
			$vehiculo->placa            = $placa ;
			$vehiculo->tipo_vehiculo_id = $tipo_vehiculo_id ;
			$vehiculo->modelo_id        = $modelo_id ;
			$vehiculo->descripcion      = $descripcion ;
			$vehiculo->save() ;
			$msj = "Registro Correcto" ;

            return \Response::json([
				'message' => 'Operación',
				'error'   => false,
				'data'    => array('mensaje'=>$msj) ,
			]);
        }
    }

    public function updateEstado(Request $request)
    {

        $vehiculo_id    = $request->input('codigo') ;
        $estado    = $request->input('estado') ;

        $vehiculo =  Vehiculo::find($vehiculo_id) ;
        $vehiculo->estado = $estado ;
        $vehiculo->save() ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $estado,
                ]
            );
    }

}
