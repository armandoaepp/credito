<?php

namespace App\Http\Controllers\Vehiculos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Vehiculos\Marca ;
use App\Models\Vehiculos\Modelo ;

class ModelosController extends Controller
{
	public function getModelos()
	{
		$data = Modelo::where('estado',1)
                    ->with('marca')
					->get() ;
		// dd($data->toArray());

		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}

    public function getModelosAll()
    {
        $data = Modelo::where('estado',1)
                    ->get() ;
        // dd($data->toArray());

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }

    public function getModelosByMarcaId(Request $request)
    {
        $marca_id = $request->input('marca_id');

        $data = Modelo::where('estado',1)
                    ->where('marca_id',$marca_id)
                    ->get() ;

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
    }


	public function save(Request $request)
    {

		$nombre      = $request->input('nombre') ;
		$descripcion = $request->input('descripcion') ;
		$marca_id    = $request->input('marca_id') ;

		/*$nombre      = 'nombres ';
		$descripcion = 'asdasd ';
		$marca_id     = 1;*/

        $modelo = Modelo::where(['nombre' => $nombre, 'marca_id' => $marca_id])->first();

        if (!$modelo)
        {
        	$modelo = new Modelo() ;
			$modelo->nombre       = $nombre ;
			$modelo->descripcion = $descripcion ;
			$modelo->marca_id     = $marca_id ;
        	$modelo->save() ;
        }
        else
        {
        	$modelo->estado = 1 ;
        	$modelo->save() ;
        }

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => "OK",
                    ]);
    }

    public function getModeloById(Request $request)
    {

        $modelo_id    = $request->input('marca_id') ;

        $data = Modelo::where('id',$modelo_id)
                    ->first() ;

        return response()->json(
                [
                	'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function update(Request $request)
    {

		$modelo_id    = $request->input('modelo_id') ;
		$nombre      = $request->input('nombre') ;
		$descripcion = $request->input('descripcion') ;
		$marca_id     = $request->input('marca_id') ;

        $modelo =  Modelo::find($modelo_id) ;
		$modelo->nombre      = $nombre ;
		$modelo->descripcion = $descripcion ;
		$modelo->marca_id     = $marca_id ;
        $modelo->save() ;

        $data =  Modelo::find($modelo_id) ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
    }

    public function updateEstado(Request $request)
    {

        $modelo_id    = $request->input('codigo') ;
        $estado    = $request->input('estado') ;

        $modelo =  Modelo::find($modelo_id) ;
        $modelo->estado = $estado ;
        $modelo->save() ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $estado,
                ]
            );
    }
}
