<?php

namespace App\Http\Controllers\Vehiculos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Vehiculos\Marca ;

class MarcasController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function getMarcas()
	{
		$data = Marca::where('estado',1)
					->get() ;

		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}

	public function save(Request $request)
    {

		$nombre      = $request->input('nombre') ;
		$descripcion = $request->input('descripcion') ;

        $marca = Marca::where(['nombre' => $nombre])->first();

        if (!$marca)
        {
        	$marca = new Marca() ;
			$marca->nombre      = $nombre ;
			$marca->descripcion = $descripcion ;
        	$marca->save() ;
        }
        else
        {
        	$marca->estado = 1 ;
        	$marca->save() ;
        }

        return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => "OK",
                    ]);
    }

    public function getMarcaById(Request $request)
    {

        $marca_id    = $request->input('marca_id') ;

        $data = Marca::where('id',$marca_id)
                    		->first() ;

        return response()->json(
                [
                	'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function update(Request $request)
    {

		$marca_id = $request->input('marca_id') ;
		$nombre           = $request->input('nombre') ;
		$descripcion      = $request->input('descripcion') ;

        $tipo_pago =  Marca::find($marca_id) ;
        $tipo_pago->nombre      = $nombre ;
		$tipo_pago->descripcion = $descripcion ;
        $tipo_pago->save() ;

        $data =  Marca::find($marca_id) ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
    }

    public function updateEstado(Request $request)
    {

		$marca_id = $request->input('codigo') ;
		$estado           = $request->input('estado') ;

        $tipo_pago =  Marca::find($marca_id) ;
        $tipo_pago->estado = $estado ;
        $tipo_pago->save() ;

        return response()->json(
                [
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $estado,
                ]
            );
    }
}
