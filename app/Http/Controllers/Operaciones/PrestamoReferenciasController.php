<?php

namespace App\Http\Controllers\Operaciones;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Operaciones\PrestamoReferencia ;


class PrestamoReferenciasController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

	public function getPretamoReferenciasByPrestamoId(Request $request)
    {

        $prestamo_id    = $request->input('prestamo_id') ;
        // $prestamo_id    = 27 ;

        $data = PrestamoReferencia::where('prestamo_id',$prestamo_id)
                                    ->where('estado',1)
                                    ->get() ;

        return response()->json([
                	'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ] );
    }

    public function getPretamoReferenciasByPrestamoIdTipo(Request $request)
    {

        $prestamo_id    = $request->input('prestamo_id') ;
        $tipo    = $request->input('tipo') ;

        $data = PrestamoReferencia::where('prestamo_id',$prestamo_id)
                        ->where('tipo',$tipo)
                        ->where('estado',1)
                        ->get() ;

        return response()->json([
                    'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ] );
    }

    public function updateEstado(Request $request)
    {

        $prestamo_referencia_id = $request->input('prestamo_referencia_id') ;
        // $prestamo_referencia_id = 1 ;
        $estado                 = empty($request->input('estado'))? 0 : $request->input('estado') ;

        $almacen =  PrestamoReferencia::find($prestamo_referencia_id) ;
        $almacen->estado = $estado ;
        $almacen->save() ;

        return response()->json([
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $estado,
                ]);
    }
}
