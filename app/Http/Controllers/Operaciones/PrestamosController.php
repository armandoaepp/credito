<?php

namespace App\Http\Controllers\Operaciones;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Operaciones\Prestamo ;
use App\Models\Operaciones\Cuota ;
use App\Models\Vehiculos\Vehiculo ;
use App\Models\TipoPago ;
use App\Models\Operaciones\AcuerdoPago ;
use App\Models\Operaciones\PrestamoEstado ;

use App\Http\Controllers\Registros\TipoPeriodosController;

class PrestamosController extends Controller
{
	public function getPrestamos(Request $request)
	{
		$data = Prestamo::where('prestamo.estado',1)
    					->join('tipo_prestamo', function($join) {
						      $join->on('tipo_prestamo.id','=', 'prestamo.tipo_prestamo_id')
						      ->where('tipo_prestamo.estado','=',1);
						    })
    					->join('cliente', function($join) {
						      $join->on('cliente.id','=', 'prestamo.cliente_id')
						      ->where('cliente.estado','=',1);
						    })
    					->join('persona', function($join) {
						      $join->on('persona.id','=', 'cliente.persona_id')
						      ->where('persona.estado','=',1);
						    })
    					->select([
    						'prestamo.*',
    						'tipo_prestamo.nombre as tipo_prestamo',
    						'persona.per_nombre',
    						'persona.per_apellidos',
    					 ])
    					->orderBy('prestamo.id','desc')
    					->get() ;

    	return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
		dd($data->toArray());

	}

	public function getPrestamosByTipoEstadoId(Request $request)
	{
		$tipo_estado_id =  empty($request->input('tipo_estado_id'))? 0 : $request->input('tipo_estado_id');
		$tipo_prestamo_id =  $request->input('tipo_prestamo_id');


		$statement = Prestamo::where('tipo_prestamo_id',$tipo_prestamo_id)
						->join('tipo_prestamo', function($join) {
						      $join->on('tipo_prestamo.id','=', 'prestamo.tipo_prestamo_id')
						      ->where('tipo_prestamo.estado','=',1);
						    })
    					->join('cliente', function($join) {
						      $join->on('cliente.id','=', 'prestamo.cliente_id')
						      ->where('cliente.estado','=',1);
						    })
    					->join('persona', function($join) {
						      $join->on('persona.id','=', 'cliente.persona_id')
						      ->where('persona.estado','=',1);
						    })
    					->join('prestamo_estado', function($join) use ($tipo_estado_id) {
						      $join->on('prestamo_estado.prestamo_id','=', 'prestamo.id')
						      // ->where('prestamo_estado.tipo_estado_id','=','prestamo.estado')
						      // ->where('prestamo_estado.tipo_estado_id','=',$tipo_estado_id)
						      ->where('prestamo_estado.estado','=',1);
						    })
    					->join('tipo_estado', function($join) {
						      $join->on('tipo_estado.id','=', 'prestamo_estado.tipo_estado_id')
						      ->where('tipo_estado.estado','=',1);
						    })
    					->select([
    						'prestamo.*',
    						'tipo_prestamo.nombre as tipo_prestamo',
    						'persona.per_nombre',
    						'persona.per_apellidos',
    						'prestamo_estado.user_id as prestamo_estado_user_id',
    						'tipo_estado.descripcion as tipo_estado',
    					 ])
    					// ->whereRaw('prestamo_estado.tipo_estado_id = ( SELECT pr.estado FROM prestamo pr WHERE pr.id = prestamo.id  )')
    					->orderBy('prestamo.id','desc') ;
    					// ->get() ;
    	$data = [];
    	if (empty($tipo_estado_id))
    	{
    		$data = $statement->whereRaw('prestamo_estado.tipo_estado_id = ( SELECT pr.estado FROM prestamo pr WHERE pr.id = prestamo.id  )')
    					->get();
    	}
    	else
    	{
    		$data = $statement->where('prestamo.estado',$tipo_estado_id)
						      ->where('prestamo_estado.tipo_estado_id','=',$tipo_estado_id)
    							->get();
    	}

    	return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);

	}

    public function save(Request $request)
    {

    	$user = \Auth::user();
    	$user_id = $user->id ;

			$valor               = $request->input('valor');
			$inicial_porcentaje  = $request->input('inicial_porcentaje');
			$inicial_monto       = $request->input('inicial_monto');
			$seguro_tr           = $request->input('seguro_tr');
			$gps                 = $request->input('gps');
			$soat                = $request->input('soat');
			$gas                 = $request->input('gas');
			$otros               = $request->input('otros');
			$vehiculo_id         = $request->input('vehiculo_id');
			$tasa_interes        = $request->input('tasa_interes');
			$num_cuotas          = $request->input('num_cuotas');
			$mora                = $request->input('mora');
			$complacencia        = $request->input('complacencia');
			$pagos_parciales     = $request->input('pagos_parciales');
			$propietario         = $request->input('propietario');
			$observacion         = $request->input('observacion');
			$tipo_prestamo_id    = $request->input('tipo_prestamo_id');
			$tipo_moneda_id      = $request->input('tipo_moneda_id');
			$tipo_periodo_id     = $request->input('tipo_periodo_id');
			$tipo_pago_id        = $request->input('tipo_pago_id'); # acuerdo de pago
			$tipo_garantia_id    = $request->input('tipo_garantia_id');
			$cliente_id          = $request->input('cliente_id');
			$aval_id             = empty($request->input('aval_id'))? null : $request->input('aval_id');
			$acuerdo_pago_id     = empty($request->input('acuerdo_pago_id'))? 0 : $request->input('acuerdo_pago_id');
			$fecha_credito       = $request->input('fecha_credito');
			$fecha_desembolso    = $request->input('fecha_desembolso');
			$fecha_prorrateo     = empty($request->input('fecha_prorrateo'))? null : $request->input('fecha_prorrateo');
			$fecha_prorrateo_esp = empty($request->input('fecha_prorrateo_esp'))? null : $request->input('fecha_prorrateo_esp');

			$check_prorrateo     = $request->input('check_prorrateo');
			$check_prorrateo_esp = $request->input('check_prorrateo_esp');


				/*$acuerdo_pago_id     = 2 ;
				$aval_id             = null ;
				$check_prorrateo     = false ;
				$check_prorrateo_esp = false ;
				$cliente_id          = 4 ;
				$complacencia        = "" ;
				$fecha_credito       = "2016-05-19" ;
				$fecha_desembolso    = "2016-05-12" ;
				$fecha_prorrateo     = null ;
				$fecha_prorrateo_esp = null ;
				$gas                 = "4" ;
				$gps                 = "54" ;
				$inicial_monto       = 53944.89 ;
				$inicial_porcentaje  = "23" ;
				$mora                = "" ;
				$num_cuotas          = "34" ;
				$observacion         = "3453454" ;
				$otros               = "23" ;
				$pagos_parciales     = "NO" ;
				$propietario         = "43" ;
				$seguro_tr           = "34" ;
				$soat                = "23" ;
				$tasa_interes        = "23" ;
				$tipo_garantia_id    = 1 ;
				$tipo_moneda_id      = 1 ;
				$tipo_pago_id        = 1 ;
				$tipo_periodo_id     = 4 ;
				$tipo_prestamo_id    = 1 ;
				$valor               = "234543" ;
				$vehiculo_id         = 1 ;*/



			$fecha_inicio_pago =  $fecha_desembolso;

			if ($check_prorrateo)
			{
				if (!empty($fecha_prorrateo)) { $fecha_inicio_pago =  $fecha_prorrateo; }

			}

			if ($check_prorrateo_esp)
			{
				if (!empty($fecha_prorrateo_esp)) { $fecha_inicio_pago =  $fecha_prorrateo_esp; }

			}


			$valor_total =  $valor +
							$seguro_tr +
							$gps +
							$soat +
							$gas +
							$otros -
							$inicial_monto ;


		$prestamo =  new Prestamo();
		$prestamo->cliente_id          = $cliente_id ;
		$prestamo->tipo_prestamo_id    = $tipo_prestamo_id ;
		$prestamo->tipo_moneda_id      = $tipo_moneda_id ;
		$prestamo->tipo_periodo_id     = $tipo_periodo_id ;
		$prestamo->tipo_garantia_id    = $tipo_garantia_id ;
		$prestamo->tipo_pago_id        = $tipo_pago_id ;
		$prestamo->vehiculo_id         = $vehiculo_id ;
		$prestamo->aval_id             = $aval_id ;
		$prestamo->user_id             = $user_id ;
		$prestamo->valor               = $valor ;
		$prestamo->inicial_porcentaje  = $inicial_porcentaje ;
		$prestamo->inicial_monto       = $inicial_monto ;
		$prestamo->tasa_interes        = $tasa_interes ;
		$prestamo->valor_total         = $valor_total ;
		$prestamo->num_cuotas          = $num_cuotas ;
		$prestamo->mora                = $mora ;
		$prestamo->complacencia        = $complacencia ;
		$prestamo->pagos_parciales     = $pagos_parciales ;
		$prestamo->propietario         = $propietario ;
		$prestamo->observacion         = $observacion ;
		$prestamo->seguro_tr           = $seguro_tr ;
		$prestamo->gps                 = $gps ;
		$prestamo->soat                = $soat ;
		$prestamo->gas                 = $gas ;
		$prestamo->otros               = $otros ;
		$prestamo->fecha_credito       = $fecha_credito ;
		$prestamo->fecha_desembolso    = $fecha_desembolso ;
		$prestamo->fecha_prorrateo     = $fecha_prorrateo ;
		$prestamo->fecha_prorrateo_esp = $fecha_prorrateo_esp ;
		$prestamo->acuerdo_pago_id     = $acuerdo_pago_id ;

		$prestamo->save();
		$prestamo_id = $prestamo->id ;

		# registrar estados
		$prestamo_estado = new PrestamoEstado();
		$prestamo_estado->prestamo_id    = $prestamo_id ;
		$prestamo_estado->tipo_estado_id = 1 ;
		$prestamo_estado->user_id        = $user_id ;
		$prestamo_estado->save();


 		//  cacular la cuota
		/*$tasa_decimal = $tasa_interes /100 ;
		$tasa = (1-pow(1/(1+$tasa_decimal),$num_cuotas))/$tasa_decimal ;
		$cuota = $valor_total / $tasa  ;*/

		# caculo de couta
			$cuota_params = array(
							'tasa_interes' => $tasa_interes,
							'num_cuotas'   => $num_cuotas,
							'valor_total'  => $valor_total,
						);

			$cuota = $this->calcularCouta($cuota_params);

		# cronograma de coutas
			$rango_dias = $this->getRangoDiasPeriodo($tipo_periodo_id) ;
			// $fecha_inicio_pago = $fecha_prorrateo ;
			// # $fecha_inicio_pago = $fecha_desembolso ;

			$cronograma_params = array(
							'prestamo_id'       => $prestamo_id ,
							'valor_total'       => $valor_total ,
							'num_cuotas'        => $num_cuotas ,
							'tasa_interes'      => $tasa_interes ,
							'couta'             => $cuota ,
							'fecha_inicio_pago' => $fecha_inicio_pago ,
							'rango_dias'        => $rango_dias ,
							'acuerdo_pago_id'   => $acuerdo_pago_id ,

						);

			$this->setCronogramaCoutas($cronograma_params) ;

		/*
		$vehiculo         =  new Vehiculo();
		$vehiculo->estado = 2 ; # vendido
		$vehiculo->save();
		*/
		$data = $valor_total." - " .$cuota . " ".$fecha_inicio_pago ;
		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);

    }

    protected function calcularCouta(array $params)
    {
		$tasa_interes = $params['tasa_interes'] ;
		$num_cuotas   = $params['num_cuotas'] ;
		$valor_total  = $params['valor_total'] ;

    	$tasa_decimal = $tasa_interes / 100 ;
		$tasa = (1-pow(1/(1+$tasa_decimal),$num_cuotas))/$tasa_decimal ;
		$cuota = $valor_total / $tasa  ;

		return $cuota ;
    }

    public function setCronogramaCoutas(array $params)
    {
		$prestamo_id     = $params['prestamo_id'] ;
		$saldo_capital   = $params['valor_total'] ;
		$num_cuotas      = $params['num_cuotas'] ;
		$tasa_interes    = $params['tasa_interes'] /100 ;
		$cuota_valor     = $params['couta'] ;
		$fecha_pago      = $params['fecha_inicio_pago'] ;
		$rango_dias      = $params['rango_dias'] ;
		$acuerdo_pago_id = $params['acuerdo_pago_id'] ;

		// para saber las particiones de pagos
		$acuerdo_pago = $this->getRangoAcuerdoPago($acuerdo_pago_id) ;
		$rango_acuerdo_pago = $acuerdo_pago->rango ;
		$partes_acuerdo_pago = empty($acuerdo_pago->partes)? 0 : $acuerdo_pago->partes ;

		$cuota_parte = empty($partes_acuerdo_pago) ? 0 : $cuota_valor /  $partes_acuerdo_pago ;
		// var_dump($acuerdo_pago );

    	for ($i=0; $i < $num_cuotas ; $i++)
    	{
    		$interes = $saldo_capital*$tasa_interes ; #Sobrecosto del período
    		$amortizacion = $cuota_valor - $interes ;

    		$fecha_pago = $this->sumaRestaDiasFecha($fecha_pago , $rango_dias );

    		# calcular las fechas de partes de pagos
				$fecha_parte_1 = null ;
				$fecha_parte_2 = null ;
				$fecha_parte_3 = null ;
				$fecha_parte_4 = null ;

	    		for ($j=$partes_acuerdo_pago; $j > 0  ; $j--)
	    		{
	    			$fecha_parte =  $this->sumaRestaDiasFecha($fecha_pago , -$rango_acuerdo_pago*($j-1) ) ;
	    			if ($j == $partes_acuerdo_pago)
	    			{
	    				$fecha_parte_1 = $fecha_parte ;
	    			}
	    			else if ($j == ($partes_acuerdo_pago - 1))
	    			{
		    			$fecha_parte_2 = $fecha_parte ;
	    			}
	    			else if ($j == ($partes_acuerdo_pago - 2))
	    			{
		    			$fecha_parte_3 = $fecha_parte ;
	    			}
	    			else if ($j == ($partes_acuerdo_pago - 3))
	    			{
		    			$fecha_parte_4 = $fecha_parte ;
	    			}

	    		}
    			/*$fecha_parte_1 = $this->sumaRestaDiasFecha($fecha_pago , $rango_acuerdo_pago*($partes_acuerdo_pago-3) ) ;
	    		$fecha_parte_2 = $this->sumaRestaDiasFecha($fecha_pago , $rango_acuerdo_pago*($partes_acuerdo_pago-2) ) ;
	    		$fecha_parte_3 = $this->sumaRestaDiasFecha($fecha_pago , $rango_acuerdo_pago*($partes_acuerdo_pago-1) ) ;
	    		$fecha_parte_4 = $fecha_pago ;
				*/

    		// $fecha_pago = $this->sumaRestaMesesFecha($fecha_pago , 1 );

			$cuota = new Cuota() ;
			$cuota->prestamo_id   = $prestamo_id;
			$cuota->numero        = ($i+1);
			$cuota->saldo_capital = $saldo_capital;
			$cuota->amortizacion  = $amortizacion;
			$cuota->interes       = $interes;
			$cuota->cuota         = $cuota_valor;
			$cuota->cuota_parte       = $cuota_parte;
			$cuota->fecha_pago    = $fecha_pago;
			$cuota->fecha_parte_1    = $fecha_parte_1;
			$cuota->fecha_parte_2    = $fecha_parte_2;
			$cuota->fecha_parte_3    = $fecha_parte_3;
			$cuota->fecha_parte_4    = $fecha_parte_4;
			// $cuota->user_id_cobro = ;
			$cuota->estado        = 1;
			$cuota->save();

    		# Capital al inicio de período(para la siguiente couta)
    		$saldo_capital = $saldo_capital - $amortizacion ;

    	}
    }

    public function getRangoAcuerdoPago($acuerdo_pago_id)
    {
    	$acuerdo_pago = AcuerdoPago::find($acuerdo_pago_id);
    	return $acuerdo_pago ;
    }

    public function getRangoDiasPeriodo($tipo_periodo_id = 0)
    {
    	$tipo_periodo_ctrl = new TipoPeriodosController() ;
    	$tipo_periodo = $tipo_periodo_ctrl->getTipoPeriodoByPeriodoId( $tipo_periodo_id ) ;

    	if (!$tipo_periodo) return 0  ;

    	$rango = $tipo_periodo->rango;
    	return $rango ;
    }

    protected function sumaRestaMesesFecha($fecha, $mes = 1)
    {
        $fechanew = date('Y-m-d', strtotime("{$fecha} $mes month"));
        return $fechanew;
    }

    protected function sumaRestaDiasFecha($fecha, $dias = 0)
    {
        $fechanew = date('Y-m-d', strtotime("{$fecha} $dias day"));
        return $fechanew;
    }

 	#==== EDIT ====================================================
    public function getPrestamoById(Request $request)
    {

        $prestamo_id    = $request->input('prestamo_id') ;

        $data = Prestamo::where('id',$prestamo_id)
                    ->first() ;

        return response()->json(
                [
                	'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ]
            );
        // dd($data) ;
    }

    public function update(Request $request)
    {
    	// dd('holas');
    	$user = \Auth::user();
    	$user_id = $user->id ;

			// $valor               = $request->input('valor');
			// $inicial_porcentaje  = $request->input('inicial_porcentaje');
			// $inicial_monto       = $request->input('inicial_monto');
			// $seguro_tr           = $request->input('seguro_tr');
			// $gps                 = $request->input('gps');
			// $soat                = $request->input('soat');
			// $gas                 = $request->input('gas');
			// $otros               = $request->input('otros');
			// $tasa_interes        = $request->input('tasa_interes');
			// $num_cuotas          = $request->input('num_cuotas');
			// $mora                = $request->input('mora');
			// $complacencia        = $request->input('complacencia');
			// $tipo_prestamo_id    = $request->input('tipo_prestamo_id');
			// $tipo_moneda_id      = $request->input('tipo_moneda_id');
			// $tipo_periodo_id     = $request->input('tipo_periodo_id');
			// $tipo_pago_id        = $request->input('tipo_pago_id'); # acuerdo de pago
			// $tipo_garantia_id    = $request->input('tipo_garantia_id');
			// $acuerdo_pago_id     = empty($request->input('acuerdo_pago_id'))? 0 : $request->input('acuerdo_pago_id');

			$prestamo_id 		 = $request->input('prestamo_id');
			$vehiculo_id         = $request->input('vehiculo_id');
			$pagos_parciales     = $request->input('pagos_parciales');
			$propietario         = $request->input('propietario');
			$observacion         = $request->input('observacion');
			$cliente_id          = $request->input('cliente_id');
			$aval_id             = empty($request->input('aval_id'))? null : $request->input('aval_id');
			$fecha_credito       = $request->input('fecha_credito');
			$fecha_desembolso    = $request->input('fecha_desembolso');
			$fecha_prorrateo     = empty($request->input('fecha_prorrateo'))? null : $request->input('fecha_prorrateo');
			$fecha_prorrateo_esp = empty($request->input('fecha_prorrateo_esp'))? null : $request->input('fecha_prorrateo_esp');

			$check_prorrateo     = $request->input('check_prorrateo');
			$check_prorrateo_esp = $request->input('check_prorrateo_esp');
			$fecha_desembolso_old = $request->input('fecha_desembolso_old');

			/*$aval_id              = 2 ;
			$check_prorrateo      = false ;
			$check_prorrateo_esp  = false ;
			$cliente_id           = 4 ;
			$fecha_credito        = "2016-05-19" ;
			$fecha_desembolso     = "2016-05-15" ;
			$fecha_desembolso_old = "2016-05-15" ;
			$fecha_prorrateo      = null ;
			$fecha_prorrateo_esp  = null ;
			$observacion          = "3453454" ;
			$pagos_parciales      = "NO" ;
			$prestamo_id          = "3" ;
			$propietario          = "43" ;
			$vehiculo_id          = 1 ;*/


			$prestamo =  Prestamo::find($prestamo_id);
			$prestamo->cliente_id          = $cliente_id ;
			$prestamo->vehiculo_id         = $vehiculo_id ;
			$prestamo->pagos_parciales     = $pagos_parciales ;
			$prestamo->propietario         = $propietario ;
			$prestamo->observacion         = $observacion ;
			$prestamo->fecha_credito       = $fecha_credito ;
			$prestamo->fecha_desembolso    = $fecha_desembolso ;
			$prestamo->fecha_prorrateo     = $fecha_prorrateo ;
			$prestamo->fecha_prorrateo_esp = $fecha_prorrateo_esp ;
			$prestamo->save();


			// $prestamo->tipo_prestamo_id    = $tipo_prestamo_id ;
			// $prestamo->tipo_moneda_id      = $tipo_moneda_id ;
			// $prestamo->tipo_periodo_id     = $tipo_periodo_id ;
			// $prestamo->tipo_garantia_id    = $tipo_garantia_id ;
			// $prestamo->tipo_pago_id        = $tipo_pago_id ;
			// $prestamo->aval_id             = $aval_id ;
			// $prestamo->user_id             = $user_id ;
			// $prestamo->valor               = $valor ;
			// $prestamo->inicial_porcentaje  = $inicial_porcentaje ;
			// $prestamo->inicial_monto       = $inicial_monto ;
			// $prestamo->tasa_interes        = $tasa_interes ;
			// $prestamo->valor_total         = $valor_total ;
			// $prestamo->num_cuotas          = $num_cuotas ;
			// $prestamo->mora                = $mora ;
			// $prestamo->complacencia        = $complacencia ;
			// $prestamo->seguro_tr           = $seguro_tr ;
			// $prestamo->gps                 = $gps ;
			// $prestamo->soat                = $soat ;
			// $prestamo->gas                 = $gas ;
			// $prestamo->otros               = $otros ;
			// $prestamo->acuerdo_pago_id     = $acuerdo_pago_id ;

			$fecha_inicio_pago =  $fecha_desembolso;

			if ($check_prorrateo)
			{
				if (!empty($fecha_prorrateo)) { $fecha_inicio_pago =  $fecha_prorrateo; }
			}

			if ($check_prorrateo_esp)
			{
				if (!empty($fecha_prorrateo_esp)) { $fecha_inicio_pago =  $fecha_prorrateo_esp; }
			}


			if ($fecha_desembolso_old != $fecha_inicio_pago)
			{
				$cronograma_params = array(
								'prestamo_id'       => $prestamo_id ,
								'fecha_inicio_pago'   => $fecha_inicio_pago ,

							);

				$this->updateCronogramaCoutas($cronograma_params) ;
			}

			return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => 'ok',
                    ]);

	}

	public function updateCronogramaCoutas(array $params)
    {
		$prestamo_id     = $params['prestamo_id'] ;
		$fecha_pago      = $params['fecha_inicio_pago'] ;
		// $acuerdo_pago_id = $params['acuerdo_pago_id'] ;

		$prestamo =  Prestamo::find($prestamo_id);
		$acuerdo_pago_id = $prestamo->acuerdo_pago_id ;
		$tipo_periodo_id = $prestamo->tipo_periodo_id ;

		# cronograma de coutas
			$rango_dias = $this->getRangoDiasPeriodo($tipo_periodo_id) ;

		// para saber las particiones de pagos
		$acuerdo_pago = $this->getRangoAcuerdoPago($acuerdo_pago_id) ;
		$rango_acuerdo_pago = $acuerdo_pago->rango ;
		$partes_acuerdo_pago = empty($acuerdo_pago->partes)? 0 : $acuerdo_pago->partes ;

		$cuota_parte = empty($partes_acuerdo_pago) ? 0 : $cuota_valor /  $partes_acuerdo_pago ;
		// var_dump($acuerdo_pago );
		$cuota = Cuota::where('prestamo_id',$prestamo_id)
						->orderBy('id', 'ASC')
						->get() ;

		$cuotas = $cuota->toArray();

		$num_cuotas = count($cuotas);

    	for ($i=0; $i < $num_cuotas ; $i++)
    	{


    		$fecha_pago = $this->sumaRestaDiasFecha($fecha_pago , $rango_dias );

    		# calcular las fechas de partes de pagos
				$fecha_parte_1 = null ;
				$fecha_parte_2 = null ;
				$fecha_parte_3 = null ;
				$fecha_parte_4 = null ;

	    		for ($j=$partes_acuerdo_pago; $j > 0  ; $j--)
	    		{
	    			$fecha_parte =  $this->sumaRestaDiasFecha($fecha_pago , -$rango_acuerdo_pago*($j-1) ) ;
	    			if ($j == $partes_acuerdo_pago)
	    			{
	    				$fecha_parte_1 = $fecha_parte ;
	    			}
	    			else if ($j == ($partes_acuerdo_pago - 1))
	    			{
		    			$fecha_parte_2 = $fecha_parte ;
	    			}
	    			else if ($j == ($partes_acuerdo_pago - 2))
	    			{
		    			$fecha_parte_3 = $fecha_parte ;
	    			}
	    			else if ($j == ($partes_acuerdo_pago - 3))
	    			{
		    			$fecha_parte_4 = $fecha_parte ;
	    			}

	    		}

	    	$cuota_id = $cuotas[$i]['id'] ;
			/*echo "$cuota_id <br> ";
	    	print_r($cuotas[$i]);
			echo "<br> ";
			echo "<br> ";*/

			$cuota_upd = Cuota::find($cuota_id) ;
			$cuota_upd->fecha_pago    = $fecha_pago;
			$cuota_upd->fecha_parte_1    = $fecha_parte_1;
			$cuota_upd->fecha_parte_2    = $fecha_parte_2;
			$cuota_upd->fecha_parte_3    = $fecha_parte_3;
			$cuota_upd->fecha_parte_4    = $fecha_parte_4;
			// $cuota_upd->prestamo_id   = $prestamo_id;
			// $cuota_upd->numero        = ($i+1);
			// $cuota_upd->saldo_capital = $saldo_capital;
			// $cuota_upd->amortizacion  = $amortizacion;
			// $cuota_upd->interes       = $interes;
			// $cuota_upd->cuota         = $cuota_valor;
			// $cuota_upd->cuota_parte       = $cuota_parte;
			// $cuota_upd->user_id_cobro = ;
			// $cuota_upd->estado        = 1;
			$cuota_upd->save();

    		# Capital al inicio de período(para la siguiente couta)
    		// $saldo_capital = $saldo_capital - $amortizacion ;

    	}
    }

    public function updateEstadoPrestamoTipoEstado(Request $request)
    {
    	$user = \Auth::user();
    	$user_id = $user->id ;

		$prestamo_id    = $request->input('prestamo_id');
		$tipo_estado_id = $request->input('tipo_estado_id');
		$mora           = $request->input('mora');
		$glosa          = empty($request->input('glosa'))? '' : $request->input('glosa');


		/*$prestamo_id    = 10 ;
		$tipo_estado_id = 2;
		$mora           = 3 ;*/

		# pasamos al nuevo estado
		Prestamo::where('id', $prestamo_id)
          				->update(['estado' => $tipo_estado_id]);
        if (!empty($mora))
        {
        	$params = array('prestamo_id' => $prestamo_id, 'mora' => $mora) ;
        	$this->updateMora($params) ;
        }

		# damos de baja a los estados existen y lo pasamos al nuevo estado
    	/*PrestamoEstado::where('prestamo_id', $prestamo_id)
          				->update(['estado' => $tipo_estado_id]);*/

    	$prestamo_estado = new PrestamoEstado();
		$prestamo_estado->prestamo_id    = $prestamo_id ;
		$prestamo_estado->tipo_estado_id = $tipo_estado_id ;
		$prestamo_estado->user_id        = $user_id ;
		$prestamo_estado->glosa          = $glosa ;
		$prestamo_estado->save();

		 return response()->json(
                [
                	'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => [],
                ]
            );
    }

    public function updateMora(array $params)
    {

		$prestamo_id = $params['prestamo_id'];
		$mora        = $params['mora'];

		# pasamos al nuevo estado
		Prestamo::where('id', $prestamo_id)
          				->update(['mora' => $mora]);

        return 'ok' ;
    }

}



