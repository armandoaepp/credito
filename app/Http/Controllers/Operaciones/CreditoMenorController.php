<?php

namespace App\Http\Controllers\Operaciones;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Controllers\Registros\TipoPeriodosController;

use App\Models\Operaciones\Prestamo ;
use App\Models\Operaciones\Cuota ;
use App\Models\Vehiculos\Vehiculo ;
use App\Models\TipoPago ;
use App\Models\Operaciones\AcuerdoPago ;
use App\Models\Operaciones\PrestamoEstado ;
use App\Models\Operaciones\PrestamoReferencia ;
use App\Models\Operaciones\PrestamoGarantia ;

use App\Traits\GeneretorCodes ;


class CreditoMenorController extends Controller
{
	use GeneretorCodes ;
	public function save(Request $request)
    {

    	$user = \Auth::user();
    	$user_id = $user->id ;

			$tipo_prestamo_id = $request->input('tipo_prestamo_id');
			$tipo_pago_id     = $request->input('tipo_pago_id');
			$cliente_id       = $request->input('cliente_id');
			$tipo_moneda_id   = $request->input('tipo_moneda_id');
			$valor            = $request->input('valor');
			$tasa_interes     = $request->input('tasa_interes');
			$tipo_periodo_id  = $request->input('tipo_periodo_id');
			$acuerdo_pago_id  = $request->input('acuerdo_pago_id');
			$num_cuotas       = $request->input('num_cuotas');
			$complacencia     = $request->input('complacencia');
			$observacion      = $request->input('observacion');
			$fecha_credito    = $request->input('fecha_credito');
			$fecha_desembolso = $request->input('fecha_desembolso');
			$referencia_web    = $request->input('referencia_web');
			$data_garantias   = empty($request->input('data_garantias'))? [] : (array)$request->input('data_garantias');


			/*$acuerdo_pago_id  = 1;
			$cliente_id       = 2;
			$complacencia     = "34";
			$fecha_credito    = "2016-06-23";
			$fecha_desembolso = "2016-06-22";
			$num_cuotas       = "34";
			$observacion      = "343";
			$referencia_web    = 'sajkdfha';
			$tasa_interes     = "23";
			$tipo_moneda_id   = 1;
			$tipo_pago_id     = 5;
			$tipo_periodo_id  = 4;
			$tipo_prestamo_id = 4;
			$valor            = "323";
			$data_garantias =  '[{ "tipo_garantia": { "id": 2, "nombre": "VEHICULO MENOR", "descripcion": "Motocicletas, Trimotos, Cuatrimotos, Maquinaria Menor, etc.", "estado": 1 }, "producto": "fdsdf", "serie": "sdf", "desripcion": null, "descripcion": "sdf" }, { "tipo_garantia": { "id": 4, "nombre": "ORO", "descripcion": "Joyas", "estado": 1 }, "producto": "sdfsda", "serie": "asdsdf", "desripcion": null, "descripcion": "sdfsdf" }] ';
			$data_garantias = (array)json_decode ($data_garantias,true);*/

			// dd($data_garantias) ;

			$fecha_inicio_pago =  $fecha_desembolso;
			$valor_total =  $valor  ;

		$null = null ;
		$mora = 0 ;
		$prestamo =  new Prestamo();
		$prestamo->cliente_id          = $cliente_id ;
		$prestamo->tipo_prestamo_id    = $tipo_prestamo_id ;
		$prestamo->tipo_moneda_id      = $tipo_moneda_id ;
		$prestamo->tipo_periodo_id     = $tipo_periodo_id ;
		$prestamo->tipo_garantia_id    = $null ;
		$prestamo->tipo_pago_id        = $tipo_pago_id ;
		$prestamo->vehiculo_id         = $null ;
		$prestamo->aval_id             = $null ;
		$prestamo->user_id             = $user_id ;
		$prestamo->valor               = $valor ;
		$prestamo->inicial_porcentaje  = 0 ;
		$prestamo->inicial_monto       = 0 ;
		$prestamo->tasa_interes        = $tasa_interes ;
		$prestamo->valor_total         = $valor_total ;
		$prestamo->num_cuotas          = $num_cuotas ;
		$prestamo->mora                = $mora ;
		$prestamo->complacencia        = $complacencia ;
		$prestamo->pagos_parciales     = '' ;
		$prestamo->propietario         = '' ;
		$prestamo->observacion         = $observacion ;
		$prestamo->seguro_tr           = '' ;
		$prestamo->gps                 = '' ;
		$prestamo->soat                = '' ;
		$prestamo->gas                 = '' ;
		$prestamo->otros               = '' ;
		$prestamo->fecha_credito       = $fecha_credito ;
		$prestamo->fecha_desembolso    = $fecha_desembolso ;
		$prestamo->fecha_prorrateo     = $null ;
		$prestamo->fecha_prorrateo_esp = $null ;
		$prestamo->acuerdo_pago_id     = $acuerdo_pago_id ;

		$prestamo->save();
		$prestamo_id = $prestamo->id ;



		# registrar estados
			$prestamo_estado = new PrestamoEstado();
			$prestamo_estado->prestamo_id    = $prestamo_id ;
			$prestamo_estado->tipo_estado_id = 1 ;
			$prestamo_estado->user_id        = $user_id ;
			$prestamo_estado->save();

		# registrar Prestamo Referencia
			$prestamo_referencia = new PrestamoReferencia();
			$prestamo_referencia->prestamo_id = $prestamo_id ;
			$prestamo_referencia->tipo        = 1 ;
			$prestamo_referencia->descripcion = $referencia_web ;
			$prestamo_referencia->url         = '' ;
			$prestamo_referencia->glosa       = "web" ;
			$prestamo_referencia->estado      = 1 ;
			$prestamo_referencia->save();

		# registrar Prestamo Referencia
			for ($j=0; $j < count($data_garantias) ; $j++)
			{
				$prestamo_garantia = new PrestamoGarantia();
				$prestamo_garantia->prestamo_id      = $prestamo_id ;
				$prestamo_garantia->tipo_garantia_id = $data_garantias[$j]['tipo_garantia']['id'] ;
				$prestamo_garantia->producto         = $data_garantias[$j]['producto'] ;
				$prestamo_garantia->serie            = $data_garantias[$j]['serie'] ;
				$prestamo_garantia->descripcion      = $data_garantias[$j]['descripcion'] ;
				$prestamo_garantia->glosa            = "garantia" ;
				$prestamo_garantia->estado           = 1 ;
				$prestamo_garantia->save();
			}


		if ($tipo_pago_id === 6 )
		{
			# caculo de couta
				$cuota_params = array(
								'tasa_interes' => $tasa_interes,
								'valor_total'  => $valor_total,
							);
				$cuota = $this->calcularCoutaLibreMinima($cuota_params);
				# cronograma de coutas
				$rango_dias = $this->getRangoDiasPeriodo($tipo_periodo_id) ;

				$cronograma_params = array(
								'prestamo_id'       => $prestamo_id ,
								'valor_total'       => $valor_total ,
								'num_cuotas'        => $num_cuotas ,
								'tasa_interes'      => $tasa_interes ,
								'couta'             => $cuota ,
								'fecha_inicio_pago' => $fecha_inicio_pago ,
								'rango_dias'        => $rango_dias ,
								'acuerdo_pago_id'   => $acuerdo_pago_id ,

							);

				$this->setCronogramaCoutasLibre($cronograma_params) ;
		}
		else
		{

			# caculo de couta
				$cuota_params = array(
								'tasa_interes' => $tasa_interes,
								'num_cuotas'   => $num_cuotas,
								'valor_total'  => $valor_total,
							);

				$cuota = $this->calcularCouta($cuota_params);

			# cronograma de coutas
				$rango_dias = $this->getRangoDiasPeriodo($tipo_periodo_id) ;
				// $fecha_inicio_pago = $fecha_prorrateo ;
				// # $fecha_inicio_pago = $fecha_desembolso ;

				$cronograma_params = array(
								'prestamo_id'       => $prestamo_id ,
								'valor_total'       => $valor_total ,
								'num_cuotas'        => $num_cuotas ,
								'tasa_interes'      => $tasa_interes ,
								'couta'             => $cuota ,
								'fecha_inicio_pago' => $fecha_inicio_pago ,
								'rango_dias'        => $rango_dias ,
								'acuerdo_pago_id'   => $acuerdo_pago_id ,

							);

				$this->setCronogramaCoutas($cronograma_params) ;
		}


		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $prestamo_id,
                    ]);

    }

    # subir archivos de referencia
    public function savePrestamoReferencia(Request $request){
    	$user = \Auth::user();

		$per_id_padre = $user->persona_id_padre ;
		$prestamo_id  = $request->input('prestamo_id') ;
		$descripcion  = $request->input('descripcion') ;



        if ( !empty( $_FILES ) )
        {

            $separator = DIRECTORY_SEPARATOR ;
            $tempPath  = $_FILES['file']['tmp_name'];
            $size_file = $_FILES["file"]['size'];

            $name      = $_FILES['file']['name'] ;
            $part      = explode('.', $name) ;
            $extension = $part[count($part)-1] ;

			$name_file   = "PR".$per_id_padre."-".$this->generateCodeAlfa(3).date('YmdHis').'.'.$extension;
			$path_file   = $separator.'files'.$separator.'pref' . $separator . $name_file;
			$upload_path = public_path() .$path_file;
			$url_file    = $request->root().$path_file ;

			$estado = move_uploaded_file( $tempPath, $upload_path );

            # registrar Prestamo Referencia
				$prestamo_referencia = new PrestamoReferencia();
				$prestamo_referencia->prestamo_id = $prestamo_id ;
				$prestamo_referencia->tipo        = 2 ;
				$prestamo_referencia->descripcion = $descripcion ;
				$prestamo_referencia->url         = $url_file  ;
				$prestamo_referencia->glosa       = $path_file ;
				$prestamo_referencia->estado      = 1 ;
				$prestamo_referencia->save();

				return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $prestamo_referencia,
                    ]);

        }
        else
        {
        	return \Response::json([
                        'message' => 'Operación Fallida',
                        'error'   => false,
                        'data'    => [],
                    ]);
        }
    }

    protected function calcularCouta(array $params)
    {
		$tasa_interes = $params['tasa_interes'] ;
		$num_cuotas   = $params['num_cuotas'] ;
		$valor_total  = $params['valor_total'] ;

    	$tasa_decimal = $tasa_interes / 100 ;
		$tasa = (1-pow(1/(1+$tasa_decimal),$num_cuotas))/$tasa_decimal ;
		$cuota = $valor_total / $tasa  ;

		return $cuota ;
    }

    public function setCronogramaCoutas(array $params)
    {
		$prestamo_id     = $params['prestamo_id'] ;
		$saldo_capital   = $params['valor_total'] ;
		$num_cuotas      = $params['num_cuotas'] ;
		$tasa_interes    = $params['tasa_interes'] /100 ;
		$cuota_valor     = $params['couta'] ;
		$fecha_pago      = $params['fecha_inicio_pago'] ;
		$rango_dias      = $params['rango_dias'] ;
		$acuerdo_pago_id = $params['acuerdo_pago_id'] ;

		// para saber las particiones de pagos
		$acuerdo_pago = $this->getRangoAcuerdoPago($acuerdo_pago_id) ;
		$rango_acuerdo_pago = $acuerdo_pago->rango ;
		$partes_acuerdo_pago = empty($acuerdo_pago->partes)? 0 : $acuerdo_pago->partes ;

		$cuota_parte = empty($partes_acuerdo_pago) ? 0 : $cuota_valor /  $partes_acuerdo_pago ;
		// var_dump($acuerdo_pago );

    	for ($i=0; $i < $num_cuotas ; $i++)
    	{
    		$interes = $saldo_capital*$tasa_interes ; #Sobrecosto del período
    		$amortizacion = $cuota_valor - $interes ;

    		$fecha_pago = $this->sumaRestaDiasFecha($fecha_pago , $rango_dias );

    		# calcular las fechas de partes de pagos
				$fecha_parte_1 = null ;
				$fecha_parte_2 = null ;
				$fecha_parte_3 = null ;
				$fecha_parte_4 = null ;

	    		for ($j=$partes_acuerdo_pago; $j > 0  ; $j--)
	    		{
	    			$fecha_parte =  $this->sumaRestaDiasFecha($fecha_pago , -$rango_acuerdo_pago*($j-1) ) ;
	    			if ($j == $partes_acuerdo_pago)
	    			{
	    				$fecha_parte_1 = $fecha_parte ;
	    			}
	    			else if ($j == ($partes_acuerdo_pago - 1))
	    			{
		    			$fecha_parte_2 = $fecha_parte ;
	    			}
	    			else if ($j == ($partes_acuerdo_pago - 2))
	    			{
		    			$fecha_parte_3 = $fecha_parte ;
	    			}
	    			else if ($j == ($partes_acuerdo_pago - 3))
	    			{
		    			$fecha_parte_4 = $fecha_parte ;
	    			}

	    		}

    		// $fecha_pago = $this->sumaRestaMesesFecha($fecha_pago , 1 );

			$cuota = new Cuota() ;
			$cuota->prestamo_id      = $prestamo_id;
			$cuota->numero           = ($i+1);
			$cuota->saldo_capital    = $saldo_capital;
			$cuota->amortizacion     = $amortizacion;
			$cuota->interes          = $interes;
			$cuota->cuota            = $cuota_valor;
			$cuota->cuota_parte      = $cuota_parte;
			$cuota->fecha_pago       = $fecha_pago;
			$cuota->fecha_parte_1    = $fecha_parte_1;
			$cuota->fecha_parte_2    = $fecha_parte_2;
			$cuota->fecha_parte_3    = $fecha_parte_3;
			$cuota->fecha_parte_4    = $fecha_parte_4;
			$cuota->estado           = 1;
			$cuota->save();

    		# Capital al inicio de período(para la siguiente couta)
    		$saldo_capital = $saldo_capital - $amortizacion ;

    	}
    }

    public function getRangoAcuerdoPago($acuerdo_pago_id)
    {
    	$acuerdo_pago = AcuerdoPago::find($acuerdo_pago_id);
    	return $acuerdo_pago ;
    }

    public function getRangoDiasPeriodo($tipo_periodo_id = 0)
    {
    	$tipo_periodo_ctrl = new TipoPeriodosController() ;
    	$tipo_periodo = $tipo_periodo_ctrl->getTipoPeriodoByPeriodoId( $tipo_periodo_id ) ;

    	if (!$tipo_periodo) return 0  ;

    	$rango = $tipo_periodo->rango;
    	return $rango ;
    }

    protected function sumaRestaMesesFecha($fecha, $mes = 1)
    {
        $fechanew = date('Y-m-d', strtotime("{$fecha} $mes month"));
        return $fechanew;
    }

    protected function sumaRestaDiasFecha($fecha, $dias = 0)
    {
        $fechanew = date('Y-m-d', strtotime("{$fecha} $dias day"));
        return $fechanew;
    }

    public function update(Request $request)
    {

    	$user = \Auth::user();
    	$user_id = $user->id ;

			$prestamo_id = $request->input('prestamo_id');
			$cliente_id       = $request->input('cliente_id');
			$complacencia     = $request->input('complacencia');
			$observacion      = $request->input('observacion');
			$fecha_credito    = $request->input('fecha_credito');
			$fecha_desembolso = $request->input('fecha_desembolso');
			$referencia_web    = $request->input('referencia_web');
			$data_garantias   = empty($request->input('data_garantias'))? [] : (array)$request->input('data_garantias');

			/*
			$tipo_prestamo_id = $request->input('tipo_prestamo_id');
			$tipo_pago_id     = $request->input('tipo_pago_id');
			$tipo_moneda_id   = $request->input('tipo_moneda_id');
			$valor            = $request->input('valor');
			$tasa_interes     = $request->input('tasa_interes');
			$tipo_periodo_id  = $request->input('tipo_periodo_id');
			$acuerdo_pago_id  = $request->input('acuerdo_pago_id');
			$num_cuotas       = $request->input('num_cuotas');*/

			/*
			$prestamo_id  = 1;
			$acuerdo_pago_id  = 1;
			$cliente_id       = 2;
			$complacencia     = "34";
			$fecha_credito    = "2016-06-23";
			$fecha_desembolso = "2016-06-22";
			$num_cuotas       = "34";
			$observacion      = "343";
			$referencia_web    = 'undefined';
			$tasa_interes     = "23";
			$tipo_moneda_id   = 1;
			$tipo_pago_id     = 5;
			$tipo_periodo_id  = 4;
			$tipo_prestamo_id = 4;
			$valor            = "323";
			$data_garantias =  '[{"tipo_garantia_id":1,"producto":"wer","serie":"werwer","descripcion":"werwer"},{"tipo_garantia_id":2,"producto":"3434","serie":"34","descripcion":"3434"}]';
			$data_garantias = (array)json_decode ($data_garantias,true);
			*/
			// dd($data_garantias) ;

			$fecha_inicio_pago =  $fecha_desembolso;
			// $valor_total =  $valor  ;

		$null = null ;
		$mora = 0 ;
		$prestamo = Prestamo::find($prestamo_id);
		$prestamo->cliente_id          = $cliente_id ;
		$prestamo->complacencia        = $complacencia ;
		$prestamo->observacion         = $observacion ;
		$prestamo->fecha_credito       = $fecha_credito ;
		$prestamo->fecha_desembolso    = $fecha_desembolso ;

		/*$prestamo->tipo_prestamo_id    = $tipo_prestamo_id ;
		$prestamo->tipo_moneda_id      = $tipo_moneda_id ;
		$prestamo->tipo_periodo_id     = $tipo_periodo_id ;
		$prestamo->tipo_garantia_id    = $null ;
		$prestamo->tipo_pago_id        = $tipo_pago_id ;
		$prestamo->vehiculo_id         = $null ;
		$prestamo->aval_id             = $null ;
		$prestamo->user_id             = $user_id ;
		$prestamo->valor               = $valor ;
		$prestamo->inicial_porcentaje  = 0 ;
		$prestamo->inicial_monto       = 0 ;
		$prestamo->tasa_interes        = $tasa_interes ;
		$prestamo->valor_total         = $valor_total ;
		$prestamo->num_cuotas          = $num_cuotas ;
		$prestamo->mora                = $mora ;
		$prestamo->pagos_parciales     = '' ;
		$prestamo->propietario         = '' ;
		$prestamo->seguro_tr           = '' ;
		$prestamo->gps                 = '' ;
		$prestamo->soat                = '' ;
		$prestamo->gas                 = '' ;
		$prestamo->otros               = '' ;
		$prestamo->fecha_prorrateo     = $null ;
		$prestamo->fecha_prorrateo_esp = $null ;
		$prestamo->acuerdo_pago_id     = $acuerdo_pago_id ;*/

		$prestamo->save();



		# registrar estados
			/*$prestamo_estado = new PrestamoEstado();
			$prestamo_estado->prestamo_id    = $prestamo_id ;
			$prestamo_estado->tipo_estado_id = 1 ;
			$prestamo_estado->user_id        = $user_id ;
			$prestamo_estado->save();*/

		# registrar Prestamo Referencia
			$prestamo_referencia =  PrestamoReferencia::where('prestamo_id',$prestamo_id)
										->where('tipo',1)
										->update(['descripcion' => $referencia_web]);

		# registrar Prestamo Referencia
			for ($j=0; $j < count($data_garantias) ; $j++)
			{
				$prestamo_garantia = new PrestamoGarantia();
				$prestamo_garantia->prestamo_id      = $prestamo_id ;
				$prestamo_garantia->tipo_garantia_id = $data_garantias[$j]['tipo_garantia']['id'] ;
				$prestamo_garantia->producto         = $data_garantias[$j]['producto'] ;
				$prestamo_garantia->serie            = $data_garantias[$j]['serie'] ;
				$prestamo_garantia->descripcion      = $data_garantias[$j]['descripcion'] ;
				$prestamo_garantia->glosa            = "garantia" ;
				$prestamo_garantia->estado           = 1 ;
				$prestamo_garantia->save();
			}



 		//  cacular la cuota


		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $prestamo_id,
                    ]);
    }

    # cuota libre
    protected function calcularCoutaLibreMinima(array $params)
    {
		$tasa_interes = $params['tasa_interes'] ;
		$valor_total  = $params['valor_total'] ;
		// $num_cuotas   = $params['num_cuotas'] ;

    	$tasa_decimal = $tasa_interes / 100 ;
		$cuota = $valor_total * $tasa_decimal  ;

		return $cuota ;
    }

    public function setCronogramaCoutasLibre(array $params)
    {
		$prestamo_id     = $params['prestamo_id'] ;
		$saldo_capital   = $params['valor_total'] ;
		$num_cuotas      = $params['num_cuotas'] ;
		$tasa_interes    = $params['tasa_interes'] /100 ;
		$cuota_valor     = $params['couta'] ;
		$fecha_pago      = $params['fecha_inicio_pago'] ;
		$rango_dias      = $params['rango_dias'] ;
		$acuerdo_pago_id = $params['acuerdo_pago_id'] ;


		$cuota_parte = $cuota_valor ;
		// var_dump($acuerdo_pago );

    	for ($i=0; $i < $num_cuotas ; $i++)
    	{
    		$interes = $saldo_capital*$tasa_interes ; #Sobrecosto del período
    		$amortizacion = $cuota_valor - $interes ;

    		$fecha_pago = $this->sumaRestaDiasFecha($fecha_pago , $rango_dias );

    		# calcular las fechas de partes de pagos
				$fecha_parte_1 = null ;
				$fecha_parte_2 = null ;
				$fecha_parte_3 = null ;
				$fecha_parte_4 = null ;

	    		/*for ($j=$partes_acuerdo_pago; $j > 0  ; $j--)
	    		{
	    			$fecha_parte =  $this->sumaRestaDiasFecha($fecha_pago , -$rango_acuerdo_pago*($j-1) ) ;
	    			if ($j == $partes_acuerdo_pago)
	    			{
	    				$fecha_parte_1 = $fecha_parte ;
	    			}
	    			else if ($j == ($partes_acuerdo_pago - 1))
	    			{
		    			$fecha_parte_2 = $fecha_parte ;
	    			}
	    			else if ($j == ($partes_acuerdo_pago - 2))
	    			{
		    			$fecha_parte_3 = $fecha_parte ;
	    			}
	    			else if ($j == ($partes_acuerdo_pago - 3))
	    			{
		    			$fecha_parte_4 = $fecha_parte ;
	    			}

	    		}
				*/
	    	if ( $num_cuotas == ($i +1))
	    	{
	    		$cuota_valor = $saldo_capital + $interes ;
	    	}

			$cuota = new Cuota() ;
			$cuota->prestamo_id      = $prestamo_id;
			$cuota->numero           = ($i+1);
			$cuota->saldo_capital    = $saldo_capital;
			$cuota->amortizacion     = $amortizacion;
			$cuota->interes          = $interes;
			$cuota->cuota            = $cuota_valor;
			$cuota->cuota_parte      = $cuota_parte;
			$cuota->fecha_pago       = $fecha_pago;
			$cuota->fecha_parte_1    = $fecha_parte_1;
			$cuota->fecha_parte_2    = $fecha_parte_2;
			$cuota->fecha_parte_3    = $fecha_parte_3;
			$cuota->fecha_parte_4    = $fecha_parte_4;
			$cuota->estado           = 1;
			$cuota->save();

    		# Capital al inicio de período(para la siguiente couta)
    		$saldo_capital = $saldo_capital - $amortizacion ;

    	}
    }

}
