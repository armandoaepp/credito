<?php

namespace App\Http\Controllers\Operaciones;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Operaciones\Cuota;

class CuotasController extends Controller
{
    public function getCuotasByPrestamoId(Request $request)
    {
    	$prestamo_id = $request->input('prestamo_id');
    	// $prestamo_id = 1;

    	$data = Cuota::where('prestamo_id',$prestamo_id)
    				->get();
    	return \Response::json([
    			'message'=> 'Operación Correcta',
    			'error'=> false,
    			'data'=> $data,
    		]);
    }
}
