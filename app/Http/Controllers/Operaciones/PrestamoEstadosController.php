<?php

namespace App\Http\Controllers\Operaciones;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Operaciones\PrestamoEstado ;

class PrestamoEstadosController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

	public function getPrestamoEstados()
	{
		$data = PrestamoEstado::where('estado',1)
					->get() ;

		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}

	public function getPrestamoEstadosInfo(Request $request)
	{
		$prestamo_id = $request->input('prestamo_id');
		// $prestamo_id = 11;

		$data = PrestamoEstado::where('prestamo_estado.estado',1)
					->where('prestamo_estado.prestamo_id',$prestamo_id)
					->join('tipo_estado', function($join) {
						      $join->on('tipo_estado.id','=', 'prestamo_estado.tipo_estado_id') ;
						    })
					->join('users', function($join) {
						      $join->on('users.id','=', 'prestamo_estado.user_id') ;
						    })
					->join('persona', function($join) {
						      $join->on('persona.id','=', 'users.persona_id') ;
						    })
					->select([
    						'prestamo_estado.*',
    						'persona.per_nombre',
    						'persona.per_apellidos',
    						'tipo_estado.descripcion',
    					 ])
					->orderBy('prestamo_estado.id','desc')
					->get() ;

		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}
}
