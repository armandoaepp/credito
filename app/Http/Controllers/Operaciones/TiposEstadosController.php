<?php

namespace App\Http\Controllers\Operaciones;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Operaciones\TipoEstado ;

class TiposEstadosController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

	public function getTipoEstados()
	{
		$data = TipoEstado::where('estado',1)
					->get() ;

		return \Response::json([
                        'message' => 'Operación Correcta',
                        'error'   => false,
                        'data'    => $data,
                    ]);
	}
}
