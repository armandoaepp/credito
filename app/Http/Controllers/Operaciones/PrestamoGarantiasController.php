<?php

namespace App\Http\Controllers\Operaciones;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Operaciones\PrestamoGarantia ;


class PrestamoGarantiasController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

	public function getPretamoGarantiasByPrestamoId(Request $request)
    {

        $prestamo_id    = $request->input('prestamo_id') ;
        // $prestamo_id    = 27 ;

        $data = PrestamoGarantia::where('prestamo_id',$prestamo_id)
                                ->where('estado',1)
                                ->with('tipoGarantia')
                                ->get() ;

        return response()->json([
                	'message' => 'Operación Correcta',
                    'error' => false,
                    'data'  => $data,
                ] );
    }

    public function updateEstado(Request $request)
    {

        $prestamo_garantia_id = $request->input('prestamo_garantia_id') ;
        $estado               = empty($request->input('estado'))? 0 : $request->input('estado') ;

        $prestamo_garantia =  PrestamoGarantia::find($prestamo_garantia_id) ;
        $prestamo_garantia->estado = $estado ;
        $prestamo_garantia->save() ;

        return response()->json([
                    'message' => 'Operacion Correcta',
                    'error' => false,
                    'data'  => $estado,
                ]);
    }
}
