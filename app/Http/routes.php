<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*Route::get('plataforma', function () {
    return view('layouts.plataforma');
});*/




/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/



    Route::get('demo', function () {
        return view('layouts.login');;
    });

    Route::get('/', function () {
        return redirect(route('login')) ;
    });

Route::group(['middleware' => ['web']], function () {
	Route::auth();

	Route::get('plataforma', [
		'uses' => 'PlataformaController@index',
		'as' => 'plataforma'
	]);
	// Authentication routes...

	Route::get('login', [
			'uses' => 'Auth\AuthController@getLogin',
			'as' => 'login',
		]);

	Route::post('login', [
			// 'uses' => 'Auth\AuthController@postLogin',
			'uses' => 'Auth\LoginController@store',
			'as' => 'login'
		]);

	Route::get('logout', [
			'uses' => 'Auth\AuthController@logout',
			'as' => 'logout'
		]);

	// Password reset link request routes...
	Route::get('password/email', 'Auth\PasswordController@getEmail');
	Route::post('password/email', 'Auth\PasswordController@postEmail');

	// Password reset routes...
	Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
	Route::post('password/reset', 'Auth\PasswordController@postReset');


});


Route::group(['prefix' => 'web',  'namespace' => 'Api' , 'middleware' => ['web','auth']], function () {

    # accesos
    	Route::get('accesos', 'AccesosUsersController@index' );
        Route::get('accesos/botonera', 'AccesosUsersController@getMenuBotonera' );
        Route::post('accesos/botonera', 'AccesosUsersController@getMenuBotonera' );
        Route::get('accesos/create', 'AccesosUsersController@createUserAccesos' );
        Route::get('accesos/user/', 'AccesosUsersController@getControlAccesosUser' );
        Route::post('accesos/user', 'AccesosUsersController@getControlAccesosUser' );
        Route::post('accesos/user/update-accesos', 'AccesosUsersController@updateAccesosUser' );
        Route::get('accesos/user/update-accesos', 'AccesosUsersController@updateAccesosUser' );
        Route::post('accesos/user/update-rol', 'AccesosUsersController@updateUserRol' );


    // Control
        Route::get('controles','ControlesController@getControles');

    // user
        Route::get('users', 'UsersController@getUsers' );
        Route::get('users/info', 'UsersController@getInfo' );
        Route::post('users/save', 'UsersController@save' );
        Route::get('users/update/estado', 'UsersController@updateEstado' );
        Route::post('users/update/estado', 'UsersController@updateEstado' );

    # Roles
        Route::get('roles', 'RolesController@getRoles' );
        Route::get('roles/get/bynombre', 'RolesController@getRolByNombre' );
        Route::post('roles/get/bynombre', 'RolesController@getRolByNombre' );
        Route::post('roles/save', 'RolesController@save' );
        Route::post('roles/get/byid', 'RolesController@getRolById' );
        Route::post('roles/update', 'RolesController@update' );
        Route::post('roles/update/estado', 'RolesController@updateEstado' );
        Route::post('roles/accesos', 'RolesController@getAccesosRol' );
        Route::post('roles/update/accesos', 'RolesController@updateAccesosRol' );

    # persona
        Route::get('personas/juridicas', 'PersonasController@getPersonasJuridicas' );
        Route::get('personas/info-basica', 'PersonasController@getPersonasInfoBasica' );
    	Route::post('personas/info-basica', 'PersonasController@getPersonasInfoBasica' );

    # Personas Naturales
    	Route::get('personas/naturales', 'PerNaturalesController@getPersonasNaturales' );
        Route::post('personas/naturales/get/dni', 'PerNaturalesController@getPerNaturalByDni' );
    	// Route::get('personas/naturales/get/dni', 'PerNaturalesController@getPerNaturalByDni' );
        Route::post('personas/naturales/save', 'PerNaturalesController@save' );
        Route::post('personas/naturales/get/infoall', 'PerNaturalesController@getPerNaturalInfoAll' );
        Route::post('personas/naturales/update/dni', 'PerNaturalesController@updateDni' );
        Route::post('personas/naturales/update/info', 'PerNaturalesController@updatePerNatInfo' );
        Route::post('personas/naturales/update/estado', 'PerNaturalesController@updateEstado' );
        // Route::get('personas/naturales/update/estado', 'PerNaturalesController@updateEstado' );
        Route::get('personas/naturales/get/info-basica', 'PerNaturalesController@getPerNaturalInfoBasica' );

        Route::post('personas/naturales/by/tipo-relacion', 'PerNaturalesController@getPersonasByTipoRelacion' );
        // Route::get('personas/naturales/by/tipo-relacion', 'PerNaturalesController@getPersonasByTipoRelacion' );

    # Per Telefonos
    	Route::post('telefonos/get/persona/id', 'PerTelefonosController@getTelefonosByPersonaId' );
    	Route::post('telefonos/delete', 'PerTelefonosController@delete' );
    	Route::post('telefonos/update', 'PerTelefonosController@update' );
    	Route::post('telefonos/save', 'PerTelefonosController@saveMultiples' );
    	Route::post('telefonos/reorder-items', 'PerTelefonosController@reOrderItems' );

    # Per Mails
    	Route::post('mails/get/persona/id', 'PerMailsContreller@getMailsAllByPersonaId' );
    	Route::post('mails/delete', 'PerMailsContreller@delete' );
    	Route::post('mails/update', 'PerMailsContreller@update' );
    	Route::post('mails/save', 'PerMailsContreller@saveMultiples' );
    	Route::post('mails/reorder-items', 'PerMailsContreller@reOrderItems' );

    # Per Ducumento
    	Route::get('documentos/update', 'PerDocumentosController@update' );
    	Route::post('documentos/update', 'PerDocumentosController@update' );

     # empleados
        // Route::get('empleados/save', 'EmpleadosController@save' );
        Route::get('empleados', 'EmpleadosController@getEmpleados');
        Route::post('empleados/save', 'EmpleadosController@save' );
        Route::post('empleados/get/byid', 'EmpleadosController@getEmpleadoById' );
        Route::post('empleados/update', 'EmpleadosController@update' );
        Route::post('empleados/update/estado', 'EmpleadosController@updateEstado' );
        Route::post('empleados/asignar/new-cargo', 'EmpleadosController@asignarNewCargo' );
        Route::get('empleados/get/info-basica', 'EmpleadosController@getEmpleadosInfoBasica' );

    # clientes
        // Route::get('clientes', 'ClientesController@getClientes');
        // Route::get('clientes/save', 'ClientesController@save' );
        Route::post('clientes', 'ClientesController@getClientes');
        Route::post('clientes/save', 'ClientesController@save' );
        Route::post('clientes/get/byid', 'ClientesController@getClienteById' );
        Route::post('clientes/update', 'ClientesController@update' );
        Route::post('clientes/update/estado', 'ClientesController@updateEstado' );
        Route::post('clientes/update/agente-comercial', 'ClientesController@updateNewAgenteComercial' );
        Route::post('clientes/get/info-basica', 'ClientesController@getClientesInfoBasica' );
        // Route::get('clientes/update/agente-comercial', 'ClientesController@updateNewAgenteComercial' );

    # avales
        Route::get('avales', 'AvalesController@getAvales');
        Route::post('avales/save', 'AvalesController@save' );
        Route::post('avales/get/byid', 'AvalesController@getAvaleById' );
        Route::post('avales/update', 'AvalesController@update' );
        Route::post('avales/update/estado', 'AvalesController@updateEstado' );
        Route::post('avales/asignar/new-cargo', 'AvalesController@asignarNewCargo' );
        Route::get('avales/get/info-basica', 'AvalesController@getAvalesInfoBasica' );

    # per-juridicas
        // Route::get('empresas', 'PerJuridicasController@getPerJuridicas');
        Route::get('personas/empresas', 'PerJuridicasController@getPerJuridicas' );
        Route::post('personas/empresas/get/ruc', 'PerJuridicasController@getPerJuridicaByRuc' );
        Route::post('personas/empresas/save', 'PerJuridicasController@save' );
        Route::get('personas/empresas/save', 'PerJuridicasController@save' );
        Route::post('personas/empresas/get/infoall', 'PerJuridicasController@getPerJuridicaInfoAll' );
        Route::post('personas/empresas/update/ruc', 'PerJuridicasController@updateRuc' );
        Route::post('personas/empresas/update/info', 'PerJuridicasController@updatePerJuridicaInfo' );
        Route::get('personas/empresas/update/info', 'PerJuridicasController@updatePerJuridicaInfo' );
        Route::post('personas/empresas/update/estado', 'PerJuridicasController@updateEstado' );
        Route::get('personas/empresas/update/estado', 'PerJuridicasController@updateEstado' );
        Route::get('personas/empresas/get/info-basica', 'PerJuridicasController@getPerJuridicaInfoBasica' );

    # Per Telefonos
        Route::post('webs/get/persona/id', 'PerWebsController@getWebsByPersonaId' );
        Route::post('webs/save', 'PerWebsController@save' );
        // Route::get('webs/save', 'PerWebsController@save' );
        Route::post('webs/update', 'PerWebsController@update' );
        // Route::get('webs/update', 'PerWebsController@update' );
        Route::post('webs/delete', 'PerWebsController@delete' );
        Route::post('webs/reorder-items', 'PerWebsController@reOrderItems' );
        Route::get('webs/get/tipo-webs', 'PerWebsController@getTipoWebs' );

    # ubigeos
        Route::get('ubigeos', 'UbigeosController@getUbigeos');

});

Route::group(['prefix' => 'reg',  'namespace' => 'Registros' , 'middleware' => ['web','auth']], function () {

    # Areas
        Route::get('areas', 'AreasController@getAreas');
        Route::post('areas/save', 'AreasController@save' );
        Route::post('areas/get/byid', 'AreasController@getAreaById' );
        Route::post('areas/update', 'AreasController@update' );
        Route::post('areas/update/estado', 'AreasController@updateEstado' );

     # Cargos
        Route::get('cargos', 'CargosController@getCargos');
        Route::get('cargos/all', 'CargosController@getCargosAll');
        Route::get('cargos/byarea', 'CargosController@getCargosByAreaId');
        Route::post('cargos/save', 'CargosController@save' );
        Route::post('cargos/get/byid', 'CargosController@getCargoById' );
        Route::post('cargos/update', 'CargosController@update' );
        Route::post('cargos/update/estado', 'CargosController@updateEstado' );

    # tipo_prestamos
        Route::get('tipo-prestamos', 'TipoPrestamosController@getTipoPrestamos');
        Route::post('tipo-prestamos/save', 'TipoPrestamosController@save' );
        Route::post('tipo-prestamos/get/byid', 'TipoPrestamosController@getTipoPrestamoById' );
        Route::post('tipo-prestamos/update', 'TipoPrestamosController@update' );
        Route::post('tipo-prestamos/update/estado', 'TipoPrestamosController@updateEstado' );

    # tipo_monedas
        Route::get('tipo-monedas', 'TipoMonedasController@getTipoMonedas');
        Route::post('tipo-monedas/save', 'TipoMonedasController@save' );
        Route::post('tipo-monedas/get/byid', 'TipoMonedasController@getTipoMonedaById' );
        Route::post('tipo-monedas/update', 'TipoMonedasController@update' );
        Route::post('tipo-monedas/update/estado', 'TipoMonedasController@updateEstado' );


    # tipo-periodos
        Route::get('tipo-periodos', 'TipoPeriodosController@getTipoPeriodos');
        Route::post('tipo-periodos/save', 'TipoPeriodosController@save' );
        Route::post('tipo-periodos/get/byid', 'TipoPeriodosController@getTipoPeriodoById' );
        Route::post('tipo-periodos/update', 'TipoPeriodosController@update' );
        Route::post('tipo-periodos/update/estado', 'TipoPeriodosController@updateEstado' );

     # tipo-garantias
        Route::get('tipo-garantias', 'TipoGarantiasController@getTipoGarantias');
        Route::post('tipo-garantias/save', 'TipoGarantiasController@save' );
        Route::post('tipo-garantias/get/byid', 'TipoGarantiasController@getTipoGarantiaById' );
        Route::post('tipo-garantias/update', 'TipoGarantiasController@update' );
        Route::post('tipo-garantias/update/estado', 'TipoGarantiasController@updateEstado' );

     # tipo-pagos
        Route::get('tipo-pagos', 'TipoPagosController@getTipoPagos');
        Route::post('tipo-pagos/save', 'TipoPagosController@save' );
        Route::post('tipo-pagos/get/byid', 'TipoPagosController@getTipoPagoById' );
        Route::post('tipo-pagos/update', 'TipoPagosController@update' );
        Route::post('tipo-pagos/update/estado', 'TipoPagosController@updateEstado' );
        Route::post('tipo-pagos/get/by/tipo-prestamo-id', 'TipoPagosController@getTipoPagosByTipoPrestamoId' );

     # acuerdo-pagos
        Route::get('acuerdo-pagos', 'AcuerdoPagosController@getAcuerdoPagos');
        Route::post('acuerdo-pagos/save', 'AcuerdoPagosController@save' );
        Route::get('acuerdo-pagos/save', 'AcuerdoPagosController@save' );
        Route::post('acuerdo-pagos/get/byid', 'AcuerdoPagosController@getAcuerdoPagoById' );
        Route::post('acuerdo-pagos/update', 'AcuerdoPagosController@update' );
        Route::get('acuerdo-pagos/update', 'AcuerdoPagosController@update' );
        Route::post('acuerdo-pagos/update/estado', 'AcuerdoPagosController@updateEstado' );
        Route::post('acuerdo-pagos/get/by/tipo-periodo-id', 'AcuerdoPagosController@getAcuerdoPagosByTipoPeriodoId' );
        Route::get('acuerdo-pagos/get/by/tipo-periodo-id', 'AcuerdoPagosController@getAcuerdoPagosByTipoPeriodoId' );

     # licencias
        Route::get('licencias', 'LicenciasController@getLicencias');
        Route::post('licencias/save', 'LicenciasController@save' );
        Route::post('licencias/get/byid', 'LicenciasController@getLicenciaById' );
        Route::post('licencias/update', 'LicenciasController@update' );
        Route::post('licencias/update/estado', 'LicenciasController@updateEstado' );

    # tipo-cambios
        Route::get('tipo-cambios', 'TipoCambiosController@getTipoCambios');
        Route::get('tipo-cambios/save', 'TipoCambiosController@save' );
        Route::post('tipo-cambios/save', 'TipoCambiosController@save' );
        Route::post('tipo-cambios/get/byid', 'TipoCambiosController@getTipoCambioById' );
        Route::post('tipo-cambios/update', 'TipoCambiosController@update' );
        Route::post('tipo-cambios/update/estado', 'TipoCambiosController@updateEstado' );
});


Route::group(['prefix' => 'vehiculos',  'namespace' => 'Vehiculos' , 'middleware' => ['web','auth']], function () {

    # clase-vehiculos
        Route::get('clase-vehiculos', 'ClaseVehiculosController@getClaseVehiculos');
        Route::post('clase-vehiculos/save', 'ClaseVehiculosController@save' );
        Route::post('clase-vehiculos/get/byid', 'ClaseVehiculosController@getClaseVehiculoById' );
        Route::post('clase-vehiculos/update', 'ClaseVehiculosController@update' );
        Route::post('clase-vehiculos/update/estado', 'ClaseVehiculosController@updateEstado' );

    # tipo-vehiculos
        Route::get('tipo-vehiculos', 'TipoVehiculosController@getTipoVehiculos');
        Route::get('tipo-vehiculos/all', 'TipoVehiculosController@getTipoVehiculosAll');
        Route::post('tipo-vehiculos/by/clase-vehiculo-id', 'TipoVehiculosController@getTipoVehiculosByClaseVehiculoId');
        Route::post('tipo-vehiculos/save', 'TipoVehiculosController@save' );
        Route::post('tipo-vehiculos/get/byid', 'TipoVehiculosController@getTipoVehiculoById' );
        Route::post('tipo-vehiculos/update', 'TipoVehiculosController@update' );
        Route::post('tipo-vehiculos/update/estado', 'TipoVehiculosController@updateEstado' );

    # marcas
        Route::get('marcas', 'MarcasController@getMarcas');
        Route::post('marcas/save', 'MarcasController@save' );
        Route::post('marcas/get/byid', 'MarcasController@getMarcaById' );
        Route::post('marcas/update', 'MarcasController@update' );
        Route::post('marcas/update/estado', 'MarcasController@updateEstado' );

     # modelos
        Route::get('modelos', 'ModelosController@getModelos');
        Route::get('modelos/all', 'ModelosController@getModelosAll');
        // Route::post('modelos/by/marca-id', 'ModelosController@getModelosByMarcaId');
        Route::post('modelos/by/marca-id', 'ModelosController@getModelosByMarcaId');
        Route::post('modelos/save', 'ModelosController@save' );
        Route::post('modelos/get/byid', 'ModelosController@getModeloById' );
        Route::post('modelos/update', 'ModelosController@update' );
        Route::post('modelos/update/estado', 'ModelosController@updateEstado' );

    # vehiculos
        Route::get('vehiculos', 'VehiculosController@getVehiculos');
        Route::post('vehiculos/save', 'VehiculosController@save' );
        Route::post('vehiculos/get/byid', 'VehiculosController@getVehiculoById' );
        Route::post('vehiculos/update', 'VehiculosController@update' );
        Route::post('vehiculos/update/estado', 'VehiculosController@updateEstado' );
        Route::get('vehiculos/for-select', 'VehiculosController@getVehiculosForSelect' );
});


Route::group(['prefix' => 'operaciones',  'namespace' => 'Operaciones' , 'middleware' => ['web','auth']], function () {

    # prestamos
        Route::get('prestamos', 'PrestamosController@getPrestamos');
        Route::post('prestamos/tipo-estado', 'PrestamosController@getPrestamosByTipoEstadoId');
        Route::get('prestamos/tipo-estado', 'PrestamosController@getPrestamosByTipoEstadoId');
        Route::post('prestamos/save', 'PrestamosController@save' );
        // Route::get('prestamos/save', 'PrestamosController@save' );
        Route::post('prestamos/get/byid', 'PrestamosController@getPrestamoById' );
        Route::post('prestamos/update', 'PrestamosController@update' );
        Route::post('prestamos/update/prestamo-tipo-estado', 'PrestamosController@updatePrestamoTipoEstado' );
        Route::post('prestamos/update/estado-all', 'PrestamosController@updateEstadoPrestamoTipoEstado' );
        Route::get('prestamos/update/estado-all', 'PrestamosController@updateEstadoPrestamoTipoEstado' );

        // Route::post('prestamos/update/estado', 'PrestamosController@updateEstado' );
        Route::get('prestamos/rango', 'PrestamosController@getRangoDiasPeriodo' );


        // Route::get('cuotas/get/by-prestamo-id', 'CuotasController@getCuotasByPrestamoId' );
        Route::post('cuotas/get/by-prestamo-id', 'CuotasController@getCuotasByPrestamoId' );

        Route::get('tipo-estados', 'TiposEstadosController@getTipoEstados' );
        Route::get('prestamo-estados', 'PrestamoEstadosController@getPrestamoEstados' );
        Route::get('prestamo-estados/get/info', 'PrestamoEstadosController@getPrestamoEstadosInfo' );
        Route::post('prestamo-estados/get/info', 'PrestamoEstadosController@getPrestamoEstadosInfo' );

        // Route::get('prestamos', 'CreditoMenorController@getPrestamos');
        Route::post('credito-menor/tipo-estado', 'CreditoMenorController@getPrestamosByTipoEstadoId');
        Route::post('credito-menor/save', 'CreditoMenorController@save' );
        Route::get('credito-menor/save', 'CreditoMenorController@save' );
        Route::get('credito-menor/update', 'CreditoMenorController@update' );
        Route::post('credito-menor/update', 'CreditoMenorController@update' );
        Route::post('credito-menor/get/byid', 'CreditoMenorController@getPrestamoById' );

        Route::post('credito-menor/uploads', 'CreditoMenorController@savePrestamoReferencia' );

        Route::get('prestamo-garantias/get/byprestamoid', 'PrestamoGarantiasController@getPretamoGarantiasByPrestamoId' );
        Route::post('prestamo-garantias/get/byprestamoid', 'PrestamoGarantiasController@getPretamoGarantiasByPrestamoId' );
        Route::post('prestamo-garantias/update/estado', 'PrestamoGarantiasController@updateEstado' );
        Route::get('prestamo-garantias/update/estado', 'PrestamoGarantiasController@updateEstado' );

        Route::get('prestamo-referencias/get/byprestamoid', 'PrestamoReferenciasController@getPretamoReferenciasByPrestamoId' );
        Route::post('prestamo-referencias/get/byprestamoid', 'PrestamoReferenciasController@getPretamoReferenciasByPrestamoId' );
        Route::post('prestamo-referencias/get/byprestamoid-tipo', 'PrestamoReferenciasController@getPretamoReferenciasByPrestamoIdTipo' );
        Route::post('prestamo-referencias/update/estado', 'PrestamoReferenciasController@updateEstado' );
        Route::get('prestamo-referencias/update/estado', 'PrestamoReferenciasController@updateEstado' );



});
// sudo chown -R :www-data /var/www/html/credito/
// sudo chmod -R 775 /var/www/html/credito/storage/