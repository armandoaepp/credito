<?php
namespace App\Traits;

trait TreeMenu {

	# crear el arbol menu
    /**
     *[$elements]: puede ser un array
     *[ $parentId]: id padre
     */
    public function buildTreeMenu(array $elements, $parentId = null)
    {
        $branch = array();
        #########################################################
          // $elements = json_decode(json_encode($elements), true) ;

        for ($i=0; $i < count($elements) ; $i++)
        {
            if ($elements[$i]['control_padre_id'] == $parentId) {
                $children = $this->buildTreeMenu($elements, $elements[$i]['id']);

                if ($children) {
                    $elements[$i]['children'] = $children;
                }

                $branch[] = $elements[$i];
            }
        }

        #########################################################

      /*  foreach ($elements as $element)
        {
            if ($element->control_padre_id == $parentId) {
                $children = $this->buildTreeMenu($elements, $element->id);

                if ($children) {
                    $element->children = $children;
                }

                $branch[] = $element;
            }
        }
*/
        return $branch;
    }
}