<?php
namespace App\Traits;

trait GeneretorCodes {


    public function generateCodeCliente($value = 0)
    {
        $longitud = strlen($value);
        if ($longitud >= 8)
            return $value ;

        $longitud = 8 - $longitud ;


        $cod = $this->generateCodeAlfa($longitud) ;
        $codigo = $cod.$value ;

        return $codigo ;
    }

    public function generateCodeAlfa($longitud)
    {
         $key = '';
         $pattern = 'abcdefghijklmnopqrstuvwxyz';
         $max = strlen($pattern)-1;
         for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
         return $key;
    }
    public function generateCodeAlfaNumeric($longitud)
    {
         $key = '';
         $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
         $max = strlen($pattern)-1;
         for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
         return $key;
    }
}