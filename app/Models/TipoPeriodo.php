<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoPeriodo extends Model
{
	protected $table = 'tipo_periodo';
    public $timestamps = false;

    protected $fillable =  [
							'nombre',
							'rango',
							'descripcion',
							'estado' ,
							] ;

	public function tipoPago()
    {
        return $this->hasMany('App\Models\TipoPago') ;
    }
}
