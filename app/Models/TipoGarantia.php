<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoGarantia extends Model
{
	protected $table = 'tipo_garantia';
    public $timestamps = false;

    protected $fillable =  [
							'nombre',
							'descripcion',
							'estado' ,
							] ;
}
