<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerRelacion extends Model
{
    protected $table = 'per_relacion';

    protected $fillable = [
								'persona_id',
								'tipo_relacion_id',
								'persona_id_padre',
								'referencia',
								'estado',
							] ;

	public function persona()
    {
        return $this->belongsTo('App\Models\Persona') ;
    }

    public function tipoRelacion()
    {
        return $this->belongsTo('App\Models\TipoRelacion') ;
    }
}
