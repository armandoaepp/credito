<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoCambio extends Model
{
	protected $table = 'tipo_cambio';
    protected $fillable = [
							'valor',
							'compra',
							'venta',
							'dia',
							'estado',
						];

	public function setDia($value)
    {
    	if (empty($value))
    	{
    		 $this->attributes['dia'] =date('Y-m-d') ;
    	}else
    	{
        	$this->attributes['dia'] = \Carbon\Carbon::createFromFormat('Y-m-d',$value)->format('Y-m-d') ;
    	}
    }
}
