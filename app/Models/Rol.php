<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table = 'rol';

	public $timestamps = false;

	protected $fillable = ['nombre', 'estado'];

	public function users()
	{
		return $this->hasMany('App\User');
	}

	public function rolControl()
	{
		return $this->hasMany('App\Models\RolControl');
	}
}
