<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoPrestamo extends Model
{
	protected $table = 'tipo_prestamo';
    public $timestamps = false;

    protected $fillable =  [
							'nombre',
							'descripcion',
							'estado' ,
							] ;
}
