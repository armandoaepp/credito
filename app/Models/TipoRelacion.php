<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoRelacion extends Model
{
    protected $table = 'tipo_relacion';
    public $timestamps = false;

    protected $fillable = [
								'nombre',
								'estado',
							] ;

    public function perRelacion()
	{
		return $this->hasMany('App\Models\PerRelacion');
	}
}
