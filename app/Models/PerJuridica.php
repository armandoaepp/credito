<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerJuridica extends Model
{
    protected $table = 'per_juridica';
    public $timestamps = false;

    protected $fillable = [
							'persona_id',
							'rubro_id',
							'ruc',
							'razon_social',
                            'nombre_comercial',
							'estado',
							];

    public function persona()
    {
        return $this->belongsTo('App\Models\Persona') ;
    }

    public function rubro()
    {
        return $this->belongsTo('App\Models\Rubro') ;
    }
}
