<?php

namespace App\Models\Vehiculos;

use Illuminate\Database\Eloquent\Model;

class TipoVehiculo extends Model
{
	protected $table = 'tipo_vehiculo';
    public $timestamps = false;
	protected $fillable = [
						'clase_vehiculo_id',
						'nombre',
						'descripcion',
						'estado',
					];

	public function claseVehiculo()
	{
        return $this->belongsTo('App\Models\Vehiculos\ClaseVehiculo') ;
	}
}
