<?php

namespace App\Models\Vehiculos;

use Illuminate\Database\Eloquent\Model;

class Modelo extends Model
{
	protected $table = 'modelo';
    public $timestamps = false;
	protected $fillable = [
						'marca_id',
						'nombre',
						'descripcion',
						'estado',
					];
	public function marca()
	{
        return $this->belongsTo('App\Models\Vehiculos\Marca') ;
	}
}
