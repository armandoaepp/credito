<?php

namespace App\Models\Vehiculos;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
	protected $table = 'marca';
    public $timestamps = false;
	protected $fillable = [
						'nombre',
						'descripcion',
						'estado',
					];

	public function modelo()
	{
		return $this->hasMany('\App\Models\Vehiculos\Modelo');
	}
}
