<?php

namespace App\Models\Vehiculos;

use Illuminate\Database\Eloquent\Model;

class ClaseVehiculo extends Model
{
	protected $table = 'clase_vehiculo';
    public $timestamps = false;
	protected $fillable = [
						'nombre',
						'descripcion',
						'estado',
					];
}
