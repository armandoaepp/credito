<?php

namespace App\Models\Vehiculos;

use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{
	protected $table = 'vehiculo';
    // public $timestamps = false;
	protected $fillable = [
							'tipo_vehiculo_id',
							'modelo_id',
							'placa',
							'serie',
							'descripcion',
							'estadotipo_vehiculo_id',
							'modelo_id',
							'placa',
							'serie',
							'descripcion',
							'estado',
					];

	public function tipoVehiculo()
	{
        return $this->belongsTo('App\Models\Vehiculos\TipoVehiculo') ;
	}

	public function modelo()
	{
        return $this->belongsTo('App\Models\Vehiculos\Modelo') ;
	}




}
