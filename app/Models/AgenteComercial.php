<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgenteComercial extends Model
{

	protected $table = 'agente_comercial';
	protected $fillable = [
						'cliente_id',
						'empleado_id',
						'estado',
					];
}
