<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerLicencia extends Model
{
	protected $table = 'per_licencia';

    protected $fillable =  [
						'persona_id',
						'licencia_id',
						'numero_lic',
						'clase_categoria',
						'fecha_emision',
						'fecha_caducidad',
						'estado',
     ] ;

    public function persona()
    {
        return $this->HasOne('App\Models\Persona') ;
    }

    public function licencia()
    {
        return $this->belongsTo('App\Models\Licencia') ;
    }

}
