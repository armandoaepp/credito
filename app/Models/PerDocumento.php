<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerDocumento extends Model
{
    protected $table = 'per_documento';
    public $timestamps = false;

    protected $fillable = [
						'persona_id',
						'tipo_documento_id',
						'numero',
						'caducidad',
						'imagen',
						'estado',
					];

	protected $guarded = ['id'];


    public function persona()
    {
        return $this->belongsTo('App\Models\Persona') ;
    }

    public function tipoDocumento()
    {
        return $this->belongsTo('App\Models\TipoDocumento') ;
    }

}
