<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerNatural extends Model
{
    protected $table = 'per_natural';
    public $timestamps = false;

	protected $fillable = [
								'persona_id' ,
								'dni' ,
								'apellidos' ,
								'nombres' ,
								'sexo' ,
								'estado_civil' ,
							] ;

	public function persona()
    {
        return $this->belongsTo('App\Models\Persona') ;
    }
}
