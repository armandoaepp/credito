<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CargoEmpleado extends Model
{
	protected $table = 'cargo_empleado';
	protected $fillable = [
							'cargo_id',
							'empleado_id',
							'estado',
						];



}
