<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoPago extends Model
{
	protected $table = 'tipo_pago';
    public $timestamps = false;

    protected $fillable =  [
    						'tipo_prestamo_id',
							'nombre',
							'descripcion',
							'estado' ,
							] ;

	public function tipoPrestamo()
    {
        return $this->belongsTo('App\Models\TipoPrestamo') ;
    }
}
