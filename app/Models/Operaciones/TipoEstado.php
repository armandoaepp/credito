<?php

namespace App\Models\Operaciones;

use Illuminate\Database\Eloquent\Model;

class TipoEstado extends Model
{
	protected $table = 'tipo_estado';
    public $timestamps = false;

	public function prestamoEstado()
    {
        return $this->hasMany('App\Models\Operaciones\PrestamoEstado') ;
    }
}
