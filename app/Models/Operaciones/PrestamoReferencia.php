<?php

namespace App\Models\Operaciones;

use Illuminate\Database\Eloquent\Model;

class PrestamoReferencia extends Model
{
	protected $table = 'prestamo_referencia';
    public $timestamps = false;

	public function prestamo()
    {
        return $this->belongsTo('App\Models\Operaciones\Prestamo') ;
    }
}
