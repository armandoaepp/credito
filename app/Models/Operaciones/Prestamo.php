<?php

namespace App\Models\Operaciones;

use Illuminate\Database\Eloquent\Model;

class Prestamo extends Model
{
	protected $table = 'prestamo';
	protected $fillable = [
						'cliente_id',
						'tipo_prestamo_id',
						'tipo_moneda_id',
						'tipo_periodo_id',
						'tipo_garantia_id',
						'tipo_pago_id',
						'vehiculo_id',
						'aval_id',
						'user_id',
						'valor',
						'incial_porcentaje',
						'inicial_monto',
						'tasa_interes',
						'num_coutas',
						'mora',
						'complacencia',
						'pagos_parciales',
						'propietario',
						'observacion',
						'seguro_tr',
						'gps',
						'soat',
						'gas',
						'otras',
						'estado',
					];


}
