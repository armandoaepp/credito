<?php

namespace App\Models\Operaciones;

use Illuminate\Database\Eloquent\Model;

class Cuota extends Model
{
	protected $table = 'cuota';
	protected $fillable = [
							'prestamo_id',
							'numero',
							'saldo_capital',
							'amortizacion',
							'interes',
							'cuota',
							'cuota_parte',
							'fecha_pago',
							'fecha_parte_1',
							'fecha_parte_2',
							'fecha_parte_3',
							'fecha_parte_4',
							'user_id_cobro',
							'estado',
					];

}
