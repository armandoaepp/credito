<?php

namespace App\Models\Operaciones;

use Illuminate\Database\Eloquent\Model;

class AcuerdoPago extends Model
{

	protected $table = 'acuerdo_pago';
    public $timestamps = false;
	protected $fillable = [
							'tipo_periodo_id',
							'nombre',
							'rango',
							'partes',
							'descripcion',
							'estado',
					];

	public function tipoPeriodo()
    {
        return $this->belongsTo('App\Models\TipoPeriodo') ;
    }
}
