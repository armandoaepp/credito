<?php

namespace App\Models\Operaciones;

use Illuminate\Database\Eloquent\Model;

class PrestamoEstado extends Model
{
	protected $table = 'prestamo_estado';
    // public $timestamps = false;

	public function prestamo()
    {
        return $this->belongsTo('App\Models\Operaciones\Prestamo') ;
    }

    public function tipoEstado()
    {
        return $this->belongsTo('App\Models\Operaciones\TipoEstado') ;
    }
}
