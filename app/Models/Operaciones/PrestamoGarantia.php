<?php

namespace App\Models\Operaciones;

use Illuminate\Database\Eloquent\Model;

class PrestamoGarantia extends Model
{
	protected $table = 'prestamo_garantia';
    public $timestamps = false;

	public function prestamo()
    {
        return $this->belongsTo('App\Models\Operaciones\Prestamo') ;
    }

    public function tipoGarantia()
    {
        return $this->belongsTo('App\Models\TipoGarantia') ;
    }

}
