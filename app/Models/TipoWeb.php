<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoWeb extends Model
{
	protected $table = 'tipo_web';
    public $timestamps = false;
	protected $fillable = [
							'nombre',
							'estado',
						];
}
