<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
	protected $table = 'area';
    public $timestamps = false;
	protected $fillable = [
						'nombre',
						'descripcion',
						'estado',
					];

	public function cargo()
	{
		return $this->hasMany('\App\Models\Cargo');
	}
}
