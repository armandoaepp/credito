<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RolControl extends Model
{
    protected $table = 'rol_control';
    public $timestamps = false;

    protected $fillable = [
								'rol_id',
								'control_id',
								'referencia',
								'estado',
							] ;

	public function rol()
    {
        return $this->belongsTo('App\Models\Rol') ;
    }

    public function control()
    {
        return $this->belongsTo('App\Models\Control') ;
    }

}
