<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerWeb extends Model
{
	protected $table = 'per_web';
    public $timestamps = false;
	protected $fillable = [
							'persona_id',
							'tipo_web_id',
							'url',
							'estado',
						];

}
