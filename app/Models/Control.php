<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Control extends Model
{
    protected $table = 'control';
    protected $fillable =  [
						'control_padre_id',
						'tipo_control_id',
						'jerarquia',
						'nombre',
						'valor',
						'descripcion',
						'glosa',
						'estado',
     ] ;

    public $timestamps = false;

    public function ChildControl()
	{
		return $this->hasMany('\App\Models\Control','control_padre_id', 'id' );
	}


	public function parentAccount()
	{
	    return $this->belongsTo('Account', 'id', 'control_padre_id');
	}

	public function allChildrenControl()
	{
	    return $this->ChildControl()->with('allChildrenControl');
	}
}
