<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerMail extends Model
{
    protected $table = 'per_mail';
    public $timestamps = false;

    protected $fillable = [
							'persona_id',
							'mail',
							'item',
						];

	public function persona()
    {
        return $this->belongsTo('App\Models\Persona') ;
    }

}
