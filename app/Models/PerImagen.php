<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerImagen extends Model
{
    protected $table = 'per_imagen';
    public $timestamps = false;

    protected $fillable = [
						'url',
						'tipo',
						'estado'
					];

	protected $guarded = ['id'];


    public function persona()
    {
        return $this->belongsTo('App\Models\Persona') ;
    }
}
