<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rubro extends Model
{
    protected $table = 'rubro';
    public $timestamps = false;


    protected $fillable = [
								'nombre',
								'estado',
							] ;

    public function perJuridica()
	{
		return $this->hasMany('App\Models\PerJuridica');
	}

}
