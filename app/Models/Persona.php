<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = 'persona';

    protected $fillable = [
						'per_nombre',
						'per_apellidos',
						'per_fecha_nac',
						'per_tipo',
						'estado'
					];

	protected $guarded = ['id'];

	# relaciones
		public function user()
		{
			return $this->hasOne('\App\User');
		}

		public function perNatural()
		{
			return $this->hasOne('\App\Models\PerNatural');
		}

		public function perJuridica()
		{
			return $this->hasOne('\App\Models\PerJuridica');
		}

		public function perTelefono()
		{
			return $this->hasMany('\App\Models\PerTelefono')->orderBy('item');
		}

		public function perMail()
		{
			return $this->hasMany('\App\Models\PerMail')->orderBy('item');
		}

		public function perDocumento()
		{
			return $this->hasMany('\App\Models\PerDocumento');
		}

		public function perDireccion()
		{
			return $this->hasMany('\App\Models\PerDireccion');
		}

		public function perImagen()
		{
			return $this->hasOne('\App\Models\PerImagen');
		}

		public function perRelacion()
		{
			return $this->hasMany('\App\Models\PerRelacion','persona_id');
		}

		public function perWeb()
		{
			return $this->hasMany('\App\Models\PerWeb','persona_id');
		}




	# attributes
		public function getFullNameAttribute()
		{
			return $this->per_nombre .' '. $this->per_apellidos ;
		}

		# edad del persona
		public function getAgeAttribute()
		{
			return \Carbon\Carbon::parse($this->per_fecha_nac)->age;
		}

		/*public function getFullNameAttribute() {
		    return $this->attributes['name'] . ' - ' . $this->attributes['code'];
		}*/


		public function setPerFechaNacAttribute($value)
	    {
	        $this->attributes['per_fecha_nac'] = \Carbon\Carbon::createFromFormat('Y-m-d',$value)->format('Y-m-d') ;
	    }

	    /*public function getPerFechaNacAttribute()
	    {
	        return \Carbon\Carbon::createFromFormat('Y-m-d',$this->attributes['per_fecha_nac'])->format('d/m/Y');
	    }*/

	# scopes
    	# personas naturales
	    public function scopeNatural($query)
	    {
	        return $query->where('per_tipo', '=', 1);
	    }

	    public function scopeJuridica($query)
	    {
	        return $query->where('per_tipo', '=', 2);
	    }

}
