<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    protected $table = 'cargo';
    public $timestamps = false;
	protected $fillable = [
						'area_id',
						'nombre',
						'descripcion',
						'estado',
					];

	public function area()
	{
        return $this->belongsTo('App\Models\Area') ;
	}
}
