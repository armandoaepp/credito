<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoTelefono extends Model
{
    protected $table = 'tipo_telefono';
    public $timestamps = false;

    protected $fillable = [
								'nombre',
								'estado',
							] ;

    public function telefono()
	{
		return $this->hasMany('App\Models\Telefono');
	}
}
