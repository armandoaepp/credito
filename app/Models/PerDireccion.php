<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerDireccion extends Model
{
    protected $table = 'per_direccion';
    public $timestamps = false;


    protected $fillable = [
							'persona_id',
							'tipo_direccion_id',
							'ubigeo_id',
							'direccion',
							'referencia',
							'estado',
						];

	public function persona()
    {
        return $this->belongsTo('App\Models\Persona') ;
    }

    public function tipoDireccion()
    {
        return $this->belongsTo('App\Models\TipoDireccion') ;
    }

    public function ubigeo()
    {
        return $this->belongsTo('App\Models\Ubigeo') ;
    }
}
