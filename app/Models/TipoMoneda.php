<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoMoneda extends Model
{
	protected $table = 'tipo_moneda';
    public $timestamps = false;

    protected $fillable =  [
							'nombre',
							'simbolo',
							'estado' ,
							] ;
}
