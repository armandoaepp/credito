<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
	protected $table = 'empleado';

    protected $fillable =  [
						'persona_id',
						'persona_id_padre',
						'estado',
     ] ;

    public function persona()
    {
    	return $this->belongsTo('App\Models\Persona');
    }
}
