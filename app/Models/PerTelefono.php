<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerTelefono extends Model
{
    protected $table = 'per_telefono';
    public $timestamps = false;

	protected $fillable = [
						    'persona_id',
							'tipo_telefono_id',
							'telefono',
							'item',
						];

	public function persona()
    {
        return $this->belongsTo('App\Models\Persona') ;
    }

    public function tipoTelefono()
    {
        return $this->belongsTo('App\Models\TipoTelefono') ;
    }
}
