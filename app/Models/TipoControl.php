<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoControl extends Model
{
    protected $table = 'tipo_control';
    public $timestamps = false;
    protected $fillable = [
							'nombre',
							'estado',
						];

	public function control()
	{
		return $this->hasMany('App\Models\Control');
	}
}
