<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{

	protected $table = 'cliente';
	protected $fillable = [
							'persona_id',
							'codigo',
							'tipo',
							'persona_id_padre',
							'estado',
						];

	public function persona()
    {
    	return $this->belongsTo('App\Models\Persona');
    }





}
