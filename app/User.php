<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function persona()
    {
        return $this->belongsTo('App\Models\Persona') ;
    }

    public function rol()
    {
        return $this->belongsTo('App\Models\Rol');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value) ;
    }
}
