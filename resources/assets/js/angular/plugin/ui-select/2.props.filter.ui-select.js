(function() {
    'use strict';

    // recomendacion tener cuidado en utilizar los filter puede consumir recursos
    angular.module('ui.select').filter("propsFilter", propsFilter);

    function propsFilter() {
        return function(items, props) {
            var out = [];

            //  texto es el mismo que se buscara para todos de elementos (por ejemplo {name: "12", age: "12"})
            var keys = Object.keys(props);
            var firstElement = keys[0];
            var text = props[firstElement].toLowerCase();

            // text nulo retornamos como maximo 100 items(para array grandes)
            if (text.length < 1) {
                if (angular.isArray(items)) {
                    if (items.length > 100) {
                        out = items.slice(0, 100);
                    } else {
                        out = items;
                    };
                } else {
                    out = items;
                }
                return out;
            }

            // text diferente de nulo,  recoremos el array
            if (angular.isArray(items)) {
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    var itemMatches = false;
                    for (var j = 0; j < keys.length; j++) {
                        var prop = keys[j];
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                    // mientras buscamos, si el arrar es grande devolvemos maximo 50(cambiarlo por lo que deseen)
                    if (out.length > 50) {
                        break;
                    };
                }

            } else {
                // Let the output be the input untouched
                out = items;
            }
            return out;
        };
    }

})();
