(function(){
    angular
        .module('appPlataforma')
        .config(appConfig) ;

        appConfig.$inject = ['$interpolateProvider','$stateProvider', '$urlRouterProvider','$locationProvider'] ;

        function appConfig($interpolateProvider,$stateProvider,$urlRouterProvider,$locationProvider)
        {
            $interpolateProvider.startSymbol('{%');
            $interpolateProvider.endSymbol('%}');

            $urlRouterProvider.
                    otherwise('/');
        }
})() ;