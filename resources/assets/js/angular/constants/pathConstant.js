(function(){

	angular.module("paths.constant").constant('PATH', {
		APIURL		: 'web',
		RAIZ		: '../partials',
		INFORMACION		: '../partials/informacion',
		MOVIMIENTOS		: '../partials/movimientos',
		REPORTES		: '../partials/reportes',
		CONFIGURACION	: '../partials/configuracion',

	}) ;


})();

