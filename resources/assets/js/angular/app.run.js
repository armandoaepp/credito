(function(){
    angular
        .module('appPlataforma')
        .run(appRun) ;

        appRun.$inject =  ['$rootScope', '$state', '$stateParams','$location','editableOptions','ngTableDefaults'] ;

        function  appRun($rootScope,   $state,   $stateParams, $location,editableOptions,ngTableDefaults) {

            // It's very handy to add references to $state and $stateParams to the $rootScope
            // so that you can access them from any scope within your applications.For example,
            // <li ng-class="{ active: $state.includes('contacts.list') }"> will set the <li>
            // to active whenever 'contacts.list' or one of its decendents is active.
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;

            editableOptions.theme = 'bs3';

            ngTableDefaults.params.count = 10;
            ngTableDefaults.settings.counts = [];
        }

})();
