(function(){
    'use strict';

    angular.module('validate.directive').directive('isDecimal',isDecimal);
    isDecimal.$inject = [] ;

        function isDecimal()
        {

            var directive = {
                link: link,
                restrict: 'A',
                require: '?ngModel',
            };

            return directive;

            function link(scope, element, attrs,ngModelCtrl)
            {
              var number_digitos = 2 ;
              var lengthDecimal = (attrs.isDecimal.trim()).length ;

              if (lengthDecimal > 0 )
              {
                number_digitos = parseInt(attrs.isDecimal) ;
              };

                if(!ngModelCtrl) {
                  return;
                }

                ngModelCtrl.$parsers.push(function(val) {
                  if (angular.isUndefined(val)) {
                      var val = '';
                  }

                  var clean = val.replace(/[^-0-9\.]/g, '');
                  var negativeCheck = clean.split('-');
                  var decimalCheck = clean.split('.');
                  if(!angular.isUndefined(negativeCheck[1])) {
                      negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                      clean =negativeCheck[0] + '-' + negativeCheck[1];
                      if(negativeCheck[0].length > 0) {
                        clean =negativeCheck[0];
                      }

                  }

                  if(!angular.isUndefined(decimalCheck[1])) {
                      // decimalCheck[1] = decimalCheck[1].slice(0,2);
                      decimalCheck[1] = decimalCheck[1].slice(0,number_digitos);
                      clean =decimalCheck[0] + '.' + decimalCheck[1];
                  }

                  if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                  }
                  return clean;
                });

                element.bind('keypress', function(event) {
                  if(event.keyCode === 32) {
                    event.preventDefault();
                  }
                });
            }
        }

})();