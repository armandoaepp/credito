(function(){
    'use strict';

    angular.module('menu.directive').directive('menuBotonera',menuBotonera);
    menuBotonera.$inject = ['$rootScope','$state', '$location','botoneraFactory'] ;

        function menuBotonera($rootScope,$state , $location  ,botoneraFactory  )
        {

            var directive = {
                link: link,
                restrict: 'A'
            };

            return directive;

            function link(scope, element, attrs){

                  element.bind('click', function(){
                    sendData() ;
                  });

                  function sendData()
                  {
                      botoneraFactory.setBotonera(null) ;
                      if (attrs.botonera === '' ) return ;
                      var botones = JSON.parse(attrs.botonera) ;
                      // ingresamos botonera a facoria
                      botoneraFactory.setBotonera(botones) ;

                      // para recargar botonera en caso de que este vacia
                      var menu       = attrs.menu ;
                      var  menupadre = attrs.menupadre  ;

                      var values =  {
                                      id               : menu,
                                      control_padre_id : menupadre,
                                    }

                      $rootScope.$broadcast("recargarBotoneraAccesoCtrl", values);

                  }

                  // activar la botonera
                  var state = attrs.uiSref;
                  function isStateActiveBotonera()
                  {
                      if ( $state.includes(state) || $state.is(state) )
                      {
                        sendData()
                      } ;
                  }
                  isStateActiveBotonera() ;

              }
        }

})();