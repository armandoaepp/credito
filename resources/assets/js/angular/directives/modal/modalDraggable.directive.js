(function(){
	'use strict';

        angular.module('modal.directive').directive('modalDraggable',modalDraggable);
        modalDraggable.$inject = ['$document'] ;

        function modalDraggable($document)
        {
        	return function(scope, element, attr){
		    	var startX = 0, startY = 0, x = 0, y = 0;
				element.css({
					cursor: 'pointer'
				});

				var padre = element.parent().parent().parent();

				element.on('mousedown', function(event) {
					// Prevent default dragging of selected content
					event.preventDefault();
					startX = event.pageX - x;
					startY = event.pageY - y;
					$document.on('mousemove', mousemove);
					$document.on('mouseup', mouseup);
				});

				function mousemove(event) {
					y = event.pageY - startY;
					x = event.pageX - startX;
					padre.css({
						top: y + 'px',
						left:  x + 'px'
					});
				}
			    function mouseup() {
					$document.off('mousemove', mousemove);
					$document.off('mouseup', mouseup);
			    }
			};

        }


})();
