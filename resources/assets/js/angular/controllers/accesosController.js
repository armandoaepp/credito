(function(){

        angular.module('menu.controller').controller('accesosMenusCrtl',accesosMenusCrtl);
        accesosMenusCrtl.$inject = ['accesosService','menusFactory'] ;

            function accesosMenusCrtl(accesosService, menusFactory)
            {
                var vm = this ;
                vm.menus = [] ;
                vm.menus = vm.menus ;

                vm.getAccesosService =  getAccesosService ;
                vm.getAccesos        =  getAccesos ;

                vm.getAccesos() ;

                function getAccesos(){

                    menusFactory.getMenus().then(
                            function(response){
                                vm.menus = response;
                                 return vm.menus ;
                            }
                        );

                } ;



                // asegurar de que carge el menu si la factory esta vacia
                function  getAccesosService(){

                    accesosService.getAccesos().then(
                        function(response){
                            if (!response.error){
                                if (response.data.length > 0)
                                {
                                    // vm.menus = response.data[0]['children'];
                                    vm.menus = response.data;
                                    menusFactory.setMenus(vm.menus);
                                    return vm.menus ;
                                };
                            }
                        }
                    );
                }
            }


        angular.module('menu.controller').controller('BotoneraAccesoCtrl',BotoneraAccesoCtrl);
        BotoneraAccesoCtrl.$inject = ['$rootScope','botoneraFactory'] ;

            function BotoneraAccesoCtrl($rootScope,botoneraFactory)
            {
                var vm = this ;
                vm.botones = botoneraFactory.getBotonera();

                  // el $broadcast se encuentra en drective(itemModulo)
                $rootScope.$on("recargarBotoneraAccesoCtrl", function(event, values) {
                        vm.botones = botoneraFactory.getBotonera();
                  });


            }



})() ;
