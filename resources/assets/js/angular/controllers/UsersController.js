(function(){


        function UserInfoCtrl(usersService)
        {
            var vm = this ;
             vm.menus = [] ;
             vm.getInfo = getInfo ;

             vm.data = [] ;

             init();
             function init() {
                vm.getInfo() ;
             }

            function getInfo(){

                usersService.getInfo().then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data = response.data;
                            return vm.data ;
                        }
                    }
                );
            }


        }

        angular.module('users.controller').controller('UserInfoCtrl',UserInfoCtrl);
        UserInfoCtrl.$inject = ['usersService'] ;


})() ;
