(function(){
    'use stric' ;

    angular.module('cred.menor.mov.controller').controller('ModalCancelarCredMenorCrtl', ModalCancelarCredMenorCrtl) ;
    ModalCancelarCredMenorCrtl.$inject = ['$uibModalInstance', 'data_prestamo','prestamoService'] ;

        function ModalCancelarCredMenorCrtl($uibModalInstance,data_prestamo, prestamoService)
        {
            var vm =  this ;

            vm.msj = "";
            vm.fillSelected = data_prestamo ;

            vm.formData = {
                glosa : '',
            }

            vm.ok = function ()
            {
                var params = {
                    'glosa' : vm.formData.glosa,
                    'tipo_estado_id' : 3,
                    'prestamo_id' : vm.fillSelected.id,
                };
                console.log(params);
                // return ;
                prestamoService.updateEstadoPrestamoTipoEstado(params).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;