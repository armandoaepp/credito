(function() {
    'use stric';

    angular.module('cred.menor.mov.controller').controller('EditCredMenorMovCrtl', EditCredMenorMovCrtl);
    EditCredMenorMovCrtl.$inject = [
        '$stateParams',
        '$rootScope',
        '$state',
        '$filter',
        'tipoPrestamoService',
        'tipoMonedaService',
        'tipoPeriodoService',
        'tipoGarantiaService',
        'tipoPagoService',
        'empleadoService',
        'clienteService',
        'vehiculoService',
        'avalService',
        'prestamoService',
        'acuerdoPagoService',
        'cuotaService',
        'PATH',
        'FileUploader',
        'NgTableParams',
        'creditoMenorService',
        'prestamoGarantiaService',
        'prestamoReferenciaService',
    ];



    function EditCredMenorMovCrtl(
        $stateParams,
        $rootScope,
        $state,
        $filter,
        tipoPrestamoService,
        tipoMonedaService,
        tipoPeriodoService,
        tipoGarantiaService,
        tipoPagoService,
        empleadoService,
        clienteService,
        vehiculoService,
        avalService,
        prestamoService,
        acuerdoPagoService,
        cuotaService,
        PATH,
        FileUploader,
        NgTableParams,
        creditoMenorService,
        prestamoGarantiaService,
        prestamoReferenciaService
    ) {
        var vm = this;

        vm.prestamo_id = $stateParams.codigo;
        vm.data_list = [];

        vm.disabledInput = true;


        vm.msj = "";

        vm.formData = {
            tipo_prestamo: undefined,
            tipo_moneda: undefined,
            tipo_periodo: undefined,
            acuerdo_pago: undefined,
            tipo_pago: undefined,
            tipo_garantia: undefined,
            asesor_negocio: undefined,
            cliente: undefined,
            aval: undefined,
            interes: '',
            num_cuotas: '',
            mora: '',
            complacencia: '',
            pagos_parciales: 'NO',
            propietario: '',
            observacion: '',
            fecha_credito: null,
            fecha_desembolso: null,
            referencia_web: '',
            monto: '',
        };

        vm.tipo_prestamos   = [];
        vm.tipo_monedas     = [];
        vm.tipo_periodos    = [];
        vm.acuerdo_pagos    = [];
        vm.tipo_pagos       = [];
        vm.tipo_garantias   = [];
        vm.asesores_negocio = [];
        vm.clientes         = [];
        vm.data_vehiculos   = [];
        vm.data_avales      = [];



        //  DatePicker ui-b
        vm.popup = {
            opened: false,
        };
        vm.dateOptions = {
            startingDay: 1, // inicie en lunes
            showWeeks: 'false',
            minDate: null,
        };

        vm.openCalendar = function() {
            vm.popup.opened = true;
        };

        vm.popup2 = {
            opened: false,
        };
        vm.openCalendar2 = function() {
            vm.popup2.opened = true;
        };

        /*   vm.popup3 = {
               opened : false,
           } ;
           vm.openCalendar3 = function() {
               vm.popup3.opened = true;
           };

           vm.popup4 = {
               opened : false,
           } ;
           vm.openCalendar4 = function() {
               vm.popup4.opened = true;
           };*/

        init();

        function init() {
            getPrestamoById();
            getTipoPrestamos();
            setTimeout(getClientesInfoBasica(), 1000);
            setTimeout(getTipoMonedas(), 1500);
            setTimeout(getTipoPeriodos(), 2000);
            setTimeout(getTipoGarantias(), 2500);
            // setTimeout(getAvalesInfoBasica(), 3000);

            setTimeout(getPretamoReferenciasByPrestamoId(), 3000);
            setTimeout(getPretamoGarantiasByPrestamoId(), 3500);

            // getCuotasByPrestamoId();

        };

        //  carga de datos

        function getPrestamoById() {
            var params = {
                'prestamo_id': vm.prestamo_id,
            };
            prestamoService.getPrestamoById(params).then(
                function(response) {
                    if (!response.error) {
                        vm.data_list = response.data;

                        console.log(vm.data_list);


                        var estado = parseInt(vm.data_list.estado);
                        vm.disabledInputsEdit = true;
                        if (estado === 1) {
                            vm.disabledInputsEdit = false;
                        };

                        vm.formData.tipo_prestamo_id    = vm.data_list.tipo_prestamo_id;
                        vm.formData.tipo_pago_id        = vm.data_list.tipo_pago_id;
                        vm.formData.cliente_id          = vm.data_list.cliente_id;
                        vm.formData.tipo_moneda_id      = vm.data_list.tipo_moneda_id;
                        vm.formData.interes             = vm.data_list.tasa_interes;
                        vm.formData.tipo_periodo_id     = vm.data_list.tipo_periodo_id;
                        vm.formData.acuerdo_pago_id     = vm.data_list.acuerdo_pago_id;
                        vm.formData.num_cuotas          = vm.data_list.num_cuotas;
                        vm.formData.pagos_parciales     = vm.data_list.pagos_parciales;
                        vm.formData.propietario         = vm.data_list.propietario;
                        vm.formData.aval_id             = vm.data_list.aval_id;
                        vm.formData.observacion         = vm.data_list.observacion;
                        vm.formData.tipo_garantia_id    = vm.data_list.tipo_garantia_id;
                        vm.formData.fecha_credito       = (!vm.data_list.fecha_credito) ? null : new Date(vm.data_list.fecha_credito + ' 00:00:00');
                        vm.formData.fecha_desembolso    = (!vm.data_list.fecha_desembolso) ? null : new Date(vm.data_list.fecha_desembolso + ' 00:00:00');
                        vm.formData.fecha_prorrateo     = (!vm.data_list.fecha_prorrateo) ? null : new Date(vm.data_list.fecha_prorrateo + ' 00:00:00');
                        vm.formData.fecha_prorrateo_esp = (!vm.data_list.fecha_prorrateo_esp) ? null : new Date(vm.data_list.fecha_prorrateo_esp + ' 00:00:00');
                        vm.formData.monto               = vm.data_list.valor;
                        vm.formData.complacencia        = vm.data_list.complacencia;

                        // vm.formData.referencia_web       = vm.data_list.complacencia;
                        /*    vm.formData.inicial_porcentaje = vm.data_list.inicial_porcentaje ;
                            vm.formData.inicial_monto      = vm.data_list.inicial_monto ;
                            vm.formData.seguro_tr          = vm.data_list.seguro_tr ;
                            vm.formData.gps                = vm.data_list.gps ;
                            vm.formData.soat               = vm.data_list.soat ;
                            vm.formData.gas                = vm.data_list.gas ;
                            vm.formData.otros              = vm.data_list.otros ;
                            vm.formData.vehiculo_id        = vm.data_list.vehiculo_id ;*/



                        getTipoPagosByTipoPrestamoId();
                        // vm.onSelectTipoPrestamo(null);
                        vm.getAcuerdoPagosByTipoPeriodoId(null, null);
                        return vm.tipo_prestamos;
                    }
                }
            );
        };
        //  carga de datos

        function getTipoPrestamos() {
            tipoPrestamoService.getTipoPrestamos().then(
                function(response) {
                    if (!response.error) {
                        vm.tipo_prestamos = response.data;
                        vm.formData.tipo_prestamo = getFill(vm.tipo_prestamos, 4);
                        getTipoPagosByTipoPrestamoId();
                        return vm.tipo_prestamos;
                    }
                }
            );
        };

        function getFill(data, idSelected) {
            var out = []
            for (var i in data) {
                if (parseInt(data[i].id) === parseInt(idSelected)) {
                    out = data[i];
                    return out;
                };
            };
            return out;
        };

        function getTipoMonedas() {
            tipoMonedaService.getTipoMonedas().then(
                function(response) {
                    if (!response.error) {
                        vm.tipo_monedas = response.data;
                        return vm.tipo_monedas;
                    }
                }
            );
        };

        function getTipoPeriodos() {
            tipoPeriodoService.getTipoPeriodos().then(
                function(response) {
                    if (!response.error) {
                        vm.tipo_periodos = response.data;
                        return vm.tipo_periodos;
                    }
                }
            );
        };

        // tipo-couta
        function getTipoPagosByTipoPrestamoId()
        {
            var data = {
                // 'tipo_prestamo_id' : vm.formData.tipo_prestamo.id,
                'tipo_prestamo_id': 4,
            };
            tipoPagoService.getTipoPagosByTipoPrestamoId(data).then(
                function(response) {
                    if (!response.error)
                    {
                        vm.tipo_pagos = response.data;
                        return vm.tipo_pagos;
                    }
                }
            );
        };

        vm.getAcuerdoPagosByTipoPeriodoId = function($item, $model) {
            var data = {
                'tipo_periodo_id': vm.formData.tipo_periodo_id,
            };

            acuerdoPagoService.getAcuerdoPagosByTipoPeriodoId(data).then(
                function(response) {
                    if (!response.error) {
                        vm.acuerdo_pagos = response.data;
                        return vm.acuerdo_pagos;
                    }
                }
            );
        };

        function getTipoGarantias() {
            tipoGarantiaService.getTipoGarantias().then(
                function(response) {
                    if (!response.error) {
                        vm.tipo_garantias = response.data;
                        return vm.tipo_garantias;
                    }
                }
            );
        };

        function getClientesInfoBasica() {
            var per_tipo = 0;
            var data = {
                'per_tipo': per_tipo,
            };

            clienteService.getClientesInfoBasica(data).then(
                function(response) {
                    console.info('cleintes', response);
                    if (!response.error) {
                        vm.clientes = response.data;
                    } else {
                        vm.msj = response.error;
                    }
                }
            );
        };

        function getPretamoReferenciasByPrestamoId() {
            var params = {
                'prestamo_id': vm.prestamo_id,
            };
            prestamoReferenciaService.getPretamoReferenciasByPrestamoId(params).then(
                function(response) {
                    if (!response.error) {
                        var data = response.data;
                        vm.data_referencias = filterItems(data, { 'tipo': 2 }) ;
                        vm.data_ref_web = filterItems(data, { 'tipo': 1 }) ;

                        return vm.data_referencias;
                    }
                }
            );
        };

        function getPretamoReferenciasByPrestamoIdTipo(tipo) {
            var params = {
                'prestamo_id': vm.prestamo_id,
                'tipo': tipo,
            };
            prestamoReferenciaService.getPretamoReferenciasByPrestamoIdTipo(params).then(
                function(response) {
                    if (!response.error) {
                        var data = response.data;
                        vm.data_referencias = filterItems(data, { 'tipo': 2 }) ;
                        return vm.data_referencias;
                    }
                }
            );
        };

        function getPretamoGarantiasByPrestamoId() {
            var params = {
                'prestamo_id': vm.prestamo_id,
            };
            prestamoGarantiaService.getPretamoGarantiasByPrestamoId(params).then(
                function(response) {
                    if (!response.error) {
                        vm.data_garantias = response.data;
                        console.info('garantias', vm.data_garantias);
                        return vm.data_garantias;
                    }
                }
            );
        };


        // =====================================================================

            vm.file_descripcion = [];
            // # sfksdfd
            var uploader = vm.uploader = new FileUploader({
                url: 'operaciones/credito-menor/uploads',
                formData: [],
            });

            // FILTERS

            uploader.filters.push({
                name: 'customFilter',
                fn: function(item /*{File|FileLikeObject}*/ , options) {
                    return this.queue.length < 10;
                }
            });


            // CALLBACKS

            uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
                console.info('onWhenAddingFileFailed', item, filter, options);
            };
            uploader.onAfterAddingFile = function(fileItem) {
                console.info('onAfterAddingFile', fileItem);
            };
            uploader.onAfterAddingAll = function(addedFileItems) {
                console.info('onAfterAddingAll', addedFileItems);
            };
            uploader.onBeforeUploadItem = function(item) {
                console.log('onBeforeUploadItem', item);

                var index = uploader.getIndexOfItem(item);
                console.info('file_descripcion: ', vm.file_descripcion[index]);

                item.formData.push({
                    descripcion: vm.file_descripcion[index],
                    prestamo_id: vm.prestamo_id
                });
            };
            uploader.onProgressItem = function(fileItem, progress) {
                console.info('onProgressItem', fileItem, progress);
            };
            uploader.onProgressAll = function(progress) {
                console.info('onProgressAll', progress);
            };
            uploader.onSuccessItem = function(fileItem, response, status, headers) {
                // console.info('onSuccessItem', fileItem, response, status, headers);
                console.info('onSuccessItem fileItem', fileItem);
                console.info('onSuccessItem response', response);
                console.info('onSuccessItem status', status);
                console.info('onSuccessItem headers', headers);


            };
            uploader.onErrorItem = function(fileItem, response, status, headers) {
                console.info('onErrorItem', fileItem, response, status, headers);
            };
            uploader.onCancelItem = function(fileItem, response, status, headers) {
                console.info('onCancelItem', fileItem, response, status, headers);
                vm.msj = null;
            };
            uploader.onCompleteItem = function(fileItem, response, status, headers) {
                console.info('onCompleteItem', fileItem, response, status, headers);
            };
            uploader.onCompleteAll = function() {
                console.info('onCompleteAll');
            };
        // =====================================================================


        function update()
        {
            var tipo_prestamo_id = vm.formData.tipo_prestamo_id ;
            var tipo_pago_id     = vm.formData.tipo_pago_id ;
            var cliente_id       = vm.formData.cliente_id ;
            var tipo_moneda_id   = vm.formData.tipo_moneda_id ;
            var valor            = vm.formData.monto ;
            var tasa_interes     = vm.formData.interes ;
            var tipo_periodo_id  = vm.formData.tipo_periodo_id ;
            var acuerdo_pago_id  = vm.formData.acuerdo_pago_id ;
            var num_cuotas       = vm.formData.num_cuotas ;
            var complacencia     = vm.formData.complacencia ;
            var observacion      = vm.formData.observacion ;
            var fecha_credito    = vm.formData.fecha_credito ;
            var fecha_desembolso = vm.formData.fecha_desembolso ;
            var referencia_web    = vm.formData.referencia_web ;

            var data_garantias   = vm.tableParams.settings().dataset ;
            vm.msj = {message : ''} ;

            // VALIDACION
                if (tipo_prestamo_id === undefined)
                {
                    return  vm.msj.message = 'Seleccionar Tipo Prestramo' ;
                };

                if (cliente_id === undefined)
                {
                    return  vm.msj.message = 'Seleccionar Cliente' ;
                };

                if (tipo_moneda_id === undefined)
                {
                    return  vm.msj.message = 'Seleccionar Tipo de Moneda' ;
                };

                if (valor === undefined)
                {
                    return  vm.msj.message = 'Ingresar Monto de prestamo' ;
                };

                if (tasa_interes === undefined)
                {
                    return  vm.msj.message = 'Ingresar Tasa de Interes' ;
                };

                if (tipo_periodo_id === undefined)
                {
                    return  vm.msj.message = 'Seleccionar Periodo de Cuotas' ;
                };

                if (num_cuotas === undefined)
                {
                    return  vm.msj.message = 'Ingresar Número de Cuotas' ;
                };

                if (fecha_desembolso === undefined)
                {
                    return  vm.msj.message = 'Ingresar Fecha Desembolso' ;
                };

                if (fecha_credito === undefined)
                {
                    return  vm.msj.message = 'Ingresar Fecha Desembolso' ;
                };


            var fecha_credito = $filter('date')(vm.formData.fecha_credito,'yyyy-MM-dd');
            var fecha_desembolso = $filter('date')(vm.formData.fecha_desembolso,'yyyy-MM-dd');

            var params = {
                'prestamo_id' : vm.prestamo_id,
                'tipo_prestamo_id' : tipo_prestamo_id,
                'tipo_pago_id' : tipo_pago_id,
                'cliente_id' : cliente_id,
                'tipo_moneda_id' : tipo_moneda_id,
                'valor' : valor,
                'tasa_interes' : tasa_interes,
                'tipo_periodo_id' : tipo_periodo_id,
                'acuerdo_pago_id' : acuerdo_pago_id,
                'num_cuotas' : num_cuotas,
                'complacencia' : complacencia,
                'observacion' : observacion,
                'fecha_credito' : fecha_credito,
                'fecha_desembolso' : fecha_desembolso,
                'referencia_web' : referencia_web,
                'data_garantias' : data_garantias
            };

            console.info('params', params);
            // return ;
            creditoMenorService.update(params).then(
                function(response)
                {
                    console.log(response);
                    if (!response.error)
                    {
                        vm.prestamo_id = response.data ;
                        setTimeout(vm.uploader.uploadAll(), 1000);
                        setTimeout(vm.reloadList(), 1500);

                    }else
                    {
                        vm.msj = response.error ;
                    }
                }
            );
        };

        vm.ok = function() {
            console.log('ok');
            update();
        };

        vm.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.reloadList = function() {
            var current = $state.current.name;
            var res = current.split(".", 2);
            var current_parent = res[0] + "." + res[1];

            var values = '';
            $state.go(current_parent, values);
            $rootScope.$broadcast("reloadList", values);
        };

        /*vm.data_list_coutas = [];

        function getCuotasByPrestamoId() {
            var data = {
                'prestamo_id': vm.prestamo_id,
            };

            cuotaService.getCuotasByPrestamoId(data).then(
                function(response) {
                    console.log(response);
                    if (!response.error) {
                        vm.data_list_coutas = response.data;
                        return vm.data_list_coutas;

                    }
                }
            );
        };*/

        // =====================================================================
            vm.simpleList = [];
            var originalData = angular.copy(vm.simpleList);

            vm.tableParams = new NgTableParams({}, {
                dataset: angular.copy(vm.simpleList)
            });

            vm.deleteCount = 0;

            vm.add = add;
            vm.cancelChanges = cancelChanges;
            vm.del = del;
            vm.hasChanges = hasChanges;
            vm.saveChanges = saveChanges;

            //////////

            function add() {
                vm.isEditing = true;
                vm.isAdding = true;
                vm.tableParams.settings().dataset.unshift({
                    tipo_garantia: null,
                    // tipo_garantia_id : null ,
                    producto: null,
                    serie: null,
                    desripcion: null,
                });
                // we need to ensure the user sees the new row we've just added.
                // it seems a poor but reliable choice to remove sorting and move them to the first page
                // where we know that our new item was added to
                vm.tableParams.sorting({});
                vm.tableParams.page(1);
                vm.tableParams.reload();
            }

            function cancelChanges() {
                resetTableStatus();
                var currentPage = vm.tableParams.page();
                vm.tableParams.settings({
                    dataset: angular.copy(originalData)
                });
                // keep the user on the current page when we can
                if (!vm.isAdding) {
                    vm.tableParams.page(currentPage);
                }
            };

            function resetRow(row, rowForm) {
                row.isEditing = false;
                rowForm.$setPristine();
                for (var i in vm.data_list) {
                    if (vm.data_list[i].id === row.id) {
                        return vm.data_list[i]
                    }
                }
            };

            function arrayObjectIndexOf(arr, obj) {
                for (var i = 0; i < arr.length; i++) {
                    if (angular.equals(arr[i], obj)) {
                        return i;
                    }
                };
                return -1;
            }

            function del(row) {
                var data = vm.tableParams.settings().dataset;
                for (var i in data) {
                    if (angular.equals(data[i], row)) {
                        vm.tableParams.settings().dataset.splice(i, 1);
                        vm.deleteCount++;
                        vm.tableParams.reload().then(function(data) {
                            if (data.length === 0 && vm.tableParams.total() > 0) {
                                vm.tableParams.page(vm.tableParams.page() - 1);
                                vm.tableParams.reload();
                                console.log(vm.tableParams.settings().dataset.length);
                                if (vm.tableParams.settings().dataset.length === 0) { vm.cancelChanges() };
                            }
                        });
                        return i;
                    }
                }

            }

            function hasChanges() {
                return vm.tableForm.$dirty || vm.deleteCount > 0
            }

            function resetTableStatus() {
                vm.isEditing = false;
                vm.isAdding = false;
                vm.deleteCount = 0;
                // vm.tableTracker.reset();
                vm.tableForm.$setPristine();
            }

            function saveChanges() {
                resetTableStatus();
                var currentPage = vm.tableParams.page();
                originalData = angular.copy(vm.tableParams.settings().dataset);
            }


        // =====================================================================


        function filterItems(items, props) {
            var out = [];

            //  texto es el mismo que se buscara para todos de elementos (por ejemplo {name: "12", age: "12"})
            var keys = Object.keys(props);
            var firstElement = keys[0];

            var text = String(props[firstElement]).toLowerCase();
            // text diferente de nulo,  recoremos el array
            if (angular.isArray(items)) {
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    var itemMatches = false;
                    var prop = keys[0];
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                }

            } else {
                // Let the output be the input untouched
                out = items;
            }
            return out;
        };

        // ========= update
            vm.prestamoReferenciaUpdateEstado = prestamoReferenciaUpdateEstado ;
            function prestamoReferenciaUpdateEstado(id)
            {
                var params = {
                    'prestamo_referencia_id': id,
                    'estado': 0,
                };

                prestamoReferenciaService.updateEstado(params).then(
                    function(response) {
                        console.log(response);
                        if (!response.error)
                        {
                            getPretamoReferenciasByPrestamoIdTipo(2) ;
                            // var  = response.data;
                            return ;

                        }
                    }
                );
            };

            vm.prestamoGarantiaUpdateEstado = prestamoGarantiaUpdateEstado ;
            function prestamoGarantiaUpdateEstado(id)
            {
                var params = {
                    'prestamo_garantia_id': id,
                    'estado': 0,
                };
                console.info(params);
                prestamoGarantiaService.updateEstado(params).then(
                    function(response) {
                        console.log(response);
                        if (!response.error)
                        {
                            getPretamoGarantiasByPrestamoId() ;
                            return ;

                        }
                    }
                );
            };


    };
})();
