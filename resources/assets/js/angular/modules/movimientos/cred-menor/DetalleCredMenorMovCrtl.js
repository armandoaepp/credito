(function(){
    'use stric' ;

    angular.module('cred.menor.mov.controller').controller('DetalleCredMenorMovCrtl', DetalleCredMenorMovCrtl) ;
    DetalleCredMenorMovCrtl.$inject = [
        '$stateParams',
        '$rootScope',
        '$state',
        '$filter',
        'tipoPrestamoService',
        'tipoMonedaService',
        'tipoPeriodoService',
        'tipoGarantiaService',
        'tipoPagoService',
        'empleadoService',
        'clienteService',
        'vehiculoService',
        'avalService',
        'prestamoService',
        'acuerdoPagoService',
        'cuotaService',
        'PATH',
        'FileUploader',
        'NgTableParams',
        'creditoMenorService',
        'prestamoGarantiaService',
        'prestamoReferenciaService',
        'prestamoEstadoService',
    ];



    function DetalleCredMenorMovCrtl(
        $stateParams,
        $rootScope,
        $state,
        $filter,
        tipoPrestamoService,
        tipoMonedaService,
        tipoPeriodoService,
        tipoGarantiaService,
        tipoPagoService,
        empleadoService,
        clienteService,
        vehiculoService,
        avalService,
        prestamoService,
        acuerdoPagoService,
        cuotaService,
        PATH,
        FileUploader,
        NgTableParams,
        creditoMenorService,
        prestamoGarantiaService,
        prestamoReferenciaService,
        prestamoEstadoService
    ) {
        var vm = this;

        vm.prestamo_id = $stateParams.codigo;
        vm.data_list = [];

        vm.disabledInput = true;


        vm.msj = "";

        vm.formData = {
            tipo_prestamo: undefined,
            tipo_moneda: undefined,
            tipo_periodo: undefined,
            acuerdo_pago: undefined,
            tipo_pago: undefined,
            tipo_garantia: undefined,
            asesor_negocio: undefined,
            cliente: undefined,
            aval: undefined,
            interes: '',
            num_cuotas: '',
            mora: '',
            complacencia: '',
            pagos_parciales: 'NO',
            propietario: '',
            observacion: '',
            fecha_credito: null,
            fecha_desembolso: null,
            referencia_web: '',
            monto: '',
        };

        vm.tipo_prestamos   = [];
        vm.tipo_monedas     = [];
        vm.tipo_periodos    = [];
        vm.acuerdo_pagos    = [];
        vm.tipo_pagos       = [];
        vm.tipo_garantias   = [];
        vm.asesores_negocio = [];
        vm.clientes         = [];
        vm.data_vehiculos   = [];
        vm.data_avales      = [];



        //  DatePicker ui-b
        vm.popup = {
            opened: false,
        };
        vm.dateOptions = {
            startingDay: 1, // inicie en lunes
            showWeeks: 'false',
            minDate: null,
        };

        vm.openCalendar = function() {
            vm.popup.opened = true;
        };

        vm.popup2 = {
            opened: false,
        };
        vm.openCalendar2 = function() {
            vm.popup2.opened = true;
        };

        init();

        function init() {
            getPrestamoById();
            getTipoPrestamos();
            setTimeout(getClientesInfoBasica(), 1000);
            setTimeout(getTipoMonedas(), 1500);
            setTimeout(getTipoPeriodos(), 2000);
            setTimeout(getTipoGarantias(), 2500);
            // setTimeout(getAvalesInfoBasica(), 3000);

            setTimeout(getPretamoReferenciasByPrestamoId(), 3000);
            setTimeout(getPretamoGarantiasByPrestamoId(), 3500);

            setTimeout(getCuotasByPrestamoId(), 5000);
            setTimeout(getPrestamoEstadosInfo(), 6000);

        };

        //  carga de datos

        function getPrestamoById() {
            var params = {
                'prestamo_id': vm.prestamo_id,
            };
            prestamoService.getPrestamoById(params).then(
                function(response) {
                    if (!response.error) {
                        vm.data_list = response.data;

                        console.log(vm.data_list);


                        var estado = parseInt(vm.data_list.estado);
                        vm.disabledInputsEdit = true;
                        if (estado === 1) {
                            vm.disabledInputsEdit = false;
                        };

                        vm.formData.tipo_prestamo_id    = vm.data_list.tipo_prestamo_id;
                        vm.formData.tipo_pago_id        = vm.data_list.tipo_pago_id;
                        vm.formData.cliente_id          = vm.data_list.cliente_id;
                        vm.formData.tipo_moneda_id      = vm.data_list.tipo_moneda_id;
                        vm.formData.interes             = vm.data_list.tasa_interes;
                        vm.formData.tipo_periodo_id     = vm.data_list.tipo_periodo_id;
                        vm.formData.acuerdo_pago_id     = vm.data_list.acuerdo_pago_id;
                        vm.formData.num_cuotas          = vm.data_list.num_cuotas;
                        vm.formData.pagos_parciales     = vm.data_list.pagos_parciales;
                        vm.formData.propietario         = vm.data_list.propietario;
                        vm.formData.aval_id             = vm.data_list.aval_id;
                        vm.formData.observacion         = vm.data_list.observacion;
                        vm.formData.tipo_garantia_id    = vm.data_list.tipo_garantia_id;
                        vm.formData.fecha_credito       = (!vm.data_list.fecha_credito) ? null : new Date(vm.data_list.fecha_credito + ' 00:00:00');
                        vm.formData.fecha_desembolso    = (!vm.data_list.fecha_desembolso) ? null : new Date(vm.data_list.fecha_desembolso + ' 00:00:00');
                        vm.formData.fecha_prorrateo     = (!vm.data_list.fecha_prorrateo) ? null : new Date(vm.data_list.fecha_prorrateo + ' 00:00:00');
                        vm.formData.fecha_prorrateo_esp = (!vm.data_list.fecha_prorrateo_esp) ? null : new Date(vm.data_list.fecha_prorrateo_esp + ' 00:00:00');
                        vm.formData.monto               = vm.data_list.valor;
                        vm.formData.complacencia        = vm.data_list.complacencia;



                        getTipoPagosByTipoPrestamoId();
                        // vm.onSelectTipoPrestamo(null);
                        vm.getAcuerdoPagosByTipoPeriodoId(null, null);
                        return vm.tipo_prestamos;
                    }
                }
            );
        };
        //  carga de datos

        function getTipoPrestamos() {
            tipoPrestamoService.getTipoPrestamos().then(
                function(response) {
                    if (!response.error) {
                        vm.tipo_prestamos = response.data;
                        vm.formData.tipo_prestamo = getFill(vm.tipo_prestamos, 4);
                        getTipoPagosByTipoPrestamoId();
                        return vm.tipo_prestamos;
                    }
                }
            );
        };

        function getFill(data, idSelected) {
            var out = []
            for (var i in data) {
                if (parseInt(data[i].id) === parseInt(idSelected)) {
                    out = data[i];
                    return out;
                };
            };
            return out;
        };

        function getTipoMonedas() {
            tipoMonedaService.getTipoMonedas().then(
                function(response) {
                    if (!response.error) {
                        vm.tipo_monedas = response.data;
                        return vm.tipo_monedas;
                    }
                }
            );
        };

        function getTipoPeriodos() {
            tipoPeriodoService.getTipoPeriodos().then(
                function(response) {
                    if (!response.error) {
                        vm.tipo_periodos = response.data;
                        return vm.tipo_periodos;
                    }
                }
            );
        };

        // tipo-couta
        function getTipoPagosByTipoPrestamoId()
        {
            var data = {
                // 'tipo_prestamo_id' : vm.formData.tipo_prestamo.id,
                'tipo_prestamo_id': 4,
            };
            tipoPagoService.getTipoPagosByTipoPrestamoId(data).then(
                function(response) {
                    if (!response.error)
                    {
                        vm.tipo_pagos = response.data;
                        return vm.tipo_pagos;
                    }
                }
            );
        };

        vm.getAcuerdoPagosByTipoPeriodoId = function($item, $model) {
            var data = {
                'tipo_periodo_id': vm.formData.tipo_periodo_id,
            };

            acuerdoPagoService.getAcuerdoPagosByTipoPeriodoId(data).then(
                function(response) {
                    if (!response.error) {
                        vm.acuerdo_pagos = response.data;
                        return vm.acuerdo_pagos;
                    }
                }
            );
        };

        function getTipoGarantias() {
            tipoGarantiaService.getTipoGarantias().then(
                function(response) {
                    if (!response.error) {
                        vm.tipo_garantias = response.data;
                        return vm.tipo_garantias;
                    }
                }
            );
        };

        function getClientesInfoBasica() {
            var per_tipo = 0;
            var data = {
                'per_tipo': per_tipo,
            };

            clienteService.getClientesInfoBasica(data).then(
                function(response) {
                    console.info('cleintes', response);
                    if (!response.error) {
                        vm.clientes = response.data;
                    } else {
                        vm.msj = response.error;
                    }
                }
            );
        };

        function getPretamoReferenciasByPrestamoId() {
            var params = {
                'prestamo_id': vm.prestamo_id,
            };
            prestamoReferenciaService.getPretamoReferenciasByPrestamoId(params).then(
                function(response) {
                    if (!response.error) {
                        var data = response.data;
                        vm.data_referencias = filterItems(data, { 'tipo': 2 }) ;
                        vm.data_ref_web = filterItems(data, { 'tipo': 1 }) ;

                        return vm.data_referencias;
                    }
                }
            );
        };

        function getPretamoReferenciasByPrestamoIdTipo(tipo) {
            var params = {
                'prestamo_id': vm.prestamo_id,
                'tipo': tipo,
            };
            prestamoReferenciaService.getPretamoReferenciasByPrestamoIdTipo(params).then(
                function(response) {
                    if (!response.error) {
                        var data = response.data;
                        vm.data_referencias = filterItems(data, { 'tipo': 2 }) ;
                        return vm.data_referencias;
                    }
                }
            );
        };

        function getPretamoGarantiasByPrestamoId() {
            var params = {
                'prestamo_id': vm.prestamo_id,
            };
            prestamoGarantiaService.getPretamoGarantiasByPrestamoId(params).then(
                function(response) {
                    if (!response.error) {
                        vm.data_garantias = response.data;
                        console.info('garantias', vm.data_garantias);
                        return vm.data_garantias;
                    }
                }
            );
        };



        vm.reloadList = function() {
            var current = $state.current.name;
            var res = current.split(".", 2);
            var current_parent = res[0] + "." + res[1];

            var values = '';
            $state.go(current_parent, values);
            $rootScope.$broadcast("reloadList", values);
        };


        function filterItems(items, props) {
            var out = [];

            //  texto es el mismo que se buscara para todos de elementos (por ejemplo {name: "12", age: "12"})
            var keys = Object.keys(props);
            var firstElement = keys[0];

            var text = String(props[firstElement]).toLowerCase();
            // text diferente de nulo,  recoremos el array
            if (angular.isArray(items)) {
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    var itemMatches = false;
                    var prop = keys[0];
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                }

            } else {
                // Let the output be the input untouched
                out = items;
            }
            return out;
        };

         vm.data_list_coutas = [] ;
            function getCuotasByPrestamoId()
            {
                var data = {
                    'prestamo_id' : vm.prestamo_id ,
                } ;

                cuotaService.getCuotasByPrestamoId(data).then(
                    function(response){
                        console.log(response);
                        if (!response.error)
                        {
                            vm.data_list_coutas = response.data ;
                            return vm.data_list_coutas ;

                        }
                    }
                );
            };
            vm.data_estados_info = [] ;
            function getPrestamoEstadosInfo()
            {
                var params = {
                    'prestamo_id' : vm.prestamo_id ,
                } ;
                console.log(params);
                prestamoEstadoService.getPrestamoEstadosInfo(params).then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data_estados_info = response.data;
                            console.log(vm.data_estados_info);
                            return vm.data_estados_info ;
                        }
                    }
                );
            };



    };
})();
