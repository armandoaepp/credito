(function(){
    'use stric' ;

    angular.module('prestamos.mov.controller').controller('NewPrestamoMovCrtl', NewPrestamoMovCrtl) ;
    NewPrestamoMovCrtl.$inject = [
                                    '$rootScope',
                                    '$state',
                                    '$filter',
                                    'tipoPrestamoService',
                                    'tipoMonedaService',
                                    'tipoPeriodoService',
                                    'tipoGarantiaService',
                                    'tipoPagoService',
                                    'empleadoService',
                                    'clienteService',
                                    'vehiculoService',
                                    'avalService',
                                    'prestamoService',
                                    'acuerdoPagoService',
                                ] ;

        function NewPrestamoMovCrtl(
                $rootScope,
                $state,
                $filter,
                tipoPrestamoService,
                tipoMonedaService,
                tipoPeriodoService,
                tipoGarantiaService,
                tipoPagoService,
                empleadoService,
                clienteService,
                vehiculoService,
                avalService,
                prestamoService,
                acuerdoPagoService
            )
        {
            var vm =  this ;

            vm.msj = "";
            vm.formData = {
                tipo_prestamo : undefined,
                tipo_moneda : undefined,
                tipo_periodo : undefined,
                acuerdo_pago : undefined,
                tipo_pago : undefined,
                tipo_garantia : undefined,
                asesor_negocio : undefined,
                cliente : undefined,
                aval : undefined,
                interes : '',
                num_cuotas : '',
                mora : '',
                complacencia : '',
                pagos_parciales : 'NO',
                propietario : '',
                observacion :'' ,
                fecha_credito :null ,
                fecha_desembolso :null ,
                fecha_prorrateo :null ,
                fecha_prorrateo_esp :null ,
                check_prorrateo : false ,
                check_prorrateo_esp : false ,
            } ;

            vm.formData_ascveh = {
                costo : '',
                inicial_porcentaje : '',
                inicial_monto : '',
                seguro_tr : '',
                gps : '',
                soat : '',
                gas : '',
                otros : '',
                vehiculo_id : '',

            } ;

            vm.tipo_prestamos   = [] ;
            vm.tipo_monedas     = [] ;
            vm.tipo_periodos    = [] ;
            vm.acuerdo_pagos    = [] ;
            vm.tipo_pagos       = [] ;
            vm.tipo_garantias   = [] ;
            vm.asesores_negocio = [] ;
            vm.clientes = [] ;
            vm.data_vehiculos = [] ;
            vm.data_avales = [] ;

            // var view
                vm.ASCVEH = false ;

            //  DatePicker ui-b
                vm.popup = {
                    opened : false,
                } ;
                vm.dateOptions = {
                    startingDay: 1, // inicie en lunes
                    showWeeks:'false',
                    minDate: null,
                };

                vm.openCalendar = function() {
                    vm.popup.opened = true;
                };

                vm.popup2 = {
                    opened : false,
                } ;
                vm.openCalendar2 = function() {
                    vm.popup2.opened = true;
                };

                vm.popup3 = {
                    opened : false,
                } ;
                vm.openCalendar3 = function() {
                    vm.popup3.opened = true;
                };

                vm.popup4 = {
                    opened : false,
                } ;
                vm.openCalendar4 = function() {
                    vm.popup4.opened = true;
                };

            init();
            function init()
            {
                getTipoPrestamos();
                getClientesInfoBasica() ;
                // setTimeout(getEmpleadosInfoBasica(), 1000);
                setTimeout(getTipoMonedas(), 1500);
                setTimeout(getTipoPeriodos(), 2000);
                setTimeout(getTipoGarantias(), 2500);
                setTimeout(getAvalesInfoBasica(), 3000);

            };

            //  carga de datos

                function getTipoPrestamos()
                {
                    tipoPrestamoService.getTipoPrestamos().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_prestamos = response.data;
                                vm.formData.tipo_prestamo =  getFill(vm.tipo_prestamos, 1) ;
                                setTimeout(vm.onSelectTipoPrestamo(), 1000);
                                return vm.tipo_prestamos ;
                            }
                        }
                    );
                };

                function getFill(data, idSelected)
                {
                    var out = []
                    for(var i in data)
                    {
                        if(parseInt(data[i].id) === parseInt(idSelected))
                        {
                            out = data[i] ;
                            return out;
                        };
                    } ;
                    return out;
                } ;

                function getTipoMonedas()
                {
                    tipoMonedaService.getTipoMonedas().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_monedas = response.data;
                                return vm.tipo_monedas ;
                            }
                        }
                    );
                };

                function getTipoPeriodos()
                {
                    tipoPeriodoService.getTipoPeriodos().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_periodos = response.data;
                                return vm.tipo_periodos ;
                            }
                        }
                    );
                };

                function getTipoPagosByTipoPrestamoId()
                {
                    var data = {
                        'tipo_prestamo_id' : vm.formData.tipo_prestamo.id,
                    };
                    tipoPagoService.getTipoPagosByTipoPrestamoId(data).then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_pagos = response.data;
                                return vm.tipo_pagos ;
                            }
                        }
                    );
                };

               vm.getAcuerdoPagosByTipoPeriodoId =  function($item, $model)
                {
                    var data = {
                        'tipo_periodo_id' : vm.formData.tipo_periodo.id,
                    };

                    acuerdoPagoService.getAcuerdoPagosByTipoPeriodoId(data).then(
                        function(response){


                            if (!response.error)
                            {
                                vm.acuerdo_pagos = response.data;
                                return vm.acuerdo_pagos ;
                            }
                        }
                    );
                };

                function getTipoGarantias()
                {
                    tipoGarantiaService.getTipoGarantias().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_garantias = response.data;
                                return vm.tipo_garantias ;
                            }
                        }
                    );
                };

                function getClientesInfoBasica()
                {
                    var per_tipo = 0 ;
                    var data = {
                        'per_tipo' : per_tipo,
                    } ;

                    clienteService.getClientesInfoBasica(data).then(
                        function(response)
                        {
                            console.log(response);
                            if (!response.error)
                            {
                                vm.clientes = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );
                } ;

                function getVehiculosForSelect()
                {
                    vehiculoService.getVehiculosForSelect().then(
                        function(response)
                        {
                            console.log(response);
                            if (!response.error)
                            {
                                vm.data_vehiculos = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );

                } ;

                function getAvalesInfoBasica()
                {
                    avalService.getAvalesInfoBasica().then(
                        function(response)
                        {
                            console.log('avales');
                            console.log(response);
                            if (!response.error)
                            {
                                var data = response.data ;


                                vm.data_avales = [{
                                    'full_name' : 'Sin Aval' ,
                                    'dni' : null ,
                                    'id' : null ,
                                    'per_apellidos' : null ,
                                    'per_natural_id' : null ,
                                    'per_nombre' : null ,
                                    'persona_id' : null ,
                                 }].concat(data)  ;

                                // vm.data_avales = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );
                } ;

                // seleccionar un ITEM del un combo
                function selecterItemCombo(data, idSeleccionar)
                {
                    // var data = vm.tipo_garantias ;
                    var idSelected = parseInt(idSeleccionar);
                    var out = [] ;
                    for(var i in data)
                    {
                        var id =  parseInt(data[i].id);
                        if( id === idSelected)
                        {
                           out = data[i] ;
                           break ;
                        }

                    }
                    return out ;
                };

            vm.onSelectTipoPrestamo = function($item, $model)
            {
                vm.ASCVEH = false ;
                // var tipo_prestamo_id = parseInt($item.id) ;
                var tipo_prestamo_id = parseInt(vm.formData.tipo_prestamo.id) ;
                vm.activeTipoGarantia = false ;
                if (tipo_prestamo_id === 1)
                {
                    vm.ASCVEH = true ;
                    getVehiculosForSelect();
                    // seleccionar el item Vehicular
                    vm.formData.tipo_garantia = selecterItemCombo(vm.tipo_garantias, 1) ;
                    vm.activeTipoGarantia = true ;
                };
                getTipoPagosByTipoPrestamoId() ;
            } ;

            // ASCVEH

                vm.inciarParamsAscveh = inciarParamsAscveh ;
                function inciarParamsAscveh(params)
                {
                    var costo    = vm.formData_ascveh.costo ;
                    var inicial_porcentaje = vm.formData_ascveh.inicial_porcentaje ;
                    var inicial_monto      = vm.formData_ascveh.inicial_monto ;
                    // vm.formData_ascveh.inicial_monto = parseFloat(inicial_monto).toFixed(2);

                    if (inicial_porcentaje === '' || inicial_porcentaje === undefined)
                    return;

                    if (params === 1 )
                    {
                        var calculo_monto = ( inicial_porcentaje * costo ) / 100 ;
                        vm.formData_ascveh.inicial_monto = calculo_monto;
                        // vm.formData_ascveh.inicial_monto = parseFloat(calculo_monto).toFixed(2);
                    }else  if (params === 2 )
                    {
                        var inicial_porcentaje_ = ( inicial_monto / costo ) * 100  ;
                        vm.formData_ascveh.inicial_porcentaje =  inicial_porcentaje_;
                        // vm.formData_ascveh.inicial_porcentaje =  parseFloat(inicial_porcentaje_).toFixed(2); ;
                    };

                } ;

                function saveAscVeh()
                {

                    var valor                = vm.formData_ascveh.costo ;
                    var inicial_porcentaje   = vm.formData_ascveh.inicial_porcentaje ;
                    var inicial_monto        = vm.formData_ascveh.inicial_monto ;
                    var seguro_tr            = vm.formData_ascveh.seguro_tr ;
                    var gps                  = vm.formData_ascveh.gps ;
                    var soat                 = vm.formData_ascveh.soat ;
                    var gas                  = vm.formData_ascveh.gas ;
                    var otros                = vm.formData_ascveh.otros ;
                    var vehiculo_id          = vm.formData_ascveh.vehiculo_id ;
                    var tasa_interes         = vm.formData.interes ;
                    var num_cuotas           = vm.formData.num_cuotas ;
                    var mora                 = vm.formData.mora ;
                    var complacencia         = vm.formData.complacencia ;
                    var pagos_parciales      = vm.formData.pagos_parciales ;
                    var propietario          = vm.formData.propietario ;
                    var observacion          = vm.formData.observacion ;
                    var tipo_prestamo_id     = vm.formData.tipo_prestamo.id ;
                    var tipo_moneda_id       = vm.formData.tipo_moneda.id ;
                    var tipo_periodo_id      = vm.formData.tipo_periodo.id ;
                    var tipo_pago_id         = vm.formData.tipo_pago.id ;
                    var tipo_garantia_id     = vm.formData.tipo_garantia.id ;
                    var cliente_id           = vm.formData.cliente.id ;
                    var aval_id              = vm.formData.aval.id ;
                    var acuerdo_pago_id      = vm.formData.acuerdo_pago.id ;
                    var fecha_credito        = vm.formData.fecha_credito ;
                    var fecha_desembolso     = vm.formData.fecha_desembolso ;
                    var fecha_prorrateo      = vm.formData.fecha_prorrateo ;
                    var fecha_prorrateo_esp  = vm.formData.fecha_prorrateo_esp ;

                    var check_prorrateo      = vm.formData.check_prorrateo ;
                    var check_prorrateo_esp  = vm.formData.check_prorrateo_esp ;


                    vm.msj = {message : ''} ;

                    // VALIDACION
                        if (tipo_prestamo_id === undefined)
                        {
                            return  vm.msj.message = 'Seleccionar Tipo Prestramo' ;
                        };

                        if (cliente_id === undefined)
                        {
                            return  vm.msj.message = 'Seleccionar Cliente' ;
                        };

                        if (fecha_desembolso === undefined)
                        {
                            return  vm.msj.message = 'Ingresar Fecha Desembolso' ;
                        };

                        if (fecha_credito === undefined)
                        {
                            return  vm.msj.message = 'Ingresar Fecha Desembolso' ;
                        };

                        if (tipo_moneda_id === undefined)
                        {
                            return  vm.msj.message = 'Seleccionar Tipo de Moneda' ;
                        };

                        if (tipo_prestamo_id === 1)
                        {
                            if (valor === undefined)
                            {
                                return  vm.msj.message = 'Ingrese Costo del Vehiculo' ;
                            };

                            if (vehiculo_id === undefined)
                            {
                                return  vm.msj.message = 'Seleccionar Vehiculo' ;
                            };

                        } ;

                        if (tasa_interes === undefined)
                        {
                            return  vm.msj.message = 'Ingresar Tasa de Interes' ;
                        };

                        if (tipo_periodo_id === undefined)
                        {
                            return  vm.msj.message = 'Seleccionar Periodo de Cuotas' ;
                        };

                        if (num_cuotas === undefined)
                        {
                            return  vm.msj.message = 'Ingresar Número de Cuotas' ;
                        };

                    var fecha_credito = $filter('date')(vm.formData.fecha_credito,'yyyy-MM-dd');
                    // vm.formData.fecha_credito = fecha_credito ;

                    var fecha_desembolso = $filter('date')(vm.formData.fecha_desembolso,'yyyy-MM-dd');
                    // vm.formData.fecha_desembolso = fecha_desembolso ;

                    var fecha_prorrateo = $filter('date')(vm.formData.fecha_prorrateo,'yyyy-MM-dd');

                    var fecha_prorrateo_esp = $filter('date')(vm.formData.fecha_prorrateo_esp,'yyyy-MM-dd');

                    var data = {
                      'valor' : valor,
                      'inicial_porcentaje' : inicial_porcentaje,
                      'inicial_monto' : inicial_monto,
                      'seguro_tr' : seguro_tr,
                      'gps' : gps,
                      'soat' : soat,
                      'gas' : gas,
                      'otros' : otros,
                      'vehiculo_id' : vehiculo_id,
                      'tasa_interes' : tasa_interes,
                      'num_cuotas' : num_cuotas,
                      'mora' : mora,
                      'complacencia' : complacencia,
                      'pagos_parciales' : pagos_parciales,
                      'propietario' : propietario,
                      'observacion' : observacion,
                      'tipo_prestamo_id' : tipo_prestamo_id,
                      'tipo_moneda_id' : tipo_moneda_id,
                      'tipo_periodo_id' : tipo_periodo_id,
                      'tipo_pago_id' : tipo_pago_id,
                      'tipo_garantia_id' : tipo_garantia_id,
                      'cliente_id' : cliente_id,
                      'aval_id' : aval_id,
                      'acuerdo_pago_id' : acuerdo_pago_id,
                      'fecha_credito' : fecha_credito,
                      'fecha_desembolso' : fecha_desembolso,
                      'fecha_prorrateo' : fecha_prorrateo,
                      'fecha_prorrateo_esp' : fecha_prorrateo_esp,
                      'check_prorrateo' : check_prorrateo,
                      'check_prorrateo_esp' : check_prorrateo_esp,

                    };
                    console.log(data);
                    prestamoService.save(data).then(
                        function(response)
                        {
                            console.log(response);
                            if (!response.error)
                            {
                                 vm.reloadListPrestamos();
                                // vm.data_avales = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );



                };


            vm.ok = function ()
            {
                saveAscVeh();
               /* var data = {
                    'nombre'       : vm.formData.nombre,
                };

                rolService.saveRol(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );*/

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };

            vm.reloadListPrestamos = function()
            {
                var current = $state.current.name ;
                var res = current.split(".",2);
                var current_parent = res[0]+"."+res[1];

                var values = '' ;
                $state.go(current_parent, values);
                $rootScope.$broadcast("reloadListPrestamos", values);
            }
        };
})() ;