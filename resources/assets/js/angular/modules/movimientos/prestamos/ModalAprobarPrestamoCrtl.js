(function(){
    'use stric' ;

    angular.module('prestamos.mov.controller').controller('ModalAprobarPrestamoCrtl', ModalAprobarPrestamoCrtl) ;
    ModalAprobarPrestamoCrtl.$inject = ['$uibModalInstance', 'data_prestamo','prestamoService'] ;

        function ModalAprobarPrestamoCrtl($uibModalInstance,data_prestamo, prestamoService)
        {
            var vm =  this ;

            vm.msj = "";
            vm.fillSelected = data_prestamo ;

            vm.formData = {
                mora : '',
            }

            vm.ok = function ()
            {
                var params = {
                    'mora' : vm.formData.mora,
                    'tipo_estado_id' : 2,
                    'prestamo_id' : vm.fillSelected.id,
                };
                // return
                // console.log(params);
                prestamoService.updateEstadoPrestamoTipoEstado(params).then(
                    function(response)
                    {
                        console.log(response);

                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;