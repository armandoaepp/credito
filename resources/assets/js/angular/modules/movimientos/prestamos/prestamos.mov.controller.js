(function(){
    'use strict';

    angular.module('prestamos.mov.controller').controller('PrestamosMovCtrl',PrestamosMovCtrl);
    PrestamosMovCtrl.$inject = ['$rootScope','botoneraFactory','$filter','$state','prestamoService', '$uibModal', 'PATH', 'modalService','NgTableParams','tipoEstadoService'] ;

        function PrestamosMovCtrl($rootScope,botoneraFactory,$filter,$state,prestamoService, $uibModal, PATH, modalService,NgTableParams,tipoEstadoService)
        {
            var vm = this ;

            // function
                vm.getPrestamos        = getPrestamos ;
                vm.onClick             = onClick ;
                vm.newPrestamo         = newPrestamo ;
                vm.detallePrestamo     = detallePrestamo ;
                vm.editPrestamo        = editPrestamo ;
                vm.aprobarPrestamo = aprobarPrestamo ;
                vm.cancelarPrestamo = cancelarPrestamo ;

            // variables
                vm.botones      = [] ;
                vm.fillSelected = [] ;
                vm.data_list    = [] ;
                vm.data_list_2  = [] ;
                vm.data_list_3  = [] ;
                vm.data_list_4  = [] ;
                vm.fillSelected = [] ;
                vm.data_tipo_estados = [] ;

                vm.btn_in_table = false ;
                vm.btn_edit     = false ;
                vm.btn_delete   = false ;

            init();
            function init() {
                tablePlugin() ;
                vm.getPrestamos(null) ;
                getTipoEstados() ;
            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getPrestamos(1);
                }
                else if (name === 'new')
                {
                      vm.newPrestamo('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editPrestamo() ;
                }
                else if (name === 'delete')
                {
                    vm.deletePrestamo() ;
                }
                else if (name === 'detail')
                {
                    vm.detallePrestamo() ;
                }
                else if (name === 'aprobar')
                {
                    vm.aprobarPrestamo() ;
                }
                else if (name === 'cancelar')
                {
                    vm.cancelarPrestamo() ;
                }
                else{
                    return ;
                };

            } ;



            function getPrestamos(tipo_estado_id)
            {
                var params = {
                        'tipo_estado_id' : tipo_estado_id ,
                         'tipo_prestamo_id' : 1 ,
                    } ;

                prestamoService.getPrestamosByTipoEstadoId(params).then(
                    function(response){
                        if (!response.error)
                        {
                            // botonesInTable() ;
                            var data = response.data;
                            vm.data_list = data ;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );

                vm.fillSelected = [] ;
            };

            function getTipoEstados()
            {

                tipoEstadoService.getTipoEstados().then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data_tipo_estados = response.data;

                             vm.data_search_tipo_estados = [{
                                "id": 0,
                                "descripcion": "Todos",
                                "estado": null,
                             }].concat(vm.data_tipo_estados)  ;

                            return vm.data_tipo_estados ;
                        }
                    }
                );
            };


            // ===== ng-table ==============================================================================================


                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                };

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                };

                function del(row) {
                    modalConfirm(row);
                };

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'rol_id' : row.id,
                        'nombre' : row.nombre,
                    };

                    prestamoService.updateRol(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New
                function newPrestamo (size)
                {
                     $state.go('operaciones.prestamos.nuevo', { })

                      /*  var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/accesos/roles/nuevo.rol.tpl.html',
                            controller: 'ModalNewPrestamoCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getPrestamos(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );*/

                };

                 // edit
                function editPrestamo (size)
                {
                    var codigo = vm.fillSelected.id ;

                    if (codigo === undefined) {
                        return modalAlert()
                    }
                    else
                    {
                        $state.go($state.current.name+'.edit', { codigo: codigo })
                    }

                };

            //  detalle de prestamo
                function detallePrestamo (size)
                {
                    var codigo = vm.fillSelected.id ;

                    if (codigo === undefined) {
                        return modalAlert()
                    }
                    else
                    {
                        $state.go($state.current.name+'.detalle', { codigo: codigo })
                        // $state.go('personas.personas.editar', { codigo: codigo })
                    }
                };

            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        prestamoService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getPrestamos() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Prestamo',
                        bodyText: '¿Esta Seguro de Eliminar Prestamo: '+ row.data+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

                $rootScope.$on("reloadListPrestamos", function(event, values) {
                    // console.log('reloadListPersona');
                       vm.getPrestamos();
                  });


            // aprobar
                function aprobarPrestamo (size)
                {
                    var codigo = vm.fillSelected.id ;
                    var estado = vm.fillSelected.estado ;

                        if (codigo === undefined) {
                            return modalAlert()
                        } ;
                        if (estado > 1){
                            return modalInfo()
                        } ;

                        var path = PATH.MOVIMIENTOS ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/operaciones/prestamos/modal.aprobar.tpl.html',
                            controller: 'ModalAprobarPrestamoCrtl',
                            controllerAs: 'vm',
                            size: size,
                            backdrop : 'static',
                            resolve: {
                                data_prestamo: function () { return vm.fillSelected; },
                            }

                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                init(data) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

            // cancelar
                function cancelarPrestamo (size)
                {
                    var codigo = vm.fillSelected.id ;
                    var estado = vm.fillSelected.estado ;

                        if (codigo === undefined) {
                            return modalAlert()
                        } ;

                        if (estado > 1){
                            return modalInfo()
                        } ;


                        var path = PATH.MOVIMIENTOS ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/operaciones/prestamos/modal.cancelar.tpl.html',
                            controller: 'ModalCancelarPrestamoCrtl',
                            controllerAs: 'vm',
                            size: size,
                            backdrop : 'static',
                            resolve: {
                                data_prestamo: function () { return vm.fillSelected; },
                            }

                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                init(data) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

            //modal info
                function modalInfo()
                {
                    var modalOptions = {
                        headerText: 'Prestamo',
                        bodyText: 'Operacion no Permitida, ¡¡¡ VER DETALLE o ESTADO PRESTAMO !!! '
                    };

                    modalService.showModalAlert({}, modalOptions).then(function (result){
                    });
                }

        }
})() ;

