(function(){
    'use strict';

    angular.module('claseVehiculos.info.controller').controller('ClaseVehiculosInfoCtrl',ClaseVehiculosInfoCtrl);
    ClaseVehiculosInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','claseVehiculoService'] ;

        function ClaseVehiculosInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,claseVehiculoService)
        {
            var vm = this ;

            // function
                vm.getClaseVehiculos = getClaseVehiculos ;
                vm.onClick           = onClick ;
                vm.newTipoVehiculo   = newTipoVehiculo ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                vm.getClaseVehiculos() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getClaseVehiculos();
                }
                else if (name === 'new')
                {
                      vm.newTipoVehiculo('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }

                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            }

            function getClaseVehiculos()
            {
                claseVehiculoService.getClaseVehiculos().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                            sorting: { nombre: "asc" }
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'clase_vehiculo_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                    };

                    claseVehiculoService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getClaseVehiculos() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                } ;

               /* function applySelectedSort(){
                  vm.tableParams.sorting('clase_vehiculo_id',  'desc');
                }*/

            // ===================================================================================================

            //  New rol
                function newTipoVehiculo (size)
                {
                   /* console.log(vm.tableParams.page());
                    console.log(vm.tableParams.count());
                    console.log(vm.tableParams.total());
                    console.log(last_page);*/
                    var last_page = Math.ceil(vm.tableParams.total() / vm.tableParams.count()) ;

                    // pages.length
                        vm.tableParams.page( last_page );
                        vm.tableParams.reload();
                        vm.tableParams.sorting({}) ;

                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/vehiculos/clase-vehiculos/new.clase-vehiculo.tpl.html',
                            controller: 'ModalNewClaseVehiculoCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getClaseVehiculos(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        claseVehiculoService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getClaseVehiculos() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    // console.log(row) ;
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Clase de Vehiculo',
                        bodyText: '¿Esta Seguro de Eliminar Clase de Vehiculo: '+ row.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;

