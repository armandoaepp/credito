(function(){
    'use stric' ;

    angular.module('claseVehiculos.info.controller').controller('ModalNewClaseVehiculoCrtl', ModalNewClaseVehiculoCrtl) ;
    ModalNewClaseVehiculoCrtl.$inject = ['$uibModalInstance', 'claseVehiculoService'] ;

        function ModalNewClaseVehiculoCrtl($uibModalInstance, claseVehiculoService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                descripcion : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                };

                claseVehiculoService.save(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;