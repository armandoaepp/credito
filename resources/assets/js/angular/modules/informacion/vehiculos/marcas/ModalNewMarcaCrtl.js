(function(){
    'use stric' ;

    angular.module('marcas.info.controller').controller('ModalNewMarcaCrtl', ModalNewMarcaCrtl) ;
    ModalNewMarcaCrtl.$inject = ['$uibModalInstance', 'marcaService'] ;

        function ModalNewMarcaCrtl($uibModalInstance, marcaService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                descripcion : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                };

                marcaService.save(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;