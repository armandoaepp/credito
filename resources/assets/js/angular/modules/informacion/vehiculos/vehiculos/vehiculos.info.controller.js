(function(){
    'use strict';

    angular.module('vehiculos.info.controller').controller('VehiculosInfoCtrl',VehiculosInfoCtrl);
    VehiculosInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','vehiculoService'] ;

        function VehiculosInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,vehiculoService)
        {
            var vm = this ;

            // function
                vm.getVehiculos = getVehiculos ;
                vm.onClick      = onClick ;
                vm.newVehiculo  = newVehiculo ;
                vm.editVehiculo = editVehiculo ;
                vm.deleteVehiculo = deleteVehiculo ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

            init();
            function init() {
                tablePlugin() ;
                vm.getVehiculos() ;
            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getVehiculos();
                }
                else if (name === 'new')
                {
                      vm.newVehiculo('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editVehiculo();
                }
                else if (name === 'delete')
                {
                      vm.deleteVehiculo();
                }


                else{
                    return ;
                };
            } ;

            function getVehiculos()
            {
                vehiculoService.getVehiculos().then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data_list = response.data;
                            console.log(vm.data_list);
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            // ===== ng-table ==============================================================================================
                // vm.cancel = cancel;
                // vm.del    = del;
                // vm.save   = save;
                // vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                            sorting: { nombre: "asc" }
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                /*function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'marca_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                    };

                    vehiculoService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getVehiculos() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }*/

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                } ;

            // ===================================================================================================

            //  New
                function newVehiculo (size)
                {
                        var last_page = Math.ceil(vm.tableParams.total() / vm.tableParams.count()) ;
                        vm.tableParams.page( last_page );
                        vm.tableParams.reload();
                        vm.tableParams.sorting({}) ;

                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/vehiculos/vehiculos/new.vehiculo.tpl.html',
                            controller: 'ModalNewVehiculoCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getVehiculos(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

            //  Editar

                function editVehiculo()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalEditVehiculo('md') ;
                    }
                };

                function modalEditVehiculo(size)
                {
                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/vehiculos/vehiculos/edit.vehiculo.tpl.html',
                            controller: 'ModalEditVehiculoCrtl',
                            controllerAs: 'vm',
                            size: size,
                             backdrop : 'static',
                            resolve: {
                                data_vehiculo: function () { return vm.fillSelected; },
                            }
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getVehiculos() ;
                            },
                            function ()
                            {
                                console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete

                function deleteVehiculo()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalConfirm() ;
                    }

                }

                function confirmDelete()
                {
                     var data = {
                            'codigo'    : vm.fillSelected.id,
                            'estado'    : 0,
                        };
                        // console.log(data);
                        vehiculoService.updateEstado(data).then(
                            function(response){

                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getVehiculos() ;
                                    return response.data;
                                }
                            }
                        );
                };

                function modalConfirm()
                {
                    // var dato = row.placa + ' '+ row.serie + ' '+row.tipo_Vehiculo_nombre ;
                    var dato = vm.fillSelected.placa+ ' / ' + vm.fillSelected.modelo+ ' / ' + vm.fillSelected.tipo_vehiculo ;

                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Vehículo',
                        bodyText: '¿Esta Seguro de Eliminar Vehículo: '+ dato+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete();
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;

