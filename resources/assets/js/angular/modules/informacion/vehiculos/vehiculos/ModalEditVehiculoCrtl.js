(function(){
    'use stric' ;

    angular.module('vehiculos.info.controller').controller('ModalEditVehiculoCrtl', ModalEditVehiculoCrtl) ;
    ModalEditVehiculoCrtl.$inject = [
                                        '$uibModalInstance',
                                        'data_vehiculo',
                                        'vehiculoService',
                                        'claseVehiculoService',
                                        'tipoVehiculoService',
                                        'marcaService',
                                        'modeloService',
                                    ] ;

        function ModalEditVehiculoCrtl(
                $uibModalInstance,
                data_vehiculo,
                vehiculoService,
                claseVehiculoService,
                tipoVehiculoService,
                marcaService,
                modeloService )
        {
            var vm =  this ;

            vm.msj = "";
            vm.fillSelected   = data_vehiculo ;
            console.log(vm.fillSelected);

            vm.clase_vehiculo_id = vm.fillSelected.clase_vehiculo_id ;
            vm.tipo_vehiculo_id  = vm.fillSelected.tipo_vehiculo_id ;
            vm.marca_id          = vm.fillSelected.marca_id ;
            vm.modelo_id         = vm.fillSelected.modelo_id ;

            vm.formData = {
                placa : vm.fillSelected.placa,
                serie : vm.fillSelected.serie,
                descripcion : vm.fillSelected.descripcion,
                clase_vehiculo : [],
                tipo_vehiculo : [],
                marca : [],
                modelo : [],
            };

            vm.data_clase_vehiculos = [] ;
            vm.data_tipo_vehiculos  = [] ;
            vm.data_marcas          = [] ;
            vm.data_modelos         = [] ;

            vm.getClaseVehiculos = getClaseVehiculos ;
            vm.getTipoVehiculos  = getTipoVehiculos ;
            vm.getMarcas         = getMarcas ;
            vm.getModelos        = getModelos ;

            init();
            function init()
            {
                vm.getClaseVehiculos();
                vm.getMarcas();
                setTimeout(getMarcas(), 500);
                setTimeout( vm.getTipoVehiculos(undefined,undefined), 750);
                setTimeout(vm.getModelos(undefined,undefined), 1000);


            } ;

            function getClaseVehiculos()
            {
                claseVehiculoService.getClaseVehiculos().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_clase_vehiculos = response.data ;
                            vm.formData.clase_vehiculo = selectItemCombo(vm.data_clase_vehiculos,vm.clase_vehiculo_id ) ;
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );
            };

            function getTipoVehiculos($item,$model)
            {
                var params = { 'clase_vehiculo_id' : ''} ;
                if ($item === undefined)
                {
                    params.clase_vehiculo_id =  vm.clase_vehiculo_id ;
                }else{
                    params.clase_vehiculo_id =  vm.formData.clase_vehiculo.id ;
                } ;

                tipoVehiculoService.getTipoVehiculosByClaseVehiculoId(params).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_tipo_vehiculos = response.data ;
                            if (vm.tipo_vehiculo_id !== undefined)
                            {
                                vm.formData.tipo_vehiculo = selectItemCombo(vm.data_tipo_vehiculos,vm.tipo_vehiculo_id ) ;
                                vm.tipo_vehiculo_id = [];

                            };
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );
            };

            function getMarcas()
            {
                marcaService.getMarcas().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_marcas = response.data ;
                            vm.formData.marca = selectItemCombo(vm.data_marcas,vm.marca_id ) ;
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );
            };

            function getModelos($item,$model)
            {
                var params = { 'marca_id' : ''} ;
                if ($item === undefined)
                {
                    params.marca_id =  vm.marca_id ;
                }else{
                    params.marca_id =  vm.formData.marca.id ;
                } ;

                modeloService.getModelosByMarcaId(params).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_modelos = response.data ;
                            if (vm.modelo_id !== undefined)
                            {
                                vm.formData.modelo = selectItemCombo(vm.data_modelos,vm.modelo_id ) ;
                                vm.modelo_id = [];
                            };
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );
            };

            // seleccionar un ITEM del un combo
            function selectItemCombo(data, id_select)
            {
                var idSelected = parseInt(id_select);
                var out = [] ;
                for(var i in data)
                {
                    var id =  parseInt(data[i].id);
                    if( id === idSelected)
                    {
                       out = data[i] ;
                       break ;
                    }
                }
                return out ;
            };


            vm.ok = function ()
            {
                    var vehiculo_id      = vm.fillSelected.id;
                    var placa            = vm.formData.placa ;
                    var serie            = vm.formData.serie ;
                    var descripcion      = vm.formData.descripcion ;
                    var tipo_vehiculo_id = vm.formData.tipo_vehiculo.id ;
                    var modelo_id        = vm.formData.modelo.id ;

                    // var clase_vehiculo = vm.formData.clase_vehiculo.id ;
                    // var marca          = vm.formData.marca.id ;

                    vm.msj = {message : ''} ;

                    if (placa === undefined)
                    {
                        return  vm.msj.message = 'Ingrese Placa' ;
                    };
                    if (serie === undefined)
                    {
                        return  vm.msj.message = 'Ingrese Serie' ;
                    };
                    if (tipo_vehiculo_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Tipo Vehiculo' ;
                    };
                    if (modelo_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Modelo' ;
                    };

                    var params = {
                      'vehiculo_id' : vehiculo_id,
                      'placa' : placa,
                      'serie' : serie,
                      'descripcion' : descripcion,
                      'tipo_vehiculo_id' : tipo_vehiculo_id,
                      'modelo_id' : modelo_id,
                    };

                vehiculoService.update(params).then(
                    function(response)
                    {
                        console.log(response);
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;