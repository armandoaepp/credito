(function(){
    'use stric' ;

    angular.module('vehiculos.info.controller').controller('ModalNewVehiculoCrtl', ModalNewVehiculoCrtl) ;
    ModalNewVehiculoCrtl.$inject = [
                                        '$uibModalInstance',
                                        'vehiculoService',
                                        'claseVehiculoService',
                                        'tipoVehiculoService',
                                        'marcaService',
                                        'modeloService',
                                    ] ;

        function ModalNewVehiculoCrtl(
                $uibModalInstance,
                vehiculoService,
                claseVehiculoService,
                tipoVehiculoService,
                marcaService,
                modeloService )
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                placa : '',
                serie : '',
                descripcion : '',
                clase_vehiculo : [],
                tipo_vehiculo : [],
                marca : [],
                modelo : [],
            };

            vm.data_clase_vehiculos = [] ;
            vm.data_tipo_vehiculos  = [] ;
            vm.data_marcas          = [] ;
            vm.data_modelos         = [] ;

            vm.getClaseVehiculos = getClaseVehiculos ;
            vm.getTipoVehiculos  = getTipoVehiculos ;
            vm.getMarcas         = getMarcas ;
            vm.getModelos        = getModelos ;

            init();
            function init()
            {
                vm.getClaseVehiculos() ;
                vm.getMarcas() ;

            }

            function getClaseVehiculos()
            {
                claseVehiculoService.getClaseVehiculos().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_clase_vehiculos = response.data ;
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );
            };

            function getTipoVehiculos($item,$model)
            {
                // console.log($item);
                // console.log($model);
                vm.formData.tipo_vehiculo = [] ;
                var params = { 'clase_vehiculo_id' : vm.formData.clase_vehiculo.id} ;

                tipoVehiculoService.getTipoVehiculosByClaseVehiculoId(params).then(
                    function(response)
                    {
                        console.log(response.data);
                        if (!response.error)
                        {
                            vm.data_tipo_vehiculos = response.data ;
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );
            };

            function getMarcas()
            {
                marcaService.getMarcas().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_marcas = response.data ;
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );
            };

            function getModelos($item,$model)
            {
                vm.formData.modelo = [] ;
                var params = { 'marca_id' : vm.formData.marca.id} ;
                modeloService.getModelos(params).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_modelos = response.data ;
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );
            };


            vm.ok = function ()
            {
                    var placa            = vm.formData.placa ;
                    var serie            = vm.formData.serie ;
                    var descripcion      = vm.formData.descripcion ;
                    var tipo_vehiculo_id = vm.formData.tipo_vehiculo.id ;
                    var modelo_id        = vm.formData.modelo.id ;

                    // var clase_vehiculo = vm.formData.clase_vehiculo.id ;
                    // var marca          = vm.formData.marca.id ;

                    vm.msj = {message : ''} ;

                    if (placa === undefined)
                    {
                        return  vm.msj.message = 'Ingrese Placa' ;
                    };
                    if (serie === undefined)
                    {
                        return  vm.msj.message = 'Ingrese Serie' ;
                    };
                    if (tipo_vehiculo_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Tipo Vehiculo' ;
                    };
                    if (modelo_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Modelo' ;
                    };

                    var params = {
                      'placa' : placa,
                      'serie' : serie,
                      'descripcion' : descripcion,
                      'tipo_vehiculo_id' : tipo_vehiculo_id,
                      'modelo_id' : modelo_id,
                    };


                vehiculoService.save(params).then(
                    function(response)
                    {
                        console.log(response);
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;