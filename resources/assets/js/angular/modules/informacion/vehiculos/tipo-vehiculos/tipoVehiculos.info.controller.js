(function(){
    'use strict';

    angular.module('tipoVehiculos.info.controller').controller('TipoVehiculosInfoCtrl',TipoVehiculosInfoCtrl);
    TipoVehiculosInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','tipoVehiculoService','claseVehiculoService'] ;

        function TipoVehiculosInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,tipoVehiculoService,claseVehiculoService)
        {

            var vm = this ;

            // function
                vm.getTipoVehiculos = getTipoVehiculos ;
                vm.onClick          = onClick ;
                vm.newTipoVehiculo  = newTipoVehiculo ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;
                vm.data_tipo_vehiculos = [];

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                getClaseVehiculos() ;
                vm.getTipoVehiculos() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getTipoVehiculos();
                }
                else if (name === 'new')
                {
                      vm.newTipoVehiculo('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }
                else if (name === 'delete')
                {
                    deleteRol() ;
                }
                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            }

            function getTipoVehiculos()
            {
                tipoVehiculoService.getTipoVehiculos().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            function getClaseVehiculos()
            {
                claseVehiculoService.getClaseVehiculos().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_tipo_vehiculos = response.data ;
                            // console.log(vm.data_areas );
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            }


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'tipo_vehiculo_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                        'clase_vehiculo_id' : row.clase_vehiculo.id,
                    };

                    tipoVehiculoService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getTipoVehiculos() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newTipoVehiculo (size)
                {
                        var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/vehiculos/tipo-vehiculos/new.tipo-vehiculo.tpl.html',
                            controller: 'ModalNewTipoVehiculoCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getTipoVehiculos(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        tipoVehiculoService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getTipoVehiculos() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Tipo de Vehiculo',
                        bodyText: '¿Esta Seguro de Eliminar Tipo de Vehiculo: '+ row.nombre +' de la Clase : '+row.clase_vehiculo.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }
        }
})() ;

