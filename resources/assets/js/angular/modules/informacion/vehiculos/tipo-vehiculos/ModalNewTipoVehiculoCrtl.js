(function(){
    'use stric' ;

    angular.module('tipoVehiculos.info.controller').controller('ModalNewTipoVehiculoCrtl', ModalNewTipoVehiculoCrtl) ;
    ModalNewTipoVehiculoCrtl.$inject = ['$uibModalInstance', 'claseVehiculoService','tipoVehiculoService'] ;

        function ModalNewTipoVehiculoCrtl($uibModalInstance, claseVehiculoService,tipoVehiculoService)
        {
            var vm =  this ;

            vm.msj = "";
            vm.data_clase_vehiculos = [] ;

            vm.formData = {
                nombre : '',
                descripcion : '',
                clase_vehiculo : [],
            }

            init();
            function init(){
                getClaseVehiculos() ;
            } ;

            function getClaseVehiculos()
            {
                claseVehiculoService.getClaseVehiculos().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_clase_vehiculos = response.data ;
                            // console.log(vm.data_clase_vehiculos );
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                    'clase_vehiculo_id'      : vm.formData.clase_vehiculo.id,

                };
                // console.log(data);
                tipoVehiculoService.save(data).then(
                    function(response)
                    {
                        // console.log(response);

                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;