(function(){
    'use strict';

    angular.module('modelos.info.controller').controller('ModelosInfoCtrl',ModelosInfoCtrl);
    ModelosInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','modeloService','marcaService'] ;

        function ModelosInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,modeloService,marcaService)
        {

            var vm = this ;

            // function
                vm.getModelos = getModelos ;
                vm.onClick    = onClick ;
                vm.newModelo   = newModelo ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;
                vm.data_marcas = [];

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                getMarcas() ;
                vm.getModelos() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getModelos();
                }
                else if (name === 'new')
                {
                      vm.newModelo('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }
                else if (name === 'delete')
                {
                    deleteRol() ;
                }
                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            }

            function getModelos()
            {
                modeloService.getModelos().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            function getMarcas()
            {
                marcaService.getMarcas().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_marcas = response.data ;
                            // console.log(vm.data_areas );
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            }


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'modelo_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                        'marca_id' : row.marca.id,
                    };

                    modeloService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getModelos() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newModelo (size)
                {
                        var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/vehiculos/modelos/new.modelo.tpl.html',
                            controller: 'ModalNewModeloCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getModelos(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        modeloService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getModelos() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Modelo',
                        bodyText: '¿Esta Seguro de Eliminar Modelo: '+ row.nombre +' de la Marca : '+row.marca.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }
        }
})() ;

