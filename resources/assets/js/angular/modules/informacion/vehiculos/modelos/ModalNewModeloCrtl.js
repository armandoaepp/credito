(function(){
    'use stric' ;

    angular.module('modelos.info.controller').controller('ModalNewModeloCrtl', ModalNewModeloCrtl) ;
    ModalNewModeloCrtl.$inject = ['$uibModalInstance', 'marcaService','modeloService'] ;

        function ModalNewModeloCrtl($uibModalInstance, marcaService,modeloService)
        {
            var vm =  this ;

            vm.msj = "";
            vm.data_marcas = [] ;

            vm.formData = {
                nombre : '',
                descripcion : '',
                clase_vehiculo : [],
            }

            init();
            function init(){
                getMarcas() ;
            } ;

            function getMarcas()
            {
                marcaService.getMarcas().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_marcas = response.data ;
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                    'marca_id'     : vm.formData.marca.id,

                };
                // console.log(data);
                modeloService.save(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;