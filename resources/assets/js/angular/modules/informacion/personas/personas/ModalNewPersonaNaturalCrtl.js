(function(){
	angular.module('persona.natural.info.controller').controller('ModalNewPersonaNaturalCrtl', ModalNewPersonaNaturalCrtl) ;
        ModalNewPersonaNaturalCrtl.$inject = ['$state','$uibModalInstance','$filter', 'personaNaturalService'] ;

            function ModalNewPersonaNaturalCrtl($state,$uibModalInstance,$filter, personaNaturalService)
            {
                var vm =  this ;

                vm.msj = "";

                vm.formData = {
                    'dni'   : '',
                    'apellidos' : '',
                    'nombres'    : '',
                    'telefonos'  : [],
                    'mails'  : [],
                    'sexo'  : 1,
                    'fecha_nac': '',
                    'tipo_relacion_id' : 2,
                    'persona_id': '',
                    'isRegistrer' : false,
                } ;

                vm.title_modal = "Persona";
                init();
                function init() {
                    config($state.current.name);
                }

                function config(state_name)
                {
                    // console.log(state_name);
                    if (state_name === 'personas.personas')
                    {
                        vm.title_modal = "Persona" ;
                        vm.formData.tipo_relacion_id       = 2;
                    }
                    else if (state_name === 'personas.clientes')
                    {
                        vm.title_modal = "Cliente" ;
                        vm.formData.tipo_relacion_id       = 3;
                    }
                    else if (state_name === 'personas.empleados')
                    {
                        vm.title_modal = "Empleado" ;
                        vm.formData.tipo_relacion_id       = 4;
                    }
                    else if (state_name === 'personas.avales')
                    {
                        vm.title_modal            = "Aval" ;
                        vm.formData.tipo_relacion_id = 5;
                    };
                    // console.log( vm.title_modal);
                    // console.log( vm.formData.tipo_relacion_id);
                };

                //  DatePicker ui-b
                    vm.today =  new Date() ;
                    vm.popup = {
                        opened : false,
                        maxDate : vm.today,
                    } ;

                    vm.openFechaNac = function() {
                        vm.popup.opened = true;
                    };

                vm.getPersonaByDni = function()
                {
                    vm.formData.apellidos = '';
                    vm.formData.nombres   = '';
                    vm.formData.telefonos = '';
                    vm.formData.mails     = '';
                    vm.formData.sexo      = '';
                    vm.formData.fecha_nac = '';
                    vm.formData.persona_id = '';
                    vm.formData.isRegistrer =  false ;

                    var dni =  vm.formData.dni ;
                    if (dni.length < 8 )
                    {
                        return ;
                    };



                    var data = {'dni': dni } ;

                    personaNaturalService.getPerNaturalByDni(data).then(
                        function(response)
                        {
                            var data = response.data ;
                            // console.log(data);
                            if (data !== null && data.length > 0)
                            {
                                // vm.formData.dni       = data[0].per_natural.dni ;
                                vm.formData.apellidos   = data[0].per_apellidos ;
                                vm.formData.nombres     = data[0].per_nombre ;
                                vm.formData.telefonos   = data[0].per_telefono  ;
                                vm.formData.mails       = data[0].per_mail ;
                                vm.formData.sexo        = data[0].per_natural.sexo;
                                vm.formData.fecha_nac   = (!data[0].per_fecha_nac)? vm.today :new Date( data[0].per_fecha_nac +' 00:00:00');
                                vm.formData.persona_id  = data[0].per_natural.persona_id;
                                vm.formData.isRegistrer =  true ;
                            }else{
                                vm.formData.isRegistrer =  false ;
                            } ;

                        }
                    );
                }

                vm.ok = function ()
                {
                    vm.save();

                  /*  var dni =  vm.formData.dni ;
                    var data = {'dni': dni } ;

                    personaNaturalService.getPerNaturalByDni(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                if (response.data !== null && response.data.length > 0)
                                {
                                    vm.msj = 'DNI Ya se encuentra registrado!'
                                }else
                                {
                                    vm.save();
                                }
                                ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );*/
                };

                vm.save = function()
                {
                    var fecha_nac = $filter('date')(vm.formData.fecha_nac,'yyyy-MM-dd');
                    vm.formData.fecha_nac = fecha_nac ;
                    // console.log(vm.formData);
                    personaNaturalService.save(vm.formData).then(
                        function(response)
                        {
                            // console.log(response);
                            if (!response.error)
                            {
                                $uibModalInstance.close(vm.formData);
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );
                };

                vm.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }

})();