(function(){
    'use strict';

    angular.module('persona.natural.info.controller').controller('PersonaNaturalInfoCtrl',PersonaNaturalInfoCtrl);
    PersonaNaturalInfoCtrl.$inject = ['$rootScope', 'personaNaturalService','$filter', '$uibModal', 'PATH', 'modalService','$state', 'NgTableParams'] ;

        function PersonaNaturalInfoCtrl($rootScope, personaNaturalService,$filter, $uibModal, PATH, modalService,$state, NgTableParams)
        {
            var vm = this ;

            // function
                vm.onClick              = onClick ;
                vm.getPersonasNaturales = getPersonasNaturales ;
                vm.newPersonaNatural    = newPersonaNatural ;
                vm.editPersona          = editPersona ;
                vm.deletePersona        = deletePersona ;

            // variables
                vm.data_list = [] ;
                vm.fillSelected  = [] ;

            init();
            function init() {
                config($state.current.name);
                tablePlugin() ;
                vm.getPersonasNaturales() ;
            }

            function config(state_name)
            {
                // console.log(state_name);
                if (state_name === 'personas.personas')
                {
                    vm.title_page = "Personas" ;
                    vm.title_tipo = "Persona" ;
                    vm.tipo_relacion_id = 2;
                }
                else if (state_name === 'personas.clientes')
                {
                    vm.title_page = "Clientes" ;
                    vm.title_tipo = "Cliente" ;
                    vm.tipo_relacion_id = 3;
                }
                else if (state_name === 'personas.empleados')
                {
                    vm.title_page = "Empleados" ;
                    vm.title_tipo = "Empleado" ;
                    vm.tipo_relacion_id = 4;
                }
                else if (state_name === 'personas.avales')
                {
                    vm.title_page = "Avales" ;
                    vm.title_tipo = "Aval" ;
                    vm.tipo_relacion_id = 5;
                };
            };

            function onClick(name)
            {
                if (name === 'list')
                {
                    vm.getPersonasNaturales();
                }
                else if (name === 'new')
                {
                      vm.newPersonaNatural('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editPersona() ;
                }
                else if (name === 'delete')
                {
                    vm.deletePersona() ;
                }else{
                    return ;
                };

            } ;

            function getPersonasNaturales()
            {

                // personaNaturalService.getPersonasNaturales().then(
                var data = { 'tipo_relacion_id' : vm.tipo_relacion_id} ;
                personaNaturalService.getPersonasByTipoRelacion(data).then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data_list = response.data;
                            reloadNgTable();

                            return vm.data_list ;
                        }
                    }
                );
            }

            // ===== ng-table ==============================================================================================

                vm.applyGlobalSearch = applyGlobalSearch;
                // vm.cancel = cancel;
                // vm.del    = del;
                // vm.save   = save;
                // vm.editRow = editRow ;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch()
                {
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }


            // ===================================================================================================

            //  New
                function newPersonaNatural (size)
                {
                    vm.tableParams.page(1);
                    vm.tableParams.reload();

                    var path = PATH.INFORMACION ;

                    var modalInstance = $uibModal.open({
                        templateUrl: path+'/personas/per_natural/nueva.natural.tpl.html',
                        controller: 'ModalNewPersonaNaturalCrtl',
                        controllerAs: 'vm',
                        size: size,
                        backdrop : 'static',
                    });

                    modalInstance.result.then(
                        function (data)
                        {
                            vm.getPersonasNaturales() ;
                        },
                        function ()
                        {
                            // console.log('Modal dismissed at: ' + new Date());
                        }
                    );

                };

            //  Editar
                function editPersona (size)
                {
                    var codigo = vm.fillSelected.persona_id ;

                    if (codigo === undefined) {
                        return modalAlert()
                    }
                    else
                    {
                        $state.go($state.current.name+'.editar', { codigo: codigo })
                        // $state.go('personas.personas.editar', { codigo: codigo })
                    }
                };

            // delete
                function deletePersona()
                {
                    var cod = vm.fillSelected.persona_id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalConfirm() ;
                    }

                }

                function confirmDelete()
                {
                     var data = {
                            'persona_id'    : vm.fillSelected.persona_id,
                            'estado'    : 0,
                            'tipo_relacion_id' : vm.tipo_relacion_id,
                        };
                        // console.log(data);
                        personaNaturalService.updateEstado(data).then(
                            function(response){

                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getPersonasNaturales() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm()
                {
                    var dato = vm.fillSelected.per_apellidos+ ' ' + vm.fillSelected.per_nombre ;
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar '+vm.title_tipo,
                        bodyText: '¿Esta Seguro de Eliminar '+vm.title_tipo+': '+ dato+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete();
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

                $rootScope.$on("reloadListPersona", function(event, values) {
                    // console.log('reloadListPersona');
                       vm.getPersonasNaturales();
                  });
        }

})() ;
