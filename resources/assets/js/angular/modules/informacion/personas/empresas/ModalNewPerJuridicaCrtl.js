(function(){
	angular.module('personas.juridicas.info.controller').controller('ModalNewPerJuridicaCrtl', ModalNewPerJuridicaCrtl) ;
        ModalNewPerJuridicaCrtl.$inject = ['$uibModalInstance','$filter', 'perJuridicaService','ubigeoService'] ;

            function ModalNewPerJuridicaCrtl($uibModalInstance,$filter, perJuridicaService,ubigeoService)
            {
                var vm =  this ;

                vm.msj = "";
                vm.data_ubigeos = [] ;

                vm.formData = {
                    'ruc' : '',
                    'razon_social' : '',
                    'nombre_comercial' : '',
                    'telefonos' : '',
                    'mails' : '',
                    'sitio_web' : '',
                    'distrito ' : [],
                } ;

                init() ;
                function init()
                {
                    getUbigeos();
                } ;

                function getUbigeos()
                {
                    ubigeoService.getUbigeos().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.data_ubigeos = response.data;
                                return vm.data_ubigeos ;
                            }
                        }
                    );
                };

                //  DatePicker ui-b
                   /* vm.today =  new Date() ;
                    vm.popup = {
                        opened : false,
                        maxDate : vm.today,
                    } ;

                    vm.openFechaNac = function() {
                        vm.popup.opened = true;
                    };
                    */

                vm.ok = function ()
                {
                    var ruc              = vm.formData.ruc ;
                    var razon_social     = vm.formData.razon_social ;
                    var nombre_comercial = vm.formData.nombre_comercial ;
                    var telefonos        = vm.formData.telefonos ;
                    var distrito_id      = vm.formData.distrito.id ;
                    var mails            = vm.formData.mails ;
                    var sitio_web        = vm.formData.sitio_web ;

                    vm.msj = {message : ''} ;
                    if (ruc === undefined)
                    {
                        return  vm.msj.message = 'Ingrese Ruc' ;
                    };
                    if (razon_social === undefined)
                    {
                        return  vm.msj.message = 'Ingrese Razon Social' ;
                    };

                    if (mails === undefined)
                    {
                        return  vm.msj.message = 'Ingrese almenos un mail' ;
                    };

                    if (telefonos === undefined)
                    {
                        return  vm.msj.message = 'Ingrese almenos un telefono' ;
                    };

                    if (distrito_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Distrito' ;
                    };


                    var data = {
                        'ruc' :  ruc ,
                        'razon_social' :  razon_social ,
                        'nombre_comercial' :  nombre_comercial ,
                        'telefonos' :  telefonos ,
                        'distrito_id' :  distrito_id ,
                        'mails' :  mails ,
                        'sitio_web' :  sitio_web ,
                    };


                    perJuridicaService.getPerJuridicaByRuc(data).then(
                        function(response)
                        {
                            // console.log(response);
                            if (!response.error)
                            {
                                if (response.data.length > 0)
                                {
                                    vm.msj = 'Ruc, ya se encuentra registrado!'
                                }else
                                {
                                    vm.save(data);
                                }
                                ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );
                };

                vm.save = function(data)
                {
                    perJuridicaService.save(data).then(
                        function(response)
                        {
                            console.log(response);
                            if (!response.error)
                            {
                                $uibModalInstance.close(vm.formData);
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );
                };

                vm.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }

})();