  angular.module('personas.juridicas.info.controller').controller('EditPerJuridicaCrtl', EditPerJuridicaCrtl) ;
        EditPerJuridicaCrtl.$inject = ['$rootScope',
                                     '$state',
                                     '$q',
                                     '$filter',
                                     '$stateParams',
                                     'modalService',
                                     'perJuridicaService',
                                     'perTelefonoService',
                                     'perMailService',
                                     'perDocumentoService',
                                     'perWebService'
                                    ] ;

            function EditPerJuridicaCrtl($rootScope,
                                        $state,
                                        $q,
                                        $filter,
                                        $stateParams,
                                        modalService,
                                        perJuridicaService,
                                        perTelefonoService,
                                        perMailService,
                                        perDocumentoService,
                                        perWebService
                                        )
            {
                var vm =  this ;

                vm.reloadListEmpresas = reloadListEmpresas ;

                vm.persona_id = $stateParams.codigo;

                // telefonos
                    vm.saveNewPhones = saveNewPhones ;
                    vm.updatePhone   = updatePhone ;
                    vm.deletePhone   = deletePhone;
                    vm.reOrderItemsTelefonos = reOrderItemsTelefonos ;

                    vm.flagNewPhones = false ;

                // mails
                    vm.saveNewMails       = saveNewMails ;
                    vm.updateMail         = updateMail ;
                    vm.deleteMail         = deleteMail;
                    vm.reOrderItemsEmails = reOrderItemsEmails ;

                    vm.flagNewMails = false ;

                 // webs
                    vm.saveNewWebs       = saveNewWebs ;
                    vm.updateWeb         = updateWeb ;
                    vm.deleteWeb         = deleteWeb;
                    vm.reOrderItemsWebs = reOrderItemsWebs ;

                    vm.flagNewWebs = false ;

                // dni
                    vm.updateRuc = updateRuc ;

                    vm.updatePerJuridicaInfo = updatePerJuridicaInfo ;

                vm.msj = "";

                vm.formData = {
                    'ruc'   : '',
                    'razon_social' : '',
                    'nombre_comercial'    : '',
                    'telefonos'  : [],
                    'mails'  : [],
                    'webs'  : [],
                    'sexo'  : '',
                    'newtelefonos': [],
                    'newmails': [],
                    'newwebs': [],
                    'fecha_nac': '',
                }

                 //  DatePicker ui-b
                    vm.today =  new Date() ;
                    vm.popup = {
                        opened : false,
                        maxDate : vm.today,
                    } ;

                    vm.openFechaNac = function() {
                        vm.popup.opened = true;
                    };

                //  sexo data
                    vm.sexoData = [
                            {value: 1, text: 'Masculino'},
                            {value: 2, text: 'Femenino'}
                          ];

                    vm.showStatus = function() {
                        var selected = $filter('filter')(vm.sexoData, {value: vm.formData.sexo});
                        return (vm.formData.sexo && selected.length) ? selected[0].text : ' No Seleccionado';
                    };


                // informacion de persona
                    getPerJuridicaInfoAll() ;
                    function getPerJuridicaInfoAll()
                    {
                        var data = {
                            'persona_id' : vm.persona_id,
                        } ;

                        perJuridicaService.getPerJuridicaInfoAll(data).then(
                            function(response){
                                console.log(response);
                                if (!response.error)
                                {
                                    vm.formData.ruc              = response.data[0].per_juridica.ruc;
                                    vm.formData.razon_social     = response.data[0].per_juridica.razon_social;
                                    vm.formData.nombre_comercial = response.data[0].per_juridica.nombre_comercial;
                                    vm.formData.telefonos        = response.data[0].per_telefono;
                                    vm.formData.mails            = response.data[0].per_mail;
                                    vm.formData.fecha_nac        = (!response.data[0].per_fecha_nac)? vm.today :new Date( response.data[0].per_fecha_nac +' 00:00:00');
                                    vm.formData.webs = response.data[0].per_web ;
                                    // console.log(vm.formData.fecha_nac);
                                    return response.data ;
                                }
                            }
                        );
                    }

                // ruc
                    function updateRuc(value)
                    {
                        var deferred = $q.defer();

                        var ruc = value;
                        var persona_id = vm.persona_id ;

                        var data = {
                                'persona_id'   : persona_id,
                                'ruc'          : ruc,
                            };

                        perJuridicaService.updateRuc(data).then(
                                function(response){

                                    if (!response.error)
                                    {
                                        vm.formData.ruc = response.data.ruc;
                                        deferred.resolve();
                                    }else
                                    {
                                        deferred.resolve(response.data.ruc[0]);
                                    }
                                }, function (response) {
                                    // the following line rejects the promise
                                    deferred.reject(response);
                                    return deferred.promise;
                                }
                            );
                        return deferred.promise;
                    } ;


                // update nombres y sexo
                    function updatePerJuridicaInfo()
                    {
                        var persona_id = vm.persona_id ;

                        var fecha_nac = $filter('date')(vm.formData.fecha_nac,'yyyy-MM-dd');

                         var data = {
                                'razon_social'  : vm.formData.razon_social,
                                'nombre_comercial'    : vm.formData.nombre_comercial,
                                'persona_id' : persona_id,
                                'fecha_nac'  : fecha_nac,
                            };


                            perJuridicaService.updatePerJuridicaInfo(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.razon_social = response.data.per_nombre;
                                        vm.formData.nombre_comercial   = response.data.per_apellidos;
                                        vm.formData.fecha_nac = (!response.data.per_fecha_nac)? vm.today :new Date( response.data.per_fecha_nac+' 00:00:00');

                                    }
                                }
                            );

                    };


                //  telefonos
                    function saveNewPhones()
                    {
                        var telefonos = vm.formData.newtelefonos ;
                        var persona_id = vm.persona_id ;

                         var data = {
                                'telefonos'    : telefonos,
                                'persona_id'   : persona_id,
                            };
                            // console.log(data) ;
                            perTelefonoService.save(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.newtelefonos = [] ;
                                        vm.flagNewPhones = false ;
                                        vm.formData.telefonos = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );

                        // console.log(vm.flagNewPhones) ;
                    }

                    function updatePhone(value, id)
                    {
                        var telefono = value;

                        if( value.length != 9  )
                        {
                          return 'Teléfono Invalido(Son Nueve Numero)';
                        }

                        var data = {
                                'telefono_id' : id,
                                'telefono'    : telefono,
                            };
                            // console.log(data) ;

                            perTelefonoService.update(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.telefonos = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );
                    }

                    function deletePhone(Telefono, id)
                    {
                        var modalOptions = {
                            closeButtonText: 'Cancelar',
                            actionButtonText: 'Eliminar',
                            headerText: 'Eliminar Teléfono',
                            bodyText: '¿Esta Seguro de Eliminar Teléfono: '+ Telefono+' ?'
                        };

                       modalService.showModalConfirm({}, modalOptions).then(function (result) {
                            confirmDeletePhone(id);
                        });
                    }

                    function confirmDeletePhone(id)
                    {
                         var data = {
                                'telefono_id'    : id
                            };

                            perTelefonoService.deleteFill(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.telefonos = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );
                    }

                    function reOrderItemsTelefonos()
                    {
                         var data = {
                                'telefonos' : vm.formData.telefonos,
                                'persona_id' : vm.persona_id ,
                            };

                            perTelefonoService.reOrderItems(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.telefonos = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );
                    }

                    // sortable telefonos
                    vm.barConfigPhones = {
                        group: 'telefonos',
                        animation: 150,
                        // onSort: function (** ngSortEvent * evt){
                        onSort: function (evt){
                            vm.reOrderItemsTelefonos() ;
                            // console.log(vm.formData.telefonos);
                        }
                    };



                //  Mail
                    function saveNewMails()
                    {
                        var mails = vm.formData.new_mails ;
                        var persona_id = vm.persona_id ;

                         var data = {
                                'persona_id'   : persona_id,
                                'mails'        : mails,
                            };

                            perMailService.save(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.new_mails = [] ;
                                        vm.flagNewMails = false ;
                                        vm.formData.mails = response.data;
                                        return vm.formData.mails;
                                    }
                                }
                            );
                    }

                    function updateMail(value, id)
                    {
                        var mail = value;

                        var data = {
                                'mail_id' : id,
                                'mail'    : mail,
                            };

                            perMailService.update(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.mails = response.data;
                                        return vm.formData.mails;
                                    }
                                }
                            );
                    }

                    function deleteMail(value, id)
                    {
                        var modalOptions = {
                            closeButtonText: 'Cancelar',
                            actionButtonText: 'Eliminar',
                            headerText: 'Eliminar Mail',
                            bodyText: '¿Esta Seguro de Eliminar Mail: '+ value+' ?'
                        };

                       modalService.showModalConfirm({}, modalOptions).then(function (result) {
                            confirmDeleteMail(id);
                        });
                    }

                    function confirmDeleteMail(id)
                    {
                         var data = {
                                'mail_id'    : id
                            };

                            perMailService.deleteFill(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.mails = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );
                    }

                    function reOrderItemsEmails()
                    {
                         var data = {
                                'mails' : vm.formData.mails,
                                'persona_id' : vm.persona_id ,
                            };

                            perMailService.reOrderItems(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.mails = response.data;
                                        return vm.formData.mails;
                                    }
                                }
                            );
                    }

                    // sortable Emails
                    vm.barConfigEmails = {
                        group: 'Mails',
                        animation: 150,
                        // onSort: function (** ngSortEvent *evt){
                        onSort: function (evt){
                            vm.reOrderItemsEmails() ;
                            // console.log(vm.formData.mails);
                        }
                    };

                    function reloadListEmpresas()
                    {
                        var values = '' ;
                        $state.go('personas.empresas', values);
                        $rootScope.$broadcast("reloadListEmpresas", values);
                    }

                //  webs
                    vm.formDataWeb = {
                        tipo_web: [] ,
                        url: '' ,
                    } ;

                    vm.data_tipo_webs = [] ;
                    getTipoWebs();
                    function getTipoWebs()
                    {
                        var persona_id = vm.persona_id ;

                            perWebService.getTipoWebs().then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.data_tipo_webs = response.data;
                                        return vm.data_tipo_webs;
                                    }
                                }
                            );

                        // console.log(vm.flagNewWebs) ;
                    }
                    function saveNewWebs()
                    {
                        var url         = vm.formDataWeb.url ;
                        var tipo_web_id = vm.formDataWeb.tipo_web.id ;
                        var persona_id  = vm.persona_id ;

                        vm.msj = {message : ''} ;
                        if (url === undefined)
                        {
                            return  vm.msj.message = 'Ingrese pagina web' ;
                        };
                        if (tipo_web_id === undefined)
                        {
                            return  vm.msj.message = 'Seleccione tipo de web' ;
                        };



                         var data = {
                                'url'           : url,
                                'tipo_web_id'   : tipo_web_id,
                                'persona_id'    : persona_id,
                            };
                            // console.log(data) ;
                            perWebService.save(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formDataWeb.url = '';
                                        vm.formDataWeb.tipo_web = [] ;
                                        vm.flagNewWebs = false ;
                                        vm.formData.webs = response.data;
                                        return vm.formData.webs;
                                    }
                                }
                            );
                    }

                    function updateWeb(value, id)
                    {
                        var url = value;

                        if( value === undefined  )
                        {
                          return 'Ingrese Url';
                        }

                        var data = {
                                'per_web_id' : id,
                                'url'    : url,
                            };
                            // console.log(data) ;

                            perWebService.update(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.webs = response.data;
                                        return vm.formData.webs;
                                    }
                                }
                            );
                    }

                    function deleteWeb(Telefono, id)
                    {
                        var modalOptions = {
                            closeButtonText: 'Cancelar',
                            actionButtonText: 'Eliminar',
                            headerText: 'Eliminar Web',
                            bodyText: '¿Esta Seguro de Eliminar Web: '+ Telefono+' ?'
                        };

                       modalService.showModalConfirm({}, modalOptions).then(function (result) {
                            confirmDeleteWeb(id);
                        });
                    }

                    function confirmDeleteWeb(id)
                    {
                         var data = {
                                'per_web_id'    : id
                            };

                            perWebService.deleteFill(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.webs = response.data;
                                        return vm.formData.webs;
                                    }
                                }
                            );
                    }

                    function reOrderItemsWebs()
                    {
                         var data = {
                                'webs' : vm.formData.webs,
                                'persona_id' : vm.persona_id ,
                            };

                            perTelefonoService.reOrderItems(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.webs = response.data;
                                        return vm.formData.webs;
                                    }
                                }
                            );
                    }

                    // sortable telefonos
                    vm.barConfigWebs = {
                        group: 'webs',
                        animation: 150,
                        // onSort: function (** ngSortEvent * evt){
                        onSort: function (evt){
                            vm.reOrderItemsWebs() ;
                            // console.log(vm.formData.telefonos);
                        }
                    };
            }