(function(){
    'use strict';

    angular.module('personas.juridicas.info.controller').controller('PerJuridicasInfoCtrl',PerJuridicasInfoCtrl);
    PerJuridicasInfoCtrl.$inject = ['$rootScope', 'perJuridicaService','$filter', '$uibModal', 'PATH', 'modalService','$state', 'NgTableParams'] ;

        function PerJuridicasInfoCtrl($rootScope, perJuridicaService,$filter, $uibModal, PATH, modalService,$state, NgTableParams)
        {
            var vm = this ;

            // function
                vm.onClick           = onClick ;
                vm.getPerJuridicas   = getPerJuridicas ;
                vm.newPerJuridica    = newPerJuridica ;
                vm.editPerJuridica   = editPerJuridica ;
                vm.deletePerJuridica = deletePerJuridica ;

            // variables
                vm.data_list = [] ;
                vm.fillSelected  = [] ;

            init();
            function init() {
                tablePlugin() ;
                vm.getPerJuridicas() ;
            } ;

            function onClick(name)
            {
                if (name === 'list')
                {
                    vm.getPerJuridicas();
                }
                else if (name === 'new')
                {
                      vm.newPerJuridica('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editPerJuridica() ;
                }
                else if (name === 'delete')
                {
                    vm.deletePerJuridica() ;
                }else{
                    return ;
                };

            } ;

            function getPerJuridicas()
            {
                perJuridicaService.getPerJuridicas().then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data_list = response.data;
                            reloadNgTable();

                            return vm.data_list ;
                        }
                    }
                );
            };



            // ===== ng-table ==============================================================================================

                vm.applyGlobalSearch = applyGlobalSearch;
                // vm.cancel = cancel;
                // vm.del    = del;
                // vm.save   = save;
                // vm.editRow = editRow ;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch()
                {
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }


            // ===================================================================================================

            //  New
                function newPerJuridica (size)
                {
                    vm.tableParams.page(1);
                    vm.tableParams.reload();

                    var path = PATH.INFORMACION ;

                    var modalInstance = $uibModal.open({
                        templateUrl: path+'/personas/empresas/nueva.empresa.tpl.html',
                        controller: 'ModalNewPerJuridicaCrtl',
                        controllerAs: 'vm',
                        size: size,
                        backdrop : 'static',
                    });

                    modalInstance.result.then(
                        function (data)
                        {
                            vm.getPerJuridicas() ;
                        },
                        function ()
                        {
                            // console.log('Modal dismissed at: ' + new Date());
                        }
                    );

                };

            //  Editar
                function editPerJuridica (size)
                {
                    var codigo = vm.fillSelected.persona_id ;

                    if (codigo === undefined) {
                        return modalAlert()
                    }
                    else
                    {
                        $state.go('personas.empresas.editar', { codigo: codigo })
                    }


                };

            // delete
                function deletePerJuridica()
                {
                    var cod = vm.fillSelected.persona_id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalConfirm() ;
                    }

                }

                function confirmDelete()
                {
                     var data = {
                            'persona_id'    : vm.fillSelected.persona_id,
                            'estado'    : 0,
                        };
                        // console.log(data);
                        perJuridicaService.updateEstado(data).then(
                            function(response){

                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getPerJuridicas() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm()
                {
                    var dato = vm.fillSelected.razon_social ;
                    console.log(vm.fillSelected);
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Empresa',
                        bodyText: '¿Esta Seguro de Eliminar Empresa: '+ dato+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete();
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

                $rootScope.$on("reloadListEmpresas", function(event, values) {
                    // console.log('reloadListPersona');
                       vm.getPerJuridicas();
                  });
        }

})() ;
