(function(){
	 angular.module('empleados.info.controller').controller('ModalNewEmpleadoCrtl', ModalNewEmpleadoCrtl) ;
        ModalNewEmpleadoCrtl.$inject = ['$uibModalInstance', 'empleadoService','personaNaturalService', 'areaService','cargoService'] ;

            function ModalNewEmpleadoCrtl($uibModalInstance ,empleadoService,personaNaturalService, areaService,cargoService)
            {
                var vm =  this ;
                vm.onSelectedArea = onSelectedArea ;

                vm.msj = "";

                vm.formData = {
                    nombre : '',
                } ;

                vm.data_persons = [];
                vm.data_areas   = [];
                vm.data_cargos   = [];
                vm.data_cargos_area   = [];

                init();
                function init(){
                    getPerNaturalInfoBasica() ;
                    getAreas() ;
                    getCargos() ;
                }

                function getPerNaturalInfoBasica()
                {
                    personaNaturalService.getPerNaturalInfoBasica().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_persons = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );

                }

                function getAreas()
                {
                    areaService.getAreas().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_areas = response.data ;
                            }else
                            {
                                vm.msj = response.message ;
                            }
                        }
                    );
                }

                function getCargos()
                {
                    cargoService.getCargosAll().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_cargos = response.data ;
                            }else
                            {
                                vm.msj = response.message ;
                            }
                        }
                    );
                }



                function onSelectedArea(itemArea)
                {
                    var select_area_id = parseInt(itemArea.id) ;
                    var data = vm.data_cargos ;

                    vm.data_cargos_area = [] ;

                    var out = []
                    for(var i in data)
                    {
                        var id =  parseInt(data[i].area_id);
                        if( id === select_area_id)
                        {
                            out.push(data[i])
                        }

                    }
                    vm.data_cargos_area = out ;

                    return vm.data_cargos_area  ;
                }


                vm.ok = function ()
                {
                    var cargo_id   = vm.formData.cargo.id ;
                    var persona_id = vm.formData.persona.persona_id ;


                    vm.msj = {message : ''} ;
                    if (cargo_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Cargo' ;
                    };
                    if (persona_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Persona' ;
                    };

                    var data = {
                        'persona_id'    : persona_id,
                        'cargo_id'         : cargo_id,
                    };
                    // console.log(data);

                    empleadoService.save(data).then(
                        function(response)
                        {
                            // console.log(response);
                            if (!response.error)
                            {
                                $uibModalInstance.close(vm.formData);
                            }else
                            {
                                vm.msj = response ;
                            }
                        }
                    );

                };

                vm.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
})();