(function(){
    'use strict';

    angular.module('empleados.info.controller').controller('EmpleadosInfoCtrl',EmpleadosInfoCtrl);
    EmpleadosInfoCtrl.$inject = ['botoneraFactory','$filter', '$uibModal', 'PATH', 'modalService','NgTableParams','empleadoService','accesosService','cargoService'] ;

        function EmpleadosInfoCtrl(botoneraFactory,$filter, $uibModal, PATH, modalService,NgTableParams,empleadoService,accesosService,cargoService)
        {
            var vm = this ;

            // function
                vm.getEmpleados   = getEmpleados ;
                vm.onClick        = onClick ;
                vm.newEmpleado    = newEmpleado ;
                vm.deleteEmpleado = deleteEmpleado ;
                vm.editEmpleado   = editEmpleado ;
                vm.asiginarNewCargoEmpleado   = asiginarNewCargoEmpleado ;

            // variables
                vm.data_list = [] ;
                vm.fillSelected = [];

            vm.data_cargos = [] ;

            init();
            function init() {
                tablePlugin() ;
                vm.getEmpleados() ;
                getCargos() ;
            }

            function onClick(name, row)
            {
                // console.log(name);
                if (name === 'list')
                {
                    vm.getEmpleados();
                }
                else if (name === 'new')
                {
                      vm.newEmpleado('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editEmpleado('md');
                }
                else if (name === 'delete')
                {
                    vm.deleteEmpleado() ;
                }
                else if (name === 'asignar')
                {
                    vm.asiginarNewCargoEmpleado('md') ;
                }

                else{
                    return ;
                };
            } ;

            function getEmpleados()
            {
                empleadoService.getEmpleados().then(
                    function(response){
                        // console.log(response);
                        if (!response.error)
                        {   vm.fillSelected = [];
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            } ;

            // ===== ng-table ==============================================================================================

                // vm.cancel = cancel;
                // vm.del    = del;
                // vm.save   = save;
                // vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New
                function newEmpleado (size)
                {
                        var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/personas/empleados/new.empleado.tpl.html',
                            controller: 'ModalNewEmpleadoCrtl',
                            controllerAs: 'vm',
                            size: size,
                             backdrop : 'static',
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getEmpleados() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

            //delete
                function deleteEmpleado()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalConfirm(vm.fillSelected) ;
                    }
                };

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Usuario',
                        bodyText: '¿Esta Seguro de Eliminar Empleado: '+ row.per_nombre + ' ' + row.per_apellidos+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                } ;

                function confirmDelete(row)
                {
                     var data = {
                            'empleado_id'   : row.id,
                            'persona_id'    : row.persona_id,
                            'estado'    : 0,
                        };

                        empleadoService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getEmpleados() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

            //  edit
                function editEmpleado()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalEditEmpleado('md') ;
                    }
                };

                function modalEditEmpleado(size)
                {
                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/personas/empleados/edit.empleado.tpl.html',
                            controller: 'ModalEditEmpleadoCrtl',
                            controllerAs: 'vm',
                            size: size,
                             backdrop : 'static',
                            resolve: {
                                data_empleado: function () { return vm.fillSelected; },
                                data_cargos: function () { return vm.data_cargos; },

                            }
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getEmpleados() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

             //  edit
                function asiginarNewCargoEmpleado()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalAsiginarNewCargoEmpleado('md') ;
                    }
                };

                function modalAsiginarNewCargoEmpleado(size)
                {
                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/personas/empleados/asignar.new.cargo.empleado.tpl.html',
                            controller: 'ModalAsignaNewCargoEmpleadoCrtl',
                            controllerAs: 'vm',
                            size: size,
                             backdrop : 'static',
                            resolve: {
                                data_empleado: function () { return vm.fillSelected; },
                                data_cargos: function () { return vm.data_cargos; },

                            }
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getEmpleados() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            function getCargos()
            {
                cargoService.getCargosAll().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_cargos = response.data ;
                        }else
                        {
                            vm.msj = response.message ;
                        }
                    }
                );
            } ;

        }
})() ;

