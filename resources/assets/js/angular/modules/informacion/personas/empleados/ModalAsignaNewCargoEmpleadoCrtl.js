(function(){
	 angular.module('empleados.info.controller').controller('ModalAsignaNewCargoEmpleadoCrtl', ModalAsignaNewCargoEmpleadoCrtl) ;
        ModalAsignaNewCargoEmpleadoCrtl.$inject = ['$uibModalInstance', 'data_empleado','data_cargos','empleadoService','areaService','cargoService'] ;

            function ModalAsignaNewCargoEmpleadoCrtl($uibModalInstance, data_empleado,data_cargos,empleadoService, areaService,cargoService)
            {
                var vm =  this ;
                vm.onSelectedCargosByArea = onSelectedCargosByArea ;
                vm.fillSelected   = data_empleado ;
                vm.data_cargos    = data_cargos;

                vm.msj = "";

                vm.formData = {
                    nombre : vm.fillSelected.per_apellidos + ' ' + vm.fillSelected.per_nombre ,
                    empleado_id : vm.fillSelected.id,
                    cargo_empleado_id : vm.fillSelected.cargo_empleado_id ,
                    area : [],
                    cargo : [],
                } ;

                vm.data_persons     = [];
                vm.data_areas       = [];
                // vm.data_cargos      = [];
                vm.data_cargos_area = [];

                init();
                function init(){
                    // getCargos() ;
                    getAreas() ;
                }


                function getAreas()
                {
                    areaService.getAreas().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_areas = response.data ;
                                for (var i in vm.data_areas)
                                {
                                    if (vm.data_areas[i].id === vm.fillSelected.area_id) {
                                        vm.formData.area =  vm.data_areas[i] ;
                                        onSelectedCargosByArea(vm.formData.area, vm.fillSelected.cargo_id) ;
                                        break ;
                                    };
                                }
                            }else
                            {
                                vm.msj = response.message ;
                            }
                        }
                    );
                }

                function onSelectedCargosByArea(itemArea, select_cargo_id)
                {
                    var select_area_id  = parseInt(itemArea.id) ;
                    var select_cargo_id = parseInt(select_cargo_id) ;

                    var data = vm.data_cargos ;
                    var cargo_select    = [] ;

                    vm.data_cargos_area = [] ;
                    vm.formData.cargo   = [];

                    var out = [] ;
                    for(var i in data)
                    {
                        var area_id =  parseInt(data[i].area_id);
                        if( area_id === select_area_id)
                        {
                            out.push(data[i])
                        };

                        var cargo_id = parseInt(data[i].id) ;
                        if ( select_area_id === area_id && select_cargo_id === cargo_id )
                        {
                           cargo_select = data[i] ;
                        };
                    } ;

                    vm.data_cargos_area = out ;
                    vm.formData.cargo   = cargo_select ;

                    return vm.data_cargos_area  ;
                }


                vm.ok = function ()
                {
                    var cargo_id          = vm.formData.cargo.id ;
                    var cargo_empleado_id = vm.formData.cargo_empleado_id ;
                    var empleado_id       = vm.formData.empleado_id ;

                    vm.msj = {message : ''} ;

                    if (cargo_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Cargo' ;
                    };
                    if (empleado_id === undefined)
                    {
                        return  vm.msj.message = 'Error Verificar Empleado' ;
                    };

                    var data = {
                        'cargo_id'          : cargo_id,
                        'cargo_empleado_id' : cargo_empleado_id,
                        'empleado_id'       : empleado_id,
                    };

                    empleadoService.asignarNewCargo(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                $uibModalInstance.close(vm.formData);
                            }else
                            {
                                vm.msj.message = response.data ;
                            }
                        }
                    );

                };

                vm.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
})();