(function(){
	 angular.module('clientes.info.controller').controller('ModalEditClienteCrtl', ModalEditClienteCrtl) ;
        ModalEditClienteCrtl.$inject = ['$uibModalInstance', 'data_cliente','empleadoService','areaService','cargoService'] ;

            function ModalEditClienteCrtl($uibModalInstance, data_cliente,empleadoService, areaService,cargoService)
            {
                var vm =  this ;
                vm.fillSelected   = data_cliente ;

                vm.msj = "";

                vm.formData = {
                    nombre : vm.fillSelected.per_apellidos + ' ' + vm.fillSelected.per_nombre ,
                    cliente_id : vm.fillSelected.cliente_id,
                    agente_comercial_id : vm.fillSelected.agente_comercial_id,
                    agente_comercial : [],
                } ;

                vm.data_agentes       = [];

                init();
                function init(){
                    getEmpleadosInfoBasica() ;
                } ;

                function getEmpleadosInfoBasica()
                {
                    empleadoService.getEmpleadosInfoBasica().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_agentes = response.data ;
                            }else
                            {
                                vm.msj = response.message ;
                            }
                        }
                    );
                }


                vm.ok = function ()
                {
                    var cargo_id          = vm.formData.cargo.id ;
                    var cargo_empleado_id = vm.formData.cargo_empleado_id ;
                    var empleado_id       = vm.formData.empleado_id ;

                    vm.msj = {message : ''} ;

                    if (cargo_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Cargo' ;
                    };
                    if (empleado_id === undefined)
                    {
                        return  vm.msj.message = 'Error Verificar Empleado' ;
                    };

                    var data = {
                        'cargo_id'          : cargo_id,
                        'cargo_empleado_id' : cargo_empleado_id,
                        'empleado_id'       : empleado_id,
                    };

                    empleadoService.update(data).then(
                        function(response)
                        {
                            // console.log(response);
                            if (!response.error)
                            {
                                $uibModalInstance.close(vm.formData);
                            }else
                            {
                                vm.msj = response ;
                            }
                        }
                    );

                };

                vm.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
})();