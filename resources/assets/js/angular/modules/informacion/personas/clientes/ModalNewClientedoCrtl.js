(function(){
	 angular.module('clientes.info.controller').controller('ModalNewClienteCrtl', ModalNewClienteCrtl) ;
        ModalNewClienteCrtl.$inject = ['$uibModalInstance', 'empleadoService','personaService','clienteService'] ;

            function ModalNewClienteCrtl($uibModalInstance ,empleadoService,personaService,clienteService)
            {
                var vm =  this ;
                // vm.onSelectedArea = onSelectedArea ;

                vm.msj = "";

                vm.formData = {
                    nombre : '',
                    persona : [],
                    agente_comercial : [],
                } ;

                vm.data_persons = [];
                vm.data_agentes = [];

                vm.data_per_tipo = [{ per_tipo: "Todos", id: 0 },
                                        { per_tipo: "Personas", id: 1 },
                                        { per_tipo: "Empresas", id: 2 }
                                    ];
                vm.searchPerTipo = vm.data_per_tipo[0];


                init();
                function init(){
                    getPersonasInfoBasica() ;
                    getEmpleadosInfoBasica();
                }

                function getPersonasInfoBasica()
                {
                    var per_tipo = vm.searchPerTipo.id ;
                    var data = {
                        'per_tipo' : per_tipo,
                    } ;

                    personaService.getPersonasInfoBasica(data).then(
                        function(response)
                        {
                            console.log(response);
                            if (!response.error)
                            {
                                vm.data_persons = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );

                }

                function getEmpleadosInfoBasica()
                {
                    empleadoService.getEmpleadosInfoBasica().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_agentes = response.data ;
                            }else
                            {
                                vm.msj = response.message ;
                            }
                        }
                    );
                }


                vm.searchPersonas = searchPersonas ;
                function searchPersonas(fill)
                {
                    vm.formData.persona = [] ;
                    getPersonasInfoBasica() ;
                } ;

                vm.ok = function ()
                {
                    var persona_id = vm.formData.persona.persona_id ;
                    var empleado_id   = vm.formData.agente_comercial.id ;

                    vm.msj = {message : ''} ;

                    if (persona_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Cliente' ;
                    };
                    if (empleado_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Agente Comercial' ;
                    };

                    var data = {
                        'persona_id'    : persona_id,
                        'empleado_id'   : empleado_id,
                    };

                    console.log(data);

                    clienteService.save(data).then(
                        function(response)
                        {
                            console.log(response);
                            if (!response.error)
                            {
                                $uibModalInstance.close(vm.formData);
                            }else
                            {
                                vm.msj = response ;
                            }
                        }
                    );

                };

                vm.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
})();