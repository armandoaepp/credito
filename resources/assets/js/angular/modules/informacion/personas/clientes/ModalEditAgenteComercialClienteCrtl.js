(function(){
	 angular.module('clientes.info.controller').controller('ModalEditAgenteComercialClienteCrtl', ModalEditAgenteComercialClienteCrtl) ;
        ModalEditAgenteComercialClienteCrtl.$inject = ['$uibModalInstance', 'data_cliente','empleadoService','clienteService'] ;

            function ModalEditAgenteComercialClienteCrtl($uibModalInstance, data_cliente,empleadoService, clienteService)
            {
                var vm =  this ;
                vm.fillSelected   = data_cliente ;
                vm.msj = "";

                vm.formData = {
                    nombre : vm.fillSelected.per_apellidos + ' ' + vm.fillSelected.per_nombre ,
                    cliente_id : vm.fillSelected.id,
                    agente_comercial_id : vm.fillSelected.agente_comercial_id,
                    agente_comercial : [],
                } ;

                vm.data_agentes       = [];

                init();
                function init(){
                    getEmpleadosInfoBasica() ;
                } ;

                function getEmpleadosInfoBasica()
                {
                    empleadoService.getEmpleadosInfoBasica().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_agentes = response.data ;
                                for (var i in vm.data_agentes)
                                {
                                    if (parseInt(vm.data_agentes[i].id) === parseInt(vm.fillSelected.empleado_id)) {
                                        vm.formData.agente_comercial =  vm.data_agentes[i] ;
                                        break ;
                                    };
                                }
                            }else
                            {
                                vm.msj = response.message ;
                            }
                        }
                    );
                }


                vm.ok = function ()
                {
                    var empleado_id_new         = vm.formData.agente_comercial.id ;
                    var empleado_id_old         = vm.fillSelected.empleado_id ;
                    var cliente_id              = vm.formData.cliente_id ;
                    var agente_comercial_id_now = vm.formData.agente_comercial_id ;

                    vm.msj = {message : ''} ;

                    if (empleado_id_new === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Agente Comercial' ;
                    };
                    if (parseInt(empleado_id_old) === parseInt(empleado_id_new))
                    {
                        return  vm.msj.message = 'El Agente Comercial no a cambiado' ;
                    };

                    var data = {
                        'empleado_id_new'       : empleado_id_new,
                        'agente_comercial_id'   : agente_comercial_id_now,
                        'cliente_id'            : cliente_id,
                    };

                    console.log(data);

                    clienteService.updateNewAgenteComercial(data).then(
                        function(response)
                        {
                            console.log(response);
                            if (!response.error)
                            {
                                $uibModalInstance.close(vm.formData);
                            }else
                            {
                                vm.msj = response ;
                            }
                        }
                    );

                };

                vm.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
})();