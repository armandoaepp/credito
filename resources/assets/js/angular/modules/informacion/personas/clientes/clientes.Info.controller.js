(function(){
    'use strict';

    angular.module('clientes.info.controller').controller('ClientesInfoCtrl',ClientesInfoCtrl);
    ClientesInfoCtrl.$inject = ['$filter', '$uibModal', 'PATH', 'modalService','NgTableParams','clienteService'] ;

        function ClientesInfoCtrl($filter, $uibModal, PATH, modalService,NgTableParams,clienteService)
        {
            var vm = this ;

            // function
                vm.getClientes         = getClientes ;
                vm.onClick             = onClick ;
                vm.newCliente          = newCliente ;
                vm.deleteCliente       = deleteCliente ;
                vm.editCliente         = editCliente ;
                vm.editAgenteComercial = editAgenteComercial ;

                vm.searchPersonas      = searchPersonas ;

            // variables
                vm.data_list     = [];
                vm.fillSelected  = [];

                vm.data_per_tipo = [{ per_tipo: "Todos", id: 0 },
                                        { per_tipo: "Personas", id: 1 },
                                        { per_tipo: "Empresas", id: 2 }
                                    ];
                vm.searchPerTipo = vm.data_per_tipo[0];

            init();
            function init() {
                tablePlugin() ;
                vm.getClientes() ;
            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getClientes();
                }
                else if (name === 'new')
                {
                      vm.newCliente('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editCliente('md');
                }
                else if (name === 'delete')
                {
                    vm.deleteCliente() ;
                }
                else if (name === 'update-agente')
                {
                    vm.editAgenteComercial() ;
                }
                else{
                    return ;
                };
            } ;

            function getClientes()
            {
                var per_tipo = vm.searchPerTipo.id ;
                var data = {
                    'per_tipo' : per_tipo,
                } ;

                clienteService.getClientes(data).then(
                    function(response){
                        if (!response.error)
                        {   vm.fillSelected = [];
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            } ;
            function searchPersonas(per_tipo)
            {
                getClientes() ;
            } ;

            // ===== ng-table ==============================================================================================

                // vm.cancel = cancel;
                // vm.del    = del;
                // vm.save   = save;
                // vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New
                function newCliente (size)
                {
                        var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/personas/clientes/new.cliente.tpl.html',
                            controller: 'ModalNewClienteCrtl',
                            controllerAs: 'vm',
                            size: size,
                            backdrop : 'static',
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getClientes() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

            //delete
                function deleteCliente()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalConfirm(vm.fillSelected) ;
                    }
                };

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Cliente',
                        bodyText: '¿Esta Seguro de Eliminar Cliente: '+ row.per_nombre + ' ' + row.per_apellidos+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                } ;

                function confirmDelete(row)
                {
                     var data = {
                            'cliente_id'   : row.id,
                            // 'persona_id'    : row.persona_id,
                            'estado'    : 0,
                        };

                        clienteService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getClientes() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

            //  edit
                function editCliente()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalEditCliente('md') ;
                    }
                };

                function modalEditCliente(size)
                {
                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/personas/clientes/edit.cliente.tpl.html',
                            controller: 'ModalEditClienteCrtl',
                            controllerAs: 'vm',
                            size: size,
                             backdrop : 'static',
                            resolve: {
                                data_cliente: function () { return vm.fillSelected; },
                            }
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getClientes() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

            // editar agente comercial
                function editAgenteComercial()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalEditAgenteComercial('md') ;
                    }
                };


                function modalEditAgenteComercial(size)
                {
                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/personas/clientes/edit.agente.comercial.tpl.html',
                            controller: 'ModalEditAgenteComercialClienteCrtl',
                            controllerAs: 'vm',
                            size: size,
                             backdrop : 'static',
                            resolve: {
                                data_cliente: function () { return vm.fillSelected; },
                            }
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getClientes() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

        }
})() ;

