(function(){
    'use strict';

    angular.module('roles.info.controller').controller('ModalAccesosRolCrtl', ModalAccesosRolCrtl) ;
    ModalAccesosRolCrtl.$inject = ['$filter','$uibModalInstance','data_rol', 'rolService', 'usersService'] ;

    function ModalAccesosRolCrtl($filter,$uibModalInstance,data_rol, rolService, usersService)
    {
        var vm =  this ;
        vm.expandAll = expandAll ;


        vm.fillSelected = data_rol ;
        vm.accesos_rol = [] ;

        init();
        function init(){
            getAccesosRol() ;
        }

        function getAccesosRol()
        {
            var data = {
                    'rol_id' : vm.fillSelected.id,
                };

            rolService.getAccesosRol(data).then(
                    function(response){
                        var data = response.data;

                        // vm.accesos_rol = buildTree(data,null) ;
                        vm.accesos_rol =  $filter('builTreeParentInChildrenFilter')(data,null) ;
                        return vm.accesos_rol ;
                    }
                );
        } ;

        /*function buildTree(arr, parent) {
            var out = []
            for(var i in arr) {
                if(arr[i].control_padre_id === parent) {
                    var children = buildTree(arr, arr[i].id)

                    if(children.length) {
                        arr[i].children = children
                    }

                    var is_active = false ;
                    if (arr[i].is_active === 1)
                    {
                        is_active = true ;
                    };
                    arr[i].is_active = is_active ;

                    out.push(arr[i])
                }
            }
            return out
        } ;*/

        function expandAll(root)
        {
            for (var i = 0 ; i < root.length ; i++)
            {
               expandir(root[i])
            };

        } ;

        function expandir(root, setting){
            if(!setting){
                setting = ! root.isExpanded;
            }
            root.isExpanded = setting;
            root.children.forEach(function(branch){
                expandir(branch, setting);
            });
        };

        vm.ok = function ()
        {
            var arr = vm.accesos_rol ;
            vm.controles = $filter('deleteParentsOfChildrenFilter')(arr) ;

            var data = {
                'controles' : vm.controles,
                'rol_id' : vm.fillSelected.id,
            };
            // console.log(data);
            rolService.updateAccesosRol(data).then(
                function(response)
                {
                    // console.log(response);

                    if (!response.error)
                    {
                        $uibModalInstance.close();
                    }else
                    {
                        vm.msj = response.message ;
                    }
                }
            );

        };

        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };


    }

})() ;