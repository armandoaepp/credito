(function(){
    'use stric' ;

    angular.module('roles.info.controller').controller('ModalNewRolCrtl', ModalNewRolCrtl) ;
    ModalNewRolCrtl.$inject = ['$uibModalInstance', 'rolService'] ;

        function ModalNewRolCrtl($uibModalInstance, rolService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                };

                rolService.saveRol(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;