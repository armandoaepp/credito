(function(){
    'use strict';

    angular.module('roles.info.controller').controller('RolesInfoCtrl',RolesInfoCtrl);
    RolesInfoCtrl.$inject = ['$filter', 'botoneraFactory','rolService', '$uibModal', 'PATH', 'modalService','NgTableParams'] ;

        function RolesInfoCtrl($filter, botoneraFactory,rolService, $uibModal, PATH, modalService,NgTableParams)
        {
            var vm = this ;

            // function
                vm.getRoles   = getRoles ;
                vm.onClick    = onClick ;
                vm.newRol     = newRol ;
                vm.accesosRol = accesosRol ;

            // variables
                vm.data_list = [] ;
                vm.botones = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                vm.getRoles() ;
            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getRoles();
                }
                else if (name === 'new')
                {
                      vm.newRol('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }
                else if (name === 'delete')
                {
                    deleteRol() ;
                }
                else if (name === 'access')
                {
                    vm.accesosRol('md') ;
                }
                else{
                    return ;
                };

            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }

            }

            function getRoles()
            {
                rolService.getRoles().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable();
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            // ===== ng-table ==============================================================================================


                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 8,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'rol_id' : row.id,
                        'nombre' : row.nombre,
                    };

                    rolService.updateRol(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newRol (size)
                {
                        var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/accesos/roles/nuevo.rol.tpl.html',
                            controller: 'ModalNewRolCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getRoles(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );

                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        rolService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getRoles() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Rol',
                        bodyText: '¿Esta Seguro de Eliminar Rol: '+ row.data+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

            //  accesos a Rol
                function accesosRol()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalAccesosRol('md') ;
                    }
                };

                function modalAccesosRol (size)
                {
                        var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/accesos/roles/accesos.rol.tpl.html',
                            controller: 'ModalAccesosRolCrtl',
                            controllerAs: 'vm',
                            size: size,
                            backdrop : 'static',
                            resolve: {
                                data_rol: function () { return vm.fillSelected; },
                            }
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getRoles() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );

                };
        }
})() ;

