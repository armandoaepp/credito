(function(){
    'use strict';

    angular.module('usuarios.info.controller').controller('ModalEditUsersCrtl', ModalEditUsersCrtl) ;
    ModalEditUsersCrtl.$inject = ['$uibModalInstance','data_user', 'accesosService', 'usersService','rolService'] ;

    function ModalEditUsersCrtl($uibModalInstance,data_user, accesosService, usersService,rolService)
    {
        var vm =  this ;

        vm.fillSelected  = data_user ;
        vm.data_roles    = [];

        vm.formData = {
            nombre : vm.fillSelected.full_name,
            email : vm.fillSelected.email,
            rol : vm.fillSelected.rol,
            user_id : vm.fillSelected.id,
        } ;

        init();
        function init(){
            getRoles() ;
        } ;

        function getRoles()
        {
            rolService.getRoles().then(
                function(response)
                {
                    if (!response.error)
                    {
                        vm.data_roles = response.data ;
                        for (var i in vm.data_roles)
                        {
                            if (vm.data_roles[i].id === vm.fillSelected.rol_id) {
                                vm.formData.rol =  vm.data_roles[i] ;
                                break ;
                            };
                        }

                    }else
                    {
                        vm.msj = response.message ;
                    }
                }
            );

        }


        vm.ok = function ()
        {
            var data = {
                'rol_id': vm.formData.rol.id ,
                'user_id': vm.formData.user_id,
            };


            accesosService.updateUserRol(data).then(
                function(response)
                {
                    if (!response.error)
                    {
                        $uibModalInstance.close(vm.formData);
                    }else
                    {
                        vm.msj = response ;
                    }
                }
            );

        };

        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

})() ;