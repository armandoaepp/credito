(function(){
    'use strict';

    angular.module('usuarios.info.controller').controller('UsuariosInfoCtrl',UsuariosInfoCtrl);
    UsuariosInfoCtrl.$inject = ['botoneraFactory','$filter', '$uibModal', 'PATH', 'modalService','NgTableParams','usersService','accesosService'] ;

        function UsuariosInfoCtrl(botoneraFactory,$filter, $uibModal, PATH, modalService,NgTableParams,usersService,accesosService)
        {
            var vm = this ;

            // function
                vm.getUsers    = getUsers ;
                vm.onClick     = onClick ;
                vm.newUsers    = newUsers ;
                vm.deleteUser  = deleteUser ;
                vm.accesosUser = accesosUser ;
                vm.accesosUser = accesosUser ;
                vm.editUser    = editUser ;
            // variables
                vm.data_list = [] ;
                vm.fillSelected = [];


            init();
            function init() {
                tablePlugin() ;
                vm.getUsers() ;
            }

            function onClick(name, row)
            {
                // console.log(name);
                if (name === 'list')
                {
                    vm.getUsers();
                }
                else if (name === 'new')
                {
                      vm.newUsers('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editUser('md');
                }
                else if (name === 'delete')
                {
                    vm.deleteUser() ;
                }
                else if (name === 'access')
                {
                    vm.accesosUser('md') ;
                }
                else{
                    return ;
                };
            } ;

            function getUsers()
            {
                usersService.getUsers().then(
                    function(response){
                        if (!response.error)
                        {   vm.fillSelected = [];
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }

            // ===== ng-table ==============================================================================================

                // vm.cancel = cancel;
                // vm.del    = del;
                // vm.save   = save;
                // vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }

                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newUsers (size)
                {
                        var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/accesos/usuarios/new.user.tpl.html',
                            controller: 'ModalNewUsersCrtl',
                            controllerAs: 'vm',
                            size: size,
                             backdrop : 'static',
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getUsers() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

            //delete
                function deleteUser()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalConfirm(vm.fillSelected) ;
                    }
                };

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Usuario',
                        bodyText: '¿Esta Seguro de Eliminar Usuario: '+ row.full_name+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                } ;

                function confirmDelete(row)
                {
                     var data = {
                            'user_id'    : row.id,
                            'estado'    : 0,
                        };

                        usersService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getUsers() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

            //  Accesos
                function accesosUser()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalAccesosUser('md') ;
                    }
                };

                function modalAccesosUser(size)
                {
                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/accesos/usuarios/access.user.tpl.html',
                            controller: 'ModalAccesosUsersCrtl',
                            controllerAs: 'vm',
                            size: size,
                             backdrop : 'static',
                            resolve: {
                                data_user: function () { return vm.fillSelected; },
                            }
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getUsers() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

            //  edit
                function editUser()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalEditUser('md') ;
                    }
                };

                function modalEditUser(size)
                {
                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/accesos/usuarios/edit.user.tpl.html',
                            controller: 'ModalEditUsersCrtl',
                            controllerAs: 'vm',
                            size: size,
                             backdrop : 'static',
                            resolve: {
                                data_user: function () { return vm.fillSelected; },
                            }
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getUsers() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };
        }
})() ;

