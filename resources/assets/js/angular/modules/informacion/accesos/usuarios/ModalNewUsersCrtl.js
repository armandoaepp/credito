(function(){
	 angular.module('usuarios.info.controller').controller('ModalNewUsersCrtl', ModalNewUsersCrtl) ;
        ModalNewUsersCrtl.$inject = ['$uibModalInstance', 'usersService','personaNaturalService', 'rolService'] ;

            function ModalNewUsersCrtl($uibModalInstance ,usersService,personaNaturalService, rolService)
            {
                var vm =  this ;

                vm.msj = "";

                vm.formData = {
                    nombre : '',
                } ;
                vm.data_persons = [];
                vm.data_roles   = [];

                init();
                function init(){
                    getPerNaturalInfoBasica() ;
                    getRoles() ;
                }

                function getPerNaturalInfoBasica()
                {
                    personaNaturalService.getPerNaturalInfoBasica().then(
                        function(response)
                        {
                            // console.log(response);
                            if (!response.error)
                            {
                                vm.data_persons = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );

                }

                function getRoles()
                {
                    rolService.getRoles().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_roles = response.data ;
                            }else
                            {
                                vm.msj = response.message ;
                            }
                        }
                    );

                }


                vm.ok = function ()
                {
                    var persona_id = vm.formData.persona.persona_id ;
                    var mail       = vm.formData.persona.mail ;
                    var password   = vm.formData.password ;
                    var rol_id     = vm.formData.rol.id ;

                    if (mail === undefined)
                    {
                        return  vm.msj = 'Seleccione Persona' ;
                    };
                    if (password.length < 6)
                    {
                        return  vm.msj = 'Contraseña debe tener 6 caracteres como minimo' ;
                    };

                    if (rol_id === undefined )
                    {
                        return  vm.msj = 'Seleccione Rol ' ;
                    };

                    var data = {
                        'persona_id'    : persona_id,
                        'email'         : mail,
                        'password'      : password,
                        'rol_id'        : rol_id,
                    };

                    usersService.save(data).then(
                        function(response)
                        {

                            if (!response.error)
                            {
                                $uibModalInstance.close(vm.formData);
                            }else
                            {
                                vm.msj = response ;
                            }
                        }
                    );

                };

                vm.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
})();