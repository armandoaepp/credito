(function(){
    'use strict';

    angular.module('usuarios.info.controller').controller('ModalAccesosUsersCrtl', ModalAccesosUsersCrtl) ;
    ModalAccesosUsersCrtl.$inject = ['$uibModalInstance','data_user', 'accesosService', 'usersService','$filter'] ;

    function ModalAccesosUsersCrtl($uibModalInstance,data_user, accesosService, usersService,$filter)
    {
        var vm =  this ;

        vm.expandAll = expandAll ;

        vm.fillSelected = data_user ;
        vm.menus_accesos = [] ;
        init();
        function init(){
            getControlAccesosUser() ;
        }

        function getControlAccesosUser()
        {
            var data = {
                    'user_id' : vm.fillSelected.id,
                };

            accesosService.getControlAccesosUser(data).then(
                    function(response){
                        var data = response.data;
                        vm.menus_accesos =  $filter('builTreeParentInChildrenFilter')(data,null) ;
                    }
                );
        } ;

        function expandAll(root)
        {
            for (var i = 0 ; i < root.length ; i++)
            {
               expandir(root[i])
            };

        } ;

        function expandir(root, setting){
            if(!setting){
                setting = ! root.isExpanded;
            }
            root.isExpanded = setting;
            root.children.forEach(function(branch){
                expandir(branch, setting);
            });
        };


        vm.ok = function ()
        {
            var arr = vm.menus_accesos ;
            vm.controles = $filter('deleteParentsOfChildrenFilter')(arr) ;


            var data = {
                'controles' : vm.controles,
                'user_id' : vm.fillSelected.id,
            };

            accesosService.updateAccesosUser(data).then(
                function(response)
                {
                    // console.log(response);
                    if (!response.error)
                    {
                        $uibModalInstance.close(vm.formData);
                    }else
                    {
                        vm.msj = response ;
                    }
                }
            );

        };

        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

})() ;