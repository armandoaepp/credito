(function(){
    'use strict';

    angular.module('cargos.info.controller').controller('CargosInfoCtrl',CargosInfoCtrl);
    CargosInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','areaService','cargoService'] ;

        function CargosInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,areaService,cargoService)
        {
            var vm = this ;

            // function
                vm.getCargos   = getCargos ;
                vm.onClick    = onClick ;
                vm.newCargo     = newCargo ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                getAreas() ;
                vm.getCargos() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getCargos();
                }
                else if (name === 'new')
                {
                      vm.newCargo('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }
                else if (name === 'delete')
                {
                    deleteRol() ;
                }
                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            }

            function getCargos()
            {
                cargoService.getCargos().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            function getAreas()
            {
                areaService.getAreas().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_areas = response.data ;
                            // console.log(vm.data_areas );
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            }


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'cargo_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                        'area_id' : row.area.id,
                    };

                    cargoService.updateCargo(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getCargos() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newCargo (size)
                {
                        var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/registros/cargos/new.cargo.tpl.html',
                            controller: 'ModalNewCargoCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getCargos(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        cargoService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getCargos() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Cargo',
                        bodyText: '¿Esta Seguro de Eliminar Cargo: '+ row.nombre +' del Area: '+row.area.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;

