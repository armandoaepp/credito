(function(){
    'use stric' ;

    angular.module('cargos.info.controller').controller('ModalNewCargoCrtl', ModalNewCargoCrtl) ;
    ModalNewCargoCrtl.$inject = ['$uibModalInstance', 'areaService','cargoService'] ;

        function ModalNewCargoCrtl($uibModalInstance, areaService,cargoService)
        {
            var vm =  this ;

            vm.msj = "";
            vm.data_areas = [] ;

            vm.formData = {
                nombre : '',
                descripcion : '',
                area : [],
            }

            init();
            function init(){
                getAreas() ;
            } ;

            function getAreas()
            {
                areaService.getAreas().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_areas = response.data ;
                            // console.log(vm.data_areas );
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                    'area_id'         : vm.formData.area.id,

                };
                // console.log(data);
                cargoService.saveCargo(data).then(
                    function(response)
                    {
                        // console.log(response);

                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;