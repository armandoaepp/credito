(function(){
    'use stric' ;

    angular.module('tipoCambios.info.controller').controller('ModalNewTipoCambioCrtl', ModalNewTipoCambioCrtl) ;
    ModalNewTipoCambioCrtl.$inject = ['$uibModalInstance', 'tipoCambioService'] ;

        function ModalNewTipoCambioCrtl($uibModalInstance, tipoCambioService)
        {
            var vm =  this ;

            vm.msj = {
                message: '',
                data: [] ,
            } ;

            vm.formData = {
                nombre : '',
                // descripcion : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'valor' : vm.formData.valor ,
                    'compra' : 0 ,
                    'venta' : 0 ,
                    'dia' : '' ,
                };

                tipoCambioService.save(data).then(
                    function(response)
                    {
                        console.log(response);
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;