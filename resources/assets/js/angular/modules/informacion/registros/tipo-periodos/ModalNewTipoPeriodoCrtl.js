(function(){
    'use stric' ;

    angular.module('tipoPeriodos.info.controller').controller('ModalNewTipoPeriodoCrtl', ModalNewTipoPeriodoCrtl) ;
    ModalNewTipoPeriodoCrtl.$inject = ['$uibModalInstance', 'tipoPeriodoService'] ;

        function ModalNewTipoPeriodoCrtl($uibModalInstance, tipoPeriodoService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                rango : '',
                descripcion : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'      : vm.formData.nombre,
                    'rango'       : vm.formData.rango,
                    'descripcion' : vm.formData.descripcion,
                };

                tipoPeriodoService.save(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;