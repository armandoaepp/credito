(function(){
    'use stric' ;

    angular.module('tipoGarantias.info.controller').controller('ModalNewTipoGarantiaCrtl', ModalNewTipoGarantiaCrtl) ;
    ModalNewTipoGarantiaCrtl.$inject = ['$uibModalInstance', 'tipoGarantiaService'] ;

        function ModalNewTipoGarantiaCrtl($uibModalInstance, tipoGarantiaService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                descripcion : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                };

                tipoGarantiaService.save(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;