(function(){
    'use stric' ;

    angular.module('licencias.info.controller').controller('ModalNewLicenciaCrtl', ModalNewLicenciaCrtl) ;
    ModalNewLicenciaCrtl.$inject = ['$uibModalInstance', 'licenciaService'] ;

        function ModalNewLicenciaCrtl($uibModalInstance, licenciaService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                descripcion : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                };

                licenciaService.save(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;