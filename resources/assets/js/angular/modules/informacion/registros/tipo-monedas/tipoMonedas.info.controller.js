(function(){
    'use strict';

    angular.module('tipoMonedas.info.controller').controller('TipoMonedasInfoCtrl',TipoMonedasInfoCtrl);
    TipoMonedasInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','tipoMonedaService'] ;

        function TipoMonedasInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,tipoMonedaService)
        {
            var vm = this ;

            // function
                vm.getTipoMonedas   = getTipoMonedas ;
                vm.onClick    = onClick ;
                vm.newTipoMoneda     = newTipoMoneda ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                vm.getTipoMonedas() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getTipoMonedas();
                }
                else if (name === 'new')
                {
                      vm.newTipoMoneda('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }

                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            }

            function getTipoMonedas()
            {
                tipoMonedaService.getTipoMonedas().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'tipo_moneda_id' : row.id,
                        'nombre' : row.nombre,
                        'simbolo' : row.simbolo,
                    };

                    tipoMonedaService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getTipoMonedas() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newTipoMoneda (size)
                {

                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/registros/tipo-monedas/new.tipo-moneda.tpl.html',
                            controller: 'ModalNewTipoMonedaCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getTipoMonedas(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        tipoMonedaService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getTipoMonedas() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    // console.log(row) ;
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Tipo de Moneda',
                        bodyText: '¿Esta Seguro de Eliminar Tipo de Moneda: '+ row.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;

