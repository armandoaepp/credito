(function(){
    'use stric' ;

    angular.module('tipoMonedas.info.controller').controller('ModalNewTipoMonedaCrtl', ModalNewTipoMonedaCrtl) ;
    ModalNewTipoMonedaCrtl.$inject = ['$uibModalInstance', 'tipoMonedaService'] ;

        function ModalNewTipoMonedaCrtl($uibModalInstance, tipoMonedaService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                simbolo : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'simbolo'  : vm.formData.simbolo,
                };

                tipoMonedaService.save(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;