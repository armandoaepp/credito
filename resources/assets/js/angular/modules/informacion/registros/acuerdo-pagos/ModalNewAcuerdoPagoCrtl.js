(function(){
    'use stric' ;

    angular.module('acuerdoPagos.info.controller').controller('ModalNewAcuerdoPagoCrtl', ModalNewAcuerdoPagoCrtl) ;
    ModalNewAcuerdoPagoCrtl.$inject = ['$uibModalInstance', 'acuerdoPagoService','tipoPeriodoService'] ;

        function ModalNewAcuerdoPagoCrtl($uibModalInstance, acuerdoPagoService,tipoPeriodoService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                descripcion : '',
                rango : '',
                partes : '',
                tipo_periodo : [],
            } ;

            vm.data_tipo_periodos = [] ;

            init() ;
            function init()
            {
                getTipoPeriodos() ;
            };

            function getTipoPeriodos()
            {
                tipoPeriodoService.getTipoPeriodos().then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data_tipo_periodos = response.data;
                            return vm.data_tipo_periodos ;
                        }
                    }
                );
            };

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                    'rango'  : vm.formData.rango,
                    'partes'  : vm.formData.partes,
                    'tipo_periodo_id' : vm.formData.tipo_periodo.id,
                };
                console.log(data);
                acuerdoPagoService.save(data).then(
                    function(response)
                    {
                        console.log(response);
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;