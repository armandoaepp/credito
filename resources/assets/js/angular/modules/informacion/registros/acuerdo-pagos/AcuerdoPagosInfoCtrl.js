(function(){
    'use strict';

    angular.module('acuerdoPagos.info.controller').controller('AcuerdoPagosInfoCtrl',AcuerdoPagosInfoCtrl);
    AcuerdoPagosInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','acuerdoPagoService','tipoPeriodoService'] ;

        function AcuerdoPagosInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,acuerdoPagoService,tipoPeriodoService)
        {
            var vm = this ;

            // function
                vm.getAcuerdoPagos   = getAcuerdoPagos ;
                vm.onClick    = onClick ;
                vm.newAcuerdoPago     = newAcuerdoPago ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;

                vm.data_tipo_prestamos = [] ;

            init();
            function init() {
                tablePlugin() ;
                vm.getAcuerdoPagos() ;
                getTipoPeriodos() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getAcuerdoPagos();
                }
                else if (name === 'new')
                {
                      vm.newAcuerdoPago('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }

                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            };

            function getAcuerdoPagos()
            {
                acuerdoPagoService.getAcuerdoPagos().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            } ;


            function getTipoPeriodos()
            {
                tipoPeriodoService.getTipoPeriodos().then(
                    function(response){
                        if (!response.error)
                        {
                            console.log(response.data);
                            vm.data_tipo_prestamos = response.data;
                            return vm.data_tipo_prestamos ;
                        }
                    }
                );
            };


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'acuerdo_pago_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                        'rango' : row.rango,
                        'partes' : row.partes,
                        'tipo_periodo_id' : row.tipo_periodo.id,
                    };

                    acuerdoPagoService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getAcuerdoPagos() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newAcuerdoPago (size)
                {

                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/registros/acuerdo-pagos/new.acuerdo-pago.tpl.html',
                            controller: 'ModalNewAcuerdoPagoCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getAcuerdoPagos(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        acuerdoPagoService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getAcuerdoPagos() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    // console.log(row) ;
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Acuerdo de Prestamo',
                        bodyText: '¿Esta Seguro de Eliminar Acuerdo de Prestamo: '+ row.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;

