(function(){
    'use stric' ;

    angular.module('areas.info.controller').controller('ModalNewAreaCrtl', ModalNewAreaCrtl) ;
    ModalNewAreaCrtl.$inject = ['$uibModalInstance', 'areaService'] ;

        function ModalNewAreaCrtl($uibModalInstance, areaService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                descripcion : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                };

                areaService.saveArea(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;