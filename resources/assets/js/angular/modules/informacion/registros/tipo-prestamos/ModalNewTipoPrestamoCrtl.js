(function(){
    'use stric' ;

    angular.module('areas.info.controller').controller('ModalNewTipoPrestamoCrtl', ModalNewTipoPrestamoCrtl) ;
    ModalNewTipoPrestamoCrtl.$inject = ['$uibModalInstance', 'tipoPrestamoService'] ;

        function ModalNewTipoPrestamoCrtl($uibModalInstance, tipoPrestamoService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                descripcion : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                };

                tipoPrestamoService.save(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;