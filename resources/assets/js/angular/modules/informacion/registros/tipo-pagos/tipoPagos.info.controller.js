(function(){
    'use strict';

    angular.module('tipoPagos.info.controller').controller('TipoPagosInfoCtrl',TipoPagosInfoCtrl);
    TipoPagosInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','tipoPagoService','tipoPrestamoService'] ;

        function TipoPagosInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,tipoPagoService,tipoPrestamoService)
        {
            var vm = this ;

            // function
                vm.getTipoPagos   = getTipoPagos ;
                vm.onClick    = onClick ;
                vm.newTipoPago     = newTipoPago ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;

                vm.data_tipo_prestamos = [] ;

            init();
            function init() {
                tablePlugin() ;
                vm.getTipoPagos() ;
                getTipoPrestamos() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getTipoPagos();
                }
                else if (name === 'new')
                {
                      vm.newTipoPago('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }

                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            };

            function getTipoPagos()
            {
                tipoPagoService.getTipoPagos().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            } ;


            function getTipoPrestamos()
            {
                tipoPrestamoService.getTipoPrestamos().then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data_tipo_prestamos = response.data;
                            return vm.data_tipo_prestamos ;
                        }
                    }
                );
            };


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'tipo_pago_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                        'tipo_prestamo_id' : row.tipo_prestamo.id,
                    };

                    tipoPagoService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getTipoPagos() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newTipoPago (size)
                {

                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/registros/tipo-pagos/new.tipo-pago.tpl.html',
                            controller: 'ModalNewTipoPagoCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getTipoPagos(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        tipoPagoService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getTipoPagos() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    // console.log(row) ;
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Tipo de Prestamo',
                        bodyText: '¿Esta Seguro de Eliminar Tipo de Prestamo: '+ row.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;

