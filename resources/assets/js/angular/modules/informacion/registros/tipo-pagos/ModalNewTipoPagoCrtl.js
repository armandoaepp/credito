(function(){
    'use stric' ;

    angular.module('tipoPagos.info.controller').controller('ModalNewTipoPagoCrtl', ModalNewTipoPagoCrtl) ;
    ModalNewTipoPagoCrtl.$inject = ['$uibModalInstance', 'tipoPagoService','tipoPrestamoService'] ;

        function ModalNewTipoPagoCrtl($uibModalInstance, tipoPagoService,tipoPrestamoService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                descripcion : '',
                tipo_prestamo : [],
            } ;

            vm.data_tipo_prestamos = [] ;

            init() ;
            function init()
            {
                getTipoPrestamos() ;
            };

            function getTipoPrestamos()
            {
                tipoPrestamoService.getTipoPrestamos().then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data_tipo_prestamos = response.data;
                            return vm.data_tipo_prestamos ;
                        }
                    }
                );
            };

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                    'tipo_prestamo_id' : vm.formData.tipo_prestamo.id,
                };

                tipoPagoService.save(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;