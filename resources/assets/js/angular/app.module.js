angular
	.module('appPlataforma', [
		'app.core',
		'app.directives',
		'app.layout' ,
		'app.filter',
		'app.informacion',
		'app.movimientos',
	]);


angular
	.module('app.core', [
		'ui.router',
		'ui.bootstrap',
		'paths.constant',
		'ngSanitize',
		'app.plugin'

	]);

//  informacion global del layout
angular
	.module('app.layout', [
		'users.controller',
		'menu.controller',
		'menu.directive',
	]);

// plugin para app
angular
	.module('app.plugin', [
		'angular.vertilize', /* poner del mismo alto elements html*/
		'ngTagsInput',
		'xeditable',
		'ng-sortable', // reordenar item
		'ngTable',
		'ui.select',
		// 'ivh.treeview', // tre list check
		'AngularBootstrapTree',
		'angularFileUpload'
		]);

//  module de informacion
angular
	.module('app.informacion', [
		'app.core',
		'app.directives',
		'roles.info.controller',
		'persona.natural.info.controller',
		'usuarios.info.controller',
		'empleados.info.controller',
		'clientes.info.controller',
		'areas.info.controller',
		'cargos.info.controller',
		'tipoPrestamos.info.controller',
		'tipoMonedas.info.controller',
		'tipoPeriodos.info.controller',
		'tipoGarantias.info.controller',
		'tipoPagos.info.controller',
		'licencias.info.controller',
		'tipoCambios.info.controller',
		'acuerdoPagos.info.controller',
		'personas.juridicas.info.controller',
		'tipoVehiculos.info.controller',
		'claseVehiculos.info.controller',
		'marcas.info.controller',
		'modelos.info.controller',
		'vehiculos.info.controller',

		]);


//  module de informacion
angular
	.module('app.movimientos', [
		'app.core',
		'app.directives',
		'prestamos.mov.controller',
		'cred.menor.mov.controller',

		]);

//  modules directivas
angular
	.module('app.directives', [
	    'validate.directive',
	    'modal.directive',
	]);

// controllers gobals
	angular.module('menu.controller', ['accesos.service','menu.factory']);
	angular.module('users.controller', ['users.service']);

	// controlles module informacion
		angular.module('persona.natural.info.controller', ['modal.service', 'personaNatural.service','perTelefono.service','perMail.service','perDocumento.service']);

		angular.module('roles.info.controller', ['rol.service', 'modal.service']);
		angular.module('usuarios.info.controller', ['AngularBootstrapTree','modal.service','accesos.service','users.service','personaNatural.service','rol.service','control.service']);
		angular.module('areas.info.controller', ['modal.service','area.service']);
		angular.module('cargos.info.controller', ['modal.service','area.service','cargo.service']);
		angular.module('empleados.info.controller', ['modal.service','empleado.service','area.service']);
		angular.module('clientes.info.controller', ['modal.service','cliente.service','empleado.service','persona.service']);
		// registros
		angular.module('tipoPrestamos.info.controller', ['modal.service','tipoPrestamo.service']);
		angular.module('tipoMonedas.info.controller', ['modal.service','tipoMoneda.service']);
		angular.module('tipoPeriodos.info.controller', ['modal.service','tipoPeriodo.service']);
		angular.module('tipoGarantias.info.controller', ['modal.service','tipoGarantia.service']);
		angular.module('tipoPagos.info.controller', ['modal.service','tipoPago.service','tipoPrestamo.service']);
		angular.module('licencias.info.controller', ['modal.service','licencia.service']);
		angular.module('tipoCambios.info.controller', ['modal.service','tipoCambio.service']);
		angular.module('acuerdoPagos.info.controller', ['modal.service','acuerdoPago.service']);

		angular.module('personas.juridicas.info.controller', ['modal.service','perJuridica.service','ubigeo.service','perTelefono.service','perMail.service','perDocumento.service','perWeb.service']);

		// VEHICULOS
		angular.module('claseVehiculos.info.controller', ['modal.service','claseVehiculo.service']);
		angular.module('tipoVehiculos.info.controller', ['modal.service','tipoVehiculo.service','claseVehiculo.service']);
		angular.module('marcas.info.controller', ['modal.service','marca.service']);
		angular.module('modelos.info.controller', ['modal.service','modelo.service','marca.service']);
		angular.module('vehiculos.info.controller', ['modal.service','modelo.service','vehiculo.service']);


	// controller dule movimientos
		angular.module('prestamos.mov.controller', [
													'modal.service',
													'tipoPrestamo.service',
													'tipoMoneda.service',
													'tipoPeriodo.service',
													'tipoGarantia.service',
													'tipoPago.service',
													'empleado.service',
													'vehiculo.service',
													'aval.service',
													'prestamo.service',
													'cuota.service',
													'acuerdoPago.service',
													'tipoEstado.service',
													'prestamoEstado.service',

													]);
		angular.module('cred.menor.mov.controller', [
													'modal.service',
													'tipoPrestamo.service',
													'tipoMoneda.service',
													'tipoPeriodo.service',
													'tipoGarantia.service',
													'tipoPago.service',
													'empleado.service',
													'vehiculo.service',
													'aval.service',
													'prestamo.service',
													'cuota.service',
													'acuerdoPago.service',
													'tipoEstado.service',
													'prestamoEstado.service',
													'creditoMenor.service',
													'prestamoGarantia.service',
													'prestamoReferencia.service'

													]);


// directives
	angular.module('menu.directive', []);
	angular.module('validate.directive', []);
	angular.module('modal.directive', []);

// service
	angular.module('accesos.service', []) ;
	angular.module('users.service', []) ;
	angular.module('rol.service', []) ;
	angular.module('persona.service', []) ;
	angular.module('personaNatural.service', []) ;
	angular.module('perTelefono.service', []) ;
	angular.module('perMail.service', []) ;
	angular.module('perDocumento.service', []) ;
	angular.module('control.service', []) ;
	angular.module('empleado.service', []) ;
	angular.module('cliente.service', []) ;
	angular.module('perJuridica.service', []) ;
	angular.module('ubigeo.service', []) ;
	angular.module('perWeb.service', []) ;
	angular.module('aval.service', []) ;

	// registros
	angular.module('area.service', []) ;
	angular.module('cargo.service', []) ;

	angular.module('tipoPrestamo.service', []) ;
	angular.module('tipoMoneda.service', []) ;
	angular.module('tipoPeriodo.service', []) ;
	angular.module('tipoGarantia.service', []) ;
	angular.module('tipoPago.service', []) ;
	angular.module('licencia.service', []) ;
	angular.module('tipoCambio.service', []) ;
	angular.module('acuerdoPago.service', []) ;

	// vehiculos
	angular.module('claseVehiculo.service', []) ;
	angular.module('tipoVehiculo.service', []) ;
	angular.module('marca.service', []) ;
	angular.module('modelo.service', []) ;
	angular.module('vehiculo.service', []) ;
	// movimientos
		// operaciones
		angular.module('prestamo.service', []) ;
		angular.module('cuota.service', []) ;
		angular.module('tipoEstado.service', []) ;
		angular.module('prestamoEstado.service', []) ;
		angular.module('creditoMenor.service', []) ;
		angular.module('prestamoGarantia.service', []) ;
		angular.module('prestamoReferencia.service', []) ;


// services pulgin
angular.module('modal.service', []) ;

// factory
angular.module('menu.factory', []) ;



// filters
angular.module('app.filter', [
		'controles.filter',
	]);

angular.module('controles.filter', []) ;


angular.module('paths.constant', []) ;

