(function() {

    // recomendacion tener cuidado en utilizar los filter puede consumir recursos
    angular.module('controles.filter').filter("arrayItemsFilter", arrayItemsFilter);

    arrayItemsFilter.$inject = [];

    function arrayItemsFilter($q) {

        return arrayItems;

        function arrayItems(items, props) {
            var out = [];

            //  texto es el mismo que se buscara para todos de elementos (por ejemplo {name: "12", age: "12"})
            var keys = Object.keys(props);
            var firstElement = keys[0];

            var text = String(props[firstElement]).toLowerCase();
            // text diferente de nulo,  recoremos el array
            if (angular.isArray(items))
            {
                for (var i = 0; i < items.length; i++)
                {
                    var item = items[i];
                    var itemMatches = false;
                    var prop = keys[0];
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                }

            } else {
                // Let the output be the input untouched
                out = items;
            }
            return out;
        };

    }

})();
