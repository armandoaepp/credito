(function(){

 // recomendacion tener cuidado en utilizar los filter puede consumir recursos
angular.module('controles.filter').filter("builTreeParentInChildrenFilter", builTreeParentInChildrenFilter ) ;

 builTreeParentInChildrenFilter.$inject = [];
function builTreeParentInChildrenFilter($q) {

    return buildTree ;

        function buildTree(arr, parentId)
        {
             var out = []
            for(var i in arr) {
                if(arr[i].control_padre_id === parentId) {
                    var children = [] ;
                    children = buildTree(arr, arr[i].id)

                    if(children.length > 0) {
                        for(var j in children)
                        {
                            children[j].parent   =  arr[i] ;
                        }

                    }
                    arr[i].children = children ;

                    var is_active = false ;
                    var valor = parseInt(arr[i].is_active );
                    if (valor === 1)
                    {
                        is_active = true ;
                    };

                    arr[i].is_active = is_active ;
                    arr[i].isExpanded = false;
                    arr[i].isSelected = is_active;
                    out.push(arr[i])
                }
            }
            return out

        } ;


}

})() ;

