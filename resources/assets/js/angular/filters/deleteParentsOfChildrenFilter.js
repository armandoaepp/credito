(function(){

 // recomendacion tener cuidado en utilizar los filter puede consumir recursos
angular.module('controles.filter').filter("deleteParentsOfChildrenFilter", deleteParentsOfChildrenFilter ) ;

 deleteParentsOfChildrenFilter.$inject = [];
function deleteParentsOfChildrenFilter($q) {

    return deleteParentsOfChildren ;

        function deleteParentsOfChildren(data)
        {
            for(var i in data)
            {
                    data[i].parent= '' ;

                    var children = data[i].children ;
                    if (children)
                    {
                        deleteParentsOfChildren(children) ;
                    }
            }
            return data ;

        }
}

})() ;

