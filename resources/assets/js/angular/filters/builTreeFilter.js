(function(){

 // recomendacion tener cuidado en utilizar los filter puede consumir recursos
angular.module('controles.filter').filter("buildTreeFilter", buildTreeFilter ) ;

 buildTreeFilter.$inject = [];
function buildTreeFilter($q) {

    return buildTree ;

        function buildTree(arr, parentId)
        {
             var out = []
            for(var i in arr) {
                if(arr[i].control_padre_id === parentId) {
                    var children = [] ;
                    children = buildTree(arr, arr[i].id)

                    arr[i].children = children ;

                    var is_active = false ;
                    if (arr[i].is_active === 1)
                    {
                        is_active = true ;
                    };
                    arr[i].is_active = is_active ;
                    arr[i].isExpanded = false;
                    arr[i].isSelected = is_active;

                    out.push(arr[i])
                }
            }
            return out

        } ;


}

})() ;

