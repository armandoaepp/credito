(function(){
    'use strict';
    angular
        .module('accesos.service')
        .service('accesosService', accesosService);

        accesosService.$inject = ['$http'];

        function accesosService($http){

            this.getAccesos            = getAccesos ;
            this.getControlAccesosUser = getControlAccesosUser ;
            this.updateAccesosUser     = updateAccesosUser ;
            this.updateUserRol         = updateUserRol ;

            var path = 'web/accesos' ;

            function getAccesos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getControlAccesosUser(params_){
                var url_ = path+'/user';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateAccesosUser(params_){
                var url_ = path+'/user/update-accesos';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateUserRol(params_){
                var url_ = path+'/user/update-rol';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();