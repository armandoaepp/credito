var appHttp =(function(window, undefined){
    // perticiones http desde cualquier nivel
       var getHttp = function($http, url_ , params_){
            return $http.get(url_, params_)
                    .then(getServiceComplete)
                    .catch(getServiceFailed) ;
        }

        var postHttp = function($http, url_ , params_){
            return $http.post(url_, params_)
                    .then(getServiceComplete)
                    .catch(getServiceFailed) ;
        }

        function getServiceComplete(response){
            return response.data ;
        }

        function getServiceFailed(errors ){
            var data = [{message:'timeService.getPostsFaild:',status: errors.status, statusText:errors.statusText,error:true}];
            console.log(data) ;
            // console.log(errors) ;
            return data ;

        }

        return {
            getHttp: getHttp,
            postHttp : postHttp
          }
})(window);