(function(){
    'use strict';
    angular
        .module('prestamo.service')
        .service('prestamoService', prestamoService);

        prestamoService.$inject = ['$http'];

        function prestamoService($http){

            this.getPrestamos                   = getPrestamos ;
            this.save                           = save ;
            this.getPrestamoById                = getPrestamoById ;
            this.update                         = update ;
            this.updateEstadoPrestamoTipoEstado = updateEstadoPrestamoTipoEstado ;
            this.getPrestamosByTipoEstadoId     = getPrestamosByTipoEstadoId ;
            this.updatePrestamoTipoEstado       = updatePrestamoTipoEstado ;

            var path = 'operaciones/prestamos' ;

            function getPrestamos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPrestamoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstadoPrestamoTipoEstado(params_){
                var url_ = path+'/update/estado-all';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPrestamosByTipoEstadoId(params_){
                var url_ = path+'/tipo-estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function updatePrestamoTipoEstado(params_){
                var url_ = path+'/update/prestamo-tipo-estado';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();