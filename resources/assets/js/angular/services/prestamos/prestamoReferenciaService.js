(function(){
    'use strict';
    angular
        .module('prestamoReferencia.service')
        .service('prestamoReferenciaService', prestamoReferenciaService);

        prestamoReferenciaService.$inject = ['$http'];

        function prestamoReferenciaService($http){

            this.getPretamoReferenciasByPrestamoId     = getPretamoReferenciasByPrestamoId ;
            this.getPretamoReferenciasByPrestamoIdTipo = getPretamoReferenciasByPrestamoIdTipo ;
            this.updateEstado                          = updateEstado ;

            var path = 'operaciones/prestamo-referencias' ;

            function getPretamoReferenciasByPrestamoId(params_){
                var url_ = path+'/get/byprestamoid';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPretamoReferenciasByPrestamoIdTipo(params_){
                var url_ = path+'/get/byprestamoid-tipo';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();