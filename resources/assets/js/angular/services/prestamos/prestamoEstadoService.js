(function(){
    'use strict';
    angular
        .module('prestamoEstado.service')
        .service('prestamoEstadoService', prestamoEstadoService);

        prestamoEstadoService.$inject = ['$http'];

        function prestamoEstadoService($http){

            this.getPrestamoEstados    = getPrestamoEstados ;
            this.getPrestamoEstadosInfo    = getPrestamoEstadosInfo ;

            var path = 'operaciones/prestamo-estados' ;


            function getPrestamoEstados(){
                var url_ = path+'';
                var params_ = null ;
                return appHttp.getHttp($http,url_, params_);
            };

            function getPrestamoEstadosInfo(params_){
                var url_ = path+'/get/info';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();