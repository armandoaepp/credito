(function(){
    'use strict';
    angular
        .module('tipoEstado.service')
        .service('tipoEstadoService', tipoEstadoService);

        tipoEstadoService.$inject = ['$http'];

        function tipoEstadoService($http){

            this.getTipoEstados    = getTipoEstados ;

            var path = 'operaciones/tipo-estados' ;


            function getTipoEstados(params_){
                var url_ = path+'';
                return appHttp.getHttp($http,url_, params_);
            };

        }
})();