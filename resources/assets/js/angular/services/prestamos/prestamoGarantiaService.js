(function(){
    'use strict';
    angular
        .module('prestamoGarantia.service')
        .service('prestamoGarantiaService', prestamoGarantiaService);

        prestamoGarantiaService.$inject = ['$http'];

        function prestamoGarantiaService($http){

            this.getPretamoGarantiasByPrestamoId = getPretamoGarantiasByPrestamoId ;
            this.updateEstado                    = updateEstado ;

            var path = 'operaciones/prestamo-garantias' ;

            function getPretamoGarantiasByPrestamoId(params_){
                var url_ = path+'/get/byprestamoid';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();