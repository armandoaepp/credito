(function(){
    'use strict';
    angular
        .module('cuota.service')
        .service('cuotaService', cuotaService);

        cuotaService.$inject = ['$http'];

        function cuotaService($http){

            this.getCuotasByPrestamoId    = getCuotasByPrestamoId ;

            var path = 'operaciones/cuotas' ;


            function getCuotasByPrestamoId(params_){
                var url_ = path+'/get/by-prestamo-id';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();