(function(){
    'use strict';
    angular
        .module('perWeb.service')
        .service('perWebService', perWebService);

        perWebService.$inject = ['$http'];

        function perWebService($http)
        {

            this.getWebsByPersonaId = getWebsByPersonaId ;
            this.save               = save ;
            this.update             = update ;
            this.getPerWebById      = getPerWebById ;
            this.deleteFill         = deleteFill ;
            this.updateEstado       = updateEstado ;
            this.reOrderItems       = reOrderItems ;

            this.getTipoWebs = getTipoWebs ;

            var path = 'web/webs' ;

            function getWebsByPersonaId(params_){
                var url_ = path+'/get/persona/id';
                return appHttp.postHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function deleteFill(params_){
                var url_ = path+'/delete';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPerWebById(params_){
                var url_ = path+'/get/id';
                return appHttp.postHttp($http,url_, params_);
            };


            function reOrderItems(params_){
                var url_ = path+'/reorder-items';
                return appHttp.postHttp($http,url_, params_);
            };

            // tipos de webs
            function getTipoWebs(){
                var url_ = path+'/get/tipo-webs';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };
        }
})();