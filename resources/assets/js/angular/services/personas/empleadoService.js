(function(){
    'use strict';
    angular
        .module('empleado.service')
        .service('empleadoService', empleadoService);

        empleadoService.$inject = ['$http'];

        function empleadoService($http){

            this.getEmpleados           = getEmpleados ;
            this.save                   = save ;
            this.getEmpleadoById        = getEmpleadoById ;
            this.update                 = update ;
            this.updateEstado           = updateEstado ;
            this.asignarNewCargo        = asignarNewCargo ;
            this.getEmpleadosInfoBasica = getEmpleadosInfoBasica ;

            var path = 'web/empleados' ;

            function getEmpleados(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getEmpleadoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function asignarNewCargo(params_){
                var url_ = path+'/asignar/new-cargo';
                return appHttp.postHttp($http,url_, params_);
            };

            function getEmpleadosInfoBasica(params_){
                var url_ = path+'/get/info-basica';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

        }
})();