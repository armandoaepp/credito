(function(){
    'use strict';
    angular
        .module('persona.service')
        .service('personaService', personaService);

        personaService.$inject = ['$http'];

        function personaService($http)
        {

            this.getPersonasInfoBasica    = getPersonasInfoBasica ;


            var path = 'web/personas' ;

            function getPersonasInfoBasica(params_){
                var url_ = path+'/info-basica';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();