(function(){
    'use strict';
    angular
        .module('perJuridica.service')
        .service('perJuridicaService', perJuridicaService);

        perJuridicaService.$inject = ['$http'];

        function perJuridicaService($http)
        {

            this.getPerJuridicas          = getPerJuridicas ;
            this.getPerJuridicaInfoAll    = getPerJuridicaInfoAll ;
            this.getPerJuridicaInfoBasica = getPerJuridicaInfoBasica ;
            this.save                     = save ;
            this.update                   = update ;
            this.updateEstado             = updateEstado ;
            this.getPerJuridicaById       = getPerJuridicaById ;
            this.getPerJuridicaByRuc      = getPerJuridicaByRuc ; //
            this.updateRuc                = updateRuc ;
            this.updatePerJuridicaInfo    = updatePerJuridicaInfo ;


            var path = 'web/personas/empresas' ;

            function getPerJuridicas(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getPerJuridicaInfoAll(params_){
               var url_ = path+'/get/infoall';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPerJuridicaInfoBasica(){
                var url_ = path+'/get/info-basica';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPerJuridicaById(params_){
                var url_ = path+'/get/id';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPerJuridicaByRuc(params_){
                var url_ = path+'/get/ruc';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateRuc(params_){
                var url_ = path+'/update/ruc';
                return appHttp.postHttp($http,url_, params_);
            };

            function updatePerJuridicaInfo(params_){
                var url_ = path+'/update/info';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();