(function(){
    'use strict';
    angular
        .module('cliente.service')
        .service('clienteService', clienteService);

        clienteService.$inject = ['$http'];

        function clienteService($http){

            this.getClientes    = getClientes ;
            this.save           = save ;
            this.getClienteById = getClienteById ;
            this.update         = update ;
            this.updateEstado   = updateEstado ;

            this.getClientesInfoBasica    = getClientesInfoBasica ;

            this.updateNewAgenteComercial    = updateNewAgenteComercial ;

            var path = 'web/clientes' ;

            function getClientes(params_){
                 var url_ = path+'';
                return appHttp.postHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getClienteById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateNewAgenteComercial(params_){
                var url_ = path+'/update/agente-comercial';
                return appHttp.postHttp($http,url_, params_);
            };

            function getClientesInfoBasica(params_){
                var url_ = path+'/get/info-basica';
                return appHttp.postHttp($http,url_, params_);
            };

         }
})();