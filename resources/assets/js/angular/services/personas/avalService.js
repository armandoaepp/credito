(function(){
    'use strict';
    angular
        .module('aval.service')
        .service('avalService', avalService);

        avalService.$inject = ['$http'];

        function avalService($http)
        {

            this.getAvalesInfoBasica    = getAvalesInfoBasica ;


            var path = 'web/avales' ;

            function getAvalesInfoBasica(params_){
                var url_ = path+'/get/info-basica';
                return appHttp.getHttp($http,url_, params_);
            };


        }
})();