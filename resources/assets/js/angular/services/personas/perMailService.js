(function(){
    'use strict';
    angular
        .module('perMail.service')
        .service('perMailService', perMailService);

        perMailService.$inject = ['$http'];

        function perMailService($http)
        {

            this.getMailsByPersonaId = getMailsByPersonaId ;
            this.save                = save ;
            this.update              = update ;
            this.deleteFill          = deleteFill ;
            this.reOrderItems        = reOrderItems ;


            var path = 'web/mails' ;

            function getMailsByPersonaId(params_){
                var url_ = path+'/get/persona/id';
                return appHttp.postHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function deleteFill(params_){
                var url_ = path+'/delete';
                return appHttp.postHttp($http,url_, params_);
            };

            function reOrderItems(params_){
                var url_ = path+'/reorder-items';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();