(function(){
    'use strict';
    angular
        .module('perDocumento.service')
        .service('perDocumentoService', perDocumentoService);

        perDocumentoService.$inject = ['$http'];

        function perDocumentoService($http)
        {
            this.update     = update ;
            this.save       = save ;
            this.deleteFill = deleteFill ;

            var path = 'web/documentos' ;

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function deleteFill(params_){
                var url_ = path+'/delete';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();