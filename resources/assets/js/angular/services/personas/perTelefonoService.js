(function(){
    'use strict';
    angular
        .module('perTelefono.service')
        .service('perTelefonoService', perTelefonoService);

        perTelefonoService.$inject = ['$http'];

        function perTelefonoService($http)
        {

            this.save                    = save ;
            this.update                  = update ;
            this.updateEstado            = updateEstado ;
            this.deleteFill              = deleteFill ;
            this.getPerTelefonoById      = getPerTelefonoById ;
            this.getPerTelefonoByNumero  = getPerTelefonoByNumero ;
            this.getTelefonosByPersonaId = getTelefonosByPersonaId ;
            this.reOrderItems            = reOrderItems ;


            var path = 'web/telefonos' ;

            function getTelefonosByPersonaId(params_){
                var url_ = path+'/get/persona/id';
                return appHttp.postHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function deleteFill(params_){
                var url_ = path+'/delete';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPerTelefonoById(params_){
                var url_ = path+'/get/id';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPerTelefonoByNumero(params_){
                var url_ = path+'/get/numero';
                return appHttp.postHttp($http,url_, params_);
            };

            function reOrderItems(params_){
                var url_ = path+'/reorder-items';
                return appHttp.postHttp($http,url_, params_);
            };
        }
})();