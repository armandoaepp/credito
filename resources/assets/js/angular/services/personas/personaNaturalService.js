(function(){
    'use strict';
    angular
        .module('personaNatural.service')
        .service('personaNaturalService', personaNaturalService);

        personaNaturalService.$inject = ['$http'];

        function personaNaturalService($http)
        {

            this.getPersonasNaturales      = getPersonasNaturales ;
            this.getPerNaturalInfoAll      = getPerNaturalInfoAll ;
            this.getPerNaturalInfoBasica   = getPerNaturalInfoBasica ;
            this.save                      = save ;
            this.update                    = update ;
            this.updateEstado              = updateEstado ;
            this.getPerNaturalById         = getPerNaturalById ;
            this.getPerNaturalByDni        = getPerNaturalByDni ;
            this.updateDni                 = updateDni ;
            this.updatePerNatInfo          = updatePerNatInfo ;
            this.getPersonasByTipoRelacion = getPersonasByTipoRelacion ;

            var path = 'web/personas/naturales' ;

            function getPersonasNaturales(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getPerNaturalInfoAll(params_){
               var url_ = path+'/get/infoall';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPerNaturalInfoBasica(){
                var url_ = path+'/get/info-basica';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPerNaturalById(params_){
                var url_ = path+'/get/id';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPerNaturalByDni(params_){
                var url_ = path+'/get/dni';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateDni(params_){
                var url_ = path+'/update/dni';
                return appHttp.postHttp($http,url_, params_);
            };

            function updatePerNatInfo(params_){
                var url_ = path+'/update/info';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPersonasByTipoRelacion(params_){
                var url_ = path+'/by/tipo-relacion';
                return appHttp.postHttp($http,url_, params_);
            };







        }
})();