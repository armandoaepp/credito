(function(){
    'use strict';
    angular
        .module('tipoMoneda.service')
        .service('tipoMonedaService', tipoMonedaService);

        tipoMonedaService.$inject = ['$http'];

        function tipoMonedaService($http){

            this.getTipoMonedas    = getTipoMonedas ;
            this.save                = save ;
            this.getTipoMonedaById = getTipoMonedaById ;
            this.update              = update ;
            this.updateEstado        = updateEstado ;

            var path = 'reg/tipo-monedas' ;

            function getTipoMonedas(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getTipoMonedaById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();