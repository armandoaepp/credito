(function(){
    'use strict';
    angular
        .module('cargo.service')
        .service('cargoService', cargoService);

        cargoService.$inject = ['$http'];

        function cargoService($http){

            this.getCargos         = getCargos ;
            this.saveCargo         = saveCargo ;
            this.getCargoById      = getCargoById ;
            this.updateCargo       = updateCargo ;
            this.updateEstado      = updateEstado ;
            this.getCargosAll    = getCargosAll ;
            this.getCargosByAreaId = getCargosByAreaId ;

            var path = 'reg/cargos' ;

            function getCargos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getCargosAll(){
                var url_ = path+'/all';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getCargosByAreaId(){
                 var url_ = path+'/byarea';
                return appHttp.postHttp($http,url_, params_);
            };

            function saveCargo(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getCargoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateCargo(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();