(function(){
    'use strict';
    angular
        .module('licencia.service')
        .service('licenciaService', licenciaService);

        licenciaService.$inject = ['$http'];

        function licenciaService($http){

            this.getLicencias    = getLicencias ;
            this.save            = save ;
            this.getLicenciaById = getLicenciaById ;
            this.update          = update ;
            this.updateEstado    = updateEstado ;

            var path = 'reg/licencias' ;

            function getLicencias(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getLicenciaById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();