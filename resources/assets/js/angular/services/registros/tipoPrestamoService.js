(function(){
    'use strict';
    angular
        .module('tipoPrestamo.service')
        .service('tipoPrestamoService', tipoPrestamoService);

        tipoPrestamoService.$inject = ['$http'];

        function tipoPrestamoService($http){

            this.getTipoPrestamos    = getTipoPrestamos ;
            this.save                = save ;
            this.getTipoPrestamoById = getTipoPrestamoById ;
            this.update              = update ;
            this.updateEstado        = updateEstado ;

            var path = 'reg/tipo-prestamos' ;

            function getTipoPrestamos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getTipoPrestamoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();