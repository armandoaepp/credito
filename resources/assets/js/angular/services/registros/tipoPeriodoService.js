(function(){
    'use strict';
    angular
        .module('tipoPeriodo.service')
        .service('tipoPeriodoService', tipoPeriodoService);

        tipoPeriodoService.$inject = ['$http'];

        function tipoPeriodoService($http){

            this.getTipoPeriodos    = getTipoPeriodos ;
            this.save                = save ;
            this.getTipoPeriodoById = getTipoPeriodoById ;
            this.update              = update ;
            this.updateEstado        = updateEstado ;

            var path = 'reg/tipo-periodos' ;

            function getTipoPeriodos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getTipoPeriodoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();