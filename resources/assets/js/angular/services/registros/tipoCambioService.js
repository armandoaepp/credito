(function(){
    'use strict';
    angular
        .module('tipoCambio.service')
        .service('tipoCambioService', tipoCambioService);

        tipoCambioService.$inject = ['$http'];

        function tipoCambioService($http){

            this.getTipoCambios    = getTipoCambios ;
            this.save                = save ;
            this.getTipoCambioById = getTipoCambioById ;
            this.update              = update ;
            this.updateEstado        = updateEstado ;

            var path = 'reg/tipo-cambios' ;

            function getTipoCambios(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getTipoCambioById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();