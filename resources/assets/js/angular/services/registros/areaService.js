(function(){
    'use strict';
    angular
        .module('area.service')
        .service('areaService', areaService);

        areaService.$inject = ['$http'];

        function areaService($http){

            this.getAreas           = getAreas ;
            this.saveArea          = saveArea ;
            this.getAreaById       = getAreaById ;
            this.updateArea        = updateArea ;
            this.updateEstado      = updateEstado ;

            var path = 'reg/areas' ;

            function getAreas(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function saveArea(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getAreaById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateArea(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();