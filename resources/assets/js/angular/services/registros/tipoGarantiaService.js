(function(){
    'use strict';
    angular
        .module('tipoGarantia.service')
        .service('tipoGarantiaService', tipoGarantiaService);

        tipoGarantiaService.$inject = ['$http'];

        function tipoGarantiaService($http){

            this.getTipoGarantias    = getTipoGarantias ;
            this.save                = save ;
            this.getTipoGarantiaById = getTipoGarantiaById ;
            this.update              = update ;
            this.updateEstado        = updateEstado ;

            var path = 'reg/tipo-garantias' ;

            function getTipoGarantias(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getTipoGarantiaById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();