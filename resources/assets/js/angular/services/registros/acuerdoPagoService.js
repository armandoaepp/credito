(function(){
    'use strict';
    angular
        .module('acuerdoPago.service')
        .service('acuerdoPagoService', acuerdoPagoService);

        acuerdoPagoService.$inject = ['$http'];

        function acuerdoPagoService($http){

            this.getAcuerdoPagos                = getAcuerdoPagos ;
            this.save                           = save ;
            this.getAcuerdoPagoById             = getAcuerdoPagoById ;
            this.update                         = update ;
            this.updateEstado                   = updateEstado ;
            this.getAcuerdoPagosByTipoPeriodoId = getAcuerdoPagosByTipoPeriodoId ;

            var path = 'reg/acuerdo-pagos' ;

            function getAcuerdoPagos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getAcuerdoPagoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function getAcuerdoPagosByTipoPeriodoId(params_){
                var url_ = path+'/get/by/tipo-periodo-id';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();