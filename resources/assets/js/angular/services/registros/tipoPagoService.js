(function(){
    'use strict';
    angular
        .module('tipoPago.service')
        .service('tipoPagoService', tipoPagoService);

        tipoPagoService.$inject = ['$http'];

        function tipoPagoService($http){

            this.getTipoPagos    = getTipoPagos ;
            this.save            = save ;
            this.getTipoPagoById = getTipoPagoById ;
            this.update          = update ;
            this.updateEstado    = updateEstado ;
            this.getTipoPagosByTipoPrestamoId    = getTipoPagosByTipoPrestamoId ;

            var path = 'reg/tipo-pagos' ;

            function getTipoPagos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getTipoPagoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function getTipoPagosByTipoPrestamoId(params_){
                var url_ = path+'/get/by/tipo-prestamo-id';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();