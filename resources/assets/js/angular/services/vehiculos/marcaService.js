(function(){
    'use strict';
    angular
        .module('marca.service')
        .service('marcaService', marcaService);

        marcaService.$inject = ['$http'];

        function marcaService($http){

            this.getMarcas    = getMarcas ;
            this.save         = save ;
            this.getMarcaById = getMarcaById ;
            this.update       = update ;
            this.updateEstado = updateEstado ;

            var path = 'vehiculos/marcas' ;

            function getMarcas(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getMarcaById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();