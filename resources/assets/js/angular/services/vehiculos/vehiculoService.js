(function(){
    'use strict';
    angular
        .module('vehiculo.service')
        .service('vehiculoService', vehiculoService);

        vehiculoService.$inject = ['$http'];

        function vehiculoService($http){

            this.getVehiculos          = getVehiculos ;
            this.save                  = save ;
            this.getVehiculoById       = getVehiculoById ;
            this.update                = update ;
            this.updateEstado          = updateEstado ;
            this.getVehiculosForSelect = getVehiculosForSelect ;

            var path = 'vehiculos/vehiculos' ;

            function getVehiculos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getVehiculoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function getVehiculosForSelect(){
                var url_ = path+'/for-select';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };


        }
})();