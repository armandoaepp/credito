(function(){
    'use strict';
    angular
        .module('tipoVehiculo.service')
        .service('tipoVehiculoService', tipoVehiculoService);

        tipoVehiculoService.$inject = ['$http'];

        function tipoVehiculoService($http){

            this.getTipoVehiculos                  = getTipoVehiculos ;
            this.save                              = save ;
            this.getTipoVehiculoById               = getTipoVehiculoById ;
            this.update                            = update ;
            this.updateEstado                      = updateEstado ;
            this.getTipoVehiculosAll               = getTipoVehiculosAll ;
            this.getTipoVehiculosByClaseVehiculoId = getTipoVehiculosByClaseVehiculoId ;

            var path = 'vehiculos/tipo-vehiculos' ;

            function getTipoVehiculos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getTipoVehiculosAll(){
                var url_ = path+'/all';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getTipoVehiculosByClaseVehiculoId(params_){
                 var url_ = path+'/by/clase-vehiculo-id';
                return appHttp.postHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getTipoVehiculoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };



        }
})();