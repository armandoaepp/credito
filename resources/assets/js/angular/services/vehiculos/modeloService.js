(function(){
    'use strict';
    angular
        .module('modelo.service')
        .service('modeloService', modeloService);

        modeloService.$inject = ['$http'];

        function modeloService($http){

            this.getModelos          = getModelos ;
            this.save                = save ;
            this.getModeloById       = getModeloById ;
            this.update              = update ;
            this.updateEstado        = updateEstado ;
            this.getModelosAll       = getModelosAll ;
            this.getModelosByMarcaId = getModelosByMarcaId ;

            var path = 'vehiculos/modelos' ;

            function getModelos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getModelosAll(){
                var url_ = path+'/all';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getModelosByMarcaId(params_){
                 var url_ = path+'/by/marca-id';
                return appHttp.postHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getModeloById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };



        }
})();