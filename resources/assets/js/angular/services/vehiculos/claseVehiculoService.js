(function(){
    'use strict';
    angular
        .module('claseVehiculo.service')
        .service('claseVehiculoService', claseVehiculoService);

        claseVehiculoService.$inject = ['$http'];

        function claseVehiculoService($http){

            this.getClaseVehiculos    = getClaseVehiculos ;
            this.save                = save ;
            this.getClaseVehiculoById = getClaseVehiculoById ;
            this.update              = update ;
            this.updateEstado        = updateEstado ;

            var path = 'vehiculos/clase-vehiculos' ;

            function getClaseVehiculos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getClaseVehiculoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();