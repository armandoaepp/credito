(function(){
    'use strict';
    angular
        .module('modal.service')
        .service('modalService', modalService);

        modalService.$inject = ['$uibModal'];

        function modalService($uibModal)
        {
            var modalDefaults = {
                backdrop: true,
                keyboard: true,
                modalFade: true,
                templateUrl: '../partials/app/modal/modal.tpl.html'
            };

            var modalOptions = {
                closeButtonText: 'Cancelar',
                actionButtonText: 'OK',
                headerText: 'Accion',
                bodyText: 'Deseo realizar esta acción?'
            };

            this.showModal = function (customModalDefaults, customModalOptions, templateUrl) {

                if (templateUrl) modalDefaults.templateUrl = templateUrl;
                if (!customModalDefaults) customModalDefaults = {};
                customModalDefaults.backdrop = 'static';
                return this.show(customModalDefaults, customModalOptions);
            };

            /*
                Mostrar modal por defecto
                Params :
                modalOptions.class = success, info ,warning , danger
                modalOptions.bodyText = mense a mostrar
            */
            this.showModalAlert = function (customModalDefaults, customModalOptions, templateUrl)
            {
                templateUrl || ( templateUrl = '../partials/app/modal/alert.tpl.html');
                modalDefaults.templateUrl = templateUrl ;

                modalOptions.bodyText = customModalOptions.bodyText ? customModalOptions.bodyText : 'Seleccionar Registro'

                if (!customModalDefaults) customModalDefaults = {};
                customModalDefaults.backdrop = true;
                return this.show(customModalDefaults, customModalOptions);
            };

            this.showModalConfirm = function (customModalDefaults, customModalOptions, templateUrl)
            {
                templateUrl || ( templateUrl = '../partials/app/modal/confirm.tpl.html');
                modalDefaults.templateUrl = templateUrl ;

                modalOptions.bodyText = customModalOptions.bodyText ? customModalOptions.bodyText : '¿Desea Eliminar el Registro?'

                if (!customModalDefaults) customModalDefaults = {};
                customModalDefaults.backdrop = 'static';
                return this.show(customModalDefaults, customModalOptions);
            };


            this.show = function (customModalDefaults, customModalOptions) {
                //Create temp objects to work with since we're in a singleton service
                var tempModalDefaults = {};
                var tempModalOptions = {};

                //Map angular-ui modal custom defaults to modal defaults defined in service
                angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

                //Map modal.html $scope custom properties to defaults defined in service
                angular.extend(tempModalOptions, modalOptions, customModalOptions);

                if (!tempModalDefaults.controller) {
                    tempModalDefaults.controller = function ($scope, $uibModalInstance) {
                        $scope.modalOptions = tempModalOptions;
                        $scope.modalOptions.ok = function (result) {
                            $uibModalInstance.close(result);
                        };
                        $scope.modalOptions.close = function (result) {
                            $uibModalInstance.dismiss('cancel');
                        };

                    }

                    tempModalDefaults.controller.$inject = ['$scope', '$uibModalInstance'];
                }

                return $uibModal.open(tempModalDefaults).result;
            };


        }
})();
