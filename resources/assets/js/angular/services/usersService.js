(function(){
    'use strict';
    angular
        .module('users.service')
        .service('usersService', usersService);

        usersService.$inject = ['$http'];

        function usersService($http){

            this.getUsers     = getUsers ;
            this.getInfo      = getInfo ;
            this.save         = save ;
            this.updateEstado = updateEstado ;

            var path = 'web/users' ;

            function getUsers(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getInfo(){
                var url_ = path+'/info';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();