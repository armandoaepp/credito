(function(){
    'use strict';
    angular
        .module('rol.service')
        .service('rolService', rolService);

        rolService.$inject = ['$http'];

        function rolService($http){

            this.getRoles         = getRoles ;
            this.getRolByNombre   = getRolByNombre ;
            this.saveRol          = saveRol ;
            this.getRolById       = getRolById ;
            this.updateRol        = updateRol ;
            this.updateEstado     = updateEstado ;
            this.getAccesosRol    = getAccesosRol ;
            this.updateAccesosRol = updateAccesosRol ;


            var path = 'web/roles' ;

            function getRoles(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getRolByNombre(params_){
                var url_ = path+'/get/bynombre';
                return appHttp.postHttp($http,url_, params_);
            };

            function saveRol(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getRolById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateRol(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function getAccesosRol(params_){
                var url_ = path+'/accesos';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateAccesosRol(params_){
                var url_ = path+'/update/accesos';
                return appHttp.postHttp($http,url_, params_);
            };




        }
})();