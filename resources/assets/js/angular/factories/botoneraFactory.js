(function(){
    'use strict';
    angular
        .module('menu.factory')
        .factory('botoneraFactory', botoneraFactory);

        botoneraFactory.$inject = [];

        function botoneraFactory(){
            var self =  this ;


            function setBotonera(botonera)
            {
                self.botonera = botonera ;
            }

            function getBotonera()
            {
                return self.botonera ;
            }

            return {
                setBotonera : setBotonera,
                getBotonera : getBotonera,
            } ;

        }

})();

