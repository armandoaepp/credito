/*(function(){
    'use strict';
    angular
        .module('tree.factory')
        .factory('treeFactory', treeFactory);

        treeFactory.$inject = [ '$q'];

        function treeFactory($q){

            var self = this ;
            self.data_tree = [] ;
            self.data_buil_tree = [] ;
            var deferred = $q.defer();



            function setDataTree(data)
            {
                self.data_buil_tree = [] ;
               self.data_tree = data ;
               return self.data_tree ;
            };

            function getDataTree(data)
            {
               return self.data_tree ;
            };

            function buildTree(arr, parent) {
                var out = []
                for(var i in arr) {
                    if(arr[i].control_padre_id === parent) {
                        var children = [] ;
                        children = buildTree(arr, arr[i].id)

                        if(children.length > 0) {
                            for(var j in children)
                            {
                                children[j].parent   =  arr[i] ;
                            }

                        }
                        arr[i].children = children ;

                        var is_active = false ;
                        if (arr[i].is_active === 1)
                        {
                            is_active = true ;
                        };
                        arr[i].is_active = is_active ;
                        arr[i].isExpanded = false;
                        arr[i].isSelected = is_active;

                        out.push(arr[i])
                    }
                }
                return out
            } ;

            //  promise para asegurar la cargar da datos
            function getDataBuilTree()
            {
                if (self.data_buil_tree === undefined || self.data_buil_tree === '' || self.data_buil_tree.length === 0  )
                {
                       self.data_buil_tree =   buildTree(self.data_tree, null) ;

                        deferred.resolve(self.data_buil_tree);
                }
                else
                {
                      deferred.resolve(self.data_tree);
                }

                return deferred.promise;
            } ;


            var factory =  {
                getDataTree : getDataTree,
                setDataTree : setDataTree,
                getDataBuilTree : getDataBuilTree,
            } ;

            return factory ;

        }

})();*/