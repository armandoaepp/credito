(function(){
    'use strict';
    angular
        .module('menu.factory')
        .factory('menusFactory', menusFactory);

        menusFactory.$inject = ['accesosService', '$q'];

        function menusFactory(accesosService,$q){

            var self = this ;
            self.menus = [] ;
            var deferred = $q.defer();


            //////////////////////////////////

            //  promise para asegurar la cargar da datos
            function getMenus(){
                if (self.menus === undefined || self.menus === '' || self.menus.length === 0  )
                {
                    accesosService.getAccesos().then(
                        function(response){
                            if (!response.error){
                                self.menus = response.data ;
                                deferred.resolve(self.menus);

                            }else
                            {
                                deferred.resolve(response);
                            }

                        }, function (response) {
                            // the following line rejects the promise
                            deferred.reject(response);
                            // promise is returned
                            return deferred.promise;
                        }
                    );
                }
                else
                {
                      deferred.resolve(self.menus);
                }

                return deferred.promise;
            } ;

            function setMenus(menus)
            {
               self.menus = menus ;
               return self.menus ;
            }


            var factory =  {
                getMenus : getMenus,
                setMenus : setMenus,
            } ;

            return factory ;

        }

})();