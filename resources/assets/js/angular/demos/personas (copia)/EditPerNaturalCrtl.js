  angular.module('persona.natural.info.controller').controller('EditPerNaturalCrtl', EditPerNaturalCrtl) ;
        EditPerNaturalCrtl.$inject = ['$rootScope',
                                     '$state',
                                     '$q',
                                     '$filter',
                                     '$stateParams',
                                     'modalService',
                                     'personaNaturalService',
                                     'perTelefonoService',
                                     'perMailService',
                                     'perDocumentoService'
                                    ] ;

            function EditPerNaturalCrtl($rootScope,
                                        $state,
                                        $q,
                                        $filter,
                                        $stateParams,
                                        modalService,
                                        personaNaturalService,
                                        perTelefonoService,
                                        perMailService,
                                        perDocumentoService
                                        )
            {
                var vm =  this ;

                vm.reloadListPersona = reloadListPersona ;

                vm.persona_id = $stateParams.codigo;

                // telefonos
                    vm.saveNewPhones = saveNewPhones ;
                    vm.updatePhone   = updatePhone ;
                    vm.deletePhone   = deletePhone;
                    vm.reOrderItemsTelefonos = reOrderItemsTelefonos ;

                    vm.flagNewPhones = false ;

                // mails
                    vm.saveNewMails       = saveNewMails ;
                    vm.updateMail         = updateMail ;
                    vm.deleteMail         = deleteMail;
                    vm.reOrderItemsEmails = reOrderItemsEmails ;

                    vm.flagNewMails = false ;

                // dni
                    vm.updateDni = updateDni ;

                    vm.updatePerNatInfo = updatePerNatInfo ;

                vm.msj = "";

                vm.formData = {
                    'dni'   : '',
                    'apellidos' : '',
                    'nombres'    : '',
                    'telefonos'  : [],
                    'mails'  : [],
                    'sexo'  : '',
                    'newtelefonos': [],
                    'newmails': [],
                    'fecha_nac': '',
                }

                 //  DatePicker ui-b
                    vm.today =  new Date() ;
                    vm.popup = {
                        opened : false,
                        maxDate : vm.today,
                    } ;

                    vm.openFechaNac = function() {
                        vm.popup.opened = true;
                    };

                //  sexo data
                    vm.sexoData = [
                            {value: 1, text: 'Masculino'},
                            {value: 2, text: 'Femenino'}
                          ];

                    vm.showStatus = function() {
                        var selected = $filter('filter')(vm.sexoData, {value: vm.formData.sexo});
                        return (vm.formData.sexo && selected.length) ? selected[0].text : ' No Seleccionado';
                    };


                // informacion de persona
                    getPerNaturalInfoAll() ;
                    function getPerNaturalInfoAll()
                    {
                        var data = {
                            'persona_id' : vm.persona_id,
                        } ;

                        personaNaturalService.getPerNaturalInfoAll(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.formData.dni       = response.data[0].per_natural.dni;
                                    vm.formData.apellidos = response.data[0].per_apellidos;
                                    vm.formData.nombres   = response.data[0].per_nombre;
                                    vm.formData.sexo      = response.data[0].per_natural.sexo;
                                    vm.formData.telefonos = response.data[0].per_telefono;
                                    vm.formData.mails     = response.data[0].per_mail;
                                    vm.formData.fecha_nac = (!response.data[0].per_fecha_nac)? vm.today :new Date( response.data[0].per_fecha_nac +' 00:00:00');
                                    return response.data ;
                                }
                            }
                        );
                    }

                // dni
                    function updateDni(value)
                    {
                        var deferred = $q.defer();

                        var dni = value;
                        var persona_id = vm.persona_id ;

                        var data = {
                                'persona_id'   : persona_id,
                                'dni'          : dni,
                            };

                        personaNaturalService.updateDni(data).then(
                                function(response){

                                    if (!response.error)
                                    {
                                        vm.formData.dni = response.data.dni;
                                        deferred.resolve();
                                    }else
                                    {
                                        deferred.resolve(response.data.dni[0]);
                                    }
                                }, function (response) {
                                    // the following line rejects the promise
                                    deferred.reject(response);
                                    return deferred.promise;
                                }
                            );
                        return deferred.promise;
                    } ;


                // update nombres y sexo
                    function updatePerNatInfo()
                    {
                        var persona_id = vm.persona_id ;

                        var fecha_nac = $filter('date')(vm.formData.fecha_nac,'yyyy-MM-dd');

                         var data = {
                                'apellidos'  : vm.formData.apellidos,
                                'nombres'    : vm.formData.nombres,
                                'sexo'       : vm.formData.sexo,
                                'persona_id' : persona_id,
                                'fecha_nac'  : fecha_nac,
                            };


                            personaNaturalService.updatePerNatInfo(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.apellidos = response.data.per_apellidos;
                                        vm.formData.nombres   = response.data.per_nombre;
                                        vm.formData.sexo      = response.data.per_natural.sexo;
                                        vm.formData.fecha_nac = (!response.data.per_fecha_nac)? vm.today :new Date( response.data.per_fecha_nac+' 00:00:00');

                                    }
                                }
                            );

                    };


                //  telefonos
                    function saveNewPhones()
                    {
                        var telefonos = vm.formData.newtelefonos ;
                        var persona_id = vm.persona_id ;

                         var data = {
                                'telefonos'    : telefonos,
                                'persona_id'   : persona_id,
                            };
                            perTelefonoService.save(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.newtelefonos = [] ;
                                        vm.flagNewPhones = false ;
                                        vm.formData.telefonos = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );

                    }

                    function updatePhone(value, id)
                    {
                        var telefono = value;

                        if( value.length != 9  )
                        {
                          return 'Teléfono Invalido(Son Nueve Numero)';
                        }

                        var data = {
                                'telefono_id' : id,
                                'telefono'    : telefono,
                            };

                            perTelefonoService.update(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.telefonos = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );
                    }

                    function deletePhone(Telefono, id)
                    {
                        var modalOptions = {
                            closeButtonText: 'Cancelar',
                            actionButtonText: 'Eliminar',
                            headerText: 'Eliminar Teléfono',
                            bodyText: '¿Esta Seguro de Eliminar Teléfono: '+ Telefono+' ?'
                        };

                       modalService.showModalConfirm({}, modalOptions).then(function (result) {
                            confirmDeletePhone(id);
                        });
                    }

                    function confirmDeletePhone(id)
                    {
                         var data = {
                                'telefono_id'    : id
                            };

                            perTelefonoService.deleteFill(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.telefonos = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );
                    }

                    function reOrderItemsTelefonos()
                    {
                         var data = {
                                'telefonos' : vm.formData.telefonos,
                                'persona_id' : vm.persona_id ,
                            };

                            perTelefonoService.reOrderItems(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.telefonos = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );
                    }

                    // sortable telefonos
                    vm.barConfigPhones = {
                        group: 'telefonos',
                        animation: 150,
                        // onSort: function (** ngSortEvent * evt){
                        onSort: function (evt){
                            vm.reOrderItemsTelefonos() ;
                        }
                    };



                //  Mail
                    function saveNewMails()
                    {
                        var mails = vm.formData.new_mails ;
                        var persona_id = vm.persona_id ;

                         var data = {
                                'persona_id'   : persona_id,
                                'mails'        : mails,
                            };

                            perMailService.save(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.new_mails = [] ;
                                        vm.flagNewMails = false ;
                                        vm.formData.mails = response.data;
                                        return vm.formData.mails;
                                    }
                                }
                            );
                    }

                    function updateMail(value, id)
                    {
                        var mail = value;

                        var data = {
                                'mail_id' : id,
                                'mail'    : mail,
                            };

                            perMailService.update(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.mails = response.data;
                                        return vm.formData.mails;
                                    }
                                }
                            );
                    }

                    function deleteMail(value, id)
                    {
                        var modalOptions = {
                            closeButtonText: 'Cancelar',
                            actionButtonText: 'Eliminar',
                            headerText: 'Eliminar Mail',
                            bodyText: '¿Esta Seguro de Eliminar Mail: '+ value+' ?'
                        };

                       modalService.showModalConfirm({}, modalOptions).then(function (result) {
                            confirmDeleteMail(id);
                        });
                    }

                    function confirmDeleteMail(id)
                    {
                         var data = {
                                'mail_id'    : id
                            };

                            perMailService.deleteFill(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.mails = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );
                    }

                    function reOrderItemsEmails()
                    {
                         var data = {
                                'mails' : vm.formData.mails,
                                'persona_id' : vm.persona_id ,
                            };

                            perMailService.reOrderItems(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.mails = response.data;
                                        return vm.formData.mails;
                                    }
                                }
                            );
                    }

                    // sortable Emails
                    vm.barConfigEmails = {
                        group: 'Mails',
                        animation: 150,
                        // onSort: function (** ngSortEvent *evt){
                        onSort: function (evt){
                            vm.reOrderItemsEmails() ;
                        }
                    };

                    function reloadListPersona()
                    {
                        var values = '' ;
                        $state.go('personas.personas', values);
                        $rootScope.$broadcast("reloadListPersona", values);
                    }

            }