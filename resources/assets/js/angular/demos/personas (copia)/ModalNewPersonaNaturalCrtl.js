(function(){
	angular.module('persona.natural.info.controller').controller('ModalNewPersonaNaturalCrtl', ModalNewPersonaNaturalCrtl) ;
        ModalNewPersonaNaturalCrtl.$inject = ['$uibModalInstance','$filter', 'personaNaturalService'] ;

            function ModalNewPersonaNaturalCrtl($uibModalInstance,$filter, personaNaturalService)
            {
                var vm =  this ;

                vm.msj = "";

                vm.formData = {
                    'dni'   : '',
                    'apellidos' : '',
                    'nombres'    : '',
                    'telefonos'  : [],
                    'mails'  : [],
                    'sexo'  : 1,
                    'fecha_nac': '',
                } ;

                //  DatePicker ui-b
                    vm.today =  new Date() ;
                    vm.popup = {
                        opened : false,
                        maxDate : vm.today,
                    } ;

                    vm.openFechaNac = function() {
                        vm.popup.opened = true;
                    };

                vm.ok = function ()
                {
                    var fecha_nac = $filter('date')(vm.formData.fecha_nac,'yyyy-MM-dd');
                    vm.formData.fecha_nac = fecha_nac ;

                    var data = vm.formData ;
                    personaNaturalService.getPerNaturalByDni(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                if (response.data.length > 0)
                                {
                                    vm.msj = 'DNI Ya se encuentra registrado!'
                                }else
                                {
                                    vm.save();
                                }
                                ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );
                };

                vm.save = function()
                {
                    personaNaturalService.save(vm.formData).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                $uibModalInstance.close(vm.formData);
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );
                };

                vm.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }

})();