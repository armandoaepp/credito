(function(){
    angular
        .module('app.informacion')
        .config(appConfig) ;

        appConfig.$inject = ['$stateProvider', '$urlRouterProvider','PATH'] ;

        function appConfig($stateProvider,$urlRouterProvider,PATH)
        {

            var path_informacion   = PATH.INFORMACION ;

            $stateProvider
                // emisor
                .state('vehiculos',{
                    url: '/vehiculos',
                    templateUrl: path_informacion+'/vehiculos/index.vehiculos.tlp.html',
                    // controller: 'informacionCtrl',
                    // controllerAs: 'vm',
                })

                    .state('vehiculos.vehiculos',{
                        url: '/vehiculos',
                        templateUrl: path_informacion+'/vehiculos/vehiculos/index.vehiculos.tpl.html',
                        controller: 'VehiculosInfoCtrl',
                        controllerAs: 'vm'
                    })

                    .state('vehiculos.clase-vehiculos',{
                        url: '/clase-vehiculos',
                        templateUrl: path_informacion+'/vehiculos/clase-vehiculos/index.clase-vehiculos.tpl.html',
                        controller: 'ClaseVehiculosInfoCtrl',
                        controllerAs: 'vm'
                    })

                    .state('vehiculos.tipo-vehiculos',{
                        url: '/tipo-vehiculos',
                        templateUrl: path_informacion+'/vehiculos/tipo-vehiculos/index.tipo-vehiculos.tpl.html',
                        controller: 'TipoVehiculosInfoCtrl',
                        controllerAs: 'vm'
                    })

                    .state('vehiculos.marcas',{
                        url: '/marcas',
                        templateUrl: path_informacion+'/vehiculos/marcas/index.marcas.tpl.html',
                        controller: 'MarcasInfoCtrl',
                        controllerAs: 'vm'
                    })

                    .state('vehiculos.modelos',{
                        url: '/modelos',
                        templateUrl: path_informacion+'/vehiculos/modelos/index.modelos.tpl.html',
                        controller: 'ModelosInfoCtrl',
                        controllerAs: 'vm'
                    })


        }
})() ;