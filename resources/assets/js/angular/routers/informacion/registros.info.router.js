(function(){
    angular
        .module('app.informacion')
        .config(appConfig) ;

        appConfig.$inject = ['$stateProvider', '$urlRouterProvider','PATH'] ;

        function appConfig($stateProvider,$urlRouterProvider,PATH)
        {

            var path_informacion   = PATH.INFORMACION ;

            $stateProvider
                // emisor
                .state('registros',{
                    url: '/registros',
                    templateUrl: path_informacion+'/registros/index.registros.tlp.html',
                    // controller: 'informacionCtrl',
                    // controllerAs: 'vm',
                })
                    .state('registros.areas',{
                        url: '/areas',
                        templateUrl: path_informacion+'/registros/areas/index.areas.tpl.html',
                        controller: 'AreasInfoCtrl',
                        controllerAs: 'vm'
                    })
                    .state('registros.cargos',{
                        url: '/cargos',
                        templateUrl: path_informacion+'/registros/cargos/index.cargos.tpl.html',
                        controller: 'CargosInfoCtrl',
                        controllerAs: 'vm'
                    })

                    .state('registros.tipo-prestamos',{
                        url: '/tipo-prestamos',
                        templateUrl: path_informacion+'/registros/tipo_prestamos/index.tipo-prestamos.tpl.html',
                        controller: 'TipoPrestamosInfoCtrl',
                        controllerAs: 'vm'
                    })
                    .state('registros.tipo-monedas',{
                        url: '/tipo-monedas',
                        templateUrl: path_informacion+'/registros/tipo-monedas/index.tipo-monedas.tpl.html',
                        controller: 'TipoMonedasInfoCtrl',
                        controllerAs: 'vm'
                    })
                    .state('registros.tipo-periodos',{
                        url: '/tipo-periodos',
                        templateUrl: path_informacion+'/registros/tipo-periodos/index.tipo-periodos.tpl.html',
                        controller: 'TipoPeriodosInfoCtrl',
                        controllerAs: 'vm'
                    })
                    .state('registros.tipo-garantias',{
                        url: '/tipo-garantias',
                        templateUrl: path_informacion+'/registros/tipo-garantias/index.tipo-garantias.tpl.html',
                        controller: 'TipoGarantiasInfoCtrl',
                        controllerAs: 'vm'
                    })
                    .state('registros.tipo-pagos',{
                        url: '/tipo-pagos',
                        templateUrl: path_informacion+'/registros/tipo-pagos/index.tipo-pagos.tpl.html',
                        controller: 'TipoPagosInfoCtrl',
                        controllerAs: 'vm'
                    })
                    .state('registros.licencias',{
                        url: '/licencias',
                        templateUrl: path_informacion+'/registros/licencias/index.licencias.tpl.html',
                        controller: 'LicenciasInfoCtrl',
                        controllerAs: 'vm'
                    })
                    .state('registros.tipo-cambios',{
                        url: '/tipo-cambios',
                        templateUrl: path_informacion+'/registros/tipo-cambios/index.tipo-cambios.tpl.html',
                        controller: 'TipoCambiosInfoCtrl',
                        controllerAs: 'vm'
                    })
                    .state('registros.acuerdo-pagos',{
                        url: '/acuerdo-pagos',
                        templateUrl: path_informacion+'/registros/acuerdo-pagos/index.acuerdo-pagos.tpl.html',
                        controller: 'AcuerdoPagosInfoCtrl',
                        controllerAs: 'vm'
                    })






        }
})() ;