(function(){
    angular
        .module('app.informacion')
        .config(appConfig) ;

        appConfig.$inject = ['$stateProvider', '$urlRouterProvider','PATH'] ;

        function appConfig($stateProvider,$urlRouterProvider,PATH)
        {

            var path_informacion   = PATH.INFORMACION ;

            $stateProvider
                // emisor
                .state('personas',{
                    url: '/personas',
                    templateUrl: path_informacion+'/personas/index.personas.tlp.html',
                    // controller: 'informacionCtrl',
                    // controllerAs: 'vm',
                })
                    .state('personas.personas',{
                        url: '/personas',
                        templateUrl: path_informacion+'/personas/per_natural/index.naturales.tpl.html',
                        controller: 'PersonaNaturalInfoCtrl',
                        controllerAs: 'vm'
                    })
                        .state('personas.personas.editar',{
                                url: '/editar/:codigo',
                                templateUrl: path_informacion+'/personas/per_natural/editar.natural.tpl.html',
                                controller: 'EditPerNaturalCrtl',
                                controllerAs: 'vm'
                        })
                    .state('personas.empresas',{
                        url: '/empresas',
                        templateUrl: path_informacion+'/personas/empresas/index.empresas.tpl.html',
                        controller: 'PerJuridicasInfoCtrl',
                        controllerAs: 'vm'
                    })
                        .state('personas.empresas.editar',{
                                url: '/editar/:codigo',
                                templateUrl: path_informacion+'/personas/empresas/editar.empresa.tpl.html',
                                controller: 'EditPerJuridicaCrtl',
                                controllerAs: 'vm'
                        })


                    .state('personas.empleados',{
                        url: '/empleados',
                        templateUrl: path_informacion+'/personas/empleados/index.empleados.tpl.html',
                        controller: 'EmpleadosInfoCtrl',
                        controllerAs: 'vm'
                    })

                    .state('personas.clientes',{
                        url: '/clientes',
                        templateUrl: path_informacion+'/personas/clientes/index.clientes.tpl.html',
                        controller: 'ClientesInfoCtrl',
                        controllerAs: 'vm'
                    })
                    .state('personas.avales',{
                        url: '/avales',
                        templateUrl: path_informacion+'/personas/per_natural/index.naturales.tpl.html',
                        controller: 'PersonaNaturalInfoCtrl',
                        controllerAs: 'vm'
                    })
                        .state('personas.avales.editar',{
                                url: '/editar/:codigo',
                                templateUrl: path_informacion+'/personas/per_natural/editar.natural.tpl.html',
                                controller: 'EditPerNaturalCrtl',
                                controllerAs: 'vm'
                        })

        }
})() ;