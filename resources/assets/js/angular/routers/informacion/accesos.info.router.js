(function(){
    angular
        .module('app.informacion')
        .config(appConfig) ;

        appConfig.$inject = ['$stateProvider', '$urlRouterProvider','PATH'] ;

        function appConfig($stateProvider,$urlRouterProvider,PATH)
        {

            var path_informacion   = PATH.INFORMACION ;

            $stateProvider
                // emisor
                .state('accesos',{
                    url: '/accesos',
                    templateUrl: path_informacion+'/accesos/index.accesos.tlp.html',
                    // controller: 'informacionCtrl',
                    // controllerAs: 'vm',
                })
                    .state('accesos.roles',{
                        url: '/roles',
                        templateUrl: path_informacion+'/accesos/roles/index.roles.tpl.html',
                        controller: 'RolesInfoCtrl',
                        controllerAs: 'vm'
                    })

                    .state('accesos.usuarios',{
                        url: '/usuarios',
                        templateUrl: path_informacion+'/accesos/usuarios/index.usuarios.tpl.html',
                        controller: 'UsuariosInfoCtrl',
                        controllerAs: 'vm'
                    })

        }
})() ;