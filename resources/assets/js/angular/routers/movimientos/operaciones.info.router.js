(function(){
    angular
        .module('app.movimientos')
        .config(appConfig) ;

        appConfig.$inject = ['$stateProvider', '$urlRouterProvider','PATH'] ;

        function appConfig($stateProvider,$urlRouterProvider,PATH)
        {
            var path_movimientos   = PATH.MOVIMIENTOS ;

            $stateProvider
                .state('operaciones',{
                    url: '/operaciones',
                    templateUrl: path_movimientos+'/operaciones/index.operaciones.tlp.html',
                    // controller: 'informacionCtrl',
                    // controllerAs: 'vm',
                })
                    .state('operaciones.prestamos',{
                        url: '/prestamos',
                        templateUrl: path_movimientos+'/operaciones/prestamos/index.prestamos.tpl.html',
                        controller: 'PrestamosMovCtrl',
                        controllerAs: 'vm'
                    })
                        .state('operaciones.prestamos.nuevo',{
                                url: '/nuevo',
                                templateUrl: path_movimientos+'/operaciones/prestamos/nuevo.prestamo.tpl.html',
                                controller: 'NewPrestamoMovCrtl',
                                controllerAs: 'vm'
                        })
                        .state('operaciones.prestamos.edit',{
                                url: '/editat/:codigo',
                                templateUrl: path_movimientos+'/operaciones/prestamos/editar.prestamo.tpl.html',
                                controller: 'EditPrestamoMovCrtl',
                                controllerAs: 'vm'
                        })
                        .state('operaciones.prestamos.detalle',{
                                url: '/detalle/:codigo',
                                templateUrl: path_movimientos+'/operaciones/prestamos/detalle.prestamo.tpl.html',
                                controller: 'DetallePrestamoCrtl',
                                controllerAs: 'vm'
                        })
                    .state('operaciones.cred-menor',{
                        url: '/cred-menor',
                        templateUrl: path_movimientos+'/operaciones/cred-menor/index.cred-menor.tpl.html',
                        controller: 'CredMenorMovCtrl',
                        controllerAs: 'vm'
                    })
                        .state('operaciones.cred-menor.nuevo',{
                                url: '/nuevo',
                                templateUrl: path_movimientos+'/operaciones/cred-menor/nuevo.cred-menor.tpl.html',
                                controller: 'NewCredMenorCrtl',
                                controllerAs: 'vm'
                        })
                        .state('operaciones.cred-menor.edit',{
                                url: '/editat/:codigo',
                                templateUrl: path_movimientos+'/operaciones/cred-menor/editar.cred-menor.tpl.html',
                                controller: 'EditCredMenorMovCrtl',
                                controllerAs: 'vm'
                        })
                        .state('operaciones.cred-menor.detalle',{
                                url: '/detalle/:codigo',
                                templateUrl: path_movimientos+'/operaciones/cred-menor/detalle.cred-menor.tpl.html',
                                controller: 'DetalleCredMenorMovCrtl',
                                controllerAs: 'vm'
                        })


        }
})() ;