<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'errors_title' => "Por Favor corriga los siguiente Errores",
    'login_title' => 'Inicio de Sesión',
    'login_button' => 'Iniciar Sesión',
    'register_title' => 'Registro',
    'register_button' => 'Registrar',
    'remember' => 'Recordar Contraseña',
    'forgot_link' => '¿Olvido su Contraseña?',
    'not_have_account' =>'¿No tiene una Cuenta?',
    'create_account' =>'Crea una Cuenta',

    'failed' => 'La Contraseña no Coincide con nuestro Registros',
    'throttle' => 'Demasiados intentos de conexión. Vuelve a intentarlo en: segundos segundos',

    'register_title_company' => 'Registrar Organización',
    'title_persona' => '¿Eres una Persona?',
    'title_company' => '¿Eres una Organización?',

    'auth_facebook'   => 'Iniciar con Facebook' ,
    'auth_twitter' => 'Iniciar con Twitter' ,
    'auth_google'  => 'Iniciar con Google' ,

];
