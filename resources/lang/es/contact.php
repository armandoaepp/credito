<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'errors_title' => "Por Favor corriga los siguiente Errores",
    'contact_title' => 'Contactenos',
    'contact_button' => 'Enviar',

];
