<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'errors_title' => "Whoops! There were some problems with your input.",
    'login_title' => 'Login',
    'login_button' => 'Login',
    'register_title' => 'Register',
    'register_button' => 'Register',
    'remember' => 'Remember Me',
    'forgot_link' => 'Forgot your password?',
    'not_have_account' =>'Do not have an account?',
    'create_account' =>'Create an account',


    'auth_faceb'   => 'Sign in with Facebook' ,
    'auth_twitter' => 'Sign in with Twitter' ,
    'auth_google'  => 'Sign in with Google' ,

];
