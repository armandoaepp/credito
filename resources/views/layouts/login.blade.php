<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <title>Plataforma Prestamos </title>

  <meta name="description" content="prestamos , creditos " />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="{{asset('css/app.min.css')}}" type="text/css" />

</head>
<body>
<div class="app app-header-fixed ">

  @yield('content')

<!--   <div class="container w-xxl w-auto-xs" ng-controller="SigninFormController" ng-init="app.settings.container = false;">
    <a href class="navbar-brand block m-t">Prestamos del Norte</a>
    <div class="m-b-lg">
      <div class="wrapper text-center">
        <strong>Iniciar Sesión</strong>
      </div>
      <form name="form" class="form-validation">
        <div class="text-danger wrapper text-center" ng-show="authError">

        </div>
        <div class="list-group list-group-sm">
          <div class="list-group-item">
            <input type="email" placeholder="Email" class="form-control no-border" ng-model="user.email" required>
          </div>
          <div class="list-group-item">
             <input type="password" placeholder="Password" class="form-control no-border" ng-model="user.password" required>
          </div>
        </div>
        <button type="submit" class="btn btn-lg btn-primary btn-block" ng-click="login()" ng-disabled='form.$invalid'>Inciar Sesión</button>
        <div class="text-center m-t m-b"><a ui-sref="access.forgotpwd">Recuperar Contraseña?</a></div>
        <div class="line line-dashed"></div>

      </form>
    </div>
    <div class="text-center" ng-include="'tpl/blocks/page_footer.html'">
      <p>

  </p>
    </div>
  </div> -->


</div>
<script src="{{asset('js/script.min.js')}}"></script>
</body>
</html>