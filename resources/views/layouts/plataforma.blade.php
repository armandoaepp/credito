<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <title>Plataforma Prestamos </title>

  <meta name="description" content="prestamos , creditos " />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="{{asset('css/app.min.css')}}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('css/app.plugin.min.css')}}" type="text/css" />

  <!-- <link rel="stylesheet" href="css/app.min.css" type="text/css" /> -->

</head>
<body ng-app="appPlataforma">

<!-- <div class="app app-header-fixed " > -->
<div class="app  app-header-fixed app-aside-fixed " >


    <!-- header -->
  <header id="header" class="app-header navbar" role="menu">
      <!-- navbar header -->
      <div class="navbar-header bg-dark">
        <button class="pull-right visible-xs dk" ui-toggle-class="show" target=".navbar-collapse">
          <i class="glyphicon glyphicon-cog"></i>
        </button>
        <button class="pull-right visible-xs" ui-toggle-class="off-screen" target=".app-aside" ui-scroll="app">
          <i class="glyphicon glyphicon-align-justify"></i>
        </button>
        <!-- brand -->
        <a href="#/" class="navbar-brand text-lt">
          <!-- <i class="fa fa-btc"></i> -->
          <img src="{{asset('assets/logos/logo.png')}}" alt="." >
          <span class="hidden-folded m-l-xs">Prestamos</span>
        </a>
        <!-- / brand -->
      </div>
      <!-- / navbar header -->

      <!-- navbar collapse -->
      <div class="collapse pos-rlt navbar-collapse box-shadow bg-white-only">
        <!-- buttons -->
        <div class="nav navbar-nav hidden-xs">
          <a href="#" class="btn no-shadow navbar-btn" ui-toggle-class="app-aside-folded" target=".app">
            <i class="fa fa-dedent fa-fw text"></i>
            <i class="fa fa-indent fa-fw text-active"></i>
          </a>
          <a href="#" class="btn no-shadow navbar-btn" ui-toggle-class="show" target="#aside-user">
            <i class="fa fa-user"></i>
          </a>
        </div>
        <!-- / buttons -->

        <!-- nabar right -->
        <ul class="nav navbar-nav navbar-right" ng-controller="UserInfoCtrl as vm">

          <li class="dropdown" >
            <a href="#" data-toggle="dropdown" class="dropdown-toggle clear" data-toggle="dropdown">
              <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
                <img ng-src="{%vm.data.avatar %}" alt="...">
                <i class="on md b-white bottom"></i>
              </span>
              <span class="hidden-sm hidden-md"> {% vm.data.per_apellidos %}  {% vm.data.per_nombre %}</span> <b class="caret"></b>
            </a>
            <!-- dropdown -->
            <ul class="dropdown-menu animated fadeInRight w">

              <li>
                <a href>
                  <span>configuración</span>
                </a>
              </li>
              <li>
                <a ui-sref="app.page.profile">Perfil</a>
              </li>

              <li class="divider"></li>
              <li>
                <a href="{{route('logout')}}">Cerrar Sesión</a>
              </li>
            </ul>
            <!-- / dropdown -->
          </li>
        </ul>
        <!-- / navbar right -->
      </div>
      <!-- / navbar collapse -->
  </header>
  <!-- / header -->


    <!-- aside -->
  <aside id="aside" class="app-aside hidden-xs bg-dark">
      <div class="aside-wrap">
        <div class="navi-wrap">

          <!-- user -->
          <div class="clearfix hidden-xs text-center hide" id="aside-user">
            <div class="dropdown wrapper" ng-controller="UserInfoCtrl as vm">
              <a >
                <span class="thumb-lg w-auto-folded avatar m-t-sm">
                  <img ng-src="{% vm.data.avatar %}" class="img-full" alt="...">
                </span>
              </a>
              <a href="#" data-toggle="dropdown" class="dropdown-toggle hidden-folded">
                <span class="clear">
                  <span class="block m-t-sm">
                    <strong class="font-bold text-lt">{% vm.data.per_nombre %}</strong>
                    <b class="caret"></b>
                  </span>
                  <span class="text-muted text-xs block">{% vm.data.rol %}</span>
                </span>
              </a>
              <!-- dropdown -->
              <ul class="dropdown-menu animated fadeInRight w hidden-folded">
                <li>
                  <a href>
                    <span>configuración</span>
                  </a>
                </li>
                <li>
                  <a ui-sref="app.page.profile">Perfil</a>
                </li>

                <li class="divider"></li>
                <li>
                  <a href="{{route('logout')}}">Cerrar Sesión</a>
                </li>
              </ul>
              <!-- / dropdown -->
            </div>
            <div class="line dk hidden-folded"></div>
          </div>
          <!-- / user -->

          <!-- nav -->
          <nav ui-nav class="navi clearfix">
            <ul class="nav" ng-controller="accesosMenusCrtl as vm">
              <div ng-repeat="modulo in vm.menus ">
                <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                  <span> {% modulo.nombre %} </span>
                </li>

              <li  ng-repeat="menu in modulo.children " ng-class="{active:$state.includes('{% menu.valor %}')}">
                <a ui-sref="{% menu.valor %}"  class="auto">
                  <span class="pull-right text-muted">
                    <i class="fa fa-fw fa-angle-right text" ></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
                  </span>
                  <!-- <i class="glyphicon glyphicon-file icon"  ></i> -->
                  <i ng-class="menu.descripcion"  ></i>

                  <span>{% menu.nombre %} </span>
                </a>
                <ul class="nav nav-sub dk" >
                  <!-- <li  ng-repeat="submenu in menu.children "  ui-sref-active="active" > -->
                  <li  ng-repeat="submenu in menu.children " ng-class="{active:$state.includes('{% submenu.valor %}')}">

                    <a ui-sref="{% submenu.valor %}"
                    menu-botonera
                    botonera="{% submenu.children%}"
                    modulo="{%modulo.id%}"
                    menu="{%menu.id%}"
                    submenu="{%submenu.id%}">
                      <span>{% submenu.nombre %}</span>
                    </a>
                  </li>

                </ul>
              </li>
              <li class="line dk hidden-folded"></li>

              </div>

             <!--  <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
                <span>Your Stuff</span>
              </li>
              <li>
                <a href="page_profile.html">
                  <i class="icon-user icon text-success-lter"></i>
                  <b class="badge bg-success pull-right">30%</b>
                  <span>Profile</span>
                </a>
              </li> -->

            </ul>
          </nav>
          <!-- nav -->


        </div>
      </div>
  </aside>
  <!-- / aside -->


  <!-- content -->
  <div id="content" class="app-content" role="main">
    <div class="app-content-body ">


      <div class="hbox hbox-auto-xs hbox-auto-sm" >
        <!-- main -->
        <div class="" ui-view >
        </div>
        <!-- / main -->

      </div>
        <!-- / right col -->
    </div>



  </div>
  </div>
  <!-- /content -->

</div>

<script src="{{asset('js/script.min.js')}}"></script>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.min.js"></script> -->
<script src="{{ asset('js/libs/angularjs/1.4.8/angular.min.js') }}"></script>
<script src="{{ asset('js/libs/angularjs/ui-router/angular-ui-router.min.js') }}"></script>
<script src="{{ asset('js/libs/angularjs/ui-bootstrap/ui-bootstrap-tpls-1.1.1.min.js')}}"></script>
<script src="{{ asset('js/libs/angularjs/angular-sanitize/angular-sanitize.min.js') }}"></script>
<script src="{{ asset('js/libs/angularjs/angular-locale/angular-locale_es-pe.js') }}"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-select/0.14.2/select.min.js"></script> -->

<script src="{{ asset('js/app.js') }}"></script>
<!-- <script src="{{ asset('js/app.plugin.js') }}"></script> -->

<!-- <script src="{{ asset('js/app.min.js') }}"></script> -->
<script src="{{ asset('js/app.plugin.min.js') }}"></script>





</body>
</html>