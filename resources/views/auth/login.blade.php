@extends('layouts.login')

@section('content')
  <div class="container w-xxl w-auto-xs" >
    <a href class="navbar-brand block m-t">Prestamos del Norte</a>
    <div class="m-b-lg">
      <div class="wrapper text-center">
        <strong>Iniciar Sesión</strong>
      </div>

      <form name="form" class="form-validation" action="{{route('login')}}" method="post">
     <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="text-danger wrapper text-center">
          @include('partials.errors')
        </div>

        <div class="list-group list-group-sm">
          <div class="list-group-item">
            <input type="email"  name="email" placeholder="Email" value="{{ old('email')}}"  class="form-control no-border" required>
          </div>
          <div class="list-group-item">
             <input type="password" name="password" placeholder="Password" class="form-control no-border"  required>
          </div>
        </div>
        <button type="submit" class="btn btn-lg btn-primary btn-block" >Inciar Sesión</button>
        <div class="text-center m-t m-b">
          <a >Recuperar Contraseña?</a>
          <!-- <a href="{{ url('/password/reset') }}">Recuperar Contraseña?</a> -->
        </div>
        <div class="line line-dashed"></div>

      </form>
    </div>
    <div class="text-center" >
      <p>
  </p>
    </div>
  </div>


@endsection


<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i>Login
                                </button>

                                <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

 -->

