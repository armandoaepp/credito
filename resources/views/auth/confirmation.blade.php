@extends('layout.emails')

@section('content')
                <table align="center" width="100%">
                  <tr>
                     <td style="color:#333;">


                        <font face="Arial, Helvetica, sans-serif" size="+1">
                          Estimado(a): {{$ful_name}}
                        </font>
                        <p>
                           AlertSMS, le envía sus accesos, con los cuales podrá ingresar a nuestra plataforma, personalizar sus datos y las alertas que usted desear recibir.
                        </p>

                        <br />
                           <strong>Usuario:</strong> {{$email}}
                        <br />
                           <strong>Contraseña:</strong> {{$password}}
                        <br />
                        <br />

                        <p>
                          Recuerde que puede, registrar múltiples números de teléfono propios, de su familia, de sus amigos, manténlos a salvo a ellos también.
                        </p>

                     </td>
                  </tr>

                </table>
@endsection
