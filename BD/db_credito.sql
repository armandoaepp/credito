/*
Navicat MySQL Data Transfer

Source Server         : VPS-Prestamos
Source Server Version : 50547
Source Host           : 162.243.235.246:3306
Source Database       : db_credito

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2016-06-14 22:59:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for accesos
-- ----------------------------
DROP TABLE IF EXISTS `accesos`;
CREATE TABLE `accesos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `control_id` int(10) unsigned NOT NULL,
  `referencia` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `accesos_user_id_foreign` (`user_id`),
  KEY `accesos_control_id_foreign` (`control_id`),
  CONSTRAINT `accesos_control_id_foreign` FOREIGN KEY (`control_id`) REFERENCES `control` (`id`),
  CONSTRAINT `accesos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=481 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of accesos
-- ----------------------------
INSERT INTO `accesos` VALUES ('1', '1', '1', '', '1');
INSERT INTO `accesos` VALUES ('2', '1', '2', '', '1');
INSERT INTO `accesos` VALUES ('3', '1', '3', '', '1');
INSERT INTO `accesos` VALUES ('4', '1', '4', '', '1');
INSERT INTO `accesos` VALUES ('5', '1', '5', '', '1');
INSERT INTO `accesos` VALUES ('6', '1', '6', '', '1');
INSERT INTO `accesos` VALUES ('7', '1', '7', '', '1');
INSERT INTO `accesos` VALUES ('8', '1', '8', '', '1');
INSERT INTO `accesos` VALUES ('9', '1', '9', '', '1');
INSERT INTO `accesos` VALUES ('10', '1', '10', '', '1');
INSERT INTO `accesos` VALUES ('11', '1', '11', '', '1');
INSERT INTO `accesos` VALUES ('12', '1', '12', '', '1');
INSERT INTO `accesos` VALUES ('13', '1', '13', '', '1');
INSERT INTO `accesos` VALUES ('14', '1', '14', '', '1');
INSERT INTO `accesos` VALUES ('15', '1', '15', '', '1');
INSERT INTO `accesos` VALUES ('16', '1', '16', '', '1');
INSERT INTO `accesos` VALUES ('17', '1', '17', '', '1');
INSERT INTO `accesos` VALUES ('18', '1', '18', '', '1');
INSERT INTO `accesos` VALUES ('19', '1', '19', '', '1');
INSERT INTO `accesos` VALUES ('20', '1', '20', '', '1');
INSERT INTO `accesos` VALUES ('21', '2', '1', '', '1');
INSERT INTO `accesos` VALUES ('22', '2', '2', '', '1');
INSERT INTO `accesos` VALUES ('23', '2', '3', '', '1');
INSERT INTO `accesos` VALUES ('24', '2', '4', '', '1');
INSERT INTO `accesos` VALUES ('25', '2', '5', '', '1');
INSERT INTO `accesos` VALUES ('26', '2', '6', '', '1');
INSERT INTO `accesos` VALUES ('27', '2', '8', '', '1');
INSERT INTO `accesos` VALUES ('28', '2', '9', '', '1');
INSERT INTO `accesos` VALUES ('29', '2', '10', '', '1');
INSERT INTO `accesos` VALUES ('30', '2', '11', '', '1');
INSERT INTO `accesos` VALUES ('31', '2', '12', '', '1');
INSERT INTO `accesos` VALUES ('32', '2', '13', '', '1');
INSERT INTO `accesos` VALUES ('33', '2', '14', '', '1');
INSERT INTO `accesos` VALUES ('34', '2', '15', '', '1');
INSERT INTO `accesos` VALUES ('35', '2', '16', '', '1');
INSERT INTO `accesos` VALUES ('36', '2', '17', '', '1');
INSERT INTO `accesos` VALUES ('37', '2', '18', '', '1');
INSERT INTO `accesos` VALUES ('38', '2', '19', '', '1');
INSERT INTO `accesos` VALUES ('39', '2', '20', '', '1');
INSERT INTO `accesos` VALUES ('40', '3', '1', '', '1');
INSERT INTO `accesos` VALUES ('41', '3', '2', '', '1');
INSERT INTO `accesos` VALUES ('42', '3', '3', '', '1');
INSERT INTO `accesos` VALUES ('43', '3', '4', '', '1');
INSERT INTO `accesos` VALUES ('44', '3', '5', '', '1');
INSERT INTO `accesos` VALUES ('45', '3', '6', '', '1');
INSERT INTO `accesos` VALUES ('46', '3', '8', '', '1');
INSERT INTO `accesos` VALUES ('47', '3', '9', '', '1');
INSERT INTO `accesos` VALUES ('48', '3', '10', '', '1');
INSERT INTO `accesos` VALUES ('49', '3', '11', '', '1');
INSERT INTO `accesos` VALUES ('50', '3', '12', '', '1');
INSERT INTO `accesos` VALUES ('51', '3', '13', '', '1');
INSERT INTO `accesos` VALUES ('52', '3', '14', '', '1');
INSERT INTO `accesos` VALUES ('53', '3', '15', '', '1');
INSERT INTO `accesos` VALUES ('54', '3', '16', '', '1');
INSERT INTO `accesos` VALUES ('55', '3', '17', '', '1');
INSERT INTO `accesos` VALUES ('56', '3', '18', '', '1');
INSERT INTO `accesos` VALUES ('57', '3', '19', '', '1');
INSERT INTO `accesos` VALUES ('58', '3', '20', '', '1');
INSERT INTO `accesos` VALUES ('59', '5', '1', '', '1');
INSERT INTO `accesos` VALUES ('60', '5', '2', '', '1');
INSERT INTO `accesos` VALUES ('61', '5', '3', '', '1');
INSERT INTO `accesos` VALUES ('62', '5', '4', '', '1');
INSERT INTO `accesos` VALUES ('63', '5', '5', '', '1');
INSERT INTO `accesos` VALUES ('64', '5', '6', '', '1');
INSERT INTO `accesos` VALUES ('65', '5', '8', '', '1');
INSERT INTO `accesos` VALUES ('66', '5', '9', '', '1');
INSERT INTO `accesos` VALUES ('67', '5', '10', '', '1');
INSERT INTO `accesos` VALUES ('68', '5', '11', '', '1');
INSERT INTO `accesos` VALUES ('69', '5', '12', '', '1');
INSERT INTO `accesos` VALUES ('70', '5', '13', '', '1');
INSERT INTO `accesos` VALUES ('71', '5', '14', '', '1');
INSERT INTO `accesos` VALUES ('72', '5', '15', '', '1');
INSERT INTO `accesos` VALUES ('73', '5', '16', '', '1');
INSERT INTO `accesos` VALUES ('74', '5', '17', '', '1');
INSERT INTO `accesos` VALUES ('75', '5', '18', '', '1');
INSERT INTO `accesos` VALUES ('76', '5', '19', '', '1');
INSERT INTO `accesos` VALUES ('77', '5', '20', '', '1');
INSERT INTO `accesos` VALUES ('79', '6', '1', '', '1');
INSERT INTO `accesos` VALUES ('80', '6', '2', '', '1');
INSERT INTO `accesos` VALUES ('81', '6', '3', '', '1');
INSERT INTO `accesos` VALUES ('82', '6', '4', '', '1');
INSERT INTO `accesos` VALUES ('83', '6', '5', '', '1');
INSERT INTO `accesos` VALUES ('84', '6', '6', '', '0');
INSERT INTO `accesos` VALUES ('85', '6', '8', '', '0');
INSERT INTO `accesos` VALUES ('86', '6', '9', '', '0');
INSERT INTO `accesos` VALUES ('87', '6', '10', '', '0');
INSERT INTO `accesos` VALUES ('88', '6', '11', '', '0');
INSERT INTO `accesos` VALUES ('89', '6', '12', '', '0');
INSERT INTO `accesos` VALUES ('90', '6', '13', '', '0');
INSERT INTO `accesos` VALUES ('91', '6', '14', '', '0');
INSERT INTO `accesos` VALUES ('92', '6', '15', '', '0');
INSERT INTO `accesos` VALUES ('93', '6', '16', '', '0');
INSERT INTO `accesos` VALUES ('94', '6', '17', '', '0');
INSERT INTO `accesos` VALUES ('95', '6', '18', '', '0');
INSERT INTO `accesos` VALUES ('96', '6', '19', '', '0');
INSERT INTO `accesos` VALUES ('97', '6', '20', '', '1');
INSERT INTO `accesos` VALUES ('98', '4', '1', '', '1');
INSERT INTO `accesos` VALUES ('99', '4', '2', '', '1');
INSERT INTO `accesos` VALUES ('100', '4', '3', '', '1');
INSERT INTO `accesos` VALUES ('101', '4', '4', '', '1');
INSERT INTO `accesos` VALUES ('102', '4', '5', '', '1');
INSERT INTO `accesos` VALUES ('103', '4', '6', '', '1');
INSERT INTO `accesos` VALUES ('104', '4', '8', '', '1');
INSERT INTO `accesos` VALUES ('105', '4', '9', '', '1');
INSERT INTO `accesos` VALUES ('106', '4', '10', '', '1');
INSERT INTO `accesos` VALUES ('107', '4', '11', '', '1');
INSERT INTO `accesos` VALUES ('108', '4', '12', '', '1');
INSERT INTO `accesos` VALUES ('109', '4', '13', '', '1');
INSERT INTO `accesos` VALUES ('110', '4', '14', '', '1');
INSERT INTO `accesos` VALUES ('111', '4', '15', '', '1');
INSERT INTO `accesos` VALUES ('112', '4', '16', '', '1');
INSERT INTO `accesos` VALUES ('113', '4', '17', '', '1');
INSERT INTO `accesos` VALUES ('114', '4', '18', '', '1');
INSERT INTO `accesos` VALUES ('115', '4', '19', '', '1');
INSERT INTO `accesos` VALUES ('116', '4', '20', '', '1');
INSERT INTO `accesos` VALUES ('117', '7', '1', '', '1');
INSERT INTO `accesos` VALUES ('118', '7', '2', '', '1');
INSERT INTO `accesos` VALUES ('119', '7', '3', '', '1');
INSERT INTO `accesos` VALUES ('120', '7', '4', '', '1');
INSERT INTO `accesos` VALUES ('121', '7', '5', '', '1');
INSERT INTO `accesos` VALUES ('122', '7', '6', '', '1');
INSERT INTO `accesos` VALUES ('123', '7', '8', '', '1');
INSERT INTO `accesos` VALUES ('124', '7', '9', '', '1');
INSERT INTO `accesos` VALUES ('125', '7', '10', '', '1');
INSERT INTO `accesos` VALUES ('126', '7', '11', '', '1');
INSERT INTO `accesos` VALUES ('127', '7', '12', '', '1');
INSERT INTO `accesos` VALUES ('128', '7', '13', '', '1');
INSERT INTO `accesos` VALUES ('129', '7', '14', '', '1');
INSERT INTO `accesos` VALUES ('130', '7', '15', '', '1');
INSERT INTO `accesos` VALUES ('131', '7', '16', '', '1');
INSERT INTO `accesos` VALUES ('132', '7', '17', '', '1');
INSERT INTO `accesos` VALUES ('133', '7', '18', '', '1');
INSERT INTO `accesos` VALUES ('134', '7', '19', '', '1');
INSERT INTO `accesos` VALUES ('135', '7', '20', '', '1');
INSERT INTO `accesos` VALUES ('136', '3', '7', '', '1');
INSERT INTO `accesos` VALUES ('137', '2', '7', '', '1');
INSERT INTO `accesos` VALUES ('138', '1', '21', '', '1');
INSERT INTO `accesos` VALUES ('139', '1', '22', '', '1');
INSERT INTO `accesos` VALUES ('140', '1', '23', '', '1');
INSERT INTO `accesos` VALUES ('141', '1', '24', '', '1');
INSERT INTO `accesos` VALUES ('142', '1', '25', '', '1');
INSERT INTO `accesos` VALUES ('143', '1', '26', '', '1');
INSERT INTO `accesos` VALUES ('144', '1', '27', '', '1');
INSERT INTO `accesos` VALUES ('145', '1', '28', '', '1');
INSERT INTO `accesos` VALUES ('146', '1', '29', '', '1');
INSERT INTO `accesos` VALUES ('147', '1', '30', '', '1');
INSERT INTO `accesos` VALUES ('148', '1', '31', '', '1');
INSERT INTO `accesos` VALUES ('149', '1', '32', '', '1');
INSERT INTO `accesos` VALUES ('150', '1', '33', '', '1');
INSERT INTO `accesos` VALUES ('151', '1', '34', '', '1');
INSERT INTO `accesos` VALUES ('152', '1', '35', '', '1');
INSERT INTO `accesos` VALUES ('153', '1', '36', '', '1');
INSERT INTO `accesos` VALUES ('154', '2', '32', '', '1');
INSERT INTO `accesos` VALUES ('155', '2', '33', '', '1');
INSERT INTO `accesos` VALUES ('156', '2', '34', '', '1');
INSERT INTO `accesos` VALUES ('157', '2', '35', '', '1');
INSERT INTO `accesos` VALUES ('158', '2', '36', '', '1');
INSERT INTO `accesos` VALUES ('159', '2', '21', '', '1');
INSERT INTO `accesos` VALUES ('160', '2', '22', '', '1');
INSERT INTO `accesos` VALUES ('161', '2', '23', '', '1');
INSERT INTO `accesos` VALUES ('162', '2', '24', '', '1');
INSERT INTO `accesos` VALUES ('163', '2', '25', '', '1');
INSERT INTO `accesos` VALUES ('164', '2', '26', '', '1');
INSERT INTO `accesos` VALUES ('165', '2', '27', '', '1');
INSERT INTO `accesos` VALUES ('166', '2', '28', '', '1');
INSERT INTO `accesos` VALUES ('167', '2', '29', '', '1');
INSERT INTO `accesos` VALUES ('168', '2', '30', '', '1');
INSERT INTO `accesos` VALUES ('169', '2', '31', '', '1');
INSERT INTO `accesos` VALUES ('170', '3', '32', '', '1');
INSERT INTO `accesos` VALUES ('171', '3', '33', '', '1');
INSERT INTO `accesos` VALUES ('172', '3', '34', '', '1');
INSERT INTO `accesos` VALUES ('173', '3', '35', '', '1');
INSERT INTO `accesos` VALUES ('174', '3', '36', '', '1');
INSERT INTO `accesos` VALUES ('175', '3', '37', '', '1');
INSERT INTO `accesos` VALUES ('176', '3', '21', '', '1');
INSERT INTO `accesos` VALUES ('177', '3', '22', '', '1');
INSERT INTO `accesos` VALUES ('178', '3', '23', '', '1');
INSERT INTO `accesos` VALUES ('179', '3', '24', '', '1');
INSERT INTO `accesos` VALUES ('180', '3', '25', '', '1');
INSERT INTO `accesos` VALUES ('181', '3', '26', '', '1');
INSERT INTO `accesos` VALUES ('182', '3', '27', '', '1');
INSERT INTO `accesos` VALUES ('183', '3', '28', '', '1');
INSERT INTO `accesos` VALUES ('184', '3', '29', '', '1');
INSERT INTO `accesos` VALUES ('185', '3', '30', '', '1');
INSERT INTO `accesos` VALUES ('186', '3', '31', '', '1');
INSERT INTO `accesos` VALUES ('187', '1', '37', '', '1');
INSERT INTO `accesos` VALUES ('188', '2', '37', '', '1');
INSERT INTO `accesos` VALUES ('189', '1', '38', '', '1');
INSERT INTO `accesos` VALUES ('190', '1', '39', '', '1');
INSERT INTO `accesos` VALUES ('191', '1', '40', '', '1');
INSERT INTO `accesos` VALUES ('192', '1', '42', '', '1');
INSERT INTO `accesos` VALUES ('193', '1', '43', '', '1');
INSERT INTO `accesos` VALUES ('194', '2', '38', '', '1');
INSERT INTO `accesos` VALUES ('195', '2', '39', '', '1');
INSERT INTO `accesos` VALUES ('196', '2', '40', '', '1');
INSERT INTO `accesos` VALUES ('197', '2', '42', '', '1');
INSERT INTO `accesos` VALUES ('198', '2', '43', '', '1');
INSERT INTO `accesos` VALUES ('199', '1', '44', '', '1');
INSERT INTO `accesos` VALUES ('200', '1', '45', '', '1');
INSERT INTO `accesos` VALUES ('201', '1', '46', '', '1');
INSERT INTO `accesos` VALUES ('202', '1', '47', '', '1');
INSERT INTO `accesos` VALUES ('203', '1', '48', '', '1');
INSERT INTO `accesos` VALUES ('204', '1', '49', '', '1');
INSERT INTO `accesos` VALUES ('205', '1', '50', '', '1');
INSERT INTO `accesos` VALUES ('206', '1', '51', '', '1');
INSERT INTO `accesos` VALUES ('207', '1', '52', '', '1');
INSERT INTO `accesos` VALUES ('208', '1', '53', '', '1');
INSERT INTO `accesos` VALUES ('209', '1', '54', '', '1');
INSERT INTO `accesos` VALUES ('210', '1', '55', '', '1');
INSERT INTO `accesos` VALUES ('211', '1', '56', '', '1');
INSERT INTO `accesos` VALUES ('212', '1', '57', '', '1');
INSERT INTO `accesos` VALUES ('213', '1', '58', '', '1');
INSERT INTO `accesos` VALUES ('214', '1', '59', '', '1');
INSERT INTO `accesos` VALUES ('215', '1', '60', '', '1');
INSERT INTO `accesos` VALUES ('216', '1', '61', '', '1');
INSERT INTO `accesos` VALUES ('217', '1', '62', '', '1');
INSERT INTO `accesos` VALUES ('218', '1', '63', '', '1');
INSERT INTO `accesos` VALUES ('219', '2', '44', '', '1');
INSERT INTO `accesos` VALUES ('220', '2', '45', '', '1');
INSERT INTO `accesos` VALUES ('221', '2', '46', '', '1');
INSERT INTO `accesos` VALUES ('222', '2', '47', '', '1');
INSERT INTO `accesos` VALUES ('223', '2', '48', '', '1');
INSERT INTO `accesos` VALUES ('224', '2', '49', '', '1');
INSERT INTO `accesos` VALUES ('225', '2', '50', '', '1');
INSERT INTO `accesos` VALUES ('226', '2', '51', '', '1');
INSERT INTO `accesos` VALUES ('227', '2', '52', '', '1');
INSERT INTO `accesos` VALUES ('228', '2', '53', '', '1');
INSERT INTO `accesos` VALUES ('229', '2', '54', '', '1');
INSERT INTO `accesos` VALUES ('230', '2', '55', '', '1');
INSERT INTO `accesos` VALUES ('231', '2', '56', '', '1');
INSERT INTO `accesos` VALUES ('232', '2', '57', '', '1');
INSERT INTO `accesos` VALUES ('233', '2', '58', '', '1');
INSERT INTO `accesos` VALUES ('234', '2', '59', '', '1');
INSERT INTO `accesos` VALUES ('235', '2', '60', '', '1');
INSERT INTO `accesos` VALUES ('236', '2', '61', '', '1');
INSERT INTO `accesos` VALUES ('237', '2', '62', '', '1');
INSERT INTO `accesos` VALUES ('238', '2', '63', '', '1');
INSERT INTO `accesos` VALUES ('239', '1', '64', '', '1');
INSERT INTO `accesos` VALUES ('240', '1', '65', '', '1');
INSERT INTO `accesos` VALUES ('241', '1', '66', '', '1');
INSERT INTO `accesos` VALUES ('242', '1', '67', '', '1');
INSERT INTO `accesos` VALUES ('243', '1', '68', '', '1');
INSERT INTO `accesos` VALUES ('244', '2', '64', '', '1');
INSERT INTO `accesos` VALUES ('245', '2', '65', '', '1');
INSERT INTO `accesos` VALUES ('246', '2', '66', '', '1');
INSERT INTO `accesos` VALUES ('247', '2', '67', '', '1');
INSERT INTO `accesos` VALUES ('248', '2', '68', '', '1');
INSERT INTO `accesos` VALUES ('249', '1', '74', '', '1');
INSERT INTO `accesos` VALUES ('250', '1', '75', '', '1');
INSERT INTO `accesos` VALUES ('251', '1', '76', '', '1');
INSERT INTO `accesos` VALUES ('252', '1', '77', '', '1');
INSERT INTO `accesos` VALUES ('253', '1', '78', '', '1');
INSERT INTO `accesos` VALUES ('254', '1', '69', '', '1');
INSERT INTO `accesos` VALUES ('255', '1', '70', '', '1');
INSERT INTO `accesos` VALUES ('256', '1', '71', '', '1');
INSERT INTO `accesos` VALUES ('257', '1', '72', '', '1');
INSERT INTO `accesos` VALUES ('258', '1', '73', '', '1');
INSERT INTO `accesos` VALUES ('259', '2', '74', '', '1');
INSERT INTO `accesos` VALUES ('260', '2', '75', '', '1');
INSERT INTO `accesos` VALUES ('261', '2', '76', '', '1');
INSERT INTO `accesos` VALUES ('262', '2', '77', '', '1');
INSERT INTO `accesos` VALUES ('263', '2', '78', '', '1');
INSERT INTO `accesos` VALUES ('264', '2', '69', '', '1');
INSERT INTO `accesos` VALUES ('265', '2', '70', '', '1');
INSERT INTO `accesos` VALUES ('266', '2', '71', '', '1');
INSERT INTO `accesos` VALUES ('267', '2', '72', '', '1');
INSERT INTO `accesos` VALUES ('268', '2', '73', '', '1');
INSERT INTO `accesos` VALUES ('269', '1', '79', '', '1');
INSERT INTO `accesos` VALUES ('270', '1', '80', '', '1');
INSERT INTO `accesos` VALUES ('271', '1', '81', '', '1');
INSERT INTO `accesos` VALUES ('272', '1', '82', '', '1');
INSERT INTO `accesos` VALUES ('273', '1', '83', '', '1');
INSERT INTO `accesos` VALUES ('274', '1', '84', '', '1');
INSERT INTO `accesos` VALUES ('275', '2', '79', '', '1');
INSERT INTO `accesos` VALUES ('276', '2', '80', '', '1');
INSERT INTO `accesos` VALUES ('277', '2', '81', '', '1');
INSERT INTO `accesos` VALUES ('278', '2', '82', '', '1');
INSERT INTO `accesos` VALUES ('279', '2', '83', '', '1');
INSERT INTO `accesos` VALUES ('280', '2', '84', '', '1');
INSERT INTO `accesos` VALUES ('281', '1', '85', '', '1');
INSERT INTO `accesos` VALUES ('282', '1', '86', '', '1');
INSERT INTO `accesos` VALUES ('283', '1', '87', '', '1');
INSERT INTO `accesos` VALUES ('284', '1', '88', '', '1');
INSERT INTO `accesos` VALUES ('285', '1', '89', '', '1');
INSERT INTO `accesos` VALUES ('286', '1', '90', '', '1');
INSERT INTO `accesos` VALUES ('287', '1', '91', '', '1');
INSERT INTO `accesos` VALUES ('288', '1', '92', '', '1');
INSERT INTO `accesos` VALUES ('289', '1', '93', '', '1');
INSERT INTO `accesos` VALUES ('290', '1', '94', '', '1');
INSERT INTO `accesos` VALUES ('291', '2', '90', '', '1');
INSERT INTO `accesos` VALUES ('292', '2', '91', '', '1');
INSERT INTO `accesos` VALUES ('293', '2', '92', '', '1');
INSERT INTO `accesos` VALUES ('294', '2', '93', '', '1');
INSERT INTO `accesos` VALUES ('295', '2', '94', '', '1');
INSERT INTO `accesos` VALUES ('296', '2', '85', '', '1');
INSERT INTO `accesos` VALUES ('297', '2', '86', '', '1');
INSERT INTO `accesos` VALUES ('298', '2', '87', '', '1');
INSERT INTO `accesos` VALUES ('299', '2', '88', '', '1');
INSERT INTO `accesos` VALUES ('300', '2', '89', '', '1');
INSERT INTO `accesos` VALUES ('301', '1', '95', '', '1');
INSERT INTO `accesos` VALUES ('302', '1', '96', '', '1');
INSERT INTO `accesos` VALUES ('303', '1', '97', '', '1');
INSERT INTO `accesos` VALUES ('304', '1', '98', '', '1');
INSERT INTO `accesos` VALUES ('305', '1', '99', '', '1');
INSERT INTO `accesos` VALUES ('306', '1', '100', '', '1');
INSERT INTO `accesos` VALUES ('307', '1', '101', '', '1');
INSERT INTO `accesos` VALUES ('308', '1', '102', '', '1');
INSERT INTO `accesos` VALUES ('309', '1', '103', '', '1');
INSERT INTO `accesos` VALUES ('310', '1', '104', '', '1');
INSERT INTO `accesos` VALUES ('311', '1', '105', '', '1');
INSERT INTO `accesos` VALUES ('312', '1', '106', '', '1');
INSERT INTO `accesos` VALUES ('313', '1', '107', '', '1');
INSERT INTO `accesos` VALUES ('314', '1', '108', '', '1');
INSERT INTO `accesos` VALUES ('315', '1', '109', '', '1');
INSERT INTO `accesos` VALUES ('316', '1', '110', '', '1');
INSERT INTO `accesos` VALUES ('317', '1', '111', '', '1');
INSERT INTO `accesos` VALUES ('318', '1', '112', '', '1');
INSERT INTO `accesos` VALUES ('319', '1', '113', '', '1');
INSERT INTO `accesos` VALUES ('320', '1', '114', '', '1');
INSERT INTO `accesos` VALUES ('321', '1', '115', '', '1');
INSERT INTO `accesos` VALUES ('322', '1', '116', '', '1');
INSERT INTO `accesos` VALUES ('323', '1', '117', '', '1');
INSERT INTO `accesos` VALUES ('324', '1', '118', '', '1');
INSERT INTO `accesos` VALUES ('325', '1', '119', '', '1');
INSERT INTO `accesos` VALUES ('326', '1', '120', '', '1');
INSERT INTO `accesos` VALUES ('327', '2', '95', '', '1');
INSERT INTO `accesos` VALUES ('328', '2', '96', '', '1');
INSERT INTO `accesos` VALUES ('329', '2', '97', '', '1');
INSERT INTO `accesos` VALUES ('330', '2', '98', '', '1');
INSERT INTO `accesos` VALUES ('331', '2', '99', '', '1');
INSERT INTO `accesos` VALUES ('332', '2', '100', '', '1');
INSERT INTO `accesos` VALUES ('333', '2', '101', '', '1');
INSERT INTO `accesos` VALUES ('334', '2', '102', '', '1');
INSERT INTO `accesos` VALUES ('335', '2', '103', '', '1');
INSERT INTO `accesos` VALUES ('336', '2', '104', '', '1');
INSERT INTO `accesos` VALUES ('337', '2', '105', '', '1');
INSERT INTO `accesos` VALUES ('338', '2', '106', '', '1');
INSERT INTO `accesos` VALUES ('339', '2', '107', '', '1');
INSERT INTO `accesos` VALUES ('340', '2', '108', '', '1');
INSERT INTO `accesos` VALUES ('341', '2', '109', '', '1');
INSERT INTO `accesos` VALUES ('342', '2', '110', '', '1');
INSERT INTO `accesos` VALUES ('343', '2', '111', '', '1');
INSERT INTO `accesos` VALUES ('344', '2', '112', '', '1');
INSERT INTO `accesos` VALUES ('345', '2', '113', '', '1');
INSERT INTO `accesos` VALUES ('346', '2', '114', '', '1');
INSERT INTO `accesos` VALUES ('347', '2', '115', '', '1');
INSERT INTO `accesos` VALUES ('348', '2', '116', '', '1');
INSERT INTO `accesos` VALUES ('349', '2', '117', '', '1');
INSERT INTO `accesos` VALUES ('350', '2', '118', '', '1');
INSERT INTO `accesos` VALUES ('351', '2', '119', '', '1');
INSERT INTO `accesos` VALUES ('352', '2', '120', '', '1');
INSERT INTO `accesos` VALUES ('353', '1', '121', '', '1');
INSERT INTO `accesos` VALUES ('354', '2', '121', '', '1');
INSERT INTO `accesos` VALUES ('355', '1', '122', '', '1');
INSERT INTO `accesos` VALUES ('356', '1', '123', '', '1');
INSERT INTO `accesos` VALUES ('357', '1', '124', '', '1');
INSERT INTO `accesos` VALUES ('358', '1', '125', '', '1');
INSERT INTO `accesos` VALUES ('359', '1', '126', '', '1');
INSERT INTO `accesos` VALUES ('360', '2', '122', '', '1');
INSERT INTO `accesos` VALUES ('361', '2', '123', '', '1');
INSERT INTO `accesos` VALUES ('362', '2', '124', '', '1');
INSERT INTO `accesos` VALUES ('363', '2', '125', '', '1');
INSERT INTO `accesos` VALUES ('364', '2', '126', '', '1');
INSERT INTO `accesos` VALUES ('365', '1', '127', '', '1');
INSERT INTO `accesos` VALUES ('366', '1', '128', '', '1');
INSERT INTO `accesos` VALUES ('367', '2', '127', '', '1');
INSERT INTO `accesos` VALUES ('368', '2', '128', '', '1');
INSERT INTO `accesos` VALUES ('369', '3', '74', '', '1');
INSERT INTO `accesos` VALUES ('370', '3', '75', '', '1');
INSERT INTO `accesos` VALUES ('371', '3', '76', '', '1');
INSERT INTO `accesos` VALUES ('372', '3', '77', '', '1');
INSERT INTO `accesos` VALUES ('373', '3', '78', '', '1');
INSERT INTO `accesos` VALUES ('374', '3', '38', '', '1');
INSERT INTO `accesos` VALUES ('375', '3', '39', '', '1');
INSERT INTO `accesos` VALUES ('376', '3', '40', '', '1');
INSERT INTO `accesos` VALUES ('377', '3', '42', '', '1');
INSERT INTO `accesos` VALUES ('378', '3', '43', '', '1');
INSERT INTO `accesos` VALUES ('379', '3', '90', '', '1');
INSERT INTO `accesos` VALUES ('380', '3', '91', '', '1');
INSERT INTO `accesos` VALUES ('381', '3', '92', '', '1');
INSERT INTO `accesos` VALUES ('382', '3', '93', '', '1');
INSERT INTO `accesos` VALUES ('383', '3', '94', '', '1');
INSERT INTO `accesos` VALUES ('384', '3', '44', '', '1');
INSERT INTO `accesos` VALUES ('385', '3', '45', '', '1');
INSERT INTO `accesos` VALUES ('386', '3', '46', '', '1');
INSERT INTO `accesos` VALUES ('387', '3', '47', '', '1');
INSERT INTO `accesos` VALUES ('388', '3', '48', '', '1');
INSERT INTO `accesos` VALUES ('389', '3', '49', '', '1');
INSERT INTO `accesos` VALUES ('390', '3', '50', '', '1');
INSERT INTO `accesos` VALUES ('391', '3', '51', '', '1');
INSERT INTO `accesos` VALUES ('392', '3', '52', '', '1');
INSERT INTO `accesos` VALUES ('393', '3', '53', '', '1');
INSERT INTO `accesos` VALUES ('394', '3', '54', '', '1');
INSERT INTO `accesos` VALUES ('395', '3', '55', '', '1');
INSERT INTO `accesos` VALUES ('396', '3', '56', '', '1');
INSERT INTO `accesos` VALUES ('397', '3', '57', '', '1');
INSERT INTO `accesos` VALUES ('398', '3', '58', '', '1');
INSERT INTO `accesos` VALUES ('399', '3', '59', '', '1');
INSERT INTO `accesos` VALUES ('400', '3', '60', '', '1');
INSERT INTO `accesos` VALUES ('401', '3', '61', '', '1');
INSERT INTO `accesos` VALUES ('402', '3', '62', '', '1');
INSERT INTO `accesos` VALUES ('403', '3', '63', '', '1');
INSERT INTO `accesos` VALUES ('404', '3', '64', '', '1');
INSERT INTO `accesos` VALUES ('405', '3', '65', '', '1');
INSERT INTO `accesos` VALUES ('406', '3', '66', '', '1');
INSERT INTO `accesos` VALUES ('407', '3', '67', '', '1');
INSERT INTO `accesos` VALUES ('408', '3', '68', '', '1');
INSERT INTO `accesos` VALUES ('409', '3', '69', '', '1');
INSERT INTO `accesos` VALUES ('410', '3', '70', '', '1');
INSERT INTO `accesos` VALUES ('411', '3', '71', '', '1');
INSERT INTO `accesos` VALUES ('412', '3', '72', '', '1');
INSERT INTO `accesos` VALUES ('413', '3', '73', '', '1');
INSERT INTO `accesos` VALUES ('414', '3', '79', '', '1');
INSERT INTO `accesos` VALUES ('415', '3', '80', '', '1');
INSERT INTO `accesos` VALUES ('416', '3', '81', '', '1');
INSERT INTO `accesos` VALUES ('417', '3', '82', '', '1');
INSERT INTO `accesos` VALUES ('418', '3', '83', '', '1');
INSERT INTO `accesos` VALUES ('419', '3', '122', '', '1');
INSERT INTO `accesos` VALUES ('420', '3', '123', '', '1');
INSERT INTO `accesos` VALUES ('421', '3', '124', '', '1');
INSERT INTO `accesos` VALUES ('422', '3', '125', '', '1');
INSERT INTO `accesos` VALUES ('423', '3', '126', '', '1');
INSERT INTO `accesos` VALUES ('424', '3', '95', '', '1');
INSERT INTO `accesos` VALUES ('425', '3', '96', '', '1');
INSERT INTO `accesos` VALUES ('426', '3', '97', '', '1');
INSERT INTO `accesos` VALUES ('427', '3', '98', '', '1');
INSERT INTO `accesos` VALUES ('428', '3', '99', '', '1');
INSERT INTO `accesos` VALUES ('429', '3', '100', '', '1');
INSERT INTO `accesos` VALUES ('430', '3', '101', '', '1');
INSERT INTO `accesos` VALUES ('431', '3', '102', '', '1');
INSERT INTO `accesos` VALUES ('432', '3', '103', '', '1');
INSERT INTO `accesos` VALUES ('433', '3', '104', '', '1');
INSERT INTO `accesos` VALUES ('434', '3', '105', '', '1');
INSERT INTO `accesos` VALUES ('435', '3', '106', '', '1');
INSERT INTO `accesos` VALUES ('436', '3', '107', '', '1');
INSERT INTO `accesos` VALUES ('437', '3', '108', '', '1');
INSERT INTO `accesos` VALUES ('438', '3', '109', '', '1');
INSERT INTO `accesos` VALUES ('439', '3', '110', '', '1');
INSERT INTO `accesos` VALUES ('440', '3', '111', '', '1');
INSERT INTO `accesos` VALUES ('441', '3', '112', '', '1');
INSERT INTO `accesos` VALUES ('442', '3', '113', '', '1');
INSERT INTO `accesos` VALUES ('443', '3', '114', '', '1');
INSERT INTO `accesos` VALUES ('444', '3', '115', '', '1');
INSERT INTO `accesos` VALUES ('445', '3', '116', '', '1');
INSERT INTO `accesos` VALUES ('446', '3', '117', '', '1');
INSERT INTO `accesos` VALUES ('447', '3', '118', '', '1');
INSERT INTO `accesos` VALUES ('448', '3', '119', '', '1');
INSERT INTO `accesos` VALUES ('449', '3', '120', '', '1');
INSERT INTO `accesos` VALUES ('450', '3', '84', '', '1');
INSERT INTO `accesos` VALUES ('451', '3', '85', '', '1');
INSERT INTO `accesos` VALUES ('452', '3', '86', '', '1');
INSERT INTO `accesos` VALUES ('453', '3', '87', '', '1');
INSERT INTO `accesos` VALUES ('454', '3', '88', '', '1');
INSERT INTO `accesos` VALUES ('455', '3', '89', '', '1');
INSERT INTO `accesos` VALUES ('456', '3', '121', '', '1');
INSERT INTO `accesos` VALUES ('457', '3', '127', '', '1');
INSERT INTO `accesos` VALUES ('458', '3', '128', '', '1');
INSERT INTO `accesos` VALUES ('459', '1', '130', '', '1');
INSERT INTO `accesos` VALUES ('460', '1', '131', '', '1');
INSERT INTO `accesos` VALUES ('461', '1', '132', '', '1');
INSERT INTO `accesos` VALUES ('462', '1', '133', '', '1');
INSERT INTO `accesos` VALUES ('463', '1', '134', '', '1');
INSERT INTO `accesos` VALUES ('464', '1', '135', '', '1');
INSERT INTO `accesos` VALUES ('465', '1', '136', '', '1');
INSERT INTO `accesos` VALUES ('466', '2', '130', '', '1');
INSERT INTO `accesos` VALUES ('467', '2', '131', '', '1');
INSERT INTO `accesos` VALUES ('468', '2', '132', '', '1');
INSERT INTO `accesos` VALUES ('469', '2', '133', '', '1');
INSERT INTO `accesos` VALUES ('470', '2', '134', '', '1');
INSERT INTO `accesos` VALUES ('471', '2', '135', '', '1');
INSERT INTO `accesos` VALUES ('472', '2', '136', '', '1');
INSERT INTO `accesos` VALUES ('473', '3', '130', '', '1');
INSERT INTO `accesos` VALUES ('474', '3', '131', '', '1');
INSERT INTO `accesos` VALUES ('475', '3', '132', '', '1');
INSERT INTO `accesos` VALUES ('476', '3', '133', '', '1');
INSERT INTO `accesos` VALUES ('477', '3', '134', '', '1');
INSERT INTO `accesos` VALUES ('478', '3', '135', '', '1');
INSERT INTO `accesos` VALUES ('479', '3', '136', '', '1');

-- ----------------------------
-- Table structure for acuerdo_pago
-- ----------------------------
DROP TABLE IF EXISTS `acuerdo_pago`;
CREATE TABLE `acuerdo_pago` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_periodo_id` int(10) unsigned NOT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `rango` smallint(5) unsigned NOT NULL DEFAULT '0',
  `partes` smallint(5) unsigned NOT NULL DEFAULT '0',
  `descripcion` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `estado` smallint(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `acuerdo_pago_tipo_periodo_id_foreign` (`tipo_periodo_id`),
  CONSTRAINT `acuerdo_pago_tipo_periodo_id_foreign` FOREIGN KEY (`tipo_periodo_id`) REFERENCES `tipo_periodo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of acuerdo_pago
-- ----------------------------
INSERT INTO `acuerdo_pago` VALUES ('1', '4', 'MENSUAL', '0', '0', 'Cuota Mensual programada para un sólo pago por mes, manejado Bajo Autorización.', '1');
INSERT INTO `acuerdo_pago` VALUES ('2', '4', 'QUINCENAL', '15', '2', 'Cuota Mensual dividido en 2, manejado sólo Bajo Autorización', '1');
INSERT INTO `acuerdo_pago` VALUES ('3', '4', 'SEMANAL', '7', '4', 'Cuota Mensual dividida entre 4 abonos semanales por mes, Procedimiento Regular para ASCVEH.', '1');

-- ----------------------------
-- Table structure for agente_comercial
-- ----------------------------
DROP TABLE IF EXISTS `agente_comercial`;
CREATE TABLE `agente_comercial` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cliente_id` int(10) unsigned NOT NULL,
  `empleado_id` int(10) unsigned NOT NULL,
  `estado` smallint(5) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `agente_comercial_cliente_id_foreign` (`cliente_id`),
  KEY `agente_comercial_empleado_id_foreign` (`empleado_id`),
  CONSTRAINT `agente_comercial_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `empleado` (`id`),
  CONSTRAINT `agente_comercial_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of agente_comercial
-- ----------------------------
INSERT INTO `agente_comercial` VALUES ('1', '1', '4', '1', '2016-03-02 17:29:47', '2016-03-02 17:29:47');
INSERT INTO `agente_comercial` VALUES ('2', '2', '5', '1', '2016-03-02 17:33:11', '2016-03-02 17:33:11');
INSERT INTO `agente_comercial` VALUES ('3', '3', '5', '1', '2016-04-14 13:03:33', '2016-04-14 13:03:33');
INSERT INTO `agente_comercial` VALUES ('4', '4', '6', '1', '2016-04-14 13:07:21', '2016-04-14 13:07:21');

-- ----------------------------
-- Table structure for area
-- ----------------------------
DROP TABLE IF EXISTS `area`;
CREATE TABLE `area` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of area
-- ----------------------------
INSERT INTO `area` VALUES ('1', 'Inspecciones', 'Inspector Vehicular', '1');
INSERT INTO `area` VALUES ('2', 'Gerencia', 'Gerente', '1');
INSERT INTO `area` VALUES ('3', 'Comercial', 'Ventas', '1');
INSERT INTO `area` VALUES ('4', 'Legal', 'Atención al Cliente & Legal', '1');
INSERT INTO `area` VALUES ('5', 'Administración', 'Administrador', '1');
INSERT INTO `area` VALUES ('6', 'Cajas', 'Cajera', '1');

-- ----------------------------
-- Table structure for aval
-- ----------------------------
DROP TABLE IF EXISTS `aval`;
CREATE TABLE `aval` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `persona_id` int(10) unsigned NOT NULL,
  `persona_id_padre` int(10) unsigned NOT NULL,
  `estado` int(10) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `aval_persona_id_foreign` (`persona_id`),
  KEY `aval_persona_id_padre_index` (`persona_id_padre`),
  CONSTRAINT `aval_persona_id_foreign` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of aval
-- ----------------------------
INSERT INTO `aval` VALUES ('1', '69', '3', '1', '2016-04-21 12:22:49', '2016-04-21 12:22:49');

-- ----------------------------
-- Table structure for bitacora
-- ----------------------------
DROP TABLE IF EXISTS `bitacora`;
CREATE TABLE `bitacora` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fecha_salida` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `bitacora_user_id_foreign` (`user_id`),
  CONSTRAINT `bitacora_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of bitacora
-- ----------------------------

-- ----------------------------
-- Table structure for cargo
-- ----------------------------
DROP TABLE IF EXISTS `cargo`;
CREATE TABLE `cargo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `area_id` int(10) unsigned NOT NULL,
  `nombre` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `cargo_area_id_foreign` (`area_id`),
  KEY `cargo_nombre_index` (`nombre`),
  CONSTRAINT `cargo_area_id_foreign` FOREIGN KEY (`area_id`) REFERENCES `area` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of cargo
-- ----------------------------
INSERT INTO `cargo` VALUES ('1', '3', 'Asesor de Ventas', 'Ventas', '1');
INSERT INTO `cargo` VALUES ('2', '3', 'Jefe de Ventas', 'Jefatura Comercial', '1');
INSERT INTO `cargo` VALUES ('3', '1', 'Inspector Vehicular', 'Inspector Vehicular', '1');
INSERT INTO `cargo` VALUES ('4', '2', 'Gerente General', 'Gerencia', '1');
INSERT INTO `cargo` VALUES ('5', '5', 'Administrador', 'Administrador', '1');
INSERT INTO `cargo` VALUES ('6', '6', 'Cajera', 'Cajas', '1');
INSERT INTO `cargo` VALUES ('7', '4', 'Atención al Cliente & Legal', 'Legal', '1');

-- ----------------------------
-- Table structure for cargo_empleado
-- ----------------------------
DROP TABLE IF EXISTS `cargo_empleado`;
CREATE TABLE `cargo_empleado` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cargo_id` int(10) unsigned NOT NULL,
  `empleado_id` int(10) unsigned NOT NULL,
  `estado` int(10) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cargo_empleado_empleado_id_foreign` (`empleado_id`),
  KEY `cargo_empleado_cargo_id_foreign` (`cargo_id`),
  CONSTRAINT `cargo_empleado_cargo_id_foreign` FOREIGN KEY (`cargo_id`) REFERENCES `cargo` (`id`),
  CONSTRAINT `cargo_empleado_empleado_id_foreign` FOREIGN KEY (`empleado_id`) REFERENCES `empleado` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of cargo_empleado
-- ----------------------------
INSERT INTO `cargo_empleado` VALUES ('1', '1', '1', '1', '2016-02-27 12:42:09', '2016-02-27 12:42:09');
INSERT INTO `cargo_empleado` VALUES ('2', '7', '2', '1', '2016-02-27 12:51:25', '2016-02-27 12:51:25');
INSERT INTO `cargo_empleado` VALUES ('3', '3', '3', '1', '2016-02-27 12:52:06', '2016-02-27 12:52:06');
INSERT INTO `cargo_empleado` VALUES ('4', '4', '4', '1', '2016-02-27 12:52:22', '2016-02-27 12:52:22');
INSERT INTO `cargo_empleado` VALUES ('5', '2', '5', '1', '2016-02-27 12:52:35', '2016-02-27 12:52:35');
INSERT INTO `cargo_empleado` VALUES ('6', '1', '6', '1', '2016-02-27 12:52:51', '2016-02-27 12:52:51');
INSERT INTO `cargo_empleado` VALUES ('7', '5', '7', '1', '2016-02-27 12:53:04', '2016-02-27 12:53:04');

-- ----------------------------
-- Table structure for clase_vehiculo
-- ----------------------------
DROP TABLE IF EXISTS `clase_vehiculo`;
CREATE TABLE `clase_vehiculo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `estado` smallint(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of clase_vehiculo
-- ----------------------------
INSERT INTO `clase_vehiculo` VALUES ('1', 'Automóvil', '', '1');
INSERT INTO `clase_vehiculo` VALUES ('2', 'Autobús', '', '1');
INSERT INTO `clase_vehiculo` VALUES ('3', 'Cabezales', '', '1');
INSERT INTO `clase_vehiculo` VALUES ('4', 'Cuadrimoto', '', '1');
INSERT INTO `clase_vehiculo` VALUES ('5', 'Tricimoto', '', '1');
INSERT INTO `clase_vehiculo` VALUES ('6', 'Microbús', '', '1');
INSERT INTO `clase_vehiculo` VALUES ('7', 'Motocicletas', '', '1');
INSERT INTO `clase_vehiculo` VALUES ('8', 'Panel', '', '1');
INSERT INTO `clase_vehiculo` VALUES ('9', 'Pick Up', '', '1');
INSERT INTO `clase_vehiculo` VALUES ('10', 'Remolques', '', '1');
INSERT INTO `clase_vehiculo` VALUES ('11', 'Camiones', '', '1');
INSERT INTO `clase_vehiculo` VALUES ('12', 'Automovil', 'Automovil', '0');

-- ----------------------------
-- Table structure for cliente
-- ----------------------------
DROP TABLE IF EXISTS `cliente`;
CREATE TABLE `cliente` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `persona_id` int(10) unsigned NOT NULL,
  `codigo` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tipo` smallint(5) unsigned NOT NULL DEFAULT '1',
  `persona_id_padre` int(10) unsigned NOT NULL,
  `estado` int(10) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cliente_persona_id_foreign` (`persona_id`),
  KEY `cliente_persona_id_padre_index` (`persona_id_padre`),
  CONSTRAINT `cliente_persona_id_foreign` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of cliente
-- ----------------------------
INSERT INTO `cliente` VALUES ('1', '26', 'telpdwf1', '1', '3', '0', '2016-03-02 17:29:47', '2016-03-02 17:29:53');
INSERT INTO `cliente` VALUES ('2', '49', 'qeeuuxc2', '1', '3', '1', '2016-03-02 17:33:11', '2016-03-02 17:33:11');
INSERT INTO `cliente` VALUES ('3', '47', 'ftmnfpn3', '1', '3', '1', '2016-04-14 13:03:33', '2016-04-14 13:03:33');
INSERT INTO `cliente` VALUES ('4', '34', 'ohpwcqs4', '1', '3', '1', '2016-04-14 13:07:21', '2016-04-14 13:07:21');

-- ----------------------------
-- Table structure for control
-- ----------------------------
DROP TABLE IF EXISTS `control`;
CREATE TABLE `control` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `control_padre_id` int(10) unsigned DEFAULT NULL,
  `tipo_control_id` int(10) unsigned NOT NULL,
  `jerarquia` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `valor` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `glosa` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `control_control_padre_id_foreign` (`control_padre_id`),
  KEY `control_tipo_control_id_foreign` (`tipo_control_id`),
  CONSTRAINT `control_control_padre_id_foreign` FOREIGN KEY (`control_padre_id`) REFERENCES `control` (`id`),
  CONSTRAINT `control_tipo_control_id_foreign` FOREIGN KEY (`tipo_control_id`) REFERENCES `tipo_control` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of control
-- ----------------------------
INSERT INTO `control` VALUES ('1', null, '1', '10', 'Gestión de Información', '', 'Gestión de Información', '', '1');
INSERT INTO `control` VALUES ('2', '1', '2', '1001', 'Personas', 'personas', 'fa fa-users', '', '1');
INSERT INTO `control` VALUES ('3', '2', '2', '100101', 'Personas', 'personas.personas', 'sub-menu', '', '1');
INSERT INTO `control` VALUES ('4', '3', '3', '10010101', 'Listar', 'list', 'fa-list', '', '1');
INSERT INTO `control` VALUES ('5', '3', '3', '10010102', 'Nuevo', 'new', 'fa-plus', '', '1');
INSERT INTO `control` VALUES ('6', '3', '3', '10010103', 'Editar', 'edit', 'fa-pencil-square-o', '', '1');
INSERT INTO `control` VALUES ('7', '3', '3', '10010104', 'Eliminar', 'delete', 'fa-trash-o', '', '1');
INSERT INTO `control` VALUES ('8', '1', '2', '1002', 'Accesos', 'accesos', 'fa fa-key', '', '1');
INSERT INTO `control` VALUES ('9', '8', '2', '100201', 'Roles', 'accesos.roles', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('10', '9', '3', '10020101', 'Listar', 'list', 'fa-list', 'inbarra', '1');
INSERT INTO `control` VALUES ('11', '9', '3', '10020102', 'Nuevo', 'new', 'fa-plus', 'inbarra', '1');
INSERT INTO `control` VALUES ('12', '9', '3', '10020103', 'Editar', 'edit', 'fa-pencil-square-o', 'intable', '1');
INSERT INTO `control` VALUES ('13', '9', '3', '10020104', 'Eliminar', 'delete', 'fa-trash-o', 'intable', '1');
INSERT INTO `control` VALUES ('14', '9', '3', '10020105', 'Accesos Rol', 'access', 'Fa-check-square-o', 'inbarra', '1');
INSERT INTO `control` VALUES ('15', '8', '2', '100202', 'Usuarios', 'accesos.usuarios', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('16', '15', '3', '10020201', 'Listar', 'list', 'fa-list', '', '1');
INSERT INTO `control` VALUES ('17', '15', '3', '10020202', 'Nuevo', 'new', 'fa-plus', '', '1');
INSERT INTO `control` VALUES ('18', '15', '3', '10020203', 'Editar', 'edit', 'fa-pencil-square-o', '', '1');
INSERT INTO `control` VALUES ('19', '15', '3', '10020204', 'Eliminar', 'delete', 'fa-trash-o', '', '1');
INSERT INTO `control` VALUES ('20', '15', '3', '10020205', 'accesos', 'access', 'fa-check-square-o', '', '1');
INSERT INTO `control` VALUES ('21', '1', '2', '1003', 'Registros', 'registros', 'fa fa-table', '', '1');
INSERT INTO `control` VALUES ('22', '21', '2', '100301', 'Areas', 'registros.areas', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('23', '22', '3', '10030101', 'Listar', 'list', 'fa-list', 'inbarra', '1');
INSERT INTO `control` VALUES ('24', '22', '3', '10030102', 'Nuevo', 'new', 'fa-plus', 'inbarra', '1');
INSERT INTO `control` VALUES ('25', '22', '3', '10030103', 'Editar', 'edit', 'fa-pencil-square-o', 'intable', '1');
INSERT INTO `control` VALUES ('26', '22', '3', '10030104', 'Eliminar', 'delete', 'fa-trash-o', 'intable', '1');
INSERT INTO `control` VALUES ('27', '21', '2', '100302', 'Cargos', 'registros.cargos', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('28', '27', '3', '10030201', 'Listar', 'list', 'fa-list', 'inbarra', '1');
INSERT INTO `control` VALUES ('29', '27', '3', '10030202', 'Nuevo', 'new', 'fa-plus', 'inbarra', '1');
INSERT INTO `control` VALUES ('30', '27', '3', '10030203', 'Editar', 'edit', 'fa-pencil-square-o', 'intable', '1');
INSERT INTO `control` VALUES ('31', '27', '3', '10030204', 'Eliminar', 'delete', 'fa-trash-o', 'intable', '1');
INSERT INTO `control` VALUES ('32', '2', '2', '100103', 'Empleados', 'personas.empleados', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('33', '32', '3', '10010201', 'Listar', 'list', 'fa-list', '', '1');
INSERT INTO `control` VALUES ('34', '32', '3', '10010202', 'Nuevo', 'new', 'fa-plus', '', '1');
INSERT INTO `control` VALUES ('35', '32', '3', '10010203', 'Editar', 'edit', 'fa-pencil-square-o', '', '1');
INSERT INTO `control` VALUES ('36', '32', '3', '10010204', 'Eliminar', 'delete', 'fa-trash-o', '', '1');
INSERT INTO `control` VALUES ('37', '32', '3', '10010205', 'Asignar Nuevo Cargo', 'asignar', 'fa-exchange', '', '1');
INSERT INTO `control` VALUES ('38', '2', '2', '100104', 'Clientes', 'personas.clientes', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('39', '38', '3', '10010301', 'Listar', 'list', 'fa-list', '', '1');
INSERT INTO `control` VALUES ('40', '38', '3', '10010302', 'Nuevo', 'new', 'fa-plus', '', '1');
INSERT INTO `control` VALUES ('41', '38', '3', '10010303', 'Editar', 'edit', 'fa-pencil-square-o', '', '0');
INSERT INTO `control` VALUES ('42', '38', '3', '10010304', 'Eliminar', 'delete', 'fa-trash-o', '', '1');
INSERT INTO `control` VALUES ('43', '38', '3', '10010305', 'Cambiar Agente Com.', 'update-agente', 'fa-exchange', '', '1');
INSERT INTO `control` VALUES ('44', '21', '2', '100303', 'Tipo de Prestamos', 'registros.tipo-prestamos', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('45', '44', '3', '10030301', 'Listar', 'list', 'fa-list', 'inbarra', '1');
INSERT INTO `control` VALUES ('46', '44', '3', '10030302', 'Nuevo', 'new', 'fa-plus', 'inbarra', '1');
INSERT INTO `control` VALUES ('47', '44', '3', '10030303', 'Editar', 'edit', 'fa-pencil-square-o', 'intable', '1');
INSERT INTO `control` VALUES ('48', '44', '3', '10030304', 'Eliminar', 'delete', 'fa-trash-o', 'intable', '1');
INSERT INTO `control` VALUES ('49', '21', '2', '100304', 'Tipo de Monedas', 'registros.tipo-monedas', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('50', '49', '3', '10030401', 'Listar', 'list', 'fa-list', 'inbarra', '1');
INSERT INTO `control` VALUES ('51', '49', '3', '10030402', 'Nuevo', 'new', 'fa-plus', 'inbarra', '1');
INSERT INTO `control` VALUES ('52', '49', '3', '10030403', 'Editar', 'edit', 'fa-pencil-square-o', 'intable', '1');
INSERT INTO `control` VALUES ('53', '49', '3', '10030404', 'Eliminar', 'delete', 'fa-trash-o', 'intable', '1');
INSERT INTO `control` VALUES ('54', '21', '2', '100305', 'Tipo de Periodos', 'registros.tipo-periodos', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('55', '54', '3', '10030501', 'Listar', 'list', 'fa-list', 'inbarra', '1');
INSERT INTO `control` VALUES ('56', '54', '3', '10030502', 'Nuevo', 'new', 'fa-plus', 'inbarra', '1');
INSERT INTO `control` VALUES ('57', '54', '3', '10030503', 'Editar', 'edit', 'fa-pencil-square-o', 'intable', '1');
INSERT INTO `control` VALUES ('58', '54', '3', '10030504', 'Eliminar', 'delete', 'fa-trash-o', 'intable', '1');
INSERT INTO `control` VALUES ('59', '21', '2', '100306', 'Tipo de Garantias', 'registros.tipo-garantias', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('60', '59', '3', '10030601', 'Listar', 'list', 'fa-list', 'inbarra', '1');
INSERT INTO `control` VALUES ('61', '59', '3', '10030602', 'Nuevo', 'new', 'fa-plus', 'inbarra', '1');
INSERT INTO `control` VALUES ('62', '59', '3', '10030603', 'Editar', 'edit', 'fa-pencil-square-o', 'intable', '1');
INSERT INTO `control` VALUES ('63', '59', '3', '10030604', 'Eliminar', 'delete', 'fa-trash-o', 'intable', '1');
INSERT INTO `control` VALUES ('64', '21', '2', '100307', 'Tipo de Pagos', 'registros.tipo-pagos', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('65', '64', '3', '10030701', 'Listar', 'list', 'fa-list', 'inbarra', '1');
INSERT INTO `control` VALUES ('66', '64', '3', '10030702', 'Nuevo', 'new', 'fa-plus', 'inbarra', '1');
INSERT INTO `control` VALUES ('67', '64', '3', '10030703', 'Editar', 'edit', 'fa-pencil-square-o', 'intable', '1');
INSERT INTO `control` VALUES ('68', '64', '3', '10030704', 'Eliminar', 'delete', 'fa-trash-o', 'intable', '1');
INSERT INTO `control` VALUES ('69', '21', '2', '100308', 'Licencias', 'registros.licencias', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('70', '69', '3', '10030801', 'Listar', 'list', 'fa-list', 'inbarra', '1');
INSERT INTO `control` VALUES ('71', '69', '3', '10030802', 'Nuevo', 'new', 'fa-plus', 'inbarra', '1');
INSERT INTO `control` VALUES ('72', '69', '3', '10030803', 'Editar', 'edit', 'fa-pencil-square-o', 'intable', '1');
INSERT INTO `control` VALUES ('73', '69', '3', '10030804', 'Eliminar', 'delete', 'fa-trash-o', 'intable', '1');
INSERT INTO `control` VALUES ('74', '2', '2', '100102', 'Empresas', 'personas.empresas', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('75', '74', '3', '10010401', 'Listar', 'list', 'fa-list', '', '1');
INSERT INTO `control` VALUES ('76', '74', '3', '10010402', 'Nuevo', 'new', 'fa-plus', '', '1');
INSERT INTO `control` VALUES ('77', '74', '3', '10010403', 'Editar', 'edit', 'fa-pencil-square-o', '', '1');
INSERT INTO `control` VALUES ('78', '74', '3', '10010404', 'Eliminar', 'delete', 'fa-trash-o', '', '1');
INSERT INTO `control` VALUES ('79', '21', '2', '100309', 'Tipo de Cambio', 'registros.tipo-cambios', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('80', '79', '3', '10030901', 'Listar', 'list', 'fa-list', 'inbarra', '1');
INSERT INTO `control` VALUES ('81', '79', '3', '10030902', 'Nuevo', 'new', 'fa-plus', 'inbarra', '1');
INSERT INTO `control` VALUES ('82', '79', '3', '10030903', 'Editar', 'edit', 'fa-pencil-square-o', 'intable', '1');
INSERT INTO `control` VALUES ('83', '79', '3', '10030904', 'Eliminar', 'delete', 'fa-trash-o', 'intable', '1');
INSERT INTO `control` VALUES ('84', null, '1', '20', 'Movimientos', '', 'Movimientos', '', '1');
INSERT INTO `control` VALUES ('85', '84', '2', '2001', 'Operaciones', 'operaciones', 'fa fa-list-alt', '', '1');
INSERT INTO `control` VALUES ('86', '85', '2', '200101', 'ASCVEH', 'operaciones.prestamos', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('87', '86', '3', '20010101', 'Listar', 'list', 'fa-list', '', '1');
INSERT INTO `control` VALUES ('88', '86', '3', '20010102', 'Nuevo', 'new', 'fa-plus', '', '1');
INSERT INTO `control` VALUES ('89', '86', '3', '20010103', 'Editar', 'edit', 'fa-pencil-square-o', '', '1');
INSERT INTO `control` VALUES ('90', '2', '2', '100105', 'Avales', 'personas.avales', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('91', '90', '3', '10010501', 'Listar', 'list', 'fa-list', '', '1');
INSERT INTO `control` VALUES ('92', '90', '3', '10010502', 'Nuevo', 'new', 'fa-plus', '', '1');
INSERT INTO `control` VALUES ('93', '90', '3', '10010503', 'Editar', 'edit', 'fa-pencil-square-o', '', '1');
INSERT INTO `control` VALUES ('94', '90', '3', '10010504', 'Eliminar', 'delete', 'fa-trash-o', '', '1');
INSERT INTO `control` VALUES ('95', '1', '2', '1004', 'Vehiculos', 'vehiculos', 'fa fa-car', '', '1');
INSERT INTO `control` VALUES ('96', '95', '2', '100401', 'Vehiculos', 'vehiculos.vehiculos', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('97', '96', '3', '10040101', 'Listar', 'list', 'fa-list', 'inbarra', '1');
INSERT INTO `control` VALUES ('98', '96', '3', '10040102', 'Nuevo', 'new', 'fa-plus', 'inbarra', '1');
INSERT INTO `control` VALUES ('99', '96', '3', '10040103', 'Editar', 'edit', 'fa-pencil-square-o', 'inbarra', '1');
INSERT INTO `control` VALUES ('100', '96', '3', '10040104', 'Eliminar', 'delete', 'fa-trash-o', 'inbarra', '1');
INSERT INTO `control` VALUES ('101', '95', '2', '100402', 'Clase de Vehiculos', 'vehiculos.clase-vehiculos', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('102', '101', '3', '10040201', 'Listar', 'list', 'fa-list', 'inbarra', '1');
INSERT INTO `control` VALUES ('103', '101', '3', '10040202', 'Nuevo', 'new', 'fa-plus', 'inbarra', '1');
INSERT INTO `control` VALUES ('104', '101', '3', '10040203', 'Editar', 'edit', 'fa-pencil-square-o', 'intable', '1');
INSERT INTO `control` VALUES ('105', '101', '3', '10040204', 'Eliminar', 'delete', 'fa-trash-o', 'intable', '1');
INSERT INTO `control` VALUES ('106', '95', '2', '100403', 'Tipo de Vehiculo', 'vehiculos.tipo-vehiculos', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('107', '106', '3', '10040301', 'Listar', 'list', 'fa-list', 'inbarra', '1');
INSERT INTO `control` VALUES ('108', '106', '3', '10040302', 'Nuevo', 'new', 'fa-plus', 'inbarra', '1');
INSERT INTO `control` VALUES ('109', '106', '3', '10040303', 'Editar', 'edit', 'fa-pencil-square-o', 'intable', '1');
INSERT INTO `control` VALUES ('110', '106', '3', '10040304', 'Eliminar', 'delete', 'fa-trash-o', 'intable', '1');
INSERT INTO `control` VALUES ('111', '95', '2', '100404', 'Marcas', 'vehiculos.marcas', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('112', '111', '3', '10040401', 'Listar', 'list', 'fa-list', 'inbarra', '1');
INSERT INTO `control` VALUES ('113', '111', '3', '10040402', 'Nuevo', 'new', 'fa-plus', 'inbarra', '1');
INSERT INTO `control` VALUES ('114', '111', '3', '10040403', 'Editar', 'edit', 'fa-pencil-square-o', 'intable', '1');
INSERT INTO `control` VALUES ('115', '111', '3', '10040404', 'Eliminar', 'delete', 'fa-trash-o', 'intable', '1');
INSERT INTO `control` VALUES ('116', '95', '2', '100405', 'Modelos', 'vehiculos.modelos', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('117', '116', '3', '10040501', 'Listar', 'list', 'fa-list', 'inbarra', '1');
INSERT INTO `control` VALUES ('118', '116', '3', '10040502', 'Nuevo', 'new', 'fa-plus', 'inbarra', '1');
INSERT INTO `control` VALUES ('119', '116', '3', '10040503', 'Editar', 'edit', 'fa-pencil-square-o', 'intable', '1');
INSERT INTO `control` VALUES ('120', '116', '3', '10040504', 'Eliminar', 'delete', 'fa-trash-o', 'intable', '1');
INSERT INTO `control` VALUES ('121', '86', '3', '20010104', 'Detalle', 'detail', 'fa-info', 'inbarra', '1');
INSERT INTO `control` VALUES ('122', '21', '2', '100310', 'Acuerdo de Pagos', 'registros.acuerdo-pagos', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('123', '122', '3', '10031001', 'Listar', 'list', 'fa-list', 'inbarra', '1');
INSERT INTO `control` VALUES ('124', '122', '3', '10031002', 'Nuevo', 'new', 'fa-plus', 'inbarra', '1');
INSERT INTO `control` VALUES ('125', '122', '3', '10031003', 'Editar', 'edit', 'fa-pencil-square-o', 'intable', '1');
INSERT INTO `control` VALUES ('126', '122', '3', '10031004', 'Eliminar', 'delete', 'fa-trash-o', 'intable', '1');
INSERT INTO `control` VALUES ('127', '86', '3', '20010105', 'Aprobar', 'aprobar', 'fa-check-square', '', '1');
INSERT INTO `control` VALUES ('128', '86', '3', '20010106', 'Cancelar', 'cancelar', 'fa-ban', '', '1');
INSERT INTO `control` VALUES ('129', '86', '3', '20010107', 'Eliminar', 'delete', 'fa-trash-o', 'intable', '0');
INSERT INTO `control` VALUES ('130', '85', '2', '200102', 'Cred Menor', 'operaciones.cred-menor', 'sub menu', '', '1');
INSERT INTO `control` VALUES ('131', '130', '3', '20010201', 'Listar', 'list', 'fa-list', '', '1');
INSERT INTO `control` VALUES ('132', '130', '3', '20010202', 'Nuevo', 'new', 'fa-plus', '', '1');
INSERT INTO `control` VALUES ('133', '130', '3', '20010203', 'Editar', 'edit', 'fa-pencil-square-o', '', '1');
INSERT INTO `control` VALUES ('134', '130', '3', '20010204', 'Detalle', 'detail', 'fa-info', '', '1');
INSERT INTO `control` VALUES ('135', '130', '3', '20010205', 'Aprobar', 'aprobar', 'fa-check-square', '', '1');
INSERT INTO `control` VALUES ('136', '130', '3', '20010206', 'Cancelar', 'cancelar', 'fa-ban', '', '1');

-- ----------------------------
-- Table structure for cuota
-- ----------------------------
DROP TABLE IF EXISTS `cuota`;
CREATE TABLE `cuota` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `prestamo_id` bigint(20) unsigned NOT NULL,
  `numero` int(10) unsigned NOT NULL,
  `saldo_capital` decimal(10,2) NOT NULL,
  `amortizacion` decimal(10,2) NOT NULL,
  `interes` decimal(10,2) NOT NULL,
  `cuota` decimal(10,2) NOT NULL,
  `cuota_parte` decimal(10,2) NOT NULL DEFAULT '0.00',
  `fecha_pago` date NOT NULL,
  `fecha_parte_1` date DEFAULT NULL,
  `fecha_parte_2` date DEFAULT NULL,
  `fecha_parte_3` date DEFAULT NULL,
  `fecha_parte_4` date DEFAULT NULL,
  `user_id_cobro` int(10) unsigned NOT NULL DEFAULT '0',
  `estado` smallint(5) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cuota_prestamo_id_foreign` (`prestamo_id`),
  CONSTRAINT `cuota_prestamo_id_foreign` FOREIGN KEY (`prestamo_id`) REFERENCES `prestamo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of cuota
-- ----------------------------
INSERT INTO `cuota` VALUES ('1', '1', '1', '7168.85', '74.80', '358.44', '433.25', '108.31', '2016-06-27', '2016-06-06', '2016-06-13', '2016-06-20', '2016-06-27', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('2', '1', '2', '7094.05', '78.54', '354.70', '433.25', '108.31', '2016-07-27', '2016-07-06', '2016-07-13', '2016-07-20', '2016-07-27', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('3', '1', '3', '7015.51', '82.47', '350.78', '433.25', '108.31', '2016-08-26', '2016-08-05', '2016-08-12', '2016-08-19', '2016-08-26', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('4', '1', '4', '6933.04', '86.59', '346.65', '433.25', '108.31', '2016-09-25', '2016-09-04', '2016-09-11', '2016-09-18', '2016-09-25', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('5', '1', '5', '6846.44', '90.92', '342.32', '433.25', '108.31', '2016-10-25', '2016-10-04', '2016-10-11', '2016-10-18', '2016-10-25', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('6', '1', '6', '6755.52', '95.47', '337.78', '433.25', '108.31', '2016-11-24', '2016-11-03', '2016-11-10', '2016-11-17', '2016-11-24', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('7', '1', '7', '6660.05', '100.24', '333.00', '433.25', '108.31', '2016-12-24', '2016-12-03', '2016-12-10', '2016-12-17', '2016-12-24', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('8', '1', '8', '6559.81', '105.26', '327.99', '433.25', '108.31', '2017-01-23', '2017-01-02', '2017-01-09', '2017-01-16', '2017-01-23', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('9', '1', '9', '6454.55', '110.52', '322.73', '433.25', '108.31', '2017-02-22', '2017-02-01', '2017-02-08', '2017-02-15', '2017-02-22', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('10', '1', '10', '6344.03', '116.04', '317.20', '433.25', '108.31', '2017-03-24', '2017-03-03', '2017-03-10', '2017-03-17', '2017-03-24', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('11', '1', '11', '6227.99', '121.85', '311.40', '433.25', '108.31', '2017-04-23', '2017-04-02', '2017-04-09', '2017-04-16', '2017-04-23', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('12', '1', '12', '6106.14', '127.94', '305.31', '433.25', '108.31', '2017-05-23', '2017-05-02', '2017-05-09', '2017-05-16', '2017-05-23', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('13', '1', '13', '5978.20', '134.34', '298.91', '433.25', '108.31', '2017-06-22', '2017-06-01', '2017-06-08', '2017-06-15', '2017-06-22', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('14', '1', '14', '5843.87', '141.05', '292.19', '433.25', '108.31', '2017-07-22', '2017-07-01', '2017-07-08', '2017-07-15', '2017-07-22', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('15', '1', '15', '5702.81', '148.10', '285.14', '433.25', '108.31', '2017-08-21', '2017-07-31', '2017-08-07', '2017-08-14', '2017-08-21', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('16', '1', '16', '5554.71', '155.51', '277.74', '433.25', '108.31', '2017-09-20', '2017-08-30', '2017-09-06', '2017-09-13', '2017-09-20', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('17', '1', '17', '5399.20', '163.29', '269.96', '433.25', '108.31', '2017-10-20', '2017-09-29', '2017-10-06', '2017-10-13', '2017-10-20', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('18', '1', '18', '5235.91', '171.45', '261.80', '433.25', '108.31', '2017-11-19', '2017-10-29', '2017-11-05', '2017-11-12', '2017-11-19', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('19', '1', '19', '5064.46', '180.02', '253.22', '433.25', '108.31', '2017-12-19', '2017-11-28', '2017-12-05', '2017-12-12', '2017-12-19', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('20', '1', '20', '4884.44', '189.02', '244.22', '433.25', '108.31', '2018-01-18', '2017-12-28', '2018-01-04', '2018-01-11', '2018-01-18', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('21', '1', '21', '4695.42', '198.47', '234.77', '433.25', '108.31', '2018-02-17', '2018-01-27', '2018-02-03', '2018-02-10', '2018-02-17', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('22', '1', '22', '4496.94', '208.40', '224.85', '433.25', '108.31', '2018-03-19', '2018-02-26', '2018-03-05', '2018-03-12', '2018-03-19', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('23', '1', '23', '4288.54', '218.82', '214.43', '433.25', '108.31', '2018-04-18', '2018-03-28', '2018-04-04', '2018-04-11', '2018-04-18', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('24', '1', '24', '4069.73', '229.76', '203.49', '433.25', '108.31', '2018-05-18', '2018-04-27', '2018-05-04', '2018-05-11', '2018-05-18', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('25', '1', '25', '3839.97', '241.25', '192.00', '433.25', '108.31', '2018-06-17', '2018-05-27', '2018-06-03', '2018-06-10', '2018-06-17', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('26', '1', '26', '3598.72', '253.31', '179.94', '433.25', '108.31', '2018-07-17', '2018-06-26', '2018-07-03', '2018-07-10', '2018-07-17', '0', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `cuota` VALUES ('27', '1', '27', '3345.41', '265.98', '167.27', '433.25', '108.31', '2018-08-16', '2018-07-26', '2018-08-02', '2018-08-09', '2018-08-16', '0', '1', '2016-05-24 10:12:42', '2016-05-24 10:12:42');
INSERT INTO `cuota` VALUES ('28', '1', '28', '3079.43', '279.27', '153.97', '433.25', '108.31', '2018-09-15', '2018-08-25', '2018-09-01', '2018-09-08', '2018-09-15', '0', '1', '2016-05-24 10:12:42', '2016-05-24 10:12:42');
INSERT INTO `cuota` VALUES ('29', '1', '29', '2800.16', '293.24', '140.01', '433.25', '108.31', '2018-10-15', '2018-09-24', '2018-10-01', '2018-10-08', '2018-10-15', '0', '1', '2016-05-24 10:12:42', '2016-05-24 10:12:42');
INSERT INTO `cuota` VALUES ('30', '1', '30', '2506.92', '307.90', '125.35', '433.25', '108.31', '2018-11-14', '2018-10-24', '2018-10-31', '2018-11-07', '2018-11-14', '0', '1', '2016-05-24 10:12:42', '2016-05-24 10:12:42');
INSERT INTO `cuota` VALUES ('31', '1', '31', '2199.02', '323.29', '109.95', '433.25', '108.31', '2018-12-14', '2018-11-23', '2018-11-30', '2018-12-07', '2018-12-14', '0', '1', '2016-05-24 10:12:42', '2016-05-24 10:12:42');
INSERT INTO `cuota` VALUES ('32', '1', '32', '1875.73', '339.46', '93.79', '433.25', '108.31', '2019-01-13', '2018-12-23', '2018-12-30', '2019-01-06', '2019-01-13', '0', '1', '2016-05-24 10:12:42', '2016-05-24 10:12:42');
INSERT INTO `cuota` VALUES ('33', '1', '33', '1536.27', '356.43', '76.81', '433.25', '108.31', '2019-02-12', '2019-01-22', '2019-01-29', '2019-02-05', '2019-02-12', '0', '1', '2016-05-24 10:12:42', '2016-05-24 10:12:42');
INSERT INTO `cuota` VALUES ('34', '1', '34', '1179.84', '374.25', '58.99', '433.25', '108.31', '2019-03-14', '2019-02-21', '2019-02-28', '2019-03-07', '2019-03-14', '0', '1', '2016-05-24 10:12:42', '2016-05-24 10:12:42');
INSERT INTO `cuota` VALUES ('35', '1', '35', '805.58', '392.97', '40.28', '433.25', '108.31', '2019-04-13', '2019-03-23', '2019-03-30', '2019-04-06', '2019-04-13', '0', '1', '2016-05-24 10:12:42', '2016-05-24 10:12:42');
INSERT INTO `cuota` VALUES ('36', '1', '36', '412.61', '412.61', '20.63', '433.25', '108.31', '2019-05-13', '2019-04-22', '2019-04-29', '2019-05-06', '2019-05-13', '0', '1', '2016-05-24 10:12:42', '2016-05-24 10:12:42');

-- ----------------------------
-- Table structure for empleado
-- ----------------------------
DROP TABLE IF EXISTS `empleado`;
CREATE TABLE `empleado` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `persona_id` int(10) unsigned NOT NULL,
  `persona_id_padre` int(10) unsigned NOT NULL,
  `estado` int(10) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `empleado_persona_id_foreign` (`persona_id`),
  KEY `empleado_persona_id_padre_index` (`persona_id_padre`),
  CONSTRAINT `empleado_persona_id_foreign` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of empleado
-- ----------------------------
INSERT INTO `empleado` VALUES ('1', '26', '3', '1', '2016-02-27 12:42:09', '2016-02-27 12:44:23');
INSERT INTO `empleado` VALUES ('2', '6', '3', '1', '2016-02-27 12:51:25', '2016-02-27 12:51:25');
INSERT INTO `empleado` VALUES ('3', '7', '3', '1', '2016-02-27 12:52:06', '2016-02-27 12:52:06');
INSERT INTO `empleado` VALUES ('4', '9', '3', '1', '2016-02-27 12:52:22', '2016-02-27 12:52:22');
INSERT INTO `empleado` VALUES ('5', '5', '3', '1', '2016-02-27 12:52:35', '2016-02-27 12:52:35');
INSERT INTO `empleado` VALUES ('6', '10', '3', '1', '2016-02-27 12:52:51', '2016-02-27 12:52:51');
INSERT INTO `empleado` VALUES ('7', '8', '3', '1', '2016-02-27 12:53:04', '2016-02-27 12:53:04');

-- ----------------------------
-- Table structure for licencia
-- ----------------------------
DROP TABLE IF EXISTS `licencia`;
CREATE TABLE `licencia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `estado` smallint(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of licencia
-- ----------------------------
INSERT INTO `licencia` VALUES ('1', 'SOAT', 'Seguro Obligatorio de Accidentes de Tránsito', '1');
INSERT INTO `licencia` VALUES ('2', 'STR', 'Seguro Todo Riesgo', '1');
INSERT INTO `licencia` VALUES ('3', 'GPS', 'Global Position System', '1');
INSERT INTO `licencia` VALUES ('4', 'GAS', 'Conversión', '1');

-- ----------------------------
-- Table structure for marca
-- ----------------------------
DROP TABLE IF EXISTS `marca`;
CREATE TABLE `marca` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `estado` smallint(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of marca
-- ----------------------------
INSERT INTO `marca` VALUES ('1', 'NISSAN', 'Maquinarias S.A.', '1');
INSERT INTO `marca` VALUES ('2', 'TOYOTA', 'NOR AUTOS S.A.', '1');
INSERT INTO `marca` VALUES ('3', 'SUZUKI', 'AUTOMOTORES PAKATNAMU S.A.', '1');
INSERT INTO `marca` VALUES ('4', 'HYUNDAI', 'SOCIEDAD DE AUTOMOTORES INKA SAC', '1');
INSERT INTO `marca` VALUES ('5', 'CHEVROLET', 'NEO MOTORS SA', '1');
INSERT INTO `marca` VALUES ('6', 'KIA', 'INTERAMERICANA NORTE SAC', '1');
INSERT INTO `marca` VALUES ('7', 'MITSUBISHI', 'INTERAMERICANA NORTE SAC', '1');
INSERT INTO `marca` VALUES ('8', 'VOLSKWAGEN', 'INTERAMERICANA NORTE SAC', '1');
INSERT INTO `marca` VALUES ('9', 'MAZDA', 'AUTOMOTORES PAKATNAMU SA', '1');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('2015_12_26_154553_create_persona_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_154556_create_tipo_control_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_154558_create_control_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_154601_create_rol_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_154604_create_rol_control_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_154607_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_154609_create_accesos_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_154714_create_transaccion_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_154717_create_bitacora_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_154720_create_tipo_relacion_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_154722_create_per_relacion_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_154725_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_154729_create_notificacion_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_170746_create_per_natural_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_170749_create_per_mail_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_170753_create_tipo_documento_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_170756_create_per_documento_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_170759_create_rubro_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_170802_create_per_juridica_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_170804_create_tipo_telefono_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_170808_create_per_telefono_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_170810_create_tipo_direccion_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_170813_create_per_direccion_table', '1');
INSERT INTO `migrations` VALUES ('2015_12_26_170901_create_ubigeo_table', '1');
INSERT INTO `migrations` VALUES ('2016_01_05_174626_create_per_imagen_table', '1');
INSERT INTO `migrations` VALUES ('2016_02_18_093514_create_areas_table', '2');
INSERT INTO `migrations` VALUES ('2016_02_20_174644_create_cargo_table', '3');
INSERT INTO `migrations` VALUES ('2016_02_21_175451_create_empleado_table', '4');
INSERT INTO `migrations` VALUES ('2016_02_23_150304_create_cargo_empleado_table', '4');
INSERT INTO `migrations` VALUES ('2016_02_26_133404_create_cliente_table', '5');
INSERT INTO `migrations` VALUES ('2016_02_27_133231_create_agente_comercial_table', '5');
INSERT INTO `migrations` VALUES ('2016_03_01_200106_create_tipo_prestamo_table', '6');
INSERT INTO `migrations` VALUES ('2016_03_01_200547_create_tipo_moneda_table', '6');
INSERT INTO `migrations` VALUES ('2016_03_02_101702_create_tipo_periodo_table', '6');
INSERT INTO `migrations` VALUES ('2016_03_02_105823_create_tipo_garantia_table', '6');
INSERT INTO `migrations` VALUES ('2016_03_03_110024_create_licencia_table', '8');
INSERT INTO `migrations` VALUES ('2016_03_03_110141_create_per_licencia_table', '8');
INSERT INTO `migrations` VALUES ('2016_03_07_121152_create_tipo_web_table', '8');
INSERT INTO `migrations` VALUES ('2016_03_07_121306_create_per_web_table', '8');
INSERT INTO `migrations` VALUES ('2016_03_02_114744_create_tipo_pago_table', '9');
INSERT INTO `migrations` VALUES ('2016_03_18_111011_create_tipo_cambio_table', '10');
INSERT INTO `migrations` VALUES ('2016_03_22_102831_create_aval_table', '11');
INSERT INTO `migrations` VALUES ('2016_03_24_150807_create_clase_vehiculo_table', '12');
INSERT INTO `migrations` VALUES ('2016_03_24_160807_create_tipo_vehiculo_table', '12');
INSERT INTO `migrations` VALUES ('2016_03_24_160928_create_marca_table', '12');
INSERT INTO `migrations` VALUES ('2016_03_24_160940_create_modelo_table', '12');
INSERT INTO `migrations` VALUES ('2016_03_24_161044_create_vehiculo_table', '12');
INSERT INTO `migrations` VALUES ('2016_04_19_004648_create_acuerdo_pago_table', '13');
INSERT INTO `migrations` VALUES ('2016_05_13_124534_create_tipo_estado_table', '14');
INSERT INTO `migrations` VALUES ('2016_04_05_084856_create_prestamo_table', '15');
INSERT INTO `migrations` VALUES ('2016_04_06_182356_create_cuota_table', '15');
INSERT INTO `migrations` VALUES ('2016_05_13_124624_create_prestamo_estado_table', '15');

-- ----------------------------
-- Table structure for modelo
-- ----------------------------
DROP TABLE IF EXISTS `modelo`;
CREATE TABLE `modelo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `marca_id` int(10) unsigned NOT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `estado` smallint(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `modelo_marca_id_foreign` (`marca_id`),
  CONSTRAINT `modelo_marca_id_foreign` FOREIGN KEY (`marca_id`) REFERENCES `marca` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of modelo
-- ----------------------------
INSERT INTO `modelo` VALUES ('1', '3', 'ALTO K10', 'DERCO', '1');
INSERT INTO `modelo` VALUES ('2', '3', 'NEW ALTO 800', 'DERCO', '1');
INSERT INTO `modelo` VALUES ('3', '3', 'NEW CELERIO', 'DERCO', '1');
INSERT INTO `modelo` VALUES ('4', '3', 'SWIFT', 'DERCO', '1');
INSERT INTO `modelo` VALUES ('5', '9', 'MAZDA 2', 'DERCO', '1');
INSERT INTO `modelo` VALUES ('6', '4', 'EON', 'AUTOMOTORES INCA SAC', '1');
INSERT INTO `modelo` VALUES ('7', '4', 'GRAND i10', 'AUTOMOTORES INCA SAC', '1');
INSERT INTO `modelo` VALUES ('8', '6', 'PICANTO', 'INTERAMERICANA', '1');
INSERT INTO `modelo` VALUES ('9', '6', 'RIO', 'INTERAMERICANA', '1');
INSERT INTO `modelo` VALUES ('10', '5', 'SPARK', 'NEOMOTORS', '1');
INSERT INTO `modelo` VALUES ('11', '5', 'SPARK LITE', 'NEOMOTORS', '1');
INSERT INTO `modelo` VALUES ('12', '5', 'SPARK GT', 'NEOMOTORS', '1');
INSERT INTO `modelo` VALUES ('13', '5', 'SAIL', 'NEOMOTORS', '1');
INSERT INTO `modelo` VALUES ('14', '7', 'MIRAGE', 'INTERAMERICANA', '1');
INSERT INTO `modelo` VALUES ('15', '8', 'UP!', 'INTERAMERICANA', '1');

-- ----------------------------
-- Table structure for notificacion
-- ----------------------------
DROP TABLE IF EXISTS `notificacion`;
CREATE TABLE `notificacion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `destino` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `asunto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensaje` text COLLATE utf8_unicode_ci NOT NULL,
  `referencia` text COLLATE utf8_unicode_ci NOT NULL,
  `tipo` smallint(6) NOT NULL,
  `fecha_envio` timestamp NULL DEFAULT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notificacion_destino_index` (`destino`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of notificacion
-- ----------------------------
INSERT INTO `notificacion` VALUES ('1', '3', '981562855', 'Credenciales Usuario', 'Usuario: egalvez@prestamosdelnortechiclayo.com contraseña: tinie8las\\n mas informacion en prestamos del norte ', '', '1', null, '1', null, null);
INSERT INTO `notificacion` VALUES ('2', '4', '980713668', 'Credenciales Usuario', 'Usuario: lsanchezg@prestamosdelnortechiclayo.com contraseña: tanialoerestodo\\n mas informacion en prestamos del norte ', '', '1', null, '1', null, null);
INSERT INTO `notificacion` VALUES ('3', '5', '976882074', 'Credenciales Usuario', 'Usuario: rfernandezm@prestamosdelnortechiclayo.com contraseña: manzanares\\n mas informacion en prestamos del norte ', '', '1', null, '1', null, null);
INSERT INTO `notificacion` VALUES ('4', '6', '945027584', 'Credenciales Usuario', 'Usuario: jgonzales@prestamosdelnortechiclayo.com contraseña: 44801848\\n mas informacion en prestamos del norte ', '', '1', null, '1', null, null);
INSERT INTO `notificacion` VALUES ('5', '7', '945936439', 'Credenciales Usuario', 'Usuario: cmercado@prestamosdelnortechiclayo.com contraseña: cmercadol\\n mas informacion en prestamos del norte ', '', '1', null, '1', null, null);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for per_direccion
-- ----------------------------
DROP TABLE IF EXISTS `per_direccion`;
CREATE TABLE `per_direccion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `persona_id` int(10) unsigned NOT NULL,
  `tipo_direccion_id` int(10) unsigned DEFAULT NULL,
  `ubigeo_id` int(10) unsigned DEFAULT NULL,
  `direccion` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `referencia` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `per_direccion_persona_id_foreign` (`persona_id`),
  KEY `per_direccion_tipo_direccion_id_foreign` (`tipo_direccion_id`),
  CONSTRAINT `per_direccion_persona_id_foreign` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`),
  CONSTRAINT `per_direccion_tipo_direccion_id_foreign` FOREIGN KEY (`tipo_direccion_id`) REFERENCES `tipo_direccion` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of per_direccion
-- ----------------------------

-- ----------------------------
-- Table structure for per_documento
-- ----------------------------
DROP TABLE IF EXISTS `per_documento`;
CREATE TABLE `per_documento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `persona_id` int(10) unsigned NOT NULL,
  `tipo_documento_id` int(10) unsigned NOT NULL,
  `numero` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `caducidad` date DEFAULT NULL,
  `imagen` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `per_documento_persona_id_foreign` (`persona_id`),
  KEY `per_documento_tipo_documento_id_foreign` (`tipo_documento_id`),
  KEY `per_documento_numero_index` (`numero`),
  CONSTRAINT `per_documento_persona_id_foreign` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`),
  CONSTRAINT `per_documento_tipo_documento_id_foreign` FOREIGN KEY (`tipo_documento_id`) REFERENCES `tipo_documento` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of per_documento
-- ----------------------------
INSERT INTO `per_documento` VALUES ('1', '1', '2', '00000000000', '2016-02-16', '', '1');
INSERT INTO `per_documento` VALUES ('2', '2', '1', '4x3x8x8x', '2016-02-16', '', '1');
INSERT INTO `per_documento` VALUES ('3', '3', '2', '20487950484', '2016-02-16', '', '1');
INSERT INTO `per_documento` VALUES ('4', '4', '1', '123456789', '2016-02-16', '', '1');
INSERT INTO `per_documento` VALUES ('5', '5', '1', '44484240', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('6', '6', '1', '47137924', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('7', '7', '1', '45261610', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('8', '8', '1', '44801848', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('9', '9', '1', '44760801', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('10', '10', '1', '43904363', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('11', '11', '1', '41463302', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('12', '12', '1', '16442955', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('13', '13', '1', '16471857', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('14', '14', '1', '16471857', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('15', '15', '1', '72690041', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('16', '16', '1', '45478046', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('17', '17', '1', '42185903', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('18', '18', '1', '17589893', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('19', '19', '1', '19329320', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('20', '20', '1', '41189673', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('21', '21', '1', '43740530', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('22', '22', '1', '42105109', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('23', '23', '1', '46865187', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('24', '24', '1', '80641345', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('25', '25', '1', '44173634', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('26', '26', '1', '40389316', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('27', '27', '1', '41837371', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('28', '28', '1', '16751284', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('29', '29', '1', '16751284', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('30', '30', '1', '43693638', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('31', '31', '1', '47736034', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('32', '32', '1', '41216968', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('33', '33', '1', '42533888', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('34', '34', '1', '02688830', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('35', '35', '1', '43481640', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('36', '36', '1', '16690834', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('37', '37', '1', '17401131', '0000-00-00', '', '1');
INSERT INTO `per_documento` VALUES ('38', '38', '1', '46889370', null, '', '1');
INSERT INTO `per_documento` VALUES ('39', '39', '1', '43088476', null, '', '1');
INSERT INTO `per_documento` VALUES ('40', '40', '1', '16795792', null, '', '1');
INSERT INTO `per_documento` VALUES ('41', '41', '1', '16627317', null, '', '1');
INSERT INTO `per_documento` VALUES ('42', '42', '1', '42891028', null, '', '1');
INSERT INTO `per_documento` VALUES ('43', '43', '1', '45326961', null, '', '1');
INSERT INTO `per_documento` VALUES ('44', '44', '1', '46350680', null, '', '1');
INSERT INTO `per_documento` VALUES ('45', '45', '1', '16656642', null, '', '1');
INSERT INTO `per_documento` VALUES ('46', '46', '1', '44896127', null, '', '1');
INSERT INTO `per_documento` VALUES ('47', '47', '1', '16795672', null, '', '1');
INSERT INTO `per_documento` VALUES ('48', '48', '1', '48302449', null, '', '1');
INSERT INTO `per_documento` VALUES ('49', '49', '1', '44842925', null, '', '1');
INSERT INTO `per_documento` VALUES ('50', '50', '1', '43303587', null, '', '1');
INSERT INTO `per_documento` VALUES ('51', '51', '1', '44814398', null, '', '1');
INSERT INTO `per_documento` VALUES ('52', '52', '1', '41746595', null, '', '1');
INSERT INTO `per_documento` VALUES ('53', '53', '1', '16760569', null, '', '1');
INSERT INTO `per_documento` VALUES ('54', '54', '1', '16686078', null, '', '1');
INSERT INTO `per_documento` VALUES ('55', '55', '1', '42010670', null, '', '1');
INSERT INTO `per_documento` VALUES ('56', '56', '1', '16751054', null, '', '1');
INSERT INTO `per_documento` VALUES ('57', '57', '1', '73743480', null, '', '1');
INSERT INTO `per_documento` VALUES ('58', '58', '1', '80628313', null, '', '1');
INSERT INTO `per_documento` VALUES ('59', '59', '1', '16648850', null, '', '1');
INSERT INTO `per_documento` VALUES ('60', '60', '1', '46865796', null, '', '1');
INSERT INTO `per_documento` VALUES ('61', '61', '1', '16459657', null, '', '1');
INSERT INTO `per_documento` VALUES ('62', '62', '1', '41931445', null, '', '1');
INSERT INTO `per_documento` VALUES ('63', '63', '1', '44546423', null, '', '1');
INSERT INTO `per_documento` VALUES ('64', '64', '1', '46743139', null, '', '1');
INSERT INTO `per_documento` VALUES ('65', '65', '1', '46743139', null, '', '1');
INSERT INTO `per_documento` VALUES ('66', '66', '1', '16704199', null, '', '1');
INSERT INTO `per_documento` VALUES ('67', '67', '1', '16692240', null, '', '1');
INSERT INTO `per_documento` VALUES ('68', '68', '1', '16542764', null, '', '1');
INSERT INTO `per_documento` VALUES ('69', '69', '1', '44721342', null, '', '1');

-- ----------------------------
-- Table structure for per_imagen
-- ----------------------------
DROP TABLE IF EXISTS `per_imagen`;
CREATE TABLE `per_imagen` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `persona_id` int(10) unsigned NOT NULL,
  `url` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` smallint(5) unsigned NOT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `per_imagen_persona_id_foreign` (`persona_id`),
  CONSTRAINT `per_imagen_persona_id_foreign` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of per_imagen
-- ----------------------------
INSERT INTO `per_imagen` VALUES ('1', '2', 'img_avatars/armandoaepp.png', '1', '1');
INSERT INTO `per_imagen` VALUES ('2', '4', 'img_avatars/avatar_admin.png', '1', '1');

-- ----------------------------
-- Table structure for per_juridica
-- ----------------------------
DROP TABLE IF EXISTS `per_juridica`;
CREATE TABLE `per_juridica` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `persona_id` int(10) unsigned NOT NULL,
  `rubro_id` int(10) unsigned NOT NULL,
  `ruc` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `razon_social` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_comercial` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `per_juridica_persona_id_foreign` (`persona_id`),
  KEY `per_juridica_ruc_index` (`ruc`),
  CONSTRAINT `per_juridica_persona_id_foreign` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of per_juridica
-- ----------------------------
INSERT INTO `per_juridica` VALUES ('1', '1', '0', '00000000000', 'Sudo - Planeatec', null, '1');
INSERT INTO `per_juridica` VALUES ('2', '3', '0', '20487950484', 'Prestamos del Norte', null, '1');

-- ----------------------------
-- Table structure for per_licencia
-- ----------------------------
DROP TABLE IF EXISTS `per_licencia`;
CREATE TABLE `per_licencia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `persona_id` int(10) unsigned NOT NULL,
  `licencia_id` int(10) unsigned NOT NULL,
  `numero_lic` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `clase_categoria` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `fecha_emision` date DEFAULT NULL,
  `fecha_caducidad` date NOT NULL,
  `estado` smallint(5) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `per_licencia_persona_id_foreign` (`persona_id`),
  KEY `per_licencia_licencia_id_foreign` (`licencia_id`),
  CONSTRAINT `per_licencia_licencia_id_foreign` FOREIGN KEY (`licencia_id`) REFERENCES `licencia` (`id`),
  CONSTRAINT `per_licencia_persona_id_foreign` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of per_licencia
-- ----------------------------

-- ----------------------------
-- Table structure for per_mail
-- ----------------------------
DROP TABLE IF EXISTS `per_mail`;
CREATE TABLE `per_mail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `persona_id` int(10) unsigned NOT NULL,
  `mail` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `item` smallint(6) NOT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `per_mail_persona_id_foreign` (`persona_id`),
  KEY `per_mail_mail_index` (`mail`),
  CONSTRAINT `per_mail_persona_id_foreign` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of per_mail
-- ----------------------------
INSERT INTO `per_mail` VALUES ('1', '2', 'armandoaepp@gmail.com', '1', '1');
INSERT INTO `per_mail` VALUES ('2', '4', 'admin@admin.com', '1', '1');
INSERT INTO `per_mail` VALUES ('3', '5', 'egalvez@prestamosdelnortechiclayo.com', '1', '1');
INSERT INTO `per_mail` VALUES ('4', '6', 'rfernandezm@prestamosdelnortechiclayo.com', '1', '1');
INSERT INTO `per_mail` VALUES ('5', '7', 'lsanchezg@prestamosdelnortechiclayo.com', '1', '1');
INSERT INTO `per_mail` VALUES ('6', '8', 'jgonzales@prestamosdelnortechiclayo.com', '1', '1');
INSERT INTO `per_mail` VALUES ('7', '9', 'mvargas@prestamosdelnortechiclayo.com', '1', '1');
INSERT INTO `per_mail` VALUES ('8', '10', 'cmercado@prestamosdelnortechiclayo.com', '1', '1');
INSERT INTO `per_mail` VALUES ('9', '22', 'ohbabyperu@hotmail.es', '1', '1');
INSERT INTO `per_mail` VALUES ('10', '26', 'rtigre@corptigersoft.com.pe', '1', '1');
INSERT INTO `per_mail` VALUES ('11', '36', 'nikol_193_2@hotmail.com', '1', '1');
INSERT INTO `per_mail` VALUES ('12', '38', 'lorenaisabel06@hotmail.com', '1', '1');
INSERT INTO `per_mail` VALUES ('13', '41', 'orlandogarayfarro@hotmail.com', '1', '1');
INSERT INTO `per_mail` VALUES ('14', '42', 'ariadna:24:54@hotmail.com', '1', '1');
INSERT INTO `per_mail` VALUES ('15', '44', 'dilaura_chg@hotmail.com', '1', '1');
INSERT INTO `per_mail` VALUES ('16', '45', 'mgarciavargas@outlook.com', '1', '1');
INSERT INTO `per_mail` VALUES ('17', '69', 'ahstt@hotmail.com', '1', '1');

-- ----------------------------
-- Table structure for per_natural
-- ----------------------------
DROP TABLE IF EXISTS `per_natural`;
CREATE TABLE `per_natural` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `persona_id` int(10) unsigned NOT NULL,
  `dni` char(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellidos` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `nombres` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `sexo` smallint(6) NOT NULL DEFAULT '1',
  `estado_civil` smallint(6) NOT NULL DEFAULT '0',
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `per_natural_persona_id_foreign` (`persona_id`),
  KEY `per_natural_dni_index` (`dni`),
  CONSTRAINT `per_natural_persona_id_foreign` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of per_natural
-- ----------------------------
INSERT INTO `per_natural` VALUES ('1', '2', '4x3x8x8x', '@sudo', '@armandoaepp', '1', '1', '1');
INSERT INTO `per_natural` VALUES ('2', '4', '12345678', 'Super Admin ', 'Adminstrador', '1', '1', '1');
INSERT INTO `per_natural` VALUES ('3', '5', '44484240', 'Gálvez Rojas', 'Herbert Engel', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('4', '6', '47137924', 'Fernandez Manzanares', 'Rosina Del Pilar', '2', '0', '1');
INSERT INTO `per_natural` VALUES ('5', '7', '45261610', 'Sanchez Garay', 'Luis Alberto', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('6', '8', '44801848', 'Gonzales Kodzman', 'Jessica Jackeline', '2', '0', '1');
INSERT INTO `per_natural` VALUES ('7', '9', '44760801', 'Victor Manuel', 'Vargas Torres', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('8', '10', '43904363', 'Cristhian Lucio', 'Mercado Lara', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('9', '11', '41463302', 'Cerna Rivera', 'Segundo Apolinar', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('10', '12', '16442955', 'Bances Serquen', 'Luis Alberto', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('11', '13', '16471857', 'Muro Rojas', 'Aurora', '2', '0', '1');
INSERT INTO `per_natural` VALUES ('12', '14', '16471857', 'Muro Rojas', 'Aurora', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('13', '15', '72690041', 'Quispe Santillan', 'Jose Felix', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('14', '16', '45478046', 'Villalobos Barboza', 'Evert', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('15', '17', '42185903', 'Espinal Nicolas', 'Luis Alberto', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('16', '18', '17589893', 'Vela Yamunaque', 'Juan Eduardo', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('17', '19', '19329320', 'De La Cruz Valle', 'Marcos Hugo', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('18', '20', '41189673', 'Sandoval Neyra', 'Oscar', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('19', '21', '43740530', 'Toro Chavez', 'Nancy Yanet', '2', '0', '1');
INSERT INTO `per_natural` VALUES ('20', '22', '42105109', 'Romero Chumbe', 'Carlos Enrique', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('21', '23', '46865187', 'Davila Vasquez', 'Anthony Jean Pierre', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('22', '24', '80641345', 'Querevalu Calderon', 'Josue Jimmy', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('23', '25', '44173634', 'Forero Peña', 'Sarita Janett', '2', '0', '1');
INSERT INTO `per_natural` VALUES ('24', '26', '40389316', 'TIGRE SIPION', 'ROLANDO KLITHO', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('25', '27', '41837371', 'Rodas Nuñez', 'Gabriela', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('26', '28', '16751284', 'Rivera Cordova', 'Reynaldo', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('27', '29', '16751284', 'Rivera Cordova', 'Reynaldo', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('28', '30', '43693638', 'Alvitres Llanos', 'Julio Albert', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('29', '31', '47736034', 'Perleche Martinez', 'Jorge Antonio', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('30', '32', '41216968', 'Calla Cardenas', 'Fernando Alonso', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('31', '33', '42533888', 'PLENGE COTTESS', 'Carlos Rafael', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('32', '34', '02688830', 'Purizaca Rivas', 'Oscar Martin', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('33', '35', '43481640', 'VELASQUEZ GALVEZ', 'Yannlui Eduardo', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('34', '36', '16690834', 'DE LA FUENTE SANCHEZ', 'Monica Magali', '2', '0', '1');
INSERT INTO `per_natural` VALUES ('35', '37', '17401131', 'CABRERA CORNETERO', 'Marco Adalberto', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('36', '38', '46889370', 'LUGO SANTA CRUZ', 'Lorena Isabel', '2', '0', '1');
INSERT INTO `per_natural` VALUES ('37', '39', '43088476', 'BECERRA HUERTAS', 'Victor Junior', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('38', '40', '16795792', 'MAYURI ALVARADO', 'Dora Elizabeth', '2', '0', '1');
INSERT INTO `per_natural` VALUES ('39', '41', '16627317', 'GARAY FARRO', 'Orlando', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('40', '42', '42891028', 'VALDIVIESO FARFAN', 'Billy Jonathan', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('41', '43', '45326961', 'SALAZAR BAUTISTA', 'Juan José Gabriel', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('42', '44', '46350680', 'CHANAMÉ GONZALES', 'Dilaura Yesenia', '2', '0', '1');
INSERT INTO `per_natural` VALUES ('43', '45', '16656642', 'GARCIA UCHOFEN', 'Marco Antonio', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('44', '46', '44896127', 'Oblitas Alamo', 'Elmer Daniel', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('45', '47', '16795672', 'Santa Maria Calle', 'Jaime', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('46', '48', '48302449', 'Hubert Esmit', 'Vega Alarcon', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('47', '49', '44842925', 'Saavedra Montenegro', 'Miluska Guisella', '2', '0', '1');
INSERT INTO `per_natural` VALUES ('48', '50', '43303587', 'Huiman Ruiz', 'Eber', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('49', '51', '44814398', 'Purihuaman Garcia', 'Juan Carlos', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('50', '52', '41746595', 'Campos zamora', 'Luis Alberto', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('51', '53', '16760569', 'Jara Anton', 'Marco Alfredo', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('52', '54', '16686078', 'Maria del Rosario', 'Niño Medianero', '2', '0', '1');
INSERT INTO `per_natural` VALUES ('53', '55', '42010670', 'Salazar Romero', 'Oscar Eduardo', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('54', '56', '16751054', 'Varillas Velasquez', 'Carlos Alberto', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('55', '57', '73743480', 'Mera Vasquez', 'Norbil Anibal', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('56', '58', '80628313', 'Guerava Campos', 'Aladino', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('57', '59', '16648850', 'Enriquez Colchado', 'Doris Nelly', '2', '0', '1');
INSERT INTO `per_natural` VALUES ('58', '60', '46865796', 'Arboleda Calle', 'Dubert Haward', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('59', '61', '16459657', 'Monteza Aguilar', 'Eleuterio', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('60', '62', '41931445', 'Morales Coveñas', 'Jaimen', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('61', '63', '44546423', 'Becerra Juarez', 'Marlon Jonathan', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('62', '64', '46743139', 'Saldaña Rivera', 'Obed Amos', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('63', '65', '46743139', 'Saldaña Rivera', 'Obed Amos', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('64', '66', '16704199', 'Hernandez Jara', 'Orlando Javier', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('65', '67', '16692240', 'Reyes De La Cruz', 'Santos', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('66', '68', '16542764', 'Fernandez Bravo', 'Victor', '1', '0', '1');
INSERT INTO `per_natural` VALUES ('67', '69', '44721342', 'Briones Zavala', 'Jorge', '1', '0', '1');

-- ----------------------------
-- Table structure for per_relacion
-- ----------------------------
DROP TABLE IF EXISTS `per_relacion`;
CREATE TABLE `per_relacion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `persona_id` int(10) unsigned NOT NULL,
  `tipo_relacion_id` int(10) unsigned NOT NULL,
  `persona_id_padre` int(10) unsigned NOT NULL,
  `referencia` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `per_relacion_persona_id_foreign` (`persona_id`),
  KEY `per_relacion_tipo_relacion_id_foreign` (`tipo_relacion_id`),
  KEY `per_relacion_persona_id_padre_index` (`persona_id_padre`),
  CONSTRAINT `per_relacion_persona_id_foreign` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`),
  CONSTRAINT `per_relacion_tipo_relacion_id_foreign` FOREIGN KEY (`tipo_relacion_id`) REFERENCES `tipo_relacion` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of per_relacion
-- ----------------------------
INSERT INTO `per_relacion` VALUES ('1', '2', '1', '1', 'SUDO', '1', '2016-02-16 13:02:45', null);
INSERT INTO `per_relacion` VALUES ('2', '3', '1', '1', 'Usuario', '1', '2016-02-16 13:02:45', null);
INSERT INTO `per_relacion` VALUES ('3', '4', '1', '3', 'Usuario', '1', '2016-02-16 13:02:45', null);
INSERT INTO `per_relacion` VALUES ('4', '5', '2', '3', 'persona - empresa', '1', '2016-02-16 16:02:26', null);
INSERT INTO `per_relacion` VALUES ('5', '5', '1', '3', ' Persona - Usuario', '1', '2016-02-16 16:02:59', null);
INSERT INTO `per_relacion` VALUES ('6', '6', '2', '3', 'persona - empresa', '1', '2016-02-16 17:02:37', null);
INSERT INTO `per_relacion` VALUES ('7', '7', '2', '3', 'persona - empresa', '1', '2016-02-16 17:02:09', null);
INSERT INTO `per_relacion` VALUES ('8', '7', '1', '3', ' Persona - Usuario', '1', '2016-02-16 17:02:39', null);
INSERT INTO `per_relacion` VALUES ('9', '6', '1', '3', ' Persona - Usuario', '1', '2016-02-16 17:02:19', null);
INSERT INTO `per_relacion` VALUES ('10', '8', '2', '3', 'persona - empresa', '1', '2016-02-16 17:02:34', null);
INSERT INTO `per_relacion` VALUES ('11', '8', '1', '3', ' Persona - Usuario', '1', '2016-02-16 17:02:03', null);
INSERT INTO `per_relacion` VALUES ('12', '9', '2', '3', 'persona - empresa', '1', '2016-02-16 17:02:42', null);
INSERT INTO `per_relacion` VALUES ('13', '10', '2', '3', 'persona - empresa', '1', '2016-02-16 17:02:49', null);
INSERT INTO `per_relacion` VALUES ('14', '11', '2', '3', 'persona - empresa', '1', '2016-02-17 11:02:01', null);
INSERT INTO `per_relacion` VALUES ('15', '12', '2', '3', 'persona - empresa', '1', '2016-02-17 11:02:16', null);
INSERT INTO `per_relacion` VALUES ('16', '13', '2', '3', 'persona - empresa', '1', '2016-02-17 11:02:47', null);
INSERT INTO `per_relacion` VALUES ('17', '14', '2', '3', 'persona - empresa', '1', '2016-02-17 11:02:47', null);
INSERT INTO `per_relacion` VALUES ('18', '15', '2', '3', 'persona - empresa', '1', '2016-02-17 17:02:27', null);
INSERT INTO `per_relacion` VALUES ('19', '16', '2', '3', 'persona - empresa', '1', '2016-02-17 17:02:55', null);
INSERT INTO `per_relacion` VALUES ('20', '17', '2', '3', 'persona - empresa', '1', '2016-02-17 18:02:52', null);
INSERT INTO `per_relacion` VALUES ('21', '18', '2', '3', 'persona - empresa', '1', '2016-02-17 18:02:25', null);
INSERT INTO `per_relacion` VALUES ('22', '19', '2', '3', 'persona - empresa', '1', '2016-02-17 18:02:02', null);
INSERT INTO `per_relacion` VALUES ('23', '20', '2', '3', 'persona - empresa', '1', '2016-02-17 18:02:38', null);
INSERT INTO `per_relacion` VALUES ('24', '21', '2', '3', 'persona - empresa', '1', '2016-02-17 18:02:59', null);
INSERT INTO `per_relacion` VALUES ('25', '10', '1', '3', ' Persona - Usuario', '1', '2016-02-18 09:02:55', null);
INSERT INTO `per_relacion` VALUES ('26', '22', '2', '3', 'persona - empresa', '1', '2016-02-18 09:02:22', null);
INSERT INTO `per_relacion` VALUES ('27', '23', '2', '3', 'persona - empresa', '1', '2016-02-18 09:02:17', null);
INSERT INTO `per_relacion` VALUES ('28', '24', '2', '3', 'persona - empresa', '1', '2016-02-18 10:02:35', null);
INSERT INTO `per_relacion` VALUES ('29', '25', '2', '3', 'persona - empresa', '1', '2016-02-18 10:02:16', null);
INSERT INTO `per_relacion` VALUES ('30', '26', '2', '3', 'persona - empresa', '1', '2016-02-18 10:02:31', null);
INSERT INTO `per_relacion` VALUES ('31', '27', '2', '3', 'persona - empresa', '1', '2016-02-18 11:02:25', null);
INSERT INTO `per_relacion` VALUES ('32', '28', '2', '3', 'persona - empresa', '1', '2016-02-18 11:02:02', null);
INSERT INTO `per_relacion` VALUES ('33', '29', '2', '3', 'persona - empresa', '1', '2016-02-18 11:02:03', null);
INSERT INTO `per_relacion` VALUES ('34', '30', '2', '3', 'persona - empresa', '1', '2016-02-18 11:02:29', null);
INSERT INTO `per_relacion` VALUES ('35', '31', '2', '3', 'persona - empresa', '1', '2016-02-18 11:02:37', null);
INSERT INTO `per_relacion` VALUES ('36', '32', '2', '3', 'persona - empresa', '1', '2016-02-18 12:02:34', null);
INSERT INTO `per_relacion` VALUES ('37', '33', '2', '3', 'persona - empresa', '1', '2016-02-18 12:02:35', null);
INSERT INTO `per_relacion` VALUES ('38', '34', '2', '3', 'persona - empresa', '1', '2016-02-18 12:02:46', null);
INSERT INTO `per_relacion` VALUES ('39', '35', '2', '3', 'persona - empresa', '1', '2016-02-18 12:02:53', null);
INSERT INTO `per_relacion` VALUES ('40', '36', '2', '3', 'persona - empresa', '1', '2016-02-18 12:02:21', null);
INSERT INTO `per_relacion` VALUES ('41', '37', '2', '3', 'persona - empresa', '1', '2016-02-18 12:02:08', null);
INSERT INTO `per_relacion` VALUES ('42', '38', '2', '3', 'persona - empresa', '1', '2016-02-18 12:02:27', null);
INSERT INTO `per_relacion` VALUES ('43', '39', '2', '3', 'persona - empresa', '1', '2016-02-18 12:02:04', null);
INSERT INTO `per_relacion` VALUES ('44', '40', '2', '3', 'persona - empresa', '1', '2016-02-18 12:02:11', null);
INSERT INTO `per_relacion` VALUES ('45', '41', '2', '3', 'persona - empresa', '1', '2016-02-18 12:02:58', null);
INSERT INTO `per_relacion` VALUES ('46', '42', '2', '3', 'persona - empresa', '1', '2016-02-18 12:02:20', null);
INSERT INTO `per_relacion` VALUES ('47', '43', '2', '3', 'persona - empresa', '1', '2016-02-18 12:02:06', null);
INSERT INTO `per_relacion` VALUES ('48', '44', '2', '3', 'persona - empresa', '1', '2016-02-18 12:02:32', null);
INSERT INTO `per_relacion` VALUES ('49', '45', '2', '3', 'persona - empresa', '1', '2016-02-18 12:02:43', null);
INSERT INTO `per_relacion` VALUES ('50', '46', '2', '3', 'persona - empresa', '1', '2016-02-18 17:02:53', null);
INSERT INTO `per_relacion` VALUES ('51', '47', '2', '3', 'persona - empresa', '1', '2016-02-18 17:02:28', null);
INSERT INTO `per_relacion` VALUES ('52', '48', '2', '3', 'persona - empresa', '1', '2016-02-18 17:02:01', null);
INSERT INTO `per_relacion` VALUES ('53', '49', '2', '3', 'persona - empresa', '1', '2016-02-18 17:02:28', null);
INSERT INTO `per_relacion` VALUES ('54', '50', '2', '3', 'persona - empresa', '1', '2016-02-18 17:02:25', null);
INSERT INTO `per_relacion` VALUES ('55', '51', '2', '3', 'persona - empresa', '1', '2016-02-19 10:02:11', null);
INSERT INTO `per_relacion` VALUES ('56', '52', '2', '3', 'persona - empresa', '1', '2016-02-20 12:02:34', null);
INSERT INTO `per_relacion` VALUES ('57', '53', '2', '3', 'persona - empresa', '1', '2016-02-20 12:02:49', null);
INSERT INTO `per_relacion` VALUES ('58', '54', '2', '3', 'persona - empresa', '1', '2016-02-20 12:02:02', null);
INSERT INTO `per_relacion` VALUES ('59', '55', '2', '3', 'persona - empresa', '1', '2016-02-20 12:02:43', null);
INSERT INTO `per_relacion` VALUES ('60', '56', '2', '3', 'persona - empresa', '1', '2016-02-22 10:02:10', null);
INSERT INTO `per_relacion` VALUES ('61', '57', '2', '3', 'persona - empresa', '1', '2016-02-22 10:02:48', null);
INSERT INTO `per_relacion` VALUES ('62', '58', '2', '3', 'persona - empresa', '1', '2016-02-22 11:02:19', null);
INSERT INTO `per_relacion` VALUES ('63', '59', '2', '3', 'persona - empresa', '1', '2016-02-22 11:02:30', null);
INSERT INTO `per_relacion` VALUES ('64', '60', '2', '3', 'persona - empresa', '1', '2016-02-22 12:02:02', null);
INSERT INTO `per_relacion` VALUES ('65', '61', '2', '3', 'persona - empresa', '1', '2016-02-22 12:02:45', null);
INSERT INTO `per_relacion` VALUES ('66', '62', '2', '3', 'persona - empresa', '1', '2016-02-22 12:02:42', null);
INSERT INTO `per_relacion` VALUES ('67', '63', '2', '3', 'persona - empresa', '1', '2016-02-22 13:02:34', null);
INSERT INTO `per_relacion` VALUES ('68', '64', '2', '3', 'persona - empresa', '1', '2016-02-22 15:02:31', null);
INSERT INTO `per_relacion` VALUES ('69', '65', '2', '3', 'persona - empresa', '1', '2016-02-22 15:02:32', null);
INSERT INTO `per_relacion` VALUES ('70', '66', '2', '3', 'persona - empresa', '1', '2016-02-22 16:02:45', null);
INSERT INTO `per_relacion` VALUES ('71', '67', '2', '3', 'persona - empresa', '1', '2016-02-22 16:02:13', null);
INSERT INTO `per_relacion` VALUES ('72', '68', '2', '3', 'persona - empresa', '1', '2016-02-22 16:02:30', null);
INSERT INTO `per_relacion` VALUES ('73', '4', '2', '3', 'persona - empresa', '1', '2016-02-18 09:02:55', null);
INSERT INTO `per_relacion` VALUES ('74', '26', '4', '3', 'Empleado - Empresa', '1', '2016-02-27 12:42:09', '2016-02-27 12:42:09');
INSERT INTO `per_relacion` VALUES ('75', '6', '4', '3', 'Empleado - Empresa', '1', '2016-02-27 12:51:25', '2016-02-27 12:51:25');
INSERT INTO `per_relacion` VALUES ('76', '7', '4', '3', 'Empleado - Empresa', '1', '2016-02-27 12:52:06', '2016-02-27 12:52:06');
INSERT INTO `per_relacion` VALUES ('77', '9', '4', '3', 'Empleado - Empresa', '1', '2016-02-27 12:52:22', '2016-02-27 12:52:22');
INSERT INTO `per_relacion` VALUES ('78', '5', '4', '3', 'Empleado - Empresa', '1', '2016-02-27 12:52:35', '2016-02-27 12:52:35');
INSERT INTO `per_relacion` VALUES ('79', '10', '4', '3', 'Empleado - Empresa', '1', '2016-02-27 12:52:51', '2016-02-27 12:52:51');
INSERT INTO `per_relacion` VALUES ('80', '8', '4', '3', 'Empleado - Empresa', '1', '2016-02-27 12:53:04', '2016-02-27 12:53:04');
INSERT INTO `per_relacion` VALUES ('81', '26', '3', '3', 'Cliente - Empresa', '1', '2016-03-02 17:29:47', '2016-03-02 17:29:47');
INSERT INTO `per_relacion` VALUES ('82', '49', '3', '3', 'Cliente - Empresa', '1', '2016-03-02 17:33:11', '2016-03-02 17:33:11');
INSERT INTO `per_relacion` VALUES ('83', '47', '3', '3', 'Cliente - Empresa', '1', '2016-04-14 13:03:33', '2016-04-14 13:03:33');
INSERT INTO `per_relacion` VALUES ('84', '34', '3', '3', 'Cliente - Empresa', '1', '2016-04-14 13:07:21', '2016-04-14 13:07:21');
INSERT INTO `per_relacion` VALUES ('85', '69', '2', '3', 'persona - empresa', '1', '2016-04-21 12:22:49', '2016-04-21 12:22:49');
INSERT INTO `per_relacion` VALUES ('86', '69', '5', '3', 'Aval - Empresa', '1', '2016-04-21 12:22:49', '2016-04-21 12:22:49');

-- ----------------------------
-- Table structure for per_telefono
-- ----------------------------
DROP TABLE IF EXISTS `per_telefono`;
CREATE TABLE `per_telefono` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `persona_id` int(10) unsigned NOT NULL,
  `tipo_telefono_id` int(10) unsigned NOT NULL,
  `telefono` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `item` smallint(6) NOT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `per_telefono_persona_id_foreign` (`persona_id`),
  KEY `per_telefono_tipo_telefono_id_foreign` (`tipo_telefono_id`),
  KEY `per_telefono_telefono_index` (`telefono`),
  CONSTRAINT `per_telefono_persona_id_foreign` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`),
  CONSTRAINT `per_telefono_tipo_telefono_id_foreign` FOREIGN KEY (`tipo_telefono_id`) REFERENCES `tipo_telefono` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of per_telefono
-- ----------------------------
INSERT INTO `per_telefono` VALUES ('1', '2', '1', '996393414', '1', '1');
INSERT INTO `per_telefono` VALUES ('2', '4', '1', '900000000', '1', '1');
INSERT INTO `per_telefono` VALUES ('3', '5', '1', '981562855', '1', '1');
INSERT INTO `per_telefono` VALUES ('4', '5', '1', '951869875', '2', '1');
INSERT INTO `per_telefono` VALUES ('5', '6', '1', '976882074', '1', '1');
INSERT INTO `per_telefono` VALUES ('6', '7', '1', '980713668', '1', '1');
INSERT INTO `per_telefono` VALUES ('7', '8', '1', '945027584', '1', '1');
INSERT INTO `per_telefono` VALUES ('8', '9', '1', '976849994', '1', '1');
INSERT INTO `per_telefono` VALUES ('9', '10', '1', '945936439', '1', '1');
INSERT INTO `per_telefono` VALUES ('10', '11', '1', '944816854', '1', '1');
INSERT INTO `per_telefono` VALUES ('11', '12', '1', '956018315', '1', '1');
INSERT INTO `per_telefono` VALUES ('12', '13', '1', '944895529', '1', '1');
INSERT INTO `per_telefono` VALUES ('13', '13', '1', '979825777', '2', '1');
INSERT INTO `per_telefono` VALUES ('14', '15', '1', '934571609', '1', '1');
INSERT INTO `per_telefono` VALUES ('15', '16', '1', '994479670', '1', '1');
INSERT INTO `per_telefono` VALUES ('16', '16', '1', '986562573', '2', '1');
INSERT INTO `per_telefono` VALUES ('17', '17', '1', '952447782', '1', '1');
INSERT INTO `per_telefono` VALUES ('18', '18', '1', '947525904', '1', '1');
INSERT INTO `per_telefono` VALUES ('19', '19', '1', '949577869', '1', '1');
INSERT INTO `per_telefono` VALUES ('20', '19', '1', '977597812', '2', '1');
INSERT INTO `per_telefono` VALUES ('21', '20', '1', '951071605', '1', '1');
INSERT INTO `per_telefono` VALUES ('22', '20', '1', '947928868', '2', '1');
INSERT INTO `per_telefono` VALUES ('23', '21', '1', '987830518', '1', '1');
INSERT INTO `per_telefono` VALUES ('24', '21', '1', '979117057', '2', '1');
INSERT INTO `per_telefono` VALUES ('25', '22', '1', '962574001', '1', '1');
INSERT INTO `per_telefono` VALUES ('26', '22', '1', '074206889', '2', '1');
INSERT INTO `per_telefono` VALUES ('27', '23', '1', '948550075', '1', '1');
INSERT INTO `per_telefono` VALUES ('28', '23', '1', '074251034', '2', '1');
INSERT INTO `per_telefono` VALUES ('29', '24', '1', '945117520', '1', '1');
INSERT INTO `per_telefono` VALUES ('30', '25', '1', '971734374', '1', '1');
INSERT INTO `per_telefono` VALUES ('31', '25', '1', '977999410', '2', '1');
INSERT INTO `per_telefono` VALUES ('32', '26', '1', '958795034', '1', '1');
INSERT INTO `per_telefono` VALUES ('33', '27', '1', '943881125', '1', '1');
INSERT INTO `per_telefono` VALUES ('34', '27', '1', '951038270', '2', '1');
INSERT INTO `per_telefono` VALUES ('35', '28', '1', '982013556', '1', '1');
INSERT INTO `per_telefono` VALUES ('36', '28', '1', '979065810', '2', '1');
INSERT INTO `per_telefono` VALUES ('37', '29', '1', '982013556', '1', '1');
INSERT INTO `per_telefono` VALUES ('38', '29', '1', '979065810', '2', '1');
INSERT INTO `per_telefono` VALUES ('39', '30', '1', '962578851', '1', '1');
INSERT INTO `per_telefono` VALUES ('40', '30', '1', '930535301', '2', '1');
INSERT INTO `per_telefono` VALUES ('41', '31', '1', '978060110', '1', '1');
INSERT INTO `per_telefono` VALUES ('42', '31', '1', '996368316', '2', '1');
INSERT INTO `per_telefono` VALUES ('43', '32', '1', '958639546', '1', '1');
INSERT INTO `per_telefono` VALUES ('44', '32', '1', '971209915', '2', '1');
INSERT INTO `per_telefono` VALUES ('45', '33', '1', '993003897', '1', '1');
INSERT INTO `per_telefono` VALUES ('46', '34', '1', '957657098', '1', '1');
INSERT INTO `per_telefono` VALUES ('47', '34', '1', '944957659', '2', '1');
INSERT INTO `per_telefono` VALUES ('48', '35', '1', '981562855', '1', '1');
INSERT INTO `per_telefono` VALUES ('49', '36', '1', '994809544', '1', '1');
INSERT INTO `per_telefono` VALUES ('50', '37', '1', '000000000', '1', '1');
INSERT INTO `per_telefono` VALUES ('51', '38', '1', '995246561', '1', '1');
INSERT INTO `per_telefono` VALUES ('52', '39', '1', '000000000', '1', '1');
INSERT INTO `per_telefono` VALUES ('53', '40', '1', '975087006', '1', '1');
INSERT INTO `per_telefono` VALUES ('54', '41', '1', '979004104', '1', '1');
INSERT INTO `per_telefono` VALUES ('55', '42', '1', '954959528', '1', '1');
INSERT INTO `per_telefono` VALUES ('56', '43', '1', '966572481', '1', '1');
INSERT INTO `per_telefono` VALUES ('57', '44', '1', '949900130', '1', '1');
INSERT INTO `per_telefono` VALUES ('58', '45', '1', '979720241', '1', '1');
INSERT INTO `per_telefono` VALUES ('59', '46', '1', '962883943', '1', '1');
INSERT INTO `per_telefono` VALUES ('60', '46', '1', '954062318', '2', '1');
INSERT INTO `per_telefono` VALUES ('61', '46', '1', '978982185', '3', '1');
INSERT INTO `per_telefono` VALUES ('62', '47', '1', '947118958', '1', '1');
INSERT INTO `per_telefono` VALUES ('63', '47', '1', '990292190', '2', '1');
INSERT INTO `per_telefono` VALUES ('64', '48', '1', '969465700', '1', '1');
INSERT INTO `per_telefono` VALUES ('65', '48', '1', '996013575', '2', '1');
INSERT INTO `per_telefono` VALUES ('66', '48', '1', '979994009', '3', '1');
INSERT INTO `per_telefono` VALUES ('67', '49', '1', '956055724', '1', '1');
INSERT INTO `per_telefono` VALUES ('68', '49', '1', '971678383', '2', '1');
INSERT INTO `per_telefono` VALUES ('69', '50', '1', '995497176', '1', '1');
INSERT INTO `per_telefono` VALUES ('70', '50', '1', '965864300', '2', '1');
INSERT INTO `per_telefono` VALUES ('71', '51', '1', '943483959', '1', '1');
INSERT INTO `per_telefono` VALUES ('72', '51', '1', '949096489', '2', '1');
INSERT INTO `per_telefono` VALUES ('73', '52', '1', '979800343', '1', '1');
INSERT INTO `per_telefono` VALUES ('74', '52', '1', '947679197', '2', '1');
INSERT INTO `per_telefono` VALUES ('75', '52', '1', '979570321', '3', '1');
INSERT INTO `per_telefono` VALUES ('76', '53', '1', '962838379', '1', '1');
INSERT INTO `per_telefono` VALUES ('77', '53', '1', '958403704', '2', '1');
INSERT INTO `per_telefono` VALUES ('78', '54', '1', '999721525', '1', '1');
INSERT INTO `per_telefono` VALUES ('79', '54', '1', '979517278', '2', '1');
INSERT INTO `per_telefono` VALUES ('80', '55', '1', '949403964', '1', '1');
INSERT INTO `per_telefono` VALUES ('81', '55', '1', '954859778', '2', '1');
INSERT INTO `per_telefono` VALUES ('82', '55', '1', '999801283', '3', '1');
INSERT INTO `per_telefono` VALUES ('83', '56', '1', '938216300', '1', '1');
INSERT INTO `per_telefono` VALUES ('84', '56', '1', '937548373', '2', '1');
INSERT INTO `per_telefono` VALUES ('85', '57', '1', '988676809', '1', '1');
INSERT INTO `per_telefono` VALUES ('86', '57', '1', '074611636', '2', '1');
INSERT INTO `per_telefono` VALUES ('87', '57', '1', '978860488', '3', '1');
INSERT INTO `per_telefono` VALUES ('88', '58', '1', '943925622', '1', '1');
INSERT INTO `per_telefono` VALUES ('89', '59', '1', '950159738', '1', '1');
INSERT INTO `per_telefono` VALUES ('90', '59', '1', '954355224', '2', '1');
INSERT INTO `per_telefono` VALUES ('91', '59', '1', '074777797', '3', '1');
INSERT INTO `per_telefono` VALUES ('92', '60', '1', '952556874', '1', '1');
INSERT INTO `per_telefono` VALUES ('93', '60', '1', '074219481', '2', '1');
INSERT INTO `per_telefono` VALUES ('94', '60', '1', '975374529', '3', '1');
INSERT INTO `per_telefono` VALUES ('95', '61', '1', '978163896', '1', '1');
INSERT INTO `per_telefono` VALUES ('96', '61', '1', '978115309', '2', '1');
INSERT INTO `per_telefono` VALUES ('97', '62', '1', '979912996', '1', '1');
INSERT INTO `per_telefono` VALUES ('98', '62', '1', '972912998', '2', '1');
INSERT INTO `per_telefono` VALUES ('99', '63', '1', '995321924', '1', '1');
INSERT INTO `per_telefono` VALUES ('100', '63', '1', '074512426', '2', '1');
INSERT INTO `per_telefono` VALUES ('101', '63', '1', '990855844', '3', '1');
INSERT INTO `per_telefono` VALUES ('102', '64', '1', '983947637', '1', '1');
INSERT INTO `per_telefono` VALUES ('103', '64', '1', '074257611', '2', '1');
INSERT INTO `per_telefono` VALUES ('104', '64', '1', '957976308', '3', '1');
INSERT INTO `per_telefono` VALUES ('105', '65', '1', '983947637', '1', '1');
INSERT INTO `per_telefono` VALUES ('106', '65', '1', '074257611', '2', '1');
INSERT INTO `per_telefono` VALUES ('107', '65', '1', '957976308', '3', '1');
INSERT INTO `per_telefono` VALUES ('108', '66', '1', '974972278', '1', '1');
INSERT INTO `per_telefono` VALUES ('109', '66', '1', '074284265', '2', '1');
INSERT INTO `per_telefono` VALUES ('110', '66', '1', '969553494', '3', '1');
INSERT INTO `per_telefono` VALUES ('111', '67', '1', '959457063', '1', '1');
INSERT INTO `per_telefono` VALUES ('112', '67', '1', '074506055', '2', '1');
INSERT INTO `per_telefono` VALUES ('113', '67', '1', '951933689', '3', '1');
INSERT INTO `per_telefono` VALUES ('114', '68', '1', '990060570', '1', '1');
INSERT INTO `per_telefono` VALUES ('115', '68', '1', '959495026', '2', '1');
INSERT INTO `per_telefono` VALUES ('116', '69', '1', '986154123', '1', '1');

-- ----------------------------
-- Table structure for per_web
-- ----------------------------
DROP TABLE IF EXISTS `per_web`;
CREATE TABLE `per_web` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `persona_id` int(10) unsigned NOT NULL,
  `tipo_web_id` int(10) unsigned NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item` smallint(6) NOT NULL,
  `estado` smallint(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `per_web_persona_id_foreign` (`persona_id`),
  KEY `per_web_tipo_web_id_foreign` (`tipo_web_id`),
  CONSTRAINT `per_web_tipo_web_id_foreign` FOREIGN KEY (`tipo_web_id`) REFERENCES `tipo_web` (`id`),
  CONSTRAINT `per_web_persona_id_foreign` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of per_web
-- ----------------------------

-- ----------------------------
-- Table structure for persona
-- ----------------------------
DROP TABLE IF EXISTS `persona`;
CREATE TABLE `persona` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `per_nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `per_apellidos` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `per_fecha_nac` date DEFAULT NULL,
  `per_tipo` int(11) NOT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `persona_per_nombre_index` (`per_nombre`),
  KEY `persona_per_apellidos_index` (`per_apellidos`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of persona
-- ----------------------------
INSERT INTO `persona` VALUES ('1', 'Sudo - Planeatec', '', '2016-02-16', '0', '1', null, null);
INSERT INTO `persona` VALUES ('2', '@armandoaepp', '@sudo', '1986-11-11', '1', '1', null, null);
INSERT INTO `persona` VALUES ('3', 'Prestamos del Norte', '', '2015-09-05', '2', '1', null, null);
INSERT INTO `persona` VALUES ('4', 'Adminstrador', 'Super Admin ', '1986-11-11', '1', '1', null, null);
INSERT INTO `persona` VALUES ('5', 'Herbert Engel', 'Gálvez Rojas', '2987-06-13', '1', '1', '2016-02-16 16:58:26', '2016-02-16 16:58:26');
INSERT INTO `persona` VALUES ('6', 'Rosina Del Pilar', 'Fernandez Manzanares', '1991-05-04', '1', '1', '2016-02-16 17:00:37', '2016-02-16 17:00:37');
INSERT INTO `persona` VALUES ('7', 'Luis Alberto', 'Sanchez Garay', '1987-04-08', '1', '1', '2016-02-16 17:03:09', '2016-02-16 17:03:09');
INSERT INTO `persona` VALUES ('8', 'Jessica Jackeline', 'Gonzales Kodzman', '1988-01-08', '1', '1', '2016-02-16 17:10:34', '2016-02-16 17:10:34');
INSERT INTO `persona` VALUES ('9', 'Vargas Torres', 'Victor Manuel', '1987-12-23', '1', '1', '2016-02-16 17:15:42', '2016-02-16 17:15:42');
INSERT INTO `persona` VALUES ('10', 'Mercado Lara', 'Cristhian Lucio', '1986-06-15', '1', '1', '2016-02-16 17:20:49', '2016-02-16 17:20:49');
INSERT INTO `persona` VALUES ('11', 'Segundo Apolinar', 'Cerna Rivera', '1981-07-27', '1', '1', '2016-02-17 11:33:01', '2016-02-22 16:47:01');
INSERT INTO `persona` VALUES ('12', 'Luis Alberto', 'Bances Serquen', '1954-04-05', '1', '1', '2016-02-17 11:43:16', '2016-02-22 16:49:32');
INSERT INTO `persona` VALUES ('13', 'Aurora', 'Muro Rojas', '1978-09-12', '1', '1', '2016-02-17 11:52:47', '2016-02-17 11:58:32');
INSERT INTO `persona` VALUES ('14', 'Aurora', 'Muro Rojas', '1978-09-13', '1', '1', '2016-02-17 11:52:47', '2016-02-17 11:52:47');
INSERT INTO `persona` VALUES ('15', 'Jose Felix', 'Quispe Santillan', '1985-01-04', '1', '1', '2016-02-17 17:44:27', '2016-02-17 17:44:27');
INSERT INTO `persona` VALUES ('16', 'Evert', 'Villalobos Barboza', '1988-08-30', '1', '1', '2016-02-17 17:52:55', '2016-02-17 17:52:55');
INSERT INTO `persona` VALUES ('17', 'Luis Alberto', 'Espinal Nicolas', '1982-05-05', '1', '1', '2016-02-17 18:00:52', '2016-02-17 18:00:52');
INSERT INTO `persona` VALUES ('18', 'Juan Eduardo', 'Vela Yamunaque', '1960-11-02', '1', '1', '2016-02-17 18:12:25', '2016-02-17 18:12:25');
INSERT INTO `persona` VALUES ('19', 'Marcos Hugo', 'De La Cruz Valle', '1975-04-16', '1', '1', '2016-02-17 18:21:02', '2016-02-17 18:21:02');
INSERT INTO `persona` VALUES ('20', 'Oscar', 'Sandoval Neyra', '1980-12-08', '1', '1', '2016-02-17 18:41:38', '2016-02-17 18:41:38');
INSERT INTO `persona` VALUES ('21', 'Nancy Yanet', 'Toro Chavez', '1984-12-20', '1', '1', '2016-02-17 18:59:59', '2016-02-17 18:59:59');
INSERT INTO `persona` VALUES ('22', 'Carlos Enrique', 'Romero Chumbe', '1982-06-17', '1', '1', '2016-02-18 09:39:22', '2016-02-18 09:39:22');
INSERT INTO `persona` VALUES ('23', 'Anthony Jean Pierre', 'Davila Vasquez', '1991-05-19', '1', '1', '2016-02-18 09:41:17', '2016-02-18 09:41:17');
INSERT INTO `persona` VALUES ('24', 'Josue Jimmy', 'Querevalu Calderon', '1978-11-12', '1', '1', '2016-02-18 10:01:35', '2016-02-18 11:06:39');
INSERT INTO `persona` VALUES ('25', 'Sarita Janett', 'Forero Peña', '1984-01-08', '1', '1', '2016-02-18 10:10:16', '2016-02-18 10:10:16');
INSERT INTO `persona` VALUES ('26', 'ROLANDO KLITHO', 'TIGRE SIPION', '1979-10-07', '1', '1', '2016-02-18 10:49:31', '2016-02-18 12:40:36');
INSERT INTO `persona` VALUES ('27', 'Gabriela', 'Rodas Nuñez', '1983-06-21', '1', '1', '2016-02-18 11:13:25', '2016-02-18 11:13:25');
INSERT INTO `persona` VALUES ('28', 'Reynaldo', 'Rivera Cordova', '1974-02-20', '1', '1', '2016-02-18 11:25:02', '2016-02-18 11:25:02');
INSERT INTO `persona` VALUES ('29', 'Reynaldo', 'Rivera Cordova', '1974-02-20', '1', '1', '2016-02-18 11:25:03', '2016-02-18 11:25:03');
INSERT INTO `persona` VALUES ('30', 'Julio Albert', 'Alvitres Llanos', '1986-07-27', '1', '1', '2016-02-18 11:40:29', '2016-02-18 11:40:29');
INSERT INTO `persona` VALUES ('31', 'Jorge Antonio', 'Perleche Martinez', '1991-06-13', '1', '1', '2016-02-18 11:59:37', '2016-02-18 11:59:37');
INSERT INTO `persona` VALUES ('32', 'Fernando Alonso', 'Calla Cardenas', '1980-08-04', '1', '1', '2016-02-18 12:04:34', '2016-02-18 12:04:34');
INSERT INTO `persona` VALUES ('33', 'Carlos Rafael', 'PLENGE COTTESS', '1982-02-24', '1', '1', '2016-02-18 12:06:35', '2016-02-18 12:06:35');
INSERT INTO `persona` VALUES ('34', 'Oscar Martin', 'Purizaca Rivas', '1963-11-09', '1', '1', '2016-02-18 12:09:46', '2016-02-18 12:09:46');
INSERT INTO `persona` VALUES ('35', 'Yannlui Eduardo', 'VELASQUEZ GALVEZ', '1986-07-10', '1', '1', '2016-02-18 12:14:53', '2016-02-18 12:14:53');
INSERT INTO `persona` VALUES ('36', 'Monica Magali', 'DE LA FUENTE SANCHEZ', '1971-05-23', '1', '1', '2016-02-18 12:18:21', '2016-02-18 12:18:21');
INSERT INTO `persona` VALUES ('37', 'Marco Adalberto', 'CABRERA CORNETERO', '1963-04-25', '1', '1', '2016-02-18 12:21:08', '2016-02-18 12:21:08');
INSERT INTO `persona` VALUES ('38', 'Lorena Isabel', 'LUGO SANTA CRUZ', '1991-11-13', '1', '1', '2016-02-18 12:23:27', '2016-02-18 12:23:27');
INSERT INTO `persona` VALUES ('39', 'Victor Junior', 'BECERRA HUERTAS', '1985-07-19', '1', '1', '2016-02-18 12:26:04', '2016-02-18 12:26:04');
INSERT INTO `persona` VALUES ('40', 'Dora Elizabeth', 'MAYURI ALVARADO', '1997-09-24', '1', '1', '2016-02-18 12:37:11', '2016-02-18 12:37:11');
INSERT INTO `persona` VALUES ('41', 'Orlando', 'GARAY FARRO', '1968-07-08', '1', '1', '2016-02-18 12:39:58', '2016-02-18 12:39:58');
INSERT INTO `persona` VALUES ('42', 'Billy Jonathan', 'VALDIVIESO FARFAN', '1985-01-23', '1', '1', '2016-02-18 12:43:20', '2016-02-18 12:49:15');
INSERT INTO `persona` VALUES ('43', 'Juan José Gabriel', 'SALAZAR BAUTISTA', '1987-12-19', '1', '1', '2016-02-18 12:46:06', '2016-02-18 12:46:06');
INSERT INTO `persona` VALUES ('44', 'Dilaura Yesenia', 'CHANAMÉ GONZALES', '1990-01-14', '1', '1', '2016-02-18 12:47:32', '2016-02-18 12:47:32');
INSERT INTO `persona` VALUES ('45', 'Marco Antonio', 'GARCIA UCHOFEN', '1967-12-06', '1', '1', '2016-02-18 12:48:43', '2016-02-18 12:48:43');
INSERT INTO `persona` VALUES ('46', 'Elmer Daniel', 'Oblitas Alamo', '1987-09-08', '1', '1', '2016-02-18 17:18:53', '2016-02-18 17:18:53');
INSERT INTO `persona` VALUES ('47', 'Jaime', 'Santa Maria Calle', '1977-02-27', '1', '1', '2016-02-18 17:24:28', '2016-02-18 17:24:28');
INSERT INTO `persona` VALUES ('48', 'Vega Alarcon', 'Hubert Esmit', '1992-10-10', '1', '1', '2016-02-18 17:30:01', '2016-02-18 17:30:01');
INSERT INTO `persona` VALUES ('49', 'Miluska Guisella', 'Saavedra Montenegro', '1987-08-25', '1', '1', '2016-02-18 17:37:28', '2016-02-18 17:37:28');
INSERT INTO `persona` VALUES ('50', 'Eber', 'Huiman Ruiz', '1984-10-18', '1', '1', '2016-02-18 17:49:25', '2016-02-18 17:49:25');
INSERT INTO `persona` VALUES ('51', 'Juan Carlos', 'Purihuaman Garcia', '1987-06-04', '1', '1', '2016-02-19 10:48:11', '2016-02-20 10:26:19');
INSERT INTO `persona` VALUES ('52', 'Luis Alberto', 'Campos zamora', '1983-04-19', '1', '1', '2016-02-20 12:25:34', '2016-02-20 12:25:34');
INSERT INTO `persona` VALUES ('53', 'Marco Alfredo', 'Jara Anton', '1976-07-14', '1', '1', '2016-02-20 12:33:49', '2016-02-20 12:33:49');
INSERT INTO `persona` VALUES ('54', 'Niño Medianero', 'Maria del Rosario', '1969-09-08', '1', '1', '2016-02-20 12:44:02', '2016-02-20 12:44:02');
INSERT INTO `persona` VALUES ('55', 'Oscar Eduardo', 'Salazar Romero', '1980-09-25', '1', '1', '2016-02-20 12:53:43', '2016-02-20 12:53:43');
INSERT INTO `persona` VALUES ('56', 'Carlos Alberto', 'Varillas Velasquez', '1972-09-28', '1', '1', '2016-02-22 10:42:10', '2016-02-22 10:42:10');
INSERT INTO `persona` VALUES ('57', 'Norbil Anibal', 'Mera Vasquez', '1993-09-12', '1', '1', '2016-02-22 10:55:48', '2016-02-22 10:55:48');
INSERT INTO `persona` VALUES ('58', 'Aladino', 'Guerava Campos', '1979-12-10', '1', '1', '2016-02-22 11:01:19', '2016-02-22 11:01:19');
INSERT INTO `persona` VALUES ('59', 'Doris Nelly', 'Enriquez Colchado', '1952-05-08', '1', '1', '2016-02-22 11:17:30', '2016-02-22 11:17:30');
INSERT INTO `persona` VALUES ('60', 'Dubert Haward', 'Arboleda Calle', '1992-02-23', '1', '1', '2016-02-22 12:29:02', '2016-02-22 12:29:02');
INSERT INTO `persona` VALUES ('61', 'Eleuterio', 'Monteza Aguilar', '1954-08-25', '1', '1', '2016-02-22 12:32:45', '2016-02-22 12:32:45');
INSERT INTO `persona` VALUES ('62', 'Jaimen', 'Morales Coveñas', '1981-08-13', '1', '1', '2016-02-22 12:38:42', '2016-02-22 12:38:42');
INSERT INTO `persona` VALUES ('63', 'Marlon Jonathan', 'Becerra Juarez', '1987-01-23', '1', '1', '2016-02-22 13:02:34', '2016-02-22 13:02:34');
INSERT INTO `persona` VALUES ('64', 'Obed Amos', 'Saldaña Rivera', '1991-01-04', '1', '1', '2016-02-22 15:58:31', '2016-02-22 15:58:31');
INSERT INTO `persona` VALUES ('65', 'Obed Amos', 'Saldaña Rivera', '1991-01-04', '1', '1', '2016-02-22 15:58:32', '2016-02-22 15:58:32');
INSERT INTO `persona` VALUES ('66', 'Orlando Javier', 'Hernandez Jara', '1972-08-14', '1', '1', '2016-02-22 16:03:45', '2016-02-22 16:03:45');
INSERT INTO `persona` VALUES ('67', 'Santos', 'Reyes De La Cruz', '1978-05-13', '1', '1', '2016-02-22 16:10:13', '2016-02-22 16:10:13');
INSERT INTO `persona` VALUES ('68', 'Victor', 'Fernandez Bravo', '1937-08-26', '1', '1', '2016-02-22 16:16:30', '2016-02-22 16:16:30');
INSERT INTO `persona` VALUES ('69', 'Jorge', 'Briones Zavala', '1987-06-13', '1', '1', '2016-04-21 12:22:49', '2016-04-21 12:22:49');

-- ----------------------------
-- Table structure for prestamo
-- ----------------------------
DROP TABLE IF EXISTS `prestamo`;
CREATE TABLE `prestamo` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cliente_id` int(10) unsigned NOT NULL,
  `tipo_prestamo_id` int(10) unsigned NOT NULL,
  `tipo_moneda_id` int(10) unsigned NOT NULL,
  `tipo_periodo_id` int(10) unsigned NOT NULL,
  `tipo_garantia_id` int(10) unsigned DEFAULT NULL,
  `tipo_pago_id` int(10) unsigned DEFAULT NULL,
  `vehiculo_id` int(10) unsigned DEFAULT NULL,
  `aval_id` int(10) unsigned DEFAULT NULL,
  `acuerdo_pago_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  `inicial_porcentaje` decimal(10,2) NOT NULL,
  `inicial_monto` decimal(10,2) NOT NULL,
  `tasa_interes` decimal(10,2) NOT NULL,
  `valor_total` decimal(10,2) NOT NULL,
  `num_cuotas` int(11) NOT NULL,
  `mora` decimal(10,2) NOT NULL,
  `complacencia` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pagos_parciales` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `propietario` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `observacion` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `seguro_tr` decimal(10,2) NOT NULL,
  `gps` decimal(10,2) NOT NULL,
  `soat` decimal(10,2) NOT NULL,
  `gas` decimal(10,2) NOT NULL,
  `otros` decimal(10,2) NOT NULL,
  `fecha_credito` date NOT NULL,
  `fecha_desembolso` date NOT NULL,
  `fecha_prorrateo` date DEFAULT NULL,
  `fecha_prorrateo_esp` date DEFAULT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prestamo_cliente_id_foreign` (`cliente_id`),
  KEY `prestamo_tipo_prestamo_id_foreign` (`tipo_prestamo_id`),
  KEY `prestamo_tipo_moneda_id_foreign` (`tipo_moneda_id`),
  KEY `prestamo_tipo_periodo_id_foreign` (`tipo_periodo_id`),
  KEY `prestamo_tipo_garantia_id_foreign` (`tipo_garantia_id`),
  KEY `prestamo_tipo_pago_id_foreign` (`tipo_pago_id`),
  KEY `prestamo_vehiculo_id_foreign` (`vehiculo_id`),
  KEY `prestamo_aval_id_foreign` (`aval_id`),
  KEY `prestamo_user_id_foreign` (`user_id`),
  CONSTRAINT `prestamo_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `prestamo_aval_id_foreign` FOREIGN KEY (`aval_id`) REFERENCES `aval` (`id`),
  CONSTRAINT `prestamo_cliente_id_foreign` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`),
  CONSTRAINT `prestamo_tipo_garantia_id_foreign` FOREIGN KEY (`tipo_garantia_id`) REFERENCES `tipo_garantia` (`id`),
  CONSTRAINT `prestamo_tipo_moneda_id_foreign` FOREIGN KEY (`tipo_moneda_id`) REFERENCES `tipo_moneda` (`id`),
  CONSTRAINT `prestamo_tipo_pago_id_foreign` FOREIGN KEY (`tipo_pago_id`) REFERENCES `tipo_pago` (`id`),
  CONSTRAINT `prestamo_tipo_periodo_id_foreign` FOREIGN KEY (`tipo_periodo_id`) REFERENCES `tipo_periodo` (`id`),
  CONSTRAINT `prestamo_tipo_prestamo_id_foreign` FOREIGN KEY (`tipo_prestamo_id`) REFERENCES `tipo_prestamo` (`id`),
  CONSTRAINT `prestamo_vehiculo_id_foreign` FOREIGN KEY (`vehiculo_id`) REFERENCES `vehiculo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of prestamo
-- ----------------------------
INSERT INTO `prestamo` VALUES ('1', '4', '1', '2', '4', '1', '1', '1', null, '3', '2', '7200.00', '25.43', '1831.15', '5.00', '7168.85', '36', '15.00', '', 'NO', '1', 'NINGUNA', '630.00', '280.00', '90.00', '800.00', '0.00', '2016-05-02', '2016-05-26', '2016-05-28', null, '2', '2016-05-24 10:12:41', '2016-05-24 10:39:08');

-- ----------------------------
-- Table structure for prestamo_estado
-- ----------------------------
DROP TABLE IF EXISTS `prestamo_estado`;
CREATE TABLE `prestamo_estado` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prestamo_id` bigint(20) unsigned NOT NULL,
  `tipo_estado_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `glosa` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `estado` smallint(5) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `prestamo_estado_prestamo_id_foreign` (`prestamo_id`),
  KEY `prestamo_estado_tipo_estado_id_foreign` (`tipo_estado_id`),
  CONSTRAINT `prestamo_estado_tipo_estado_id_foreign` FOREIGN KEY (`tipo_estado_id`) REFERENCES `tipo_estado` (`id`),
  CONSTRAINT `prestamo_estado_prestamo_id_foreign` FOREIGN KEY (`prestamo_id`) REFERENCES `prestamo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of prestamo_estado
-- ----------------------------
INSERT INTO `prestamo_estado` VALUES ('1', '1', '1', '2', '', '1', '2016-05-24 10:12:41', '2016-05-24 10:12:41');
INSERT INTO `prestamo_estado` VALUES ('2', '1', '2', '3', '', '1', '2016-05-24 10:39:08', '2016-05-24 10:39:08');

-- ----------------------------
-- Table structure for rol
-- ----------------------------
DROP TABLE IF EXISTS `rol`;
CREATE TABLE `rol` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rol
-- ----------------------------
INSERT INTO `rol` VALUES ('1', 'Sudo', '2');
INSERT INTO `rol` VALUES ('2', 'Super Admin', '2');
INSERT INTO `rol` VALUES ('3', 'Jefe Comercial', '1');
INSERT INTO `rol` VALUES ('4', 'Vendedor', '1');
INSERT INTO `rol` VALUES ('5', 'Gerencia', '1');
INSERT INTO `rol` VALUES ('6', 'Inspector Vehicular', '1');
INSERT INTO `rol` VALUES ('7', 'Administrador', '1');
INSERT INTO `rol` VALUES ('8', 'Cobranza', '1');
INSERT INTO `rol` VALUES ('9', 'Atención al Cliente & Legal', '1');

-- ----------------------------
-- Table structure for rol_control
-- ----------------------------
DROP TABLE IF EXISTS `rol_control`;
CREATE TABLE `rol_control` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rol_id` int(10) unsigned NOT NULL,
  `control_id` int(10) unsigned NOT NULL,
  `referencia` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `rol_control_rol_id_foreign` (`rol_id`),
  KEY `rol_control_control_id_foreign` (`control_id`),
  CONSTRAINT `rol_control_control_id_foreign` FOREIGN KEY (`control_id`) REFERENCES `control` (`id`),
  CONSTRAINT `rol_control_rol_id_foreign` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=457 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rol_control
-- ----------------------------
INSERT INTO `rol_control` VALUES ('1', '1', '1', 'Gestión de Información', '1');
INSERT INTO `rol_control` VALUES ('2', '1', '2', 'Maestros', '1');
INSERT INTO `rol_control` VALUES ('3', '1', '3', 'Personas', '1');
INSERT INTO `rol_control` VALUES ('4', '1', '4', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('5', '1', '5', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('6', '1', '6', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('7', '1', '7', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('8', '1', '8', 'Accesos', '1');
INSERT INTO `rol_control` VALUES ('9', '1', '9', 'Roles', '1');
INSERT INTO `rol_control` VALUES ('10', '1', '10', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('11', '1', '11', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('12', '1', '12', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('13', '1', '13', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('14', '1', '14', 'Accesos Rol', '1');
INSERT INTO `rol_control` VALUES ('15', '1', '15', 'Usuarios', '1');
INSERT INTO `rol_control` VALUES ('16', '1', '16', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('17', '1', '17', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('18', '1', '18', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('19', '1', '19', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('20', '1', '20', 'accesos', '1');
INSERT INTO `rol_control` VALUES ('21', '3', '1', 'Gestión de Información', '1');
INSERT INTO `rol_control` VALUES ('22', '3', '2', 'Maestros', '1');
INSERT INTO `rol_control` VALUES ('23', '3', '3', 'Personas', '1');
INSERT INTO `rol_control` VALUES ('24', '3', '4', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('25', '3', '5', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('26', '3', '6', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('27', '3', '8', 'Accesos', '1');
INSERT INTO `rol_control` VALUES ('28', '3', '9', 'Roles', '1');
INSERT INTO `rol_control` VALUES ('29', '3', '10', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('30', '3', '11', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('31', '3', '12', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('32', '3', '13', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('33', '3', '14', 'Accesos Rol', '1');
INSERT INTO `rol_control` VALUES ('34', '3', '15', 'Usuarios', '1');
INSERT INTO `rol_control` VALUES ('35', '3', '16', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('36', '3', '17', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('37', '3', '18', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('38', '3', '19', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('39', '3', '20', 'accesos', '1');
INSERT INTO `rol_control` VALUES ('40', '4', '1', 'Gestión de Información', '1');
INSERT INTO `rol_control` VALUES ('41', '4', '2', 'Maestros', '1');
INSERT INTO `rol_control` VALUES ('42', '4', '3', 'Personas', '1');
INSERT INTO `rol_control` VALUES ('43', '4', '4', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('44', '4', '5', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('45', '4', '6', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('46', '4', '8', 'Accesos', '1');
INSERT INTO `rol_control` VALUES ('47', '4', '9', 'Roles', '1');
INSERT INTO `rol_control` VALUES ('48', '4', '10', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('49', '4', '11', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('50', '4', '12', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('51', '4', '13', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('52', '4', '14', 'Accesos Rol', '1');
INSERT INTO `rol_control` VALUES ('53', '4', '15', 'Usuarios', '1');
INSERT INTO `rol_control` VALUES ('54', '4', '16', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('55', '4', '17', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('56', '4', '18', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('57', '4', '19', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('58', '4', '20', 'accesos', '1');
INSERT INTO `rol_control` VALUES ('59', '6', '1', 'Gestión de Información', '1');
INSERT INTO `rol_control` VALUES ('60', '6', '2', 'Maestros', '1');
INSERT INTO `rol_control` VALUES ('61', '6', '3', 'Personas', '1');
INSERT INTO `rol_control` VALUES ('62', '6', '4', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('63', '6', '5', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('64', '6', '6', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('65', '6', '8', 'Accesos', '1');
INSERT INTO `rol_control` VALUES ('66', '6', '9', 'Roles', '1');
INSERT INTO `rol_control` VALUES ('67', '6', '10', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('68', '6', '11', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('69', '6', '12', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('70', '6', '13', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('71', '6', '14', 'Accesos Rol', '1');
INSERT INTO `rol_control` VALUES ('72', '6', '15', 'Usuarios', '1');
INSERT INTO `rol_control` VALUES ('73', '6', '16', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('74', '6', '17', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('75', '6', '18', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('76', '6', '19', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('77', '6', '20', 'accesos', '1');
INSERT INTO `rol_control` VALUES ('78', '9', '1', 'Gestión de Información', '1');
INSERT INTO `rol_control` VALUES ('79', '9', '2', 'Maestros', '1');
INSERT INTO `rol_control` VALUES ('80', '9', '3', 'Personas', '1');
INSERT INTO `rol_control` VALUES ('81', '9', '4', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('82', '9', '5', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('83', '9', '6', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('84', '9', '8', 'Accesos', '1');
INSERT INTO `rol_control` VALUES ('85', '9', '9', 'Roles', '1');
INSERT INTO `rol_control` VALUES ('86', '9', '10', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('87', '9', '11', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('88', '9', '12', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('89', '9', '13', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('90', '9', '14', 'Accesos Rol', '1');
INSERT INTO `rol_control` VALUES ('91', '9', '15', 'Usuarios', '1');
INSERT INTO `rol_control` VALUES ('92', '9', '16', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('93', '9', '17', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('94', '9', '18', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('95', '9', '19', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('96', '9', '20', 'accesos', '1');
INSERT INTO `rol_control` VALUES ('97', '7', '1', 'Gestión de Información', '1');
INSERT INTO `rol_control` VALUES ('98', '7', '2', 'Maestros', '1');
INSERT INTO `rol_control` VALUES ('99', '7', '3', 'Personas', '1');
INSERT INTO `rol_control` VALUES ('100', '7', '4', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('101', '7', '5', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('102', '7', '6', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('103', '7', '8', 'Accesos', '1');
INSERT INTO `rol_control` VALUES ('104', '7', '9', 'Roles', '1');
INSERT INTO `rol_control` VALUES ('105', '7', '10', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('106', '7', '11', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('107', '7', '12', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('108', '7', '13', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('109', '7', '14', 'Accesos Rol', '1');
INSERT INTO `rol_control` VALUES ('110', '7', '15', 'Usuarios', '1');
INSERT INTO `rol_control` VALUES ('111', '7', '16', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('112', '7', '17', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('113', '7', '18', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('114', '7', '19', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('115', '7', '20', 'accesos', '1');
INSERT INTO `rol_control` VALUES ('116', '3', '7', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('117', '1', '21', 'Registros', '1');
INSERT INTO `rol_control` VALUES ('118', '1', '22', 'Areas', '1');
INSERT INTO `rol_control` VALUES ('119', '1', '23', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('120', '1', '24', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('121', '1', '25', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('122', '1', '26', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('123', '1', '27', 'Cargos', '1');
INSERT INTO `rol_control` VALUES ('124', '1', '28', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('125', '1', '29', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('126', '1', '30', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('127', '1', '31', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('128', '1', '32', 'Empleados', '1');
INSERT INTO `rol_control` VALUES ('129', '1', '33', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('130', '1', '34', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('131', '1', '35', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('132', '1', '36', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('133', '1', '37', 'Asignar Nuevo Cargo', '1');
INSERT INTO `rol_control` VALUES ('134', '2', '1', 'Gestión de Información', '1');
INSERT INTO `rol_control` VALUES ('135', '2', '2', 'Personas', '1');
INSERT INTO `rol_control` VALUES ('136', '2', '3', 'Personas', '1');
INSERT INTO `rol_control` VALUES ('137', '2', '4', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('138', '2', '5', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('139', '2', '6', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('140', '2', '7', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('141', '2', '32', 'Empleados', '1');
INSERT INTO `rol_control` VALUES ('142', '2', '33', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('143', '2', '34', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('144', '2', '35', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('145', '2', '36', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('146', '2', '37', 'Asignar Nuevo Cargo', '1');
INSERT INTO `rol_control` VALUES ('147', '2', '8', 'Accesos', '1');
INSERT INTO `rol_control` VALUES ('148', '2', '9', 'Roles', '1');
INSERT INTO `rol_control` VALUES ('149', '2', '10', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('150', '2', '11', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('151', '2', '12', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('152', '2', '13', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('153', '2', '14', 'Accesos Rol', '1');
INSERT INTO `rol_control` VALUES ('154', '2', '15', 'Usuarios', '1');
INSERT INTO `rol_control` VALUES ('155', '2', '16', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('156', '2', '17', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('157', '2', '18', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('158', '2', '19', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('159', '2', '20', 'accesos', '1');
INSERT INTO `rol_control` VALUES ('160', '2', '21', 'Registros', '1');
INSERT INTO `rol_control` VALUES ('161', '2', '22', 'Areas', '1');
INSERT INTO `rol_control` VALUES ('162', '2', '23', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('163', '2', '24', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('164', '2', '25', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('165', '2', '26', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('166', '2', '27', 'Cargos', '1');
INSERT INTO `rol_control` VALUES ('167', '2', '28', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('168', '2', '29', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('169', '2', '30', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('170', '2', '31', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('171', '3', '32', 'Empleados', '1');
INSERT INTO `rol_control` VALUES ('172', '3', '33', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('173', '3', '34', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('174', '3', '35', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('175', '3', '36', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('176', '3', '37', 'Asignar Nuevo Cargo', '1');
INSERT INTO `rol_control` VALUES ('177', '3', '21', 'Registros', '1');
INSERT INTO `rol_control` VALUES ('178', '3', '22', 'Areas', '1');
INSERT INTO `rol_control` VALUES ('179', '3', '23', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('180', '3', '24', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('181', '3', '25', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('182', '3', '26', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('183', '3', '27', 'Cargos', '1');
INSERT INTO `rol_control` VALUES ('184', '3', '28', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('185', '3', '29', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('186', '3', '30', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('187', '3', '31', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('188', '1', '38', 'Clientes', '1');
INSERT INTO `rol_control` VALUES ('189', '1', '39', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('190', '1', '40', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('191', '1', '42', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('192', '1', '43', 'Cambiar Agente Com.', '1');
INSERT INTO `rol_control` VALUES ('193', '2', '38', 'Clientes', '1');
INSERT INTO `rol_control` VALUES ('194', '2', '39', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('195', '2', '40', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('196', '2', '42', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('197', '2', '43', 'Cambiar Agente Com.', '1');
INSERT INTO `rol_control` VALUES ('198', '1', '44', 'Tipo de Prestamos', '1');
INSERT INTO `rol_control` VALUES ('199', '1', '45', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('200', '1', '46', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('201', '1', '47', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('202', '1', '48', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('203', '1', '49', 'Tipo de Monedas', '1');
INSERT INTO `rol_control` VALUES ('204', '1', '50', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('205', '1', '51', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('206', '1', '52', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('207', '1', '53', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('208', '1', '54', 'Tipo de Periodos', '1');
INSERT INTO `rol_control` VALUES ('209', '1', '55', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('210', '1', '56', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('211', '1', '57', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('212', '1', '58', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('213', '1', '59', 'Tipo de Garantias', '1');
INSERT INTO `rol_control` VALUES ('214', '1', '60', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('215', '1', '61', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('216', '1', '62', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('217', '1', '63', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('218', '2', '44', 'Tipo de Prestamos', '1');
INSERT INTO `rol_control` VALUES ('219', '2', '45', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('220', '2', '46', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('221', '2', '47', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('222', '2', '48', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('223', '2', '49', 'Tipo de Monedas', '1');
INSERT INTO `rol_control` VALUES ('224', '2', '50', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('225', '2', '51', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('226', '2', '52', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('227', '2', '53', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('228', '2', '54', 'Tipo de Periodos', '1');
INSERT INTO `rol_control` VALUES ('229', '2', '55', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('230', '2', '56', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('231', '2', '57', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('232', '2', '58', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('233', '2', '59', 'Tipo de Garantias', '1');
INSERT INTO `rol_control` VALUES ('234', '2', '60', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('235', '2', '61', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('236', '2', '62', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('237', '2', '63', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('238', '3', '38', 'Clientes', '1');
INSERT INTO `rol_control` VALUES ('239', '3', '39', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('240', '3', '40', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('241', '3', '42', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('242', '3', '43', 'Cambiar Agente Com.', '1');
INSERT INTO `rol_control` VALUES ('243', '3', '44', 'Tipo de Prestamos', '1');
INSERT INTO `rol_control` VALUES ('244', '3', '45', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('245', '3', '46', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('246', '3', '47', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('247', '3', '48', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('248', '3', '49', 'Tipo de Monedas', '1');
INSERT INTO `rol_control` VALUES ('249', '3', '50', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('250', '3', '51', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('251', '3', '52', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('252', '3', '53', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('253', '3', '54', 'Tipo de Periodos', '1');
INSERT INTO `rol_control` VALUES ('254', '3', '55', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('255', '3', '56', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('256', '3', '57', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('257', '3', '58', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('258', '3', '59', 'Tipo de Garantias', '1');
INSERT INTO `rol_control` VALUES ('259', '3', '60', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('260', '3', '61', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('261', '3', '62', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('262', '3', '63', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('263', '1', '67', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('264', '1', '68', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('265', '2', '64', 'Tipo de Pagos', '1');
INSERT INTO `rol_control` VALUES ('266', '2', '65', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('267', '2', '66', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('268', '2', '67', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('269', '2', '68', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('270', '1', '64', 'Tipo de Pagos', '1');
INSERT INTO `rol_control` VALUES ('271', '1', '65', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('272', '1', '66', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('273', '1', '74', 'Empresas', '1');
INSERT INTO `rol_control` VALUES ('274', '1', '75', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('275', '1', '76', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('276', '1', '77', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('277', '1', '78', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('278', '1', '69', 'Licencias', '1');
INSERT INTO `rol_control` VALUES ('279', '1', '70', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('280', '1', '71', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('281', '1', '72', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('282', '1', '73', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('283', '2', '74', 'Empresas', '1');
INSERT INTO `rol_control` VALUES ('284', '2', '75', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('285', '2', '76', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('286', '2', '77', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('287', '2', '78', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('288', '2', '69', 'Licencias', '1');
INSERT INTO `rol_control` VALUES ('289', '2', '70', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('290', '2', '71', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('291', '2', '72', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('292', '2', '73', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('293', '1', '90', 'Avales', '1');
INSERT INTO `rol_control` VALUES ('294', '1', '91', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('295', '1', '92', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('296', '1', '93', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('297', '1', '94', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('298', '1', '79', 'Tipo de Cambio', '1');
INSERT INTO `rol_control` VALUES ('299', '1', '80', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('300', '1', '81', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('301', '1', '82', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('302', '1', '83', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('303', '1', '84', 'Movimientos', '1');
INSERT INTO `rol_control` VALUES ('304', '1', '85', 'Operaciones', '1');
INSERT INTO `rol_control` VALUES ('305', '1', '86', 'Prestamos', '1');
INSERT INTO `rol_control` VALUES ('306', '1', '87', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('307', '1', '88', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('308', '1', '89', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('309', '2', '90', 'Avales', '1');
INSERT INTO `rol_control` VALUES ('310', '2', '91', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('311', '2', '92', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('312', '2', '93', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('313', '2', '94', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('314', '2', '79', 'Tipo de Cambio', '1');
INSERT INTO `rol_control` VALUES ('315', '2', '80', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('316', '2', '81', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('317', '2', '82', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('318', '2', '83', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('319', '2', '84', 'Movimientos', '1');
INSERT INTO `rol_control` VALUES ('320', '2', '85', 'Operaciones', '1');
INSERT INTO `rol_control` VALUES ('321', '2', '86', 'Prestamos', '1');
INSERT INTO `rol_control` VALUES ('322', '2', '87', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('323', '2', '88', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('324', '2', '89', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('325', '1', '95', 'Vehiculos', '1');
INSERT INTO `rol_control` VALUES ('326', '1', '96', 'Vehiculos', '1');
INSERT INTO `rol_control` VALUES ('327', '1', '97', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('328', '1', '98', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('329', '1', '99', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('330', '1', '100', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('331', '1', '101', 'Clase de Vehiculos', '1');
INSERT INTO `rol_control` VALUES ('332', '1', '102', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('333', '1', '103', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('334', '1', '104', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('335', '1', '105', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('336', '1', '106', 'Tipo de Vehiculo', '1');
INSERT INTO `rol_control` VALUES ('337', '1', '107', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('338', '1', '108', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('339', '1', '109', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('340', '1', '110', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('341', '1', '111', 'Marcas', '1');
INSERT INTO `rol_control` VALUES ('342', '1', '112', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('343', '1', '113', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('344', '1', '114', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('345', '1', '115', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('346', '1', '116', 'Modelos', '1');
INSERT INTO `rol_control` VALUES ('347', '1', '117', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('348', '1', '118', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('349', '1', '119', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('350', '1', '120', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('351', '2', '95', 'Vehiculos', '1');
INSERT INTO `rol_control` VALUES ('352', '2', '96', 'Vehiculos', '1');
INSERT INTO `rol_control` VALUES ('353', '2', '97', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('354', '2', '98', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('355', '2', '99', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('356', '2', '100', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('357', '2', '101', 'Clase de Vehiculos', '1');
INSERT INTO `rol_control` VALUES ('358', '2', '102', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('359', '2', '103', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('360', '2', '104', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('361', '2', '105', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('362', '2', '106', 'Tipo de Vehiculo', '1');
INSERT INTO `rol_control` VALUES ('363', '2', '107', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('364', '2', '108', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('365', '2', '109', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('366', '2', '110', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('367', '2', '111', 'Marcas', '1');
INSERT INTO `rol_control` VALUES ('368', '2', '112', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('369', '2', '113', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('370', '2', '114', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('371', '2', '115', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('372', '2', '116', 'Modelos', '1');
INSERT INTO `rol_control` VALUES ('373', '2', '117', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('374', '2', '118', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('375', '2', '119', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('376', '2', '120', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('377', '2', '122', 'Acuerdo de Pagos', '1');
INSERT INTO `rol_control` VALUES ('378', '2', '123', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('379', '2', '124', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('380', '2', '125', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('381', '2', '126', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('382', '2', '121', 'Detalle', '1');
INSERT INTO `rol_control` VALUES ('383', '3', '77', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('384', '3', '78', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('385', '3', '93', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('386', '3', '94', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('387', '3', '125', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('388', '3', '120', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('389', '3', '84', 'Movimientos', '1');
INSERT INTO `rol_control` VALUES ('390', '3', '85', 'Operaciones', '1');
INSERT INTO `rol_control` VALUES ('391', '3', '86', 'Prestamos', '1');
INSERT INTO `rol_control` VALUES ('392', '3', '87', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('393', '3', '88', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('394', '3', '89', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('395', '3', '121', 'Detalle', '1');
INSERT INTO `rol_control` VALUES ('396', '3', '127', 'Aprobar', '1');
INSERT INTO `rol_control` VALUES ('397', '3', '128', 'Cancelar', '1');
INSERT INTO `rol_control` VALUES ('398', '2', '127', 'Aprobar', '1');
INSERT INTO `rol_control` VALUES ('399', '2', '128', 'Cancelar', '1');
INSERT INTO `rol_control` VALUES ('400', '3', '74', 'Empresas', '1');
INSERT INTO `rol_control` VALUES ('401', '3', '75', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('402', '3', '90', 'Avales', '1');
INSERT INTO `rol_control` VALUES ('403', '3', '91', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('404', '3', '122', 'Acuerdo de Pagos', '1');
INSERT INTO `rol_control` VALUES ('405', '3', '126', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('406', '3', '95', 'Vehiculos', '1');
INSERT INTO `rol_control` VALUES ('407', '3', '96', 'Vehiculos', '1');
INSERT INTO `rol_control` VALUES ('408', '3', '97', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('409', '3', '98', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('410', '3', '99', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('411', '3', '100', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('412', '3', '101', 'Clase de Vehiculos', '1');
INSERT INTO `rol_control` VALUES ('413', '3', '102', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('414', '3', '103', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('415', '3', '104', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('416', '3', '105', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('417', '3', '106', 'Tipo de Vehiculo', '1');
INSERT INTO `rol_control` VALUES ('418', '3', '107', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('419', '3', '108', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('420', '3', '109', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('421', '3', '110', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('422', '3', '111', 'Marcas', '1');
INSERT INTO `rol_control` VALUES ('423', '3', '112', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('424', '3', '113', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('425', '3', '114', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('426', '3', '115', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('427', '3', '116', 'Modelos', '1');
INSERT INTO `rol_control` VALUES ('428', '3', '117', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('429', '3', '118', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('430', '3', '119', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('431', '3', '76', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('432', '3', '92', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('433', '3', '64', 'Tipo de Pagos', '1');
INSERT INTO `rol_control` VALUES ('434', '3', '65', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('435', '3', '66', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('436', '3', '67', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('437', '3', '68', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('438', '3', '69', 'Licencias', '1');
INSERT INTO `rol_control` VALUES ('439', '3', '70', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('440', '3', '71', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('441', '3', '72', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('442', '3', '73', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('443', '3', '79', 'Tipo de Cambio', '1');
INSERT INTO `rol_control` VALUES ('444', '3', '80', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('445', '3', '81', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('446', '3', '82', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('447', '3', '83', 'Eliminar', '1');
INSERT INTO `rol_control` VALUES ('448', '3', '123', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('449', '3', '124', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('450', '3', '130', 'Cred Menor', '1');
INSERT INTO `rol_control` VALUES ('451', '3', '131', 'Listar', '1');
INSERT INTO `rol_control` VALUES ('452', '3', '132', 'Nuevo', '1');
INSERT INTO `rol_control` VALUES ('453', '3', '133', 'Editar', '1');
INSERT INTO `rol_control` VALUES ('454', '3', '134', 'Detalle', '1');
INSERT INTO `rol_control` VALUES ('455', '3', '135', 'Aprobar', '1');
INSERT INTO `rol_control` VALUES ('456', '3', '136', 'Cancelar', '1');

-- ----------------------------
-- Table structure for rubro
-- ----------------------------
DROP TABLE IF EXISTS `rubro`;
CREATE TABLE `rubro` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rubro
-- ----------------------------

-- ----------------------------
-- Table structure for tipo_cambio
-- ----------------------------
DROP TABLE IF EXISTS `tipo_cambio`;
CREATE TABLE `tipo_cambio` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `valor` decimal(5,3) NOT NULL,
  `compra` decimal(5,3) NOT NULL DEFAULT '0.000',
  `venta` decimal(5,3) NOT NULL DEFAULT '0.000',
  `dia` date NOT NULL,
  `estado` smallint(5) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipo_cambio
-- ----------------------------

-- ----------------------------
-- Table structure for tipo_control
-- ----------------------------
DROP TABLE IF EXISTS `tipo_control`;
CREATE TABLE `tipo_control` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipo_control
-- ----------------------------
INSERT INTO `tipo_control` VALUES ('1', 'Modulo', '1');
INSERT INTO `tipo_control` VALUES ('2', 'Menu', '1');
INSERT INTO `tipo_control` VALUES ('3', 'Boton', '1');

-- ----------------------------
-- Table structure for tipo_direccion
-- ----------------------------
DROP TABLE IF EXISTS `tipo_direccion`;
CREATE TABLE `tipo_direccion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipo_direccion
-- ----------------------------
INSERT INTO `tipo_direccion` VALUES ('1', 'Dirección Fiscal', '1');
INSERT INTO `tipo_direccion` VALUES ('2', 'Dirección Domicilio', '1');

-- ----------------------------
-- Table structure for tipo_documento
-- ----------------------------
DROP TABLE IF EXISTS `tipo_documento`;
CREATE TABLE `tipo_documento` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipo_documento
-- ----------------------------
INSERT INTO `tipo_documento` VALUES ('1', 'Dni', '1');
INSERT INTO `tipo_documento` VALUES ('2', 'Ruc', '1');
INSERT INTO `tipo_documento` VALUES ('3', 'Pasaporte', '1');

-- ----------------------------
-- Table structure for tipo_estado
-- ----------------------------
DROP TABLE IF EXISTS `tipo_estado`;
CREATE TABLE `tipo_estado` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `estado` smallint(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipo_estado
-- ----------------------------
INSERT INTO `tipo_estado` VALUES ('1', 'Pendiente', '1');
INSERT INTO `tipo_estado` VALUES ('2', 'Aprobado', '1');
INSERT INTO `tipo_estado` VALUES ('3', 'Cancelado', '1');
INSERT INTO `tipo_estado` VALUES ('4', 'Eliminado', '0');

-- ----------------------------
-- Table structure for tipo_garantia
-- ----------------------------
DROP TABLE IF EXISTS `tipo_garantia`;
CREATE TABLE `tipo_garantia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipo_garantia
-- ----------------------------
INSERT INTO `tipo_garantia` VALUES ('1', 'VEHICULAR', 'Vehículos, Camionetas, Autos, Camiones, etc.', '1');
INSERT INTO `tipo_garantia` VALUES ('2', 'VEHICULO MENOR', 'Motocicletas, Trimotos, Cuatrimotos, Maquinaria Menor, etc.', '1');
INSERT INTO `tipo_garantia` VALUES ('3', 'ELECTRO', 'Electrodomésticos, linea blanca, linea marrón, telefonía, etc.', '1');
INSERT INTO `tipo_garantia` VALUES ('4', 'ORO', 'Joyas', '1');
INSERT INTO `tipo_garantia` VALUES ('5', 'INMUEBLE', 'Terrenos, Viviendas, Departamentos, Locales, etc.', '1');

-- ----------------------------
-- Table structure for tipo_moneda
-- ----------------------------
DROP TABLE IF EXISTS `tipo_moneda`;
CREATE TABLE `tipo_moneda` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `simbolo` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipo_moneda
-- ----------------------------
INSERT INTO `tipo_moneda` VALUES ('1', 'Soles', 'S/.', '1');
INSERT INTO `tipo_moneda` VALUES ('2', 'Dolares', '$', '1');

-- ----------------------------
-- Table structure for tipo_pago
-- ----------------------------
DROP TABLE IF EXISTS `tipo_pago`;
CREATE TABLE `tipo_pago` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_prestamo_id` int(10) unsigned NOT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `estado` smallint(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `tipo_pago_tipo_prestamo_id_foreign` (`tipo_prestamo_id`),
  CONSTRAINT `tipo_pago_tipo_prestamo_id_foreign` FOREIGN KEY (`tipo_prestamo_id`) REFERENCES `tipo_prestamo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipo_pago
-- ----------------------------
INSERT INTO `tipo_pago` VALUES ('1', '1', 'Cuota Fija', 'Cuota Fija', '1');
INSERT INTO `tipo_pago` VALUES ('2', '1', 'Couta Libre', 'Cuota Fija', '1');
INSERT INTO `tipo_pago` VALUES ('3', '4', 'Cuota Fija', 'Cuota Fija', '1');
INSERT INTO `tipo_pago` VALUES ('4', '2', 'Cuota Fija', 'Cuota Fija', '1');
INSERT INTO `tipo_pago` VALUES ('9', '3', 'MENSUAL', 'MENSUAL', '1');
INSERT INTO `tipo_pago` VALUES ('10', '4', 'Cuota Libre', 'Libre', '1');
INSERT INTO `tipo_pago` VALUES ('11', '5', 'Cuota Libre', 'Libre', '1');

-- ----------------------------
-- Table structure for tipo_periodo
-- ----------------------------
DROP TABLE IF EXISTS `tipo_periodo`;
CREATE TABLE `tipo_periodo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `rango` int(11) DEFAULT NULL,
  `descripcion` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipo_periodo
-- ----------------------------
INSERT INTO `tipo_periodo` VALUES ('1', 'Diario', '1', 'Diario', '1');
INSERT INTO `tipo_periodo` VALUES ('2', 'Semanal', '7', 'Semanal', '1');
INSERT INTO `tipo_periodo` VALUES ('3', 'Quincenal', '15', 'Quincenal', '1');
INSERT INTO `tipo_periodo` VALUES ('4', 'Mensual', '30', 'Mensual', '1');
INSERT INTO `tipo_periodo` VALUES ('5', 'Trimestral', '90', 'Trimestral', '1');
INSERT INTO `tipo_periodo` VALUES ('6', 'Anual', '365', 'Anual', '1');

-- ----------------------------
-- Table structure for tipo_prestamo
-- ----------------------------
DROP TABLE IF EXISTS `tipo_prestamo`;
CREATE TABLE `tipo_prestamo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipo_prestamo
-- ----------------------------
INSERT INTO `tipo_prestamo` VALUES ('1', 'ASCVEH', 'Crédito Asistencia en Compra Vehicular', '1');
INSERT INTO `tipo_prestamo` VALUES ('2', 'GARVEH  CON CUSTODIA', 'Crédito Garantía Vehicular Con Custodia', '1');
INSERT INTO `tipo_prestamo` VALUES ('3', 'CREDHIPOT', 'Crédito Hipotecario', '1');
INSERT INTO `tipo_prestamo` VALUES ('4', 'CREDMENOR', 'Credito Menor', '1');
INSERT INTO `tipo_prestamo` VALUES ('5', 'GARVEH SIN CUSTODIA', 'Garantía Vehicular Sin Custodia', '1');

-- ----------------------------
-- Table structure for tipo_relacion
-- ----------------------------
DROP TABLE IF EXISTS `tipo_relacion`;
CREATE TABLE `tipo_relacion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipo_relacion
-- ----------------------------
INSERT INTO `tipo_relacion` VALUES ('1', 'Usuaraio', '1');
INSERT INTO `tipo_relacion` VALUES ('2', 'Persona  - Empresa', '1');
INSERT INTO `tipo_relacion` VALUES ('3', 'Cliente', '1');
INSERT INTO `tipo_relacion` VALUES ('4', 'Empleado', '1');
INSERT INTO `tipo_relacion` VALUES ('5', 'Contacto', '1');

-- ----------------------------
-- Table structure for tipo_telefono
-- ----------------------------
DROP TABLE IF EXISTS `tipo_telefono`;
CREATE TABLE `tipo_telefono` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipo_telefono
-- ----------------------------
INSERT INTO `tipo_telefono` VALUES ('1', 'Telefono Fijo', '1');
INSERT INTO `tipo_telefono` VALUES ('2', 'Celular', '1');

-- ----------------------------
-- Table structure for tipo_vehiculo
-- ----------------------------
DROP TABLE IF EXISTS `tipo_vehiculo`;
CREATE TABLE `tipo_vehiculo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clase_vehiculo_id` int(10) unsigned NOT NULL,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `estado` smallint(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `tipo_vehiculo_clase_vehiculo_id_foreign` (`clase_vehiculo_id`),
  CONSTRAINT `tipo_vehiculo_clase_vehiculo_id_foreign` FOREIGN KEY (`clase_vehiculo_id`) REFERENCES `clase_vehiculo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipo_vehiculo
-- ----------------------------
INSERT INTO `tipo_vehiculo` VALUES ('1', '1', 'HATCHBACK', '5 Puertas', '1');
INSERT INTO `tipo_vehiculo` VALUES ('2', '1', 'COUPE', '3 Puertas', '1');
INSERT INTO `tipo_vehiculo` VALUES ('3', '1', 'SEDAN', '4 PUERTAS', '1');

-- ----------------------------
-- Table structure for tipo_web
-- ----------------------------
DROP TABLE IF EXISTS `tipo_web`;
CREATE TABLE `tipo_web` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `estado` smallint(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipo_web
-- ----------------------------
INSERT INTO `tipo_web` VALUES ('1', 'Web', '1');
INSERT INTO `tipo_web` VALUES ('2', 'Facebook', '1');
INSERT INTO `tipo_web` VALUES ('3', 'Twitter', '1');
INSERT INTO `tipo_web` VALUES ('4', 'Gmail', '1');
INSERT INTO `tipo_web` VALUES ('5', 'Linkedin', '1');
INSERT INTO `tipo_web` VALUES ('6', 'Git Hub', '1');

-- ----------------------------
-- Table structure for transaccion
-- ----------------------------
DROP TABLE IF EXISTS `transaccion`;
CREATE TABLE `transaccion` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `ope_codigo` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sql` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `transaccion_user_id_foreign` (`user_id`),
  CONSTRAINT `transaccion_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of transaccion
-- ----------------------------

-- ----------------------------
-- Table structure for ubigeo
-- ----------------------------
DROP TABLE IF EXISTS `ubigeo`;
CREATE TABLE `ubigeo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `ubigeo` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo_ubigeo_id` int(10) unsigned NOT NULL DEFAULT '1',
  `pais_id` int(10) unsigned NOT NULL DEFAULT '1',
  `latitud` decimal(16,12) DEFAULT NULL,
  `longitud` decimal(16,12) DEFAULT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ubigeo_codigo_index` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=2053 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ubigeo
-- ----------------------------
INSERT INTO `ubigeo` VALUES ('1', '250401', 'Purus', 'Purus, Purus, Ucayali', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2', '250400', 'Purus', 'Purus, Ucayali', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('3', '250303', 'Curimana', 'Curimana, Padre Abad, Ucayali', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('4', '250302', 'Irazola', 'Irazola, Padre Abad, Ucayali', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('5', '250301', 'Padre Abad', 'Padre Abad, Padre Abad, Ucayali', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('6', '250300', 'Padre Abad', 'Padre Abad, Ucayali', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('7', '250204', 'Yurua', 'Yurua, Atalaya, Ucayali', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('8', '250203', 'Tahuania', 'Tahuania, Atalaya, Ucayali', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('9', '250202', 'Sepahua', 'Sepahua, Atalaya, Ucayali', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('10', '250201', 'Raymondi', 'Raymondi, Atalaya, Ucayali', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('11', '250200', 'Atalaya', 'Atalaya, Ucayali', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('12', '250106', 'Nueva Requena', 'Nueva Requena, Coronel Portillo, Ucayali', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('13', '250105', 'Yarinacocha', 'Yarinacocha, Coronel Portillo, Ucayali', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('14', '250104', 'Masisea', 'Masisea, Coronel Portillo, Ucayali', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('15', '250103', 'Iparia', 'Iparia, Coronel Portillo, Ucayali', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('16', '250102', 'Campoverde', 'Campoverde, Coronel Portillo, Ucayali', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('17', '250101', 'Calleria', 'Calleria, Coronel Portillo, Ucayali', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('18', '250100', 'Coronel Portillo', 'Coronel Portillo, Ucayali', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('19', '250000', 'Ucayali', 'Ucayali', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('20', '240304', 'Papayal', 'Papayal, Zarumilla, Tumbes', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('21', '240303', 'Matapalo', 'Matapalo, Zarumilla, Tumbes', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('22', '240302', 'Aguas Verdes', 'Aguas Verdes, Zarumilla, Tumbes', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('23', '240301', 'Zarumilla', 'Zarumilla, Zarumilla, Tumbes', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('24', '240300', 'Zarumilla', 'Zarumilla, Tumbes', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('25', '240202', 'Casitas', 'Casitas, Contralmirante Villar, Tumbes', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('26', '240201', 'Zorritos', 'Zorritos, Contralmirante Villar, Tumbes', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('27', '240200', 'Contralmirante Villar', 'Contralmirante Villar, Tumbes', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('28', '240106', 'San Juan de la Virgen', 'San Juan de la Virgen, Tumbes, Tumbes', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('29', '240105', 'San Jacinto', 'San Jacinto, Tumbes, Tumbes', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('30', '240104', 'Pampas de Hospital', 'Pampas de Hospital, Tumbes, Tumbes', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('31', '240103', 'La Cruz', 'La Cruz, Tumbes, Tumbes', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('32', '240102', 'Corrales', 'Corrales, Tumbes, Tumbes', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('33', '240101', 'Tumbes', 'Tumbes, Tumbes, Tumbes', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('34', '240100', 'Tumbes', 'Tumbes, Tumbes', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('35', '240000', 'Tumbes', 'Tumbes', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('36', '230408', 'Ticaco', 'Ticaco, Tarata, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('37', '230407', 'Tarucachi', 'Tarucachi, Tarata, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('38', '230406', 'Susapaya', 'Susapaya, Tarata, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('39', '230405', 'Sitajara', 'Sitajara, Tarata, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('40', '230404', 'Estique-Pampa', 'Estique-Pampa, Tarata, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('41', '230403', 'Estique', 'Estique, Tarata, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('42', '230402', 'Chucatamani', 'Chucatamani, Tarata, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('43', '230401', 'Tarata', 'Tarata, Tarata, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('44', '230400', 'Tarata', 'Tarata, Tacna', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('45', '230303', 'Ite', 'Ite, Jorge Basadre, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('46', '230302', 'Ilabaya', 'Ilabaya, Jorge Basadre, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('47', '230301', 'Locumba', 'Locumba, Jorge Basadre, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('48', '230300', 'Jorge Basadre', 'Jorge Basadre, Tacna', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('49', '230206', 'Quilahuani', 'Quilahuani, Candarave, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('50', '230205', 'Huanuara', 'Huanuara, Candarave, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('51', '230204', 'Curibaya', 'Curibaya, Candarave, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('52', '230203', 'Camilaca', 'Camilaca, Candarave, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('53', '230202', 'Cairani', 'Cairani, Candarave, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('54', '230201', 'Candarave', 'Candarave, Candarave, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('55', '230200', 'Candarave', 'Candarave, Tacna', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('56', '230110', 'Cor Gregorio Albarracín', 'Cor Gregorio Albarracín, Tacna, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('57', '230109', 'Sama', 'Sama, Tacna, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('58', '230108', 'Pocollay', 'Pocollay, Tacna, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('59', '230107', 'Palca', 'Palca, Tacna, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('60', '230106', 'Pachia', 'Pachia, Tacna, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('61', '230105', 'Inclan', 'Inclan, Tacna, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('62', '230104', 'Ciudad Nueva', 'Ciudad Nueva, Tacna, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('63', '230103', 'Calana', 'Calana, Tacna, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('64', '230102', 'Alto de la Alianza', 'Alto de la Alianza, Tacna, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('65', '230101', 'Tacna', 'Tacna, Tacna, Tacna', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('66', '230100', 'Tacna', 'Tacna, Tacna', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('67', '230000', 'Tacna', 'Tacna', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('68', '221005', 'Uchiza', 'Uchiza, Tocache, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('69', '221004', 'Shunte', 'Shunte, Tocache, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('70', '221003', 'Polvora', 'Polvora, Tocache, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('71', '221002', 'Nuevo Progreso', 'Nuevo Progreso, Tocache, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('72', '221001', 'Tocache', 'Tocache, Tocache, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('73', '221000', 'Tocache', 'Tocache, San Martin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('74', '220914', 'Shapaja', 'Shapaja, San Martin, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('75', '220913', 'Sauce', 'Sauce, San Martin, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('76', '220912', 'San Antonio', 'San Antonio, San Martin, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('77', '220911', 'Papaplaya', 'Papaplaya, San Martin, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('78', '220910', 'Morales', 'Morales, San Martin, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('79', '220909', 'La Banda de Shilcayo', 'La Banda de Shilcayo, San Martin, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('80', '220908', 'Juan Guerra', 'Juan Guerra, San Martin, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('81', '220907', 'Huimbayoc', 'Huimbayoc, San Martin, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('82', '220906', 'El Porvenir', 'El Porvenir, San Martin, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('83', '220905', 'Chipurana', 'Chipurana, San Martin, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('84', '220904', 'Chazuta', 'Chazuta, San Martin, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('85', '220903', 'Cacatachi', 'Cacatachi, San Martin, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('86', '220902', 'Alberto Leveau', 'Alberto Leveau, San Martin, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('87', '220901', 'Tarapoto', 'Tarapoto, San Martin, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('88', '220900', 'San Martin', 'San Martin, San Martin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('89', '220809', 'Yuracyacu', 'Yuracyacu, Rioja, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('90', '220808', 'Yorongos', 'Yorongos, Rioja, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('91', '220807', 'San Fernando', 'San Fernando, Rioja, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('92', '220806', 'Posic', 'Posic, Rioja, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('93', '220805', 'Pardo Miguel', 'Pardo Miguel, Rioja, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('94', '220804', 'Nueva Cajamarca', 'Nueva Cajamarca, Rioja, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('95', '220803', 'Elias Soplin Vargas', 'Elias Soplin Vargas, Rioja, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('96', '220802', 'Awajun', 'Awajun, Rioja, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('97', '220801', 'Rioja', 'Rioja, Rioja, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('98', '220800', 'Rioja', 'Rioja, San Martin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('99', '220710', 'Tres Unidos', 'Tres Unidos, Picota, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('100', '220709', 'Tingo de Ponasa', 'Tingo de Ponasa, Picota, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('101', '220708', 'Shamboyacu', 'Shamboyacu, Picota, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('102', '220707', 'San Hilarion', 'San Hilarion, Picota, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('103', '220706', 'San Cristobal', 'San Cristobal, Picota, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('104', '220705', 'Pucacaca', 'Pucacaca, Picota, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('105', '220704', 'Pilluana', 'Pilluana, Picota, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('106', '220703', 'Caspisapa', 'Caspisapa, Picota, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('107', '220702', 'Buenos Aires', 'Buenos Aires, Picota, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('108', '220701', 'Picota', 'Picota, Picota, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('109', '220700', 'Picota', 'Picota, San Martin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('110', '220605', 'Pajarillo', 'Pajarillo, Mariscal Caceres, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('111', '220604', 'Pachiza', 'Pachiza, Mariscal Caceres, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('112', '220603', 'Huicungo', 'Huicungo, Mariscal Caceres, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('113', '220602', 'Campanilla', 'Campanilla, Mariscal Caceres, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('114', '220601', 'Juanjui', 'Juanjui, Mariscal Caceres, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('115', '220600', 'Mariscal Caceres', 'Mariscal Caceres, San Martin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('116', '220511', 'Zapatero', 'Zapatero, Lamas, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('117', '220510', 'Tabalosos', 'Tabalosos, Lamas, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('118', '220509', 'Shanao', 'Shanao, Lamas, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('119', '220508', 'San Roque de Cumbaza', 'San Roque de Cumbaza, Lamas, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('120', '220507', 'Rumisapa', 'Rumisapa, Lamas, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('121', '220506', 'Pinto Recodo', 'Pinto Recodo, Lamas, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('122', '220505', 'Cuqumbuqui', 'Cuqumbuqui, Lamas, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('123', '220504', 'Caynarachi', 'Caynarachi, Lamas, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('124', '220503', 'Barranquita', 'Barranquita, Lamas, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('125', '220502', 'Alonso de Alvarado', 'Alonso de Alvarado, Lamas, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('126', '220501', 'Lamas', 'Lamas, Lamas, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('127', '220500', 'Lamas', 'Lamas, San Martin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('128', '220406', 'Tingo de Saposoa', 'Tingo de Saposoa, Huallaga, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('129', '220405', 'Sacanche', 'Sacanche, Huallaga, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('130', '220404', 'Piscoyacu', 'Piscoyacu, Huallaga, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('131', '220403', 'El Eslabon', 'El Eslabon, Huallaga, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('132', '220402', 'Alto Saposoa', 'Alto Saposoa, Huallaga, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('133', '220401', 'Saposoa', 'Saposoa, Huallaga, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('134', '220400', 'Huallaga', 'Huallaga, San Martin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('135', '220305', 'Shatoja', 'Shatoja, El Dorado, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('136', '220304', 'Santa Rosa', 'Santa Rosa, El Dorado, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('137', '220303', 'San Martin', 'San Martin, El Dorado, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('138', '220302', 'Agua Blanca', 'Agua Blanca, El Dorado, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('139', '220301', 'San Jose de Sisa', 'San Jose de Sisa, El Dorado, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('140', '220300', 'El Dorado', 'El Dorado, San Martin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('141', '220206', 'San Rafael', 'San Rafael, Bellavista, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('142', '220205', 'San Pablo', 'San Pablo, Bellavista, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('143', '220204', 'Huallaga', 'Huallaga, Bellavista, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('144', '220203', 'Bajo Biavo', 'Bajo Biavo, Bellavista, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('145', '220202', 'Alto Biavo', 'Alto Biavo, Bellavista, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('146', '220201', 'Bellavista', 'Bellavista, Bellavista, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('147', '220200', 'Bellavista', 'Bellavista, San Martin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('148', '220106', 'Yantalo', 'Yantalo, Moyobamba, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('149', '220105', 'Soritor', 'Soritor, Moyobamba, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('150', '220104', 'Jepelacio', 'Jepelacio, Moyobamba, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('151', '220103', 'Habana', 'Habana, Moyobamba, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('152', '220102', 'Calzada', 'Calzada, Moyobamba, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('153', '220101', 'Moyobamba', 'Moyobamba, Moyobamba, San Martin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('154', '220100', 'Moyobamba', 'Moyobamba, San Martin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('155', '220000', 'San Martin', 'San Martin', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('156', '211307', 'Unicachi', 'Unicachi, Yunguyo, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('157', '211306', 'Tinicachi', 'Tinicachi, Yunguyo, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('158', '211305', 'Ollaraya', 'Ollaraya, Yunguyo, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('159', '211304', 'Cuturapi', 'Cuturapi, Yunguyo, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('160', '211303', 'Copani', 'Copani, Yunguyo, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('161', '211302', 'Anapia', 'Anapia, Yunguyo, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('162', '211301', 'Yunguyo', 'Yunguyo, Yunguyo, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('163', '211300', 'Yunguyo', 'Yunguyo, Puno', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('164', '211209', 'Alto Inambari', 'Alto Inambari, Sandia, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('165', '211208', 'Yanahuaya', 'Yanahuaya, Sandia, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('166', '211207', 'San Juan del Oro', 'San Juan del Oro, Sandia, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('167', '211206', 'Quiaca', 'Quiaca, Sandia, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('168', '211205', 'Phara', 'Phara, Sandia, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('169', '211204', 'Patambuco', 'Patambuco, Sandia, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('170', '211203', 'Limbani', 'Limbani, Sandia, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('171', '211202', 'Cuyocuyo', 'Cuyocuyo, Sandia, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('172', '211201', 'Sandia', 'Sandia, Sandia, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('173', '211200', 'Sandia', 'Sandia, Puno', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('174', '211104', 'Caracoto', 'Caracoto, San Roman, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('175', '211103', 'Cabanillas', 'Cabanillas, San Roman, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('176', '211102', 'Cabana', 'Cabana, San Roman, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('177', '211101', 'Juliaca', 'Juliaca, San Roman, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('178', '211100', 'San Roman', 'San Roman, Puno', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('179', '211005', 'Sina', 'Sina, San Antonio de Putina, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('180', '211004', 'Quilcapuncu', 'Quilcapuncu, San Antonio de Putina, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('181', '211003', 'Pedro Vilca Apaza', 'Pedro Vilca Apaza, San Antonio de Putina, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('182', '211002', 'Ananea', 'Ananea, San Antonio de Putina, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('183', '211001', 'Putina', 'Putina, San Antonio de Putina, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('184', '211000', 'San Antonio de Putina', 'San Antonio de Putina, Puno', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('185', '210904', 'Tilali', 'Tilali, Moho, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('186', '210903', 'Huayrapata', 'Huayrapata, Moho, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('187', '210902', 'Conima', 'Conima, Moho, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('188', '210901', 'Moho', 'Moho, Moho, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('189', '210900', 'Moho', 'Moho, Puno', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('190', '210809', 'Umachiri', 'Umachiri, Melgar, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('191', '210808', 'Santa Rosa', 'Santa Rosa, Melgar, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('192', '210807', 'Orurillo', 'Orurillo, Melgar, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('193', '210806', 'Nuñoa', 'Nuñoa, Melgar, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('194', '210805', 'Macari', 'Macari, Melgar, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('195', '210804', 'Llalli', 'Llalli, Melgar, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('196', '210803', 'Cupi', 'Cupi, Melgar, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('197', '210802', 'Antauta', 'Antauta, Melgar, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('198', '210801', 'Ayaviri', 'Ayaviri, Melgar, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('199', '210800', 'Melgar', 'Melgar, Puno', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('200', '210710', 'Vilavila', 'Vilavila, Lampa, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('201', '210709', 'Santa Lucia', 'Santa Lucia, Lampa, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('202', '210708', 'Pucara', 'Pucara, Lampa, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('203', '210707', 'Paratia', 'Paratia, Lampa, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('204', '210706', 'Palca', 'Palca, Lampa, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('205', '210705', 'Ocuviri', 'Ocuviri, Lampa, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('206', '210704', 'Nicasio', 'Nicasio, Lampa, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('207', '210703', 'Calapuja', 'Calapuja, Lampa, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('208', '210702', 'Cabanilla', 'Cabanilla, Lampa, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('209', '210701', 'Lampa', 'Lampa, Lampa, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('210', '210700', 'Lampa', 'Lampa, Puno', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('211', '210608', 'Vilque Chico', 'Vilque Chico, Huancane, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('212', '210607', 'Taraco', 'Taraco, Huancane, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('213', '210606', 'Rosaspata', 'Rosaspata, Huancane, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('214', '210605', 'Pusi', 'Pusi, Huancane, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('215', '210604', 'Inchupalla', 'Inchupalla, Huancane, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('216', '210603', 'Huatasani', 'Huatasani, Huancane, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('217', '210602', 'Cojata', 'Cojata, Huancane, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('218', '210601', 'Huancane', 'Huancane, Huancane, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('219', '210600', 'Huancane', 'Huancane, Puno', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('220', '210505', 'Conduriri', 'Conduriri, El Collao, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('221', '210504', 'Santa Rosa', 'Santa Rosa, El Collao, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('222', '210503', 'Pilcuyo', 'Pilcuyo, El Collao, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('223', '210502', 'Capazo', 'Capazo, El Collao, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('224', '210501', 'Ilave', 'Ilave, El Collao, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('225', '210500', 'El Collao', 'El Collao, Puno', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('226', '210407', 'Zepita', 'Zepita, Chucuito, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('227', '210406', 'Pomata', 'Pomata, Chucuito, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('228', '210405', 'Pisacoma', 'Pisacoma, Chucuito, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('229', '210404', 'Kelluyo', 'Kelluyo, Chucuito, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('230', '210403', 'Huacullani', 'Huacullani, Chucuito, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('231', '210402', 'Desaguadero', 'Desaguadero, Chucuito, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('232', '210401', 'Juli', 'Juli, Chucuito, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('233', '210400', 'Chucuito', 'Chucuito, Puno', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('234', '210310', 'Usicayos', 'Usicayos, Carabaya, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('235', '210309', 'San Gaban', 'San Gaban, Carabaya, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('236', '210308', 'Ollachea', 'Ollachea, Carabaya, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('237', '210307', 'Ituata', 'Ituata, Carabaya, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('238', '210306', 'Crucero', 'Crucero, Carabaya, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('239', '210305', 'Corani', 'Corani, Carabaya, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('240', '210304', 'Coasa', 'Coasa, Carabaya, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('241', '210303', 'Ayapata', 'Ayapata, Carabaya, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('242', '210302', 'Ajoyani', 'Ajoyani, Carabaya, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('243', '210301', 'Macusani', 'Macusani, Carabaya, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('244', '210300', 'Carabaya', 'Carabaya, Puno', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('245', '210215', 'Tirapata', 'Tirapata, Azangaro, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('246', '210214', 'Santiago de Pupuja', 'Santiago de Pupuja, Azangaro, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('247', '210213', 'San Juan de Salinas', 'San Juan de Salinas, Azangaro, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('248', '210212', 'San Jose', 'San Jose, Azangaro, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('249', '210211', 'San Anton', 'San Anton, Azangaro, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('250', '210210', 'Saman', 'Saman, Azangaro, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('251', '210209', 'Potoni', 'Potoni, Azangaro, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('252', '210208', 'Muñani', 'Muñani, Azangaro, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('253', '210207', 'Jose Domingo Choquehuanca', 'Jose Domingo Choquehuanca, Azangaro, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('254', '210206', 'Chupa', 'Chupa, Azangaro, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('255', '210205', 'Caminaca', 'Caminaca, Azangaro, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('256', '210204', 'Asillo', 'Asillo, Azangaro, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('257', '210203', 'Arapa', 'Arapa, Azangaro, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('258', '210202', 'Achaya', 'Achaya, Azangaro, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('259', '210201', 'Azangaro', 'Azangaro, Azangaro, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('260', '210200', 'Azangaro', 'Azangaro, Puno', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('261', '210115', 'Vilque', 'Vilque, Puno, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('262', '210114', 'Tiquillaca', 'Tiquillaca, Puno, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('263', '210113', 'San Antonio', 'San Antonio, Puno, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('264', '210112', 'Plateria', 'Plateria, Puno, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('265', '210111', 'Pichacani', 'Pichacani, Puno, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('266', '210110', 'Paucarcolla', 'Paucarcolla, Puno, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('267', '210109', 'Mañazo', 'Mañazo, Puno, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('268', '210108', 'Huata', 'Huata, Puno, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('269', '210107', 'Coata', 'Coata, Puno, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('270', '210106', 'Chucuito', 'Chucuito, Puno, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('271', '210105', 'Capachica', 'Capachica, Puno, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('272', '210104', 'Atuncolla', 'Atuncolla, Puno, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('273', '210103', 'Amantani', 'Amantani, Puno, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('274', '210102', 'Acora', 'Acora, Puno, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('275', '210101', 'Puno', 'Puno, Puno, Puno', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('276', '210100', 'Puno', 'Puno, Puno', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('277', '210000', 'Puno', 'Puno', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('278', '200806', 'Rinconada Llicuar', 'Rinconada Llicuar, Sechura, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('279', '200805', 'Vice', 'Vice, Sechura, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('280', '200804', 'Cristo Nos Valga', 'Cristo Nos Valga, Sechura, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('281', '200803', 'Bernal', 'Bernal, Sechura, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('282', '200802', 'Bellavista de la Union', 'Bellavista de la Union, Sechura, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('283', '200801', 'Sechura', 'Sechura, Sechura, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('284', '200800', 'Sechura', 'Sechura, Piura', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('285', '200706', 'Mancora', 'Mancora, Talara, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('286', '200705', 'Los Organos', 'Los Organos, Talara, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('287', '200704', 'Lobitos', 'Lobitos, Talara, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('288', '200703', 'La Brea', 'La Brea, Talara, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('289', '200702', 'El Alto', 'El Alto, Talara, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('290', '200701', 'Pariñas', 'Pariñas, Talara, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('291', '200700', 'Talara', 'Talara, Piura', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('292', '200608', 'Salitral', 'Salitral, Sullana, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('293', '200607', 'Querecotillo', 'Querecotillo, Sullana, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('294', '200606', 'Miguel Checa', 'Miguel Checa, Sullana, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('295', '200605', 'Marcavelica', 'Marcavelica, Sullana, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('296', '200604', 'Lancones', 'Lancones, Sullana, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('297', '200603', 'Ignacio Escudero', 'Ignacio Escudero, Sullana, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('298', '200602', 'Bellavista', 'Bellavista, Sullana, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('299', '200601', 'Sullana', 'Sullana, Sullana, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('300', '200600', 'Sullana', 'Sullana, Piura', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('301', '200507', 'Vichayal', 'Vichayal, Paita, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('302', '200506', 'Tamarindo', 'Tamarindo, Paita, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('303', '200505', 'La Huaca', 'La Huaca, Paita, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('304', '200504', 'Colan', 'Colan, Paita, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('305', '200503', 'Arenal', 'Arenal, Paita, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('306', '200502', 'Amotape', 'Amotape, Paita, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('307', '200501', 'Paita', 'Paita, Paita, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('308', '200500', 'Paita', 'Paita, Piura', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('309', '200410', 'Yamango', 'Yamango, Morropon, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('310', '200409', 'Santo Domingo', 'Santo Domingo, Morropon, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('311', '200408', 'Santa Catalina de Mossa', 'Santa Catalina de Mossa, Morropon, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('312', '200407', 'San Juan de Bigote', 'San Juan de Bigote, Morropon, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('313', '200406', 'Salitral', 'Salitral, Morropon, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('314', '200405', 'Morropon', 'Morropon, Morropon, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('315', '200404', 'La Matanza', 'La Matanza, Morropon, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('316', '200403', 'Chalaco', 'Chalaco, Morropon, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('317', '200402', 'Buenos Aires', 'Buenos Aires, Morropon, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('318', '200401', 'Chulucanas', 'Chulucanas, Morropon, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('319', '200400', 'Morropon', 'Morropon, Piura', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('320', '200308', 'Sondorillo', 'Sondorillo, Huancabamba, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('321', '200307', 'Sondor', 'Sondor, Huancabamba, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('322', '200306', 'San Miguel de El Faique', 'San Miguel de El Faique, Huancabamba, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('323', '200305', 'Lalaquiz', 'Lalaquiz, Huancabamba, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('324', '200304', 'Huarmaca', 'Huarmaca, Huancabamba, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('325', '200303', 'El Carmen de la Frontera', 'El Carmen de la Frontera, Huancabamba, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('326', '200302', 'Canchaque', 'Canchaque, Huancabamba, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('327', '200301', 'Huancabamba', 'Huancabamba, Huancabamba, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('328', '200300', 'Huancabamba', 'Huancabamba, Piura', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('329', '200210', 'Suyo', 'Suyo, Ayabaca, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('330', '200209', 'Sicchez', 'Sicchez, Ayabaca, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('331', '200208', 'Sapillica', 'Sapillica, Ayabaca, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('332', '200207', 'Paimas', 'Paimas, Ayabaca, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('333', '200206', 'Pacaipampa', 'Pacaipampa, Ayabaca, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('334', '200205', 'Montero', 'Montero, Ayabaca, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('335', '200204', 'Lagunas', 'Lagunas, Ayabaca, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('336', '200203', 'Jilili', 'Jilili, Ayabaca, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('337', '200202', 'Frias', 'Frias, Ayabaca, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('338', '200201', 'Ayabaca', 'Ayabaca, Ayabaca, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('339', '200200', 'Ayabaca', 'Ayabaca, Piura', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('340', '200114', 'Tambo Grande', 'Tambo Grande, Piura, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('341', '200111', 'Las Lomas', 'Las Lomas, Piura, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('342', '200110', 'La Union', 'La Union, Piura, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('343', '200109', 'La Arena', 'La Arena, Piura, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('344', '200108', 'El Tallan', 'El Tallan, Piura, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('345', '200107', 'Cura Mori', 'Cura Mori, Piura, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('346', '200105', 'Catacaos', 'Catacaos, Piura, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('347', '200104', 'Castilla', 'Castilla, Piura, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('348', '200101', 'Piura', 'Piura, Piura, Piura', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('349', '200100', 'Piura', 'Piura, Piura', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('350', '200000', 'Piura', 'Piura', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('351', '190307', 'Villa Rica', 'Villa Rica, Oxapampa, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('352', '190306', 'Puerto Bermudez', 'Puerto Bermudez, Oxapampa, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('353', '190305', 'Pozuzo', 'Pozuzo, Oxapampa, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('354', '190304', 'Palcazu', 'Palcazu, Oxapampa, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('355', '190303', 'Huancabamba', 'Huancabamba, Oxapampa, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('356', '190302', 'Chontabamba', 'Chontabamba, Oxapampa, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('357', '190301', 'Oxapampa', 'Oxapampa, Oxapampa, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('358', '190300', 'Oxapampa', 'Oxapampa, Pasco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('359', '190208', 'Vilcabamba', 'Vilcabamba, Daniel Alcides Carrion, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('360', '190207', 'Tapuc', 'Tapuc, Daniel Alcides Carrion, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('361', '190206', 'Santa Ana de Tusi', 'Santa Ana de Tusi, Daniel Alcides Carrion, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('362', '190205', 'San Pedro de Pillao', 'San Pedro de Pillao, Daniel Alcides Carrion, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('363', '190204', 'Paucar', 'Paucar, Daniel Alcides Carrion, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('364', '190203', 'Goyllarisquizga', 'Goyllarisquizga, Daniel Alcides Carrion, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('365', '190202', 'Chacayan', 'Chacayan, Daniel Alcides Carrion, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('366', '190201', 'Yanahuanca', 'Yanahuanca, Daniel Alcides Carrion, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('367', '190200', 'Daniel Alcides Carrion', 'Daniel Alcides Carrion, Pasco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('368', '190113', 'Yanacancha', 'Yanacancha, Pasco, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('369', '190112', 'Vicco', 'Vicco, Pasco, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('370', '190111', 'Tinyahuarco', 'Tinyahuarco, Pasco, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('371', '190110', 'Ticlacayan', 'Ticlacayan, Pasco, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('372', '190109', 'Simon Bolivar', 'Simon Bolivar, Pasco, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('373', '190108', 'San Fco.De Asis de Yarusyacan', 'San Fco.De Asis de Yarusyacan, Pasco, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('374', '190107', 'Paucartambo', 'Paucartambo, Pasco, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('375', '190106', 'Pallanchacra', 'Pallanchacra, Pasco, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('376', '190105', 'Ninacaca', 'Ninacaca, Pasco, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('377', '190104', 'Huayllay', 'Huayllay, Pasco, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('378', '190103', 'Huariaca', 'Huariaca, Pasco, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('379', '190102', 'Huachon', 'Huachon, Pasco, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('380', '190101', 'Chaupimarca', 'Chaupimarca, Pasco, Pasco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('381', '190100', 'Pasco', 'Pasco, Pasco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('382', '190000', 'Pasco', 'Pasco', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('383', '180303', 'Pacocha', 'Pacocha, Ilo, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('384', '180302', 'El Algarrobal', 'El Algarrobal, Ilo, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('385', '180301', 'Ilo', 'Ilo, Ilo, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('386', '180300', 'Ilo', 'Ilo, Moquegua', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('387', '180211', 'Yunga', 'Yunga, General Sanchez Cerro, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('388', '180210', 'Ubinas', 'Ubinas, General Sanchez Cerro, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('389', '180209', 'Quinistaquillas', 'Quinistaquillas, General Sanchez Cerro, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('390', '180208', 'Puquina', 'Puquina, General Sanchez Cerro, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('391', '180207', 'Matalaque', 'Matalaque, General Sanchez Cerro, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('392', '180206', 'Lloque', 'Lloque, General Sanchez Cerro, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('393', '180205', 'La Capilla', 'La Capilla, General Sanchez Cerro, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('394', '180204', 'Ichuña', 'Ichuña, General Sanchez Cerro, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('395', '180203', 'Coalaque', 'Coalaque, General Sanchez Cerro, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('396', '180202', 'Chojata', 'Chojata, General Sanchez Cerro, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('397', '180201', 'Omate', 'Omate, General Sanchez Cerro, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('398', '180200', 'General Sanchez Cerro', 'General Sanchez Cerro, Moquegua', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('399', '180106', 'Torata', 'Torata, Mariscal Nieto, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('400', '180105', 'San Cristobal', 'San Cristobal, Mariscal Nieto, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('401', '180104', 'Samegua', 'Samegua, Mariscal Nieto, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('402', '180103', 'Cuchumbaya', 'Cuchumbaya, Mariscal Nieto, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('403', '180102', 'Carumas', 'Carumas, Mariscal Nieto, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('404', '180101', 'Moquegua', 'Moquegua, Mariscal Nieto, Moquegua', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('405', '180100', 'Mariscal Nieto', 'Mariscal Nieto, Moquegua', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('406', '180000', 'Moquegua', 'Moquegua', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('407', '170303', 'Tahuamanu', 'Tahuamanu, Tahuamanu, Madre de Dios', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('408', '170302', 'Iberia', 'Iberia, Tahuamanu, Madre de Dios', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('409', '170301', 'Iñapari', 'Iñapari, Tahuamanu, Madre de Dios', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('410', '170300', 'Tahuamanu', 'Tahuamanu, Madre de Dios', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('411', '170204', 'Huepetuhe', 'Huepetuhe, Manu, Madre de Dios', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('412', '170203', 'Madre de Dios', 'Madre de Dios, Manu, Madre de Dios', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('413', '170202', 'Fitzcarrald', 'Fitzcarrald, Manu, Madre de Dios', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('414', '170201', 'Manu', 'Manu, Manu, Madre de Dios', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('415', '170200', 'Manu', 'Manu, Madre de Dios', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('416', '170104', 'Laberinto', 'Laberinto, Tambopata, Madre de Dios', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('417', '170103', 'Las Piedras', 'Las Piedras, Tambopata, Madre de Dios', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('418', '170102', 'Inambari', 'Inambari, Tambopata, Madre de Dios', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('419', '170101', 'Tambopata', 'Tambopata, Tambopata, Madre de Dios', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('420', '170100', 'Tambopata', 'Tambopata, Madre de Dios', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('421', '170000', 'Madre de Dios', 'Madre de Dios', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('422', '160606', 'Vargas Guerra', 'Vargas Guerra, Ucayali, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('423', '160605', 'Sarayacu', 'Sarayacu, Ucayali, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('424', '160604', 'Pampa Hermosa', 'Pampa Hermosa, Ucayali, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('425', '160603', 'Padre Marquez', 'Padre Marquez, Ucayali, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('426', '160602', 'Inahuaya', 'Inahuaya, Ucayali, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('427', '160601', 'Contamana', 'Contamana, Ucayali, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('428', '160600', 'Ucayali', 'Ucayali, Loreto', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('429', '160511', 'Yaquerana', 'Yaquerana, Requena, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('430', '160510', 'Jenaro Herrera', 'Jenaro Herrera, Requena, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('431', '160509', 'Tapiche', 'Tapiche, Requena, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('432', '160508', 'Soplin', 'Soplin, Requena, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('433', '160507', 'Saquena', 'Saquena, Requena, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('434', '160506', 'Puinahua', 'Puinahua, Requena, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('435', '160505', 'Maquia', 'Maquia, Requena, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('436', '160504', 'Emilio San Martin', 'Emilio San Martin, Requena, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('437', '160503', 'Capelo', 'Capelo, Requena, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('438', '160502', 'Alto Tapiche', 'Alto Tapiche, Requena, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('439', '160501', 'Requena', 'Requena, Requena, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('440', '160500', 'Requena', 'Requena, Loreto', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('441', '160404', 'San Pablo', 'San Pablo, Mariscal Ramon Castilla, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('442', '160403', 'Yavari', 'Yavari, Mariscal Ramon Castilla, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('443', '160402', 'Pebas', 'Pebas, Mariscal Ramon Castilla, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('444', '160401', 'Ramon Castilla', 'Ramon Castilla, Mariscal Ramon Castilla, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('445', '160400', 'Mariscal Ramon Castilla', 'Mariscal Ramon Castilla, Loreto', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('446', '160305', 'Urarinas', 'Urarinas, Loreto, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('447', '160304', 'Trompeteros', 'Trompeteros, Loreto, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('448', '160303', 'Tigre', 'Tigre, Loreto, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('449', '160302', 'Parinari', 'Parinari, Loreto, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('450', '160301', 'Nauta', 'Nauta, Loreto, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('451', '160300', 'Loreto', 'Loreto, Loreto', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('452', '160211', 'Teniente Cesar Lopez Rojas', 'Teniente Cesar Lopez Rojas, Alto Amazonas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('453', '160210', 'Santa Cruz', 'Santa Cruz, Alto Amazonas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('454', '160209', 'Pastaza', 'Pastaza, Alto Amazonas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('455', '160208', 'Morona', 'Morona, Alto Amazonas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('456', '160207', 'Manseriche', 'Manseriche, Alto Amazonas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('457', '160206', 'Lagunas', 'Lagunas, Alto Amazonas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('458', '160205', 'Jeberos', 'Jeberos, Alto Amazonas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('459', '160204', 'Cahuapanas', 'Cahuapanas, Alto Amazonas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('460', '160203', 'Barranca', 'Barranca, Alto Amazonas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('461', '160202', 'Balsapuerto', 'Balsapuerto, Alto Amazonas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('462', '160201', 'Yurimaguas', 'Yurimaguas, Alto Amazonas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('463', '160200', 'Alto Amazonas', 'Alto Amazonas, Loreto', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('464', '160113', 'San Juan Bautista', 'San Juan Bautista, Maynas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('465', '160112', 'Belén', 'Belén, Maynas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('466', '160111', 'Yaquerana', 'Yaquerana, Maynas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('467', '160110', 'Torres Causana', 'Torres Causana, Maynas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('468', '160109', 'Putumayo', 'Putumayo, Maynas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('469', '160108', 'Punchana', 'Punchana, Maynas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('470', '160107', 'Napo', 'Napo, Maynas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('471', '160106', 'Mazan', 'Mazan, Maynas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('472', '160105', 'Las Amazonas', 'Las Amazonas, Maynas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('473', '160104', 'Indiana', 'Indiana, Maynas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('474', '160103', 'Fernando Lores', 'Fernando Lores, Maynas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('475', '160102', 'Alto Nanay', 'Alto Nanay, Maynas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('476', '160101', 'Iquitos', 'Iquitos, Maynas, Loreto', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('477', '160100', 'Maynas', 'Maynas, Loreto', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('478', '160000', 'Loreto', 'Loreto', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('479', '151033', 'Vitis', 'Vitis, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('480', '151032', 'Viñac', 'Viñac, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('481', '151031', 'Tupe', 'Tupe, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('482', '151030', 'Tomas', 'Tomas, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('483', '151029', 'Tauripampa', 'Tauripampa, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('484', '151028', 'Tanta', 'Tanta, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('485', '151027', 'San Pedro de Pilas', 'San Pedro de Pilas, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('486', '151026', 'San Joaquin', 'San Joaquin, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('487', '151025', 'Quinocay', 'Quinocay, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('488', '151024', 'Quinches', 'Quinches, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('489', '151023', 'Putinza', 'Putinza, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('490', '151022', 'Omas', 'Omas, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('491', '151021', 'Miraflores', 'Miraflores, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('492', '151020', 'Madean', 'Madean, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('493', '151019', 'Lincha', 'Lincha, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('494', '151018', 'Laraos', 'Laraos, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('495', '151017', 'Huañec', 'Huañec, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('496', '151016', 'Huantan', 'Huantan, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('497', '151015', 'Huangascar', 'Huangascar, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('498', '151014', 'Huancaya', 'Huancaya, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('499', '151013', 'Huampara', 'Huampara, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('500', '151012', 'Hongos', 'Hongos, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('501', '151011', 'Colonia', 'Colonia, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('502', '151010', 'Cochas', 'Cochas, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('503', '151009', 'Chocos', 'Chocos, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('504', '151008', 'Catahuasi', 'Catahuasi, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('505', '151007', 'Carania', 'Carania, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('506', '151006', 'Cacra', 'Cacra, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('507', '151005', 'Azangaro', 'Azangaro, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('508', '151004', 'Ayaviri', 'Ayaviri, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('509', '151003', 'Ayauca', 'Ayauca, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('510', '151002', 'Alis', 'Alis, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('511', '151001', 'Yauyos', 'Yauyos, Yauyos, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('512', '151000', 'Yauyos', 'Yauyos, Lima', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('513', '150906', 'Pachangara', 'Pachangara, Oyon, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('514', '150905', 'Navan', 'Navan, Oyon, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('515', '150904', 'Cochamarca', 'Cochamarca, Oyon, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('516', '150903', 'Caujul', 'Caujul, Oyon, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('517', '150902', 'Andajes', 'Andajes, Oyon, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('518', '150901', 'Oyon', 'Oyon, Oyon, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('519', '150900', 'Oyon', 'Oyon, Lima', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('520', '150812', 'Vegueta', 'Vegueta, Huaura, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('521', '150811', 'Sayan', 'Sayan, Huaura, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('522', '150810', 'Santa Maria', 'Santa Maria, Huaura, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('523', '150809', 'Santa Leonor', 'Santa Leonor, Huaura, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('524', '150808', 'Paccho', 'Paccho, Huaura, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('525', '150807', 'Leoncio Prado', 'Leoncio Prado, Huaura, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('526', '150806', 'Huaura', 'Huaura, Huaura, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('527', '150805', 'Hualmay', 'Hualmay, Huaura, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('528', '150804', 'Checras', 'Checras, Huaura, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('529', '150803', 'Caleta de Carquin', 'Caleta de Carquin, Huaura, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('530', '150802', 'Ambar', 'Ambar, Huaura, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('531', '150801', 'Huacho', 'Huacho, Huaura, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('532', '150800', 'Huaura', 'Huaura, Lima', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('533', '150732', 'Surco', 'Surco, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('534', '150731', 'Santo Domingo de los Olleros', 'Santo Domingo de los Olleros, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('535', '150730', 'Santiago de Tuna', 'Santiago de Tuna, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('536', '150729', 'Santiago de Anchucaya', 'Santiago de Anchucaya, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('537', '150728', 'Santa Eulalia', 'Santa Eulalia, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('538', '150727', 'Santa Cruz de Cocachacra', 'Santa Cruz de Cocachacra, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('539', '150726', 'Sangallaya', 'Sangallaya, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('540', '150725', 'San Pedro de Huancayre', 'San Pedro de Huancayre, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('541', '150724', 'San Pedro de Casta', 'San Pedro de Casta, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('542', '150723', 'San Mateo de Otao', 'San Mateo de Otao, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('543', '150722', 'San Mateo', 'San Mateo, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('544', '150721', 'San Lorenzo de Quinti', 'San Lorenzo de Quinti, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('545', '150720', 'San Juan de Tantaranche', 'San Juan de Tantaranche, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('546', '150719', 'San Juan de Iris', 'San Juan de Iris, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('547', '150718', 'San Damian', 'San Damian, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('548', '150717', 'San Bartolome', 'San Bartolome, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('549', '150716', 'San Antonio', 'San Antonio, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('550', '150715', 'San Andres de Tupicocha', 'San Andres de Tupicocha, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('551', '150714', 'Ricardo Palma', 'Ricardo Palma, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('552', '150713', 'Mariatana', 'Mariatana, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('553', '150712', 'Laraos', 'Laraos, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('554', '150711', 'Langa', 'Langa, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('555', '150710', 'Lahuaytambo', 'Lahuaytambo, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('556', '150709', 'Huarochiri', 'Huarochiri, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('557', '150708', 'Huanza', 'Huanza, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('558', '150707', 'Huachupampa', 'Huachupampa, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('559', '150706', 'Cuenca', 'Cuenca, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('560', '150705', 'Chicla', 'Chicla, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('561', '150704', 'Carampoma', 'Carampoma, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('562', '150703', 'Callahuanca', 'Callahuanca, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('563', '150702', 'Antioquia', 'Antioquia, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('564', '150701', 'Matucana', 'Matucana, Huarochiri, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('565', '150700', 'Huarochiri', 'Huarochiri, Lima', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('566', '150612', 'Veintisiete de Noviembre', 'Veintisiete de Noviembre, Huaral, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('567', '150611', 'Sumbilca', 'Sumbilca, Huaral, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('568', '150610', 'Santa Cruz de Andamarca', 'Santa Cruz de Andamarca, Huaral, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('569', '150609', 'San Miguel de Acos', 'San Miguel de Acos, Huaral, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('570', '150608', 'Pacaraos', 'Pacaraos, Huaral, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('571', '150607', 'Lampian', 'Lampian, Huaral, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('572', '150606', 'Ihuari', 'Ihuari, Huaral, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('573', '150605', 'Chancay', 'Chancay, Huaral, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('574', '150604', 'Aucallama', 'Aucallama, Huaral, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('575', '150603', 'Atavillos Bajo', 'Atavillos Bajo, Huaral, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('576', '150602', 'Atavillos Alto', 'Atavillos Alto, Huaral, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('577', '150601', 'Huaral', 'Huaral, Huaral, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('578', '150600', 'Huaral', 'Huaral, Lima', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('579', '150516', 'Zuñiga', 'Zuñiga, Cañete, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('580', '150515', 'Santa Cruz de Flores', 'Santa Cruz de Flores, Cañete, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('581', '150514', 'San Luis', 'San Luis, Cañete, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('582', '150513', 'San Antonio', 'San Antonio, Cañete, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('583', '150512', 'Quilmana', 'Quilmana, Cañete, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('584', '150511', 'Pacaran', 'Pacaran, Cañete, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('585', '150510', 'Nuevo Imperial', 'Nuevo Imperial, Cañete, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('586', '150509', 'Mala', 'Mala, Cañete, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('587', '150508', 'Lunahuana', 'Lunahuana, Cañete, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('588', '150507', 'Imperial', 'Imperial, Cañete, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('589', '150506', 'Coayllo', 'Coayllo, Cañete, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('590', '150505', 'Chilca', 'Chilca, Cañete, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('591', '150504', 'Cerro Azul', 'Cerro Azul, Cañete, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('592', '150503', 'Calango', 'Calango, Cañete, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('593', '150502', 'Asia', 'Asia, Cañete, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('594', '150501', 'San Vicente de Cañete', 'San Vicente de Cañete, Cañete, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('595', '150500', 'Cañete', 'Cañete, Lima', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('596', '150407', 'Santa Rosa de Quives', 'Santa Rosa de Quives, Canta, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('597', '150406', 'San Buenaventura', 'San Buenaventura, Canta, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('598', '150405', 'Lachaqui', 'Lachaqui, Canta, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('599', '150404', 'Huaros', 'Huaros, Canta, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('600', '150403', 'Huamantanga', 'Huamantanga, Canta, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('601', '150402', 'Arahuay', 'Arahuay, Canta, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('602', '150401', 'Canta', 'Canta, Canta, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('603', '150400', 'Canta', 'Canta, Lima', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('604', '150305', 'Manas', 'Manas, Cajatambo, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('605', '150304', 'Huancapon', 'Huancapon, Cajatambo, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('606', '150303', 'Gorgor', 'Gorgor, Cajatambo, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('607', '150302', 'Copa', 'Copa, Cajatambo, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('608', '150301', 'Cajatambo', 'Cajatambo, Cajatambo, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('609', '150300', 'Cajatambo', 'Cajatambo, Lima', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('610', '150205', 'Supe Puerto', 'Supe Puerto, Barranca, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('611', '150204', 'Supe', 'Supe, Barranca, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('612', '150203', 'Pativilca', 'Pativilca, Barranca, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('613', '150202', 'Paramonga', 'Paramonga, Barranca, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('614', '150201', 'Barranca', 'Barranca, Barranca, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('615', '150200', 'Barranca', 'Barranca, Lima', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('616', '150143', 'Villa Maria del Triunfo', 'Villa Maria del Triunfo, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('617', '150142', 'Villa El Salvador', 'Villa El Salvador, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('618', '150141', 'Surquillo', 'Surquillo, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('619', '150140', 'Santiago de Surco', 'Santiago de Surco, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('620', '150139', 'Santa Rosa', 'Santa Rosa, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('621', '150138', 'Santa Maria del Mar', 'Santa Maria del Mar, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('622', '150137', 'Santa Anita', 'Santa Anita, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('623', '150136', 'San Miguel', 'San Miguel, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('624', '150135', 'San Martin de Porres', 'San Martin de Porres, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('625', '150134', 'San Luis', 'San Luis, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('626', '150133', 'San Juan de Miraflores', 'San Juan de Miraflores, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('627', '150132', 'San Juan de Lurigancho', 'San Juan de Lurigancho, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('628', '150131', 'San Isidro', 'San Isidro, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('629', '150130', 'San Borja', 'San Borja, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('630', '150129', 'San Bartolo', 'San Bartolo, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('631', '150128', 'Rimac', 'Rimac, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('632', '150127', 'Punta Negra', 'Punta Negra, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('633', '150126', 'Punta Hermosa', 'Punta Hermosa, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('634', '150125', 'Puente Piedra', 'Puente Piedra, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('635', '150124', 'Pucusana', 'Pucusana, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('636', '150123', 'Pachacamac', 'Pachacamac, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('637', '150122', 'Miraflores', 'Miraflores, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('638', '150121', 'Magdalena Vieja', 'Magdalena Vieja, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('639', '150120', 'Magdalena del Mar', 'Magdalena del Mar, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('640', '150119', 'Lurin', 'Lurin, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('641', '150118', 'Lurigancho', 'Lurigancho, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('642', '150117', 'Los Olivos', 'Los Olivos, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('643', '150116', 'Lince', 'Lince, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('644', '150115', 'La Victoria', 'La Victoria, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('645', '150114', 'La Molina', 'La Molina, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('646', '150113', 'Jesus Maria', 'Jesus Maria, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('647', '150112', 'Independencia', 'Independencia, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('648', '150111', 'El Agustino', 'El Agustino, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('649', '150110', 'Comas', 'Comas, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('650', '150109', 'Cieneguilla', 'Cieneguilla, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('651', '150108', 'Chorrillos', 'Chorrillos, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('652', '150107', 'Chaclacayo', 'Chaclacayo, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('653', '150106', 'Carabayllo', 'Carabayllo, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('654', '150105', 'Breña', 'Breña, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('655', '150104', 'Barranco', 'Barranco, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('656', '150103', 'Ate', 'Ate, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('657', '150102', 'Ancon', 'Ancon, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('658', '150101', 'Lima', 'Lima, Lima, Lima', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('659', '150100', 'Lima', 'Lima, Lima', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('660', '150000', 'Lima', 'Lima', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('661', '140312', 'Tucume', 'Tucume, Lambayeque, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('662', '140311', 'San Jose', 'San Jose, Lambayeque, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('663', '140310', 'Salas', 'Salas, Lambayeque, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('664', '140309', 'Pacora', 'Pacora, Lambayeque, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('665', '140308', 'Olmos', 'Olmos, Lambayeque, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('666', '140307', 'Motupe', 'Motupe, Lambayeque, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('667', '140306', 'Morrope', 'Morrope, Lambayeque, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('668', '140305', 'Mochumi', 'Mochumi, Lambayeque, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('669', '140304', 'Jayanca', 'Jayanca, Lambayeque, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('670', '140303', 'Illimo', 'Illimo, Lambayeque, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('671', '140302', 'Chochope', 'Chochope, Lambayeque, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('672', '140301', 'Lambayeque', 'Lambayeque, Lambayeque, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('673', '140300', 'Lambayeque', 'Lambayeque, Lambayeque', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('674', '140206', 'Pueblo Nuevo', 'Pueblo Nuevo, Ferreñafe, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('675', '140205', 'Pitipo', 'Pitipo, Ferreñafe, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('676', '140204', 'Manuel Antonio Mesones Muro', 'Manuel Antonio Mesones Muro, Ferreñafe, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('677', '140203', 'Incahuasi', 'Incahuasi, Ferreñafe, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('678', '140202', 'Cañaris', 'Cañaris, Ferreñafe, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('679', '140201', 'Ferreñafe', 'Ferreñafe, Ferreñafe, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('680', '140200', 'Ferreñafe', 'Ferreñafe, Lambayeque', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('681', '140120', 'Tumán', 'Tumán, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('682', '140119', 'Pucalá', 'Pucalá, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('683', '140118', 'Pomalca', 'Pomalca, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('684', '140117', 'Patapo', 'Patapo, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('685', '140116', 'Cayaltí', 'Cayaltí, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('686', '140115', 'Saña', 'Saña, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('687', '140114', 'Santa Rosa', 'Santa Rosa, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('688', '140113', 'Reque', 'Reque, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('689', '140112', 'Pimentel', 'Pimentel, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('690', '140111', 'Picsi', 'Picsi, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('691', '140110', 'Oyotun', 'Oyotun, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('692', '140109', 'Nueva Arica', 'Nueva Arica, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('693', '140108', 'Monsefu', 'Monsefu, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('694', '140107', 'Lagunas', 'Lagunas, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('695', '140106', 'La Victoria', 'La Victoria, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('696', '140105', 'Jose Leonardo Ortiz', 'Jose Leonardo Ortiz, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('697', '140104', 'Eten Puerto', 'Eten Puerto, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('698', '140103', 'Eten', 'Eten, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('699', '140102', 'Chongoyape', 'Chongoyape, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('700', '140101', 'Chiclayo', 'Chiclayo, Chiclayo, Lambayeque', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('701', '140100', 'Chiclayo', 'Chiclayo, Lambayeque', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('702', '140000', 'Lambayeque', 'Lambayeque', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('703', '131203', 'Guadalupito', 'Guadalupito, Viru, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('704', '131202', 'Chao', 'Chao, Viru, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('705', '131201', 'Viru', 'Viru, Viru, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('706', '131200', 'Viru', 'Viru, La Libertad', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('707', '131104', 'Sayapullo', 'Sayapullo, Gran Chimu, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('708', '131103', 'Marmot', 'Marmot, Gran Chimu, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('709', '131102', 'Lucma', 'Lucma, Gran Chimu, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('710', '131101', 'Cascas', 'Cascas, Gran Chimu, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('711', '131100', 'Gran Chimu', 'Gran Chimu, La Libertad', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('712', '131008', 'Sitabamba', 'Sitabamba, Santiago de Chuco, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('713', '131007', 'Santa Cruz de Chuca', 'Santa Cruz de Chuca, Santiago de Chuco, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('714', '131006', 'Quiruvilca', 'Quiruvilca, Santiago de Chuco, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('715', '131005', 'Mollepata', 'Mollepata, Santiago de Chuco, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('716', '131004', 'Mollebamba', 'Mollebamba, Santiago de Chuco, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('717', '131003', 'Cachicadan', 'Cachicadan, Santiago de Chuco, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('718', '131002', 'Angasmarca', 'Angasmarca, Santiago de Chuco, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('719', '131001', 'Santiago de Chuco', 'Santiago de Chuco, Santiago de Chuco, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('720', '131000', 'Santiago de Chuco', 'Santiago de Chuco, La Libertad', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('721', '130908', 'Sartimbamba', 'Sartimbamba, Sanchez Carrion, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('722', '130907', 'Sarin', 'Sarin, Sanchez Carrion, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('723', '130906', 'Sanagoran', 'Sanagoran, Sanchez Carrion, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('724', '130905', 'Marcabal', 'Marcabal, Sanchez Carrion, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('725', '130904', 'Curgos', 'Curgos, Sanchez Carrion, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('726', '130903', 'Cochorco', 'Cochorco, Sanchez Carrion, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('727', '130902', 'Chugay', 'Chugay, Sanchez Carrion, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('728', '130901', 'Huamachuco', 'Huamachuco, Sanchez Carrion, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('729', '130900', 'Sanchez Carrion', 'Sanchez Carrion, La Libertad', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('730', '130813', 'Urpay', 'Urpay, Pataz, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('731', '130812', 'Taurija', 'Taurija, Pataz, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('732', '130811', 'Santiago de Challas', 'Santiago de Challas, Pataz, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('733', '130810', 'Pias', 'Pias, Pataz, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('734', '130809', 'Pataz', 'Pataz, Pataz, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('735', '130808', 'Parcoy', 'Parcoy, Pataz, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('736', '130807', 'Ongon', 'Ongon, Pataz, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('737', '130806', 'Huayo', 'Huayo, Pataz, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('738', '130805', 'Huaylillas', 'Huaylillas, Pataz, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('739', '130804', 'Huancaspata', 'Huancaspata, Pataz, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('740', '130803', 'Chillia', 'Chillia, Pataz, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('741', '130802', 'Buldibuyo', 'Buldibuyo, Pataz, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('742', '130801', 'Tayabamba', 'Tayabamba, Pataz, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('743', '130800', 'Pataz', 'Pataz, La Libertad', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('744', '130705', 'San Jose', 'San Jose, Pacasmayo, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('745', '130704', 'Pacasmayo', 'Pacasmayo, Pacasmayo, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('746', '130703', 'Jequetepeque', 'Jequetepeque, Pacasmayo, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('747', '130702', 'Guadalupe', 'Guadalupe, Pacasmayo, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('748', '130701', 'San Pedro de Lloc', 'San Pedro de Lloc, Pacasmayo, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('749', '130700', 'Pacasmayo', 'Pacasmayo, La Libertad', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('750', '130614', 'Usquil', 'Usquil, Otuzco, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('751', '130613', 'Sinsicap', 'Sinsicap, Otuzco, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('752', '130611', 'Salpo', 'Salpo, Otuzco, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('753', '130610', 'Paranday', 'Paranday, Otuzco, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('754', '130608', 'Mache', 'Mache, Otuzco, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('755', '130606', 'La Cuesta', 'La Cuesta, Otuzco, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('756', '130605', 'Huaranchal', 'Huaranchal, Otuzco, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('757', '130604', 'Charat', 'Charat, Otuzco, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('758', '130602', 'Agallpampa', 'Agallpampa, Otuzco, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('759', '130601', 'Otuzco', 'Otuzco, Otuzco, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('760', '130600', 'Otuzco', 'Otuzco, La Libertad', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('761', '130504', 'Huaso', 'Huaso, Julcan, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('762', '130503', 'Carabamba', 'Carabamba, Julcan, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('763', '130502', 'Calamarca', 'Calamarca, Julcan, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('764', '130501', 'Julcan', 'Julcan, Julcan, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('765', '130500', 'Julcan', 'Julcan, La Libertad', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('766', '130403', 'Pueblo Nuevo', 'Pueblo Nuevo, Chepen, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('767', '130402', 'Pacanga', 'Pacanga, Chepen, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('768', '130401', 'Chepen', 'Chepen, Chepen, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('769', '130400', 'Chepen', 'Chepen, La Libertad', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('770', '130306', 'Ucuncha', 'Ucuncha, Bolivar, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('771', '130305', 'Uchumarca', 'Uchumarca, Bolivar, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('772', '130304', 'Longotea', 'Longotea, Bolivar, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('773', '130303', 'Condormarca', 'Condormarca, Bolivar, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('774', '130302', 'Bambamarca', 'Bambamarca, Bolivar, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('775', '130301', 'Bolivar', 'Bolivar, Bolivar, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('776', '130300', 'Bolivar', 'Bolivar, La Libertad', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('777', '130208', 'Casa Grande', 'Casa Grande, Ascope, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('778', '130207', 'Santiago de Cao', 'Santiago de Cao, Ascope, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('779', '130206', 'Razuri', 'Razuri, Ascope, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('780', '130205', 'Paijan', 'Paijan, Ascope, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('781', '130204', 'Magdalena de Cao', 'Magdalena de Cao, Ascope, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('782', '130203', 'Chocope', 'Chocope, Ascope, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('783', '130202', 'Chicama', 'Chicama, Ascope, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('784', '130201', 'Ascope', 'Ascope, Ascope, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('785', '130200', 'Ascope', 'Ascope, La Libertad', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('786', '130111', 'Victor Larco Herrera', 'Victor Larco Herrera, Trujillo, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('787', '130110', 'Simbal', 'Simbal, Trujillo, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('788', '130109', 'Salaverry', 'Salaverry, Trujillo, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('789', '130108', 'Poroto', 'Poroto, Trujillo, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('790', '130107', 'Moche', 'Moche, Trujillo, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('791', '130106', 'Laredo', 'Laredo, Trujillo, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('792', '130105', 'La Esperanza', 'La Esperanza, Trujillo, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('793', '130104', 'Huanchaco', 'Huanchaco, Trujillo, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('794', '130103', 'Florencia de Mora', 'Florencia de Mora, Trujillo, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('795', '130102', 'El Porvenir', 'El Porvenir, Trujillo, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('796', '130101', 'Trujillo', 'Trujillo, Trujillo, La Libertad', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('797', '130100', 'Trujillo', 'Trujillo, La Libertad', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('798', '130000', 'La Libertad', 'La Libertad', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('799', '120909', 'Yanacancha', 'Yanacancha, Chupaca, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('800', '120908', 'Tres de Diciembre', 'Tres de Diciembre, Chupaca, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('801', '120907', 'San Juan de Jarpa', 'San Juan de Jarpa, Chupaca, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('802', '120906', 'San Juan de Iscos', 'San Juan de Iscos, Chupaca, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('803', '120905', 'Huamancaca Chico', 'Huamancaca Chico, Chupaca, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('804', '120904', 'Huachac', 'Huachac, Chupaca, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('805', '120903', 'Chongos Bajo', 'Chongos Bajo, Chupaca, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('806', '120902', 'Ahuac', 'Ahuac, Chupaca, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('807', '120901', 'Chupaca', 'Chupaca, Chupaca, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('808', '120900', 'Chupaca', 'Chupaca, Junin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('809', '120810', 'Yauli', 'Yauli, Yauli, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('810', '120809', 'Suitucancha', 'Suitucancha, Yauli, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('811', '120808', 'Santa Rosa de Sacco', 'Santa Rosa de Sacco, Yauli, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('812', '120807', 'Santa Barbara de Carhuacayan', 'Santa Barbara de Carhuacayan, Yauli, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('813', '120806', 'Paccha', 'Paccha, Yauli, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('814', '120805', 'Morococha', 'Morococha, Yauli, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('815', '120804', 'Marcapomacocha', 'Marcapomacocha, Yauli, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('816', '120803', 'Huay-Huay', 'Huay-Huay, Yauli, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('817', '120802', 'Chacapalpa', 'Chacapalpa, Yauli, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('818', '120801', 'La Oroya', 'La Oroya, Yauli, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('819', '120800', 'Yauli', 'Yauli, Junin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('820', '120709', 'Tapo', 'Tapo, Tarma, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('821', '120708', 'San Pedro de Cajas', 'San Pedro de Cajas, Tarma, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('822', '120707', 'Palcamayo', 'Palcamayo, Tarma, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('823', '120706', 'Palca', 'Palca, Tarma, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('824', '120705', 'La Union', 'La Union, Tarma, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('825', '120704', 'Huasahuasi', 'Huasahuasi, Tarma, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('826', '120703', 'Huaricolca', 'Huaricolca, Tarma, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('827', '120702', 'Acobamba', 'Acobamba, Tarma, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('828', '120701', 'Tarma', 'Tarma, Tarma, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('829', '120700', 'Tarma', 'Tarma, Junin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('830', '120608', 'Rio Tambo', 'Rio Tambo, Satipo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('831', '120607', 'Rio Negro', 'Rio Negro, Satipo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('832', '120606', 'Pangoa', 'Pangoa, Satipo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('833', '120605', 'Pampa Hermosa', 'Pampa Hermosa, Satipo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('834', '120604', 'Mazamari', 'Mazamari, Satipo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('835', '120603', 'Llaylla', 'Llaylla, Satipo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('836', '120602', 'Coviriali', 'Coviriali, Satipo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('837', '120601', 'Satipo', 'Satipo, Satipo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('838', '120600', 'Satipo', 'Satipo, Junin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('839', '120504', 'Ulcumayo', 'Ulcumayo, Junin, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('840', '120503', 'Ondores', 'Ondores, Junin, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('841', '120502', 'Carhuamayo', 'Carhuamayo, Junin, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('842', '120501', 'Junin', 'Junin, Junin, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('843', '120500', 'Junin', 'Junin, Junin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('844', '120434', 'Yauyos', 'Yauyos, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('845', '120433', 'Yauli', 'Yauli, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('846', '120432', 'Tunan Marca', 'Tunan Marca, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('847', '120431', 'Sincos', 'Sincos, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('848', '120430', 'Sausa', 'Sausa, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('849', '120429', 'San Pedro de Chunan', 'San Pedro de Chunan, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('850', '120428', 'San Lorenzo', 'San Lorenzo, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('851', '120427', 'Ricran', 'Ricran, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('852', '120426', 'Pomacancha', 'Pomacancha, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('853', '120425', 'Parco', 'Parco, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('854', '120424', 'Pancan', 'Pancan, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('855', '120423', 'Paccha', 'Paccha, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('856', '120422', 'Paca', 'Paca, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('857', '120421', 'Muquiyauyo', 'Muquiyauyo, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('858', '120420', 'Muqui', 'Muqui, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('859', '120419', 'Monobamba', 'Monobamba, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('860', '120418', 'Molinos', 'Molinos, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('861', '120417', 'Masma Chicche', 'Masma Chicche, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('862', '120416', 'Masma', 'Masma, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('863', '120415', 'Marco', 'Marco, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('864', '120414', 'Llocllapampa', 'Llocllapampa, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('865', '120413', 'Leonor Ordoñez', 'Leonor Ordoñez, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('866', '120412', 'Julcan', 'Julcan, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('867', '120411', 'Janjaillo', 'Janjaillo, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('868', '120410', 'Huertas', 'Huertas, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('869', '120409', 'Huaripampa', 'Huaripampa, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('870', '120408', 'Huamali', 'Huamali, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('871', '120407', 'El Mantaro', 'El Mantaro, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('872', '120406', 'Curicaca', 'Curicaca, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('873', '120405', 'Canchayllo', 'Canchayllo, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('874', '120404', 'Ataura', 'Ataura, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('875', '120403', 'Apata', 'Apata, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('876', '120402', 'Acolla', 'Acolla, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('877', '120401', 'Jauja', 'Jauja, Jauja, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('878', '120400', 'Jauja', 'Jauja, Junin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('879', '120306', 'Vitoc', 'Vitoc, Chanchamayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('880', '120305', 'San Ramon', 'San Ramon, Chanchamayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('881', '120304', 'San Luis de Shuaro', 'San Luis de Shuaro, Chanchamayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('882', '120303', 'Pichanaqui', 'Pichanaqui, Chanchamayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('883', '120302', 'Perene', 'Perene, Chanchamayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('884', '120301', 'Chanchamayo', 'Chanchamayo, Chanchamayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('885', '120300', 'Chanchamayo', 'Chanchamayo, Junin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('886', '120215', 'Santa Rosa de Ocopa', 'Santa Rosa de Ocopa, Concepcion, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('887', '120214', 'San Jose de Quero', 'San Jose de Quero, Concepcion, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('888', '120213', 'Orcotuna', 'Orcotuna, Concepcion, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('889', '120212', 'Nueve de Julio', 'Nueve de Julio, Concepcion, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('890', '120211', 'Mito', 'Mito, Concepcion, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('891', '120210', 'Matahuasi', 'Matahuasi, Concepcion, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('892', '120209', 'Mariscal Castilla', 'Mariscal Castilla, Concepcion, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('893', '120208', 'Manzanares', 'Manzanares, Concepcion, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('894', '120207', 'Heroinas Toledo', 'Heroinas Toledo, Concepcion, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('895', '120206', 'Comas', 'Comas, Concepcion, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('896', '120205', 'Cochas', 'Cochas, Concepcion, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('897', '120204', 'Chambara', 'Chambara, Concepcion, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('898', '120203', 'Andamarca', 'Andamarca, Concepcion, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('899', '120202', 'Aco', 'Aco, Concepcion, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('900', '120201', 'Concepcion', 'Concepcion, Concepcion, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('901', '120200', 'Concepcion', 'Concepcion, Junin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('902', '120136', 'Viques', 'Viques, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('903', '120135', 'Santo Domingo de Acobamba', 'Santo Domingo de Acobamba, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('904', '120134', 'Sicaya', 'Sicaya, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('905', '120133', 'Sapallanga', 'Sapallanga, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('906', '120132', 'Saño', 'Saño, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('907', '120130', 'San Jeronimo de Tunan', 'San Jeronimo de Tunan, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('908', '120129', 'San Agustin', 'San Agustin, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('909', '120128', 'Quilcas', 'Quilcas, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('910', '120127', 'Quichuay', 'Quichuay, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('911', '120126', 'Pucara', 'Pucara, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('912', '120125', 'Pilcomayo', 'Pilcomayo, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('913', '120124', 'Pariahuanca', 'Pariahuanca, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('914', '120122', 'Ingenio', 'Ingenio, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('915', '120121', 'Huayucachi', 'Huayucachi, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('916', '120120', 'Huasicancha', 'Huasicancha, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('917', '120119', 'Huancan', 'Huancan, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('918', '120117', 'Hualhuas', 'Hualhuas, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('919', '120116', 'Huacrapuquio', 'Huacrapuquio, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('920', '120114', 'El Tambo', 'El Tambo, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('921', '120113', 'Cullhuas', 'Cullhuas, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('922', '120112', 'Colca', 'Colca, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('923', '120111', 'Chupuro', 'Chupuro, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('924', '120108', 'Chongos Alto', 'Chongos Alto, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('925', '120107', 'Chilca', 'Chilca, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('926', '120106', 'Chicche', 'Chicche, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('927', '120105', 'Chacapampa', 'Chacapampa, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('928', '120104', 'Carhuacallanga', 'Carhuacallanga, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('929', '120101', 'Huancayo', 'Huancayo, Huancayo, Junin', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('930', '120100', 'Huancayo', 'Huancayo, Junin', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('931', '120000', 'Junin', 'Junin', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('932', '110508', 'Tupac Amaru Inca', 'Tupac Amaru Inca, Pisco, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('933', '110507', 'San Clemente', 'San Clemente, Pisco, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('934', '110506', 'San Andres', 'San Andres, Pisco, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('935', '110505', 'Paracas', 'Paracas, Pisco, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('936', '110504', 'Independencia', 'Independencia, Pisco, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('937', '110503', 'Humay', 'Humay, Pisco, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('938', '110502', 'Huancano', 'Huancano, Pisco, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('939', '110501', 'Pisco', 'Pisco, Pisco, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('940', '110500', 'Pisco', 'Pisco, Ica', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('941', '110405', 'Tibillo', 'Tibillo, Palpa, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('942', '110404', 'Santa Cruz', 'Santa Cruz, Palpa, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('943', '110403', 'Rio Grande', 'Rio Grande, Palpa, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('944', '110402', 'Llipata', 'Llipata, Palpa, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('945', '110401', 'Palpa', 'Palpa, Palpa, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('946', '110400', 'Palpa', 'Palpa, Ica', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('947', '110305', 'Vista Alegre', 'Vista Alegre, Nazca, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('948', '110304', 'Marcona', 'Marcona, Nazca, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('949', '110303', 'El Ingenio', 'El Ingenio, Nazca, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('950', '110302', 'Changuillo', 'Changuillo, Nazca, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('951', '110301', 'Nazca', 'Nazca, Nazca, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('952', '110300', 'Nazca', 'Nazca, Ica', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('953', '110211', 'Tambo de Mora', 'Tambo de Mora, Chincha, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('954', '110210', 'Sunampe', 'Sunampe, Chincha, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('955', '110209', 'San Pedro de Huacarpana', 'San Pedro de Huacarpana, Chincha, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('956', '110208', 'San Juan de Yanac', 'San Juan de Yanac, Chincha, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('957', '110207', 'Pueblo Nuevo', 'Pueblo Nuevo, Chincha, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('958', '110206', 'Grocio Prado', 'Grocio Prado, Chincha, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('959', '110205', 'El Carmen', 'El Carmen, Chincha, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('960', '110204', 'Chincha Baja', 'Chincha Baja, Chincha, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('961', '110203', 'Chavin', 'Chavin, Chincha, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('962', '110202', 'Alto Laran', 'Alto Laran, Chincha, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('963', '110201', 'Chincha Alta', 'Chincha Alta, Chincha, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('964', '110200', 'Chincha', 'Chincha, Ica', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('965', '110114', 'Yauca del Rosario  1/', 'Yauca del Rosario  1/, Ica, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('966', '110113', 'Tate', 'Tate, Ica, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('967', '110112', 'Subtanjalla', 'Subtanjalla, Ica, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('968', '110111', 'Santiago', 'Santiago, Ica, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('969', '110110', 'San Juan Bautista', 'San Juan Bautista, Ica, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('970', '110109', 'San Jose de los Molinos', 'San Jose de los Molinos, Ica, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('971', '110108', 'Salas', 'Salas, Ica, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('972', '110107', 'Pueblo Nuevo', 'Pueblo Nuevo, Ica, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('973', '110106', 'Parcona', 'Parcona, Ica, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('974', '110105', 'Pachacutec', 'Pachacutec, Ica, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('975', '110104', 'Ocucaje', 'Ocucaje, Ica, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('976', '110103', 'Los Aquijes', 'Los Aquijes, Ica, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('977', '110102', 'La Tinguiña', 'La Tinguiña, Ica, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('978', '110101', 'Ica', 'Ica, Ica, Ica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('979', '110100', 'Ica', 'Ica, Ica', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('980', '110000', 'Ica', 'Ica', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('981', '101108', 'Choras', 'Choras, Yarowilca, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('982', '101107', 'Pampamarca', 'Pampamarca, Yarowilca, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('983', '101106', 'Obas', 'Obas, Yarowilca, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('984', '101105', 'Jacas Chico', 'Jacas Chico, Yarowilca, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('985', '101104', 'Chupan', 'Chupan, Yarowilca, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('986', '101103', 'Chacabamba', 'Chacabamba, Yarowilca, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('987', '101102', 'Cahuac', 'Cahuac, Yarowilca, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('988', '101101', 'Chavinillo', 'Chavinillo, Yarowilca, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('989', '101100', 'Yarowilca', 'Yarowilca, Huanuco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('990', '101007', 'San Miguel de Cauri', 'San Miguel de Cauri, Lauricocha, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('991', '101006', 'San Francisco de Asis', 'San Francisco de Asis, Lauricocha, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('992', '101005', 'Rondos', 'Rondos, Lauricocha, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('993', '101004', 'Queropalca', 'Queropalca, Lauricocha, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('994', '101003', 'Jivia', 'Jivia, Lauricocha, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('995', '101002', 'Baños', 'Baños, Lauricocha, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('996', '101001', 'Jesus', 'Jesus, Lauricocha, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('997', '101000', 'Lauricocha', 'Lauricocha, Huanuco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('998', '100905', 'Yuyapichis', 'Yuyapichis, Puerto Inca, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('999', '100904', 'Tournavista', 'Tournavista, Puerto Inca, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1000', '100903', 'Honoria', 'Honoria, Puerto Inca, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1001', '100902', 'Codo del Pozuzo', 'Codo del Pozuzo, Puerto Inca, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1002', '100901', 'Puerto Inca', 'Puerto Inca, Puerto Inca, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1003', '100900', 'Puerto Inca', 'Puerto Inca, Huanuco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1004', '100804', 'Umari', 'Umari, Pachitea, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1005', '100803', 'Molino', 'Molino, Pachitea, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1006', '100802', 'Chaglla', 'Chaglla, Pachitea, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1007', '100801', 'Panao', 'Panao, Pachitea, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1008', '100800', 'Pachitea', 'Pachitea, Huanuco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1009', '100703', 'San Buenaventura', 'San Buenaventura, Maraqon, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1010', '100702', 'Cholon', 'Cholon, Maraqon, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1011', '100701', 'Huacrachuco', 'Huacrachuco, Maraqon, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1012', '100700', 'Maraqon', 'Maraqon, Huanuco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1013', '100606', 'Mariano Damaso Beraun', 'Mariano Damaso Beraun, Leoncio Prado, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1014', '100605', 'Luyando', 'Luyando, Leoncio Prado, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1015', '100604', 'Jose Crespo y Castillo', 'Jose Crespo y Castillo, Leoncio Prado, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1016', '100603', 'Hermilio Valdizan', 'Hermilio Valdizan, Leoncio Prado, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1017', '100602', 'Daniel Alomias Robles', 'Daniel Alomias Robles, Leoncio Prado, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1018', '100601', 'Rupa-Rupa', 'Rupa-Rupa, Leoncio Prado, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1019', '100600', 'Leoncio Prado', 'Leoncio Prado, Huanuco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1020', '100511', 'Tantamayo', 'Tantamayo, Huamalies, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1021', '100510', 'Singa', 'Singa, Huamalies, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1022', '100509', 'Puños', 'Puños, Huamalies, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1023', '100508', 'Punchao', 'Punchao, Huamalies, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1024', '100507', 'Monzon', 'Monzon, Huamalies, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1025', '100506', 'Miraflores', 'Miraflores, Huamalies, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1026', '100505', 'Jircan', 'Jircan, Huamalies, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1027', '100504', 'Jacas Grande', 'Jacas Grande, Huamalies, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1028', '100503', 'Chavin de Pariarca', 'Chavin de Pariarca, Huamalies, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1029', '100502', 'Arancay', 'Arancay, Huamalies, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1030', '100501', 'Llata', 'Llata, Huamalies, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1031', '100500', 'Huamalies', 'Huamalies, Huanuco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1032', '100404', 'Pinra', 'Pinra, Huacaybamba, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1033', '100403', 'Cochabamba', 'Cochabamba, Huacaybamba, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1034', '100402', 'Canchabamba', 'Canchabamba, Huacaybamba, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1035', '100401', 'Huacaybamba', 'Huacaybamba, Huacaybamba, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1036', '100400', 'Huacaybamba', 'Huacaybamba, Huanuco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1037', '100323', 'Yanas', 'Yanas, Dos de Mayo, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1038', '100322', 'Sillapata', 'Sillapata, Dos de Mayo, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1039', '100321', 'Shunqui', 'Shunqui, Dos de Mayo, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1040', '100317', 'Ripan', 'Ripan, Dos de Mayo, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1041', '100316', 'Quivilla', 'Quivilla, Dos de Mayo, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1042', '100313', 'Pachas', 'Pachas, Dos de Mayo, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1043', '100311', 'Marias', 'Marias, Dos de Mayo, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1044', '100307', 'Chuquis', 'Chuquis, Dos de Mayo, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1045', '100301', 'La Union', 'La Union, Dos de Mayo, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1046', '100300', 'Dos de Mayo', 'Dos de Mayo, Huanuco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1047', '100208', 'Tomay Kichwa', 'Tomay Kichwa, Ambo, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1048', '100207', 'San Rafael', 'San Rafael, Ambo, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1049', '100206', 'San Francisco', 'San Francisco, Ambo, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1050', '100205', 'Huacar', 'Huacar, Ambo, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1051', '100204', 'Conchamarca', 'Conchamarca, Ambo, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1052', '100203', 'Colpas', 'Colpas, Ambo, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1053', '100202', 'Cayna', 'Cayna, Ambo, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1054', '100201', 'Ambo', 'Ambo, Ambo, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1055', '100200', 'Ambo', 'Ambo, Huanuco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1056', '100111', 'Pillcomarca', 'Pillcomarca, Huanuco, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1057', '100110', 'Yarumayo', 'Yarumayo, Huanuco, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1058', '100109', 'Santa Maria del Valle', 'Santa Maria del Valle, Huanuco, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1059', '100108', 'San Pedro de Chaulan', 'San Pedro de Chaulan, Huanuco, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1060', '100107', 'San Francisco de Cayran', 'San Francisco de Cayran, Huanuco, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1061', '100106', 'Quisqui', 'Quisqui, Huanuco, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1062', '100105', 'Margos', 'Margos, Huanuco, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1063', '100104', 'Churubamba', 'Churubamba, Huanuco, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1064', '100103', 'Chinchao', 'Chinchao, Huanuco, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1065', '100102', 'Amarilis', 'Amarilis, Huanuco, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1066', '100101', 'Huanuco', 'Huanuco, Huanuco, Huanuco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1067', '100100', 'Huanuco', 'Huanuco, Huanuco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1068', '100000', 'Huanuco', 'Huanuco', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1069', '090718', 'Tintay Puncu', 'Tintay Puncu, Tayacaja, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1070', '090717', 'Surcubamba', 'Surcubamba, Tayacaja, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1071', '090716', 'San Marcos de Rocchac', 'San Marcos de Rocchac, Tayacaja, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1072', '090715', 'Salcahuasi', 'Salcahuasi, Tayacaja, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1073', '090714', 'Salcabamba', 'Salcabamba, Tayacaja, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1074', '090713', 'Quishuar', 'Quishuar, Tayacaja, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1075', '090712', 'Pachamarca', 'Pachamarca, Tayacaja, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1076', '090711', 'Pazos', 'Pazos, Tayacaja, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1077', '090710', 'Ñahuimpuquio', 'Ñahuimpuquio, Tayacaja, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1078', '090709', 'Huaribamba', 'Huaribamba, Tayacaja, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1079', '090708', 'Huando', 'Huando, Tayacaja, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1080', '090707', 'Huachocolpa', 'Huachocolpa, Tayacaja, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1081', '090706', 'Daniel Hernandez', 'Daniel Hernandez, Tayacaja, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1082', '090705', 'Colcabamba', 'Colcabamba, Tayacaja, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1083', '090704', 'Ahuaycha', 'Ahuaycha, Tayacaja, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1084', '090703', 'Acraquia', 'Acraquia, Tayacaja, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1085', '090702', 'Acostambo', 'Acostambo, Tayacaja, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1086', '090701', 'Pampas', 'Pampas, Tayacaja, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1087', '090700', 'Tayacaja', 'Tayacaja, Huancavelica', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1088', '090616', 'Tambo', 'Tambo, Huaytara, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1089', '090615', 'Santo Domingo de Capillas', 'Santo Domingo de Capillas, Huaytara, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1090', '090614', 'Santiago de Quirahuara', 'Santiago de Quirahuara, Huaytara, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1091', '090613', 'Santiago de Chocorvos', 'Santiago de Chocorvos, Huaytara, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1092', '090612', 'San Isidro', 'San Isidro, Huaytara, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1093', '090611', 'San Francisco de Sangayaico', 'San Francisco de Sangayaico, Huaytara, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1094', '090610', 'San Antonio de Cusicancha', 'San Antonio de Cusicancha, Huaytara, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1095', '090609', 'Quito-Arma', 'Quito-Arma, Huaytara, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1096', '090608', 'Querco', 'Querco, Huaytara, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1097', '090607', 'Pilpichaca', 'Pilpichaca, Huaytara, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1098', '090606', 'Ocoyo', 'Ocoyo, Huaytara, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1099', '090605', 'Laramarca', 'Laramarca, Huaytara, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1100', '090604', 'Huayacundo Arma', 'Huayacundo Arma, Huaytara, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1101', '090603', 'Cordova', 'Cordova, Huaytara, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1102', '090602', 'Ayavi', 'Ayavi, Huaytara, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1103', '090601', 'Huaytara', 'Huaytara, Huaytara, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1104', '090600', 'Huaytara', 'Huaytara, Huancavelica', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1105', '090510', 'Pachamarca', 'Pachamarca, Churcampa, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1106', '090509', 'San Pedro de Coris', 'San Pedro de Coris, Churcampa, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1107', '090508', 'San Miguel de Mayocc', 'San Miguel de Mayocc, Churcampa, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1108', '090507', 'Paucarbamba', 'Paucarbamba, Churcampa, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1109', '090506', 'Locroja', 'Locroja, Churcampa, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1110', '090505', 'La Merced', 'La Merced, Churcampa, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1111', '090504', 'El Carmen', 'El Carmen, Churcampa, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1112', '090503', 'Chinchihuasi', 'Chinchihuasi, Churcampa, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1113', '090502', 'Anco', 'Anco, Churcampa, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1114', '090501', 'Churcampa', 'Churcampa, Churcampa, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1115', '090500', 'Churcampa', 'Churcampa, Huancavelica', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1116', '090413', 'Ticrapo', 'Ticrapo, Castrovirreyna, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1117', '090412', 'Tantara', 'Tantara, Castrovirreyna, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1118', '090411', 'Santa Ana', 'Santa Ana, Castrovirreyna, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1119', '090410', 'San Juan', 'San Juan, Castrovirreyna, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1120', '090409', 'Mollepampa', 'Mollepampa, Castrovirreyna, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1121', '090408', 'Huamatambo', 'Huamatambo, Castrovirreyna, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1122', '090407', 'Huachos', 'Huachos, Castrovirreyna, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1123', '090406', 'Cocas', 'Cocas, Castrovirreyna, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1124', '090405', 'Chupamarca', 'Chupamarca, Castrovirreyna, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1125', '090404', 'Capillas', 'Capillas, Castrovirreyna, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1126', '090403', 'Aurahua', 'Aurahua, Castrovirreyna, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1127', '090402', 'Arma', 'Arma, Castrovirreyna, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1128', '090401', 'Castrovirreyna', 'Castrovirreyna, Castrovirreyna, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1129', '090400', 'Castrovirreyna', 'Castrovirreyna, Huancavelica', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1130', '090312', 'Secclla', 'Secclla, Angaraes, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1131', '090311', 'Santo Tomas de Pata', 'Santo Tomas de Pata, Angaraes, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1132', '090310', 'San Antonio de Antaparco', 'San Antonio de Antaparco, Angaraes, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1133', '090309', 'Julcamarca', 'Julcamarca, Angaraes, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1134', '090308', 'Huayllay Grande', 'Huayllay Grande, Angaraes, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1135', '090307', 'Huanca-Huanca', 'Huanca-Huanca, Angaraes, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1136', '090306', 'Congalla', 'Congalla, Angaraes, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1137', '090305', 'Chincho', 'Chincho, Angaraes, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1138', '090304', 'Ccochaccasa', 'Ccochaccasa, Angaraes, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1139', '090303', 'Callanmarca', 'Callanmarca, Angaraes, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1140', '090302', 'Anchonga', 'Anchonga, Angaraes, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1141', '090301', 'Lircay', 'Lircay, Angaraes, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1142', '090300', 'Angaraes', 'Angaraes, Huancavelica', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1143', '090208', 'Rosario', 'Rosario, Acobamba, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1144', '090207', 'Pomacocha', 'Pomacocha, Acobamba, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1145', '090206', 'Paucara', 'Paucara, Acobamba, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1146', '090205', 'Marcas', 'Marcas, Acobamba, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1147', '090204', 'Caja', 'Caja, Acobamba, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1148', '090203', 'Anta', 'Anta, Acobamba, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1149', '090202', 'Andabamba', 'Andabamba, Acobamba, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1150', '090201', 'Acobamba', 'Acobamba, Acobamba, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1151', '090200', 'Acobamba', 'Acobamba, Huancavelica', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1152', '090119', 'Huando', 'Huando, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1153', '090118', 'Ascensión', 'Ascensión, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1154', '090117', 'Yauli', 'Yauli, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1155', '090116', 'Vilca', 'Vilca, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1156', '090115', 'Pilchaca', 'Pilchaca, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1157', '090114', 'Palca', 'Palca, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1158', '090113', 'Nuevo Occoro', 'Nuevo Occoro, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1159', '090112', 'Moya', 'Moya, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1160', '090111', 'Mariscal Caceres', 'Mariscal Caceres, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1161', '090110', 'Manta', 'Manta, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1162', '090109', 'Laria', 'Laria, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1163', '090108', 'Izcuchaca', 'Izcuchaca, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1164', '090107', 'Huayllahuara', 'Huayllahuara, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1165', '090106', 'Huachocolpa', 'Huachocolpa, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1166', '090105', 'Cuenca', 'Cuenca, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1167', '090104', 'Conayca', 'Conayca, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1168', '090103', 'Acoria', 'Acoria, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1169', '090102', 'Acobambilla', 'Acobambilla, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1170', '090101', 'Huancavelica', 'Huancavelica, Huancavelica, Huancavelica', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1171', '090100', 'Huancavelica', 'Huancavelica, Huancavelica', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1172', '090000', 'Huancavelica', 'Huancavelica', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1173', '081307', 'Yucay', 'Yucay, Urubamba, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1174', '081306', 'Ollantaytambo', 'Ollantaytambo, Urubamba, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1175', '081305', 'Maras', 'Maras, Urubamba, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1176', '081304', 'Machupicchu', 'Machupicchu, Urubamba, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1177', '081303', 'Huayllabamba', 'Huayllabamba, Urubamba, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1178', '081302', 'Chinchero', 'Chinchero, Urubamba, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1179', '081301', 'Urubamba', 'Urubamba, Urubamba, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1180', '081300', 'Urubamba', 'Urubamba, Cusco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1181', '081212', 'Quiquijana', 'Quiquijana, Quispicanchi, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1182', '081211', 'Oropesa', 'Oropesa, Quispicanchi, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1183', '081210', 'Ocongate', 'Ocongate, Quispicanchi, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1184', '081209', 'Marcapata', 'Marcapata, Quispicanchi, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1185', '081208', 'Lucre', 'Lucre, Quispicanchi, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1186', '081207', 'Huaro', 'Huaro, Quispicanchi, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1187', '081206', 'Cusipata', 'Cusipata, Quispicanchi, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1188', '081205', 'Ccatca', 'Ccatca, Quispicanchi, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1189', '081204', 'Ccarhuayo', 'Ccarhuayo, Quispicanchi, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1190', '081203', 'Camanti', 'Camanti, Quispicanchi, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1191', '081202', 'Andahuaylillas', 'Andahuaylillas, Quispicanchi, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1192', '081201', 'Urcos', 'Urcos, Quispicanchi, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1193', '081200', 'Quispicanchi', 'Quispicanchi, Cusco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1194', '081106', 'Kosñipata', 'Kosñipata, Paucartambo, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1195', '081105', 'Huancarani', 'Huancarani, Paucartambo, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1196', '081104', 'Colquepata', 'Colquepata, Paucartambo, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1197', '081103', 'Challabamba', 'Challabamba, Paucartambo, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1198', '081102', 'Caicay', 'Caicay, Paucartambo, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1199', '081101', 'Paucartambo', 'Paucartambo, Paucartambo, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1200', '081100', 'Paucartambo', 'Paucartambo, Cusco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1201', '081009', 'Yaurisque', 'Yaurisque, Paruro, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1202', '081008', 'Pillpinto', 'Pillpinto, Paruro, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1203', '081007', 'Paccaritambo', 'Paccaritambo, Paruro, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1204', '081006', 'Omacha', 'Omacha, Paruro, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1205', '081005', 'Huanoquite', 'Huanoquite, Paruro, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1206', '081004', 'Colcha', 'Colcha, Paruro, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1207', '081003', 'Ccapi', 'Ccapi, Paruro, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1208', '081002', 'Accha', 'Accha, Paruro, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1209', '081001', 'Paruro', 'Paruro, Paruro, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1210', '081000', 'Paruro', 'Paruro, Cusco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1211', '080910', 'Pichari', 'Pichari, La Convencion, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1212', '080909', 'Vilcabamba', 'Vilcabamba, La Convencion, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1213', '080908', 'Santa Teresa', 'Santa Teresa, La Convencion, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1214', '080907', 'Quimbiri', 'Quimbiri, La Convencion, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1215', '080906', 'Quellouno', 'Quellouno, La Convencion, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1216', '080905', 'Ocobamba', 'Ocobamba, La Convencion, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1217', '080904', 'Maranura', 'Maranura, La Convencion, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1218', '080903', 'Huayopata', 'Huayopata, La Convencion, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1219', '080902', 'Echarate', 'Echarate, La Convencion, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1220', '080901', 'Santa Ana', 'Santa Ana, La Convencion, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1221', '080900', 'La Convencion', 'La Convencion, Cusco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1222', '080808', 'Alto Pichigua', 'Alto Pichigua, Espinar, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1223', '080807', 'Suyckutambo', 'Suyckutambo, Espinar, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1224', '080806', 'Pichigua', 'Pichigua, Espinar, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1225', '080805', 'Pallpata', 'Pallpata, Espinar, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1226', '080804', 'Ocoruro', 'Ocoruro, Espinar, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1227', '080803', 'Coporaque', 'Coporaque, Espinar, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1228', '080802', 'Condoroma', 'Condoroma, Espinar, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1229', '080801', 'Espinar', 'Espinar, Espinar, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1230', '080800', 'Espinar', 'Espinar, Cusco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1231', '080708', 'Velille', 'Velille, Chumbivilcas, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1232', '080707', 'Quiñota', 'Quiñota, Chumbivilcas, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1233', '080706', 'Llusco', 'Llusco, Chumbivilcas, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1234', '080705', 'Livitaca', 'Livitaca, Chumbivilcas, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1235', '080704', 'Colquemarca', 'Colquemarca, Chumbivilcas, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1236', '080703', 'Chamaca', 'Chamaca, Chumbivilcas, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1237', '080702', 'Capacmarca', 'Capacmarca, Chumbivilcas, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1238', '080701', 'Santo Tomas', 'Santo Tomas, Chumbivilcas, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1239', '080700', 'Chumbivilcas', 'Chumbivilcas, Cusco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1240', '080608', 'Tinta', 'Tinta, Canchis, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1241', '080607', 'San Pedro', 'San Pedro, Canchis, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1242', '080606', 'San Pablo', 'San Pablo, Canchis, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1243', '080605', 'Pitumarca', 'Pitumarca, Canchis, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1244', '080604', 'Marangani', 'Marangani, Canchis, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1245', '080603', 'Combapata', 'Combapata, Canchis, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1246', '080602', 'Checacupe', 'Checacupe, Canchis, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1247', '080601', 'Sicuani', 'Sicuani, Canchis, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1248', '080600', 'Canchis', 'Canchis, Cusco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1249', '080508', 'Tupac Amaru', 'Tupac Amaru, Canas, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1250', '080507', 'Quehue', 'Quehue, Canas, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1251', '080506', 'Pampamarca', 'Pampamarca, Canas, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1252', '080505', 'Layo', 'Layo, Canas, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1253', '080504', 'Langui', 'Langui, Canas, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1254', '080503', 'Kunturkanki', 'Kunturkanki, Canas, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1255', '080502', 'Checca', 'Checca, Canas, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1256', '080501', 'Yanaoca', 'Yanaoca, Canas, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1257', '080500', 'Canas', 'Canas, Cusco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1258', '080408', 'Yanatile', 'Yanatile, Calca, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1259', '080407', 'Taray', 'Taray, Calca, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1260', '080406', 'San Salvador', 'San Salvador, Calca, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1261', '080405', 'Pisac', 'Pisac, Calca, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1262', '080404', 'Lares', 'Lares, Calca, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1263', '080403', 'Lamay', 'Lamay, Calca, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1264', '080402', 'Coya', 'Coya, Calca, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1265', '080401', 'Calca', 'Calca, Calca, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1266', '080400', 'Calca', 'Calca, Cusco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1267', '080309', 'Zurite', 'Zurite, Anta, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1268', '080308', 'Pucyura', 'Pucyura, Anta, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1269', '080307', 'Mollepata', 'Mollepata, Anta, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1270', '080306', 'Limatambo', 'Limatambo, Anta, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1271', '080305', 'Huarocondo', 'Huarocondo, Anta, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1272', '080304', 'Chinchaypujio', 'Chinchaypujio, Anta, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1273', '080303', 'Cachimayo', 'Cachimayo, Anta, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1274', '080302', 'Ancahuasi', 'Ancahuasi, Anta, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1275', '080301', 'Anta', 'Anta, Anta, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1276', '080300', 'Anta', 'Anta, Cusco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1277', '080207', 'Sangarara', 'Sangarara, Acomayo, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1278', '080206', 'Rondocan', 'Rondocan, Acomayo, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1279', '080205', 'Pomacanchi', 'Pomacanchi, Acomayo, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1280', '080204', 'Mosoc Llacta', 'Mosoc Llacta, Acomayo, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1281', '080203', 'Acos', 'Acos, Acomayo, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1282', '080202', 'Acopia', 'Acopia, Acomayo, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1283', '080201', 'Acomayo', 'Acomayo, Acomayo, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1284', '080200', 'Acomayo', 'Acomayo, Cusco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1285', '080108', 'Wanchaq', 'Wanchaq, Cusco, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1286', '080107', 'Saylla', 'Saylla, Cusco, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1287', '080106', 'Santiago', 'Santiago, Cusco, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1288', '080105', 'San Sebastian', 'San Sebastian, Cusco, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1289', '080104', 'San Jeronimo', 'San Jeronimo, Cusco, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1290', '080103', 'Poroy', 'Poroy, Cusco, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1291', '080102', 'Ccorca', 'Ccorca, Cusco, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1292', '080101', 'Cusco', 'Cusco, Cusco, Cusco', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1293', '080100', 'Cusco', 'Cusco, Cusco', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1294', '080000', 'Cusco', 'Cusco', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1295', '070106', 'Ventanilla', 'Ventanilla, Callao, Callao', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1296', '070105', 'La Punta', 'La Punta, Callao, Callao', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1297', '070104', 'La Perla', 'La Perla, Callao, Callao', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1298', '070103', 'Carmen de la Legua Reynoso', 'Carmen de la Legua Reynoso, Callao, Callao', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1299', '070102', 'Bellavista', 'Bellavista, Callao, Callao', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1300', '070101', 'Callao', 'Callao, Callao, Callao', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1301', '070100', 'Callao', 'Callao, Callao', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1302', '070000', 'Callao', 'Callao', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1303', '061311', 'Yauyucan', 'Yauyucan, Santa Cruz, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1304', '061310', 'Uticyacu', 'Uticyacu, Santa Cruz, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1305', '061309', 'Sexi', 'Sexi, Santa Cruz, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1306', '061308', 'Saucepampa', 'Saucepampa, Santa Cruz, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1307', '061307', 'Pulan', 'Pulan, Santa Cruz, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1308', '061306', 'Ninabamba', 'Ninabamba, Santa Cruz, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1309', '061305', 'La Esperanza', 'La Esperanza, Santa Cruz, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1310', '061304', 'Chancaybaños', 'Chancaybaños, Santa Cruz, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1311', '061303', 'Catache', 'Catache, Santa Cruz, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1312', '061302', 'Andabamba', 'Andabamba, Santa Cruz, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1313', '061301', 'Santa Cruz', 'Santa Cruz, Santa Cruz, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1314', '061300', 'Santa Cruz', 'Santa Cruz, Cajamarca', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1315', '061204', 'Tumbaden', 'Tumbaden, San Pablo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1316', '061203', 'San Luis', 'San Luis, San Pablo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1317', '061202', 'San Bernardino', 'San Bernardino, San Pablo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1318', '061201', 'San Pablo', 'San Pablo, San Pablo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1319', '061200', 'San Pablo', 'San Pablo, Cajamarca', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1320', '061113', 'Union Agua Blanca', 'Union Agua Blanca, San Miguel, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1321', '061112', 'Tongod', 'Tongod, San Miguel, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1322', '061111', 'San Silvestre de Cochan', 'San Silvestre de Cochan, San Miguel, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1323', '061110', 'San Gregorio', 'San Gregorio, San Miguel, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1324', '061109', 'Niepos', 'Niepos, San Miguel, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1325', '061108', 'Nanchoc', 'Nanchoc, San Miguel, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1326', '061107', 'Llapa', 'Llapa, San Miguel, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1327', '061106', 'La Florida', 'La Florida, San Miguel, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1328', '061105', 'El Prado', 'El Prado, San Miguel, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1329', '061104', 'Catilluc', 'Catilluc, San Miguel, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1330', '061103', 'Calquis', 'Calquis, San Miguel, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1331', '061102', 'Bolivar', 'Bolivar, San Miguel, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1332', '061101', 'San Miguel', 'San Miguel, San Miguel, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1333', '061100', 'San Miguel', 'San Miguel, Cajamarca', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1334', '061007', 'Jose Sabogal', 'Jose Sabogal, San Marcos, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1335', '061006', 'Jose Manuel Quiroz', 'Jose Manuel Quiroz, San Marcos, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1336', '061005', 'Ichocan', 'Ichocan, San Marcos, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1337', '061004', 'Gregorio Pita', 'Gregorio Pita, San Marcos, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1338', '061003', 'Eduardo Villanueva', 'Eduardo Villanueva, San Marcos, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1339', '061002', 'Chancay', 'Chancay, San Marcos, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1340', '061001', 'Pedro Galvez', 'Pedro Galvez, San Marcos, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1341', '061000', 'San Marcos', 'San Marcos, Cajamarca', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1342', '060907', 'Tabaconas', 'Tabaconas, San Ignacio, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1343', '060906', 'San Jose de Lourdes', 'San Jose de Lourdes, San Ignacio, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1344', '060905', 'Namballe', 'Namballe, San Ignacio, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1345', '060904', 'La Coipa', 'La Coipa, San Ignacio, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1346', '060903', 'Huarango', 'Huarango, San Ignacio, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1347', '060902', 'Chirinos', 'Chirinos, San Ignacio, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1348', '060901', 'San Ignacio', 'San Ignacio, San Ignacio, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1349', '060900', 'San Ignacio', 'San Ignacio, Cajamarca', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1350', '060812', 'Santa Rosa', 'Santa Rosa, Jaen, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1351', '060811', 'San Jose del Alto', 'San Jose del Alto, Jaen, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1352', '060810', 'San Felipe', 'San Felipe, Jaen, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1353', '060809', 'Sallique', 'Sallique, Jaen, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1354', '060808', 'Pucara', 'Pucara, Jaen, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1355', '060807', 'Pomahuaca', 'Pomahuaca, Jaen, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1356', '060806', 'Las Pirias', 'Las Pirias, Jaen, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1357', '060805', 'Huabal', 'Huabal, Jaen, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1358', '060804', 'Colasay', 'Colasay, Jaen, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1359', '060803', 'Chontali', 'Chontali, Jaen, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1360', '060802', 'Bellavista', 'Bellavista, Jaen, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1361', '060801', 'Jaen', 'Jaen, Jaen, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1362', '060800', 'Jaen', 'Jaen, Cajamarca', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1363', '060703', 'Hualgayoc', 'Hualgayoc, Hualgayoc, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1364', '060702', 'Chugur', 'Chugur, Hualgayoc, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1365', '060701', 'Bambamarca', 'Bambamarca, Hualgayoc, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1366', '060700', 'Hualgayoc', 'Hualgayoc, Cajamarca', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1367', '060615', 'Toribio Casanova', 'Toribio Casanova, Cutervo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1368', '060614', 'Socota', 'Socota, Cutervo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1369', '060613', 'Santo Tomas', 'Santo Tomas, Cutervo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1370', '060612', 'Santo Domingo de la Capilla', 'Santo Domingo de la Capilla, Cutervo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1371', '060611', 'Santa Cruz', 'Santa Cruz, Cutervo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1372', '060610', 'San Luis de Lucma', 'San Luis de Lucma, Cutervo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1373', '060609', 'San Juan de Cutervo', 'San Juan de Cutervo, Cutervo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1374', '060608', 'San Andres de Cutervo', 'San Andres de Cutervo, Cutervo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1375', '060607', 'Querocotillo', 'Querocotillo, Cutervo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1376', '060606', 'Pimpingos', 'Pimpingos, Cutervo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1377', '060605', 'La Ramada', 'La Ramada, Cutervo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1378', '060604', 'Cujillo', 'Cujillo, Cutervo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1379', '060603', 'Choros', 'Choros, Cutervo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1380', '060602', 'Callayuc', 'Callayuc, Cutervo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1381', '060601', 'Cutervo', 'Cutervo, Cutervo, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1382', '060600', 'Cutervo', 'Cutervo, Cajamarca', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1383', '060508', 'Yonan', 'Yonan, Contumaza, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1384', '060507', 'Tantarica', 'Tantarica, Contumaza, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1385', '060506', 'Santa Cruz de Toled', 'Santa Cruz de Toled, Contumaza, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1386', '060505', 'San Benito', 'San Benito, Contumaza, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1387', '060504', 'Guzmango', 'Guzmango, Contumaza, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1388', '060503', 'Cupisnique', 'Cupisnique, Contumaza, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1389', '060502', 'Chilete', 'Chilete, Contumaza, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1390', '060501', 'Contumaza', 'Contumaza, Contumaza, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1391', '060500', 'Contumaza', 'Contumaza, Cajamarca', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1392', '060419', 'Chalamarca', 'Chalamarca, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1393', '060418', 'Tocmoche', 'Tocmoche, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1394', '060417', 'Tacabamba', 'Tacabamba, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1395', '060416', 'San Juan de Licupis', 'San Juan de Licupis, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1396', '060415', 'Querocoto', 'Querocoto, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1397', '060414', 'Pion', 'Pion, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1398', '060413', 'Paccha', 'Paccha, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1399', '060412', 'Miracosta', 'Miracosta, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1400', '060411', 'Llama', 'Llama, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1401', '060410', 'Lajas', 'Lajas, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1402', '060409', 'Huambos', 'Huambos, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1403', '060408', 'Conchan', 'Conchan, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1404', '060407', 'Cochabamba', 'Cochabamba, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1405', '060406', 'Choropampa', 'Choropampa, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1406', '060405', 'Chimban', 'Chimban, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1407', '060404', 'Chiguirip', 'Chiguirip, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1408', '060403', 'Chadin', 'Chadin, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1409', '060402', 'Anguia', 'Anguia, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1410', '060401', 'Chota', 'Chota, Chota, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1411', '060400', 'Chota', 'Chota, Cajamarca', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1412', '060312', 'La Libertad de Pallan', 'La Libertad de Pallan, Celendin, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1413', '060311', 'Utco', 'Utco, Celendin, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1414', '060310', 'Sucre', 'Sucre, Celendin, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1415', '060309', 'Sorochuco', 'Sorochuco, Celendin, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1416', '060308', 'Oxamarca', 'Oxamarca, Celendin, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1417', '060307', 'Miguel Iglesias', 'Miguel Iglesias, Celendin, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1418', '060306', 'Jose Galvez', 'Jose Galvez, Celendin, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1419', '060305', 'Jorge Chavez', 'Jorge Chavez, Celendin, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1420', '060304', 'Huasmin', 'Huasmin, Celendin, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1421', '060303', 'Cortegana', 'Cortegana, Celendin, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1422', '060302', 'Chumuch', 'Chumuch, Celendin, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1423', '060301', 'Celendin', 'Celendin, Celendin, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1424', '060300', 'Celendin', 'Celendin, Cajamarca', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1425', '060204', 'Sitacocha', 'Sitacocha, Cajabamba, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1426', '060203', 'Condebamba', 'Condebamba, Cajabamba, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1427', '060202', 'Cachachi', 'Cachachi, Cajabamba, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1428', '060201', 'Cajabamba', 'Cajabamba, Cajabamba, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1429', '060200', 'Cajabamba', 'Cajabamba, Cajamarca', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1430', '060112', 'San Juan', 'San Juan, Cajamarca, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1431', '060111', 'Namora', 'Namora, Cajamarca, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1432', '060110', 'Matara', 'Matara, Cajamarca, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1433', '060109', 'Magdalena', 'Magdalena, Cajamarca, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1434', '060108', 'Los Baños del Inca', 'Los Baños del Inca, Cajamarca, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1435', '060107', 'Llacanora', 'Llacanora, Cajamarca, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1436', '060106', 'Jesus', 'Jesus, Cajamarca, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1437', '060105', 'Encañada', 'Encañada, Cajamarca, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1438', '060104', 'Cospan', 'Cospan, Cajamarca, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1439', '060103', 'Chetilla', 'Chetilla, Cajamarca, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1440', '060102', 'Asuncion', 'Asuncion, Cajamarca, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1441', '060101', 'Cajamarca', 'Cajamarca, Cajamarca, Cajamarca', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1442', '060100', 'Cajamarca', 'Cajamarca, Cajamarca', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1443', '060000', 'Cajamarca', 'Cajamarca', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1444', '051108', 'Vischongo', 'Vischongo, Vilcas Huaman, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1445', '051107', 'Saurama', 'Saurama, Vilcas Huaman, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1446', '051106', 'Independencia', 'Independencia, Vilcas Huaman, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1447', '051105', 'Huambalpa', 'Huambalpa, Vilcas Huaman, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1448', '051104', 'Concepcion', 'Concepcion, Vilcas Huaman, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1449', '051103', 'Carhuanca', 'Carhuanca, Vilcas Huaman, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1450', '051102', 'Accomarca', 'Accomarca, Vilcas Huaman, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1451', '051101', 'Vilcas Huaman', 'Vilcas Huaman, Vilcas Huaman, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1452', '051100', 'Vilcas Huaman', 'Vilcas Huaman, Ayacucho', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1453', '051012', 'Vilcanchos', 'Vilcanchos, Victor Fajardo, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1454', '051011', 'Sarhua', 'Sarhua, Victor Fajardo, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1455', '051010', 'Huaya', 'Huaya, Victor Fajardo, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1456', '051009', 'Huancaraylla', 'Huancaraylla, Victor Fajardo, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1457', '051008', 'Huamanquiquia', 'Huamanquiquia, Victor Fajardo, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1458', '051007', 'Colca', 'Colca, Victor Fajardo, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1459', '051006', 'Cayara', 'Cayara, Victor Fajardo, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1460', '051005', 'Canaria', 'Canaria, Victor Fajardo, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1461', '051004', 'Asquipata', 'Asquipata, Victor Fajardo, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1462', '051003', 'Apongo', 'Apongo, Victor Fajardo, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1463', '051002', 'Alcamenca', 'Alcamenca, Victor Fajardo, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1464', '051001', 'Huancapi', 'Huancapi, Victor Fajardo, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1465', '051000', 'Victor Fajardo', 'Victor Fajardo, Ayacucho', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1466', '050911', 'Soras', 'Soras, Sucre, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1467', '050910', 'Santiago de Paucaray', 'Santiago de Paucaray, Sucre, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1468', '050909', 'San Salvador de Quije', 'San Salvador de Quije, Sucre, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1469', '050908', 'San Pedro de Larcay', 'San Pedro de Larcay, Sucre, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1470', '050907', 'Paico', 'Paico, Sucre, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1471', '050906', 'Morcolla', 'Morcolla, Sucre, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1472', '050905', 'Huacaña', 'Huacaña, Sucre, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1473', '050904', 'Chilcayoc', 'Chilcayoc, Sucre, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1474', '050903', 'Chalcos', 'Chalcos, Sucre, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1475', '050902', 'Belen', 'Belen, Sucre, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1476', '050901', 'Querobamba', 'Querobamba, Sucre, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1477', '050900', 'Sucre', 'Sucre, Ayacucho', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1478', '050810', 'Sara Sara', 'Sara Sara, Paucar del Sara Sara, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1479', '050809', 'San Jose de Ushua', 'San Jose de Ushua, Paucar del Sara Sara, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1480', '050808', 'San Javier de Alpabamba', 'San Javier de Alpabamba, Paucar del Sara Sara, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1481', '050807', 'Pararca', 'Pararca, Paucar del Sara Sara, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1482', '050806', 'Oyolo', 'Oyolo, Paucar del Sara Sara, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1483', '050805', 'Marcabamba', 'Marcabamba, Paucar del Sara Sara, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1484', '050804', 'Lampa', 'Lampa, Paucar del Sara Sara, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1485', '050803', 'Corculla', 'Corculla, Paucar del Sara Sara, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1486', '050802', 'Colta', 'Colta, Paucar del Sara Sara, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1487', '050801', 'Pausa', 'Pausa, Paucar del Sara Sara, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1488', '050800', 'Paucar del Sara Sara', 'Paucar del Sara Sara, Ayacucho', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1489', '050708', 'Upahuacho', 'Upahuacho, Parinacochas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1490', '050707', 'San Francisco de Ravacayco', 'San Francisco de Ravacayco, Parinacochas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1491', '050706', 'Puyusca', 'Puyusca, Parinacochas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1492', '050705', 'Pullo', 'Pullo, Parinacochas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1493', '050704', 'Pacapausa', 'Pacapausa, Parinacochas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1494', '050703', 'Coronel Castañeda', 'Coronel Castañeda, Parinacochas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1495', '050702', 'Chumpi', 'Chumpi, Parinacochas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1496', '050701', 'Coracora', 'Coracora, Parinacochas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1497', '050700', 'Parinacochas', 'Parinacochas, Ayacucho', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1498', '050621', 'Santa Lucia', 'Santa Lucia, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1499', '050620', 'Santa Ana de Huaycahuacho', 'Santa Ana de Huaycahuacho, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1500', '050619', 'Sancos', 'Sancos, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1501', '050618', 'San Pedro de Palco', 'San Pedro de Palco, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1502', '050617', 'San Pedro', 'San Pedro, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1503', '050616', 'San Juan', 'San Juan, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1504', '050615', 'San Cristobal', 'San Cristobal, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1505', '050614', 'Saisa', 'Saisa, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1506', '050613', 'Otoca', 'Otoca, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1507', '050612', 'Ocaña', 'Ocaña, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1508', '050611', 'Lucanas', 'Lucanas, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1509', '050610', 'Llauta', 'Llauta, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1510', '050609', 'Leoncio Prado', 'Leoncio Prado, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1511', '050608', 'Laramate', 'Laramate, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1512', '050607', 'Huac-Huas', 'Huac-Huas, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1513', '050606', 'Chipao', 'Chipao, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1514', '050605', 'Chaviña', 'Chaviña, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1515', '050604', 'Carmen Salcedo', 'Carmen Salcedo, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1516', '050603', 'Cabana', 'Cabana, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1517', '050602', 'Aucara', 'Aucara, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1518', '050601', 'Puquio', 'Puquio, Lucanas, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1519', '050600', 'Lucanas', 'Lucanas, Ayacucho', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1520', '050508', 'Tambo', 'Tambo, La Mar, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1521', '050507', 'Santa Rosa', 'Santa Rosa, La Mar, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1522', '050506', 'Luis Carranza', 'Luis Carranza, La Mar, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1523', '050505', 'Chungui', 'Chungui, La Mar, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1524', '050504', 'Chilcas', 'Chilcas, La Mar, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1525', '050503', 'Ayna', 'Ayna, La Mar, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1526', '050502', 'Anco', 'Anco, La Mar, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1527', '050501', 'San Miguel', 'San Miguel, La Mar, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1528', '050500', 'La Mar', 'La Mar, Ayacucho', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1529', '050408', 'Llochegua', 'Llochegua, Huanta, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1530', '050407', 'Sivia', 'Sivia, Huanta, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1531', '050406', 'Santillana', 'Santillana, Huanta, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1532', '050405', 'Luricocha', 'Luricocha, Huanta, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1533', '050404', 'Iguain', 'Iguain, Huanta, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1534', '050403', 'Huamanguilla', 'Huamanguilla, Huanta, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1535', '050402', 'Ayahuanco', 'Ayahuanco, Huanta, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1536', '050401', 'Huanta', 'Huanta, Huanta, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1537', '050400', 'Huanta', 'Huanta, Ayacucho', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1538', '050304', 'Santiago de Lucanamarca', 'Santiago de Lucanamarca, Huanca Sancos, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1539', '050303', 'Sacsamarca', 'Sacsamarca, Huanca Sancos, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1540', '050302', 'Carapo', 'Carapo, Huanca Sancos, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1541', '050301', 'Sancos', 'Sancos, Huanca Sancos, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1542', '050300', 'Huanca Sancos', 'Huanca Sancos, Ayacucho', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1543', '050206', 'Totos', 'Totos, Cangallo, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1544', '050205', 'Paras', 'Paras, Cangallo, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1545', '050204', 'Maria Parado de Bellido', 'Maria Parado de Bellido, Cangallo, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1546', '050203', 'Los Morochucos', 'Los Morochucos, Cangallo, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1547', '050202', 'Chuschi', 'Chuschi, Cangallo, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1548', '050201', 'Cangallo', 'Cangallo, Cangallo, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1549', '050200', 'Cangallo', 'Cangallo, Ayacucho', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1550', '050115', 'Jesús Nazareno', 'Jesús Nazareno, Huamanga, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1551', '050114', 'Vinchos', 'Vinchos, Huamanga, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1552', '050113', 'Tambillo', 'Tambillo, Huamanga, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1553', '050112', 'Socos', 'Socos, Huamanga, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1554', '050111', 'Santiago de Pischa', 'Santiago de Pischa, Huamanga, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1555', '050110', 'San Juan Bautista', 'San Juan Bautista, Huamanga, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1556', '050109', 'San Jose de Ticllas', 'San Jose de Ticllas, Huamanga, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1557', '050108', 'Quinua', 'Quinua, Huamanga, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1558', '050107', 'Pacaycasa', 'Pacaycasa, Huamanga, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1559', '050106', 'Ocros', 'Ocros, Huamanga, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1560', '050105', 'Chiara', 'Chiara, Huamanga, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1561', '050104', 'Carmen Alto', 'Carmen Alto, Huamanga, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1562', '050103', 'Acos Vinchos', 'Acos Vinchos, Huamanga, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1563', '050102', 'Acocro', 'Acocro, Huamanga, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1564', '050101', 'Ayacucho', 'Ayacucho, Huamanga, Ayacucho', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1565', '050100', 'Huamanga', 'Huamanga, Ayacucho', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1566', '050000', 'Ayacucho', 'Ayacucho', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1567', '040811', 'Toro', 'Toro, La Union, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1568', '040810', 'Tomepampa', 'Tomepampa, La Union, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1569', '040809', 'Tauria', 'Tauria, La Union, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1570', '040808', 'Sayla', 'Sayla, La Union, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1571', '040807', 'Quechualla', 'Quechualla, La Union, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1572', '040806', 'Puyca', 'Puyca, La Union, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1573', '040805', 'Pampamarca', 'Pampamarca, La Union, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1574', '040804', 'Huaynacotas', 'Huaynacotas, La Union, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1575', '040803', 'Charcana', 'Charcana, La Union, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1576', '040802', 'Alca', 'Alca, La Union, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1577', '040801', 'Cotahuasi', 'Cotahuasi, La Union, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1578', '040800', 'La Union', 'La Union, Arequipa', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1579', '040706', 'Punta de Bombon', 'Punta de Bombon, Islay, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1580', '040705', 'Mejia', 'Mejia, Islay, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1581', '040704', 'Islay', 'Islay, Islay, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1582', '040703', 'Dean Valdivia', 'Dean Valdivia, Islay, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1583', '040702', 'Cocachacra', 'Cocachacra, Islay, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1584', '040701', 'Mollendo', 'Mollendo, Islay, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1585', '040700', 'Islay', 'Islay, Arequipa', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1586', '040608', 'Yanaquihua', 'Yanaquihua, Condesuyos, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1587', '040607', 'Salamanca', 'Salamanca, Condesuyos, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1588', '040606', 'Rio Grande', 'Rio Grande, Condesuyos, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1589', '040605', 'Iray', 'Iray, Condesuyos, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1590', '040604', 'Chichas', 'Chichas, Condesuyos, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1591', '040603', 'Cayarani', 'Cayarani, Condesuyos, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1592', '040602', 'Andaray', 'Andaray, Condesuyos, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1593', '040601', 'Chuquibamba', 'Chuquibamba, Condesuyos, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1594', '040600', 'Condesuyos', 'Condesuyos, Arequipa', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1595', '040520', 'Majes', 'Majes, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1596', '040519', 'Yanque', 'Yanque, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1597', '040518', 'Tuti', 'Tuti, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1598', '040517', 'Tisco', 'Tisco, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1599', '040516', 'Tapay', 'Tapay, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1600', '040515', 'Sibayo', 'Sibayo, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1601', '040514', 'San Antonio de Chuca', 'San Antonio de Chuca, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1602', '040513', 'Madrigal', 'Madrigal, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1603', '040512', 'Maca', 'Maca, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1604', '040511', 'Lluta', 'Lluta, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1605', '040510', 'Lari', 'Lari, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1606', '040509', 'Ichupampa', 'Ichupampa, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1607', '040508', 'Huanca', 'Huanca, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1608', '040507', 'Huambo', 'Huambo, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1609', '040506', 'Coporaque', 'Coporaque, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1610', '040505', 'Caylloma', 'Caylloma, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1611', '040504', 'Callalli', 'Callalli, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1612', '040503', 'Cabanaconde', 'Cabanaconde, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1613', '040502', 'Achoma', 'Achoma, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1614', '040501', 'Chivay', 'Chivay, Caylloma, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1615', '040500', 'Caylloma', 'Caylloma, Arequipa', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1616', '040420', 'Majes', 'Majes, Castilla, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1617', '040419', 'Yanque', 'Yanque, Castilla, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1618', '040414', 'Viraco', 'Viraco, Castilla, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1619', '040413', 'Uraca', 'Uraca, Castilla, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1620', '040412', 'Uñon', 'Uñon, Castilla, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1621', '040411', 'Tipan', 'Tipan, Castilla, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1622', '040410', 'Pampacolca', 'Pampacolca, Castilla, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1623', '040409', 'Orcopampa', 'Orcopampa, Castilla, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1624', '040408', 'Machaguay', 'Machaguay, Castilla, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1625', '040407', 'Huancarqui', 'Huancarqui, Castilla, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1626', '040406', 'Choco', 'Choco, Castilla, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1627', '040405', 'Chilcaymarca', 'Chilcaymarca, Castilla, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1628', '040404', 'Chachas', 'Chachas, Castilla, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1629', '040403', 'Ayo', 'Ayo, Castilla, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1630', '040402', 'Andagua', 'Andagua, Castilla, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1631', '040401', 'Aplao', 'Aplao, Castilla, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1632', '040400', 'Castilla', 'Castilla, Arequipa', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1633', '040313', 'Yauca', 'Yauca, Caraveli, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1634', '040312', 'Quicacha', 'Quicacha, Caraveli, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1635', '040311', 'Lomas', 'Lomas, Caraveli, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1636', '040310', 'Jaqui', 'Jaqui, Caraveli, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1637', '040309', 'Huanuhuanu', 'Huanuhuanu, Caraveli, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1638', '040308', 'Chaparra', 'Chaparra, Caraveli, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1639', '040307', 'Chala', 'Chala, Caraveli, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1640', '040306', 'Cahuacho', 'Cahuacho, Caraveli, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1641', '040305', 'Bella Union', 'Bella Union, Caraveli, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1642', '040304', 'Atiquipa', 'Atiquipa, Caraveli, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1643', '040303', 'Atico', 'Atico, Caraveli, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1644', '040302', 'Acari', 'Acari, Caraveli, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1645', '040301', 'Caraveli', 'Caraveli, Caraveli, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1646', '040300', 'Caraveli', 'Caraveli, Arequipa', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1647', '040208', 'Samuel Pastor', 'Samuel Pastor, Camana, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1648', '040207', 'Quilca', 'Quilca, Camana, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1649', '040206', 'Ocoña', 'Ocoña, Camana, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1650', '040205', 'Nicolas de Pierola', 'Nicolas de Pierola, Camana, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1651', '040204', 'Mariscal Caceres', 'Mariscal Caceres, Camana, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1652', '040203', 'Mariano Nicolas Valcarcel', 'Mariano Nicolas Valcarcel, Camana, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1653', '040202', 'Jose Maria Quimper', 'Jose Maria Quimper, Camana, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1654', '040201', 'Camana', 'Camana, Camana, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1655', '040200', 'Camana', 'Camana, Arequipa', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1656', '040129', 'Jose Luis Bustamante y Rivero', 'Jose Luis Bustamante y Rivero, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1657', '040128', 'Yura', 'Yura, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1658', '040127', 'Yarabamba', 'Yarabamba, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1659', '040126', 'Yanahuara', 'Yanahuara, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1660', '040125', 'Vitor', 'Vitor, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1661', '040124', 'Uchumayo', 'Uchumayo, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1662', '040123', 'Tiabaya', 'Tiabaya, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1663', '040122', 'Socabaya', 'Socabaya, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1664', '040121', 'Santa Rita de Siguas', 'Santa Rita de Siguas, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1665', '040120', 'Santa Isabel de Siguas', 'Santa Isabel de Siguas, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1666', '040119', 'San Juan de Tarucani', 'San Juan de Tarucani, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1667', '040118', 'San Juan de Siguas', 'San Juan de Siguas, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1668', '040117', 'Sachaca', 'Sachaca, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1669', '040116', 'Sabandia', 'Sabandia, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1670', '040115', 'Quequeña', 'Quequeña, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1671', '040114', 'Polobaya', 'Polobaya, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1672', '040113', 'Pocsi', 'Pocsi, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1673', '040112', 'Paucarpata', 'Paucarpata, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1674', '040111', 'Mollebaya', 'Mollebaya, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1675', '040110', 'Miraflores', 'Miraflores, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1676', '040109', 'Mariano Melgar', 'Mariano Melgar, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1677', '040108', 'La Joya', 'La Joya, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1678', '040107', 'Jacobo Hunter', 'Jacobo Hunter, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1679', '040106', 'Chiguata', 'Chiguata, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1680', '040105', 'Characato', 'Characato, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1681', '040104', 'Cerro Colorado', 'Cerro Colorado, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1682', '040103', 'Cayma', 'Cayma, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1683', '040102', 'Alto Selva Alegre', 'Alto Selva Alegre, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1684', '040101', 'Arequipa', 'Arequipa, Arequipa, Arequipa', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1685', '040100', 'Arequipa', 'Arequipa, Arequipa', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1686', '040000', 'Arequipa', 'Arequipa', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1687', '030714', 'Curasco', 'Curasco, Grau, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1688', '030713', 'Virundo', 'Virundo, Grau, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1689', '030712', 'Vilcabamba', 'Vilcabamba, Grau, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1690', '030711', 'Turpay', 'Turpay, Grau, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1691', '030710', 'Santa Rosa', 'Santa Rosa, Grau, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1692', '030709', 'San Antonio', 'San Antonio, Grau, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1693', '030708', 'Progreso', 'Progreso, Grau, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1694', '030707', 'Pataypampa', 'Pataypampa, Grau, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1695', '030706', 'Micaela Bastidas', 'Micaela Bastidas, Grau, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1696', '030705', 'Mamara', 'Mamara, Grau, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1697', '030704', 'Huayllati', 'Huayllati, Grau, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1698', '030703', 'Gamarra', 'Gamarra, Grau, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1699', '030702', 'Curpahuasi', 'Curpahuasi, Grau, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1700', '030701', 'Chuquibambilla', 'Chuquibambilla, Grau, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1701', '030700', 'Grau', 'Grau, Apurimac', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1702', '030608', 'Ranracancha', 'Ranracancha, Chincheros, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1703', '030607', 'Uranmarca', 'Uranmarca, Chincheros, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1704', '030606', 'Ongoy', 'Ongoy, Chincheros, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1705', '030605', 'Ocobamba', 'Ocobamba, Chincheros, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1706', '030604', 'Huaccana', 'Huaccana, Chincheros, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1707', '030603', 'Cocharcas', 'Cocharcas, Chincheros, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1708', '030602', 'Anco-Huallo', 'Anco-Huallo, Chincheros, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1709', '030601', 'Chincheros', 'Chincheros, Chincheros, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1710', '030600', 'Chincheros', 'Chincheros, Apurimac', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1711', '030506', 'Challhuahuacho', 'Challhuahuacho, Cotabambas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1712', '030505', 'Mara', 'Mara, Cotabambas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1713', '030504', 'Haquira', 'Haquira, Cotabambas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1714', '030503', 'Coyllurqui', 'Coyllurqui, Cotabambas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1715', '030502', 'Cotabambas', 'Cotabambas, Cotabambas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1716', '030501', 'Tambobamba', 'Tambobamba, Cotabambas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1717', '030500', 'Cotabambas', 'Cotabambas, Apurimac', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1718', '030417', 'Yanaca', 'Yanaca, Aymaraes, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1719', '030416', 'Toraya', 'Toraya, Aymaraes, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1720', '030415', 'Tintay', 'Tintay, Aymaraes, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1721', '030414', 'Tapairihua', 'Tapairihua, Aymaraes, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1722', '030413', 'Soraya', 'Soraya, Aymaraes, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1723', '030412', 'Sañayca', 'Sañayca, Aymaraes, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1724', '030411', 'San Juan de Chacña', 'San Juan de Chacña, Aymaraes, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1725', '030410', 'Pocohuanca', 'Pocohuanca, Aymaraes, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1726', '030409', 'Lucre', 'Lucre, Aymaraes, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1727', '030408', 'Justo Apu Sahuaraura', 'Justo Apu Sahuaraura, Aymaraes, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1728', '030407', 'Huayllo', 'Huayllo, Aymaraes, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1729', '030406', 'Cotaruse', 'Cotaruse, Aymaraes, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1730', '030405', 'Colcabamba', 'Colcabamba, Aymaraes, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1731', '030404', 'Chapimarca', 'Chapimarca, Aymaraes, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1732', '030403', 'Caraybamba', 'Caraybamba, Aymaraes, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1733', '030402', 'Capaya', 'Capaya, Aymaraes, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1734', '030401', 'Chalhuanca', 'Chalhuanca, Aymaraes, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1735', '030400', 'Aymaraes', 'Aymaraes, Apurimac', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1736', '030307', 'Sabaino', 'Sabaino, Antabamba, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1737', '030306', 'Pachaconas', 'Pachaconas, Antabamba, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1738', '030305', 'Oropesa', 'Oropesa, Antabamba, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1739', '030304', 'Juan Espinoza Medrano', 'Juan Espinoza Medrano, Antabamba, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1740', '030303', 'Huaquirca', 'Huaquirca, Antabamba, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1741', '030302', 'El Oro', 'El Oro, Antabamba, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1742', '030301', 'Antabamba', 'Antabamba, Antabamba, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1743', '030300', 'Antabamba', 'Antabamba, Apurimac', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1744', '030219', 'Kaquiabamba', 'Kaquiabamba, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1745', '030218', 'Turpo', 'Turpo, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1746', '030217', 'Tumay Huaraca', 'Tumay Huaraca, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1747', '030216', 'Talavera', 'Talavera, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1748', '030215', 'Santa Maria de Chicmo', 'Santa Maria de Chicmo, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1749', '030214', 'San Miguel de Chaccrampa', 'San Miguel de Chaccrampa, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1750', '030213', 'San Jeronimo', 'San Jeronimo, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1751', '030212', 'San Antonio de Cachi', 'San Antonio de Cachi, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1752', '030211', 'Pomacocha', 'Pomacocha, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1753', '030210', 'Pampachiri', 'Pampachiri, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1754', '030209', 'Pacucha', 'Pacucha, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1755', '030208', 'Pacobamba', 'Pacobamba, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1756', '030207', 'Kishuara', 'Kishuara, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1757', '030206', 'Huayana', 'Huayana, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1758', '030205', 'Huancaray', 'Huancaray, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1759', '030204', 'Huancarama', 'Huancarama, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1760', '030203', 'Chiara', 'Chiara, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1761', '030202', 'Andarapa', 'Andarapa, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1762', '030201', 'Andahuaylas', 'Andahuaylas, Andahuaylas, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1763', '030200', 'Andahuaylas', 'Andahuaylas, Apurimac', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1764', '030109', 'Tamburco', 'Tamburco, Abancay, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1765', '030108', 'San Pedro de Cachora', 'San Pedro de Cachora, Abancay, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1766', '030107', 'Pichirhua', 'Pichirhua, Abancay, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1767', '030106', 'Lambrama', 'Lambrama, Abancay, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1768', '030105', 'Huanipaca', 'Huanipaca, Abancay, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1769', '030104', 'Curahuasi', 'Curahuasi, Abancay, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1770', '030103', 'Circa', 'Circa, Abancay, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1771', '030102', 'Chacoche', 'Chacoche, Abancay, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1772', '030101', 'Abancay', 'Abancay, Abancay, Apurimac', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1773', '030100', 'Abancay', 'Abancay, Apurimac', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1774', '030000', 'Apurimac', 'Apurimac', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1775', '022008', 'Yanama', 'Yanama, Yungay, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1776', '022007', 'Shupluy', 'Shupluy, Yungay, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1777', '022006', 'Ranrahirca', 'Ranrahirca, Yungay, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1778', '022005', 'Quillo', 'Quillo, Yungay, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1779', '022004', 'Matacoto', 'Matacoto, Yungay, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1780', '022003', 'Mancos', 'Mancos, Yungay, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1781', '022002', 'Cascapara', 'Cascapara, Yungay, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1782', '022001', 'Yungay', 'Yungay, Yungay, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1783', '022000', 'Yungay', 'Yungay, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1784', '021910', 'Sicsibamba', 'Sicsibamba, Sihuas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1785', '021909', 'San Juan', 'San Juan, Sihuas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1786', '021908', 'Ragash', 'Ragash, Sihuas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1787', '021907', 'Quiches', 'Quiches, Sihuas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1788', '021906', 'Huayllabamba', 'Huayllabamba, Sihuas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1789', '021905', 'Chingalpo', 'Chingalpo, Sihuas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1790', '021904', 'Cashapampa', 'Cashapampa, Sihuas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1791', '021903', 'Alfonso Ugarte', 'Alfonso Ugarte, Sihuas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1792', '021902', 'Acobamba', 'Acobamba, Sihuas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1793', '021901', 'Sihuas', 'Sihuas, Sihuas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1794', '021900', 'Sihuas', 'Sihuas, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1795', '021809', 'Nuevo Chimbote', 'Nuevo Chimbote, Santa, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1796', '021808', 'Santa', 'Santa, Santa, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1797', '021807', 'Samanco', 'Samanco, Santa, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1798', '021806', 'Nepeña', 'Nepeña, Santa, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1799', '021805', 'Moro', 'Moro, Santa, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1800', '021804', 'Macate', 'Macate, Santa, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1801', '021803', 'Coishco', 'Coishco, Santa, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1802', '021802', 'Caceres del Peru', 'Caceres del Peru, Santa, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1803', '021801', 'Chimbote', 'Chimbote, Santa, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1804', '021800', 'Santa', 'Santa, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1805', '021710', 'Ticapampa', 'Ticapampa, Recuay, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1806', '021709', 'Tapacocha', 'Tapacocha, Recuay, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1807', '021708', 'Pararin', 'Pararin, Recuay, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1808', '021707', 'Pampas Chico', 'Pampas Chico, Recuay, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1809', '021706', 'Marca', 'Marca, Recuay, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1810', '021705', 'Llacllin', 'Llacllin, Recuay, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1811', '021704', 'Huayllapampa', 'Huayllapampa, Recuay, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1812', '021703', 'Cotaparaco', 'Cotaparaco, Recuay, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1813', '021702', 'Catac', 'Catac, Recuay, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1814', '021701', 'Recuay', 'Recuay, Recuay, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1815', '021700', 'Recuay', 'Recuay, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1816', '021604', 'Quinuabamba', 'Quinuabamba, Pomabamba, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1817', '021603', 'Parobamba', 'Parobamba, Pomabamba, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1818', '021602', 'Huayllan', 'Huayllan, Pomabamba, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1819', '021601', 'Pomabamba', 'Pomabamba, Pomabamba, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1820', '021600', 'Pomabamba', 'Pomabamba, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1821', '021511', 'Tauca', 'Tauca, Pallasca, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1822', '021510', 'Santa Rosa', 'Santa Rosa, Pallasca, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1823', '021509', 'Pampas', 'Pampas, Pallasca, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1824', '021508', 'Pallasca', 'Pallasca, Pallasca, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1825', '021507', 'Llapo', 'Llapo, Pallasca, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1826', '021506', 'Lacabamba', 'Lacabamba, Pallasca, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1827', '021505', 'Huandoval', 'Huandoval, Pallasca, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1828', '021504', 'Huacaschuque', 'Huacaschuque, Pallasca, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1829', '021503', 'Conchucos', 'Conchucos, Pallasca, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1830', '021502', 'Bolognesi', 'Bolognesi, Pallasca, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1831', '021501', 'Cabana', 'Cabana, Pallasca, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1832', '021500', 'Pallasca', 'Pallasca, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1833', '021410', 'Santiago de Chilcas', 'Santiago de Chilcas, Ocros, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1834', '021409', 'San Pedro', 'San Pedro, Ocros, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1835', '021408', 'San Cristobal de Rajan', 'San Cristobal de Rajan, Ocros, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1836', '021407', 'Llipa', 'Llipa, Ocros, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1837', '021406', 'Congas', 'Congas, Ocros, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1838', '021405', 'Cochas', 'Cochas, Ocros, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1839', '021404', 'Carhuapampa', 'Carhuapampa, Ocros, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1840', '021403', 'Cajamarquilla', 'Cajamarquilla, Ocros, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1841', '021402', 'Acas', 'Acas, Ocros, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1842', '021401', 'Ocros', 'Ocros, Ocros, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1843', '021400', 'Ocros', 'Ocros, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1844', '021308', 'Musga', 'Musga, Mariscal Luzuriaga, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1845', '021307', 'Lucma', 'Lucma, Mariscal Luzuriaga, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1846', '021306', 'Llumpa', 'Llumpa, Mariscal Luzuriaga, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1847', '021305', 'Llama', 'Llama, Mariscal Luzuriaga, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1848', '021304', 'Fidel Olivas Escudero', 'Fidel Olivas Escudero, Mariscal Luzuriaga, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1849', '021303', 'Eleazar Guzman Barron', 'Eleazar Guzman Barron, Mariscal Luzuriaga, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1850', '021302', 'Casca', 'Casca, Mariscal Luzuriaga, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1851', '021301', 'Piscobamba', 'Piscobamba, Mariscal Luzuriaga, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1852', '021300', 'Mariscal Luzuriaga', 'Mariscal Luzuriaga, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1853', '021210', 'Yuracmarca', 'Yuracmarca, Huaylas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1854', '021209', 'Santo Toribio', 'Santo Toribio, Huaylas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1855', '021208', 'Santa Cruz', 'Santa Cruz, Huaylas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1856', '021207', 'Pueblo Libre', 'Pueblo Libre, Huaylas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1857', '021206', 'Pamparomas', 'Pamparomas, Huaylas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1858', '021205', 'Mato', 'Mato, Huaylas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1859', '021204', 'Huaylas', 'Huaylas, Huaylas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1860', '021203', 'Huata', 'Huata, Huaylas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1861', '021202', 'Huallanca', 'Huallanca, Huaylas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1862', '021201', 'Caraz', 'Caraz, Huaylas, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1863', '021200', 'Huaylas', 'Huaylas, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1864', '021105', 'Malvas', 'Malvas, Huarmey, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1865', '021104', 'Huayan', 'Huayan, Huarmey, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1866', '021103', 'Culebras', 'Culebras, Huarmey, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1867', '021102', 'Cochapeti', 'Cochapeti, Huarmey, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1868', '021101', 'Huarmey', 'Huarmey, Huarmey, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1869', '021100', 'Huarmey', 'Huarmey, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1870', '021016', 'Uco', 'Uco, Huari, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1871', '021015', 'San Pedro de Chana', 'San Pedro de Chana, Huari, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1872', '021014', 'San Marcos', 'San Marcos, Huari, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1873', '021013', 'Rapayan', 'Rapayan, Huari, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1874', '021012', 'Rahuapampa', 'Rahuapampa, Huari, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1875', '021011', 'Ponto', 'Ponto, Huari, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1876', '021010', 'Paucas', 'Paucas, Huari, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1877', '021009', 'Masin', 'Masin, Huari, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1878', '021008', 'Huantar', 'Huantar, Huari, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1879', '021007', 'Huachis', 'Huachis, Huari, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1880', '021006', 'Huacchis', 'Huacchis, Huari, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1881', '021005', 'Huacachi', 'Huacachi, Huari, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1882', '021004', 'Chavin de Huantar', 'Chavin de Huantar, Huari, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1883', '021003', 'Cajay', 'Cajay, Huari, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1884', '021002', 'Anra', 'Anra, Huari, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1885', '021001', 'Huari', 'Huari, Huari, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1886', '021000', 'Huari', 'Huari, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1887', '020907', 'Yupan', 'Yupan, Corongo, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1888', '020906', 'Yanac', 'Yanac, Corongo, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1889', '020905', 'La Pampa', 'La Pampa, Corongo, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1890', '020904', 'Cusca', 'Cusca, Corongo, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1891', '020903', 'Bambas', 'Bambas, Corongo, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1892', '020902', 'Aco', 'Aco, Corongo, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1893', '020901', 'Corongo', 'Corongo, Corongo, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1894', '020900', 'Corongo', 'Corongo, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1895', '020804', 'Yautan', 'Yautan, Casma, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1896', '020803', 'Comandante Noel', 'Comandante Noel, Casma, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1897', '020802', 'Buena Vista Alta', 'Buena Vista Alta, Casma, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1898', '020801', 'Casma', 'Casma, Casma, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1899', '020800', 'Casma', 'Casma, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1900', '020703', 'Yauya', 'Yauya, Carlos Fermin Fitzcarrald, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1901', '020702', 'San Nicolas', 'San Nicolas, Carlos Fermin Fitzcarrald, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1902', '020701', 'San Luis', 'San Luis, Carlos Fermin Fitzcarrald, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1903', '020700', 'Carlos Fermin Fitzcarrald', 'Carlos Fermin Fitzcarrald, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1904', '020611', 'Yungar', 'Yungar, Carhuaz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1905', '020610', 'Tinco', 'Tinco, Carhuaz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1906', '020609', 'Shilla', 'Shilla, Carhuaz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1907', '020608', 'San Miguel de Aco', 'San Miguel de Aco, Carhuaz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1908', '020607', 'Pariahuanca', 'Pariahuanca, Carhuaz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1909', '020606', 'Marcara', 'Marcara, Carhuaz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1910', '020605', 'Ataquero', 'Ataquero, Carhuaz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1911', '020604', 'Anta', 'Anta, Carhuaz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1912', '020603', 'Amashca', 'Amashca, Carhuaz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1913', '020602', 'Acopampa', 'Acopampa, Carhuaz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1914', '020601', 'Carhuaz', 'Carhuaz, Carhuaz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1915', '020600', 'Carhuaz', 'Carhuaz, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1916', '020515', 'Ticllos', 'Ticllos, Bolognesi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1917', '020514', 'San Miguel de Corpanqui', 'San Miguel de Corpanqui, Bolognesi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1918', '020513', 'Pacllon', 'Pacllon, Bolognesi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1919', '020512', 'Mangas', 'Mangas, Bolognesi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1920', '020511', 'La Primavera', 'La Primavera, Bolognesi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1921', '020510', 'Huayllacayan', 'Huayllacayan, Bolognesi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1922', '020509', 'Huasta', 'Huasta, Bolognesi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1923', '020508', 'Huallanca', 'Huallanca, Bolognesi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1924', '020507', 'Colquioc', 'Colquioc, Bolognesi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1925', '020506', 'Canis', 'Canis, Bolognesi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1926', '020505', 'Cajacay', 'Cajacay, Bolognesi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1927', '020504', 'Aquia', 'Aquia, Bolognesi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1928', '020503', 'Antonio Raymondi', 'Antonio Raymondi, Bolognesi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1929', '020502', 'Abelardo Pardo Lezameta', 'Abelardo Pardo Lezameta, Bolognesi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1930', '020501', 'Chiquian', 'Chiquian, Bolognesi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1931', '020500', 'Bolognesi', 'Bolognesi, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1932', '020402', 'Acochaca', 'Acochaca, Asuncion, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1933', '020401', 'Chacas', 'Chacas, Asuncion, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1934', '020400', 'Asuncion', 'Asuncion, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1935', '020306', 'San Juan de Rontoy', 'San Juan de Rontoy, Antonio Raymondi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1936', '020305', 'Mirgas', 'Mirgas, Antonio Raymondi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1937', '020304', 'Chingas', 'Chingas, Antonio Raymondi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1938', '020303', 'Chaccho', 'Chaccho, Antonio Raymondi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1939', '020302', 'Aczo', 'Aczo, Antonio Raymondi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1940', '020301', 'Llamellin', 'Llamellin, Antonio Raymondi, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1941', '020300', 'Antonio Raymondi', 'Antonio Raymondi, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1942', '020205', 'Succha', 'Succha, Aija, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1943', '020204', 'La Merced', 'La Merced, Aija, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1944', '020203', 'Huacllan', 'Huacllan, Aija, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1945', '020202', 'Coris', 'Coris, Aija, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1946', '020201', 'Aija', 'Aija, Aija, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1947', '020200', 'Aija', 'Aija, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1948', '020112', 'Tarica', 'Tarica, Huaraz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1949', '020111', 'Pira', 'Pira, Huaraz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1950', '020110', 'Pariacoto', 'Pariacoto, Huaraz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1951', '020109', 'Pampas', 'Pampas, Huaraz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1952', '020108', 'Olleros', 'Olleros, Huaraz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1953', '020107', 'La Libertad', 'La Libertad, Huaraz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1954', '020106', 'Jangas', 'Jangas, Huaraz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1955', '020105', 'Independencia', 'Independencia, Huaraz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1956', '020104', 'Huanchay', 'Huanchay, Huaraz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1957', '020103', 'Colcabamba', 'Colcabamba, Huaraz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1958', '020102', 'Cochabamba', 'Cochabamba, Huaraz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1959', '020101', 'Huaraz', 'Huaraz, Huaraz, Ancash', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1960', '020100', 'Huaraz', 'Huaraz, Ancash', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1961', '020000', 'Ancash', 'Ancash', '1', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1962', '010707', 'Yamon', 'Yamon, Utcubamba, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1963', '010706', 'Lonya Grande', 'Lonya Grande, Utcubamba, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1964', '010705', 'Jamalca', 'Jamalca, Utcubamba, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1965', '010704', 'El Milagro', 'El Milagro, Utcubamba, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1966', '010703', 'Cumba', 'Cumba, Utcubamba, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1967', '010702', 'Cajaruro', 'Cajaruro, Utcubamba, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1968', '010701', 'Bagua Grande', 'Bagua Grande, Utcubamba, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1969', '010700', 'Utcubamba', 'Utcubamba, Amazonas', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1970', '010612', 'Vista Alegre', 'Vista Alegre, Rodriguez de Mendoza, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1971', '010611', 'Totora', 'Totora, Rodriguez de Mendoza, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1972', '010610', 'Santa Rosa', 'Santa Rosa, Rodriguez de Mendoza, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1973', '010609', 'Omia', 'Omia, Rodriguez de Mendoza, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1974', '010608', 'Milpuc', 'Milpuc, Rodriguez de Mendoza, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1975', '010607', 'Mariscal Benavides', 'Mariscal Benavides, Rodriguez de Mendoza, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1976', '010606', 'Longar', 'Longar, Rodriguez de Mendoza, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1977', '010605', 'Limabamba', 'Limabamba, Rodriguez de Mendoza, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1978', '010604', 'Huambo', 'Huambo, Rodriguez de Mendoza, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1979', '010603', 'Cochamal', 'Cochamal, Rodriguez de Mendoza, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1980', '010602', 'Chirimoto', 'Chirimoto, Rodriguez de Mendoza, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1981', '010601', 'San Nicolas', 'San Nicolas, Rodriguez de Mendoza, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1982', '010600', 'Rodriguez de Mendoza', 'Rodriguez de Mendoza, Amazonas', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1983', '010523', 'Trita', 'Trita, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1984', '010522', 'Tingo', 'Tingo, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1985', '010521', 'Santo Tomas', 'Santo Tomas, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1986', '010520', 'Santa Catalina', 'Santa Catalina, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1987', '010519', 'San Juan de Lopecancha', 'San Juan de Lopecancha, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1988', '010518', 'San Jeronimo', 'San Jeronimo, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1989', '010517', 'San Francisco del Yeso', 'San Francisco del Yeso, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1990', '010516', 'San Cristobal', 'San Cristobal, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1991', '010515', 'Providencia', 'Providencia, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1992', '010514', 'Pisuquia', 'Pisuquia, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1993', '010513', 'Ocumal', 'Ocumal, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1994', '010512', 'Ocalli', 'Ocalli, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1995', '010511', 'Maria', 'Maria, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1996', '010510', 'Luya Viejo', 'Luya Viejo, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1997', '010509', 'Luya', 'Luya, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1998', '010508', 'Lonya Chico', 'Lonya Chico, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('1999', '010507', 'Longuita', 'Longuita, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2000', '010506', 'Inguilpata', 'Inguilpata, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2001', '010505', 'Conila', 'Conila, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2002', '010504', 'Colcamar', 'Colcamar, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2003', '010503', 'Cocabamba', 'Cocabamba, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2004', '010502', 'Camporredondo', 'Camporredondo, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2005', '010501', 'Lamud', 'Lamud, Luya, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2006', '010500', 'Luya', 'Luya, Amazonas', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2007', '010403', 'Rio Santiago', 'Rio Santiago, Condorcanqui, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2008', '010402', 'El Cenepa', 'El Cenepa, Condorcanqui, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2009', '010401', 'Nieva', 'Nieva, Condorcanqui, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2010', '010400', 'Condorcanqui', 'Condorcanqui, Amazonas', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2011', '010312', 'Jazan', 'Jazan, Bongara, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2012', '010311', 'Yambrasbamba', 'Yambrasbamba, Bongara, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2013', '010310', 'Valera', 'Valera, Bongara, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2014', '010309', 'Shipasbamba', 'Shipasbamba, Bongara, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2015', '010308', 'San Carlos', 'San Carlos, Bongara, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2016', '010307', 'Recta', 'Recta, Bongara, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2017', '010306', 'Florida', 'Florida, Bongara, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2018', '010305', 'Churuja', 'Churuja, Bongara, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2019', '010304', 'Chisquilla', 'Chisquilla, Bongara, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2020', '010303', 'Cuispes', 'Cuispes, Bongara, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2021', '010302', 'Corosha', 'Corosha, Bongara, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2022', '010301', 'Jumbilla', 'Jumbilla, Bongara, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2023', '010300', 'Bongara', 'Bongara, Amazonas', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2024', '010205', 'Imaza', 'Imaza, Bagua, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2025', '010204', 'El Parco', 'El Parco, Bagua, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2026', '010203', 'Copallin', 'Copallin, Bagua, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2027', '010202', 'Aramango', 'Aramango, Bagua, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2028', '010201', 'La Peca', 'La Peca, Bagua, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2029', '010200', 'Bagua', 'Bagua, Amazonas', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2030', '010121', 'Sonche', 'Sonche, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2031', '010120', 'Soloco', 'Soloco, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2032', '010119', 'San Isidro de Maino', 'San Isidro de Maino, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2033', '010118', 'San Francisco de Daguas', 'San Francisco de Daguas, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2034', '010117', 'Quinjalca', 'Quinjalca, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2035', '010116', 'Olleros', 'Olleros, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2036', '010115', 'Montevideo', 'Montevideo, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2037', '010114', 'Molinopampa', 'Molinopampa, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2038', '010113', 'Mariscal Castilla', 'Mariscal Castilla, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2039', '010112', 'Magdalena', 'Magdalena, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2040', '010111', 'Levanto', 'Levanto, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2041', '010110', 'Leimebamba', 'Leimebamba, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2042', '010109', 'La Jalca', 'La Jalca, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2043', '010108', 'Huancas', 'Huancas, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2044', '010107', 'Granada', 'Granada, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2045', '010106', 'Chuquibamba', 'Chuquibamba, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2046', '010105', 'Chiliquin', 'Chiliquin, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2047', '010104', 'Cheto', 'Cheto, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2048', '010103', 'Balsas', 'Balsas, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2049', '010102', 'Asuncion', 'Asuncion, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2050', '010101', 'Chachapoyas', 'Chachapoyas, Chachapoyas, Amazonas', '3', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2051', '010100', 'Chachapoyas', 'Chachapoyas, Amazonas', '2', '1', null, null, '1');
INSERT INTO `ubigeo` VALUES ('2052', '010000', 'Amazonas', 'Amazonas', '1', '1', null, null, '1');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `persona_id` int(10) unsigned NOT NULL,
  `rol_id` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `persona_id_padre` int(10) unsigned NOT NULL DEFAULT '0',
  `alias` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` smallint(6) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_rol_id_foreign` (`rol_id`),
  KEY `users_persona_id_foreign` (`persona_id`),
  KEY `users_persona_id_padre_index` (`persona_id_padre`),
  CONSTRAINT `users_persona_id_foreign` FOREIGN KEY (`persona_id`) REFERENCES `persona` (`id`),
  CONSTRAINT `users_rol_id_foreign` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '2', '1', 'armandoaepp@gmail.com', '$2y$10$meiLK6ua2tBj3mhf.42qnudtPBPRYdSgh23b4q23PK6gUy4yiPQ/m', '1', 'armandoaepp', 'UEW9UohBu3GzlDTz2C1JV7x9mvnb8K2yUiC9OGDeszSBPhtDZcvpKd5vbaCK', '1', null, '2016-05-21 20:50:08');
INSERT INTO `users` VALUES ('2', '4', '2', 'admin@admin.com', '$2y$10$UfpTNEnNVLIHyMohZ14FMu5/h9JacWtnAgRYJE7..01p63bxIruPa', '3', 'armandoaepp', 'GylF23lq9XN6WfWgwrTwMxXd1RgPGVKm7gEcJ7nVxvgFrmkdfKAddVvMxwXF', '1', '2016-02-16 13:02:45', '2016-06-06 12:23:36');
INSERT INTO `users` VALUES ('3', '5', '3', 'egalvez@prestamosdelnortechiclayo.com', '$2y$10$j9aDhQDWMsnVeyltgUx0LuBRmFsGJCex.VK4AF4GeVMKsGT432Hxi', '3', 'egalvez', 'BTkiEE81MNMpfPnUMEgZH0MU6pLbe6SX1lH4oHHMNqaS3SV6ZxIvvhLbc07m', '1', '2016-02-16 16:58:59', '2016-06-06 12:24:20');
INSERT INTO `users` VALUES ('4', '7', '6', 'lsanchezg@prestamosdelnortechiclayo.com', '$2y$10$uJgHuYj2mICwQkE/qWaLuOZgibm1sJzk0ChFP2Yd/di5WRS.e4Gsa', '3', 'lsanchezg', 'snxro9RK8mDjvvC23QXIHYVMfWlzMy44m3Q8IZyMH51q6syjpb4a5Is3bSWa', '1', '2016-02-16 17:04:39', '2016-02-17 10:47:23');
INSERT INTO `users` VALUES ('5', '6', '9', 'rfernandezm@prestamosdelnortechiclayo.com', '$2y$10$FvL0nlF/ieEfaPOZCyc1LuZvHEhCLrb8DVgVlz3aq5juCMKThf7be', '3', 'rfernandezm', null, '1', '2016-02-16 17:09:19', '2016-02-16 17:09:19');
INSERT INTO `users` VALUES ('6', '8', '7', 'jgonzales@prestamosdelnortechiclayo.com', '$2y$10$e34Rm3yQu8ASdZmkarkrked85zV/re6MpX3RQzhf/.ZE0bYmh7TDq', '3', 'jgonzales', null, '1', '2016-02-16 17:11:03', '2016-02-16 17:11:03');
INSERT INTO `users` VALUES ('7', '10', '4', 'cmercado@prestamosdelnortechiclayo.com', '$2y$10$FrADjsrJDU8lqYLR3ogYgeQuHIumVTUIdHnMolvQx2NlIhbvZdwsC', '3', 'cmercado', '8jrPaFtwgfwmeVm5Byc92GOlup3UfAcihf6H5snJTnQDNIbivTgttZISmMLK', '1', '2016-02-18 09:27:55', '2016-02-18 09:38:30');

-- ----------------------------
-- Table structure for vehiculo
-- ----------------------------
DROP TABLE IF EXISTS `vehiculo`;
CREATE TABLE `vehiculo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_vehiculo_id` int(10) unsigned NOT NULL,
  `modelo_id` int(10) unsigned NOT NULL,
  `placa` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `serie` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `estado` smallint(5) unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vehiculo_tipo_vehiculo_id_foreign` (`tipo_vehiculo_id`),
  KEY `vehiculo_modelo_id_foreign` (`modelo_id`),
  CONSTRAINT `vehiculo_modelo_id_foreign` FOREIGN KEY (`modelo_id`) REFERENCES `modelo` (`id`),
  CONSTRAINT `vehiculo_tipo_vehiculo_id_foreign` FOREIGN KEY (`tipo_vehiculo_id`) REFERENCES `tipo_vehiculo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of vehiculo
-- ----------------------------
INSERT INTO `vehiculo` VALUES ('1', '1', '3', 'M3X-335', '65370997098GJHKGF', 'NINGUNA', '1', '2016-04-14 12:57:30', '2016-04-14 12:57:30');
