========= actualizar migraciones =============================
	php artisan migrate

===============================================================

sudo service nginx start
sudo service php5-fpm restart
sudo service mysql restart

sudo apt-get install --reinstall php5-common php5-mysql


# crear migraciones

php artisan make:migration create_persona_table --create="persona"
php artisan make:migration create_tipo_control_table --create="tipo_control"
php artisan make:migration create_control_table --create="control"
php artisan make:migration create_rol_table --create="rol"
php artisan make:migration create_rol_control_table --create="rol_control"
php artisan make:migration create_users_table --create="users"
php artisan make:migration create_accesos_table --create="accesos"
php artisan make:migration create_transaccion_table --create="transaccion"
php artisan make:migration create_bitacora_table --create="bitacora"
php artisan make:migration create_tipo_relacion_table --create="tipo_relacion"
php artisan make:migration create_per_relacion_table --create="per_relacion"
php artisan make:migration create_password_resets_table --create="password_resets"
php artisan make:migration create_notificacion_table --create="notificacion"

php artisan make:migration create_per_natural_table --create="per_natural"
php artisan make:migration create_per_mail_table --create="per_mail"
php artisan make:migration create_tipo_documento_table --create="tipo_documento"
php artisan make:migration create_per_documento_table --create="per_documento"
php artisan make:migration create_rubro_table --create="rubro"
php artisan make:migration create_per_juridica_table --create="per_juridica"
php artisan make:migration create_tipo_telefono_table --create="tipo_telefono"
php artisan make:migration create_per_telefono_table --create="per_telefono"
php artisan make:migration create_tipo_direccion_table --create="tipo_direccion"
php artisan make:migration create_per_direccion_table --create="per_direccion"
php artisan make:migration create_per_imagen_table --create="per_imagen"
php artisan make:migration create_ubigeo_table --create="ubigeo"


php artisan make:migration create_usp_cursor_set_notificar_consolidado_by_alerta_function --create="create_usp_cursor_set_notificar_consolidado_by_alerta_function"

#
php artisan make:model Models/Persona
php artisan make:model Models/TipoControl
php artisan make:model Models/Control
php artisan make:model Models/Rol
php artisan make:model Models/RolControl
php artisan make:model Models/Acceso
php artisan make:model Models/Transaccion
php artisan make:model Models/Bitacora
php artisan make:model Models/TipoRelacion
php artisan make:model Models/PerRelacion
php artisan make:model Models/Notificacion

php artisan make:model Models/PerNatural
php artisan make:model Models/PerMail
php artisan make:model Models/TipoDocumento
php artisan make:model Models/PerDocumento
php artisan make:model Models/Rubro
php artisan make:model Models/PerJuridica
php artisan make:model Models/TipoTelefono
php artisan make:model Models/PerTelefono
php artisan make:model Models/TipoDireccion
php artisan make:model Models/PerDireccion
php artisan make:model Models/Ubigeo
php artisan make:model Models/PerImagen

# seeder
php artisan make:seeder AdminTableSeeder
php artisan make:seeder TipoControlTableSeeder
php artisan make:seeder RolTableSeeder
php artisan make:seeder PersonaTableSeeder
php artisan make:seeder UserTableSeeder
php artisan make:seeder UbigeoTableSeeder
php artisan make:seeder AdminRecepetorTableSeeder
php artisan make:seeder AdminEmisorTableSeeder

php artisan make:seeder TipoDocumentoTableSeeder
php artisan make:seeder TipoRegistroTableSeeder
php artisan make:seeder TipoAlertaTableSeeder
php artisan make:seeder TipoTelefonoTableSeeder
php artisan make:seeder TipoDireccionTableSeeder
php artisan make:seeder TipoRelacionTableSeeder
php artisan make:seeder PerJuridicaTableSeeder

php artisan make:seeder AlertaTableSeeder
php artisan make:seeder AlertaUbigeoTableSeeder

php artisan make:seeder ProviderTableSeeder
php artisan make:seeder OauthIdentitiesTableSeeder


#####
# generar controllers
php artisan make:controller PlataformaController
php artisan make:controller UsersController
php artisan make:controller CompanyController
php artisan make:controller AlertaController
php artisan make:controller Api/TipoAlertaController
php artisan make:controller Api/UbigeoController
php artisan make:controller Api/UbigeoController

php artisan make:controller Receptor/ReceptorController
php artisan make:controller Emisor/EmisorController
php artisan make:controller Admin/AdminController


# para registrar un usuario
============================
- tabla persona
- tabla usuario
- table per_relacion
- tabla per_telefono
- tabla per_mail
