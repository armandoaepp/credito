/*
Navicat MySQL Data Transfer

Source Server         : Homestead
Source Server Version : 50710
Source Host           : 192.168.10.10:3306
Source Database       : db_demo

Target Server Type    : MYSQL
Target Server Version : 50710
File Encoding         : 65001

Date: 2016-04-20 10:25:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for demo
-- ----------------------------
DROP TABLE IF EXISTS `demo`;
CREATE TABLE `demo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campo1` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `estado` int(11) unsigned zerofill NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `demo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of demo
-- ----------------------------
INSERT INTO `demo` VALUES ('1', 'demo 1', '00000000001', null, null, null);
INSERT INTO `demo` VALUES ('2', 'demo 2', '00000000002', null, null, null);
