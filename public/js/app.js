angular
	.module('appPlataforma', [
		'app.core',
		'app.directives',
		'app.layout' ,
		'app.filter',
		'app.informacion',
		'app.movimientos',
	]);


angular
	.module('app.core', [
		'ui.router',
		'ui.bootstrap',
		'paths.constant',
		'ngSanitize',
		'app.plugin'

	]);

//  informacion global del layout
angular
	.module('app.layout', [
		'users.controller',
		'menu.controller',
		'menu.directive',
	]);

// plugin para app
angular
	.module('app.plugin', [
		'angular.vertilize', /* poner del mismo alto elements html*/
		'ngTagsInput',
		'xeditable',
		'ng-sortable', // reordenar item
		'ngTable',
		'ui.select',
		// 'ivh.treeview', // tre list check
		'AngularBootstrapTree',
		'angularFileUpload'
		]);

//  module de informacion
angular
	.module('app.informacion', [
		'app.core',
		'app.directives',
		'roles.info.controller',
		'persona.natural.info.controller',
		'usuarios.info.controller',
		'empleados.info.controller',
		'clientes.info.controller',
		'areas.info.controller',
		'cargos.info.controller',
		'tipoPrestamos.info.controller',
		'tipoMonedas.info.controller',
		'tipoPeriodos.info.controller',
		'tipoGarantias.info.controller',
		'tipoPagos.info.controller',
		'licencias.info.controller',
		'tipoCambios.info.controller',
		'acuerdoPagos.info.controller',
		'personas.juridicas.info.controller',
		'tipoVehiculos.info.controller',
		'claseVehiculos.info.controller',
		'marcas.info.controller',
		'modelos.info.controller',
		'vehiculos.info.controller',

		]);


//  module de informacion
angular
	.module('app.movimientos', [
		'app.core',
		'app.directives',
		'prestamos.mov.controller',
		'cred.menor.mov.controller',

		]);

//  modules directivas
angular
	.module('app.directives', [
	    'validate.directive',
	    'modal.directive',
	]);

// controllers gobals
	angular.module('menu.controller', ['accesos.service','menu.factory']);
	angular.module('users.controller', ['users.service']);

	// controlles module informacion
		angular.module('persona.natural.info.controller', ['modal.service', 'personaNatural.service','perTelefono.service','perMail.service','perDocumento.service']);

		angular.module('roles.info.controller', ['rol.service', 'modal.service']);
		angular.module('usuarios.info.controller', ['AngularBootstrapTree','modal.service','accesos.service','users.service','personaNatural.service','rol.service','control.service']);
		angular.module('areas.info.controller', ['modal.service','area.service']);
		angular.module('cargos.info.controller', ['modal.service','area.service','cargo.service']);
		angular.module('empleados.info.controller', ['modal.service','empleado.service','area.service']);
		angular.module('clientes.info.controller', ['modal.service','cliente.service','empleado.service','persona.service']);
		// registros
		angular.module('tipoPrestamos.info.controller', ['modal.service','tipoPrestamo.service']);
		angular.module('tipoMonedas.info.controller', ['modal.service','tipoMoneda.service']);
		angular.module('tipoPeriodos.info.controller', ['modal.service','tipoPeriodo.service']);
		angular.module('tipoGarantias.info.controller', ['modal.service','tipoGarantia.service']);
		angular.module('tipoPagos.info.controller', ['modal.service','tipoPago.service','tipoPrestamo.service']);
		angular.module('licencias.info.controller', ['modal.service','licencia.service']);
		angular.module('tipoCambios.info.controller', ['modal.service','tipoCambio.service']);
		angular.module('acuerdoPagos.info.controller', ['modal.service','acuerdoPago.service']);

		angular.module('personas.juridicas.info.controller', ['modal.service','perJuridica.service','ubigeo.service','perTelefono.service','perMail.service','perDocumento.service','perWeb.service']);

		// VEHICULOS
		angular.module('claseVehiculos.info.controller', ['modal.service','claseVehiculo.service']);
		angular.module('tipoVehiculos.info.controller', ['modal.service','tipoVehiculo.service','claseVehiculo.service']);
		angular.module('marcas.info.controller', ['modal.service','marca.service']);
		angular.module('modelos.info.controller', ['modal.service','modelo.service','marca.service']);
		angular.module('vehiculos.info.controller', ['modal.service','modelo.service','vehiculo.service']);


	// controller dule movimientos
		angular.module('prestamos.mov.controller', [
													'modal.service',
													'tipoPrestamo.service',
													'tipoMoneda.service',
													'tipoPeriodo.service',
													'tipoGarantia.service',
													'tipoPago.service',
													'empleado.service',
													'vehiculo.service',
													'aval.service',
													'prestamo.service',
													'cuota.service',
													'acuerdoPago.service',
													'tipoEstado.service',
													'prestamoEstado.service',

													]);
		angular.module('cred.menor.mov.controller', [
													'modal.service',
													'tipoPrestamo.service',
													'tipoMoneda.service',
													'tipoPeriodo.service',
													'tipoGarantia.service',
													'tipoPago.service',
													'empleado.service',
													'vehiculo.service',
													'aval.service',
													'prestamo.service',
													'cuota.service',
													'acuerdoPago.service',
													'tipoEstado.service',
													'prestamoEstado.service',
													'creditoMenor.service',
													'prestamoGarantia.service',
													'prestamoReferencia.service'

													]);


// directives
	angular.module('menu.directive', []);
	angular.module('validate.directive', []);
	angular.module('modal.directive', []);

// service
	angular.module('accesos.service', []) ;
	angular.module('users.service', []) ;
	angular.module('rol.service', []) ;
	angular.module('persona.service', []) ;
	angular.module('personaNatural.service', []) ;
	angular.module('perTelefono.service', []) ;
	angular.module('perMail.service', []) ;
	angular.module('perDocumento.service', []) ;
	angular.module('control.service', []) ;
	angular.module('empleado.service', []) ;
	angular.module('cliente.service', []) ;
	angular.module('perJuridica.service', []) ;
	angular.module('ubigeo.service', []) ;
	angular.module('perWeb.service', []) ;
	angular.module('aval.service', []) ;

	// registros
	angular.module('area.service', []) ;
	angular.module('cargo.service', []) ;

	angular.module('tipoPrestamo.service', []) ;
	angular.module('tipoMoneda.service', []) ;
	angular.module('tipoPeriodo.service', []) ;
	angular.module('tipoGarantia.service', []) ;
	angular.module('tipoPago.service', []) ;
	angular.module('licencia.service', []) ;
	angular.module('tipoCambio.service', []) ;
	angular.module('acuerdoPago.service', []) ;

	// vehiculos
	angular.module('claseVehiculo.service', []) ;
	angular.module('tipoVehiculo.service', []) ;
	angular.module('marca.service', []) ;
	angular.module('modelo.service', []) ;
	angular.module('vehiculo.service', []) ;
	// movimientos
		// operaciones
		angular.module('prestamo.service', []) ;
		angular.module('cuota.service', []) ;
		angular.module('tipoEstado.service', []) ;
		angular.module('prestamoEstado.service', []) ;
		angular.module('creditoMenor.service', []) ;
		angular.module('prestamoGarantia.service', []) ;
		angular.module('prestamoReferencia.service', []) ;


// services pulgin
angular.module('modal.service', []) ;

// factory
angular.module('menu.factory', []) ;



// filters
angular.module('app.filter', [
		'controles.filter',
	]);

angular.module('controles.filter', []) ;


angular.module('paths.constant', []) ;


(function(){
    angular
        .module('appPlataforma')
        .config(appConfig) ;

        appConfig.$inject = ['$interpolateProvider','$stateProvider', '$urlRouterProvider','$locationProvider'] ;

        function appConfig($interpolateProvider,$stateProvider,$urlRouterProvider,$locationProvider)
        {
            $interpolateProvider.startSymbol('{%');
            $interpolateProvider.endSymbol('%}');

            $urlRouterProvider.
                    otherwise('/');
        }
})() ;
(function(){
    angular
        .module('appPlataforma')
        .run(appRun) ;

        appRun.$inject =  ['$rootScope', '$state', '$stateParams','$location','editableOptions','ngTableDefaults'] ;

        function  appRun($rootScope,   $state,   $stateParams, $location,editableOptions,ngTableDefaults) {

            // It's very handy to add references to $state and $stateParams to the $rootScope
            // so that you can access them from any scope within your applications.For example,
            // <li ng-class="{ active: $state.includes('contacts.list') }"> will set the <li>
            // to active whenever 'contacts.list' or one of its decendents is active.
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;

            editableOptions.theme = 'bs3';

            ngTableDefaults.params.count = 10;
            ngTableDefaults.settings.counts = [];
        }

})();

(function(){


        function UserInfoCtrl(usersService)
        {
            var vm = this ;
             vm.menus = [] ;
             vm.getInfo = getInfo ;

             vm.data = [] ;

             init();
             function init() {
                vm.getInfo() ;
             }

            function getInfo(){

                usersService.getInfo().then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data = response.data;
                            return vm.data ;
                        }
                    }
                );
            }


        }

        angular.module('users.controller').controller('UserInfoCtrl',UserInfoCtrl);
        UserInfoCtrl.$inject = ['usersService'] ;


})() ;

(function(){

        angular.module('menu.controller').controller('accesosMenusCrtl',accesosMenusCrtl);
        accesosMenusCrtl.$inject = ['accesosService','menusFactory'] ;

            function accesosMenusCrtl(accesosService, menusFactory)
            {
                var vm = this ;
                vm.menus = [] ;
                vm.menus = vm.menus ;

                vm.getAccesosService =  getAccesosService ;
                vm.getAccesos        =  getAccesos ;

                vm.getAccesos() ;

                function getAccesos(){

                    menusFactory.getMenus().then(
                            function(response){
                                vm.menus = response;
                                 return vm.menus ;
                            }
                        );

                } ;



                // asegurar de que carge el menu si la factory esta vacia
                function  getAccesosService(){

                    accesosService.getAccesos().then(
                        function(response){
                            if (!response.error){
                                if (response.data.length > 0)
                                {
                                    // vm.menus = response.data[0]['children'];
                                    vm.menus = response.data;
                                    menusFactory.setMenus(vm.menus);
                                    return vm.menus ;
                                };
                            }
                        }
                    );
                }
            }


        angular.module('menu.controller').controller('BotoneraAccesoCtrl',BotoneraAccesoCtrl);
        BotoneraAccesoCtrl.$inject = ['$rootScope','botoneraFactory'] ;

            function BotoneraAccesoCtrl($rootScope,botoneraFactory)
            {
                var vm = this ;
                vm.botones = botoneraFactory.getBotonera();

                  // el $broadcast se encuentra en drective(itemModulo)
                $rootScope.$on("recargarBotoneraAccesoCtrl", function(event, values) {
                        vm.botones = botoneraFactory.getBotonera();
                  });


            }



})() ;

(function(){
    'use strict';

    angular.module('cred.menor.mov.controller').controller('CredMenorMovCtrl',CredMenorMovCtrl);
    CredMenorMovCtrl.$inject = ['$rootScope','botoneraFactory','$filter','$state','prestamoService', '$uibModal', 'PATH', 'modalService','NgTableParams','tipoEstadoService'] ;

        function CredMenorMovCtrl($rootScope,botoneraFactory,$filter,$state,prestamoService, $uibModal, PATH, modalService,NgTableParams,tipoEstadoService)
        {
            var vm = this ;

            // function
                vm.getPrestamos     = getPrestamos ;
                vm.onClick          = onClick ;
                vm.newPrestamo      = newPrestamo ;
                vm.detallePrestamo  = detallePrestamo ;
                vm.editPrestamo     = editPrestamo ;
                vm.aprobarPrestamo  = aprobarPrestamo ;
                vm.cancelarPrestamo = cancelarPrestamo ;

            // variables
                vm.botones      = [] ;
                vm.fillSelected = [] ;
                vm.data_list    = [] ;
                vm.fillSelected = [] ;
                vm.data_tipo_estados = [] ;

                vm.btn_in_table = false ;
                vm.btn_edit     = false ;
                vm.btn_delete   = false ;

            init();
            function init() {
                tablePlugin() ;
                vm.getPrestamos(null) ;
                getTipoEstados() ;
            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getPrestamos(1);
                }
                else if (name === 'new')
                {
                      vm.newPrestamo('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editPrestamo() ;
                }
                else if (name === 'delete')
                {
                    vm.deletePrestamo() ;
                }
                else if (name === 'detail')
                {
                    vm.detallePrestamo() ;
                }
                else if (name === 'aprobar')
                {
                    vm.aprobarPrestamo() ;
                }
                else if (name === 'cancelar')
                {
                    vm.cancelarPrestamo() ;
                }
                else{
                    return ;
                };

            } ;



            function getPrestamos(tipo_estado_id)
            {
                var params = {
                        'tipo_estado_id' : tipo_estado_id ,
                        'tipo_prestamo_id' : 4 ,
                    } ;

                prestamoService.getPrestamosByTipoEstadoId(params).then(
                    function(response){
                        if (!response.error)
                        {
                            // botonesInTable() ;
                            var data = response.data;
                            vm.data_list = data ;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );

                vm.fillSelected = [] ;
            };

            function getTipoEstados()
            {

                tipoEstadoService.getTipoEstados().then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data_tipo_estados = response.data;

                             vm.data_search_tipo_estados = [{
                                "id": 0,
                                "descripcion": "Todos",
                                "estado": null,
                             }].concat(vm.data_tipo_estados)  ;

                            return vm.data_tipo_estados ;
                        }
                    }
                );
            };


            // ===== ng-table ==============================================================================================


             /*   vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;*/
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                };

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New
                function newPrestamo (size)
                {
                     $state.go($state.current.name+'.nuevo', { }) ;
                };

                 // edit
                function editPrestamo (size)
                {
                    var codigo = vm.fillSelected.id ;

                    if (codigo === undefined) {
                        return modalAlert()
                    }
                    else
                    {
                        $state.go($state.current.name+'.edit', { codigo: codigo });
                    }

                };

            //  detalle de prestamo
                function detallePrestamo (size)
                {
                    var codigo = vm.fillSelected.id ;

                    if (codigo === undefined) {
                        return modalAlert()
                    }
                    else
                    {
                        $state.go($state.current.name+'.detalle', { codigo: codigo });
                    }
                };

            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        prestamoService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getPrestamos() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Prestamo',
                        bodyText: '¿Esta Seguro de Eliminar Prestamo: '+ row.data+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }




            // aprobar
                function aprobarPrestamo (size)
                {
                    var codigo = vm.fillSelected.id ;
                    var estado = vm.fillSelected.estado ;

                        if (codigo === undefined) {
                            return modalAlert()
                        } ;
                        if (estado > 1){
                            return modalInfo()
                        } ;

                        var path = PATH.MOVIMIENTOS ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/operaciones/prestamos/modal.aprobar.tpl.html',
                            controller: 'ModalAprobarPrestamoCrtl',
                            controllerAs: 'vm',
                            size: size,
                            backdrop : 'static',
                            resolve: {
                                data_prestamo: function () { return vm.fillSelected; },
                            }

                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                init(data) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

            // cancelar
                function cancelarPrestamo (size)
                {
                    var codigo = vm.fillSelected.id ;
                    var estado = vm.fillSelected.estado ;

                        if (codigo === undefined) {
                            return modalAlert()
                        } ;

                        if (estado > 1){
                            return modalInfo()
                        } ;


                        var path = PATH.MOVIMIENTOS ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/operaciones/prestamos/modal.cancelar.tpl.html',
                            controller: 'ModalCancelarPrestamoCrtl',
                            controllerAs: 'vm',
                            size: size,
                            backdrop : 'static',
                            resolve: {
                                data_prestamo: function () { return vm.fillSelected; },
                            }

                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                init(data) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

            //modal info
                function modalInfo()
                {
                    var modalOptions = {
                        headerText: 'Prestamo',
                        bodyText: 'Operacion no Permitida, ¡¡¡ VER DETALLE o ESTADO PRESTAMO !!! '
                    };

                    modalService.showModalAlert({}, modalOptions).then(function (result){
                    });
                };

            // ======== recagar
                $rootScope.$on("reloadListCreditoMenor", function(event, values) {
                       vm.getPrestamos();
                });




        }
})() ;


(function(){
    'use stric' ;

    angular.module('cred.menor.mov.controller').controller('DetalleCredMenorMovCrtl', DetalleCredMenorMovCrtl) ;
    DetalleCredMenorMovCrtl.$inject = [
        '$stateParams',
        '$rootScope',
        '$state',
        '$filter',
        'tipoPrestamoService',
        'tipoMonedaService',
        'tipoPeriodoService',
        'tipoGarantiaService',
        'tipoPagoService',
        'empleadoService',
        'clienteService',
        'vehiculoService',
        'avalService',
        'prestamoService',
        'acuerdoPagoService',
        'cuotaService',
        'PATH',
        'FileUploader',
        'NgTableParams',
        'creditoMenorService',
        'prestamoGarantiaService',
        'prestamoReferenciaService',
        'prestamoEstadoService',
    ];



    function DetalleCredMenorMovCrtl(
        $stateParams,
        $rootScope,
        $state,
        $filter,
        tipoPrestamoService,
        tipoMonedaService,
        tipoPeriodoService,
        tipoGarantiaService,
        tipoPagoService,
        empleadoService,
        clienteService,
        vehiculoService,
        avalService,
        prestamoService,
        acuerdoPagoService,
        cuotaService,
        PATH,
        FileUploader,
        NgTableParams,
        creditoMenorService,
        prestamoGarantiaService,
        prestamoReferenciaService,
        prestamoEstadoService
    ) {
        var vm = this;

        vm.prestamo_id = $stateParams.codigo;
        vm.data_list = [];

        vm.disabledInput = true;


        vm.msj = "";

        vm.formData = {
            tipo_prestamo: undefined,
            tipo_moneda: undefined,
            tipo_periodo: undefined,
            acuerdo_pago: undefined,
            tipo_pago: undefined,
            tipo_garantia: undefined,
            asesor_negocio: undefined,
            cliente: undefined,
            aval: undefined,
            interes: '',
            num_cuotas: '',
            mora: '',
            complacencia: '',
            pagos_parciales: 'NO',
            propietario: '',
            observacion: '',
            fecha_credito: null,
            fecha_desembolso: null,
            referencia_web: '',
            monto: '',
        };

        vm.tipo_prestamos   = [];
        vm.tipo_monedas     = [];
        vm.tipo_periodos    = [];
        vm.acuerdo_pagos    = [];
        vm.tipo_pagos       = [];
        vm.tipo_garantias   = [];
        vm.asesores_negocio = [];
        vm.clientes         = [];
        vm.data_vehiculos   = [];
        vm.data_avales      = [];



        //  DatePicker ui-b
        vm.popup = {
            opened: false,
        };
        vm.dateOptions = {
            startingDay: 1, // inicie en lunes
            showWeeks: 'false',
            minDate: null,
        };

        vm.openCalendar = function() {
            vm.popup.opened = true;
        };

        vm.popup2 = {
            opened: false,
        };
        vm.openCalendar2 = function() {
            vm.popup2.opened = true;
        };

        init();

        function init() {
            getPrestamoById();
            getTipoPrestamos();
            setTimeout(getClientesInfoBasica(), 1000);
            setTimeout(getTipoMonedas(), 1500);
            setTimeout(getTipoPeriodos(), 2000);
            setTimeout(getTipoGarantias(), 2500);
            // setTimeout(getAvalesInfoBasica(), 3000);

            setTimeout(getPretamoReferenciasByPrestamoId(), 3000);
            setTimeout(getPretamoGarantiasByPrestamoId(), 3500);

            setTimeout(getCuotasByPrestamoId(), 5000);
            setTimeout(getPrestamoEstadosInfo(), 6000);

        };

        //  carga de datos

        function getPrestamoById() {
            var params = {
                'prestamo_id': vm.prestamo_id,
            };
            prestamoService.getPrestamoById(params).then(
                function(response) {
                    if (!response.error) {
                        vm.data_list = response.data;

                        console.log(vm.data_list);


                        var estado = parseInt(vm.data_list.estado);
                        vm.disabledInputsEdit = true;
                        if (estado === 1) {
                            vm.disabledInputsEdit = false;
                        };

                        vm.formData.tipo_prestamo_id    = vm.data_list.tipo_prestamo_id;
                        vm.formData.tipo_pago_id        = vm.data_list.tipo_pago_id;
                        vm.formData.cliente_id          = vm.data_list.cliente_id;
                        vm.formData.tipo_moneda_id      = vm.data_list.tipo_moneda_id;
                        vm.formData.interes             = vm.data_list.tasa_interes;
                        vm.formData.tipo_periodo_id     = vm.data_list.tipo_periodo_id;
                        vm.formData.acuerdo_pago_id     = vm.data_list.acuerdo_pago_id;
                        vm.formData.num_cuotas          = vm.data_list.num_cuotas;
                        vm.formData.pagos_parciales     = vm.data_list.pagos_parciales;
                        vm.formData.propietario         = vm.data_list.propietario;
                        vm.formData.aval_id             = vm.data_list.aval_id;
                        vm.formData.observacion         = vm.data_list.observacion;
                        vm.formData.tipo_garantia_id    = vm.data_list.tipo_garantia_id;
                        vm.formData.fecha_credito       = (!vm.data_list.fecha_credito) ? null : new Date(vm.data_list.fecha_credito + ' 00:00:00');
                        vm.formData.fecha_desembolso    = (!vm.data_list.fecha_desembolso) ? null : new Date(vm.data_list.fecha_desembolso + ' 00:00:00');
                        vm.formData.fecha_prorrateo     = (!vm.data_list.fecha_prorrateo) ? null : new Date(vm.data_list.fecha_prorrateo + ' 00:00:00');
                        vm.formData.fecha_prorrateo_esp = (!vm.data_list.fecha_prorrateo_esp) ? null : new Date(vm.data_list.fecha_prorrateo_esp + ' 00:00:00');
                        vm.formData.monto               = vm.data_list.valor;
                        vm.formData.complacencia        = vm.data_list.complacencia;



                        getTipoPagosByTipoPrestamoId();
                        // vm.onSelectTipoPrestamo(null);
                        vm.getAcuerdoPagosByTipoPeriodoId(null, null);
                        return vm.tipo_prestamos;
                    }
                }
            );
        };
        //  carga de datos

        function getTipoPrestamos() {
            tipoPrestamoService.getTipoPrestamos().then(
                function(response) {
                    if (!response.error) {
                        vm.tipo_prestamos = response.data;
                        vm.formData.tipo_prestamo = getFill(vm.tipo_prestamos, 4);
                        getTipoPagosByTipoPrestamoId();
                        return vm.tipo_prestamos;
                    }
                }
            );
        };

        function getFill(data, idSelected) {
            var out = []
            for (var i in data) {
                if (parseInt(data[i].id) === parseInt(idSelected)) {
                    out = data[i];
                    return out;
                };
            };
            return out;
        };

        function getTipoMonedas() {
            tipoMonedaService.getTipoMonedas().then(
                function(response) {
                    if (!response.error) {
                        vm.tipo_monedas = response.data;
                        return vm.tipo_monedas;
                    }
                }
            );
        };

        function getTipoPeriodos() {
            tipoPeriodoService.getTipoPeriodos().then(
                function(response) {
                    if (!response.error) {
                        vm.tipo_periodos = response.data;
                        return vm.tipo_periodos;
                    }
                }
            );
        };

        // tipo-couta
        function getTipoPagosByTipoPrestamoId()
        {
            var data = {
                // 'tipo_prestamo_id' : vm.formData.tipo_prestamo.id,
                'tipo_prestamo_id': 4,
            };
            tipoPagoService.getTipoPagosByTipoPrestamoId(data).then(
                function(response) {
                    if (!response.error)
                    {
                        vm.tipo_pagos = response.data;
                        return vm.tipo_pagos;
                    }
                }
            );
        };

        vm.getAcuerdoPagosByTipoPeriodoId = function($item, $model) {
            var data = {
                'tipo_periodo_id': vm.formData.tipo_periodo_id,
            };

            acuerdoPagoService.getAcuerdoPagosByTipoPeriodoId(data).then(
                function(response) {
                    if (!response.error) {
                        vm.acuerdo_pagos = response.data;
                        return vm.acuerdo_pagos;
                    }
                }
            );
        };

        function getTipoGarantias() {
            tipoGarantiaService.getTipoGarantias().then(
                function(response) {
                    if (!response.error) {
                        vm.tipo_garantias = response.data;
                        return vm.tipo_garantias;
                    }
                }
            );
        };

        function getClientesInfoBasica() {
            var per_tipo = 0;
            var data = {
                'per_tipo': per_tipo,
            };

            clienteService.getClientesInfoBasica(data).then(
                function(response) {
                    console.info('cleintes', response);
                    if (!response.error) {
                        vm.clientes = response.data;
                    } else {
                        vm.msj = response.error;
                    }
                }
            );
        };

        function getPretamoReferenciasByPrestamoId() {
            var params = {
                'prestamo_id': vm.prestamo_id,
            };
            prestamoReferenciaService.getPretamoReferenciasByPrestamoId(params).then(
                function(response) {
                    if (!response.error) {
                        var data = response.data;
                        vm.data_referencias = filterItems(data, { 'tipo': 2 }) ;
                        vm.data_ref_web = filterItems(data, { 'tipo': 1 }) ;

                        return vm.data_referencias;
                    }
                }
            );
        };

        function getPretamoReferenciasByPrestamoIdTipo(tipo) {
            var params = {
                'prestamo_id': vm.prestamo_id,
                'tipo': tipo,
            };
            prestamoReferenciaService.getPretamoReferenciasByPrestamoIdTipo(params).then(
                function(response) {
                    if (!response.error) {
                        var data = response.data;
                        vm.data_referencias = filterItems(data, { 'tipo': 2 }) ;
                        return vm.data_referencias;
                    }
                }
            );
        };

        function getPretamoGarantiasByPrestamoId() {
            var params = {
                'prestamo_id': vm.prestamo_id,
            };
            prestamoGarantiaService.getPretamoGarantiasByPrestamoId(params).then(
                function(response) {
                    if (!response.error) {
                        vm.data_garantias = response.data;
                        console.info('garantias', vm.data_garantias);
                        return vm.data_garantias;
                    }
                }
            );
        };



        vm.reloadList = function() {
            var current = $state.current.name;
            var res = current.split(".", 2);
            var current_parent = res[0] + "." + res[1];

            var values = '';
            $state.go(current_parent, values);
            $rootScope.$broadcast("reloadList", values);
        };


        function filterItems(items, props) {
            var out = [];

            //  texto es el mismo que se buscara para todos de elementos (por ejemplo {name: "12", age: "12"})
            var keys = Object.keys(props);
            var firstElement = keys[0];

            var text = String(props[firstElement]).toLowerCase();
            // text diferente de nulo,  recoremos el array
            if (angular.isArray(items)) {
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    var itemMatches = false;
                    var prop = keys[0];
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                }

            } else {
                // Let the output be the input untouched
                out = items;
            }
            return out;
        };

         vm.data_list_coutas = [] ;
            function getCuotasByPrestamoId()
            {
                var data = {
                    'prestamo_id' : vm.prestamo_id ,
                } ;

                cuotaService.getCuotasByPrestamoId(data).then(
                    function(response){
                        console.log(response);
                        if (!response.error)
                        {
                            vm.data_list_coutas = response.data ;
                            return vm.data_list_coutas ;

                        }
                    }
                );
            };
            vm.data_estados_info = [] ;
            function getPrestamoEstadosInfo()
            {
                var params = {
                    'prestamo_id' : vm.prestamo_id ,
                } ;
                console.log(params);
                prestamoEstadoService.getPrestamoEstadosInfo(params).then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data_estados_info = response.data;
                            console.log(vm.data_estados_info);
                            return vm.data_estados_info ;
                        }
                    }
                );
            };



    };
})();

(function() {
    'use stric';

    angular.module('cred.menor.mov.controller').controller('EditCredMenorMovCrtl', EditCredMenorMovCrtl);
    EditCredMenorMovCrtl.$inject = [
        '$stateParams',
        '$rootScope',
        '$state',
        '$filter',
        'tipoPrestamoService',
        'tipoMonedaService',
        'tipoPeriodoService',
        'tipoGarantiaService',
        'tipoPagoService',
        'empleadoService',
        'clienteService',
        'vehiculoService',
        'avalService',
        'prestamoService',
        'acuerdoPagoService',
        'cuotaService',
        'PATH',
        'FileUploader',
        'NgTableParams',
        'creditoMenorService',
        'prestamoGarantiaService',
        'prestamoReferenciaService',
    ];



    function EditCredMenorMovCrtl(
        $stateParams,
        $rootScope,
        $state,
        $filter,
        tipoPrestamoService,
        tipoMonedaService,
        tipoPeriodoService,
        tipoGarantiaService,
        tipoPagoService,
        empleadoService,
        clienteService,
        vehiculoService,
        avalService,
        prestamoService,
        acuerdoPagoService,
        cuotaService,
        PATH,
        FileUploader,
        NgTableParams,
        creditoMenorService,
        prestamoGarantiaService,
        prestamoReferenciaService
    ) {
        var vm = this;

        vm.prestamo_id = $stateParams.codigo;
        vm.data_list = [];

        vm.disabledInput = true;


        vm.msj = "";

        vm.formData = {
            tipo_prestamo: undefined,
            tipo_moneda: undefined,
            tipo_periodo: undefined,
            acuerdo_pago: undefined,
            tipo_pago: undefined,
            tipo_garantia: undefined,
            asesor_negocio: undefined,
            cliente: undefined,
            aval: undefined,
            interes: '',
            num_cuotas: '',
            mora: '',
            complacencia: '',
            pagos_parciales: 'NO',
            propietario: '',
            observacion: '',
            fecha_credito: null,
            fecha_desembolso: null,
            referencia_web: '',
            monto: '',
        };

        vm.tipo_prestamos   = [];
        vm.tipo_monedas     = [];
        vm.tipo_periodos    = [];
        vm.acuerdo_pagos    = [];
        vm.tipo_pagos       = [];
        vm.tipo_garantias   = [];
        vm.asesores_negocio = [];
        vm.clientes         = [];
        vm.data_vehiculos   = [];
        vm.data_avales      = [];



        //  DatePicker ui-b
        vm.popup = {
            opened: false,
        };
        vm.dateOptions = {
            startingDay: 1, // inicie en lunes
            showWeeks: 'false',
            minDate: null,
        };

        vm.openCalendar = function() {
            vm.popup.opened = true;
        };

        vm.popup2 = {
            opened: false,
        };
        vm.openCalendar2 = function() {
            vm.popup2.opened = true;
        };

        /*   vm.popup3 = {
               opened : false,
           } ;
           vm.openCalendar3 = function() {
               vm.popup3.opened = true;
           };

           vm.popup4 = {
               opened : false,
           } ;
           vm.openCalendar4 = function() {
               vm.popup4.opened = true;
           };*/

        init();

        function init() {
            getPrestamoById();
            getTipoPrestamos();
            setTimeout(getClientesInfoBasica(), 1000);
            setTimeout(getTipoMonedas(), 1500);
            setTimeout(getTipoPeriodos(), 2000);
            setTimeout(getTipoGarantias(), 2500);
            // setTimeout(getAvalesInfoBasica(), 3000);

            setTimeout(getPretamoReferenciasByPrestamoId(), 3000);
            setTimeout(getPretamoGarantiasByPrestamoId(), 3500);

            // getCuotasByPrestamoId();

        };

        //  carga de datos

        function getPrestamoById() {
            var params = {
                'prestamo_id': vm.prestamo_id,
            };
            prestamoService.getPrestamoById(params).then(
                function(response) {
                    if (!response.error) {
                        vm.data_list = response.data;

                        console.log(vm.data_list);


                        var estado = parseInt(vm.data_list.estado);
                        vm.disabledInputsEdit = true;
                        if (estado === 1) {
                            vm.disabledInputsEdit = false;
                        };

                        vm.formData.tipo_prestamo_id    = vm.data_list.tipo_prestamo_id;
                        vm.formData.tipo_pago_id        = vm.data_list.tipo_pago_id;
                        vm.formData.cliente_id          = vm.data_list.cliente_id;
                        vm.formData.tipo_moneda_id      = vm.data_list.tipo_moneda_id;
                        vm.formData.interes             = vm.data_list.tasa_interes;
                        vm.formData.tipo_periodo_id     = vm.data_list.tipo_periodo_id;
                        vm.formData.acuerdo_pago_id     = vm.data_list.acuerdo_pago_id;
                        vm.formData.num_cuotas          = vm.data_list.num_cuotas;
                        vm.formData.pagos_parciales     = vm.data_list.pagos_parciales;
                        vm.formData.propietario         = vm.data_list.propietario;
                        vm.formData.aval_id             = vm.data_list.aval_id;
                        vm.formData.observacion         = vm.data_list.observacion;
                        vm.formData.tipo_garantia_id    = vm.data_list.tipo_garantia_id;
                        vm.formData.fecha_credito       = (!vm.data_list.fecha_credito) ? null : new Date(vm.data_list.fecha_credito + ' 00:00:00');
                        vm.formData.fecha_desembolso    = (!vm.data_list.fecha_desembolso) ? null : new Date(vm.data_list.fecha_desembolso + ' 00:00:00');
                        vm.formData.fecha_prorrateo     = (!vm.data_list.fecha_prorrateo) ? null : new Date(vm.data_list.fecha_prorrateo + ' 00:00:00');
                        vm.formData.fecha_prorrateo_esp = (!vm.data_list.fecha_prorrateo_esp) ? null : new Date(vm.data_list.fecha_prorrateo_esp + ' 00:00:00');
                        vm.formData.monto               = vm.data_list.valor;
                        vm.formData.complacencia        = vm.data_list.complacencia;

                        // vm.formData.referencia_web       = vm.data_list.complacencia;
                        /*    vm.formData.inicial_porcentaje = vm.data_list.inicial_porcentaje ;
                            vm.formData.inicial_monto      = vm.data_list.inicial_monto ;
                            vm.formData.seguro_tr          = vm.data_list.seguro_tr ;
                            vm.formData.gps                = vm.data_list.gps ;
                            vm.formData.soat               = vm.data_list.soat ;
                            vm.formData.gas                = vm.data_list.gas ;
                            vm.formData.otros              = vm.data_list.otros ;
                            vm.formData.vehiculo_id        = vm.data_list.vehiculo_id ;*/



                        getTipoPagosByTipoPrestamoId();
                        // vm.onSelectTipoPrestamo(null);
                        vm.getAcuerdoPagosByTipoPeriodoId(null, null);
                        return vm.tipo_prestamos;
                    }
                }
            );
        };
        //  carga de datos

        function getTipoPrestamos() {
            tipoPrestamoService.getTipoPrestamos().then(
                function(response) {
                    if (!response.error) {
                        vm.tipo_prestamos = response.data;
                        vm.formData.tipo_prestamo = getFill(vm.tipo_prestamos, 4);
                        getTipoPagosByTipoPrestamoId();
                        return vm.tipo_prestamos;
                    }
                }
            );
        };

        function getFill(data, idSelected) {
            var out = []
            for (var i in data) {
                if (parseInt(data[i].id) === parseInt(idSelected)) {
                    out = data[i];
                    return out;
                };
            };
            return out;
        };

        function getTipoMonedas() {
            tipoMonedaService.getTipoMonedas().then(
                function(response) {
                    if (!response.error) {
                        vm.tipo_monedas = response.data;
                        return vm.tipo_monedas;
                    }
                }
            );
        };

        function getTipoPeriodos() {
            tipoPeriodoService.getTipoPeriodos().then(
                function(response) {
                    if (!response.error) {
                        vm.tipo_periodos = response.data;
                        return vm.tipo_periodos;
                    }
                }
            );
        };

        // tipo-couta
        function getTipoPagosByTipoPrestamoId()
        {
            var data = {
                // 'tipo_prestamo_id' : vm.formData.tipo_prestamo.id,
                'tipo_prestamo_id': 4,
            };
            tipoPagoService.getTipoPagosByTipoPrestamoId(data).then(
                function(response) {
                    if (!response.error)
                    {
                        vm.tipo_pagos = response.data;
                        return vm.tipo_pagos;
                    }
                }
            );
        };

        vm.getAcuerdoPagosByTipoPeriodoId = function($item, $model) {
            var data = {
                'tipo_periodo_id': vm.formData.tipo_periodo_id,
            };

            acuerdoPagoService.getAcuerdoPagosByTipoPeriodoId(data).then(
                function(response) {
                    if (!response.error) {
                        vm.acuerdo_pagos = response.data;
                        return vm.acuerdo_pagos;
                    }
                }
            );
        };

        function getTipoGarantias() {
            tipoGarantiaService.getTipoGarantias().then(
                function(response) {
                    if (!response.error) {
                        vm.tipo_garantias = response.data;
                        return vm.tipo_garantias;
                    }
                }
            );
        };

        function getClientesInfoBasica() {
            var per_tipo = 0;
            var data = {
                'per_tipo': per_tipo,
            };

            clienteService.getClientesInfoBasica(data).then(
                function(response) {
                    console.info('cleintes', response);
                    if (!response.error) {
                        vm.clientes = response.data;
                    } else {
                        vm.msj = response.error;
                    }
                }
            );
        };

        function getPretamoReferenciasByPrestamoId() {
            var params = {
                'prestamo_id': vm.prestamo_id,
            };
            prestamoReferenciaService.getPretamoReferenciasByPrestamoId(params).then(
                function(response) {
                    if (!response.error) {
                        var data = response.data;
                        vm.data_referencias = filterItems(data, { 'tipo': 2 }) ;
                        vm.data_ref_web = filterItems(data, { 'tipo': 1 }) ;

                        return vm.data_referencias;
                    }
                }
            );
        };

        function getPretamoReferenciasByPrestamoIdTipo(tipo) {
            var params = {
                'prestamo_id': vm.prestamo_id,
                'tipo': tipo,
            };
            prestamoReferenciaService.getPretamoReferenciasByPrestamoIdTipo(params).then(
                function(response) {
                    if (!response.error) {
                        var data = response.data;
                        vm.data_referencias = filterItems(data, { 'tipo': 2 }) ;
                        return vm.data_referencias;
                    }
                }
            );
        };

        function getPretamoGarantiasByPrestamoId() {
            var params = {
                'prestamo_id': vm.prestamo_id,
            };
            prestamoGarantiaService.getPretamoGarantiasByPrestamoId(params).then(
                function(response) {
                    if (!response.error) {
                        vm.data_garantias = response.data;
                        console.info('garantias', vm.data_garantias);
                        return vm.data_garantias;
                    }
                }
            );
        };


        // =====================================================================

            vm.file_descripcion = [];
            // # sfksdfd
            var uploader = vm.uploader = new FileUploader({
                url: 'operaciones/credito-menor/uploads',
                formData: [],
            });

            // FILTERS

            uploader.filters.push({
                name: 'customFilter',
                fn: function(item /*{File|FileLikeObject}*/ , options) {
                    return this.queue.length < 10;
                }
            });


            // CALLBACKS

            uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/ , filter, options) {
                console.info('onWhenAddingFileFailed', item, filter, options);
            };
            uploader.onAfterAddingFile = function(fileItem) {
                console.info('onAfterAddingFile', fileItem);
            };
            uploader.onAfterAddingAll = function(addedFileItems) {
                console.info('onAfterAddingAll', addedFileItems);
            };
            uploader.onBeforeUploadItem = function(item) {
                console.log('onBeforeUploadItem', item);

                var index = uploader.getIndexOfItem(item);
                console.info('file_descripcion: ', vm.file_descripcion[index]);

                item.formData.push({
                    descripcion: vm.file_descripcion[index],
                    prestamo_id: vm.prestamo_id
                });
            };
            uploader.onProgressItem = function(fileItem, progress) {
                console.info('onProgressItem', fileItem, progress);
            };
            uploader.onProgressAll = function(progress) {
                console.info('onProgressAll', progress);
            };
            uploader.onSuccessItem = function(fileItem, response, status, headers) {
                // console.info('onSuccessItem', fileItem, response, status, headers);
                console.info('onSuccessItem fileItem', fileItem);
                console.info('onSuccessItem response', response);
                console.info('onSuccessItem status', status);
                console.info('onSuccessItem headers', headers);


            };
            uploader.onErrorItem = function(fileItem, response, status, headers) {
                console.info('onErrorItem', fileItem, response, status, headers);
            };
            uploader.onCancelItem = function(fileItem, response, status, headers) {
                console.info('onCancelItem', fileItem, response, status, headers);
                vm.msj = null;
            };
            uploader.onCompleteItem = function(fileItem, response, status, headers) {
                console.info('onCompleteItem', fileItem, response, status, headers);
            };
            uploader.onCompleteAll = function() {
                console.info('onCompleteAll');
            };
        // =====================================================================


        function update()
        {
            var tipo_prestamo_id = vm.formData.tipo_prestamo_id ;
            var tipo_pago_id     = vm.formData.tipo_pago_id ;
            var cliente_id       = vm.formData.cliente_id ;
            var tipo_moneda_id   = vm.formData.tipo_moneda_id ;
            var valor            = vm.formData.monto ;
            var tasa_interes     = vm.formData.interes ;
            var tipo_periodo_id  = vm.formData.tipo_periodo_id ;
            var acuerdo_pago_id  = vm.formData.acuerdo_pago_id ;
            var num_cuotas       = vm.formData.num_cuotas ;
            var complacencia     = vm.formData.complacencia ;
            var observacion      = vm.formData.observacion ;
            var fecha_credito    = vm.formData.fecha_credito ;
            var fecha_desembolso = vm.formData.fecha_desembolso ;
            var referencia_web    = vm.formData.referencia_web ;

            var data_garantias   = vm.tableParams.settings().dataset ;
            vm.msj = {message : ''} ;

            // VALIDACION
                if (tipo_prestamo_id === undefined)
                {
                    return  vm.msj.message = 'Seleccionar Tipo Prestramo' ;
                };

                if (cliente_id === undefined)
                {
                    return  vm.msj.message = 'Seleccionar Cliente' ;
                };

                if (tipo_moneda_id === undefined)
                {
                    return  vm.msj.message = 'Seleccionar Tipo de Moneda' ;
                };

                if (valor === undefined)
                {
                    return  vm.msj.message = 'Ingresar Monto de prestamo' ;
                };

                if (tasa_interes === undefined)
                {
                    return  vm.msj.message = 'Ingresar Tasa de Interes' ;
                };

                if (tipo_periodo_id === undefined)
                {
                    return  vm.msj.message = 'Seleccionar Periodo de Cuotas' ;
                };

                if (num_cuotas === undefined)
                {
                    return  vm.msj.message = 'Ingresar Número de Cuotas' ;
                };

                if (fecha_desembolso === undefined)
                {
                    return  vm.msj.message = 'Ingresar Fecha Desembolso' ;
                };

                if (fecha_credito === undefined)
                {
                    return  vm.msj.message = 'Ingresar Fecha Desembolso' ;
                };


            var fecha_credito = $filter('date')(vm.formData.fecha_credito,'yyyy-MM-dd');
            var fecha_desembolso = $filter('date')(vm.formData.fecha_desembolso,'yyyy-MM-dd');

            var params = {
                'prestamo_id' : vm.prestamo_id,
                'tipo_prestamo_id' : tipo_prestamo_id,
                'tipo_pago_id' : tipo_pago_id,
                'cliente_id' : cliente_id,
                'tipo_moneda_id' : tipo_moneda_id,
                'valor' : valor,
                'tasa_interes' : tasa_interes,
                'tipo_periodo_id' : tipo_periodo_id,
                'acuerdo_pago_id' : acuerdo_pago_id,
                'num_cuotas' : num_cuotas,
                'complacencia' : complacencia,
                'observacion' : observacion,
                'fecha_credito' : fecha_credito,
                'fecha_desembolso' : fecha_desembolso,
                'referencia_web' : referencia_web,
                'data_garantias' : data_garantias
            };

            console.info('params', params);
            // return ;
            creditoMenorService.update(params).then(
                function(response)
                {
                    console.log(response);
                    if (!response.error)
                    {
                        vm.prestamo_id = response.data ;
                        setTimeout(vm.uploader.uploadAll(), 1000);
                        setTimeout(vm.reloadList(), 1500);

                    }else
                    {
                        vm.msj = response.error ;
                    }
                }
            );
        };

        vm.ok = function() {
            console.log('ok');
            update();
        };

        vm.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.reloadList = function() {
            var current = $state.current.name;
            var res = current.split(".", 2);
            var current_parent = res[0] + "." + res[1];

            var values = '';
            $state.go(current_parent, values);
            $rootScope.$broadcast("reloadList", values);
        };

        /*vm.data_list_coutas = [];

        function getCuotasByPrestamoId() {
            var data = {
                'prestamo_id': vm.prestamo_id,
            };

            cuotaService.getCuotasByPrestamoId(data).then(
                function(response) {
                    console.log(response);
                    if (!response.error) {
                        vm.data_list_coutas = response.data;
                        return vm.data_list_coutas;

                    }
                }
            );
        };*/

        // =====================================================================
            vm.simpleList = [];
            var originalData = angular.copy(vm.simpleList);

            vm.tableParams = new NgTableParams({}, {
                dataset: angular.copy(vm.simpleList)
            });

            vm.deleteCount = 0;

            vm.add = add;
            vm.cancelChanges = cancelChanges;
            vm.del = del;
            vm.hasChanges = hasChanges;
            vm.saveChanges = saveChanges;

            //////////

            function add() {
                vm.isEditing = true;
                vm.isAdding = true;
                vm.tableParams.settings().dataset.unshift({
                    tipo_garantia: null,
                    // tipo_garantia_id : null ,
                    producto: null,
                    serie: null,
                    desripcion: null,
                });
                // we need to ensure the user sees the new row we've just added.
                // it seems a poor but reliable choice to remove sorting and move them to the first page
                // where we know that our new item was added to
                vm.tableParams.sorting({});
                vm.tableParams.page(1);
                vm.tableParams.reload();
            }

            function cancelChanges() {
                resetTableStatus();
                var currentPage = vm.tableParams.page();
                vm.tableParams.settings({
                    dataset: angular.copy(originalData)
                });
                // keep the user on the current page when we can
                if (!vm.isAdding) {
                    vm.tableParams.page(currentPage);
                }
            };

            function resetRow(row, rowForm) {
                row.isEditing = false;
                rowForm.$setPristine();
                for (var i in vm.data_list) {
                    if (vm.data_list[i].id === row.id) {
                        return vm.data_list[i]
                    }
                }
            };

            function arrayObjectIndexOf(arr, obj) {
                for (var i = 0; i < arr.length; i++) {
                    if (angular.equals(arr[i], obj)) {
                        return i;
                    }
                };
                return -1;
            }

            function del(row) {
                var data = vm.tableParams.settings().dataset;
                for (var i in data) {
                    if (angular.equals(data[i], row)) {
                        vm.tableParams.settings().dataset.splice(i, 1);
                        vm.deleteCount++;
                        vm.tableParams.reload().then(function(data) {
                            if (data.length === 0 && vm.tableParams.total() > 0) {
                                vm.tableParams.page(vm.tableParams.page() - 1);
                                vm.tableParams.reload();
                                console.log(vm.tableParams.settings().dataset.length);
                                if (vm.tableParams.settings().dataset.length === 0) { vm.cancelChanges() };
                            }
                        });
                        return i;
                    }
                }

            }

            function hasChanges() {
                return vm.tableForm.$dirty || vm.deleteCount > 0
            }

            function resetTableStatus() {
                vm.isEditing = false;
                vm.isAdding = false;
                vm.deleteCount = 0;
                // vm.tableTracker.reset();
                vm.tableForm.$setPristine();
            }

            function saveChanges() {
                resetTableStatus();
                var currentPage = vm.tableParams.page();
                originalData = angular.copy(vm.tableParams.settings().dataset);
            }


        // =====================================================================


        function filterItems(items, props) {
            var out = [];

            //  texto es el mismo que se buscara para todos de elementos (por ejemplo {name: "12", age: "12"})
            var keys = Object.keys(props);
            var firstElement = keys[0];

            var text = String(props[firstElement]).toLowerCase();
            // text diferente de nulo,  recoremos el array
            if (angular.isArray(items)) {
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    var itemMatches = false;
                    var prop = keys[0];
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                }

            } else {
                // Let the output be the input untouched
                out = items;
            }
            return out;
        };

        // ========= update
            vm.prestamoReferenciaUpdateEstado = prestamoReferenciaUpdateEstado ;
            function prestamoReferenciaUpdateEstado(id)
            {
                var params = {
                    'prestamo_referencia_id': id,
                    'estado': 0,
                };

                prestamoReferenciaService.updateEstado(params).then(
                    function(response) {
                        console.log(response);
                        if (!response.error)
                        {
                            getPretamoReferenciasByPrestamoIdTipo(2) ;
                            // var  = response.data;
                            return ;

                        }
                    }
                );
            };

            vm.prestamoGarantiaUpdateEstado = prestamoGarantiaUpdateEstado ;
            function prestamoGarantiaUpdateEstado(id)
            {
                var params = {
                    'prestamo_garantia_id': id,
                    'estado': 0,
                };
                console.info(params);
                prestamoGarantiaService.updateEstado(params).then(
                    function(response) {
                        console.log(response);
                        if (!response.error)
                        {
                            getPretamoGarantiasByPrestamoId() ;
                            return ;

                        }
                    }
                );
            };


    };
})();

(function(){
    'use stric' ;

    angular.module('cred.menor.mov.controller').controller('ModalAprobarCredMenorCrtl', ModalAprobarCredMenorCrtl) ;
    ModalAprobarCredMenorCrtl.$inject = ['$uibModalInstance', 'data_prestamo','prestamoService'] ;

        function ModalAprobarCredMenorCrtl($uibModalInstance,data_prestamo, prestamoService)
        {
            var vm =  this ;

            vm.msj = "";
            vm.fillSelected = data_prestamo ;

            vm.formData = {
                mora : '',
            }

            vm.ok = function ()
            {
                var params = {
                    'mora' : vm.formData.mora,
                    'tipo_estado_id' : 2,
                    'prestamo_id' : vm.fillSelected.id,
                };
                // return
                // console.log(params);
                prestamoService.updateEstadoPrestamoTipoEstado(params).then(
                    function(response)
                    {
                        console.log(response);

                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use stric' ;

    angular.module('cred.menor.mov.controller').controller('ModalCancelarCredMenorCrtl', ModalCancelarCredMenorCrtl) ;
    ModalCancelarCredMenorCrtl.$inject = ['$uibModalInstance', 'data_prestamo','prestamoService'] ;

        function ModalCancelarCredMenorCrtl($uibModalInstance,data_prestamo, prestamoService)
        {
            var vm =  this ;

            vm.msj = "";
            vm.fillSelected = data_prestamo ;

            vm.formData = {
                glosa : '',
            }

            vm.ok = function ()
            {
                var params = {
                    'glosa' : vm.formData.glosa,
                    'tipo_estado_id' : 3,
                    'prestamo_id' : vm.fillSelected.id,
                };
                console.log(params);
                // return ;
                prestamoService.updateEstadoPrestamoTipoEstado(params).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use stric' ;

    angular.module('cred.menor.mov.controller').controller('NewCredMenorCrtl', NewCredMenorCrtl) ;
    NewCredMenorCrtl.$inject = [
                                    '$rootScope',
                                    '$state',
                                    '$filter',
                                    'tipoPrestamoService',
                                    'tipoMonedaService',
                                    'tipoPeriodoService',
                                    'tipoGarantiaService',
                                    'tipoPagoService',
                                    'empleadoService',
                                    'clienteService',
                                    'vehiculoService',
                                    'avalService',
                                    'prestamoService',
                                    'acuerdoPagoService',
                                    'FileUploader',
                                    'NgTableParams',
                                    'creditoMenorService'
                                ] ;

        function NewCredMenorCrtl(
                $rootScope,
                $state,
                $filter,
                tipoPrestamoService,
                tipoMonedaService,
                tipoPeriodoService,
                tipoGarantiaService,
                tipoPagoService,
                empleadoService,
                clienteService,
                vehiculoService,
                avalService,
                prestamoService,
                acuerdoPagoService,
                FileUploader,
                NgTableParams,
                creditoMenorService
            )
        {
            var vm =  this ;

            vm.msj = "";
            vm.formData = {
                tipo_prestamo : undefined,
                tipo_moneda : undefined,
                tipo_periodo : undefined,
                acuerdo_pago : undefined,
                tipo_pago : undefined,
                tipo_garantia : undefined,
                asesor_negocio : undefined,
                cliente : undefined,
                aval : undefined,
                interes : '',
                num_cuotas : '',
                mora : '',
                complacencia : '',
                pagos_parciales : 'NO',
                propietario : '',
                observacion :'' ,
                fecha_credito :null ,
                fecha_desembolso :null ,
                referencia_web : '' ,
                monto : '',
            } ;



            vm.prestamo_id = null ;

            vm.tipo_prestamos   = [] ;
            vm.tipo_monedas     = [] ;
            vm.tipo_periodos    = [] ;
            vm.acuerdo_pagos    = [] ;
            vm.tipo_pagos       = [] ;
            vm.tipo_garantias   = [] ;
            vm.asesores_negocio = [] ;
            vm.clientes         = [] ;
            vm.data_vehiculos   = [] ;
            vm.data_avales      = [] ;

            // var view
                vm.ASCVEH = false ;

            //  DatePicker ui-b
                vm.popup = {
                    opened : false,
                } ;
                vm.dateOptions = {
                    startingDay: 1, // inicie en lunes
                    showWeeks:'false',
                    minDate: null,
                };

                vm.openCalendar = function() {
                    vm.popup.opened = true;
                };

                vm.popup2 = {
                    opened : false,
                } ;
                vm.openCalendar2 = function() {
                    vm.popup2.opened = true;
                };

                vm.popup3 = {
                    opened : false,
                } ;
                vm.openCalendar3 = function() {
                    vm.popup3.opened = true;
                };

                vm.popup4 = {
                    opened : false,
                } ;
                vm.openCalendar4 = function() {
                    vm.popup4.opened = true;
                };

            init();
            function init()
            {
                getTipoPrestamos();
                getClientesInfoBasica() ;
                // setTimeout(getEmpleadosInfoBasica(), 1000);
                setTimeout(getTipoMonedas(), 1500);
                setTimeout(getTipoPeriodos(), 2000);
                setTimeout(getTipoGarantias(), 2500);

            };

            //  carga de datos

                function getTipoPrestamos()
                {
                    tipoPrestamoService.getTipoPrestamos().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_prestamos = response.data;
                                vm.formData.tipo_prestamo =  getFill(vm.tipo_prestamos, 4) ;
                                getTipoPagosByTipoPrestamoId();
                                return vm.tipo_prestamos ;
                            }
                        }
                    );
                };
                function getTipoMonedas()
                {
                    tipoMonedaService.getTipoMonedas().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_monedas = response.data;
                                return vm.tipo_monedas ;
                            }
                        }
                    );
                };

                function getTipoPeriodos()
                {
                    tipoPeriodoService.getTipoPeriodos().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_periodos = response.data;
                                return vm.tipo_periodos ;
                            }
                        }
                    );
                };

                // tipo-couta
                function getTipoPagosByTipoPrestamoId()
                {
                    var data = {
                        // 'tipo_prestamo_id' : vm.formData.tipo_prestamo.id,
                        'tipo_prestamo_id' : 4,
                    };
                    tipoPagoService.getTipoPagosByTipoPrestamoId(data).then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_pagos = response.data;
                                return vm.tipo_pagos ;
                            }
                        }
                    );
                };

                vm.getAcuerdoPagosByTipoPeriodoId =  function($item, $model)
                {
                    var data = {
                        'tipo_periodo_id' : vm.formData.tipo_periodo.id,
                    };

                    acuerdoPagoService.getAcuerdoPagosByTipoPeriodoId(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.acuerdo_pagos = response.data;
                                return vm.acuerdo_pagos ;
                            }
                        }
                    );
                };

                function getTipoGarantias()
                {
                    tipoGarantiaService.getTipoGarantias().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_garantias = response.data;
                                return vm.tipo_garantias ;
                            }
                        }
                    );
                };

                function getClientesInfoBasica()
                {
                    var per_tipo = 0 ;
                    var data = {
                        'per_tipo' : per_tipo,
                    } ;

                    clienteService.getClientesInfoBasica(data).then(
                        function(response)
                        {
                            console.log(response);
                            if (!response.error)
                            {
                                vm.clientes = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );
                } ;

                function getFill(data, idSelected)
                {
                    var out = []
                    for(var i in data)
                    {
                        if(parseInt(data[i].id) === parseInt(idSelected))
                        {
                            out = data[i] ;
                            return out;
                        };
                    } ;
                    return out;
                } ;

                vm.save = save ;
                function save()
                {
                    var tipo_prestamo_id = vm.formData.tipo_prestamo.id ;
                    var tipo_pago_id     = vm.formData.tipo_pago.id ;
                    var cliente_id       = vm.formData.cliente.id ;
                    var tipo_moneda_id   = vm.formData.tipo_moneda.id ;
                    var valor            = vm.formData.monto ;
                    var tasa_interes     = vm.formData.interes ;
                    var tipo_periodo_id  = vm.formData.tipo_periodo.id ;
                    var acuerdo_pago_id  = vm.formData.acuerdo_pago.id ;
                    var num_cuotas       = vm.formData.num_cuotas ;
                    var complacencia     = vm.formData.complacencia ;
                    var observacion      = vm.formData.observacion ;
                    var fecha_credito    = vm.formData.fecha_credito ;
                    var fecha_desembolso = vm.formData.fecha_desembolso ;
                    var referencia_web    = vm.formData.referencia_web ;

                    var data_garantias   = vm.tableParams.settings().dataset ;
                    vm.msj = {message : ''} ;

                    // VALIDACION
                        if (tipo_prestamo_id === undefined)
                        {
                            return  vm.msj.message = 'Seleccionar Tipo Prestramo' ;
                        };

                        if (cliente_id === undefined)
                        {
                            return  vm.msj.message = 'Seleccionar Cliente' ;
                        };

                        if (tipo_moneda_id === undefined)
                        {
                            return  vm.msj.message = 'Seleccionar Tipo de Moneda' ;
                        };

                        if (valor === undefined)
                        {
                            return  vm.msj.message = 'Ingresar Monto de prestamo' ;
                        };

                        if (tasa_interes === undefined)
                        {
                            return  vm.msj.message = 'Ingresar Tasa de Interes' ;
                        };

                        if (tipo_periodo_id === undefined)
                        {
                            return  vm.msj.message = 'Seleccionar Periodo de Cuotas' ;
                        };

                        if (num_cuotas === undefined)
                        {
                            return  vm.msj.message = 'Ingresar Número de Cuotas' ;
                        };

                        if (fecha_desembolso === undefined)
                        {
                            return  vm.msj.message = 'Ingresar Fecha Desembolso' ;
                        };

                        if (fecha_credito === undefined)
                        {
                            return  vm.msj.message = 'Ingresar Fecha Desembolso' ;
                        };


                    var fecha_credito = $filter('date')(vm.formData.fecha_credito,'yyyy-MM-dd');
                    // vm.formData.fecha_credito = fecha_credito ;
                    var fecha_desembolso = $filter('date')(vm.formData.fecha_desembolso,'yyyy-MM-dd');

                    var params = {
                        'tipo_prestamo_id' : tipo_prestamo_id,
                        'tipo_pago_id' : tipo_pago_id,
                        'cliente_id' : cliente_id,
                        'tipo_moneda_id' : tipo_moneda_id,
                        'valor' : valor,
                        'tasa_interes' : tasa_interes,
                        'tipo_periodo_id' : tipo_periodo_id,
                        'acuerdo_pago_id' : acuerdo_pago_id,
                        'num_cuotas' : num_cuotas,
                        'complacencia' : complacencia,
                        'observacion' : observacion,
                        'fecha_credito' : fecha_credito,
                        'fecha_desembolso' : fecha_desembolso,
                        'referencia_web' : referencia_web,
                        'data_garantias' : data_garantias
                    };

                    // console.log(params);
                    creditoMenorService.save(params).then(
                        function(response)
                        {
                            console.log(response);
                            if (!response.error)
                            {
                                vm.prestamo_id = response.data ;
                                setTimeout(vm.uploader.uploadAll(), 1000);

                                 vm.reloadList();
                                // vm.data_avales = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );
                };

                vm.reloadList = function()
                {
                    var current = $state.current.name ;
                    var res = current.split(".",2);
                    var current_parent = res[0]+"."+res[1];

                    var values = '' ;
                    $state.go(current_parent, values);
                    $rootScope.$broadcast("reloadList", values);
                };

            // ========= START UPLOADs ======================================================

                vm.file_descripcion = [];
                // # sfksdfd
                var uploader = vm.uploader = new FileUploader({
                    url: 'operaciones/credito-menor/uploads',
                    formData: [],
                });

                // FILTERS

                uploader.filters.push({
                    name: 'customFilter',
                    fn: function(item /*{File|FileLikeObject}*/, options) {
                        return this.queue.length < 10;
                    }
                });


                // CALLBACKS

                uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
                    console.info('onWhenAddingFileFailed', item, filter, options);
                };
                uploader.onAfterAddingFile = function(fileItem) {
                    console.info('onAfterAddingFile', fileItem);
                };
                uploader.onAfterAddingAll = function(addedFileItems) {
                    console.info('onAfterAddingAll', addedFileItems);
                };
                uploader.onBeforeUploadItem = function(item) {
                    console.log('onBeforeUploadItem', item);

                    var index = uploader.getIndexOfItem(item);
                    console.info('file_descripcion: ',vm.file_descripcion[index]);

                    item.formData.push({
                                        descripcion: vm.file_descripcion[index],
                                        prestamo_id: vm.prestamo_id
                                    });
                };
                uploader.onProgressItem = function(fileItem, progress) {
                    console.info('onProgressItem', fileItem, progress);
                };
                uploader.onProgressAll = function(progress) {
                    console.info('onProgressAll', progress);
                };
                uploader.onSuccessItem = function(fileItem, response, status, headers) {
                    // console.info('onSuccessItem', fileItem, response, status, headers);
                    console.info('onSuccessItem fileItem', fileItem);
                    console.info('onSuccessItem response', response);
                    console.info('onSuccessItem status', status);
                    console.info('onSuccessItem headers', headers);


                };
                uploader.onErrorItem = function(fileItem, response, status, headers) {
                    console.info('onErrorItem', fileItem, response, status, headers);
                };
                uploader.onCancelItem = function(fileItem, response, status, headers) {
                    console.info('onCancelItem', fileItem, response, status, headers);
                    vm.msj = null ;
                };
                uploader.onCompleteItem = function(fileItem, response, status, headers) {
                    console.info('onCompleteItem', fileItem, response, status, headers);
                };
                uploader.onCompleteAll = function() {
                    console.info('onCompleteAll');
                };
            // ========= END UPLOADS ========================================================


            // ========= START TABLE GARANTIAS ========================================================
                vm.simpleList = [] ;
                var originalData = angular.copy(vm.simpleList);

                vm.tableParams = new NgTableParams({}, {
                  dataset: angular.copy(vm.simpleList)
                });

                vm.deleteCount = 0;

                vm.add           = add;
                vm.cancelChanges = cancelChanges;
                vm.del           = del;
                vm.hasChanges    = hasChanges;
                vm.saveChanges   = saveChanges;

                //////////

                function add() {
                  vm.isEditing = true;
                  vm.isAdding = true;
                  vm.tableParams.settings().dataset.unshift({
                   tipo_garantia : null ,
                   producto : null ,
                   serie : null ,
                   desripcion : null ,
                  });
                  // we need to ensure the user sees the new row we've just added.
                  // it seems a poor but reliable choice to remove sorting and move them to the first page
                  // where we know that our new item was added to
                  vm.tableParams.sorting({});
                  vm.tableParams.page(1);
                  vm.tableParams.reload();
                }

                function cancelChanges() {
                  resetTableStatus();
                  var currentPage = vm.tableParams.page();
                  vm.tableParams.settings({
                    dataset: angular.copy(originalData)
                  });
                  // keep the user on the current page when we can
                  if (!vm.isAdding) {
                    vm.tableParams.page(currentPage);
                  }
                };
                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                 function arrayObjectIndexOf(arr, obj){
                    for(var i = 0; i < arr.length; i++){
                        if(angular.equals(arr[i], obj)){
                            return i;
                        }
                    };
                    return -1;
                }

                function del(row) {
                    var data = vm.tableParams.settings().dataset ;
                    for ( var i in data)
                    {
                        if(angular.equals(data[i], row))
                        {
                                vm.tableParams.settings().dataset.splice(i, 1);
                                vm.deleteCount++;
                                vm.tableParams.reload().then(function(data) {
                                    if (data.length === 0 && vm.tableParams.total() > 0) {
                                      vm.tableParams.page(vm.tableParams.page() - 1);
                                      vm.tableParams.reload();
                                      console.log(vm.tableParams.settings().dataset.length);
                                      if ( vm.tableParams.settings().dataset.length === 0) {vm.cancelChanges()};
                                    }
                                });
                            return i;
                        }

                    }

                }

                function hasChanges() {
                  return vm.tableForm.$dirty || vm.deleteCount > 0
                }

                function resetTableStatus() {
                  vm.isEditing = false;
                  vm.isAdding = false;
                  vm.deleteCount = 0;
                  // vm.tableTracker.reset();
                  vm.tableForm.$setPristine();
                }

                function saveChanges() {
                  resetTableStatus();
                  var currentPage = vm.tableParams.page();
                  originalData = angular.copy(vm.tableParams.settings().dataset);
                }
            // ========= END TABLE GARANTIAS ==========================================================

            // ========= START cuota libre ======================================================
                vm.onSelectTipoCuota = function($item, $model)
                {

                    var tipo_pago_id = parseInt($item.id) ;
                    // var tipo_pago_id = parseInt(vm.formData.tipo_prestamo.id) ;

                    vm.inputDisabled = false ;
                    if (tipo_pago_id === 6)
                    {
                        vm.formData.num_cuotas = 4 ;
                        vm.inputDisabled = true ;
                    };

                } ;

            // =========  End cuota libre ======================================================




        };
})() ;
(function(){
    'use stric' ;

    angular.module('prestamos.mov.controller').controller('DetallePrestamoCrtl', DetallePrestamoCrtl) ;
    DetallePrestamoCrtl.$inject = [
                                    '$stateParams',
                                    '$rootScope',
                                    '$state',
                                    '$filter',
                                    'tipoPrestamoService',
                                    'tipoMonedaService',
                                    'tipoPeriodoService',
                                    'tipoGarantiaService',
                                    'tipoPagoService',
                                    'empleadoService',
                                    'clienteService',
                                    'vehiculoService',
                                    'avalService',
                                    'prestamoService',
                                    'acuerdoPagoService',
                                    'cuotaService',
                                    'PATH',
                                    '$uibModal',
                                    'prestamoEstadoService',
                                ] ;

        function DetallePrestamoCrtl(
                $stateParams,
                $rootScope,
                $state,
                $filter,
                tipoPrestamoService,
                tipoMonedaService,
                tipoPeriodoService,
                tipoGarantiaService,
                tipoPagoService,
                empleadoService,
                clienteService,
                vehiculoService,
                avalService,
                prestamoService,
                acuerdoPagoService,
                cuotaService,
                PATH,
                $uibModal,
                prestamoEstadoService
            )
        {
            var vm =  this ;

            vm.prestamo_id = $stateParams.codigo;
            vm.data_list = [] ;

            vm.disabledInput =  true ;
            vm.disabledInput =  true ;


            vm.msj = "";

            vm.formData = {
                tipo_prestamo : undefined,
                tipo_moneda : undefined,
                tipo_periodo : undefined,
                acuerdo_pago : undefined,
                tipo_pago : undefined,
                tipo_garantia : undefined,
                asesor_negocio : undefined,
                cliente : undefined,
                aval : undefined,
                interes : '',
                num_cuotas : '',
                mora : '',
                complacencia : '',
                pagos_parciales : 'NO',
                propietario : '',
                observacion :'' ,
                fecha_credito :null ,
                fecha_desembolso :null ,
                fecha_prorrateo :null ,
                fecha_prorrateo_esp :null ,
                check_prorrateo : false ,
                check_prorrateo_esp : false ,
                acuerdo_pago_id : undefined ,
            } ;



            vm.formData_ascveh = {
                costo : '',
                inicial_porcentaje : '',
                inicial_monto : '',
                seguro_tr : '',
                gps : '',
                soat : '',
                gas : '',
                otros : '',
                vehiculo_id : '',

            } ;

            vm.tipo_prestamos     = [] ;
            vm.tipo_monedas       = [] ;
            vm.tipo_periodos      = [] ;
            vm.acuerdo_pagos      = [] ;
            vm.tipo_pagos         = [] ;
            vm.tipo_garantias     = [] ;
            vm.asesores_negocio   = [] ;
            vm.clientes           = [] ;
            vm.data_vehiculos     = [] ;
            vm.data_avales        = [] ;
            vm.disabledInputsEdit = true;
            vm.data_estados_info  = [] ;

            // var view
                vm.ASCVEH = false ;

            //  DatePicker ui-b
                vm.popup = {
                    opened : false,
                } ;
                vm.dateOptions = {
                    startingDay: 1, // inicie en lunes
                    showWeeks:'false',
                    minDate: null,
                };

                vm.openCalendar = function() {
                    vm.popup.opened = true;
                };

                vm.popup2 = {
                    opened : false,
                } ;
                vm.openCalendar2 = function() {
                    vm.popup2.opened = true;
                };

                vm.popup3 = {
                    opened : false,
                } ;
                vm.openCalendar3 = function() {
                    vm.popup3.opened = true;
                };

                vm.popup4 = {
                    opened : false,
                } ;
                vm.openCalendar4 = function() {
                    vm.popup4.opened = true;
                };

            init();
            function init()
            {
                getPrestamoById() ;
                getTipoPrestamos();
                // getClientesInfoBasica() ;
                setTimeout(getClientesInfoBasica(), 1000);
                setTimeout(getTipoMonedas(), 1500);
                setTimeout(getTipoPeriodos(), 2000);
                setTimeout(getTipoGarantias(), 2500);
                setTimeout(getAvalesInfoBasica(), 3000);
                setTimeout(getCuotasByPrestamoId(), 5000);
                setTimeout(getPrestamoEstadosInfo(), 6000);


            };

            //  carga de datos

                function getPrestamoById()
                {
                    var params = {
                        'prestamo_id': vm.prestamo_id ,
                    };
                    prestamoService.getPrestamoById(params).then(
                        function(response){
                            if (!response.error)
                            {
                                vm.data_list = response.data;

                                vm.formData.tipo_prestamo_id    = vm.data_list.tipo_prestamo_id;
                                vm.formData.tipo_pago_id        = vm.data_list.tipo_pago_id;
                                vm.formData.cliente_id          = vm.data_list.cliente_id;
                                vm.formData.tipo_moneda_id      = vm.data_list.tipo_moneda_id;
                                vm.formData.interes             = vm.data_list.tasa_interes;
                                vm.formData.tipo_periodo_id     = vm.data_list.tipo_periodo_id;
                                vm.formData.acuerdo_pago_id     = vm.data_list.acuerdo_pago_id;
                                vm.formData.num_cuotas          = vm.data_list.num_cuotas;
                                vm.formData.pagos_parciales     = vm.data_list.pagos_parciales;
                                vm.formData.propietario         = vm.data_list.propietario;
                                vm.formData.aval_id             = vm.data_list.aval_id;
                                vm.formData.observacion         = vm.data_list.observacion;
                                vm.formData.tipo_garantia_id    = vm.data_list.tipo_garantia_id;
                                vm.formData.fecha_credito       = (!vm.data_list.fecha_credito)? null :new Date(vm.data_list.fecha_credito+' 00:00:00');
                                vm.formData.fecha_desembolso    = (!vm.data_list.fecha_desembolso)? null :new Date(vm.data_list.fecha_desembolso+' 00:00:00');
                                vm.formData.fecha_prorrateo     = (!vm.data_list.fecha_prorrateo)? null :new Date(vm.data_list.fecha_prorrateo+' 00:00:00');
                                vm.formData.fecha_prorrateo_esp = (!vm.data_list.fecha_prorrateo_esp)? null :new Date(vm.data_list.fecha_prorrateo_esp+' 00:00:00');


                                vm.formData_ascveh.costo              = vm.data_list.valor ;
                                vm.formData_ascveh.inicial_porcentaje = vm.data_list.inicial_porcentaje ;
                                vm.formData_ascveh.inicial_monto      = vm.data_list.inicial_monto ;
                                vm.formData_ascveh.seguro_tr          = vm.data_list.seguro_tr ;
                                vm.formData_ascveh.gps                = vm.data_list.gps ;
                                vm.formData_ascveh.soat               = vm.data_list.soat ;
                                vm.formData_ascveh.gas                = vm.data_list.gas ;
                                vm.formData_ascveh.otros              = vm.data_list.otros ;
                                vm.formData_ascveh.vehiculo_id        = vm.data_list.vehiculo_id ;

                                getTipoPagosByTipoPrestamoId ();
                                onSelectTipoPrestamo(null);
                                getAcuerdoPagosByTipoPeriodoId(null,null);
                                return vm.tipo_prestamos ;
                            }
                        }
                    );
                };


                function getTipoPrestamos()
                {
                    tipoPrestamoService.getTipoPrestamos().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_prestamos = response.data;
                                return vm.tipo_prestamos ;
                            }
                        }
                    );
                };

                function getTipoMonedas()
                {
                    tipoMonedaService.getTipoMonedas().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_monedas = response.data;
                                return vm.tipo_monedas ;
                            }
                        }
                    );
                };

                function getTipoPeriodos()
                {
                    tipoPeriodoService.getTipoPeriodos().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_periodos = response.data;
                                return vm.tipo_periodos ;
                            }
                        }
                    );
                };

                function getTipoPagosByTipoPrestamoId()
                {
                    var data = {
                        'tipo_prestamo_id' : vm.formData.tipo_prestamo_id,
                    };
                    tipoPagoService.getTipoPagosByTipoPrestamoId(data).then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_pagos = response.data;
                                return vm.tipo_pagos ;
                            }
                        }
                    );
                };

                vm.getAcuerdoPagosByTipoPeriodoId = getAcuerdoPagosByTipoPeriodoId ;
                function getAcuerdoPagosByTipoPeriodoId($item, $model)
                {
                    var data = {
                        'tipo_periodo_id' : vm.formData.tipo_periodo_id,
                    };
                    acuerdoPagoService.getAcuerdoPagosByTipoPeriodoId(data).then(
                        function(response){
                            if (!response.error)
                            {
                                vm.acuerdo_pagos = response.data;
                                return vm.acuerdo_pagos ;
                            }
                        }
                    );
                };

                function getTipoGarantias()
                {
                    tipoGarantiaService.getTipoGarantias().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_garantias = response.data;
                                return vm.tipo_garantias ;
                            }
                        }
                    );
                };

                function getClientesInfoBasica()
                {
                    var per_tipo = 0 ;
                    var data = {
                        'per_tipo' : per_tipo,
                    } ;

                    clienteService.getClientesInfoBasica(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.clientes = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );
                } ;

                function getVehiculosForSelect()
                {
                    vehiculoService.getVehiculosForSelect().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_vehiculos = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );

                } ;

                function getAvalesInfoBasica()
                {
                    avalService.getAvalesInfoBasica().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var data = response.data ;


                                vm.data_avales = [{
                                    'full_name' : 'Sin Aval' ,
                                    'dni' : null ,
                                    'id' : null ,
                                    'per_apellidos' : null ,
                                    'per_natural_id' : null ,
                                    'per_nombre' : null ,
                                    'persona_id' : null ,
                                 }].concat(data)  ;

                                // vm.data_avales = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );
                } ;

                // seleccionar un ITEM del un combo
                function selecterItemCombo(data, idSeleccionar)
                {
                    // var data = vm.tipo_garantias ;
                    var idSelected = parseInt(idSeleccionar);
                    var out = [] ;
                    for(var i in data)
                    {
                        var id =  parseInt(data[i].id);
                        if( id === idSelected)
                        {
                           out = data[i] ;
                           break ;
                        }

                    }
                    return out ;
                };

            vm.onSelectTipoPrestamo = onSelectTipoPrestamo ;
            function onSelectTipoPrestamo($item)
            {
                vm.ASCVEH = false ;
                var tipo_prestamo_id = parseInt(vm.formData.tipo_prestamo_id ) ;
                vm.activeTipoGarantia = false ;
                if (tipo_prestamo_id === 1)
                {
                    vm.ASCVEH = true ;
                    getVehiculosForSelect();
                    // seleccionar el item Vehicular
                    vm.formData.tipo_garantia = selecterItemCombo(vm.tipo_garantias, 1) ;
                    vm.activeTipoGarantia = true ;
                };
                getTipoPagosByTipoPrestamoId() ;
            } ;

            // ASCVEH

                vm.inciarParamsAscveh = inciarParamsAscveh ;
                function inciarParamsAscveh(params)
                {
                    var costo    = vm.formData_ascveh.costo ;
                    var inicial_porcentaje = vm.formData_ascveh.inicial_porcentaje ;
                    var inicial_monto      = vm.formData_ascveh.inicial_monto ;
                    // vm.formData_ascveh.inicial_monto = parseFloat(inicial_monto).toFixed(2);

                    if (inicial_porcentaje === '' || inicial_porcentaje === undefined)
                    return;

                    if (params === 1 )
                    {
                        var calculo_monto = ( inicial_porcentaje * costo ) / 100 ;
                        vm.formData_ascveh.inicial_monto = calculo_monto;
                        // vm.formData_ascveh.inicial_monto = parseFloat(calculo_monto).toFixed(2);
                    }else  if (params === 2 )
                    {
                        var inicial_porcentaje_ = ( inicial_monto / costo ) * 100  ;
                        vm.formData_ascveh.inicial_porcentaje =  inicial_porcentaje_;
                        // vm.formData_ascveh.inicial_porcentaje =  parseFloat(inicial_porcentaje_).toFixed(2); ;
                    };

                } ;

            vm.reloadListPrestamos = function()
            {
                var current = $state.current.name ;
                var res = current.split(".",2);
                var current_parent = res[0]+"."+res[1];

                var values = '' ;
                $state.go(current_parent, values);
                $rootScope.$broadcast("reloadListPrestamos", values);
            };

            vm.data_list_coutas = [] ;
            function getCuotasByPrestamoId()
            {
                var data = {
                    'prestamo_id' : vm.prestamo_id ,
                } ;

                cuotaService.getCuotasByPrestamoId(data).then(
                    function(response){
                        console.log(response);
                        if (!response.error)
                        {
                            vm.data_list_coutas = response.data ;
                            return vm.data_list_coutas ;

                        }
                    }
                );
            };
            vm.data_estados_info = [] ;
            function getPrestamoEstadosInfo()
            {
                var params = {
                    'prestamo_id' : vm.prestamo_id ,
                } ;
                console.log(params);
                prestamoEstadoService.getPrestamoEstadosInfo(params).then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data_estados_info = response.data;
                            console.log(vm.data_estados_info);
                            return vm.data_estados_info ;
                        }
                    }
                );
            };




        };
})() ;
(function(){
    'use stric' ;

    angular.module('prestamos.mov.controller').controller('EditPrestamoMovCrtl', EditPrestamoMovCrtl) ;
    EditPrestamoMovCrtl.$inject = [
                                    '$stateParams',
                                    '$rootScope',
                                    '$state',
                                    '$filter',
                                    'tipoPrestamoService',
                                    'tipoMonedaService',
                                    'tipoPeriodoService',
                                    'tipoGarantiaService',
                                    'tipoPagoService',
                                    'empleadoService',
                                    'clienteService',
                                    'vehiculoService',
                                    'avalService',
                                    'prestamoService',
                                    'acuerdoPagoService',
                                    'cuotaService',
                                    'PATH',
                                    '$uibModal',
                                ] ;

        function EditPrestamoMovCrtl(
                $stateParams,
                $rootScope,
                $state,
                $filter,
                tipoPrestamoService,
                tipoMonedaService,
                tipoPeriodoService,
                tipoGarantiaService,
                tipoPagoService,
                empleadoService,
                clienteService,
                vehiculoService,
                avalService,
                prestamoService,
                acuerdoPagoService,
                cuotaService,
                PATH,
                $uibModal
            )
        {
            var vm =  this ;

            vm.prestamo_id = $stateParams.codigo;
            vm.data_list = [] ;

            vm.disabledInput =  true ;
            vm.disabledInput =  true ;


            vm.msj = "";

            vm.formData = {
                tipo_prestamo : undefined,
                tipo_moneda : undefined,
                tipo_periodo : undefined,
                acuerdo_pago : undefined,
                tipo_pago : undefined,
                tipo_garantia : undefined,
                asesor_negocio : undefined,
                cliente : undefined,
                aval : undefined,
                interes : '',
                num_cuotas : '',
                mora : '',
                complacencia : '',
                pagos_parciales : 'NO',
                propietario : '',
                observacion :'' ,
                fecha_credito :null ,
                fecha_desembolso :null ,
                fecha_prorrateo :null ,
                fecha_prorrateo_esp :null ,
                check_prorrateo : false ,
                check_prorrateo_esp : false ,
                acuerdo_pago_id : undefined ,
            } ;



            vm.formData_ascveh = {
                costo : '',
                inicial_porcentaje : '',
                inicial_monto : '',
                seguro_tr : '',
                gps : '',
                soat : '',
                gas : '',
                otros : '',
                vehiculo_id : '',

            } ;

            vm.tipo_prestamos   = [] ;
            vm.tipo_monedas     = [] ;
            vm.tipo_periodos    = [] ;
            vm.acuerdo_pagos    = [] ;
            vm.tipo_pagos       = [] ;
            vm.tipo_garantias   = [] ;
            vm.asesores_negocio = [] ;
            vm.clientes = [] ;
            vm.data_vehiculos = [] ;
            vm.data_avales = [] ;

            // var view
                vm.ASCVEH = false ;

            //  DatePicker ui-b
                vm.popup = {
                    opened : false,
                } ;
                vm.dateOptions = {
                    startingDay: 1, // inicie en lunes
                    showWeeks:'false',
                    minDate: null,
                };

                vm.openCalendar = function() {
                    vm.popup.opened = true;
                };

                vm.popup2 = {
                    opened : false,
                } ;
                vm.openCalendar2 = function() {
                    vm.popup2.opened = true;
                };

                vm.popup3 = {
                    opened : false,
                } ;
                vm.openCalendar3 = function() {
                    vm.popup3.opened = true;
                };

                vm.popup4 = {
                    opened : false,
                } ;
                vm.openCalendar4 = function() {
                    vm.popup4.opened = true;
                };

            init();
            function init()
            {
                getPrestamoById() ;
                getTipoPrestamos();
                // getClientesInfoBasica() ;
                setTimeout(getClientesInfoBasica(), 1000);
                setTimeout(getTipoMonedas(), 1500);
                setTimeout(getTipoPeriodos(), 2000);
                setTimeout(getTipoGarantias(), 2500);
                setTimeout(getAvalesInfoBasica(), 3000);

                getCuotasByPrestamoId() ;

            };

            //  carga de datos

                function getPrestamoById()
                {
                    var params = {
                        'prestamo_id': vm.prestamo_id ,
                    };
                    prestamoService.getPrestamoById(params).then(
                        function(response){
                            if (!response.error)
                            {
                                vm.data_list = response.data;

                                console.log(vm.data_list);


                                var estado    = parseInt(vm.data_list.estado);
                                vm.disabledInputsEdit    = true;
                                if (estado === 1 ) {
                                    vm.disabledInputsEdit    = false;
                                };

                                vm.formData.tipo_prestamo_id    = vm.data_list.tipo_prestamo_id;
                                vm.formData.tipo_pago_id        = vm.data_list.tipo_pago_id;
                                vm.formData.cliente_id          = vm.data_list.cliente_id;
                                vm.formData.tipo_moneda_id      = vm.data_list.tipo_moneda_id;
                                vm.formData.interes             = vm.data_list.tasa_interes;
                                vm.formData.tipo_periodo_id     = vm.data_list.tipo_periodo_id;
                                vm.formData.acuerdo_pago_id     = vm.data_list.acuerdo_pago_id;
                                vm.formData.num_cuotas          = vm.data_list.num_cuotas;
                                vm.formData.pagos_parciales     = vm.data_list.pagos_parciales;
                                vm.formData.propietario         = vm.data_list.propietario;
                                vm.formData.aval_id             = vm.data_list.aval_id;
                                vm.formData.observacion         = vm.data_list.observacion;
                                vm.formData.tipo_garantia_id    = vm.data_list.tipo_garantia_id;
                                vm.formData.fecha_credito       = (!vm.data_list.fecha_credito)? null :new Date(vm.data_list.fecha_credito+' 00:00:00');
                                vm.formData.fecha_desembolso    = (!vm.data_list.fecha_desembolso)? null :new Date(vm.data_list.fecha_desembolso+' 00:00:00');
                                vm.formData.fecha_prorrateo     = (!vm.data_list.fecha_prorrateo)? null :new Date(vm.data_list.fecha_prorrateo+' 00:00:00');
                                vm.formData.fecha_prorrateo_esp = (!vm.data_list.fecha_prorrateo_esp)? null :new Date(vm.data_list.fecha_prorrateo_esp+' 00:00:00');


                                vm.formData_ascveh.costo              = vm.data_list.valor ;
                                vm.formData_ascveh.inicial_porcentaje = vm.data_list.inicial_porcentaje ;
                                vm.formData_ascveh.inicial_monto      = vm.data_list.inicial_monto ;
                                vm.formData_ascveh.seguro_tr          = vm.data_list.seguro_tr ;
                                vm.formData_ascveh.gps                = vm.data_list.gps ;
                                vm.formData_ascveh.soat               = vm.data_list.soat ;
                                vm.formData_ascveh.gas                = vm.data_list.gas ;
                                vm.formData_ascveh.otros              = vm.data_list.otros ;
                                vm.formData_ascveh.vehiculo_id        = vm.data_list.vehiculo_id ;

                             /*   vm.formData.check_prorrateo_esp = vm.data_list.;
                                vm.formData.check_prorrateo = vm.data_list.;*/
                                // vehiculo_id

                              /*  valor_total
                                complacencia*/

                                getTipoPagosByTipoPrestamoId ();
                                vm.onSelectTipoPrestamo(null);
                                vm.getAcuerdoPagosByTipoPeriodoId(null,null);
                                return vm.tipo_prestamos ;
                            }
                        }
                    );
                };


                function getTipoPrestamos()
                {
                    tipoPrestamoService.getTipoPrestamos().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_prestamos = response.data;
                                return vm.tipo_prestamos ;
                            }
                        }
                    );
                };

                function getTipoMonedas()
                {
                    tipoMonedaService.getTipoMonedas().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_monedas = response.data;
                                return vm.tipo_monedas ;
                            }
                        }
                    );
                };

                function getTipoPeriodos()
                {
                    tipoPeriodoService.getTipoPeriodos().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_periodos = response.data;
                                return vm.tipo_periodos ;
                            }
                        }
                    );
                };

                function getTipoPagosByTipoPrestamoId()
                {
                    var data = {
                        'tipo_prestamo_id' : vm.formData.tipo_prestamo_id,
                    };
                    tipoPagoService.getTipoPagosByTipoPrestamoId(data).then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_pagos = response.data;
                                return vm.tipo_pagos ;
                            }
                        }
                    );
                };

                vm.getAcuerdoPagosByTipoPeriodoId =  function($item, $model)
                {
                    var data = {
                        'tipo_periodo_id' : vm.formData.tipo_periodo_id,
                    };
                    acuerdoPagoService.getAcuerdoPagosByTipoPeriodoId(data).then(
                        function(response){
                            if (!response.error)
                            {
                                vm.acuerdo_pagos = response.data;
                                return vm.acuerdo_pagos ;
                            }
                        }
                    );
                };

                function getTipoGarantias()
                {
                    tipoGarantiaService.getTipoGarantias().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_garantias = response.data;
                                return vm.tipo_garantias ;
                            }
                        }
                    );
                };

                function getClientesInfoBasica()
                {
                    var per_tipo = 0 ;
                    var data = {
                        'per_tipo' : per_tipo,
                    } ;

                    clienteService.getClientesInfoBasica(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.clientes = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );
                } ;

                function getVehiculosForSelect()
                {
                    vehiculoService.getVehiculosForSelect().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_vehiculos = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );

                } ;

                function getAvalesInfoBasica()
                {
                    avalService.getAvalesInfoBasica().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var data = response.data ;


                                vm.data_avales = [{
                                    'full_name' : 'Sin Aval' ,
                                    'dni' : null ,
                                    'id' : null ,
                                    'per_apellidos' : null ,
                                    'per_natural_id' : null ,
                                    'per_nombre' : null ,
                                    'persona_id' : null ,
                                 }].concat(data)  ;

                                // vm.data_avales = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );
                } ;

                // seleccionar un ITEM del un combo
                function selecterItemCombo(data, idSeleccionar)
                {
                    // var data = vm.tipo_garantias ;
                    var idSelected = parseInt(idSeleccionar);
                    var out = [] ;
                    for(var i in data)
                    {
                        var id =  parseInt(data[i].id);
                        if( id === idSelected)
                        {
                           out = data[i] ;
                           break ;
                        }

                    }
                    return out ;
                };

            vm.onSelectTipoPrestamo = function($item)
            {
                vm.ASCVEH = false ;
                var tipo_prestamo_id = parseInt(vm.formData.tipo_prestamo_id ) ;
                vm.activeTipoGarantia = false ;
                if (tipo_prestamo_id === 1)
                {
                    vm.ASCVEH = true ;
                    getVehiculosForSelect();
                    // seleccionar el item Vehicular
                    vm.formData.tipo_garantia = selecterItemCombo(vm.tipo_garantias, 1) ;
                    vm.activeTipoGarantia = true ;
                };
                getTipoPagosByTipoPrestamoId() ;
            } ;

            // ASCVEH

                vm.inciarParamsAscveh = inciarParamsAscveh ;
                function inciarParamsAscveh(params)
                {
                    var costo    = vm.formData_ascveh.costo ;
                    var inicial_porcentaje = vm.formData_ascveh.inicial_porcentaje ;
                    var inicial_monto      = vm.formData_ascveh.inicial_monto ;
                    // vm.formData_ascveh.inicial_monto = parseFloat(inicial_monto).toFixed(2);

                    if (inicial_porcentaje === '' || inicial_porcentaje === undefined)
                    return;

                    if (params === 1 )
                    {
                        var calculo_monto = ( inicial_porcentaje * costo ) / 100 ;
                        vm.formData_ascveh.inicial_monto = calculo_monto;
                        // vm.formData_ascveh.inicial_monto = parseFloat(calculo_monto).toFixed(2);
                    }else  if (params === 2 )
                    {
                        var inicial_porcentaje_ = ( inicial_monto / costo ) * 100  ;
                        vm.formData_ascveh.inicial_porcentaje =  inicial_porcentaje_;
                        // vm.formData_ascveh.inicial_porcentaje =  parseFloat(inicial_porcentaje_).toFixed(2); ;
                    };

                } ;

                function updateAscVeh()
                {

                   /* var valor                = vm.formData_ascveh.costo ;
                    var inicial_porcentaje   = vm.formData_ascveh.inicial_porcentaje ;
                    var inicial_monto        = vm.formData_ascveh.inicial_monto ;
                    var seguro_tr            = vm.formData_ascveh.seguro_tr ;
                    var gps                  = vm.formData_ascveh.gps ;
                    var soat                 = vm.formData_ascveh.soat ;
                    var gas                  = vm.formData_ascveh.gas ;
                    var otros                = vm.formData_ascveh.otros ;
                    var tasa_interes         = vm.formData.interes ;
                    var num_cuotas           = vm.formData.num_cuotas ;
                    var mora                 = vm.formData.mora ;
                    var complacencia         = vm.formData.complacencia ;
                    var tipo_prestamo_id     = vm.formData.tipo_prestamo_id ;
                    var tipo_moneda_id       = vm.formData.tipo_moneda_id ;
                    var tipo_periodo_id      = vm.formData.tipo_periodo_id ;
                    var tipo_pago_id         = vm.formData.tipo_pago_id ;
                    var tipo_garantia_id     = vm.formData.tipo_garantia_id ;
                    var acuerdo_pago_id      = vm.formData.acuerdo_pago_id ;*/

                    var vehiculo_id          = vm.formData_ascveh.vehiculo_id ;
                    var pagos_parciales      = vm.formData.pagos_parciales ;
                    var propietario          = vm.formData.propietario ;
                    var observacion          = vm.formData.observacion ;
                    var cliente_id           = vm.formData.cliente_id ;
                    var aval_id              = vm.formData.aval_id ;
                    var fecha_credito        = vm.formData.fecha_credito ;
                    var fecha_desembolso     = vm.formData.fecha_desembolso ;
                    var fecha_prorrateo      = vm.formData.fecha_prorrateo ;
                    var fecha_prorrateo_esp  = vm.formData.fecha_prorrateo_esp ;

                    var check_prorrateo      = vm.formData.check_prorrateo ;
                    var check_prorrateo_esp  = vm.formData.check_prorrateo_esp ;


                    vm.msj = {message : ''} ;
                console.log(' updateAscVeh');
                //             return  vm.msj.message = 'Seleccionar Tipo Prestramo' ;
                // console.log(tipo_garantia_id + '  === updateAscVeh');

                    // VALIDACION
                        /*if (tipo_prestamo_id === undefined)
                        {
                            return  vm.msj.message = 'Seleccionar Tipo Prestramo' ;
                        };*/

                        if (cliente_id === undefined)
                        {
                            return  vm.msj.message = 'Seleccionar Cliente' ;
                        };

                        if (fecha_desembolso === undefined)
                        {
                            return  vm.msj.message = 'Ingresar Fecha Desembolso' ;
                        };

                        if (fecha_credito === undefined)
                        {
                            return  vm.msj.message = 'Ingresar Fecha Desembolso' ;
                        };

                        /*if (tipo_moneda_id === undefined)
                        {
                            return  vm.msj.message = 'Seleccionar Tipo de Moneda' ;
                        };

                        if (tipo_prestamo_id === 1)
                        {
                            if (valor === undefined)
                            {
                                return  vm.msj.message = 'Ingrese Costo del Vehiculo' ;
                            };

                            if (vehiculo_id === undefined)
                            {
                                return  vm.msj.message = 'Seleccionar Vehiculo' ;
                            };

                        } ;

                        if (tasa_interes === undefined)
                        {
                            return  vm.msj.message = 'Ingresar Tasa de Interes' ;
                        };

                        if (tipo_periodo_id === undefined)
                        {
                            return  vm.msj.message = 'Seleccionar Periodo de Cuotas' ;
                        };

                        if (num_cuotas === undefined)
                        {
                            return  vm.msj.message = 'Ingresar Número de Cuotas' ;
                        };*/

                    var fecha_credito = $filter('date')(vm.formData.fecha_credito,'yyyy-MM-dd');
                    // vm.formData.fecha_credito = fecha_credito ;

                    var fecha_desembolso = $filter('date')(vm.formData.fecha_desembolso,'yyyy-MM-dd');
                    // vm.formData.fecha_desembolso = fecha_desembolso ;

                    var fecha_prorrateo = $filter('date')(vm.formData.fecha_prorrateo,'yyyy-MM-dd');

                    var fecha_prorrateo_esp = $filter('date')(vm.formData.fecha_prorrateo_esp,'yyyy-MM-dd');

                    var params = {
                      // 'valor' : valor,
                      // 'inicial_porcentaje' : inicial_porcentaje,
                      // 'inicial_monto' : inicial_monto,
                      // 'seguro_tr' : seguro_tr,
                      // 'gps' : gps,
                      // 'soat' : soat,
                      // 'gas' : gas,
                      // 'otros' : otros,
                      // 'tasa_interes' : tasa_interes,
                      // 'num_cuotas' : num_cuotas,
                      // 'mora' : mora,
                      // 'complacencia' : complacencia,
                      // 'tipo_prestamo_id' : tipo_prestamo_id,
                      // 'tipo_moneda_id' : tipo_moneda_id,
                      // 'tipo_periodo_id' : tipo_periodo_id,
                      // 'tipo_pago_id' : tipo_pago_id,
                      // 'tipo_garantia_id' : tipo_garantia_id,
                      // 'acuerdo_pago_id' : acuerdo_pago_id,
                      'prestamo_id' :  vm.prestamo_id ,
                      'vehiculo_id' : vehiculo_id,
                      'pagos_parciales' : pagos_parciales,
                      'propietario' : propietario,
                      'observacion' : observacion,
                      'cliente_id' : cliente_id,
                      'aval_id' : aval_id,
                      'fecha_credito' : fecha_credito,
                      'fecha_desembolso' : fecha_desembolso,
                      'fecha_prorrateo' : fecha_prorrateo,
                      'fecha_prorrateo_esp' : fecha_prorrateo_esp,
                      'check_prorrateo' : check_prorrateo,
                      'check_prorrateo_esp' : check_prorrateo_esp,

                      'fecha_desembolso_old' : vm.data_list.fecha_desembolso,

                    };

                    prestamoService.update(params).then(
                        function(response)
                        {
                            console.log(response);
                            if (!response.error)
                            {
                                 vm.msj.message = 'Registro Correcto' ;
                                 getCuotasByPrestamoId() ;
                                 setTimeout(function(){
                                      vm.msj.message = 'Registro Correcto' ;
                                    }, 10000);
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );



                };


            vm.ok = function ()
            {
                console.log('ok');
                updateAscVeh();
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };

            vm.reloadListPrestamos = function()
            {
                var current = $state.current.name ;
                var res = current.split(".",2);
                var current_parent = res[0]+"."+res[1];

                var values = '' ;
                $state.go(current_parent, values);
                $rootScope.$broadcast("reloadListPrestamos", values);
            };

            vm.data_list_coutas = [] ;
            function getCuotasByPrestamoId()
            {
                var data = {
                    'prestamo_id' : vm.prestamo_id ,
                } ;

                cuotaService.getCuotasByPrestamoId(data).then(
                    function(response){
                        console.log(response);
                        if (!response.error)
                        {
                            vm.data_list_coutas = response.data ;
                            return vm.data_list_coutas ;

                        }
                    }
                );
            };




        };
})() ;
(function(){
    'use stric' ;

    angular.module('prestamos.mov.controller').controller('ModalAprobarPrestamoCrtl', ModalAprobarPrestamoCrtl) ;
    ModalAprobarPrestamoCrtl.$inject = ['$uibModalInstance', 'data_prestamo','prestamoService'] ;

        function ModalAprobarPrestamoCrtl($uibModalInstance,data_prestamo, prestamoService)
        {
            var vm =  this ;

            vm.msj = "";
            vm.fillSelected = data_prestamo ;

            vm.formData = {
                mora : '',
            }

            vm.ok = function ()
            {
                var params = {
                    'mora' : vm.formData.mora,
                    'tipo_estado_id' : 2,
                    'prestamo_id' : vm.fillSelected.id,
                };
                // return
                // console.log(params);
                prestamoService.updateEstadoPrestamoTipoEstado(params).then(
                    function(response)
                    {
                        console.log(response);

                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use stric' ;

    angular.module('prestamos.mov.controller').controller('ModalCancelarPrestamoCrtl', ModalCancelarPrestamoCrtl) ;
    ModalCancelarPrestamoCrtl.$inject = ['$uibModalInstance', 'data_prestamo','prestamoService'] ;

        function ModalCancelarPrestamoCrtl($uibModalInstance,data_prestamo, prestamoService)
        {
            var vm =  this ;

            vm.msj = "";
            vm.fillSelected = data_prestamo ;

            vm.formData = {
                glosa : '',
            }

            vm.ok = function ()
            {
                var params = {
                    'glosa' : vm.formData.glosa,
                    'tipo_estado_id' : 3,
                    'prestamo_id' : vm.fillSelected.id,
                };
                console.log(params);
                // return ;
                prestamoService.updateEstadoPrestamoTipoEstado(params).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use stric' ;

    angular.module('prestamos.mov.controller').controller('NewPrestamoMovCrtl', NewPrestamoMovCrtl) ;
    NewPrestamoMovCrtl.$inject = [
                                    '$rootScope',
                                    '$state',
                                    '$filter',
                                    'tipoPrestamoService',
                                    'tipoMonedaService',
                                    'tipoPeriodoService',
                                    'tipoGarantiaService',
                                    'tipoPagoService',
                                    'empleadoService',
                                    'clienteService',
                                    'vehiculoService',
                                    'avalService',
                                    'prestamoService',
                                    'acuerdoPagoService',
                                ] ;

        function NewPrestamoMovCrtl(
                $rootScope,
                $state,
                $filter,
                tipoPrestamoService,
                tipoMonedaService,
                tipoPeriodoService,
                tipoGarantiaService,
                tipoPagoService,
                empleadoService,
                clienteService,
                vehiculoService,
                avalService,
                prestamoService,
                acuerdoPagoService
            )
        {
            var vm =  this ;

            vm.msj = "";
            vm.formData = {
                tipo_prestamo : undefined,
                tipo_moneda : undefined,
                tipo_periodo : undefined,
                acuerdo_pago : undefined,
                tipo_pago : undefined,
                tipo_garantia : undefined,
                asesor_negocio : undefined,
                cliente : undefined,
                aval : undefined,
                interes : '',
                num_cuotas : '',
                mora : '',
                complacencia : '',
                pagos_parciales : 'NO',
                propietario : '',
                observacion :'' ,
                fecha_credito :null ,
                fecha_desembolso :null ,
                fecha_prorrateo :null ,
                fecha_prorrateo_esp :null ,
                check_prorrateo : false ,
                check_prorrateo_esp : false ,
            } ;

            vm.formData_ascveh = {
                costo : '',
                inicial_porcentaje : '',
                inicial_monto : '',
                seguro_tr : '',
                gps : '',
                soat : '',
                gas : '',
                otros : '',
                vehiculo_id : '',

            } ;

            vm.tipo_prestamos   = [] ;
            vm.tipo_monedas     = [] ;
            vm.tipo_periodos    = [] ;
            vm.acuerdo_pagos    = [] ;
            vm.tipo_pagos       = [] ;
            vm.tipo_garantias   = [] ;
            vm.asesores_negocio = [] ;
            vm.clientes = [] ;
            vm.data_vehiculos = [] ;
            vm.data_avales = [] ;

            // var view
                vm.ASCVEH = false ;

            //  DatePicker ui-b
                vm.popup = {
                    opened : false,
                } ;
                vm.dateOptions = {
                    startingDay: 1, // inicie en lunes
                    showWeeks:'false',
                    minDate: null,
                };

                vm.openCalendar = function() {
                    vm.popup.opened = true;
                };

                vm.popup2 = {
                    opened : false,
                } ;
                vm.openCalendar2 = function() {
                    vm.popup2.opened = true;
                };

                vm.popup3 = {
                    opened : false,
                } ;
                vm.openCalendar3 = function() {
                    vm.popup3.opened = true;
                };

                vm.popup4 = {
                    opened : false,
                } ;
                vm.openCalendar4 = function() {
                    vm.popup4.opened = true;
                };

            init();
            function init()
            {
                getTipoPrestamos();
                getClientesInfoBasica() ;
                // setTimeout(getEmpleadosInfoBasica(), 1000);
                setTimeout(getTipoMonedas(), 1500);
                setTimeout(getTipoPeriodos(), 2000);
                setTimeout(getTipoGarantias(), 2500);
                setTimeout(getAvalesInfoBasica(), 3000);

            };

            //  carga de datos

                function getTipoPrestamos()
                {
                    tipoPrestamoService.getTipoPrestamos().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_prestamos = response.data;
                                vm.formData.tipo_prestamo =  getFill(vm.tipo_prestamos, 1) ;
                                setTimeout(vm.onSelectTipoPrestamo(), 1000);
                                return vm.tipo_prestamos ;
                            }
                        }
                    );
                };

                function getFill(data, idSelected)
                {
                    var out = []
                    for(var i in data)
                    {
                        if(parseInt(data[i].id) === parseInt(idSelected))
                        {
                            out = data[i] ;
                            return out;
                        };
                    } ;
                    return out;
                } ;

                function getTipoMonedas()
                {
                    tipoMonedaService.getTipoMonedas().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_monedas = response.data;
                                return vm.tipo_monedas ;
                            }
                        }
                    );
                };

                function getTipoPeriodos()
                {
                    tipoPeriodoService.getTipoPeriodos().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_periodos = response.data;
                                return vm.tipo_periodos ;
                            }
                        }
                    );
                };

                function getTipoPagosByTipoPrestamoId()
                {
                    var data = {
                        'tipo_prestamo_id' : vm.formData.tipo_prestamo.id,
                    };
                    tipoPagoService.getTipoPagosByTipoPrestamoId(data).then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_pagos = response.data;
                                return vm.tipo_pagos ;
                            }
                        }
                    );
                };

               vm.getAcuerdoPagosByTipoPeriodoId =  function($item, $model)
                {
                    var data = {
                        'tipo_periodo_id' : vm.formData.tipo_periodo.id,
                    };

                    acuerdoPagoService.getAcuerdoPagosByTipoPeriodoId(data).then(
                        function(response){


                            if (!response.error)
                            {
                                vm.acuerdo_pagos = response.data;
                                return vm.acuerdo_pagos ;
                            }
                        }
                    );
                };

                function getTipoGarantias()
                {
                    tipoGarantiaService.getTipoGarantias().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.tipo_garantias = response.data;
                                return vm.tipo_garantias ;
                            }
                        }
                    );
                };

                function getClientesInfoBasica()
                {
                    var per_tipo = 0 ;
                    var data = {
                        'per_tipo' : per_tipo,
                    } ;

                    clienteService.getClientesInfoBasica(data).then(
                        function(response)
                        {
                            console.log(response);
                            if (!response.error)
                            {
                                vm.clientes = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );
                } ;

                function getVehiculosForSelect()
                {
                    vehiculoService.getVehiculosForSelect().then(
                        function(response)
                        {
                            console.log(response);
                            if (!response.error)
                            {
                                vm.data_vehiculos = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );

                } ;

                function getAvalesInfoBasica()
                {
                    avalService.getAvalesInfoBasica().then(
                        function(response)
                        {
                            console.log('avales');
                            console.log(response);
                            if (!response.error)
                            {
                                var data = response.data ;


                                vm.data_avales = [{
                                    'full_name' : 'Sin Aval' ,
                                    'dni' : null ,
                                    'id' : null ,
                                    'per_apellidos' : null ,
                                    'per_natural_id' : null ,
                                    'per_nombre' : null ,
                                    'persona_id' : null ,
                                 }].concat(data)  ;

                                // vm.data_avales = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );
                } ;

                // seleccionar un ITEM del un combo
                function selecterItemCombo(data, idSeleccionar)
                {
                    // var data = vm.tipo_garantias ;
                    var idSelected = parseInt(idSeleccionar);
                    var out = [] ;
                    for(var i in data)
                    {
                        var id =  parseInt(data[i].id);
                        if( id === idSelected)
                        {
                           out = data[i] ;
                           break ;
                        }

                    }
                    return out ;
                };

            vm.onSelectTipoPrestamo = function($item, $model)
            {
                vm.ASCVEH = false ;
                // var tipo_prestamo_id = parseInt($item.id) ;
                var tipo_prestamo_id = parseInt(vm.formData.tipo_prestamo.id) ;
                vm.activeTipoGarantia = false ;
                if (tipo_prestamo_id === 1)
                {
                    vm.ASCVEH = true ;
                    getVehiculosForSelect();
                    // seleccionar el item Vehicular
                    vm.formData.tipo_garantia = selecterItemCombo(vm.tipo_garantias, 1) ;
                    vm.activeTipoGarantia = true ;
                };
                getTipoPagosByTipoPrestamoId() ;
            } ;

            // ASCVEH

                vm.inciarParamsAscveh = inciarParamsAscveh ;
                function inciarParamsAscveh(params)
                {
                    var costo    = vm.formData_ascveh.costo ;
                    var inicial_porcentaje = vm.formData_ascveh.inicial_porcentaje ;
                    var inicial_monto      = vm.formData_ascveh.inicial_monto ;
                    // vm.formData_ascveh.inicial_monto = parseFloat(inicial_monto).toFixed(2);

                    if (inicial_porcentaje === '' || inicial_porcentaje === undefined)
                    return;

                    if (params === 1 )
                    {
                        var calculo_monto = ( inicial_porcentaje * costo ) / 100 ;
                        vm.formData_ascveh.inicial_monto = calculo_monto;
                        // vm.formData_ascveh.inicial_monto = parseFloat(calculo_monto).toFixed(2);
                    }else  if (params === 2 )
                    {
                        var inicial_porcentaje_ = ( inicial_monto / costo ) * 100  ;
                        vm.formData_ascveh.inicial_porcentaje =  inicial_porcentaje_;
                        // vm.formData_ascveh.inicial_porcentaje =  parseFloat(inicial_porcentaje_).toFixed(2); ;
                    };

                } ;

                function saveAscVeh()
                {

                    var valor                = vm.formData_ascveh.costo ;
                    var inicial_porcentaje   = vm.formData_ascveh.inicial_porcentaje ;
                    var inicial_monto        = vm.formData_ascveh.inicial_monto ;
                    var seguro_tr            = vm.formData_ascveh.seguro_tr ;
                    var gps                  = vm.formData_ascveh.gps ;
                    var soat                 = vm.formData_ascveh.soat ;
                    var gas                  = vm.formData_ascveh.gas ;
                    var otros                = vm.formData_ascveh.otros ;
                    var vehiculo_id          = vm.formData_ascveh.vehiculo_id ;
                    var tasa_interes         = vm.formData.interes ;
                    var num_cuotas           = vm.formData.num_cuotas ;
                    var mora                 = vm.formData.mora ;
                    var complacencia         = vm.formData.complacencia ;
                    var pagos_parciales      = vm.formData.pagos_parciales ;
                    var propietario          = vm.formData.propietario ;
                    var observacion          = vm.formData.observacion ;
                    var tipo_prestamo_id     = vm.formData.tipo_prestamo.id ;
                    var tipo_moneda_id       = vm.formData.tipo_moneda.id ;
                    var tipo_periodo_id      = vm.formData.tipo_periodo.id ;
                    var tipo_pago_id         = vm.formData.tipo_pago.id ;
                    var tipo_garantia_id     = vm.formData.tipo_garantia.id ;
                    var cliente_id           = vm.formData.cliente.id ;
                    var aval_id              = vm.formData.aval.id ;
                    var acuerdo_pago_id      = vm.formData.acuerdo_pago.id ;
                    var fecha_credito        = vm.formData.fecha_credito ;
                    var fecha_desembolso     = vm.formData.fecha_desembolso ;
                    var fecha_prorrateo      = vm.formData.fecha_prorrateo ;
                    var fecha_prorrateo_esp  = vm.formData.fecha_prorrateo_esp ;

                    var check_prorrateo      = vm.formData.check_prorrateo ;
                    var check_prorrateo_esp  = vm.formData.check_prorrateo_esp ;


                    vm.msj = {message : ''} ;

                    // VALIDACION
                        if (tipo_prestamo_id === undefined)
                        {
                            return  vm.msj.message = 'Seleccionar Tipo Prestramo' ;
                        };

                        if (cliente_id === undefined)
                        {
                            return  vm.msj.message = 'Seleccionar Cliente' ;
                        };

                        if (fecha_desembolso === undefined)
                        {
                            return  vm.msj.message = 'Ingresar Fecha Desembolso' ;
                        };

                        if (fecha_credito === undefined)
                        {
                            return  vm.msj.message = 'Ingresar Fecha Desembolso' ;
                        };

                        if (tipo_moneda_id === undefined)
                        {
                            return  vm.msj.message = 'Seleccionar Tipo de Moneda' ;
                        };

                        if (tipo_prestamo_id === 1)
                        {
                            if (valor === undefined)
                            {
                                return  vm.msj.message = 'Ingrese Costo del Vehiculo' ;
                            };

                            if (vehiculo_id === undefined)
                            {
                                return  vm.msj.message = 'Seleccionar Vehiculo' ;
                            };

                        } ;

                        if (tasa_interes === undefined)
                        {
                            return  vm.msj.message = 'Ingresar Tasa de Interes' ;
                        };

                        if (tipo_periodo_id === undefined)
                        {
                            return  vm.msj.message = 'Seleccionar Periodo de Cuotas' ;
                        };

                        if (num_cuotas === undefined)
                        {
                            return  vm.msj.message = 'Ingresar Número de Cuotas' ;
                        };

                    var fecha_credito = $filter('date')(vm.formData.fecha_credito,'yyyy-MM-dd');
                    // vm.formData.fecha_credito = fecha_credito ;

                    var fecha_desembolso = $filter('date')(vm.formData.fecha_desembolso,'yyyy-MM-dd');
                    // vm.formData.fecha_desembolso = fecha_desembolso ;

                    var fecha_prorrateo = $filter('date')(vm.formData.fecha_prorrateo,'yyyy-MM-dd');

                    var fecha_prorrateo_esp = $filter('date')(vm.formData.fecha_prorrateo_esp,'yyyy-MM-dd');

                    var data = {
                      'valor' : valor,
                      'inicial_porcentaje' : inicial_porcentaje,
                      'inicial_monto' : inicial_monto,
                      'seguro_tr' : seguro_tr,
                      'gps' : gps,
                      'soat' : soat,
                      'gas' : gas,
                      'otros' : otros,
                      'vehiculo_id' : vehiculo_id,
                      'tasa_interes' : tasa_interes,
                      'num_cuotas' : num_cuotas,
                      'mora' : mora,
                      'complacencia' : complacencia,
                      'pagos_parciales' : pagos_parciales,
                      'propietario' : propietario,
                      'observacion' : observacion,
                      'tipo_prestamo_id' : tipo_prestamo_id,
                      'tipo_moneda_id' : tipo_moneda_id,
                      'tipo_periodo_id' : tipo_periodo_id,
                      'tipo_pago_id' : tipo_pago_id,
                      'tipo_garantia_id' : tipo_garantia_id,
                      'cliente_id' : cliente_id,
                      'aval_id' : aval_id,
                      'acuerdo_pago_id' : acuerdo_pago_id,
                      'fecha_credito' : fecha_credito,
                      'fecha_desembolso' : fecha_desembolso,
                      'fecha_prorrateo' : fecha_prorrateo,
                      'fecha_prorrateo_esp' : fecha_prorrateo_esp,
                      'check_prorrateo' : check_prorrateo,
                      'check_prorrateo_esp' : check_prorrateo_esp,

                    };
                    console.log(data);
                    prestamoService.save(data).then(
                        function(response)
                        {
                            console.log(response);
                            if (!response.error)
                            {
                                 vm.reloadListPrestamos();
                                // vm.data_avales = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );



                };


            vm.ok = function ()
            {
                saveAscVeh();
               /* var data = {
                    'nombre'       : vm.formData.nombre,
                };

                rolService.saveRol(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );*/

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };

            vm.reloadListPrestamos = function()
            {
                var current = $state.current.name ;
                var res = current.split(".",2);
                var current_parent = res[0]+"."+res[1];

                var values = '' ;
                $state.go(current_parent, values);
                $rootScope.$broadcast("reloadListPrestamos", values);
            }
        };
})() ;
(function(){
    'use strict';

    angular.module('prestamos.mov.controller').controller('PrestamosMovCtrl',PrestamosMovCtrl);
    PrestamosMovCtrl.$inject = ['$rootScope','botoneraFactory','$filter','$state','prestamoService', '$uibModal', 'PATH', 'modalService','NgTableParams','tipoEstadoService'] ;

        function PrestamosMovCtrl($rootScope,botoneraFactory,$filter,$state,prestamoService, $uibModal, PATH, modalService,NgTableParams,tipoEstadoService)
        {
            var vm = this ;

            // function
                vm.getPrestamos        = getPrestamos ;
                vm.onClick             = onClick ;
                vm.newPrestamo         = newPrestamo ;
                vm.detallePrestamo     = detallePrestamo ;
                vm.editPrestamo        = editPrestamo ;
                vm.aprobarPrestamo = aprobarPrestamo ;
                vm.cancelarPrestamo = cancelarPrestamo ;

            // variables
                vm.botones      = [] ;
                vm.fillSelected = [] ;
                vm.data_list    = [] ;
                vm.data_list_2  = [] ;
                vm.data_list_3  = [] ;
                vm.data_list_4  = [] ;
                vm.fillSelected = [] ;
                vm.data_tipo_estados = [] ;

                vm.btn_in_table = false ;
                vm.btn_edit     = false ;
                vm.btn_delete   = false ;

            init();
            function init() {
                tablePlugin() ;
                vm.getPrestamos(null) ;
                getTipoEstados() ;
            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getPrestamos(1);
                }
                else if (name === 'new')
                {
                      vm.newPrestamo('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editPrestamo() ;
                }
                else if (name === 'delete')
                {
                    vm.deletePrestamo() ;
                }
                else if (name === 'detail')
                {
                    vm.detallePrestamo() ;
                }
                else if (name === 'aprobar')
                {
                    vm.aprobarPrestamo() ;
                }
                else if (name === 'cancelar')
                {
                    vm.cancelarPrestamo() ;
                }
                else{
                    return ;
                };

            } ;



            function getPrestamos(tipo_estado_id)
            {
                var params = {
                        'tipo_estado_id' : tipo_estado_id ,
                         'tipo_prestamo_id' : 1 ,
                    } ;

                prestamoService.getPrestamosByTipoEstadoId(params).then(
                    function(response){
                        if (!response.error)
                        {
                            // botonesInTable() ;
                            var data = response.data;
                            vm.data_list = data ;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );

                vm.fillSelected = [] ;
            };

            function getTipoEstados()
            {

                tipoEstadoService.getTipoEstados().then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data_tipo_estados = response.data;

                             vm.data_search_tipo_estados = [{
                                "id": 0,
                                "descripcion": "Todos",
                                "estado": null,
                             }].concat(vm.data_tipo_estados)  ;

                            return vm.data_tipo_estados ;
                        }
                    }
                );
            };


            // ===== ng-table ==============================================================================================


                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                };

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                };

                function del(row) {
                    modalConfirm(row);
                };

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'rol_id' : row.id,
                        'nombre' : row.nombre,
                    };

                    prestamoService.updateRol(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New
                function newPrestamo (size)
                {
                     $state.go('operaciones.prestamos.nuevo', { })

                      /*  var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/accesos/roles/nuevo.rol.tpl.html',
                            controller: 'ModalNewPrestamoCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getPrestamos(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );*/

                };

                 // edit
                function editPrestamo (size)
                {
                    var codigo = vm.fillSelected.id ;

                    if (codigo === undefined) {
                        return modalAlert()
                    }
                    else
                    {
                        $state.go($state.current.name+'.edit', { codigo: codigo })
                    }

                };

            //  detalle de prestamo
                function detallePrestamo (size)
                {
                    var codigo = vm.fillSelected.id ;

                    if (codigo === undefined) {
                        return modalAlert()
                    }
                    else
                    {
                        $state.go($state.current.name+'.detalle', { codigo: codigo })
                        // $state.go('personas.personas.editar', { codigo: codigo })
                    }
                };

            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        prestamoService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getPrestamos() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Prestamo',
                        bodyText: '¿Esta Seguro de Eliminar Prestamo: '+ row.data+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

                $rootScope.$on("reloadListPrestamos", function(event, values) {
                    // console.log('reloadListPersona');
                       vm.getPrestamos();
                  });


            // aprobar
                function aprobarPrestamo (size)
                {
                    var codigo = vm.fillSelected.id ;
                    var estado = vm.fillSelected.estado ;

                        if (codigo === undefined) {
                            return modalAlert()
                        } ;
                        if (estado > 1){
                            return modalInfo()
                        } ;

                        var path = PATH.MOVIMIENTOS ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/operaciones/prestamos/modal.aprobar.tpl.html',
                            controller: 'ModalAprobarPrestamoCrtl',
                            controllerAs: 'vm',
                            size: size,
                            backdrop : 'static',
                            resolve: {
                                data_prestamo: function () { return vm.fillSelected; },
                            }

                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                init(data) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

            // cancelar
                function cancelarPrestamo (size)
                {
                    var codigo = vm.fillSelected.id ;
                    var estado = vm.fillSelected.estado ;

                        if (codigo === undefined) {
                            return modalAlert()
                        } ;

                        if (estado > 1){
                            return modalInfo()
                        } ;


                        var path = PATH.MOVIMIENTOS ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/operaciones/prestamos/modal.cancelar.tpl.html',
                            controller: 'ModalCancelarPrestamoCrtl',
                            controllerAs: 'vm',
                            size: size,
                            backdrop : 'static',
                            resolve: {
                                data_prestamo: function () { return vm.fillSelected; },
                            }

                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                init(data) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

            //modal info
                function modalInfo()
                {
                    var modalOptions = {
                        headerText: 'Prestamo',
                        bodyText: 'Operacion no Permitida, ¡¡¡ VER DETALLE o ESTADO PRESTAMO !!! '
                    };

                    modalService.showModalAlert({}, modalOptions).then(function (result){
                    });
                }

        }
})() ;


(function(){
    'use strict';

    angular.module('roles.info.controller').controller('ModalAccesosRolCrtl', ModalAccesosRolCrtl) ;
    ModalAccesosRolCrtl.$inject = ['$filter','$uibModalInstance','data_rol', 'rolService', 'usersService'] ;

    function ModalAccesosRolCrtl($filter,$uibModalInstance,data_rol, rolService, usersService)
    {
        var vm =  this ;
        vm.expandAll = expandAll ;


        vm.fillSelected = data_rol ;
        vm.accesos_rol = [] ;

        init();
        function init(){
            getAccesosRol() ;
        }

        function getAccesosRol()
        {
            var data = {
                    'rol_id' : vm.fillSelected.id,
                };

            rolService.getAccesosRol(data).then(
                    function(response){
                        var data = response.data;

                        // vm.accesos_rol = buildTree(data,null) ;
                        vm.accesos_rol =  $filter('builTreeParentInChildrenFilter')(data,null) ;
                        return vm.accesos_rol ;
                    }
                );
        } ;

        /*function buildTree(arr, parent) {
            var out = []
            for(var i in arr) {
                if(arr[i].control_padre_id === parent) {
                    var children = buildTree(arr, arr[i].id)

                    if(children.length) {
                        arr[i].children = children
                    }

                    var is_active = false ;
                    if (arr[i].is_active === 1)
                    {
                        is_active = true ;
                    };
                    arr[i].is_active = is_active ;

                    out.push(arr[i])
                }
            }
            return out
        } ;*/

        function expandAll(root)
        {
            for (var i = 0 ; i < root.length ; i++)
            {
               expandir(root[i])
            };

        } ;

        function expandir(root, setting){
            if(!setting){
                setting = ! root.isExpanded;
            }
            root.isExpanded = setting;
            root.children.forEach(function(branch){
                expandir(branch, setting);
            });
        };

        vm.ok = function ()
        {
            var arr = vm.accesos_rol ;
            vm.controles = $filter('deleteParentsOfChildrenFilter')(arr) ;

            var data = {
                'controles' : vm.controles,
                'rol_id' : vm.fillSelected.id,
            };
            // console.log(data);
            rolService.updateAccesosRol(data).then(
                function(response)
                {
                    // console.log(response);

                    if (!response.error)
                    {
                        $uibModalInstance.close();
                    }else
                    {
                        vm.msj = response.message ;
                    }
                }
            );

        };

        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };


    }

})() ;
(function(){
    'use stric' ;

    angular.module('roles.info.controller').controller('ModalNewRolCrtl', ModalNewRolCrtl) ;
    ModalNewRolCrtl.$inject = ['$uibModalInstance', 'rolService'] ;

        function ModalNewRolCrtl($uibModalInstance, rolService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                };

                rolService.saveRol(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use strict';

    angular.module('roles.info.controller').controller('RolesInfoCtrl',RolesInfoCtrl);
    RolesInfoCtrl.$inject = ['$filter', 'botoneraFactory','rolService', '$uibModal', 'PATH', 'modalService','NgTableParams'] ;

        function RolesInfoCtrl($filter, botoneraFactory,rolService, $uibModal, PATH, modalService,NgTableParams)
        {
            var vm = this ;

            // function
                vm.getRoles   = getRoles ;
                vm.onClick    = onClick ;
                vm.newRol     = newRol ;
                vm.accesosRol = accesosRol ;

            // variables
                vm.data_list = [] ;
                vm.botones = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                vm.getRoles() ;
            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getRoles();
                }
                else if (name === 'new')
                {
                      vm.newRol('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }
                else if (name === 'delete')
                {
                    deleteRol() ;
                }
                else if (name === 'access')
                {
                    vm.accesosRol('md') ;
                }
                else{
                    return ;
                };

            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }

            }

            function getRoles()
            {
                rolService.getRoles().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable();
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            // ===== ng-table ==============================================================================================


                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 8,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'rol_id' : row.id,
                        'nombre' : row.nombre,
                    };

                    rolService.updateRol(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newRol (size)
                {
                        var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/accesos/roles/nuevo.rol.tpl.html',
                            controller: 'ModalNewRolCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getRoles(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );

                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        rolService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getRoles() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Rol',
                        bodyText: '¿Esta Seguro de Eliminar Rol: '+ row.data+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

            //  accesos a Rol
                function accesosRol()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalAccesosRol('md') ;
                    }
                };

                function modalAccesosRol (size)
                {
                        var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/accesos/roles/accesos.rol.tpl.html',
                            controller: 'ModalAccesosRolCrtl',
                            controllerAs: 'vm',
                            size: size,
                            backdrop : 'static',
                            resolve: {
                                data_rol: function () { return vm.fillSelected; },
                            }
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getRoles() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );

                };
        }
})() ;


(function(){
    'use strict';

    angular.module('usuarios.info.controller').controller('ModalAccesosUsersCrtl', ModalAccesosUsersCrtl) ;
    ModalAccesosUsersCrtl.$inject = ['$uibModalInstance','data_user', 'accesosService', 'usersService','$filter'] ;

    function ModalAccesosUsersCrtl($uibModalInstance,data_user, accesosService, usersService,$filter)
    {
        var vm =  this ;

        vm.expandAll = expandAll ;

        vm.fillSelected = data_user ;
        vm.menus_accesos = [] ;
        init();
        function init(){
            getControlAccesosUser() ;
        }

        function getControlAccesosUser()
        {
            var data = {
                    'user_id' : vm.fillSelected.id,
                };

            accesosService.getControlAccesosUser(data).then(
                    function(response){
                        var data = response.data;
                        vm.menus_accesos =  $filter('builTreeParentInChildrenFilter')(data,null) ;
                    }
                );
        } ;

        function expandAll(root)
        {
            for (var i = 0 ; i < root.length ; i++)
            {
               expandir(root[i])
            };

        } ;

        function expandir(root, setting){
            if(!setting){
                setting = ! root.isExpanded;
            }
            root.isExpanded = setting;
            root.children.forEach(function(branch){
                expandir(branch, setting);
            });
        };


        vm.ok = function ()
        {
            var arr = vm.menus_accesos ;
            vm.controles = $filter('deleteParentsOfChildrenFilter')(arr) ;


            var data = {
                'controles' : vm.controles,
                'user_id' : vm.fillSelected.id,
            };

            accesosService.updateAccesosUser(data).then(
                function(response)
                {
                    // console.log(response);
                    if (!response.error)
                    {
                        $uibModalInstance.close(vm.formData);
                    }else
                    {
                        vm.msj = response ;
                    }
                }
            );

        };

        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

})() ;
(function(){
    'use strict';

    angular.module('usuarios.info.controller').controller('ModalEditUsersCrtl', ModalEditUsersCrtl) ;
    ModalEditUsersCrtl.$inject = ['$uibModalInstance','data_user', 'accesosService', 'usersService','rolService'] ;

    function ModalEditUsersCrtl($uibModalInstance,data_user, accesosService, usersService,rolService)
    {
        var vm =  this ;

        vm.fillSelected  = data_user ;
        vm.data_roles    = [];

        vm.formData = {
            nombre : vm.fillSelected.full_name,
            email : vm.fillSelected.email,
            rol : vm.fillSelected.rol,
            user_id : vm.fillSelected.id,
        } ;

        init();
        function init(){
            getRoles() ;
        } ;

        function getRoles()
        {
            rolService.getRoles().then(
                function(response)
                {
                    if (!response.error)
                    {
                        vm.data_roles = response.data ;
                        for (var i in vm.data_roles)
                        {
                            if (vm.data_roles[i].id === vm.fillSelected.rol_id) {
                                vm.formData.rol =  vm.data_roles[i] ;
                                break ;
                            };
                        }

                    }else
                    {
                        vm.msj = response.message ;
                    }
                }
            );

        }


        vm.ok = function ()
        {
            var data = {
                'rol_id': vm.formData.rol.id ,
                'user_id': vm.formData.user_id,
            };


            accesosService.updateUserRol(data).then(
                function(response)
                {
                    if (!response.error)
                    {
                        $uibModalInstance.close(vm.formData);
                    }else
                    {
                        vm.msj = response ;
                    }
                }
            );

        };

        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }

})() ;
(function(){
	 angular.module('usuarios.info.controller').controller('ModalNewUsersCrtl', ModalNewUsersCrtl) ;
        ModalNewUsersCrtl.$inject = ['$uibModalInstance', 'usersService','personaNaturalService', 'rolService'] ;

            function ModalNewUsersCrtl($uibModalInstance ,usersService,personaNaturalService, rolService)
            {
                var vm =  this ;

                vm.msj = "";

                vm.formData = {
                    nombre : '',
                } ;
                vm.data_persons = [];
                vm.data_roles   = [];

                init();
                function init(){
                    getPerNaturalInfoBasica() ;
                    getRoles() ;
                }

                function getPerNaturalInfoBasica()
                {
                    personaNaturalService.getPerNaturalInfoBasica().then(
                        function(response)
                        {
                            // console.log(response);
                            if (!response.error)
                            {
                                vm.data_persons = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );

                }

                function getRoles()
                {
                    rolService.getRoles().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_roles = response.data ;
                            }else
                            {
                                vm.msj = response.message ;
                            }
                        }
                    );

                }


                vm.ok = function ()
                {
                    var persona_id = vm.formData.persona.persona_id ;
                    var mail       = vm.formData.persona.mail ;
                    var password   = vm.formData.password ;
                    var rol_id     = vm.formData.rol.id ;

                    if (mail === undefined)
                    {
                        return  vm.msj = 'Seleccione Persona' ;
                    };
                    if (password.length < 6)
                    {
                        return  vm.msj = 'Contraseña debe tener 6 caracteres como minimo' ;
                    };

                    if (rol_id === undefined )
                    {
                        return  vm.msj = 'Seleccione Rol ' ;
                    };

                    var data = {
                        'persona_id'    : persona_id,
                        'email'         : mail,
                        'password'      : password,
                        'rol_id'        : rol_id,
                    };

                    usersService.save(data).then(
                        function(response)
                        {

                            if (!response.error)
                            {
                                $uibModalInstance.close(vm.formData);
                            }else
                            {
                                vm.msj = response ;
                            }
                        }
                    );

                };

                vm.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
})();
(function(){
    'use strict';

    angular.module('usuarios.info.controller').controller('UsuariosInfoCtrl',UsuariosInfoCtrl);
    UsuariosInfoCtrl.$inject = ['botoneraFactory','$filter', '$uibModal', 'PATH', 'modalService','NgTableParams','usersService','accesosService'] ;

        function UsuariosInfoCtrl(botoneraFactory,$filter, $uibModal, PATH, modalService,NgTableParams,usersService,accesosService)
        {
            var vm = this ;

            // function
                vm.getUsers    = getUsers ;
                vm.onClick     = onClick ;
                vm.newUsers    = newUsers ;
                vm.deleteUser  = deleteUser ;
                vm.accesosUser = accesosUser ;
                vm.accesosUser = accesosUser ;
                vm.editUser    = editUser ;
            // variables
                vm.data_list = [] ;
                vm.fillSelected = [];


            init();
            function init() {
                tablePlugin() ;
                vm.getUsers() ;
            }

            function onClick(name, row)
            {
                // console.log(name);
                if (name === 'list')
                {
                    vm.getUsers();
                }
                else if (name === 'new')
                {
                      vm.newUsers('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editUser('md');
                }
                else if (name === 'delete')
                {
                    vm.deleteUser() ;
                }
                else if (name === 'access')
                {
                    vm.accesosUser('md') ;
                }
                else{
                    return ;
                };
            } ;

            function getUsers()
            {
                usersService.getUsers().then(
                    function(response){
                        if (!response.error)
                        {   vm.fillSelected = [];
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }

            // ===== ng-table ==============================================================================================

                // vm.cancel = cancel;
                // vm.del    = del;
                // vm.save   = save;
                // vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }

                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newUsers (size)
                {
                        var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/accesos/usuarios/new.user.tpl.html',
                            controller: 'ModalNewUsersCrtl',
                            controllerAs: 'vm',
                            size: size,
                             backdrop : 'static',
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getUsers() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

            //delete
                function deleteUser()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalConfirm(vm.fillSelected) ;
                    }
                };

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Usuario',
                        bodyText: '¿Esta Seguro de Eliminar Usuario: '+ row.full_name+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                } ;

                function confirmDelete(row)
                {
                     var data = {
                            'user_id'    : row.id,
                            'estado'    : 0,
                        };

                        usersService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getUsers() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

            //  Accesos
                function accesosUser()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalAccesosUser('md') ;
                    }
                };

                function modalAccesosUser(size)
                {
                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/accesos/usuarios/access.user.tpl.html',
                            controller: 'ModalAccesosUsersCrtl',
                            controllerAs: 'vm',
                            size: size,
                             backdrop : 'static',
                            resolve: {
                                data_user: function () { return vm.fillSelected; },
                            }
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getUsers() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

            //  edit
                function editUser()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalEditUser('md') ;
                    }
                };

                function modalEditUser(size)
                {
                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/accesos/usuarios/edit.user.tpl.html',
                            controller: 'ModalEditUsersCrtl',
                            controllerAs: 'vm',
                            size: size,
                             backdrop : 'static',
                            resolve: {
                                data_user: function () { return vm.fillSelected; },
                            }
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getUsers() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };
        }
})() ;


(function(){
	 angular.module('clientes.info.controller').controller('ModalEditAgenteComercialClienteCrtl', ModalEditAgenteComercialClienteCrtl) ;
        ModalEditAgenteComercialClienteCrtl.$inject = ['$uibModalInstance', 'data_cliente','empleadoService','clienteService'] ;

            function ModalEditAgenteComercialClienteCrtl($uibModalInstance, data_cliente,empleadoService, clienteService)
            {
                var vm =  this ;
                vm.fillSelected   = data_cliente ;
                vm.msj = "";

                vm.formData = {
                    nombre : vm.fillSelected.per_apellidos + ' ' + vm.fillSelected.per_nombre ,
                    cliente_id : vm.fillSelected.id,
                    agente_comercial_id : vm.fillSelected.agente_comercial_id,
                    agente_comercial : [],
                } ;

                vm.data_agentes       = [];

                init();
                function init(){
                    getEmpleadosInfoBasica() ;
                } ;

                function getEmpleadosInfoBasica()
                {
                    empleadoService.getEmpleadosInfoBasica().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_agentes = response.data ;
                                for (var i in vm.data_agentes)
                                {
                                    if (parseInt(vm.data_agentes[i].id) === parseInt(vm.fillSelected.empleado_id)) {
                                        vm.formData.agente_comercial =  vm.data_agentes[i] ;
                                        break ;
                                    };
                                }
                            }else
                            {
                                vm.msj = response.message ;
                            }
                        }
                    );
                }


                vm.ok = function ()
                {
                    var empleado_id_new         = vm.formData.agente_comercial.id ;
                    var empleado_id_old         = vm.fillSelected.empleado_id ;
                    var cliente_id              = vm.formData.cliente_id ;
                    var agente_comercial_id_now = vm.formData.agente_comercial_id ;

                    vm.msj = {message : ''} ;

                    if (empleado_id_new === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Agente Comercial' ;
                    };
                    if (parseInt(empleado_id_old) === parseInt(empleado_id_new))
                    {
                        return  vm.msj.message = 'El Agente Comercial no a cambiado' ;
                    };

                    var data = {
                        'empleado_id_new'       : empleado_id_new,
                        'agente_comercial_id'   : agente_comercial_id_now,
                        'cliente_id'            : cliente_id,
                    };

                    console.log(data);

                    clienteService.updateNewAgenteComercial(data).then(
                        function(response)
                        {
                            console.log(response);
                            if (!response.error)
                            {
                                $uibModalInstance.close(vm.formData);
                            }else
                            {
                                vm.msj = response ;
                            }
                        }
                    );

                };

                vm.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
})();
(function(){
	 angular.module('clientes.info.controller').controller('ModalEditClienteCrtl', ModalEditClienteCrtl) ;
        ModalEditClienteCrtl.$inject = ['$uibModalInstance', 'data_cliente','empleadoService','areaService','cargoService'] ;

            function ModalEditClienteCrtl($uibModalInstance, data_cliente,empleadoService, areaService,cargoService)
            {
                var vm =  this ;
                vm.fillSelected   = data_cliente ;

                vm.msj = "";

                vm.formData = {
                    nombre : vm.fillSelected.per_apellidos + ' ' + vm.fillSelected.per_nombre ,
                    cliente_id : vm.fillSelected.cliente_id,
                    agente_comercial_id : vm.fillSelected.agente_comercial_id,
                    agente_comercial : [],
                } ;

                vm.data_agentes       = [];

                init();
                function init(){
                    getEmpleadosInfoBasica() ;
                } ;

                function getEmpleadosInfoBasica()
                {
                    empleadoService.getEmpleadosInfoBasica().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_agentes = response.data ;
                            }else
                            {
                                vm.msj = response.message ;
                            }
                        }
                    );
                }


                vm.ok = function ()
                {
                    var cargo_id          = vm.formData.cargo.id ;
                    var cargo_empleado_id = vm.formData.cargo_empleado_id ;
                    var empleado_id       = vm.formData.empleado_id ;

                    vm.msj = {message : ''} ;

                    if (cargo_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Cargo' ;
                    };
                    if (empleado_id === undefined)
                    {
                        return  vm.msj.message = 'Error Verificar Empleado' ;
                    };

                    var data = {
                        'cargo_id'          : cargo_id,
                        'cargo_empleado_id' : cargo_empleado_id,
                        'empleado_id'       : empleado_id,
                    };

                    empleadoService.update(data).then(
                        function(response)
                        {
                            // console.log(response);
                            if (!response.error)
                            {
                                $uibModalInstance.close(vm.formData);
                            }else
                            {
                                vm.msj = response ;
                            }
                        }
                    );

                };

                vm.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
})();
(function(){
	 angular.module('clientes.info.controller').controller('ModalNewClienteCrtl', ModalNewClienteCrtl) ;
        ModalNewClienteCrtl.$inject = ['$uibModalInstance', 'empleadoService','personaService','clienteService'] ;

            function ModalNewClienteCrtl($uibModalInstance ,empleadoService,personaService,clienteService)
            {
                var vm =  this ;
                // vm.onSelectedArea = onSelectedArea ;

                vm.msj = "";

                vm.formData = {
                    nombre : '',
                    persona : [],
                    agente_comercial : [],
                } ;

                vm.data_persons = [];
                vm.data_agentes = [];

                vm.data_per_tipo = [{ per_tipo: "Todos", id: 0 },
                                        { per_tipo: "Personas", id: 1 },
                                        { per_tipo: "Empresas", id: 2 }
                                    ];
                vm.searchPerTipo = vm.data_per_tipo[0];


                init();
                function init(){
                    getPersonasInfoBasica() ;
                    getEmpleadosInfoBasica();
                }

                function getPersonasInfoBasica()
                {
                    var per_tipo = vm.searchPerTipo.id ;
                    var data = {
                        'per_tipo' : per_tipo,
                    } ;

                    personaService.getPersonasInfoBasica(data).then(
                        function(response)
                        {
                            console.log(response);
                            if (!response.error)
                            {
                                vm.data_persons = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );

                }

                function getEmpleadosInfoBasica()
                {
                    empleadoService.getEmpleadosInfoBasica().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_agentes = response.data ;
                            }else
                            {
                                vm.msj = response.message ;
                            }
                        }
                    );
                }


                vm.searchPersonas = searchPersonas ;
                function searchPersonas(fill)
                {
                    vm.formData.persona = [] ;
                    getPersonasInfoBasica() ;
                } ;

                vm.ok = function ()
                {
                    var persona_id = vm.formData.persona.persona_id ;
                    var empleado_id   = vm.formData.agente_comercial.id ;

                    vm.msj = {message : ''} ;

                    if (persona_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Cliente' ;
                    };
                    if (empleado_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Agente Comercial' ;
                    };

                    var data = {
                        'persona_id'    : persona_id,
                        'empleado_id'   : empleado_id,
                    };

                    console.log(data);

                    clienteService.save(data).then(
                        function(response)
                        {
                            console.log(response);
                            if (!response.error)
                            {
                                $uibModalInstance.close(vm.formData);
                            }else
                            {
                                vm.msj = response ;
                            }
                        }
                    );

                };

                vm.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
})();
(function(){
    'use strict';

    angular.module('clientes.info.controller').controller('ClientesInfoCtrl',ClientesInfoCtrl);
    ClientesInfoCtrl.$inject = ['$filter', '$uibModal', 'PATH', 'modalService','NgTableParams','clienteService'] ;

        function ClientesInfoCtrl($filter, $uibModal, PATH, modalService,NgTableParams,clienteService)
        {
            var vm = this ;

            // function
                vm.getClientes         = getClientes ;
                vm.onClick             = onClick ;
                vm.newCliente          = newCliente ;
                vm.deleteCliente       = deleteCliente ;
                vm.editCliente         = editCliente ;
                vm.editAgenteComercial = editAgenteComercial ;

                vm.searchPersonas      = searchPersonas ;

            // variables
                vm.data_list     = [];
                vm.fillSelected  = [];

                vm.data_per_tipo = [{ per_tipo: "Todos", id: 0 },
                                        { per_tipo: "Personas", id: 1 },
                                        { per_tipo: "Empresas", id: 2 }
                                    ];
                vm.searchPerTipo = vm.data_per_tipo[0];

            init();
            function init() {
                tablePlugin() ;
                vm.getClientes() ;
            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getClientes();
                }
                else if (name === 'new')
                {
                      vm.newCliente('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editCliente('md');
                }
                else if (name === 'delete')
                {
                    vm.deleteCliente() ;
                }
                else if (name === 'update-agente')
                {
                    vm.editAgenteComercial() ;
                }
                else{
                    return ;
                };
            } ;

            function getClientes()
            {
                var per_tipo = vm.searchPerTipo.id ;
                var data = {
                    'per_tipo' : per_tipo,
                } ;

                clienteService.getClientes(data).then(
                    function(response){
                        if (!response.error)
                        {   vm.fillSelected = [];
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            } ;
            function searchPersonas(per_tipo)
            {
                getClientes() ;
            } ;

            // ===== ng-table ==============================================================================================

                // vm.cancel = cancel;
                // vm.del    = del;
                // vm.save   = save;
                // vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New
                function newCliente (size)
                {
                        var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/personas/clientes/new.cliente.tpl.html',
                            controller: 'ModalNewClienteCrtl',
                            controllerAs: 'vm',
                            size: size,
                            backdrop : 'static',
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getClientes() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

            //delete
                function deleteCliente()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalConfirm(vm.fillSelected) ;
                    }
                };

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Cliente',
                        bodyText: '¿Esta Seguro de Eliminar Cliente: '+ row.per_nombre + ' ' + row.per_apellidos+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                } ;

                function confirmDelete(row)
                {
                     var data = {
                            'cliente_id'   : row.id,
                            // 'persona_id'    : row.persona_id,
                            'estado'    : 0,
                        };

                        clienteService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getClientes() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

            //  edit
                function editCliente()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalEditCliente('md') ;
                    }
                };

                function modalEditCliente(size)
                {
                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/personas/clientes/edit.cliente.tpl.html',
                            controller: 'ModalEditClienteCrtl',
                            controllerAs: 'vm',
                            size: size,
                             backdrop : 'static',
                            resolve: {
                                data_cliente: function () { return vm.fillSelected; },
                            }
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getClientes() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

            // editar agente comercial
                function editAgenteComercial()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalEditAgenteComercial('md') ;
                    }
                };


                function modalEditAgenteComercial(size)
                {
                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/personas/clientes/edit.agente.comercial.tpl.html',
                            controller: 'ModalEditAgenteComercialClienteCrtl',
                            controllerAs: 'vm',
                            size: size,
                             backdrop : 'static',
                            resolve: {
                                data_cliente: function () { return vm.fillSelected; },
                            }
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getClientes() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

        }
})() ;


(function(){
	 angular.module('empleados.info.controller').controller('ModalAsignaNewCargoEmpleadoCrtl', ModalAsignaNewCargoEmpleadoCrtl) ;
        ModalAsignaNewCargoEmpleadoCrtl.$inject = ['$uibModalInstance', 'data_empleado','data_cargos','empleadoService','areaService','cargoService'] ;

            function ModalAsignaNewCargoEmpleadoCrtl($uibModalInstance, data_empleado,data_cargos,empleadoService, areaService,cargoService)
            {
                var vm =  this ;
                vm.onSelectedCargosByArea = onSelectedCargosByArea ;
                vm.fillSelected   = data_empleado ;
                vm.data_cargos    = data_cargos;

                vm.msj = "";

                vm.formData = {
                    nombre : vm.fillSelected.per_apellidos + ' ' + vm.fillSelected.per_nombre ,
                    empleado_id : vm.fillSelected.id,
                    cargo_empleado_id : vm.fillSelected.cargo_empleado_id ,
                    area : [],
                    cargo : [],
                } ;

                vm.data_persons     = [];
                vm.data_areas       = [];
                // vm.data_cargos      = [];
                vm.data_cargos_area = [];

                init();
                function init(){
                    // getCargos() ;
                    getAreas() ;
                }


                function getAreas()
                {
                    areaService.getAreas().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_areas = response.data ;
                                for (var i in vm.data_areas)
                                {
                                    if (vm.data_areas[i].id === vm.fillSelected.area_id) {
                                        vm.formData.area =  vm.data_areas[i] ;
                                        onSelectedCargosByArea(vm.formData.area, vm.fillSelected.cargo_id) ;
                                        break ;
                                    };
                                }
                            }else
                            {
                                vm.msj = response.message ;
                            }
                        }
                    );
                }

                function onSelectedCargosByArea(itemArea, select_cargo_id)
                {
                    var select_area_id  = parseInt(itemArea.id) ;
                    var select_cargo_id = parseInt(select_cargo_id) ;

                    var data = vm.data_cargos ;
                    var cargo_select    = [] ;

                    vm.data_cargos_area = [] ;
                    vm.formData.cargo   = [];

                    var out = [] ;
                    for(var i in data)
                    {
                        var area_id =  parseInt(data[i].area_id);
                        if( area_id === select_area_id)
                        {
                            out.push(data[i])
                        };

                        var cargo_id = parseInt(data[i].id) ;
                        if ( select_area_id === area_id && select_cargo_id === cargo_id )
                        {
                           cargo_select = data[i] ;
                        };
                    } ;

                    vm.data_cargos_area = out ;
                    vm.formData.cargo   = cargo_select ;

                    return vm.data_cargos_area  ;
                }


                vm.ok = function ()
                {
                    var cargo_id          = vm.formData.cargo.id ;
                    var cargo_empleado_id = vm.formData.cargo_empleado_id ;
                    var empleado_id       = vm.formData.empleado_id ;

                    vm.msj = {message : ''} ;

                    if (cargo_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Cargo' ;
                    };
                    if (empleado_id === undefined)
                    {
                        return  vm.msj.message = 'Error Verificar Empleado' ;
                    };

                    var data = {
                        'cargo_id'          : cargo_id,
                        'cargo_empleado_id' : cargo_empleado_id,
                        'empleado_id'       : empleado_id,
                    };

                    empleadoService.asignarNewCargo(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                $uibModalInstance.close(vm.formData);
                            }else
                            {
                                vm.msj.message = response.data ;
                            }
                        }
                    );

                };

                vm.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
})();
(function(){
	 angular.module('empleados.info.controller').controller('ModalEditEmpleadoCrtl', ModalEditEmpleadoCrtl) ;
        ModalEditEmpleadoCrtl.$inject = ['$uibModalInstance', 'data_empleado','data_cargos','empleadoService','areaService','cargoService'] ;

            function ModalEditEmpleadoCrtl($uibModalInstance, data_empleado,data_cargos,empleadoService, areaService,cargoService)
            {
                var vm =  this ;
                vm.onSelectedCargosByArea = onSelectedCargosByArea ;
                vm.fillSelected   = data_empleado ;
                vm.data_cargos    = data_cargos;

                vm.msj = "";

                vm.formData = {
                    nombre : vm.fillSelected.per_apellidos + ' ' + vm.fillSelected.per_nombre ,
                    empleado_id : vm.fillSelected.id,
                    cargo_empleado_id : vm.fillSelected.cargo_empleado_id ,
                    area : [],
                    cargo : [],
                } ;

                vm.data_persons     = [];
                vm.data_areas       = [];
                // vm.data_cargos      = [];
                vm.data_cargos_area = [];

                init();
                function init(){
                    // getCargos() ;
                    getAreas() ;
                }


                function getAreas()
                {
                    areaService.getAreas().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_areas = response.data ;
                                for (var i in vm.data_areas)
                                {
                                    if (parseInt(vm.data_areas[i].id) === parseInt(vm.fillSelected.area_id)) {
                                        vm.formData.area =  vm.data_areas[i] ;
                                        onSelectedCargosByArea(vm.formData.area, vm.fillSelected.cargo_id) ;
                                        break ;
                                    };
                                }
                            }else
                            {
                                vm.msj = response.message ;
                            }
                        }
                    );
                }

                /*function getCargos()
                {
                    cargoService.getCargosAll().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_cargos = response.data ;
                            }else
                            {
                                vm.msj = response.message ;
                            }
                        }
                    );
                } ;*/



                function onSelectedCargosByArea(itemArea, select_cargo_id)
                {
                    console.log('select_cargo_id: '+select_cargo_id);
                    var select_area_id  = parseInt(itemArea.id) ;
                    var select_cargo_id = parseInt(select_cargo_id) ;

                    var data = vm.data_cargos ;
                    var cargo_select    = [] ;

                    vm.data_cargos_area = [] ;
                    vm.formData.cargo   = [];

                    var out = [] ;
                    for(var i in data)
                    {
                        var area_id =  parseInt(data[i].area_id);
                        if( area_id === select_area_id)
                        {
                            out.push(data[i])
                        };

                        var cargo_id = parseInt(data[i].id) ;
                        if ( select_area_id === area_id && select_cargo_id === cargo_id )
                        {
                           cargo_select = data[i] ;
                        };
                    } ;

                    vm.data_cargos_area = out ;
                    vm.formData.cargo   = cargo_select ;

                    return vm.data_cargos_area  ;
                }


                vm.ok = function ()
                {
                    var cargo_id          = vm.formData.cargo.id ;
                    var cargo_empleado_id = vm.formData.cargo_empleado_id ;
                    var empleado_id       = vm.formData.empleado_id ;

                    vm.msj = {message : ''} ;

                    if (cargo_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Cargo' ;
                    };
                    if (empleado_id === undefined)
                    {
                        return  vm.msj.message = 'Error Verificar Empleado' ;
                    };

                    var data = {
                        'cargo_id'          : cargo_id,
                        'cargo_empleado_id' : cargo_empleado_id,
                        'empleado_id'       : empleado_id,
                    };

                    empleadoService.update(data).then(
                        function(response)
                        {
                            // console.log(response);
                            if (!response.error)
                            {
                                $uibModalInstance.close(vm.formData);
                            }else
                            {
                                vm.msj = response ;
                            }
                        }
                    );

                };

                vm.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
})();
(function(){
	 angular.module('empleados.info.controller').controller('ModalNewEmpleadoCrtl', ModalNewEmpleadoCrtl) ;
        ModalNewEmpleadoCrtl.$inject = ['$uibModalInstance', 'empleadoService','personaNaturalService', 'areaService','cargoService'] ;

            function ModalNewEmpleadoCrtl($uibModalInstance ,empleadoService,personaNaturalService, areaService,cargoService)
            {
                var vm =  this ;
                vm.onSelectedArea = onSelectedArea ;

                vm.msj = "";

                vm.formData = {
                    nombre : '',
                } ;

                vm.data_persons = [];
                vm.data_areas   = [];
                vm.data_cargos   = [];
                vm.data_cargos_area   = [];

                init();
                function init(){
                    getPerNaturalInfoBasica() ;
                    getAreas() ;
                    getCargos() ;
                }

                function getPerNaturalInfoBasica()
                {
                    personaNaturalService.getPerNaturalInfoBasica().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_persons = response.data ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );

                }

                function getAreas()
                {
                    areaService.getAreas().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_areas = response.data ;
                            }else
                            {
                                vm.msj = response.message ;
                            }
                        }
                    );
                }

                function getCargos()
                {
                    cargoService.getCargosAll().then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                vm.data_cargos = response.data ;
                            }else
                            {
                                vm.msj = response.message ;
                            }
                        }
                    );
                }



                function onSelectedArea(itemArea)
                {
                    var select_area_id = parseInt(itemArea.id) ;
                    var data = vm.data_cargos ;

                    vm.data_cargos_area = [] ;

                    var out = []
                    for(var i in data)
                    {
                        var id =  parseInt(data[i].area_id);
                        if( id === select_area_id)
                        {
                            out.push(data[i])
                        }

                    }
                    vm.data_cargos_area = out ;

                    return vm.data_cargos_area  ;
                }


                vm.ok = function ()
                {
                    var cargo_id   = vm.formData.cargo.id ;
                    var persona_id = vm.formData.persona.persona_id ;


                    vm.msj = {message : ''} ;
                    if (cargo_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Cargo' ;
                    };
                    if (persona_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Persona' ;
                    };

                    var data = {
                        'persona_id'    : persona_id,
                        'cargo_id'         : cargo_id,
                    };
                    // console.log(data);

                    empleadoService.save(data).then(
                        function(response)
                        {
                            // console.log(response);
                            if (!response.error)
                            {
                                $uibModalInstance.close(vm.formData);
                            }else
                            {
                                vm.msj = response ;
                            }
                        }
                    );

                };

                vm.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
})();
(function(){
    'use strict';

    angular.module('empleados.info.controller').controller('EmpleadosInfoCtrl',EmpleadosInfoCtrl);
    EmpleadosInfoCtrl.$inject = ['botoneraFactory','$filter', '$uibModal', 'PATH', 'modalService','NgTableParams','empleadoService','accesosService','cargoService'] ;

        function EmpleadosInfoCtrl(botoneraFactory,$filter, $uibModal, PATH, modalService,NgTableParams,empleadoService,accesosService,cargoService)
        {
            var vm = this ;

            // function
                vm.getEmpleados   = getEmpleados ;
                vm.onClick        = onClick ;
                vm.newEmpleado    = newEmpleado ;
                vm.deleteEmpleado = deleteEmpleado ;
                vm.editEmpleado   = editEmpleado ;
                vm.asiginarNewCargoEmpleado   = asiginarNewCargoEmpleado ;

            // variables
                vm.data_list = [] ;
                vm.fillSelected = [];

            vm.data_cargos = [] ;

            init();
            function init() {
                tablePlugin() ;
                vm.getEmpleados() ;
                getCargos() ;
            }

            function onClick(name, row)
            {
                // console.log(name);
                if (name === 'list')
                {
                    vm.getEmpleados();
                }
                else if (name === 'new')
                {
                      vm.newEmpleado('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editEmpleado('md');
                }
                else if (name === 'delete')
                {
                    vm.deleteEmpleado() ;
                }
                else if (name === 'asignar')
                {
                    vm.asiginarNewCargoEmpleado('md') ;
                }

                else{
                    return ;
                };
            } ;

            function getEmpleados()
            {
                empleadoService.getEmpleados().then(
                    function(response){
                        // console.log(response);
                        if (!response.error)
                        {   vm.fillSelected = [];
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            } ;

            // ===== ng-table ==============================================================================================

                // vm.cancel = cancel;
                // vm.del    = del;
                // vm.save   = save;
                // vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New
                function newEmpleado (size)
                {
                        var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/personas/empleados/new.empleado.tpl.html',
                            controller: 'ModalNewEmpleadoCrtl',
                            controllerAs: 'vm',
                            size: size,
                             backdrop : 'static',
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getEmpleados() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

            //delete
                function deleteEmpleado()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalConfirm(vm.fillSelected) ;
                    }
                };

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Usuario',
                        bodyText: '¿Esta Seguro de Eliminar Empleado: '+ row.per_nombre + ' ' + row.per_apellidos+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                } ;

                function confirmDelete(row)
                {
                     var data = {
                            'empleado_id'   : row.id,
                            'persona_id'    : row.persona_id,
                            'estado'    : 0,
                        };

                        empleadoService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getEmpleados() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

            //  edit
                function editEmpleado()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalEditEmpleado('md') ;
                    }
                };

                function modalEditEmpleado(size)
                {
                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/personas/empleados/edit.empleado.tpl.html',
                            controller: 'ModalEditEmpleadoCrtl',
                            controllerAs: 'vm',
                            size: size,
                             backdrop : 'static',
                            resolve: {
                                data_empleado: function () { return vm.fillSelected; },
                                data_cargos: function () { return vm.data_cargos; },

                            }
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getEmpleados() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

             //  edit
                function asiginarNewCargoEmpleado()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalAsiginarNewCargoEmpleado('md') ;
                    }
                };

                function modalAsiginarNewCargoEmpleado(size)
                {
                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/personas/empleados/asignar.new.cargo.empleado.tpl.html',
                            controller: 'ModalAsignaNewCargoEmpleadoCrtl',
                            controllerAs: 'vm',
                            size: size,
                             backdrop : 'static',
                            resolve: {
                                data_empleado: function () { return vm.fillSelected; },
                                data_cargos: function () { return vm.data_cargos; },

                            }
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getEmpleados() ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            function getCargos()
            {
                cargoService.getCargosAll().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_cargos = response.data ;
                        }else
                        {
                            vm.msj = response.message ;
                        }
                    }
                );
            } ;

        }
})() ;


  angular.module('personas.juridicas.info.controller').controller('EditPerJuridicaCrtl', EditPerJuridicaCrtl) ;
        EditPerJuridicaCrtl.$inject = ['$rootScope',
                                     '$state',
                                     '$q',
                                     '$filter',
                                     '$stateParams',
                                     'modalService',
                                     'perJuridicaService',
                                     'perTelefonoService',
                                     'perMailService',
                                     'perDocumentoService',
                                     'perWebService'
                                    ] ;

            function EditPerJuridicaCrtl($rootScope,
                                        $state,
                                        $q,
                                        $filter,
                                        $stateParams,
                                        modalService,
                                        perJuridicaService,
                                        perTelefonoService,
                                        perMailService,
                                        perDocumentoService,
                                        perWebService
                                        )
            {
                var vm =  this ;

                vm.reloadListEmpresas = reloadListEmpresas ;

                vm.persona_id = $stateParams.codigo;

                // telefonos
                    vm.saveNewPhones = saveNewPhones ;
                    vm.updatePhone   = updatePhone ;
                    vm.deletePhone   = deletePhone;
                    vm.reOrderItemsTelefonos = reOrderItemsTelefonos ;

                    vm.flagNewPhones = false ;

                // mails
                    vm.saveNewMails       = saveNewMails ;
                    vm.updateMail         = updateMail ;
                    vm.deleteMail         = deleteMail;
                    vm.reOrderItemsEmails = reOrderItemsEmails ;

                    vm.flagNewMails = false ;

                 // webs
                    vm.saveNewWebs       = saveNewWebs ;
                    vm.updateWeb         = updateWeb ;
                    vm.deleteWeb         = deleteWeb;
                    vm.reOrderItemsWebs = reOrderItemsWebs ;

                    vm.flagNewWebs = false ;

                // dni
                    vm.updateRuc = updateRuc ;

                    vm.updatePerJuridicaInfo = updatePerJuridicaInfo ;

                vm.msj = "";

                vm.formData = {
                    'ruc'   : '',
                    'razon_social' : '',
                    'nombre_comercial'    : '',
                    'telefonos'  : [],
                    'mails'  : [],
                    'webs'  : [],
                    'sexo'  : '',
                    'newtelefonos': [],
                    'newmails': [],
                    'newwebs': [],
                    'fecha_nac': '',
                }

                 //  DatePicker ui-b
                    vm.today =  new Date() ;
                    vm.popup = {
                        opened : false,
                        maxDate : vm.today,
                    } ;

                    vm.openFechaNac = function() {
                        vm.popup.opened = true;
                    };

                //  sexo data
                    vm.sexoData = [
                            {value: 1, text: 'Masculino'},
                            {value: 2, text: 'Femenino'}
                          ];

                    vm.showStatus = function() {
                        var selected = $filter('filter')(vm.sexoData, {value: vm.formData.sexo});
                        return (vm.formData.sexo && selected.length) ? selected[0].text : ' No Seleccionado';
                    };


                // informacion de persona
                    getPerJuridicaInfoAll() ;
                    function getPerJuridicaInfoAll()
                    {
                        var data = {
                            'persona_id' : vm.persona_id,
                        } ;

                        perJuridicaService.getPerJuridicaInfoAll(data).then(
                            function(response){
                                console.log(response);
                                if (!response.error)
                                {
                                    vm.formData.ruc              = response.data[0].per_juridica.ruc;
                                    vm.formData.razon_social     = response.data[0].per_juridica.razon_social;
                                    vm.formData.nombre_comercial = response.data[0].per_juridica.nombre_comercial;
                                    vm.formData.telefonos        = response.data[0].per_telefono;
                                    vm.formData.mails            = response.data[0].per_mail;
                                    vm.formData.fecha_nac        = (!response.data[0].per_fecha_nac)? vm.today :new Date( response.data[0].per_fecha_nac +' 00:00:00');
                                    vm.formData.webs = response.data[0].per_web ;
                                    // console.log(vm.formData.fecha_nac);
                                    return response.data ;
                                }
                            }
                        );
                    }

                // ruc
                    function updateRuc(value)
                    {
                        var deferred = $q.defer();

                        var ruc = value;
                        var persona_id = vm.persona_id ;

                        var data = {
                                'persona_id'   : persona_id,
                                'ruc'          : ruc,
                            };

                        perJuridicaService.updateRuc(data).then(
                                function(response){

                                    if (!response.error)
                                    {
                                        vm.formData.ruc = response.data.ruc;
                                        deferred.resolve();
                                    }else
                                    {
                                        deferred.resolve(response.data.ruc[0]);
                                    }
                                }, function (response) {
                                    // the following line rejects the promise
                                    deferred.reject(response);
                                    return deferred.promise;
                                }
                            );
                        return deferred.promise;
                    } ;


                // update nombres y sexo
                    function updatePerJuridicaInfo()
                    {
                        var persona_id = vm.persona_id ;

                        var fecha_nac = $filter('date')(vm.formData.fecha_nac,'yyyy-MM-dd');

                         var data = {
                                'razon_social'  : vm.formData.razon_social,
                                'nombre_comercial'    : vm.formData.nombre_comercial,
                                'persona_id' : persona_id,
                                'fecha_nac'  : fecha_nac,
                            };


                            perJuridicaService.updatePerJuridicaInfo(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.razon_social = response.data.per_nombre;
                                        vm.formData.nombre_comercial   = response.data.per_apellidos;
                                        vm.formData.fecha_nac = (!response.data.per_fecha_nac)? vm.today :new Date( response.data.per_fecha_nac+' 00:00:00');

                                    }
                                }
                            );

                    };


                //  telefonos
                    function saveNewPhones()
                    {
                        var telefonos = vm.formData.newtelefonos ;
                        var persona_id = vm.persona_id ;

                         var data = {
                                'telefonos'    : telefonos,
                                'persona_id'   : persona_id,
                            };
                            // console.log(data) ;
                            perTelefonoService.save(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.newtelefonos = [] ;
                                        vm.flagNewPhones = false ;
                                        vm.formData.telefonos = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );

                        // console.log(vm.flagNewPhones) ;
                    }

                    function updatePhone(value, id)
                    {
                        var telefono = value;

                        if( value.length != 9  )
                        {
                          return 'Teléfono Invalido(Son Nueve Numero)';
                        }

                        var data = {
                                'telefono_id' : id,
                                'telefono'    : telefono,
                            };
                            // console.log(data) ;

                            perTelefonoService.update(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.telefonos = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );
                    }

                    function deletePhone(Telefono, id)
                    {
                        var modalOptions = {
                            closeButtonText: 'Cancelar',
                            actionButtonText: 'Eliminar',
                            headerText: 'Eliminar Teléfono',
                            bodyText: '¿Esta Seguro de Eliminar Teléfono: '+ Telefono+' ?'
                        };

                       modalService.showModalConfirm({}, modalOptions).then(function (result) {
                            confirmDeletePhone(id);
                        });
                    }

                    function confirmDeletePhone(id)
                    {
                         var data = {
                                'telefono_id'    : id
                            };

                            perTelefonoService.deleteFill(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.telefonos = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );
                    }

                    function reOrderItemsTelefonos()
                    {
                         var data = {
                                'telefonos' : vm.formData.telefonos,
                                'persona_id' : vm.persona_id ,
                            };

                            perTelefonoService.reOrderItems(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.telefonos = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );
                    }

                    // sortable telefonos
                    vm.barConfigPhones = {
                        group: 'telefonos',
                        animation: 150,
                        // onSort: function (** ngSortEvent * evt){
                        onSort: function (evt){
                            vm.reOrderItemsTelefonos() ;
                            // console.log(vm.formData.telefonos);
                        }
                    };



                //  Mail
                    function saveNewMails()
                    {
                        var mails = vm.formData.new_mails ;
                        var persona_id = vm.persona_id ;

                         var data = {
                                'persona_id'   : persona_id,
                                'mails'        : mails,
                            };

                            perMailService.save(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.new_mails = [] ;
                                        vm.flagNewMails = false ;
                                        vm.formData.mails = response.data;
                                        return vm.formData.mails;
                                    }
                                }
                            );
                    }

                    function updateMail(value, id)
                    {
                        var mail = value;

                        var data = {
                                'mail_id' : id,
                                'mail'    : mail,
                            };

                            perMailService.update(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.mails = response.data;
                                        return vm.formData.mails;
                                    }
                                }
                            );
                    }

                    function deleteMail(value, id)
                    {
                        var modalOptions = {
                            closeButtonText: 'Cancelar',
                            actionButtonText: 'Eliminar',
                            headerText: 'Eliminar Mail',
                            bodyText: '¿Esta Seguro de Eliminar Mail: '+ value+' ?'
                        };

                       modalService.showModalConfirm({}, modalOptions).then(function (result) {
                            confirmDeleteMail(id);
                        });
                    }

                    function confirmDeleteMail(id)
                    {
                         var data = {
                                'mail_id'    : id
                            };

                            perMailService.deleteFill(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.mails = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );
                    }

                    function reOrderItemsEmails()
                    {
                         var data = {
                                'mails' : vm.formData.mails,
                                'persona_id' : vm.persona_id ,
                            };

                            perMailService.reOrderItems(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.mails = response.data;
                                        return vm.formData.mails;
                                    }
                                }
                            );
                    }

                    // sortable Emails
                    vm.barConfigEmails = {
                        group: 'Mails',
                        animation: 150,
                        // onSort: function (** ngSortEvent *evt){
                        onSort: function (evt){
                            vm.reOrderItemsEmails() ;
                            // console.log(vm.formData.mails);
                        }
                    };

                    function reloadListEmpresas()
                    {
                        var values = '' ;
                        $state.go('personas.empresas', values);
                        $rootScope.$broadcast("reloadListEmpresas", values);
                    }

                //  webs
                    vm.formDataWeb = {
                        tipo_web: [] ,
                        url: '' ,
                    } ;

                    vm.data_tipo_webs = [] ;
                    getTipoWebs();
                    function getTipoWebs()
                    {
                        var persona_id = vm.persona_id ;

                            perWebService.getTipoWebs().then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.data_tipo_webs = response.data;
                                        return vm.data_tipo_webs;
                                    }
                                }
                            );

                        // console.log(vm.flagNewWebs) ;
                    }
                    function saveNewWebs()
                    {
                        var url         = vm.formDataWeb.url ;
                        var tipo_web_id = vm.formDataWeb.tipo_web.id ;
                        var persona_id  = vm.persona_id ;

                        vm.msj = {message : ''} ;
                        if (url === undefined)
                        {
                            return  vm.msj.message = 'Ingrese pagina web' ;
                        };
                        if (tipo_web_id === undefined)
                        {
                            return  vm.msj.message = 'Seleccione tipo de web' ;
                        };



                         var data = {
                                'url'           : url,
                                'tipo_web_id'   : tipo_web_id,
                                'persona_id'    : persona_id,
                            };
                            // console.log(data) ;
                            perWebService.save(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formDataWeb.url = '';
                                        vm.formDataWeb.tipo_web = [] ;
                                        vm.flagNewWebs = false ;
                                        vm.formData.webs = response.data;
                                        return vm.formData.webs;
                                    }
                                }
                            );
                    }

                    function updateWeb(value, id)
                    {
                        var url = value;

                        if( value === undefined  )
                        {
                          return 'Ingrese Url';
                        }

                        var data = {
                                'per_web_id' : id,
                                'url'    : url,
                            };
                            // console.log(data) ;

                            perWebService.update(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.webs = response.data;
                                        return vm.formData.webs;
                                    }
                                }
                            );
                    }

                    function deleteWeb(Telefono, id)
                    {
                        var modalOptions = {
                            closeButtonText: 'Cancelar',
                            actionButtonText: 'Eliminar',
                            headerText: 'Eliminar Web',
                            bodyText: '¿Esta Seguro de Eliminar Web: '+ Telefono+' ?'
                        };

                       modalService.showModalConfirm({}, modalOptions).then(function (result) {
                            confirmDeleteWeb(id);
                        });
                    }

                    function confirmDeleteWeb(id)
                    {
                         var data = {
                                'per_web_id'    : id
                            };

                            perWebService.deleteFill(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.webs = response.data;
                                        return vm.formData.webs;
                                    }
                                }
                            );
                    }

                    function reOrderItemsWebs()
                    {
                         var data = {
                                'webs' : vm.formData.webs,
                                'persona_id' : vm.persona_id ,
                            };

                            perTelefonoService.reOrderItems(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.webs = response.data;
                                        return vm.formData.webs;
                                    }
                                }
                            );
                    }

                    // sortable telefonos
                    vm.barConfigWebs = {
                        group: 'webs',
                        animation: 150,
                        // onSort: function (** ngSortEvent * evt){
                        onSort: function (evt){
                            vm.reOrderItemsWebs() ;
                            // console.log(vm.formData.telefonos);
                        }
                    };
            }
(function(){
	angular.module('personas.juridicas.info.controller').controller('ModalNewPerJuridicaCrtl', ModalNewPerJuridicaCrtl) ;
        ModalNewPerJuridicaCrtl.$inject = ['$uibModalInstance','$filter', 'perJuridicaService','ubigeoService'] ;

            function ModalNewPerJuridicaCrtl($uibModalInstance,$filter, perJuridicaService,ubigeoService)
            {
                var vm =  this ;

                vm.msj = "";
                vm.data_ubigeos = [] ;

                vm.formData = {
                    'ruc' : '',
                    'razon_social' : '',
                    'nombre_comercial' : '',
                    'telefonos' : '',
                    'mails' : '',
                    'sitio_web' : '',
                    'distrito ' : [],
                } ;

                init() ;
                function init()
                {
                    getUbigeos();
                } ;

                function getUbigeos()
                {
                    ubigeoService.getUbigeos().then(
                        function(response){
                            if (!response.error)
                            {
                                vm.data_ubigeos = response.data;
                                return vm.data_ubigeos ;
                            }
                        }
                    );
                };

                //  DatePicker ui-b
                   /* vm.today =  new Date() ;
                    vm.popup = {
                        opened : false,
                        maxDate : vm.today,
                    } ;

                    vm.openFechaNac = function() {
                        vm.popup.opened = true;
                    };
                    */

                vm.ok = function ()
                {
                    var ruc              = vm.formData.ruc ;
                    var razon_social     = vm.formData.razon_social ;
                    var nombre_comercial = vm.formData.nombre_comercial ;
                    var telefonos        = vm.formData.telefonos ;
                    var distrito_id      = vm.formData.distrito.id ;
                    var mails            = vm.formData.mails ;
                    var sitio_web        = vm.formData.sitio_web ;

                    vm.msj = {message : ''} ;
                    if (ruc === undefined)
                    {
                        return  vm.msj.message = 'Ingrese Ruc' ;
                    };
                    if (razon_social === undefined)
                    {
                        return  vm.msj.message = 'Ingrese Razon Social' ;
                    };

                    if (mails === undefined)
                    {
                        return  vm.msj.message = 'Ingrese almenos un mail' ;
                    };

                    if (telefonos === undefined)
                    {
                        return  vm.msj.message = 'Ingrese almenos un telefono' ;
                    };

                    if (distrito_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Distrito' ;
                    };


                    var data = {
                        'ruc' :  ruc ,
                        'razon_social' :  razon_social ,
                        'nombre_comercial' :  nombre_comercial ,
                        'telefonos' :  telefonos ,
                        'distrito_id' :  distrito_id ,
                        'mails' :  mails ,
                        'sitio_web' :  sitio_web ,
                    };


                    perJuridicaService.getPerJuridicaByRuc(data).then(
                        function(response)
                        {
                            // console.log(response);
                            if (!response.error)
                            {
                                if (response.data.length > 0)
                                {
                                    vm.msj = 'Ruc, ya se encuentra registrado!'
                                }else
                                {
                                    vm.save(data);
                                }
                                ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );
                };

                vm.save = function(data)
                {
                    perJuridicaService.save(data).then(
                        function(response)
                        {
                            console.log(response);
                            if (!response.error)
                            {
                                $uibModalInstance.close(vm.formData);
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );
                };

                vm.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }

})();
(function(){
    'use strict';

    angular.module('personas.juridicas.info.controller').controller('PerJuridicasInfoCtrl',PerJuridicasInfoCtrl);
    PerJuridicasInfoCtrl.$inject = ['$rootScope', 'perJuridicaService','$filter', '$uibModal', 'PATH', 'modalService','$state', 'NgTableParams'] ;

        function PerJuridicasInfoCtrl($rootScope, perJuridicaService,$filter, $uibModal, PATH, modalService,$state, NgTableParams)
        {
            var vm = this ;

            // function
                vm.onClick           = onClick ;
                vm.getPerJuridicas   = getPerJuridicas ;
                vm.newPerJuridica    = newPerJuridica ;
                vm.editPerJuridica   = editPerJuridica ;
                vm.deletePerJuridica = deletePerJuridica ;

            // variables
                vm.data_list = [] ;
                vm.fillSelected  = [] ;

            init();
            function init() {
                tablePlugin() ;
                vm.getPerJuridicas() ;
            } ;

            function onClick(name)
            {
                if (name === 'list')
                {
                    vm.getPerJuridicas();
                }
                else if (name === 'new')
                {
                      vm.newPerJuridica('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editPerJuridica() ;
                }
                else if (name === 'delete')
                {
                    vm.deletePerJuridica() ;
                }else{
                    return ;
                };

            } ;

            function getPerJuridicas()
            {
                perJuridicaService.getPerJuridicas().then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data_list = response.data;
                            reloadNgTable();

                            return vm.data_list ;
                        }
                    }
                );
            };



            // ===== ng-table ==============================================================================================

                vm.applyGlobalSearch = applyGlobalSearch;
                // vm.cancel = cancel;
                // vm.del    = del;
                // vm.save   = save;
                // vm.editRow = editRow ;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch()
                {
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }


            // ===================================================================================================

            //  New
                function newPerJuridica (size)
                {
                    vm.tableParams.page(1);
                    vm.tableParams.reload();

                    var path = PATH.INFORMACION ;

                    var modalInstance = $uibModal.open({
                        templateUrl: path+'/personas/empresas/nueva.empresa.tpl.html',
                        controller: 'ModalNewPerJuridicaCrtl',
                        controllerAs: 'vm',
                        size: size,
                        backdrop : 'static',
                    });

                    modalInstance.result.then(
                        function (data)
                        {
                            vm.getPerJuridicas() ;
                        },
                        function ()
                        {
                            // console.log('Modal dismissed at: ' + new Date());
                        }
                    );

                };

            //  Editar
                function editPerJuridica (size)
                {
                    var codigo = vm.fillSelected.persona_id ;

                    if (codigo === undefined) {
                        return modalAlert()
                    }
                    else
                    {
                        $state.go('personas.empresas.editar', { codigo: codigo })
                    }


                };

            // delete
                function deletePerJuridica()
                {
                    var cod = vm.fillSelected.persona_id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalConfirm() ;
                    }

                }

                function confirmDelete()
                {
                     var data = {
                            'persona_id'    : vm.fillSelected.persona_id,
                            'estado'    : 0,
                        };
                        // console.log(data);
                        perJuridicaService.updateEstado(data).then(
                            function(response){

                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getPerJuridicas() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm()
                {
                    var dato = vm.fillSelected.razon_social ;
                    console.log(vm.fillSelected);
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Empresa',
                        bodyText: '¿Esta Seguro de Eliminar Empresa: '+ dato+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete();
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

                $rootScope.$on("reloadListEmpresas", function(event, values) {
                    // console.log('reloadListPersona');
                       vm.getPerJuridicas();
                  });
        }

})() ;

  angular.module('persona.natural.info.controller').controller('EditPerNaturalCrtl', EditPerNaturalCrtl) ;
        EditPerNaturalCrtl.$inject = ['$rootScope',
                                     '$state',
                                     '$q',
                                     '$filter',
                                     '$stateParams',
                                     'modalService',
                                     'personaNaturalService',
                                     'perTelefonoService',
                                     'perMailService',
                                     'perDocumentoService'
                                    ] ;

            function EditPerNaturalCrtl($rootScope,
                                        $state,
                                        $q,
                                        $filter,
                                        $stateParams,
                                        modalService,
                                        personaNaturalService,
                                        perTelefonoService,
                                        perMailService,
                                        perDocumentoService
                                        )
            {
                var vm =  this ;

                vm.reloadListPersona = reloadListPersona ;

                vm.persona_id = $stateParams.codigo;

                // telefonos
                    vm.saveNewPhones = saveNewPhones ;
                    vm.updatePhone   = updatePhone ;
                    vm.deletePhone   = deletePhone;
                    vm.reOrderItemsTelefonos = reOrderItemsTelefonos ;

                    vm.flagNewPhones = false ;

                // mails
                    vm.saveNewMails       = saveNewMails ;
                    vm.updateMail         = updateMail ;
                    vm.deleteMail         = deleteMail;
                    vm.reOrderItemsEmails = reOrderItemsEmails ;

                    vm.flagNewMails = false ;

                // dni
                    vm.updateDni = updateDni ;

                    vm.updatePerNatInfo = updatePerNatInfo ;

                vm.msj = "";

                vm.formData = {
                    'dni'   : '',
                    'apellidos' : '',
                    'nombres'    : '',
                    'telefonos'  : [],
                    'mails'  : [],
                    'sexo'  : '',
                    'newtelefonos': [],
                    'newmails': [],
                    'fecha_nac': '',
                }

                 //  DatePicker ui-b
                    vm.today =  new Date() ;
                    vm.popup = {
                        opened : false,
                        maxDate : vm.today,
                    } ;

                    vm.openFechaNac = function() {
                        vm.popup.opened = true;
                    };

                //  sexo data
                    vm.sexoData = [
                            {value: 1, text: 'Masculino'},
                            {value: 2, text: 'Femenino'}
                          ];

                    vm.showStatus = function() {
                        var selected = $filter('filter')(vm.sexoData, {value: vm.formData.sexo});
                        return (vm.formData.sexo && selected.length) ? selected[0].text : ' No Seleccionado';
                    };


                // informacion de persona
                    getPerNaturalInfoAll() ;
                    function getPerNaturalInfoAll()
                    {
                        var data = {
                            'persona_id' : vm.persona_id,
                        } ;

                        personaNaturalService.getPerNaturalInfoAll(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.formData.dni       = response.data[0].per_natural.dni;
                                    vm.formData.apellidos = response.data[0].per_apellidos;
                                    vm.formData.nombres   = response.data[0].per_nombre;
                                    vm.formData.sexo      = response.data[0].per_natural.sexo;
                                    vm.formData.telefonos = response.data[0].per_telefono;
                                    vm.formData.mails     = response.data[0].per_mail;
                                    vm.formData.fecha_nac = (!response.data[0].per_fecha_nac)? vm.today :new Date( response.data[0].per_fecha_nac +' 00:00:00');
                                    return response.data ;
                                }
                            }
                        );
                    }

                // dni
                    function updateDni(value)
                    {
                        var deferred = $q.defer();

                        var dni = value;
                        var persona_id = vm.persona_id ;

                        var data = {
                                'persona_id'   : persona_id,
                                'dni'          : dni,
                            };

                        personaNaturalService.updateDni(data).then(
                                function(response){

                                    if (!response.error)
                                    {
                                        vm.formData.dni = response.data.dni;
                                        deferred.resolve();
                                    }else
                                    {
                                        deferred.resolve(response.data.dni[0]);
                                    }
                                }, function (response) {
                                    // the following line rejects the promise
                                    deferred.reject(response);
                                    return deferred.promise;
                                }
                            );
                        return deferred.promise;
                    } ;


                // update nombres y sexo
                    function updatePerNatInfo()
                    {
                        var persona_id = vm.persona_id ;

                        var fecha_nac = $filter('date')(vm.formData.fecha_nac,'yyyy-MM-dd');

                         var data = {
                                'apellidos'  : vm.formData.apellidos,
                                'nombres'    : vm.formData.nombres,
                                'sexo'       : vm.formData.sexo,
                                'persona_id' : persona_id,
                                'fecha_nac'  : fecha_nac,
                            };


                            personaNaturalService.updatePerNatInfo(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.apellidos = response.data.per_apellidos;
                                        vm.formData.nombres   = response.data.per_nombre;
                                        vm.formData.sexo      = response.data.per_natural.sexo;
                                        vm.formData.fecha_nac = (!response.data.per_fecha_nac)? vm.today :new Date( response.data.per_fecha_nac+' 00:00:00');

                                    }
                                }
                            );

                    };


                //  telefonos
                    function saveNewPhones()
                    {
                        var telefonos = vm.formData.newtelefonos ;
                        var persona_id = vm.persona_id ;

                         var data = {
                                'telefonos'    : telefonos,
                                'persona_id'   : persona_id,
                            };
                            perTelefonoService.save(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.newtelefonos = [] ;
                                        vm.flagNewPhones = false ;
                                        vm.formData.telefonos = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );

                    }

                    function updatePhone(value, id)
                    {
                        var telefono = value;

                        if( value.length != 9  )
                        {
                          return 'Teléfono Invalido(Son Nueve Numero)';
                        }

                        var data = {
                                'telefono_id' : id,
                                'telefono'    : telefono,
                            };

                            perTelefonoService.update(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.telefonos = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );
                    }

                    function deletePhone(Telefono, id)
                    {
                        var modalOptions = {
                            closeButtonText: 'Cancelar',
                            actionButtonText: 'Eliminar',
                            headerText: 'Eliminar Teléfono',
                            bodyText: '¿Esta Seguro de Eliminar Teléfono: '+ Telefono+' ?'
                        };

                       modalService.showModalConfirm({}, modalOptions).then(function (result) {
                            confirmDeletePhone(id);
                        });
                    }

                    function confirmDeletePhone(id)
                    {
                         var data = {
                                'telefono_id'    : id
                            };

                            perTelefonoService.deleteFill(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.telefonos = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );
                    }

                    function reOrderItemsTelefonos()
                    {
                         var data = {
                                'telefonos' : vm.formData.telefonos,
                                'persona_id' : vm.persona_id ,
                            };

                            perTelefonoService.reOrderItems(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.telefonos = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );
                    }

                    // sortable telefonos
                    vm.barConfigPhones = {
                        group: 'telefonos',
                        animation: 150,
                        // onSort: function (** ngSortEvent * evt){
                        onSort: function (evt){
                            vm.reOrderItemsTelefonos() ;
                        }
                    };



                //  Mail
                    function saveNewMails()
                    {
                        var mails = vm.formData.new_mails ;
                        var persona_id = vm.persona_id ;

                         var data = {
                                'persona_id'   : persona_id,
                                'mails'        : mails,
                            };

                            perMailService.save(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.new_mails = [] ;
                                        vm.flagNewMails = false ;
                                        vm.formData.mails = response.data;
                                        return vm.formData.mails;
                                    }
                                }
                            );
                    }

                    function updateMail(value, id)
                    {
                        var mail = value;

                        var data = {
                                'mail_id' : id,
                                'mail'    : mail,
                            };

                            perMailService.update(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.mails = response.data;
                                        return vm.formData.mails;
                                    }
                                }
                            );
                    }

                    function deleteMail(value, id)
                    {
                        var modalOptions = {
                            closeButtonText: 'Cancelar',
                            actionButtonText: 'Eliminar',
                            headerText: 'Eliminar Mail',
                            bodyText: '¿Esta Seguro de Eliminar Mail: '+ value+' ?'
                        };

                       modalService.showModalConfirm({}, modalOptions).then(function (result) {
                            confirmDeleteMail(id);
                        });
                    }

                    function confirmDeleteMail(id)
                    {
                         var data = {
                                'mail_id'    : id
                            };

                            perMailService.deleteFill(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.mails = response.data;
                                        return vm.formData.telefonos;
                                    }
                                }
                            );
                    }

                    function reOrderItemsEmails()
                    {
                         var data = {
                                'mails' : vm.formData.mails,
                                'persona_id' : vm.persona_id ,
                            };

                            perMailService.reOrderItems(data).then(
                                function(response){
                                    if (!response.error)
                                    {
                                        vm.formData.mails = response.data;
                                        return vm.formData.mails;
                                    }
                                }
                            );
                    }

                    // sortable Emails
                    vm.barConfigEmails = {
                        group: 'Mails',
                        animation: 150,
                        // onSort: function (** ngSortEvent *evt){
                        onSort: function (evt){
                            vm.reOrderItemsEmails() ;
                        }
                    };

                    function reloadListPersona()
                    {
                        var current = $state.current.name ;
                        var res = current.split(".",2);
                        var current_parent = res[0]+"."+res[1];

                        var values = '' ;
                        $state.go(current_parent, values);
                        $rootScope.$broadcast("reloadListPersona", values);
                    }

            }
(function(){
	angular.module('persona.natural.info.controller').controller('ModalNewPersonaNaturalCrtl', ModalNewPersonaNaturalCrtl) ;
        ModalNewPersonaNaturalCrtl.$inject = ['$state','$uibModalInstance','$filter', 'personaNaturalService'] ;

            function ModalNewPersonaNaturalCrtl($state,$uibModalInstance,$filter, personaNaturalService)
            {
                var vm =  this ;

                vm.msj = "";

                vm.formData = {
                    'dni'   : '',
                    'apellidos' : '',
                    'nombres'    : '',
                    'telefonos'  : [],
                    'mails'  : [],
                    'sexo'  : 1,
                    'fecha_nac': '',
                    'tipo_relacion_id' : 2,
                    'persona_id': '',
                    'isRegistrer' : false,
                } ;

                vm.title_modal = "Persona";
                init();
                function init() {
                    config($state.current.name);
                }

                function config(state_name)
                {
                    // console.log(state_name);
                    if (state_name === 'personas.personas')
                    {
                        vm.title_modal = "Persona" ;
                        vm.formData.tipo_relacion_id       = 2;
                    }
                    else if (state_name === 'personas.clientes')
                    {
                        vm.title_modal = "Cliente" ;
                        vm.formData.tipo_relacion_id       = 3;
                    }
                    else if (state_name === 'personas.empleados')
                    {
                        vm.title_modal = "Empleado" ;
                        vm.formData.tipo_relacion_id       = 4;
                    }
                    else if (state_name === 'personas.avales')
                    {
                        vm.title_modal            = "Aval" ;
                        vm.formData.tipo_relacion_id = 5;
                    };
                    // console.log( vm.title_modal);
                    // console.log( vm.formData.tipo_relacion_id);
                };

                //  DatePicker ui-b
                    vm.today =  new Date() ;
                    vm.popup = {
                        opened : false,
                        maxDate : vm.today,
                    } ;

                    vm.openFechaNac = function() {
                        vm.popup.opened = true;
                    };

                vm.getPersonaByDni = function()
                {
                    vm.formData.apellidos = '';
                    vm.formData.nombres   = '';
                    vm.formData.telefonos = '';
                    vm.formData.mails     = '';
                    vm.formData.sexo      = '';
                    vm.formData.fecha_nac = '';
                    vm.formData.persona_id = '';
                    vm.formData.isRegistrer =  false ;

                    var dni =  vm.formData.dni ;
                    if (dni.length < 8 )
                    {
                        return ;
                    };



                    var data = {'dni': dni } ;

                    personaNaturalService.getPerNaturalByDni(data).then(
                        function(response)
                        {
                            var data = response.data ;
                            // console.log(data);
                            if (data !== null && data.length > 0)
                            {
                                // vm.formData.dni       = data[0].per_natural.dni ;
                                vm.formData.apellidos   = data[0].per_apellidos ;
                                vm.formData.nombres     = data[0].per_nombre ;
                                vm.formData.telefonos   = data[0].per_telefono  ;
                                vm.formData.mails       = data[0].per_mail ;
                                vm.formData.sexo        = data[0].per_natural.sexo;
                                vm.formData.fecha_nac   = (!data[0].per_fecha_nac)? vm.today :new Date( data[0].per_fecha_nac +' 00:00:00');
                                vm.formData.persona_id  = data[0].per_natural.persona_id;
                                vm.formData.isRegistrer =  true ;
                            }else{
                                vm.formData.isRegistrer =  false ;
                            } ;

                        }
                    );
                }

                vm.ok = function ()
                {
                    vm.save();

                  /*  var dni =  vm.formData.dni ;
                    var data = {'dni': dni } ;

                    personaNaturalService.getPerNaturalByDni(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                if (response.data !== null && response.data.length > 0)
                                {
                                    vm.msj = 'DNI Ya se encuentra registrado!'
                                }else
                                {
                                    vm.save();
                                }
                                ;
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );*/
                };

                vm.save = function()
                {
                    var fecha_nac = $filter('date')(vm.formData.fecha_nac,'yyyy-MM-dd');
                    vm.formData.fecha_nac = fecha_nac ;
                    // console.log(vm.formData);
                    personaNaturalService.save(vm.formData).then(
                        function(response)
                        {
                            // console.log(response);
                            if (!response.error)
                            {
                                $uibModalInstance.close(vm.formData);
                            }else
                            {
                                vm.msj = response.error ;
                            }
                        }
                    );
                };

                vm.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }

})();
(function(){
    'use strict';

    angular.module('persona.natural.info.controller').controller('PersonaNaturalInfoCtrl',PersonaNaturalInfoCtrl);
    PersonaNaturalInfoCtrl.$inject = ['$rootScope', 'personaNaturalService','$filter', '$uibModal', 'PATH', 'modalService','$state', 'NgTableParams'] ;

        function PersonaNaturalInfoCtrl($rootScope, personaNaturalService,$filter, $uibModal, PATH, modalService,$state, NgTableParams)
        {
            var vm = this ;

            // function
                vm.onClick              = onClick ;
                vm.getPersonasNaturales = getPersonasNaturales ;
                vm.newPersonaNatural    = newPersonaNatural ;
                vm.editPersona          = editPersona ;
                vm.deletePersona        = deletePersona ;

            // variables
                vm.data_list = [] ;
                vm.fillSelected  = [] ;

            init();
            function init() {
                config($state.current.name);
                tablePlugin() ;
                vm.getPersonasNaturales() ;
            }

            function config(state_name)
            {
                // console.log(state_name);
                if (state_name === 'personas.personas')
                {
                    vm.title_page = "Personas" ;
                    vm.title_tipo = "Persona" ;
                    vm.tipo_relacion_id = 2;
                }
                else if (state_name === 'personas.clientes')
                {
                    vm.title_page = "Clientes" ;
                    vm.title_tipo = "Cliente" ;
                    vm.tipo_relacion_id = 3;
                }
                else if (state_name === 'personas.empleados')
                {
                    vm.title_page = "Empleados" ;
                    vm.title_tipo = "Empleado" ;
                    vm.tipo_relacion_id = 4;
                }
                else if (state_name === 'personas.avales')
                {
                    vm.title_page = "Avales" ;
                    vm.title_tipo = "Aval" ;
                    vm.tipo_relacion_id = 5;
                };
            };

            function onClick(name)
            {
                if (name === 'list')
                {
                    vm.getPersonasNaturales();
                }
                else if (name === 'new')
                {
                      vm.newPersonaNatural('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editPersona() ;
                }
                else if (name === 'delete')
                {
                    vm.deletePersona() ;
                }else{
                    return ;
                };

            } ;

            function getPersonasNaturales()
            {

                // personaNaturalService.getPersonasNaturales().then(
                var data = { 'tipo_relacion_id' : vm.tipo_relacion_id} ;
                personaNaturalService.getPersonasByTipoRelacion(data).then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data_list = response.data;
                            reloadNgTable();

                            return vm.data_list ;
                        }
                    }
                );
            }

            // ===== ng-table ==============================================================================================

                vm.applyGlobalSearch = applyGlobalSearch;
                // vm.cancel = cancel;
                // vm.del    = del;
                // vm.save   = save;
                // vm.editRow = editRow ;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch()
                {
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }


            // ===================================================================================================

            //  New
                function newPersonaNatural (size)
                {
                    vm.tableParams.page(1);
                    vm.tableParams.reload();

                    var path = PATH.INFORMACION ;

                    var modalInstance = $uibModal.open({
                        templateUrl: path+'/personas/per_natural/nueva.natural.tpl.html',
                        controller: 'ModalNewPersonaNaturalCrtl',
                        controllerAs: 'vm',
                        size: size,
                        backdrop : 'static',
                    });

                    modalInstance.result.then(
                        function (data)
                        {
                            vm.getPersonasNaturales() ;
                        },
                        function ()
                        {
                            // console.log('Modal dismissed at: ' + new Date());
                        }
                    );

                };

            //  Editar
                function editPersona (size)
                {
                    var codigo = vm.fillSelected.persona_id ;

                    if (codigo === undefined) {
                        return modalAlert()
                    }
                    else
                    {
                        $state.go($state.current.name+'.editar', { codigo: codigo })
                        // $state.go('personas.personas.editar', { codigo: codigo })
                    }
                };

            // delete
                function deletePersona()
                {
                    var cod = vm.fillSelected.persona_id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalConfirm() ;
                    }

                }

                function confirmDelete()
                {
                     var data = {
                            'persona_id'    : vm.fillSelected.persona_id,
                            'estado'    : 0,
                            'tipo_relacion_id' : vm.tipo_relacion_id,
                        };
                        // console.log(data);
                        personaNaturalService.updateEstado(data).then(
                            function(response){

                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getPersonasNaturales() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm()
                {
                    var dato = vm.fillSelected.per_apellidos+ ' ' + vm.fillSelected.per_nombre ;
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar '+vm.title_tipo,
                        bodyText: '¿Esta Seguro de Eliminar '+vm.title_tipo+': '+ dato+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete();
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

                $rootScope.$on("reloadListPersona", function(event, values) {
                    // console.log('reloadListPersona');
                       vm.getPersonasNaturales();
                  });
        }

})() ;

(function(){
    'use strict';

    angular.module('acuerdoPagos.info.controller').controller('AcuerdoPagosInfoCtrl',AcuerdoPagosInfoCtrl);
    AcuerdoPagosInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','acuerdoPagoService','tipoPeriodoService'] ;

        function AcuerdoPagosInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,acuerdoPagoService,tipoPeriodoService)
        {
            var vm = this ;

            // function
                vm.getAcuerdoPagos   = getAcuerdoPagos ;
                vm.onClick    = onClick ;
                vm.newAcuerdoPago     = newAcuerdoPago ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;

                vm.data_tipo_prestamos = [] ;

            init();
            function init() {
                tablePlugin() ;
                vm.getAcuerdoPagos() ;
                getTipoPeriodos() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getAcuerdoPagos();
                }
                else if (name === 'new')
                {
                      vm.newAcuerdoPago('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }

                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            };

            function getAcuerdoPagos()
            {
                acuerdoPagoService.getAcuerdoPagos().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            } ;


            function getTipoPeriodos()
            {
                tipoPeriodoService.getTipoPeriodos().then(
                    function(response){
                        if (!response.error)
                        {
                            console.log(response.data);
                            vm.data_tipo_prestamos = response.data;
                            return vm.data_tipo_prestamos ;
                        }
                    }
                );
            };


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'acuerdo_pago_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                        'rango' : row.rango,
                        'partes' : row.partes,
                        'tipo_periodo_id' : row.tipo_periodo.id,
                    };

                    acuerdoPagoService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getAcuerdoPagos() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newAcuerdoPago (size)
                {

                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/registros/acuerdo-pagos/new.acuerdo-pago.tpl.html',
                            controller: 'ModalNewAcuerdoPagoCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getAcuerdoPagos(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        acuerdoPagoService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getAcuerdoPagos() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    // console.log(row) ;
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Acuerdo de Prestamo',
                        bodyText: '¿Esta Seguro de Eliminar Acuerdo de Prestamo: '+ row.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;


(function(){
    'use stric' ;

    angular.module('acuerdoPagos.info.controller').controller('ModalNewAcuerdoPagoCrtl', ModalNewAcuerdoPagoCrtl) ;
    ModalNewAcuerdoPagoCrtl.$inject = ['$uibModalInstance', 'acuerdoPagoService','tipoPeriodoService'] ;

        function ModalNewAcuerdoPagoCrtl($uibModalInstance, acuerdoPagoService,tipoPeriodoService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                descripcion : '',
                rango : '',
                partes : '',
                tipo_periodo : [],
            } ;

            vm.data_tipo_periodos = [] ;

            init() ;
            function init()
            {
                getTipoPeriodos() ;
            };

            function getTipoPeriodos()
            {
                tipoPeriodoService.getTipoPeriodos().then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data_tipo_periodos = response.data;
                            return vm.data_tipo_periodos ;
                        }
                    }
                );
            };

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                    'rango'  : vm.formData.rango,
                    'partes'  : vm.formData.partes,
                    'tipo_periodo_id' : vm.formData.tipo_periodo.id,
                };
                console.log(data);
                acuerdoPagoService.save(data).then(
                    function(response)
                    {
                        console.log(response);
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use strict';

    angular.module('areas.info.controller').controller('AreasInfoCtrl',AreasInfoCtrl);
    AreasInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','areaService'] ;

        function AreasInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,areaService)
        {
            var vm = this ;

            // function
                vm.getAreas   = getAreas ;
                vm.onClick    = onClick ;
                vm.newArea     = newArea ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                vm.getAreas() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getAreas();
                }
                else if (name === 'new')
                {
                      vm.newArea('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }
                else if (name === 'delete')
                {
                    deleteRol() ;
                }
                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            }

            function getAreas()
            {
                areaService.getAreas().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'area_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                    };

                    areaService.updateArea(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getAreas() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newArea (size)
                {
                        var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/registros/areas/new.area.tpl.html',
                            controller: 'ModalNewAreaCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getAreas(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        areaService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getAreas() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Area',
                        bodyText: '¿Esta Seguro de Eliminar Area: '+ row.data+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;


(function(){
    'use stric' ;

    angular.module('areas.info.controller').controller('ModalNewAreaCrtl', ModalNewAreaCrtl) ;
    ModalNewAreaCrtl.$inject = ['$uibModalInstance', 'areaService'] ;

        function ModalNewAreaCrtl($uibModalInstance, areaService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                descripcion : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                };

                areaService.saveArea(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use strict';

    angular.module('cargos.info.controller').controller('CargosInfoCtrl',CargosInfoCtrl);
    CargosInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','areaService','cargoService'] ;

        function CargosInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,areaService,cargoService)
        {
            var vm = this ;

            // function
                vm.getCargos   = getCargos ;
                vm.onClick    = onClick ;
                vm.newCargo     = newCargo ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                getAreas() ;
                vm.getCargos() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getCargos();
                }
                else if (name === 'new')
                {
                      vm.newCargo('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }
                else if (name === 'delete')
                {
                    deleteRol() ;
                }
                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            }

            function getCargos()
            {
                cargoService.getCargos().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            function getAreas()
            {
                areaService.getAreas().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_areas = response.data ;
                            // console.log(vm.data_areas );
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            }


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'cargo_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                        'area_id' : row.area.id,
                    };

                    cargoService.updateCargo(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getCargos() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newCargo (size)
                {
                        var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/registros/cargos/new.cargo.tpl.html',
                            controller: 'ModalNewCargoCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getCargos(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        cargoService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getCargos() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Cargo',
                        bodyText: '¿Esta Seguro de Eliminar Cargo: '+ row.nombre +' del Area: '+row.area.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;


(function(){
    'use stric' ;

    angular.module('cargos.info.controller').controller('ModalNewCargoCrtl', ModalNewCargoCrtl) ;
    ModalNewCargoCrtl.$inject = ['$uibModalInstance', 'areaService','cargoService'] ;

        function ModalNewCargoCrtl($uibModalInstance, areaService,cargoService)
        {
            var vm =  this ;

            vm.msj = "";
            vm.data_areas = [] ;

            vm.formData = {
                nombre : '',
                descripcion : '',
                area : [],
            }

            init();
            function init(){
                getAreas() ;
            } ;

            function getAreas()
            {
                areaService.getAreas().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_areas = response.data ;
                            // console.log(vm.data_areas );
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                    'area_id'         : vm.formData.area.id,

                };
                // console.log(data);
                cargoService.saveCargo(data).then(
                    function(response)
                    {
                        // console.log(response);

                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use stric' ;

    angular.module('licencias.info.controller').controller('ModalNewLicenciaCrtl', ModalNewLicenciaCrtl) ;
    ModalNewLicenciaCrtl.$inject = ['$uibModalInstance', 'licenciaService'] ;

        function ModalNewLicenciaCrtl($uibModalInstance, licenciaService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                descripcion : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                };

                licenciaService.save(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use strict';

    angular.module('licencias.info.controller').controller('LicenciasInfoCtrl',LicenciasInfoCtrl);
    LicenciasInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','licenciaService'] ;

        function LicenciasInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,licenciaService)
        {
            var vm = this ;

            // function
                vm.getLicencias   = getLicencias ;
                vm.onClick    = onClick ;
                vm.newLicencia     = newLicencia ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                vm.getLicencias() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getLicencias();
                }
                else if (name === 'new')
                {
                      vm.newLicencia('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }
                else if (name === 'delete')
                {
                    deleteRol() ;
                }
                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            }

            function getLicencias()
            {
                licenciaService.getLicencias().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'licencia_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                    };

                    licenciaService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getLicencias() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newLicencia (size)
                {
                        var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/registros/licencias/new.licencia.tpl.html',
                            controller: 'ModalNewLicenciaCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getLicencias(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        licenciaService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getLicencias() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Licencia',
                        bodyText: '¿Esta Seguro de Eliminar Licencia: '+ row.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;


(function(){
    'use stric' ;

    angular.module('tipoCambios.info.controller').controller('ModalNewTipoCambioCrtl', ModalNewTipoCambioCrtl) ;
    ModalNewTipoCambioCrtl.$inject = ['$uibModalInstance', 'tipoCambioService'] ;

        function ModalNewTipoCambioCrtl($uibModalInstance, tipoCambioService)
        {
            var vm =  this ;

            vm.msj = {
                message: '',
                data: [] ,
            } ;

            vm.formData = {
                nombre : '',
                // descripcion : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'valor' : vm.formData.valor ,
                    'compra' : 0 ,
                    'venta' : 0 ,
                    'dia' : '' ,
                };

                tipoCambioService.save(data).then(
                    function(response)
                    {
                        console.log(response);
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use strict';

    angular.module('tipoCambios.info.controller').controller('TipoCambiosInfoCtrl',TipoCambiosInfoCtrl);
    TipoCambiosInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','tipoCambioService'] ;

        function TipoCambiosInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,tipoCambioService)
        {
            var vm = this ;

            // function
                vm.getTipoCambios   = getTipoCambios ;
                vm.onClick    = onClick ;
                vm.newTipoCambio     = newTipoCambio ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                vm.getTipoCambios() ;
            };

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getTipoCambios();
                }
                else if (name === 'new')
                {
                      vm.newTipoCambio('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }
                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            }

            function getTipoCambios()
            {
                tipoCambioService.getTipoCambios().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'tipo_cambio_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                    };

                    tipoCambioService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getTipoCambios() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                    console.log(row) ;
                    return false ;
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newTipoCambio (size)
                {

                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/registros/tipo-cambios/new.tipo-cambio.tpl.html',
                            controller: 'ModalNewTipoCambioCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getTipoCambios(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        tipoCambioService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getTipoCambios() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    // console.log(row) ;
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Tipo de Cambio',
                        bodyText: '¿Esta Seguro de Eliminar Tipo de Cambio: '+ row.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;


(function(){
    'use stric' ;

    angular.module('tipoGarantias.info.controller').controller('ModalNewTipoGarantiaCrtl', ModalNewTipoGarantiaCrtl) ;
    ModalNewTipoGarantiaCrtl.$inject = ['$uibModalInstance', 'tipoGarantiaService'] ;

        function ModalNewTipoGarantiaCrtl($uibModalInstance, tipoGarantiaService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                descripcion : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                };

                tipoGarantiaService.save(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use strict';

    angular.module('tipoGarantias.info.controller').controller('TipoGarantiasInfoCtrl',TipoGarantiasInfoCtrl);
    TipoGarantiasInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','tipoGarantiaService'] ;

        function TipoGarantiasInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,tipoGarantiaService)
        {
            var vm = this ;

            // function
                vm.getTipoGarantias   = getTipoGarantias ;
                vm.onClick    = onClick ;
                vm.newTipoGarantia     = newTipoGarantia ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                vm.getTipoGarantias() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getTipoGarantias();
                }
                else if (name === 'new')
                {
                      vm.newTipoGarantia('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }

                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            }

            function getTipoGarantias()
            {
                tipoGarantiaService.getTipoGarantias().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'tipo_garantia_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                    };

                    tipoGarantiaService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getTipoGarantias() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newTipoGarantia (size)
                {

                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/registros/tipo-garantias/new.tipo-garantia.tpl.html',
                            controller: 'ModalNewTipoGarantiaCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getTipoGarantias(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        tipoGarantiaService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getTipoGarantias() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    // console.log(row) ;
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Tipo de Prestamo',
                        bodyText: '¿Esta Seguro de Eliminar Tipo de Prestamo: '+ row.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;


(function(){
    'use stric' ;

    angular.module('tipoMonedas.info.controller').controller('ModalNewTipoMonedaCrtl', ModalNewTipoMonedaCrtl) ;
    ModalNewTipoMonedaCrtl.$inject = ['$uibModalInstance', 'tipoMonedaService'] ;

        function ModalNewTipoMonedaCrtl($uibModalInstance, tipoMonedaService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                simbolo : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'simbolo'  : vm.formData.simbolo,
                };

                tipoMonedaService.save(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use strict';

    angular.module('tipoMonedas.info.controller').controller('TipoMonedasInfoCtrl',TipoMonedasInfoCtrl);
    TipoMonedasInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','tipoMonedaService'] ;

        function TipoMonedasInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,tipoMonedaService)
        {
            var vm = this ;

            // function
                vm.getTipoMonedas   = getTipoMonedas ;
                vm.onClick    = onClick ;
                vm.newTipoMoneda     = newTipoMoneda ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                vm.getTipoMonedas() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getTipoMonedas();
                }
                else if (name === 'new')
                {
                      vm.newTipoMoneda('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }

                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            }

            function getTipoMonedas()
            {
                tipoMonedaService.getTipoMonedas().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'tipo_moneda_id' : row.id,
                        'nombre' : row.nombre,
                        'simbolo' : row.simbolo,
                    };

                    tipoMonedaService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getTipoMonedas() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newTipoMoneda (size)
                {

                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/registros/tipo-monedas/new.tipo-moneda.tpl.html',
                            controller: 'ModalNewTipoMonedaCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getTipoMonedas(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        tipoMonedaService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getTipoMonedas() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    // console.log(row) ;
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Tipo de Moneda',
                        bodyText: '¿Esta Seguro de Eliminar Tipo de Moneda: '+ row.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;


(function(){
    'use stric' ;

    angular.module('tipoPagos.info.controller').controller('ModalNewTipoPagoCrtl', ModalNewTipoPagoCrtl) ;
    ModalNewTipoPagoCrtl.$inject = ['$uibModalInstance', 'tipoPagoService','tipoPrestamoService'] ;

        function ModalNewTipoPagoCrtl($uibModalInstance, tipoPagoService,tipoPrestamoService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                descripcion : '',
                tipo_prestamo : [],
            } ;

            vm.data_tipo_prestamos = [] ;

            init() ;
            function init()
            {
                getTipoPrestamos() ;
            };

            function getTipoPrestamos()
            {
                tipoPrestamoService.getTipoPrestamos().then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data_tipo_prestamos = response.data;
                            return vm.data_tipo_prestamos ;
                        }
                    }
                );
            };

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                    'tipo_prestamo_id' : vm.formData.tipo_prestamo.id,
                };

                tipoPagoService.save(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use strict';

    angular.module('tipoPagos.info.controller').controller('TipoPagosInfoCtrl',TipoPagosInfoCtrl);
    TipoPagosInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','tipoPagoService','tipoPrestamoService'] ;

        function TipoPagosInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,tipoPagoService,tipoPrestamoService)
        {
            var vm = this ;

            // function
                vm.getTipoPagos   = getTipoPagos ;
                vm.onClick    = onClick ;
                vm.newTipoPago     = newTipoPago ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;

                vm.data_tipo_prestamos = [] ;

            init();
            function init() {
                tablePlugin() ;
                vm.getTipoPagos() ;
                getTipoPrestamos() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getTipoPagos();
                }
                else if (name === 'new')
                {
                      vm.newTipoPago('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }

                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            };

            function getTipoPagos()
            {
                tipoPagoService.getTipoPagos().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            } ;


            function getTipoPrestamos()
            {
                tipoPrestamoService.getTipoPrestamos().then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data_tipo_prestamos = response.data;
                            return vm.data_tipo_prestamos ;
                        }
                    }
                );
            };


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'tipo_pago_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                        'tipo_prestamo_id' : row.tipo_prestamo.id,
                    };

                    tipoPagoService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getTipoPagos() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newTipoPago (size)
                {

                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/registros/tipo-pagos/new.tipo-pago.tpl.html',
                            controller: 'ModalNewTipoPagoCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getTipoPagos(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        tipoPagoService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getTipoPagos() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    // console.log(row) ;
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Tipo de Prestamo',
                        bodyText: '¿Esta Seguro de Eliminar Tipo de Prestamo: '+ row.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;


(function(){
    'use stric' ;

    angular.module('tipoPeriodos.info.controller').controller('ModalNewTipoPeriodoCrtl', ModalNewTipoPeriodoCrtl) ;
    ModalNewTipoPeriodoCrtl.$inject = ['$uibModalInstance', 'tipoPeriodoService'] ;

        function ModalNewTipoPeriodoCrtl($uibModalInstance, tipoPeriodoService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                rango : '',
                descripcion : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'      : vm.formData.nombre,
                    'rango'       : vm.formData.rango,
                    'descripcion' : vm.formData.descripcion,
                };

                tipoPeriodoService.save(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use strict';

    angular.module('tipoPeriodos.info.controller').controller('TipoPeriodosInfoCtrl',TipoPeriodosInfoCtrl);
    TipoPeriodosInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','tipoPeriodoService'] ;

        function TipoPeriodosInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,tipoPeriodoService)
        {
            var vm = this ;

            // function
                vm.getTipoPeriodos   = getTipoPeriodos ;
                vm.onClick    = onClick ;
                vm.newTipoPeriodo     = newTipoPeriodo ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                vm.getTipoPeriodos() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getTipoPeriodos();
                }
                else if (name === 'new')
                {
                      vm.newTipoPeriodo('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }

                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            }

            function getTipoPeriodos()
            {
                tipoPeriodoService.getTipoPeriodos().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'tipo_periodo_id' : row.id,
                        'nombre' : row.nombre,
                        'rango' : row.rango,
                        'descripcion' : row.descripcion,
                    };


                    tipoPeriodoService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getTipoPeriodos() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newTipoPeriodo (size)
                {

                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/registros/tipo-periodos/new.tipo-periodo.tpl.html',
                            controller: 'ModalNewTipoPeriodoCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getTipoPeriodos(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        tipoPeriodoService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getTipoPeriodos() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    // console.log(row) ;
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Tipo de Prestamo',
                        bodyText: '¿Esta Seguro de Eliminar Tipo de Prestamo: '+ row.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;


(function(){
    'use stric' ;

    angular.module('areas.info.controller').controller('ModalNewTipoPrestamoCrtl', ModalNewTipoPrestamoCrtl) ;
    ModalNewTipoPrestamoCrtl.$inject = ['$uibModalInstance', 'tipoPrestamoService'] ;

        function ModalNewTipoPrestamoCrtl($uibModalInstance, tipoPrestamoService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                descripcion : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                };

                tipoPrestamoService.save(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use strict';

    angular.module('tipoPrestamos.info.controller').controller('TipoPrestamosInfoCtrl',TipoPrestamosInfoCtrl);
    TipoPrestamosInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','tipoPrestamoService'] ;

        function TipoPrestamosInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,tipoPrestamoService)
        {
            var vm = this ;

            // function
                vm.getTipoPrestamos   = getTipoPrestamos ;
                vm.onClick    = onClick ;
                vm.newTipoPrestamo     = newTipoPrestamo ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                vm.getTipoPrestamos() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getTipoPrestamos();
                }
                else if (name === 'new')
                {
                      vm.newTipoPrestamo('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }

                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            }

            function getTipoPrestamos()
            {
                tipoPrestamoService.getTipoPrestamos().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'tipo_prestamo_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                    };

                    tipoPrestamoService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getTipoPrestamos() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newTipoPrestamo (size)
                {

                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/registros/tipo_prestamos/new.tipo-prestamo.tpl.html',
                            controller: 'ModalNewTipoPrestamoCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getTipoPrestamos(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        tipoPrestamoService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getTipoPrestamos() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    // console.log(row) ;
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Tipo de Prestamo',
                        bodyText: '¿Esta Seguro de Eliminar Tipo de Prestamo: '+ row.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;


(function(){
    'use stric' ;

    angular.module('claseVehiculos.info.controller').controller('ModalNewClaseVehiculoCrtl', ModalNewClaseVehiculoCrtl) ;
    ModalNewClaseVehiculoCrtl.$inject = ['$uibModalInstance', 'claseVehiculoService'] ;

        function ModalNewClaseVehiculoCrtl($uibModalInstance, claseVehiculoService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                descripcion : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                };

                claseVehiculoService.save(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use strict';

    angular.module('claseVehiculos.info.controller').controller('ClaseVehiculosInfoCtrl',ClaseVehiculosInfoCtrl);
    ClaseVehiculosInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','claseVehiculoService'] ;

        function ClaseVehiculosInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,claseVehiculoService)
        {
            var vm = this ;

            // function
                vm.getClaseVehiculos = getClaseVehiculos ;
                vm.onClick           = onClick ;
                vm.newTipoVehiculo   = newTipoVehiculo ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                vm.getClaseVehiculos() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getClaseVehiculos();
                }
                else if (name === 'new')
                {
                      vm.newTipoVehiculo('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }

                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            }

            function getClaseVehiculos()
            {
                claseVehiculoService.getClaseVehiculos().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                            sorting: { nombre: "asc" }
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'clase_vehiculo_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                    };

                    claseVehiculoService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getClaseVehiculos() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                } ;

               /* function applySelectedSort(){
                  vm.tableParams.sorting('clase_vehiculo_id',  'desc');
                }*/

            // ===================================================================================================

            //  New rol
                function newTipoVehiculo (size)
                {
                   /* console.log(vm.tableParams.page());
                    console.log(vm.tableParams.count());
                    console.log(vm.tableParams.total());
                    console.log(last_page);*/
                    var last_page = Math.ceil(vm.tableParams.total() / vm.tableParams.count()) ;

                    // pages.length
                        vm.tableParams.page( last_page );
                        vm.tableParams.reload();
                        vm.tableParams.sorting({}) ;

                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/vehiculos/clase-vehiculos/new.clase-vehiculo.tpl.html',
                            controller: 'ModalNewClaseVehiculoCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getClaseVehiculos(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        claseVehiculoService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getClaseVehiculos() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    // console.log(row) ;
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Clase de Vehiculo',
                        bodyText: '¿Esta Seguro de Eliminar Clase de Vehiculo: '+ row.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;


(function(){
    'use stric' ;

    angular.module('marcas.info.controller').controller('ModalNewMarcaCrtl', ModalNewMarcaCrtl) ;
    ModalNewMarcaCrtl.$inject = ['$uibModalInstance', 'marcaService'] ;

        function ModalNewMarcaCrtl($uibModalInstance, marcaService)
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                nombre : '',
                descripcion : '',
            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                };

                marcaService.save(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response.error ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use strict';

    angular.module('marcas.info.controller').controller('MarcasInfoCtrl',MarcasInfoCtrl);
    MarcasInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','marcaService'] ;

        function MarcasInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,marcaService)
        {
            var vm = this ;

            // function
                vm.getMarcas = getMarcas ;
                vm.onClick   = onClick ;
                vm.newMarca  = newMarca ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                vm.getMarcas() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getMarcas();
                }
                else if (name === 'new')
                {
                      vm.newMarca('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }

                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            }

            function getMarcas()
            {
                marcaService.getMarcas().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                            sorting: { nombre: "asc" }
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'marca_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                    };

                    marcaService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getMarcas() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                } ;

            // ===================================================================================================

            //  New rol
                function newMarca (size)
                {
                        var last_page = Math.ceil(vm.tableParams.total() / vm.tableParams.count()) ;
                        vm.tableParams.page( last_page );
                        vm.tableParams.reload();
                        vm.tableParams.sorting({}) ;

                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/vehiculos/marcas/new.marca.tpl.html',
                            controller: 'ModalNewMarcaCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getMarcas(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        marcaService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getMarcas() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    // console.log(row) ;
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Marca',
                        bodyText: '¿Esta Seguro de Eliminar Marca: '+ row.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;


(function(){
    'use stric' ;

    angular.module('modelos.info.controller').controller('ModalNewModeloCrtl', ModalNewModeloCrtl) ;
    ModalNewModeloCrtl.$inject = ['$uibModalInstance', 'marcaService','modeloService'] ;

        function ModalNewModeloCrtl($uibModalInstance, marcaService,modeloService)
        {
            var vm =  this ;

            vm.msj = "";
            vm.data_marcas = [] ;

            vm.formData = {
                nombre : '',
                descripcion : '',
                clase_vehiculo : [],
            }

            init();
            function init(){
                getMarcas() ;
            } ;

            function getMarcas()
            {
                marcaService.getMarcas().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_marcas = response.data ;
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                    'marca_id'     : vm.formData.marca.id,

                };
                // console.log(data);
                modeloService.save(data).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use strict';

    angular.module('modelos.info.controller').controller('ModelosInfoCtrl',ModelosInfoCtrl);
    ModelosInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','modeloService','marcaService'] ;

        function ModelosInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,modeloService,marcaService)
        {

            var vm = this ;

            // function
                vm.getModelos = getModelos ;
                vm.onClick    = onClick ;
                vm.newModelo   = newModelo ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;
                vm.data_marcas = [];

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                getMarcas() ;
                vm.getModelos() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getModelos();
                }
                else if (name === 'new')
                {
                      vm.newModelo('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }
                else if (name === 'delete')
                {
                    deleteRol() ;
                }
                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            }

            function getModelos()
            {
                modeloService.getModelos().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            function getMarcas()
            {
                marcaService.getMarcas().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_marcas = response.data ;
                            // console.log(vm.data_areas );
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            }


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'modelo_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                        'marca_id' : row.marca.id,
                    };

                    modeloService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getModelos() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newModelo (size)
                {
                        var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/vehiculos/modelos/new.modelo.tpl.html',
                            controller: 'ModalNewModeloCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getModelos(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        modeloService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getModelos() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Modelo',
                        bodyText: '¿Esta Seguro de Eliminar Modelo: '+ row.nombre +' de la Marca : '+row.marca.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }
        }
})() ;


(function(){
    'use stric' ;

    angular.module('tipoVehiculos.info.controller').controller('ModalNewTipoVehiculoCrtl', ModalNewTipoVehiculoCrtl) ;
    ModalNewTipoVehiculoCrtl.$inject = ['$uibModalInstance', 'claseVehiculoService','tipoVehiculoService'] ;

        function ModalNewTipoVehiculoCrtl($uibModalInstance, claseVehiculoService,tipoVehiculoService)
        {
            var vm =  this ;

            vm.msj = "";
            vm.data_clase_vehiculos = [] ;

            vm.formData = {
                nombre : '',
                descripcion : '',
                clase_vehiculo : [],
            }

            init();
            function init(){
                getClaseVehiculos() ;
            } ;

            function getClaseVehiculos()
            {
                claseVehiculoService.getClaseVehiculos().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_clase_vehiculos = response.data ;
                            // console.log(vm.data_clase_vehiculos );
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            }

            vm.ok = function ()
            {
                var data = {
                    'nombre'       : vm.formData.nombre,
                    'descripcion'  : vm.formData.descripcion,
                    'clase_vehiculo_id'      : vm.formData.clase_vehiculo.id,

                };
                // console.log(data);
                tipoVehiculoService.save(data).then(
                    function(response)
                    {
                        // console.log(response);

                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use strict';

    angular.module('tipoVehiculos.info.controller').controller('TipoVehiculosInfoCtrl',TipoVehiculosInfoCtrl);
    TipoVehiculosInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','tipoVehiculoService','claseVehiculoService'] ;

        function TipoVehiculosInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,tipoVehiculoService,claseVehiculoService)
        {

            var vm = this ;

            // function
                vm.getTipoVehiculos = getTipoVehiculos ;
                vm.onClick          = onClick ;
                vm.newTipoVehiculo  = newTipoVehiculo ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;
                vm.data_tipo_vehiculos = [];

                vm.btn_edit     = false ;
                vm.btn_delete   = false ;
                vm.btn_in_table = false ;


            init();
            function init() {
                tablePlugin() ;
                getClaseVehiculos() ;
                vm.getTipoVehiculos() ;

            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getTipoVehiculos();
                }
                else if (name === 'new')
                {
                      vm.newTipoVehiculo('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editRow(row)
                }
                else if (name === 'delete')
                {
                    deleteRol() ;
                }
                else{
                    return ;
                };
            } ;


            function botonesInTable()
            {
               vm.botones =  botoneraFactory.getBotonera() ;
               vm.btn_in_table = true ;

                for ( var i in vm.botones){
                    if(vm.botones[i].glosa === 'intable')
                    {
                         vm.btn_in_table = true ;
                        if (vm.botones[i].valor === 'edit')
                        {
                            vm.btn_edit = true;
                        };

                        if(vm.botones[i].valor === 'delete')
                        {
                             vm.btn_delete = true;
                        }
                    }
                }
            }

            function getTipoVehiculos()
            {
                tipoVehiculoService.getTipoVehiculos().then(
                    function(response){
                        if (!response.error)
                        {
                            botonesInTable() ;
                            vm.data_list = response.data;
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            function getClaseVehiculos()
            {
                claseVehiculoService.getClaseVehiculos().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_tipo_vehiculos = response.data ;
                            // console.log(vm.data_areas );
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            }


            // ===== ng-table ==============================================================================================
                vm.cancel = cancel;
                vm.del    = del;
                vm.save   = save;
                vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'tipo_vehiculo_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                        'clase_vehiculo_id' : row.clase_vehiculo.id,
                    };

                    tipoVehiculoService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getTipoVehiculos() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                }

            // ===================================================================================================

            //  New rol
                function newTipoVehiculo (size)
                {
                        var path = PATH.INFORMACION ;

                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/vehiculos/tipo-vehiculos/new.tipo-vehiculo.tpl.html',
                            controller: 'ModalNewTipoVehiculoCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getTipoVehiculos(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete
                function confirmDelete(row)
                {
                     var data = {
                            'codigo'    : row.id,
                            'estado'    : 0,
                        };

                        tipoVehiculoService.updateEstado(data).then(
                            function(response){
                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getTipoVehiculos() ;
                                    return response.data;
                                }
                            }
                        );
                }

                function modalConfirm(row)
                {
                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Tipo de Vehiculo',
                        bodyText: '¿Esta Seguro de Eliminar Tipo de Vehiculo: '+ row.nombre +' de la Clase : '+row.clase_vehiculo.nombre+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete(row);
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }
        }
})() ;


(function(){
    'use stric' ;

    angular.module('vehiculos.info.controller').controller('ModalEditVehiculoCrtl', ModalEditVehiculoCrtl) ;
    ModalEditVehiculoCrtl.$inject = [
                                        '$uibModalInstance',
                                        'data_vehiculo',
                                        'vehiculoService',
                                        'claseVehiculoService',
                                        'tipoVehiculoService',
                                        'marcaService',
                                        'modeloService',
                                    ] ;

        function ModalEditVehiculoCrtl(
                $uibModalInstance,
                data_vehiculo,
                vehiculoService,
                claseVehiculoService,
                tipoVehiculoService,
                marcaService,
                modeloService )
        {
            var vm =  this ;

            vm.msj = "";
            vm.fillSelected   = data_vehiculo ;
            console.log(vm.fillSelected);

            vm.clase_vehiculo_id = vm.fillSelected.clase_vehiculo_id ;
            vm.tipo_vehiculo_id  = vm.fillSelected.tipo_vehiculo_id ;
            vm.marca_id          = vm.fillSelected.marca_id ;
            vm.modelo_id         = vm.fillSelected.modelo_id ;

            vm.formData = {
                placa : vm.fillSelected.placa,
                serie : vm.fillSelected.serie,
                descripcion : vm.fillSelected.descripcion,
                clase_vehiculo : [],
                tipo_vehiculo : [],
                marca : [],
                modelo : [],
            };

            vm.data_clase_vehiculos = [] ;
            vm.data_tipo_vehiculos  = [] ;
            vm.data_marcas          = [] ;
            vm.data_modelos         = [] ;

            vm.getClaseVehiculos = getClaseVehiculos ;
            vm.getTipoVehiculos  = getTipoVehiculos ;
            vm.getMarcas         = getMarcas ;
            vm.getModelos        = getModelos ;

            init();
            function init()
            {
                vm.getClaseVehiculos();
                vm.getMarcas();
                setTimeout(getMarcas(), 500);
                setTimeout( vm.getTipoVehiculos(undefined,undefined), 750);
                setTimeout(vm.getModelos(undefined,undefined), 1000);


            } ;

            function getClaseVehiculos()
            {
                claseVehiculoService.getClaseVehiculos().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_clase_vehiculos = response.data ;
                            vm.formData.clase_vehiculo = selectItemCombo(vm.data_clase_vehiculos,vm.clase_vehiculo_id ) ;
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );
            };

            function getTipoVehiculos($item,$model)
            {
                var params = { 'clase_vehiculo_id' : ''} ;
                if ($item === undefined)
                {
                    params.clase_vehiculo_id =  vm.clase_vehiculo_id ;
                }else{
                    params.clase_vehiculo_id =  vm.formData.clase_vehiculo.id ;
                } ;

                tipoVehiculoService.getTipoVehiculosByClaseVehiculoId(params).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_tipo_vehiculos = response.data ;
                            if (vm.tipo_vehiculo_id !== undefined)
                            {
                                vm.formData.tipo_vehiculo = selectItemCombo(vm.data_tipo_vehiculos,vm.tipo_vehiculo_id ) ;
                                vm.tipo_vehiculo_id = [];

                            };
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );
            };

            function getMarcas()
            {
                marcaService.getMarcas().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_marcas = response.data ;
                            vm.formData.marca = selectItemCombo(vm.data_marcas,vm.marca_id ) ;
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );
            };

            function getModelos($item,$model)
            {
                var params = { 'marca_id' : ''} ;
                if ($item === undefined)
                {
                    params.marca_id =  vm.marca_id ;
                }else{
                    params.marca_id =  vm.formData.marca.id ;
                } ;

                modeloService.getModelosByMarcaId(params).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_modelos = response.data ;
                            if (vm.modelo_id !== undefined)
                            {
                                vm.formData.modelo = selectItemCombo(vm.data_modelos,vm.modelo_id ) ;
                                vm.modelo_id = [];
                            };
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );
            };

            // seleccionar un ITEM del un combo
            function selectItemCombo(data, id_select)
            {
                var idSelected = parseInt(id_select);
                var out = [] ;
                for(var i in data)
                {
                    var id =  parseInt(data[i].id);
                    if( id === idSelected)
                    {
                       out = data[i] ;
                       break ;
                    }
                }
                return out ;
            };


            vm.ok = function ()
            {
                    var vehiculo_id      = vm.fillSelected.id;
                    var placa            = vm.formData.placa ;
                    var serie            = vm.formData.serie ;
                    var descripcion      = vm.formData.descripcion ;
                    var tipo_vehiculo_id = vm.formData.tipo_vehiculo.id ;
                    var modelo_id        = vm.formData.modelo.id ;

                    // var clase_vehiculo = vm.formData.clase_vehiculo.id ;
                    // var marca          = vm.formData.marca.id ;

                    vm.msj = {message : ''} ;

                    if (placa === undefined)
                    {
                        return  vm.msj.message = 'Ingrese Placa' ;
                    };
                    if (serie === undefined)
                    {
                        return  vm.msj.message = 'Ingrese Serie' ;
                    };
                    if (tipo_vehiculo_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Tipo Vehiculo' ;
                    };
                    if (modelo_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Modelo' ;
                    };

                    var params = {
                      'vehiculo_id' : vehiculo_id,
                      'placa' : placa,
                      'serie' : serie,
                      'descripcion' : descripcion,
                      'tipo_vehiculo_id' : tipo_vehiculo_id,
                      'modelo_id' : modelo_id,
                    };

                vehiculoService.update(params).then(
                    function(response)
                    {
                        console.log(response);
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use stric' ;

    angular.module('vehiculos.info.controller').controller('ModalNewVehiculoCrtl', ModalNewVehiculoCrtl) ;
    ModalNewVehiculoCrtl.$inject = [
                                        '$uibModalInstance',
                                        'vehiculoService',
                                        'claseVehiculoService',
                                        'tipoVehiculoService',
                                        'marcaService',
                                        'modeloService',
                                    ] ;

        function ModalNewVehiculoCrtl(
                $uibModalInstance,
                vehiculoService,
                claseVehiculoService,
                tipoVehiculoService,
                marcaService,
                modeloService )
        {
            var vm =  this ;

            vm.msj = "";

            vm.formData = {
                placa : '',
                serie : '',
                descripcion : '',
                clase_vehiculo : [],
                tipo_vehiculo : [],
                marca : [],
                modelo : [],
            };

            vm.data_clase_vehiculos = [] ;
            vm.data_tipo_vehiculos  = [] ;
            vm.data_marcas          = [] ;
            vm.data_modelos         = [] ;

            vm.getClaseVehiculos = getClaseVehiculos ;
            vm.getTipoVehiculos  = getTipoVehiculos ;
            vm.getMarcas         = getMarcas ;
            vm.getModelos        = getModelos ;

            init();
            function init()
            {
                vm.getClaseVehiculos() ;
                vm.getMarcas() ;

            }

            function getClaseVehiculos()
            {
                claseVehiculoService.getClaseVehiculos().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_clase_vehiculos = response.data ;
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );
            };

            function getTipoVehiculos($item,$model)
            {
                // console.log($item);
                // console.log($model);
                vm.formData.tipo_vehiculo = [] ;
                var params = { 'clase_vehiculo_id' : vm.formData.clase_vehiculo.id} ;

                tipoVehiculoService.getTipoVehiculosByClaseVehiculoId(params).then(
                    function(response)
                    {
                        console.log(response.data);
                        if (!response.error)
                        {
                            vm.data_tipo_vehiculos = response.data ;
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );
            };

            function getMarcas()
            {
                marcaService.getMarcas().then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_marcas = response.data ;
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );
            };

            function getModelos($item,$model)
            {
                vm.formData.modelo = [] ;
                var params = { 'marca_id' : vm.formData.marca.id} ;
                modeloService.getModelos(params).then(
                    function(response)
                    {
                        if (!response.error)
                        {
                            vm.data_modelos = response.data ;
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );
            };


            vm.ok = function ()
            {
                    var placa            = vm.formData.placa ;
                    var serie            = vm.formData.serie ;
                    var descripcion      = vm.formData.descripcion ;
                    var tipo_vehiculo_id = vm.formData.tipo_vehiculo.id ;
                    var modelo_id        = vm.formData.modelo.id ;

                    // var clase_vehiculo = vm.formData.clase_vehiculo.id ;
                    // var marca          = vm.formData.marca.id ;

                    vm.msj = {message : ''} ;

                    if (placa === undefined)
                    {
                        return  vm.msj.message = 'Ingrese Placa' ;
                    };
                    if (serie === undefined)
                    {
                        return  vm.msj.message = 'Ingrese Serie' ;
                    };
                    if (tipo_vehiculo_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Tipo Vehiculo' ;
                    };
                    if (modelo_id === undefined)
                    {
                        return  vm.msj.message = 'Seleccione Modelo' ;
                    };

                    var params = {
                      'placa' : placa,
                      'serie' : serie,
                      'descripcion' : descripcion,
                      'tipo_vehiculo_id' : tipo_vehiculo_id,
                      'modelo_id' : modelo_id,
                    };


                vehiculoService.save(params).then(
                    function(response)
                    {
                        console.log(response);
                        if (!response.error)
                        {
                            $uibModalInstance.close(vm.formData);
                        }else
                        {
                            vm.msj = response ;
                        }
                    }
                );

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        };
})() ;
(function(){
    'use strict';

    angular.module('vehiculos.info.controller').controller('VehiculosInfoCtrl',VehiculosInfoCtrl);
    VehiculosInfoCtrl.$inject = ['$filter', 'botoneraFactory', '$uibModal', 'PATH', 'modalService','NgTableParams','vehiculoService'] ;

        function VehiculosInfoCtrl($filter, botoneraFactory, $uibModal, PATH, modalService,NgTableParams,vehiculoService)
        {
            var vm = this ;

            // function
                vm.getVehiculos = getVehiculos ;
                vm.onClick      = onClick ;
                vm.newVehiculo  = newVehiculo ;
                vm.editVehiculo = editVehiculo ;
                vm.deleteVehiculo = deleteVehiculo ;

            // variables
                vm.data_list    = [] ;
                vm.botones      = [] ;
                vm.fillSelected = [] ;

            init();
            function init() {
                tablePlugin() ;
                vm.getVehiculos() ;
            }

            function onClick(name, row)
            {
                if (name === 'list')
                {
                    vm.getVehiculos();
                }
                else if (name === 'new')
                {
                      vm.newVehiculo('md') ;

                }
                else if (name === 'edit')
                {
                      vm.editVehiculo();
                }
                else if (name === 'delete')
                {
                      vm.deleteVehiculo();
                }


                else{
                    return ;
                };
            } ;

            function getVehiculos()
            {
                vehiculoService.getVehiculos().then(
                    function(response){
                        if (!response.error)
                        {
                            vm.data_list = response.data;
                            console.log(vm.data_list);
                            reloadNgTable() ;
                            return vm.data_list ;
                        }
                    }
                );
            }


            // ===== ng-table ==============================================================================================
                // vm.cancel = cancel;
                // vm.del    = del;
                // vm.save   = save;
                // vm.editRow = editRow ;
                vm.applyGlobalSearch = applyGlobalSearch;

                function tablePlugin()
                {
                       vm.tableParams =  new NgTableParams({
                            page: 1,
                            count: 10,
                            sorting: { nombre: "asc" }
                        }, {
                            // debugMode: true,
                            total: vm.data_list.length,
                            getData: function($defer, params) {
                                var orderedData = params.sorting() ? $filter('orderBy')(vm.data_list, params.orderBy()) : data;
                                orderedData = $filter('filter')(orderedData, params.filter());
                                params.total(orderedData.length);
                                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                            }
                        });
                }

                /*function cancel(row, rowForm) {
                  var originalRow = resetRow(row, rowForm);
                  angular.extend(row, originalRow);
                }

                function del(row) {
                    modalConfirm(row);
                }

                function resetRow(row, rowForm){
                  row.isEditing = false;
                  rowForm.$setPristine();
                   for ( var i in vm.data_list){
                        if(vm.data_list[i].id === row.id){
                            return vm.data_list[i]
                        }
                    }
                };

                function save(row, rowForm)
                {
                  var data = {
                        'marca_id' : row.id,
                        'nombre' : row.nombre,
                        'descripcion' : row.descripcion,
                    };

                    vehiculoService.update(data).then(
                        function(response)
                        {
                            if (!response.error)
                            {
                                var originalRow = resetRow(row, rowForm);
                                angular.extend(originalRow, row);
                                vm.getVehiculos() ;
                            }
                            else
                            {
                                row.isEditing = true;
                            }
                        }
                    );
                }

                function editRow(row)
                {
                     row.isEditing = true ;
                }*/

                function reloadNgTable()
                {
                    vm.tableParams.reload().then(function(data) {
                        if (data.length === 0 && vm.tableParams.total() > 0) {
                          vm.tableParams.page(vm.tableParams.page() - 1);
                          vm.tableParams.reload();
                        }
                    });
                } ;

                function applyGlobalSearch(){
                  var term = vm.globalSearchTerm;
                  vm.tableParams.filter({ $: term });
                } ;

            // ===================================================================================================

            //  New
                function newVehiculo (size)
                {
                        var last_page = Math.ceil(vm.tableParams.total() / vm.tableParams.count()) ;
                        vm.tableParams.page( last_page );
                        vm.tableParams.reload();
                        vm.tableParams.sorting({}) ;

                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/vehiculos/vehiculos/new.vehiculo.tpl.html',
                            controller: 'ModalNewVehiculoCrtl',
                            controllerAs: 'vm',
                            size: size,
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getVehiculos(data.nombre) ;
                            },
                            function ()
                            {
                                // console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };

            //  Editar

                function editVehiculo()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalEditVehiculo('md') ;
                    }
                };

                function modalEditVehiculo(size)
                {
                        var path = PATH.INFORMACION ;
                        var modalInstance = $uibModal.open({
                            templateUrl: path+'/vehiculos/vehiculos/edit.vehiculo.tpl.html',
                            controller: 'ModalEditVehiculoCrtl',
                            controllerAs: 'vm',
                            size: size,
                             backdrop : 'static',
                            resolve: {
                                data_vehiculo: function () { return vm.fillSelected; },
                            }
                        });

                        modalInstance.result.then(
                            function (data)
                            {
                                vm.getVehiculos() ;
                            },
                            function ()
                            {
                                console.log('Modal dismissed at: ' + new Date());
                            }
                        );
                };


            //delete

                function deleteVehiculo()
                {
                    var cod = vm.fillSelected.id ;

                    if (cod === 0 || cod === undefined) {
                        return modalAlert()
                    }
                    else{
                        modalConfirm() ;
                    }

                }

                function confirmDelete()
                {
                     var data = {
                            'codigo'    : vm.fillSelected.id,
                            'estado'    : 0,
                        };
                        // console.log(data);
                        vehiculoService.updateEstado(data).then(
                            function(response){

                                if (!response.error)
                                {
                                    vm.fillSelected = [] ;
                                    vm.getVehiculos() ;
                                    return response.data;
                                }
                            }
                        );
                };

                function modalConfirm()
                {
                    // var dato = row.placa + ' '+ row.serie + ' '+row.tipo_Vehiculo_nombre ;
                    var dato = vm.fillSelected.placa+ ' / ' + vm.fillSelected.modelo+ ' / ' + vm.fillSelected.tipo_vehiculo ;

                    var modalOptions = {
                        closeButtonText: 'Cancelar',
                        actionButtonText: 'Eliminar',
                        headerText: 'Eliminar Vehículo',
                        bodyText: '¿Esta Seguro de Eliminar Vehículo: '+ dato+' ?'
                    };

                   modalService.showModalConfirm({}, modalOptions).then(function (result) {
                        confirmDelete();
                    });
                }

                function modalAlert()
                {
                    modalService.showModalAlert({}, {}).then(function (result){
                    });
                }

        }
})() ;


(function(){
    'use strict';
    angular
        .module('accesos.service')
        .service('accesosService', accesosService);

        accesosService.$inject = ['$http'];

        function accesosService($http){

            this.getAccesos            = getAccesos ;
            this.getControlAccesosUser = getControlAccesosUser ;
            this.updateAccesosUser     = updateAccesosUser ;
            this.updateUserRol         = updateUserRol ;

            var path = 'web/accesos' ;

            function getAccesos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getControlAccesosUser(params_){
                var url_ = path+'/user';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateAccesosUser(params_){
                var url_ = path+'/user/update-accesos';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateUserRol(params_){
                var url_ = path+'/user/update-rol';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();
var appHttp =(function(window, undefined){
    // perticiones http desde cualquier nivel
       var getHttp = function($http, url_ , params_){
            return $http.get(url_, params_)
                    .then(getServiceComplete)
                    .catch(getServiceFailed) ;
        }

        var postHttp = function($http, url_ , params_){
            return $http.post(url_, params_)
                    .then(getServiceComplete)
                    .catch(getServiceFailed) ;
        }

        function getServiceComplete(response){
            return response.data ;
        }

        function getServiceFailed(errors ){
            var data = [{message:'timeService.getPostsFaild:',status: errors.status, statusText:errors.statusText,error:true}];
            console.log(data) ;
            // console.log(errors) ;
            return data ;

        }

        return {
            getHttp: getHttp,
            postHttp : postHttp
          }
})(window);
(function(){
    'use strict';
    angular
        .module('control.service')
        .service('controlService', controlService);

        controlService.$inject = ['$http'];

        function controlService($http)
        {
            this.getControles = getControles ;
            // this.save         = save ;
            // this.update       = update ;
            // this.deleteFill   = deleteFill ;


            var path = 'web/controles' ;

            function getControles(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            /*function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function deleteFill(params_){
                var url_ = path+'/delete';
                return appHttp.postHttp($http,url_, params_);
            };*/



        }
})();
(function(){
    'use strict';
    angular
        .module('rol.service')
        .service('rolService', rolService);

        rolService.$inject = ['$http'];

        function rolService($http){

            this.getRoles         = getRoles ;
            this.getRolByNombre   = getRolByNombre ;
            this.saveRol          = saveRol ;
            this.getRolById       = getRolById ;
            this.updateRol        = updateRol ;
            this.updateEstado     = updateEstado ;
            this.getAccesosRol    = getAccesosRol ;
            this.updateAccesosRol = updateAccesosRol ;


            var path = 'web/roles' ;

            function getRoles(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getRolByNombre(params_){
                var url_ = path+'/get/bynombre';
                return appHttp.postHttp($http,url_, params_);
            };

            function saveRol(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getRolById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateRol(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function getAccesosRol(params_){
                var url_ = path+'/accesos';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateAccesosRol(params_){
                var url_ = path+'/update/accesos';
                return appHttp.postHttp($http,url_, params_);
            };




        }
})();
(function(){
    'use strict';
    angular
        .module('ubigeo.service')
        .service('ubigeoService', ubigeoService);

        ubigeoService.$inject = ['$http'];

        function ubigeoService($http){

            this.getUbigeos         = getUbigeos ;

            var path = 'web/ubigeos' ;

            function getUbigeos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            /*
            function getRolByNombre(params_){
                var url_ = path+'/get/bynombre';
                return appHttp.postHttp($http,url_, params_);
            };
            */




        }
})();
(function(){
    'use strict';
    angular
        .module('users.service')
        .service('usersService', usersService);

        usersService.$inject = ['$http'];

        function usersService($http){

            this.getUsers     = getUsers ;
            this.getInfo      = getInfo ;
            this.save         = save ;
            this.updateEstado = updateEstado ;

            var path = 'web/users' ;

            function getUsers(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getInfo(){
                var url_ = path+'/info';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();
(function(){
    'use strict';
    angular
        .module('modal.service')
        .service('modalService', modalService);

        modalService.$inject = ['$uibModal'];

        function modalService($uibModal)
        {
            var modalDefaults = {
                backdrop: true,
                keyboard: true,
                modalFade: true,
                templateUrl: '../partials/app/modal/modal.tpl.html'
            };

            var modalOptions = {
                closeButtonText: 'Cancelar',
                actionButtonText: 'OK',
                headerText: 'Accion',
                bodyText: 'Deseo realizar esta acción?'
            };

            this.showModal = function (customModalDefaults, customModalOptions, templateUrl) {

                if (templateUrl) modalDefaults.templateUrl = templateUrl;
                if (!customModalDefaults) customModalDefaults = {};
                customModalDefaults.backdrop = 'static';
                return this.show(customModalDefaults, customModalOptions);
            };

            /*
                Mostrar modal por defecto
                Params :
                modalOptions.class = success, info ,warning , danger
                modalOptions.bodyText = mense a mostrar
            */
            this.showModalAlert = function (customModalDefaults, customModalOptions, templateUrl)
            {
                templateUrl || ( templateUrl = '../partials/app/modal/alert.tpl.html');
                modalDefaults.templateUrl = templateUrl ;

                modalOptions.bodyText = customModalOptions.bodyText ? customModalOptions.bodyText : 'Seleccionar Registro'

                if (!customModalDefaults) customModalDefaults = {};
                customModalDefaults.backdrop = true;
                return this.show(customModalDefaults, customModalOptions);
            };

            this.showModalConfirm = function (customModalDefaults, customModalOptions, templateUrl)
            {
                templateUrl || ( templateUrl = '../partials/app/modal/confirm.tpl.html');
                modalDefaults.templateUrl = templateUrl ;

                modalOptions.bodyText = customModalOptions.bodyText ? customModalOptions.bodyText : '¿Desea Eliminar el Registro?'

                if (!customModalDefaults) customModalDefaults = {};
                customModalDefaults.backdrop = 'static';
                return this.show(customModalDefaults, customModalOptions);
            };


            this.show = function (customModalDefaults, customModalOptions) {
                //Create temp objects to work with since we're in a singleton service
                var tempModalDefaults = {};
                var tempModalOptions = {};

                //Map angular-ui modal custom defaults to modal defaults defined in service
                angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);

                //Map modal.html $scope custom properties to defaults defined in service
                angular.extend(tempModalOptions, modalOptions, customModalOptions);

                if (!tempModalDefaults.controller) {
                    tempModalDefaults.controller = function ($scope, $uibModalInstance) {
                        $scope.modalOptions = tempModalOptions;
                        $scope.modalOptions.ok = function (result) {
                            $uibModalInstance.close(result);
                        };
                        $scope.modalOptions.close = function (result) {
                            $uibModalInstance.dismiss('cancel');
                        };

                    }

                    tempModalDefaults.controller.$inject = ['$scope', '$uibModalInstance'];
                }

                return $uibModal.open(tempModalDefaults).result;
            };


        }
})();

(function(){
    'use strict';
    angular
        .module('aval.service')
        .service('avalService', avalService);

        avalService.$inject = ['$http'];

        function avalService($http)
        {

            this.getAvalesInfoBasica    = getAvalesInfoBasica ;


            var path = 'web/avales' ;

            function getAvalesInfoBasica(params_){
                var url_ = path+'/get/info-basica';
                return appHttp.getHttp($http,url_, params_);
            };


        }
})();
(function(){
    'use strict';
    angular
        .module('cliente.service')
        .service('clienteService', clienteService);

        clienteService.$inject = ['$http'];

        function clienteService($http){

            this.getClientes    = getClientes ;
            this.save           = save ;
            this.getClienteById = getClienteById ;
            this.update         = update ;
            this.updateEstado   = updateEstado ;

            this.getClientesInfoBasica    = getClientesInfoBasica ;

            this.updateNewAgenteComercial    = updateNewAgenteComercial ;

            var path = 'web/clientes' ;

            function getClientes(params_){
                 var url_ = path+'';
                return appHttp.postHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getClienteById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateNewAgenteComercial(params_){
                var url_ = path+'/update/agente-comercial';
                return appHttp.postHttp($http,url_, params_);
            };

            function getClientesInfoBasica(params_){
                var url_ = path+'/get/info-basica';
                return appHttp.postHttp($http,url_, params_);
            };

         }
})();
(function(){
    'use strict';
    angular
        .module('empleado.service')
        .service('empleadoService', empleadoService);

        empleadoService.$inject = ['$http'];

        function empleadoService($http){

            this.getEmpleados           = getEmpleados ;
            this.save                   = save ;
            this.getEmpleadoById        = getEmpleadoById ;
            this.update                 = update ;
            this.updateEstado           = updateEstado ;
            this.asignarNewCargo        = asignarNewCargo ;
            this.getEmpleadosInfoBasica = getEmpleadosInfoBasica ;

            var path = 'web/empleados' ;

            function getEmpleados(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getEmpleadoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function asignarNewCargo(params_){
                var url_ = path+'/asignar/new-cargo';
                return appHttp.postHttp($http,url_, params_);
            };

            function getEmpleadosInfoBasica(params_){
                var url_ = path+'/get/info-basica';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

        }
})();
(function(){
    'use strict';
    angular
        .module('perDocumento.service')
        .service('perDocumentoService', perDocumentoService);

        perDocumentoService.$inject = ['$http'];

        function perDocumentoService($http)
        {
            this.update     = update ;
            this.save       = save ;
            this.deleteFill = deleteFill ;

            var path = 'web/documentos' ;

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function deleteFill(params_){
                var url_ = path+'/delete';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();
(function(){
    'use strict';
    angular
        .module('perJuridica.service')
        .service('perJuridicaService', perJuridicaService);

        perJuridicaService.$inject = ['$http'];

        function perJuridicaService($http)
        {

            this.getPerJuridicas          = getPerJuridicas ;
            this.getPerJuridicaInfoAll    = getPerJuridicaInfoAll ;
            this.getPerJuridicaInfoBasica = getPerJuridicaInfoBasica ;
            this.save                     = save ;
            this.update                   = update ;
            this.updateEstado             = updateEstado ;
            this.getPerJuridicaById       = getPerJuridicaById ;
            this.getPerJuridicaByRuc      = getPerJuridicaByRuc ; //
            this.updateRuc                = updateRuc ;
            this.updatePerJuridicaInfo    = updatePerJuridicaInfo ;


            var path = 'web/personas/empresas' ;

            function getPerJuridicas(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getPerJuridicaInfoAll(params_){
               var url_ = path+'/get/infoall';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPerJuridicaInfoBasica(){
                var url_ = path+'/get/info-basica';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPerJuridicaById(params_){
                var url_ = path+'/get/id';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPerJuridicaByRuc(params_){
                var url_ = path+'/get/ruc';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateRuc(params_){
                var url_ = path+'/update/ruc';
                return appHttp.postHttp($http,url_, params_);
            };

            function updatePerJuridicaInfo(params_){
                var url_ = path+'/update/info';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();
(function(){
    'use strict';
    angular
        .module('perMail.service')
        .service('perMailService', perMailService);

        perMailService.$inject = ['$http'];

        function perMailService($http)
        {

            this.getMailsByPersonaId = getMailsByPersonaId ;
            this.save                = save ;
            this.update              = update ;
            this.deleteFill          = deleteFill ;
            this.reOrderItems        = reOrderItems ;


            var path = 'web/mails' ;

            function getMailsByPersonaId(params_){
                var url_ = path+'/get/persona/id';
                return appHttp.postHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function deleteFill(params_){
                var url_ = path+'/delete';
                return appHttp.postHttp($http,url_, params_);
            };

            function reOrderItems(params_){
                var url_ = path+'/reorder-items';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();
(function(){
    'use strict';
    angular
        .module('perTelefono.service')
        .service('perTelefonoService', perTelefonoService);

        perTelefonoService.$inject = ['$http'];

        function perTelefonoService($http)
        {

            this.save                    = save ;
            this.update                  = update ;
            this.updateEstado            = updateEstado ;
            this.deleteFill              = deleteFill ;
            this.getPerTelefonoById      = getPerTelefonoById ;
            this.getPerTelefonoByNumero  = getPerTelefonoByNumero ;
            this.getTelefonosByPersonaId = getTelefonosByPersonaId ;
            this.reOrderItems            = reOrderItems ;


            var path = 'web/telefonos' ;

            function getTelefonosByPersonaId(params_){
                var url_ = path+'/get/persona/id';
                return appHttp.postHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function deleteFill(params_){
                var url_ = path+'/delete';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPerTelefonoById(params_){
                var url_ = path+'/get/id';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPerTelefonoByNumero(params_){
                var url_ = path+'/get/numero';
                return appHttp.postHttp($http,url_, params_);
            };

            function reOrderItems(params_){
                var url_ = path+'/reorder-items';
                return appHttp.postHttp($http,url_, params_);
            };
        }
})();
(function(){
    'use strict';
    angular
        .module('perWeb.service')
        .service('perWebService', perWebService);

        perWebService.$inject = ['$http'];

        function perWebService($http)
        {

            this.getWebsByPersonaId = getWebsByPersonaId ;
            this.save               = save ;
            this.update             = update ;
            this.getPerWebById      = getPerWebById ;
            this.deleteFill         = deleteFill ;
            this.updateEstado       = updateEstado ;
            this.reOrderItems       = reOrderItems ;

            this.getTipoWebs = getTipoWebs ;

            var path = 'web/webs' ;

            function getWebsByPersonaId(params_){
                var url_ = path+'/get/persona/id';
                return appHttp.postHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function deleteFill(params_){
                var url_ = path+'/delete';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPerWebById(params_){
                var url_ = path+'/get/id';
                return appHttp.postHttp($http,url_, params_);
            };


            function reOrderItems(params_){
                var url_ = path+'/reorder-items';
                return appHttp.postHttp($http,url_, params_);
            };

            // tipos de webs
            function getTipoWebs(){
                var url_ = path+'/get/tipo-webs';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };
        }
})();
(function(){
    'use strict';
    angular
        .module('personaNatural.service')
        .service('personaNaturalService', personaNaturalService);

        personaNaturalService.$inject = ['$http'];

        function personaNaturalService($http)
        {

            this.getPersonasNaturales      = getPersonasNaturales ;
            this.getPerNaturalInfoAll      = getPerNaturalInfoAll ;
            this.getPerNaturalInfoBasica   = getPerNaturalInfoBasica ;
            this.save                      = save ;
            this.update                    = update ;
            this.updateEstado              = updateEstado ;
            this.getPerNaturalById         = getPerNaturalById ;
            this.getPerNaturalByDni        = getPerNaturalByDni ;
            this.updateDni                 = updateDni ;
            this.updatePerNatInfo          = updatePerNatInfo ;
            this.getPersonasByTipoRelacion = getPersonasByTipoRelacion ;

            var path = 'web/personas/naturales' ;

            function getPersonasNaturales(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getPerNaturalInfoAll(params_){
               var url_ = path+'/get/infoall';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPerNaturalInfoBasica(){
                var url_ = path+'/get/info-basica';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPerNaturalById(params_){
                var url_ = path+'/get/id';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPerNaturalByDni(params_){
                var url_ = path+'/get/dni';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateDni(params_){
                var url_ = path+'/update/dni';
                return appHttp.postHttp($http,url_, params_);
            };

            function updatePerNatInfo(params_){
                var url_ = path+'/update/info';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPersonasByTipoRelacion(params_){
                var url_ = path+'/by/tipo-relacion';
                return appHttp.postHttp($http,url_, params_);
            };







        }
})();
(function(){
    'use strict';
    angular
        .module('persona.service')
        .service('personaService', personaService);

        personaService.$inject = ['$http'];

        function personaService($http)
        {

            this.getPersonasInfoBasica    = getPersonasInfoBasica ;


            var path = 'web/personas' ;

            function getPersonasInfoBasica(params_){
                var url_ = path+'/info-basica';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();
(function(){
    'use strict';
    angular
        .module('creditoMenor.service')
        .service('creditoMenorService', creditoMenorService);

        creditoMenorService.$inject = ['$http'];

        function creditoMenorService($http){

            this.getPrestamos                   = getPrestamos ;
            this.save                           = save ;
            this.getPrestamoById                = getPrestamoById ;
            this.update                         = update ;
            this.updateEstadoPrestamoTipoEstado = updateEstadoPrestamoTipoEstado ;
            this.getPrestamosByTipoEstadoId     = getPrestamosByTipoEstadoId ;
            this.updatePrestamoTipoEstado       = updatePrestamoTipoEstado ;

            var path = 'operaciones/credito-menor' ;

            function getPrestamos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPrestamoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstadoPrestamoTipoEstado(params_){
                var url_ = path+'/update/estado-all';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPrestamosByTipoEstadoId(params_){
                var url_ = path+'/tipo-estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function updatePrestamoTipoEstado(params_){
                var url_ = path+'/update/prestamo-tipo-estado';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();
(function(){
    'use strict';
    angular
        .module('cuota.service')
        .service('cuotaService', cuotaService);

        cuotaService.$inject = ['$http'];

        function cuotaService($http){

            this.getCuotasByPrestamoId    = getCuotasByPrestamoId ;

            var path = 'operaciones/cuotas' ;


            function getCuotasByPrestamoId(params_){
                var url_ = path+'/get/by-prestamo-id';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();
(function(){
    'use strict';
    angular
        .module('prestamoEstado.service')
        .service('prestamoEstadoService', prestamoEstadoService);

        prestamoEstadoService.$inject = ['$http'];

        function prestamoEstadoService($http){

            this.getPrestamoEstados    = getPrestamoEstados ;
            this.getPrestamoEstadosInfo    = getPrestamoEstadosInfo ;

            var path = 'operaciones/prestamo-estados' ;


            function getPrestamoEstados(){
                var url_ = path+'';
                var params_ = null ;
                return appHttp.getHttp($http,url_, params_);
            };

            function getPrestamoEstadosInfo(params_){
                var url_ = path+'/get/info';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();
(function(){
    'use strict';
    angular
        .module('prestamoGarantia.service')
        .service('prestamoGarantiaService', prestamoGarantiaService);

        prestamoGarantiaService.$inject = ['$http'];

        function prestamoGarantiaService($http){

            this.getPretamoGarantiasByPrestamoId = getPretamoGarantiasByPrestamoId ;
            this.updateEstado                    = updateEstado ;

            var path = 'operaciones/prestamo-garantias' ;

            function getPretamoGarantiasByPrestamoId(params_){
                var url_ = path+'/get/byprestamoid';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();
(function(){
    'use strict';
    angular
        .module('prestamoReferencia.service')
        .service('prestamoReferenciaService', prestamoReferenciaService);

        prestamoReferenciaService.$inject = ['$http'];

        function prestamoReferenciaService($http){

            this.getPretamoReferenciasByPrestamoId     = getPretamoReferenciasByPrestamoId ;
            this.getPretamoReferenciasByPrestamoIdTipo = getPretamoReferenciasByPrestamoIdTipo ;
            this.updateEstado                          = updateEstado ;

            var path = 'operaciones/prestamo-referencias' ;

            function getPretamoReferenciasByPrestamoId(params_){
                var url_ = path+'/get/byprestamoid';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPretamoReferenciasByPrestamoIdTipo(params_){
                var url_ = path+'/get/byprestamoid-tipo';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();
(function(){
    'use strict';
    angular
        .module('prestamo.service')
        .service('prestamoService', prestamoService);

        prestamoService.$inject = ['$http'];

        function prestamoService($http){

            this.getPrestamos                   = getPrestamos ;
            this.save                           = save ;
            this.getPrestamoById                = getPrestamoById ;
            this.update                         = update ;
            this.updateEstadoPrestamoTipoEstado = updateEstadoPrestamoTipoEstado ;
            this.getPrestamosByTipoEstadoId     = getPrestamosByTipoEstadoId ;
            this.updatePrestamoTipoEstado       = updatePrestamoTipoEstado ;

            var path = 'operaciones/prestamos' ;

            function getPrestamos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPrestamoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstadoPrestamoTipoEstado(params_){
                var url_ = path+'/update/estado-all';
                return appHttp.postHttp($http,url_, params_);
            };

            function getPrestamosByTipoEstadoId(params_){
                var url_ = path+'/tipo-estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function updatePrestamoTipoEstado(params_){
                var url_ = path+'/update/prestamo-tipo-estado';
                return appHttp.postHttp($http,url_, params_);
            };

        }
})();
(function(){
    'use strict';
    angular
        .module('tipoEstado.service')
        .service('tipoEstadoService', tipoEstadoService);

        tipoEstadoService.$inject = ['$http'];

        function tipoEstadoService($http){

            this.getTipoEstados    = getTipoEstados ;

            var path = 'operaciones/tipo-estados' ;


            function getTipoEstados(params_){
                var url_ = path+'';
                return appHttp.getHttp($http,url_, params_);
            };

        }
})();
(function(){
    'use strict';
    angular
        .module('acuerdoPago.service')
        .service('acuerdoPagoService', acuerdoPagoService);

        acuerdoPagoService.$inject = ['$http'];

        function acuerdoPagoService($http){

            this.getAcuerdoPagos                = getAcuerdoPagos ;
            this.save                           = save ;
            this.getAcuerdoPagoById             = getAcuerdoPagoById ;
            this.update                         = update ;
            this.updateEstado                   = updateEstado ;
            this.getAcuerdoPagosByTipoPeriodoId = getAcuerdoPagosByTipoPeriodoId ;

            var path = 'reg/acuerdo-pagos' ;

            function getAcuerdoPagos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getAcuerdoPagoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function getAcuerdoPagosByTipoPeriodoId(params_){
                var url_ = path+'/get/by/tipo-periodo-id';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();
(function(){
    'use strict';
    angular
        .module('area.service')
        .service('areaService', areaService);

        areaService.$inject = ['$http'];

        function areaService($http){

            this.getAreas           = getAreas ;
            this.saveArea          = saveArea ;
            this.getAreaById       = getAreaById ;
            this.updateArea        = updateArea ;
            this.updateEstado      = updateEstado ;

            var path = 'reg/areas' ;

            function getAreas(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function saveArea(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getAreaById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateArea(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();
(function(){
    'use strict';
    angular
        .module('cargo.service')
        .service('cargoService', cargoService);

        cargoService.$inject = ['$http'];

        function cargoService($http){

            this.getCargos         = getCargos ;
            this.saveCargo         = saveCargo ;
            this.getCargoById      = getCargoById ;
            this.updateCargo       = updateCargo ;
            this.updateEstado      = updateEstado ;
            this.getCargosAll    = getCargosAll ;
            this.getCargosByAreaId = getCargosByAreaId ;

            var path = 'reg/cargos' ;

            function getCargos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getCargosAll(){
                var url_ = path+'/all';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getCargosByAreaId(){
                 var url_ = path+'/byarea';
                return appHttp.postHttp($http,url_, params_);
            };

            function saveCargo(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getCargoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateCargo(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();
(function(){
    'use strict';
    angular
        .module('licencia.service')
        .service('licenciaService', licenciaService);

        licenciaService.$inject = ['$http'];

        function licenciaService($http){

            this.getLicencias    = getLicencias ;
            this.save            = save ;
            this.getLicenciaById = getLicenciaById ;
            this.update          = update ;
            this.updateEstado    = updateEstado ;

            var path = 'reg/licencias' ;

            function getLicencias(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getLicenciaById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();
(function(){
    'use strict';
    angular
        .module('tipoCambio.service')
        .service('tipoCambioService', tipoCambioService);

        tipoCambioService.$inject = ['$http'];

        function tipoCambioService($http){

            this.getTipoCambios    = getTipoCambios ;
            this.save                = save ;
            this.getTipoCambioById = getTipoCambioById ;
            this.update              = update ;
            this.updateEstado        = updateEstado ;

            var path = 'reg/tipo-cambios' ;

            function getTipoCambios(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getTipoCambioById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();
(function(){
    'use strict';
    angular
        .module('tipoGarantia.service')
        .service('tipoGarantiaService', tipoGarantiaService);

        tipoGarantiaService.$inject = ['$http'];

        function tipoGarantiaService($http){

            this.getTipoGarantias    = getTipoGarantias ;
            this.save                = save ;
            this.getTipoGarantiaById = getTipoGarantiaById ;
            this.update              = update ;
            this.updateEstado        = updateEstado ;

            var path = 'reg/tipo-garantias' ;

            function getTipoGarantias(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getTipoGarantiaById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();
(function(){
    'use strict';
    angular
        .module('tipoMoneda.service')
        .service('tipoMonedaService', tipoMonedaService);

        tipoMonedaService.$inject = ['$http'];

        function tipoMonedaService($http){

            this.getTipoMonedas    = getTipoMonedas ;
            this.save                = save ;
            this.getTipoMonedaById = getTipoMonedaById ;
            this.update              = update ;
            this.updateEstado        = updateEstado ;

            var path = 'reg/tipo-monedas' ;

            function getTipoMonedas(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getTipoMonedaById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();
(function(){
    'use strict';
    angular
        .module('tipoPago.service')
        .service('tipoPagoService', tipoPagoService);

        tipoPagoService.$inject = ['$http'];

        function tipoPagoService($http){

            this.getTipoPagos    = getTipoPagos ;
            this.save            = save ;
            this.getTipoPagoById = getTipoPagoById ;
            this.update          = update ;
            this.updateEstado    = updateEstado ;
            this.getTipoPagosByTipoPrestamoId    = getTipoPagosByTipoPrestamoId ;

            var path = 'reg/tipo-pagos' ;

            function getTipoPagos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getTipoPagoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function getTipoPagosByTipoPrestamoId(params_){
                var url_ = path+'/get/by/tipo-prestamo-id';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();
(function(){
    'use strict';
    angular
        .module('tipoPeriodo.service')
        .service('tipoPeriodoService', tipoPeriodoService);

        tipoPeriodoService.$inject = ['$http'];

        function tipoPeriodoService($http){

            this.getTipoPeriodos    = getTipoPeriodos ;
            this.save                = save ;
            this.getTipoPeriodoById = getTipoPeriodoById ;
            this.update              = update ;
            this.updateEstado        = updateEstado ;

            var path = 'reg/tipo-periodos' ;

            function getTipoPeriodos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getTipoPeriodoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();
(function(){
    'use strict';
    angular
        .module('tipoPrestamo.service')
        .service('tipoPrestamoService', tipoPrestamoService);

        tipoPrestamoService.$inject = ['$http'];

        function tipoPrestamoService($http){

            this.getTipoPrestamos    = getTipoPrestamos ;
            this.save                = save ;
            this.getTipoPrestamoById = getTipoPrestamoById ;
            this.update              = update ;
            this.updateEstado        = updateEstado ;

            var path = 'reg/tipo-prestamos' ;

            function getTipoPrestamos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getTipoPrestamoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();
(function(){
    'use strict';
    angular
        .module('claseVehiculo.service')
        .service('claseVehiculoService', claseVehiculoService);

        claseVehiculoService.$inject = ['$http'];

        function claseVehiculoService($http){

            this.getClaseVehiculos    = getClaseVehiculos ;
            this.save                = save ;
            this.getClaseVehiculoById = getClaseVehiculoById ;
            this.update              = update ;
            this.updateEstado        = updateEstado ;

            var path = 'vehiculos/clase-vehiculos' ;

            function getClaseVehiculos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getClaseVehiculoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();
(function(){
    'use strict';
    angular
        .module('marca.service')
        .service('marcaService', marcaService);

        marcaService.$inject = ['$http'];

        function marcaService($http){

            this.getMarcas    = getMarcas ;
            this.save         = save ;
            this.getMarcaById = getMarcaById ;
            this.update       = update ;
            this.updateEstado = updateEstado ;

            var path = 'vehiculos/marcas' ;

            function getMarcas(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getMarcaById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };


        }
})();
(function(){
    'use strict';
    angular
        .module('modelo.service')
        .service('modeloService', modeloService);

        modeloService.$inject = ['$http'];

        function modeloService($http){

            this.getModelos          = getModelos ;
            this.save                = save ;
            this.getModeloById       = getModeloById ;
            this.update              = update ;
            this.updateEstado        = updateEstado ;
            this.getModelosAll       = getModelosAll ;
            this.getModelosByMarcaId = getModelosByMarcaId ;

            var path = 'vehiculos/modelos' ;

            function getModelos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getModelosAll(){
                var url_ = path+'/all';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getModelosByMarcaId(params_){
                 var url_ = path+'/by/marca-id';
                return appHttp.postHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getModeloById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };



        }
})();
(function(){
    'use strict';
    angular
        .module('tipoVehiculo.service')
        .service('tipoVehiculoService', tipoVehiculoService);

        tipoVehiculoService.$inject = ['$http'];

        function tipoVehiculoService($http){

            this.getTipoVehiculos                  = getTipoVehiculos ;
            this.save                              = save ;
            this.getTipoVehiculoById               = getTipoVehiculoById ;
            this.update                            = update ;
            this.updateEstado                      = updateEstado ;
            this.getTipoVehiculosAll               = getTipoVehiculosAll ;
            this.getTipoVehiculosByClaseVehiculoId = getTipoVehiculosByClaseVehiculoId ;

            var path = 'vehiculos/tipo-vehiculos' ;

            function getTipoVehiculos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getTipoVehiculosAll(){
                var url_ = path+'/all';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function getTipoVehiculosByClaseVehiculoId(params_){
                 var url_ = path+'/by/clase-vehiculo-id';
                return appHttp.postHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getTipoVehiculoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };



        }
})();
(function(){
    'use strict';
    angular
        .module('vehiculo.service')
        .service('vehiculoService', vehiculoService);

        vehiculoService.$inject = ['$http'];

        function vehiculoService($http){

            this.getVehiculos          = getVehiculos ;
            this.save                  = save ;
            this.getVehiculoById       = getVehiculoById ;
            this.update                = update ;
            this.updateEstado          = updateEstado ;
            this.getVehiculosForSelect = getVehiculosForSelect ;

            var path = 'vehiculos/vehiculos' ;

            function getVehiculos(){
                var url_ = path+'';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };

            function save(params_){
                var url_ = path+'/save';
                return appHttp.postHttp($http,url_, params_);
            };

            function getVehiculoById(params_){
                var url_ = path+'/get/byid';
                return appHttp.postHttp($http,url_, params_);
            };

            function update(params_){
                var url_ = path+'/update';
                return appHttp.postHttp($http,url_, params_);
            };

            function updateEstado(params_){
                var url_ = path+'/update/estado';
                return appHttp.postHttp($http,url_, params_);
            };

            function getVehiculosForSelect(){
                var url_ = path+'/for-select';
                var params_ = null;
                return appHttp.getHttp($http,url_, params_);
            };


        }
})();
(function(){
    'use strict';
    angular
        .module('menu.factory')
        .factory('botoneraFactory', botoneraFactory);

        botoneraFactory.$inject = [];

        function botoneraFactory(){
            var self =  this ;


            function setBotonera(botonera)
            {
                self.botonera = botonera ;
            }

            function getBotonera()
            {
                return self.botonera ;
            }

            return {
                setBotonera : setBotonera,
                getBotonera : getBotonera,
            } ;

        }

})();


(function(){
    'use strict';
    angular
        .module('menu.factory')
        .factory('menusFactory', menusFactory);

        menusFactory.$inject = ['accesosService', '$q'];

        function menusFactory(accesosService,$q){

            var self = this ;
            self.menus = [] ;
            var deferred = $q.defer();


            //////////////////////////////////

            //  promise para asegurar la cargar da datos
            function getMenus(){
                if (self.menus === undefined || self.menus === '' || self.menus.length === 0  )
                {
                    accesosService.getAccesos().then(
                        function(response){
                            if (!response.error){
                                self.menus = response.data ;
                                deferred.resolve(self.menus);

                            }else
                            {
                                deferred.resolve(response);
                            }

                        }, function (response) {
                            // the following line rejects the promise
                            deferred.reject(response);
                            // promise is returned
                            return deferred.promise;
                        }
                    );
                }
                else
                {
                      deferred.resolve(self.menus);
                }

                return deferred.promise;
            } ;

            function setMenus(menus)
            {
               self.menus = menus ;
               return self.menus ;
            }


            var factory =  {
                getMenus : getMenus,
                setMenus : setMenus,
            } ;

            return factory ;

        }

})();
/*(function(){
    'use strict';
    angular
        .module('tree.factory')
        .factory('treeFactory', treeFactory);

        treeFactory.$inject = [ '$q'];

        function treeFactory($q){

            var self = this ;
            self.data_tree = [] ;
            self.data_buil_tree = [] ;
            var deferred = $q.defer();



            function setDataTree(data)
            {
                self.data_buil_tree = [] ;
               self.data_tree = data ;
               return self.data_tree ;
            };

            function getDataTree(data)
            {
               return self.data_tree ;
            };

            function buildTree(arr, parent) {
                var out = []
                for(var i in arr) {
                    if(arr[i].control_padre_id === parent) {
                        var children = [] ;
                        children = buildTree(arr, arr[i].id)

                        if(children.length > 0) {
                            for(var j in children)
                            {
                                children[j].parent   =  arr[i] ;
                            }

                        }
                        arr[i].children = children ;

                        var is_active = false ;
                        if (arr[i].is_active === 1)
                        {
                            is_active = true ;
                        };
                        arr[i].is_active = is_active ;
                        arr[i].isExpanded = false;
                        arr[i].isSelected = is_active;

                        out.push(arr[i])
                    }
                }
                return out
            } ;

            //  promise para asegurar la cargar da datos
            function getDataBuilTree()
            {
                if (self.data_buil_tree === undefined || self.data_buil_tree === '' || self.data_buil_tree.length === 0  )
                {
                       self.data_buil_tree =   buildTree(self.data_tree, null) ;

                        deferred.resolve(self.data_buil_tree);
                }
                else
                {
                      deferred.resolve(self.data_tree);
                }

                return deferred.promise;
            } ;


            var factory =  {
                getDataTree : getDataTree,
                setDataTree : setDataTree,
                getDataBuilTree : getDataBuilTree,
            } ;

            return factory ;

        }

})();*/

(function(){
    'use strict';

        angular.module('menu.directive').directive('botonesBotonera',botonesBotonera);
        botonesBotonera.$inject = ['PATH'] ;

        function botonesBotonera( PATH)
        {
            var directive = {
                restrict: 'E',
                replace: true,
                link: link,
                controller: 'BotoneraAccesoCtrl',
                controllerAs: "btnCtrl",
                templateUrl: PATH.RAIZ+ '/menu/botonera.tpl.html',
                bindToController: true
            };

            return directive;

           function link(scope, element, attrs){
            /*   var links =element.find('a');
               var link = angular.element(links[0]);
               console.log('links links') ;

               console.log('scope botones') ;*/

              }

        }

})();

(function(){
    'use strict';

    angular.module('menu.directive').directive('menuBotonera',menuBotonera);
    menuBotonera.$inject = ['$rootScope','$state', '$location','botoneraFactory'] ;

        function menuBotonera($rootScope,$state , $location  ,botoneraFactory  )
        {

            var directive = {
                link: link,
                restrict: 'A'
            };

            return directive;

            function link(scope, element, attrs){

                  element.bind('click', function(){
                    sendData() ;
                  });

                  function sendData()
                  {
                      botoneraFactory.setBotonera(null) ;
                      if (attrs.botonera === '' ) return ;
                      var botones = JSON.parse(attrs.botonera) ;
                      // ingresamos botonera a facoria
                      botoneraFactory.setBotonera(botones) ;

                      // para recargar botonera en caso de que este vacia
                      var menu       = attrs.menu ;
                      var  menupadre = attrs.menupadre  ;

                      var values =  {
                                      id               : menu,
                                      control_padre_id : menupadre,
                                    }

                      $rootScope.$broadcast("recargarBotoneraAccesoCtrl", values);

                  }

                  // activar la botonera
                  var state = attrs.uiSref;
                  function isStateActiveBotonera()
                  {
                      if ( $state.includes(state) || $state.is(state) )
                      {
                        sendData()
                      } ;
                  }
                  isStateActiveBotonera() ;

              }
        }

})();
(function(){
	'use strict';

        angular.module('modal.directive').directive('modalDraggable',modalDraggable);
        modalDraggable.$inject = ['$document'] ;

        function modalDraggable($document)
        {
        	return function(scope, element, attr){
		    	var startX = 0, startY = 0, x = 0, y = 0;
				element.css({
					cursor: 'pointer'
				});

				var padre = element.parent().parent().parent();

				element.on('mousedown', function(event) {
					// Prevent default dragging of selected content
					event.preventDefault();
					startX = event.pageX - x;
					startY = event.pageY - y;
					$document.on('mousemove', mousemove);
					$document.on('mouseup', mouseup);
				});

				function mousemove(event) {
					y = event.pageY - startY;
					x = event.pageX - startX;
					padre.css({
						top: y + 'px',
						left:  x + 'px'
					});
				}
			    function mouseup() {
					$document.off('mousemove', mousemove);
					$document.off('mouseup', mouseup);
			    }
			};

        }


})();

(function(){
    'use strict';

    angular.module('validate.directive').directive('httpPrefix',httpPrefix);
    httpPrefix.$inject = [] ;

        function httpPrefix()
        {

            var directive = {
                link: link,
                restrict: 'A',
                require: 'ngModel',
            };

            return directive;

            function link(scope, element, attrs, controller) {
                function ensureHttpPrefix(value) {
                    // Need to add prefix if we don't have http:// prefix already AND we don't have part of it
                    if(value && !/^(https?):\/\//i.test(value)
                       && 'http://'.indexOf(value) !== 0 && 'https://'.indexOf(value) !== 0 ) {
                        controller.$setViewValue('http://' + value);
                        controller.$render();
                        return 'http://' + value;
                    }
                    else
                        return value;
                }
                controller.$formatters.push(ensureHttpPrefix);
                controller.$parsers.splice(0, 0, ensureHttpPrefix);
            }

        }

})();
(function(){
    'use strict';

    angular.module('validate.directive').directive('isDecimal',isDecimal);
    isDecimal.$inject = [] ;

        function isDecimal()
        {

            var directive = {
                link: link,
                restrict: 'A',
                require: '?ngModel',
            };

            return directive;

            function link(scope, element, attrs,ngModelCtrl)
            {
              var number_digitos = 2 ;
              var lengthDecimal = (attrs.isDecimal.trim()).length ;

              if (lengthDecimal > 0 )
              {
                number_digitos = parseInt(attrs.isDecimal) ;
              };

                if(!ngModelCtrl) {
                  return;
                }

                ngModelCtrl.$parsers.push(function(val) {
                  if (angular.isUndefined(val)) {
                      var val = '';
                  }

                  var clean = val.replace(/[^-0-9\.]/g, '');
                  var negativeCheck = clean.split('-');
                  var decimalCheck = clean.split('.');
                  if(!angular.isUndefined(negativeCheck[1])) {
                      negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                      clean =negativeCheck[0] + '-' + negativeCheck[1];
                      if(negativeCheck[0].length > 0) {
                        clean =negativeCheck[0];
                      }

                  }

                  if(!angular.isUndefined(decimalCheck[1])) {
                      // decimalCheck[1] = decimalCheck[1].slice(0,2);
                      decimalCheck[1] = decimalCheck[1].slice(0,number_digitos);
                      clean =decimalCheck[0] + '.' + decimalCheck[1];
                  }

                  if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                  }
                  return clean;
                });

                element.bind('keypress', function(event) {
                  if(event.keyCode === 32) {
                    event.preventDefault();
                  }
                });
            }
        }

})();
(function(){
    'use strict';

    angular.module('validate.directive').directive('isNumber',isNumber);
    isNumber.$inject = [] ;

        function isNumber()
        {

            var directive = {
                link: link,
                restrict: 'A',
                require: '?ngModel',
            };

            return directive;

            function link(scope, element, attrs,ngModelCtrl)
            {
                if(!ngModelCtrl) {
                  return;
                }

                ngModelCtrl.$parsers.push(function(val) {
                  if (angular.isUndefined(val)) {
                      var val = '';
                  }

                  var clean = val.replace(/[^-0-9]/g, '');
                  var negativeCheck = clean.split('-');
                  if(!angular.isUndefined(negativeCheck[1])) {
                      negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                      clean =negativeCheck[0] + '-' + negativeCheck[1];
                      if(negativeCheck[0].length > 0) {
                        clean =negativeCheck[0];
                      }

                  }

                  if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                  }
                  return clean;
                });

                element.bind('keypress', function(event) {
                  if(event.keyCode === 32) {
                    event.preventDefault();
                  }
                });
            }
        }

})();
(function() {

    // recomendacion tener cuidado en utilizar los filter puede consumir recursos
    angular.module('controles.filter').filter("arrayItemsFilter", arrayItemsFilter);

    arrayItemsFilter.$inject = [];

    function arrayItemsFilter($q) {

        return arrayItems;

        function arrayItems(items, props) {
            var out = [];

            //  texto es el mismo que se buscara para todos de elementos (por ejemplo {name: "12", age: "12"})
            var keys = Object.keys(props);
            var firstElement = keys[0];

            var text = String(props[firstElement]).toLowerCase();
            // text diferente de nulo,  recoremos el array
            if (angular.isArray(items))
            {
                for (var i = 0; i < items.length; i++)
                {
                    var item = items[i];
                    var itemMatches = false;
                    var prop = keys[0];
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                }

            } else {
                // Let the output be the input untouched
                out = items;
            }
            return out;
        };

    }

})();

(function(){

 // recomendacion tener cuidado en utilizar los filter puede consumir recursos
angular.module('controles.filter').filter("buildTreeFilter", buildTreeFilter ) ;

 buildTreeFilter.$inject = [];
function buildTreeFilter($q) {

    return buildTree ;

        function buildTree(arr, parentId)
        {
             var out = []
            for(var i in arr) {
                if(arr[i].control_padre_id === parentId) {
                    var children = [] ;
                    children = buildTree(arr, arr[i].id)

                    arr[i].children = children ;

                    var is_active = false ;
                    if (arr[i].is_active === 1)
                    {
                        is_active = true ;
                    };
                    arr[i].is_active = is_active ;
                    arr[i].isExpanded = false;
                    arr[i].isSelected = is_active;

                    out.push(arr[i])
                }
            }
            return out

        } ;


}

})() ;


(function(){

 // recomendacion tener cuidado en utilizar los filter puede consumir recursos
angular.module('controles.filter').filter("builTreeParentInChildrenFilter", builTreeParentInChildrenFilter ) ;

 builTreeParentInChildrenFilter.$inject = [];
function builTreeParentInChildrenFilter($q) {

    return buildTree ;

        function buildTree(arr, parentId)
        {
             var out = []
            for(var i in arr) {
                if(arr[i].control_padre_id === parentId) {
                    var children = [] ;
                    children = buildTree(arr, arr[i].id)

                    if(children.length > 0) {
                        for(var j in children)
                        {
                            children[j].parent   =  arr[i] ;
                        }

                    }
                    arr[i].children = children ;

                    var is_active = false ;
                    var valor = parseInt(arr[i].is_active );
                    if (valor === 1)
                    {
                        is_active = true ;
                    };

                    arr[i].is_active = is_active ;
                    arr[i].isExpanded = false;
                    arr[i].isSelected = is_active;
                    out.push(arr[i])
                }
            }
            return out

        } ;


}

})() ;


(function(){

 // recomendacion tener cuidado en utilizar los filter puede consumir recursos
angular.module('controles.filter').filter("deleteParentsOfChildrenFilter", deleteParentsOfChildrenFilter ) ;

 deleteParentsOfChildrenFilter.$inject = [];
function deleteParentsOfChildrenFilter($q) {

    return deleteParentsOfChildren ;

        function deleteParentsOfChildren(data)
        {
            for(var i in data)
            {
                    data[i].parent= '' ;

                    var children = data[i].children ;
                    if (children)
                    {
                        deleteParentsOfChildren(children) ;
                    }
            }
            return data ;

        }
}

})() ;


(function(){

	angular.module("paths.constant").constant('PATH', {
		APIURL		: 'web',
		RAIZ		: '../partials',
		INFORMACION		: '../partials/informacion',
		MOVIMIENTOS		: '../partials/movimientos',
		REPORTES		: '../partials/reportes',
		CONFIGURACION	: '../partials/configuracion',

	}) ;


})();


(function(){
    angular
        .module('app.informacion')
        .config(appConfig) ;

        appConfig.$inject = ['$stateProvider', '$urlRouterProvider','PATH'] ;

        function appConfig($stateProvider,$urlRouterProvider,PATH)
        {

            var path_informacion   = PATH.INFORMACION ;

            $stateProvider
                // emisor
                .state('accesos',{
                    url: '/accesos',
                    templateUrl: path_informacion+'/accesos/index.accesos.tlp.html',
                    // controller: 'informacionCtrl',
                    // controllerAs: 'vm',
                })
                    .state('accesos.roles',{
                        url: '/roles',
                        templateUrl: path_informacion+'/accesos/roles/index.roles.tpl.html',
                        controller: 'RolesInfoCtrl',
                        controllerAs: 'vm'
                    })

                    .state('accesos.usuarios',{
                        url: '/usuarios',
                        templateUrl: path_informacion+'/accesos/usuarios/index.usuarios.tpl.html',
                        controller: 'UsuariosInfoCtrl',
                        controllerAs: 'vm'
                    })

        }
})() ;
(function(){
    angular
        .module('app.informacion')
        .config(appConfig) ;

        appConfig.$inject = ['$stateProvider', '$urlRouterProvider','PATH'] ;

        function appConfig($stateProvider,$urlRouterProvider,PATH)
        {

            var path_informacion   = PATH.INFORMACION ;

            $stateProvider
                // emisor
                .state('personas',{
                    url: '/personas',
                    templateUrl: path_informacion+'/personas/index.personas.tlp.html',
                    // controller: 'informacionCtrl',
                    // controllerAs: 'vm',
                })
                    .state('personas.personas',{
                        url: '/personas',
                        templateUrl: path_informacion+'/personas/per_natural/index.naturales.tpl.html',
                        controller: 'PersonaNaturalInfoCtrl',
                        controllerAs: 'vm'
                    })
                        .state('personas.personas.editar',{
                                url: '/editar/:codigo',
                                templateUrl: path_informacion+'/personas/per_natural/editar.natural.tpl.html',
                                controller: 'EditPerNaturalCrtl',
                                controllerAs: 'vm'
                        })
                    .state('personas.empresas',{
                        url: '/empresas',
                        templateUrl: path_informacion+'/personas/empresas/index.empresas.tpl.html',
                        controller: 'PerJuridicasInfoCtrl',
                        controllerAs: 'vm'
                    })
                        .state('personas.empresas.editar',{
                                url: '/editar/:codigo',
                                templateUrl: path_informacion+'/personas/empresas/editar.empresa.tpl.html',
                                controller: 'EditPerJuridicaCrtl',
                                controllerAs: 'vm'
                        })


                    .state('personas.empleados',{
                        url: '/empleados',
                        templateUrl: path_informacion+'/personas/empleados/index.empleados.tpl.html',
                        controller: 'EmpleadosInfoCtrl',
                        controllerAs: 'vm'
                    })

                    .state('personas.clientes',{
                        url: '/clientes',
                        templateUrl: path_informacion+'/personas/clientes/index.clientes.tpl.html',
                        controller: 'ClientesInfoCtrl',
                        controllerAs: 'vm'
                    })
                    .state('personas.avales',{
                        url: '/avales',
                        templateUrl: path_informacion+'/personas/per_natural/index.naturales.tpl.html',
                        controller: 'PersonaNaturalInfoCtrl',
                        controllerAs: 'vm'
                    })
                        .state('personas.avales.editar',{
                                url: '/editar/:codigo',
                                templateUrl: path_informacion+'/personas/per_natural/editar.natural.tpl.html',
                                controller: 'EditPerNaturalCrtl',
                                controllerAs: 'vm'
                        })

        }
})() ;
(function(){
    angular
        .module('app.informacion')
        .config(appConfig) ;

        appConfig.$inject = ['$stateProvider', '$urlRouterProvider','PATH'] ;

        function appConfig($stateProvider,$urlRouterProvider,PATH)
        {

            var path_informacion   = PATH.INFORMACION ;

            $stateProvider
                // emisor
                .state('registros',{
                    url: '/registros',
                    templateUrl: path_informacion+'/registros/index.registros.tlp.html',
                    // controller: 'informacionCtrl',
                    // controllerAs: 'vm',
                })
                    .state('registros.areas',{
                        url: '/areas',
                        templateUrl: path_informacion+'/registros/areas/index.areas.tpl.html',
                        controller: 'AreasInfoCtrl',
                        controllerAs: 'vm'
                    })
                    .state('registros.cargos',{
                        url: '/cargos',
                        templateUrl: path_informacion+'/registros/cargos/index.cargos.tpl.html',
                        controller: 'CargosInfoCtrl',
                        controllerAs: 'vm'
                    })

                    .state('registros.tipo-prestamos',{
                        url: '/tipo-prestamos',
                        templateUrl: path_informacion+'/registros/tipo_prestamos/index.tipo-prestamos.tpl.html',
                        controller: 'TipoPrestamosInfoCtrl',
                        controllerAs: 'vm'
                    })
                    .state('registros.tipo-monedas',{
                        url: '/tipo-monedas',
                        templateUrl: path_informacion+'/registros/tipo-monedas/index.tipo-monedas.tpl.html',
                        controller: 'TipoMonedasInfoCtrl',
                        controllerAs: 'vm'
                    })
                    .state('registros.tipo-periodos',{
                        url: '/tipo-periodos',
                        templateUrl: path_informacion+'/registros/tipo-periodos/index.tipo-periodos.tpl.html',
                        controller: 'TipoPeriodosInfoCtrl',
                        controllerAs: 'vm'
                    })
                    .state('registros.tipo-garantias',{
                        url: '/tipo-garantias',
                        templateUrl: path_informacion+'/registros/tipo-garantias/index.tipo-garantias.tpl.html',
                        controller: 'TipoGarantiasInfoCtrl',
                        controllerAs: 'vm'
                    })
                    .state('registros.tipo-pagos',{
                        url: '/tipo-pagos',
                        templateUrl: path_informacion+'/registros/tipo-pagos/index.tipo-pagos.tpl.html',
                        controller: 'TipoPagosInfoCtrl',
                        controllerAs: 'vm'
                    })
                    .state('registros.licencias',{
                        url: '/licencias',
                        templateUrl: path_informacion+'/registros/licencias/index.licencias.tpl.html',
                        controller: 'LicenciasInfoCtrl',
                        controllerAs: 'vm'
                    })
                    .state('registros.tipo-cambios',{
                        url: '/tipo-cambios',
                        templateUrl: path_informacion+'/registros/tipo-cambios/index.tipo-cambios.tpl.html',
                        controller: 'TipoCambiosInfoCtrl',
                        controllerAs: 'vm'
                    })
                    .state('registros.acuerdo-pagos',{
                        url: '/acuerdo-pagos',
                        templateUrl: path_informacion+'/registros/acuerdo-pagos/index.acuerdo-pagos.tpl.html',
                        controller: 'AcuerdoPagosInfoCtrl',
                        controllerAs: 'vm'
                    })






        }
})() ;
(function(){
    angular
        .module('app.informacion')
        .config(appConfig) ;

        appConfig.$inject = ['$stateProvider', '$urlRouterProvider','PATH'] ;

        function appConfig($stateProvider,$urlRouterProvider,PATH)
        {

            var path_informacion   = PATH.INFORMACION ;

            $stateProvider
                // emisor
                .state('vehiculos',{
                    url: '/vehiculos',
                    templateUrl: path_informacion+'/vehiculos/index.vehiculos.tlp.html',
                    // controller: 'informacionCtrl',
                    // controllerAs: 'vm',
                })

                    .state('vehiculos.vehiculos',{
                        url: '/vehiculos',
                        templateUrl: path_informacion+'/vehiculos/vehiculos/index.vehiculos.tpl.html',
                        controller: 'VehiculosInfoCtrl',
                        controllerAs: 'vm'
                    })

                    .state('vehiculos.clase-vehiculos',{
                        url: '/clase-vehiculos',
                        templateUrl: path_informacion+'/vehiculos/clase-vehiculos/index.clase-vehiculos.tpl.html',
                        controller: 'ClaseVehiculosInfoCtrl',
                        controllerAs: 'vm'
                    })

                    .state('vehiculos.tipo-vehiculos',{
                        url: '/tipo-vehiculos',
                        templateUrl: path_informacion+'/vehiculos/tipo-vehiculos/index.tipo-vehiculos.tpl.html',
                        controller: 'TipoVehiculosInfoCtrl',
                        controllerAs: 'vm'
                    })

                    .state('vehiculos.marcas',{
                        url: '/marcas',
                        templateUrl: path_informacion+'/vehiculos/marcas/index.marcas.tpl.html',
                        controller: 'MarcasInfoCtrl',
                        controllerAs: 'vm'
                    })

                    .state('vehiculos.modelos',{
                        url: '/modelos',
                        templateUrl: path_informacion+'/vehiculos/modelos/index.modelos.tpl.html',
                        controller: 'ModelosInfoCtrl',
                        controllerAs: 'vm'
                    })


        }
})() ;
(function(){
    angular
        .module('app.movimientos')
        .config(appConfig) ;

        appConfig.$inject = ['$stateProvider', '$urlRouterProvider','PATH'] ;

        function appConfig($stateProvider,$urlRouterProvider,PATH)
        {
            var path_movimientos   = PATH.MOVIMIENTOS ;

            $stateProvider
                .state('operaciones',{
                    url: '/operaciones',
                    templateUrl: path_movimientos+'/operaciones/index.operaciones.tlp.html',
                    // controller: 'informacionCtrl',
                    // controllerAs: 'vm',
                })
                    .state('operaciones.prestamos',{
                        url: '/prestamos',
                        templateUrl: path_movimientos+'/operaciones/prestamos/index.prestamos.tpl.html',
                        controller: 'PrestamosMovCtrl',
                        controllerAs: 'vm'
                    })
                        .state('operaciones.prestamos.nuevo',{
                                url: '/nuevo',
                                templateUrl: path_movimientos+'/operaciones/prestamos/nuevo.prestamo.tpl.html',
                                controller: 'NewPrestamoMovCrtl',
                                controllerAs: 'vm'
                        })
                        .state('operaciones.prestamos.edit',{
                                url: '/editat/:codigo',
                                templateUrl: path_movimientos+'/operaciones/prestamos/editar.prestamo.tpl.html',
                                controller: 'EditPrestamoMovCrtl',
                                controllerAs: 'vm'
                        })
                        .state('operaciones.prestamos.detalle',{
                                url: '/detalle/:codigo',
                                templateUrl: path_movimientos+'/operaciones/prestamos/detalle.prestamo.tpl.html',
                                controller: 'DetallePrestamoCrtl',
                                controllerAs: 'vm'
                        })
                    .state('operaciones.cred-menor',{
                        url: '/cred-menor',
                        templateUrl: path_movimientos+'/operaciones/cred-menor/index.cred-menor.tpl.html',
                        controller: 'CredMenorMovCtrl',
                        controllerAs: 'vm'
                    })
                        .state('operaciones.cred-menor.nuevo',{
                                url: '/nuevo',
                                templateUrl: path_movimientos+'/operaciones/cred-menor/nuevo.cred-menor.tpl.html',
                                controller: 'NewCredMenorCrtl',
                                controllerAs: 'vm'
                        })
                        .state('operaciones.cred-menor.edit',{
                                url: '/editat/:codigo',
                                templateUrl: path_movimientos+'/operaciones/cred-menor/editar.cred-menor.tpl.html',
                                controller: 'EditCredMenorMovCrtl',
                                controllerAs: 'vm'
                        })
                        .state('operaciones.cred-menor.detalle',{
                                url: '/detalle/:codigo',
                                templateUrl: path_movimientos+'/operaciones/cred-menor/detalle.cred-menor.tpl.html',
                                controller: 'DetalleCredMenorMovCrtl',
                                controllerAs: 'vm'
                        })


        }
})() ;