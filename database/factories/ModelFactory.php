<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Persona::class, function (Faker\Generator $faker) {
    return [
        'per_nombre' => $faker->firstName,
        'per_apellidos' => $faker->lastName,
        'per_fecha_nac' => $faker->dateTimeBetween($startDate = '-45 years', $endDate = '-15 years'  )->format('Y-m-d'),
        'per_tipo'=> $faker->numberBetween($min = 1, $max = 2),
        'estado' => 1,
    ];
});



$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'persona_id' => App\Models\Persona::all()->random()->id,
        'rol_id'     => App\Models\Rol::all()->random()->id,
        'email'      => $faker->unique()->email,
        'password'   => Hash::make('12345'),
        'alias'      => "armandoaepp",
        'estado'     => 1,
    ];
});


/*$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});
*/