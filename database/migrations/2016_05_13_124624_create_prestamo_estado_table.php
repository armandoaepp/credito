<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestamoEstadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestamo_estado', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('prestamo_id')->unsigned();
            $table->integer('tipo_estado_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('glosa')->default('');
            $table->smallInteger('estado')->unsigned()->default(1);
            $table->timestamps();

            $table->foreign('prestamo_id')
                    ->references('id')
                    ->on('prestamo');

            $table->foreign('tipo_estado_id')
                    ->references('id')
                    ->on('tipo_estado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prestamo_estado');
    }
}
