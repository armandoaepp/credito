<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestamoReferenciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestamo_referencia', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('prestamo_id')->unsigned();
            $table->integer('tipo')->unsigned();
            $table->string('descripcion',500)->default('');
            $table->string('url',255)->default('');
            $table->string('glosa',500)->default('');
            $table->smallInteger('estado')->default(1);
            // $table->timestamps();

            $table->foreign('prestamo_id')
                    ->references('id')
                    ->on('prestamo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prestamo_referencia');
    }
}
