<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgenteComercialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agente_comercial', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cliente_id')->unsigned();
            $table->integer('empleado_id')->unsigned();
            $table->smallInteger('estado')->unsigned()->default(1);
            $table->timestamps();

            $table->foreign('cliente_id')
                    ->references('id')
                    ->on('cliente');

            $table->foreign('empleado_id')
                    ->references('id')
                    ->on('empleado');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agente_comercial');
    }
}
