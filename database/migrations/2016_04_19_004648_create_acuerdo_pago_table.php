<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcuerdoPagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acuerdo_pago', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_periodo_id')->unsigned();
            $table->string('nombre',250);
            $table->smallInteger('rango')->unsigned()->default(0);
            $table->smallInteger('partes')->unsigned()->default(0);
            $table->string('descripcion',250)->default('');
            $table->smallInteger('estado')->unsigned()->default(1);


            $table->foreign('tipo_periodo_id')
                    ->references('id')
                    ->on('tipo_periodo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('acuerdo_pago');
    }
}
