<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCargoEmpleadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargo_empleado', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cargo_id')->unsigned();
             $table->integer('empleado_id')->unsigned();
            $table->integer('estado')->unsigned()->default(1);
            $table->timestamps();

            $table->foreign('empleado_id')
                    ->references('id')
                    ->on('empleado');

            $table->foreign('cargo_id')
                    ->references('id')
                    ->on('cargo');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cargo_empleado');
    }
}
