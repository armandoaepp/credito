<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerWebTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('per_web', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->integer('tipo_web_id')->unsigned();
            $table->string('url',255);
            $table->smallinteger('item');
            $table->smallInteger('estado')->unsigned()->default(1);

            $table->foreign('persona_id')
                    ->references('id')
                    ->on('persona');

            $table->foreign('tipo_web_id')
                    ->references('id')
                    ->on('tipo_web');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('per_web');
    }
}
