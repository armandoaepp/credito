<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

         Schema::create('persona', function (Blueprint $table) {
            $table->increments('id');
            $table->string("per_nombre", 250);
            $table->string("per_apellidos", 250);
            $table->date("per_fecha_nac")->nullable();
            $table->integer("per_tipo");
            $table->smallInteger('estado')->default(1);
            $table->timestamps();


            $table->index('per_nombre');
            $table->index('per_apellidos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('persona');
    }
}
