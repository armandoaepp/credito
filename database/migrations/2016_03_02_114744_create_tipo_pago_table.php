<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoPagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_pago', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_prestamo_id')->unsigned();
            $table->string('nombre',250);
            // $table->smallInteger('rango')->unsigned()->default(0);
            $table->string('descripcion',250)->default('');
            $table->smallInteger('estado')->unsigned()->default(1);


            $table->foreign('tipo_prestamo_id')
                    ->references('id')
                    ->on('tipo_prestamo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipo_pago');
    }
}