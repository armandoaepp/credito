<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuotaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuota', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('prestamo_id')->unsigned();
            $table->integer('numero')->unsigned();
            $table->decimal('saldo_capital',10,2);
            $table->decimal('amortizacion',10,2);
            $table->decimal('interes',10,2);
            $table->decimal('cuota',10,2);
            $table->decimal('cuota_parte',10,2)->default(0);
            $table->date('fecha_pago');
            $table->date('fecha_parte_1')->nullable();
            $table->date('fecha_parte_2')->nullable();
            $table->date('fecha_parte_3')->nullable();
            $table->date('fecha_parte_4')->nullable();
            $table->integer('user_id_cobro')->unsigned()->default(0);
            $table->smallInteger('estado')->unsigned()->default(1);
            $table->timestamps();

             $table->foreign('prestamo_id')
                    ->references('id')
                    ->on('prestamo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cuota');
    }
}
