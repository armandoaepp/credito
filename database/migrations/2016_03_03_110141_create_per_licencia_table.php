<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerLicenciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('per_licencia', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->integer('licencia_id')->unsigned();
            $table->string('numero_lic',20)->default('');
            $table->string('clase_categoria',100)->default('');
            $table->date('fecha_emision')->nullable();
            $table->date('fecha_caducidad');
            $table->smallInteger('estado')->unsigned()->default(1);
            $table->timestamps();

            $table->foreign('persona_id')
                    ->references('id')
                    ->on('persona');

            $table->foreign('licencia_id')
                    ->references('id')
                    ->on('licencia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('per_licencia');
    }
}
