<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUbigeoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ubigeo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo',8);
            $table->string('ubigeo',150);
            $table->string('descripcion',500)->nullable();
            $table->integer('tipo_ubigeo_id')->unsigned()->default(1); # 1= departamento ; 2=provincia: 3= distrito
            $table->integer('pais_id')->unsigned()->default(1);
            $table->decimal('latitud',16,12)->nullable();
            $table->decimal('longitud',16,12)->nullable();
            $table->smallInteger('estado')->default(1);

            $table->index('codigo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ubigeo');
    }
}
