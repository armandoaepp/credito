<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestamoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestamo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('cliente_id')->unsigned();
            $table->integer('tipo_prestamo_id')->unsigned();
            $table->integer('tipo_moneda_id')->unsigned();
            $table->integer('tipo_periodo_id')->unsigned();
            $table->integer('tipo_garantia_id')->unsigned()->nullable();
            $table->integer('tipo_pago_id')->unsigned()->nullable();
            $table->integer('vehiculo_id')->unsigned()->nullable();
            $table->integer('aval_id')->unsigned()->nullable();
            $table->integer('acuerdo_pago_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();
            $table->decimal('valor',10,2); # valor unitario sin adicionales
            $table->decimal('inicial_porcentaje',10,2)->default(0);
            $table->decimal('inicial_monto',10,2)->default(0);
            $table->decimal('tasa_interes',10,2);
            $table->decimal('valor_total',10,2); # valor del prestamo = cuando hay algo adicional
            $table->integer('num_cuotas');
            $table->decimal('mora',10,2)->default(0);
            $table->string('complacencia',50)->default('');
            $table->string('pagos_parciales',50)->default('');
            $table->string('propietario',100)->default('');
            $table->string('observacion',250)->default('');
            $table->decimal('seguro_tr',10,2)->default(0);
            $table->decimal('gps',10,2)->default(0);
            $table->decimal('soat',10,2)->default(0);
            $table->decimal('gas',10,2)->default(0);
            $table->decimal('otros',10,2)->default(0);
            $table->date('fecha_credito');
            $table->date('fecha_desembolso');
            $table->date('fecha_prorrateo')->nullable();
            $table->date('fecha_prorrateo_esp')->nullable();

            $table->smallInteger('estado')->default(1);
            $table->timestamps();

            $table->foreign('cliente_id')
                    ->references('id')
                    ->on('cliente');

            $table->foreign('tipo_prestamo_id')
                    ->references('id')
                    ->on('tipo_prestamo');

            $table->foreign('tipo_moneda_id')
                    ->references('id')
                    ->on('tipo_moneda');

            $table->foreign('tipo_periodo_id')
                    ->references('id')
                    ->on('tipo_periodo');

            /*$table->foreign('tipo_garantia_id')
                    ->references('id')
                    ->on('tipo_garantia');*/

            $table->foreign('tipo_pago_id')
                    ->references('id')
                    ->on('tipo_pago');

            $table->foreign('vehiculo_id')
                    ->references('id')
                    ->on('vehiculo');

            $table->foreign('aval_id')
                    ->references('id')
                    ->on('aval');

            $table->foreign('user_id')
                    ->references('id')
                    ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prestamo');
    }
}
