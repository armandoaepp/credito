<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerJuridicaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('per_juridica', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->integer('rubro_id')->unsigned();
            $table->string('ruc',20);
            $table->string('razon_social',250);
            $table->string('nombre_comercial',250);
            $table->smallInteger('estado')->default(1);

            $table->foreign('persona_id')
                    ->references('id')
                    ->on('persona');

            $table->index('ruc');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('per_juridica');
    }
}
