<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccesosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accesos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned() ;
            $table->integer('control_id')->unsigned() ;
            $table->string('referencia',50)->default("") ;
            $table->smallInteger('estado')->default(1);

            $table->foreign('user_id')
                    ->references('id')
                    ->on('users');

            $table->foreign('control_id')
                    ->references('id')
                    ->on('control');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accesos');
    }
}
