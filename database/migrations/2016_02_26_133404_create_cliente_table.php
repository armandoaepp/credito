<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->string('codigo',20)->default('');
            $table->smallInteger('tipo')->unsigned()->default(1);
            $table->integer('persona_id_padre')->unsigned()->index();
            $table->integer('estado')->unsigned()->default(1);
            $table->timestamps();

            $table->foreign('persona_id')
                    ->references('id')
                    ->on('persona');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cliente');
    }
}
