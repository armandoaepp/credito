<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->integer('rol_id')->unsigned();
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->integer('persona_id_padre')->unsigned()->index()->default(0);
            $table->string('alias',100);
            $table->rememberToken();
            $table->smallInteger('estado')->default(1);
            $table->timestamps();

            $table->foreign('rol_id')
                    ->references('id')
                    ->on('rol');

            $table->foreign('persona_id')
                    ->references('id')
                    ->on('persona');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
