<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoVehiculoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_vehiculo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clase_vehiculo_id')->unsigned();
            $table->string('nombre',250);
            $table->string('descripcion',250)->default('');
            $table->smallInteger('estado')->unsigned()->default(1);

            $table->foreign('clase_vehiculo_id')
                    ->references('id')
                    ->on('clase_vehiculo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipo_vehiculo');
    }
}
