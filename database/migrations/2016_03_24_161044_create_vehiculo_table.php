<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiculoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_vehiculo_id')->unsigned();
            $table->integer('modelo_id')->unsigned();
            $table->string('placa',30);
            $table->string('serie',30);
            $table->string('descripcion',250)->default('');
            $table->smallInteger('estado')->unsigned()->default(1);
            $table->timestamps();

            $table->foreign('tipo_vehiculo_id')
                    ->references('id')
                    ->on('tipo_vehiculo');

            $table->foreign('modelo_id')
                    ->references('id')
                    ->on('modelo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vehiculo');
    }
}
