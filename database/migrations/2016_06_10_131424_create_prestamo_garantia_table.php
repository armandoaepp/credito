<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestamoGarantiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestamo_garantia', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('prestamo_id')->unsigned();
            $table->integer('tipo_garantia_id')->unsigned();
            $table->string('producto',500)->default('');
            $table->string('serie',255)->default('');
            $table->string('descripcion',255)->default('');
            $table->string('glosa',500)->default('');
            $table->smallInteger('estado')->default(1);
            // $table->timestamps();

            $table->foreign('prestamo_id')
                    ->references('id')
                    ->on('prestamo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prestamo_garantia');
    }
}
