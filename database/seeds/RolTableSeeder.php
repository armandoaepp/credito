<?php

use Illuminate\Database\Seeder;

class RolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$roles = array('@Sudo','@Super Admin','Administrador', 'Cliente', 'Vendedor') ;

    	for ($i=0; $i < count($roles) ; $i++)
    	{
            $estado = 1 ;
            if ($i < 2 )
            {
                $estado = 2 ; # Sudo and Super Admin
            }

    		DB::table('rol')->insert(array(
                    'nombre'      => $roles[$i],
                    // 'descripcion' => $roles[$i],
                    'estado'      => $estado
				)
	        );
    	}

    }
}
