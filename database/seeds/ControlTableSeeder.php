<?php

use Illuminate\Database\Seeder;

class ControlTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $userJson = File::get(storage_path() . "/jsondata/ubigeo.json");
    	$json = File::get(base_path()."/database/seeds/jsondata/control.json");
		$data = json_decode($json);

		foreach ($data as $object) {
			// var_dump($object);
			App\Models\Control::create(array(
				'control_padre_id' => empty($object->control_padre_id) ? null : $object->control_padre_id ,
				'tipo_control_id'  => $object->tipo_control_id ,
				'jerarquia'        => $object->jerarquia ,
				'nombre'           => $object->nombre ,
				'valor'            => empty($object->valor) ? '' : $object->valor ,
				'descripcion'      => empty($object->descripcion) ? '' : $object->descripcion  ,
				'glosa'            => empty($object->glosa) ? '' : $object->glosa  ,
				'estado'           => $object->estado
			));
		}

		$controles = App\Models\Control::get() ;
		// $controlles = $controlles->toArray() ;

        # accesos para el rol_control SUDO
    		$data_insert = array() ;
            if (count($controles) > 0)
            {
                foreach ($controles as $row)
                {
                    $fill = array(
    								'rol_id'     => 1,
    								'control_id' => $row->id,
    								'referencia' => $row->nombre ,
    								'estado'     => $row->estado ,
                                 ) ;
                    array_push($data_insert, $fill) ;
                }
                # insertarmos los accesso
                App\Models\RolControl::insert( $data_insert) ;

            }
        # accesos para el usuario sudo
    		$user_id = 1 ;
    		$data_accesos = array() ;

            if (count($controles) > 0)
            {
                foreach ($controles as $row)
                {
                    $fill = array(
                                    'user_id' => $user_id ,
                                    'control_id' => $row->id,
                                    'referencia' => "",
                                 ) ;
                    array_push($data_accesos, $fill) ;
                }
                # insertarmos los accesso
                App\Models\Acceso::insert( $data_accesos) ;
            }

        # accesos para el rol_control ADMINISTRADOR-> para empresas
        $data_rol_admin = array() ;
            if (count($controles) > 0)
            {
                foreach ($controles as $row)
                {
                    $fill = array(
                                    'rol_id'     => 2,
                                    'control_id' => $row->id,
                                    'referencia' => $row->nombre ,
                                    'estado'     => $row->estado ,
                                 ) ;
                    array_push($data_rol_admin, $fill) ;
                }
                # insertarmos los accesso
                App\Models\RolControl::insert( $data_rol_admin) ;

            }


    }
}
