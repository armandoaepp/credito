<?php

use Illuminate\Database\Seeder;

class TipoControlTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array('Modulo', 'Menu', 'Boton') ;

    	for ($i=0; $i < count($data) ; $i++)
    	{
    		 DB::table('tipo_control')->insert(array(
					'nombre'=> $data[$i],
					'estado'=> 1
				)
	        );
    	}
    }
}
