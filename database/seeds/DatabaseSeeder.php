<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Seeder\admin;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TipoWebTableSeeder::class);
        $this->call(UbigeoTableSeeder::class);

        /*$this->call(RolTableSeeder::class);
        $this->call(TipoControlTableSeeder::class);
        $this->call(TipoDocumentoTableSeeder::class);
        $this->call(TipoTelefonoTableSeeder::class);
        $this->call(TipoDireccionTableSeeder::class);
        $this->call(TipoRelacionTableSeeder::class);

        $this->call(UbigeoTableSeeder::class);


        # se tienen que registrar SUDO sudo
        $this->call(SuperAdminTableSeeder::class);
        $this->call(ControlTableSeeder::class);

        # Organización(empresa de utiliza Sistema)
        $this->call(OrganizacionTableSeeder::class);*/


        # para hacer pruebas -> comentar al migrar a produccion
        // $this->call(PerNaturalSeeder::class);
        // $this->call(UserTableSeeder::class);

    }
}
