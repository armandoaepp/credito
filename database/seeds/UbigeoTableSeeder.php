<?php

use Illuminate\Database\Seeder;

class UbigeoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	// $userJson = File::get(storage_path() . "/jsondata/ubigeo.json");
    	$userJson = File::get(base_path()."/database/seeds/jsondata/ubigeo.json");
		$user = json_decode($userJson);
		foreach ($user as $object){
			App\Models\Ubigeo::create(array(
				'codigo'         => $object->codigo ,
				'ubigeo'         => $object->ubigeo ,
				'descripcion'    => $object->descripcion ,
				'tipo_ubigeo_id' => $object->tipo_ubigeo_id ,
				'pais_id'        => $object->pais_id ,
				'latitud'        => $object->latitud ,
				'longitud'       => $object->longitud ,
				'estado'         => $object->estado ,
			));
		}


		# actulizar descripcion departamento

		DB::select('UPDATE ubigeo
					SET  descripcion = ubigeo.ubigeo
					WHERE    ubigeo.id  = ubigeo.id
					AND ubigeo.codigo = concat(left(ubigeo.codigo,2),"0000");') ;

		# actulizar descripcion provincias
		DB::select("UPDATE ubigeo AS provincia
					INNER JOIN ubigeo AS departamento ON departamento.codigo = concat(left(provincia.codigo, 2),'0000') AND departamento.tipo_ubigeo_id =  1
					SET
					provincia.descripcion =  concat( provincia.ubigeo ,', ' ,departamento.ubigeo)
					WHERE provincia.tipo_ubigeo_id = 2") ;

		# actulizar descripcion distritos
		DB::select("UPDATE ubigeo AS distrito
					INNER JOIN ubigeo AS provincia ON provincia.codigo = concat(left(distrito.codigo, 4),'00') AND provincia.tipo_ubigeo_id =  2
					INNER JOIN ubigeo AS departamento ON departamento.codigo = concat(left(distrito.codigo, 2),'0000') AND departamento.tipo_ubigeo_id =  1
					SET
					distrito.descripcion =  concat(distrito.ubigeo,', ' , provincia.ubigeo ,', ' ,departamento.ubigeo)
					WHERE distrito.tipo_ubigeo_id = 3" ) ;

    }
}
