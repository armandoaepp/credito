<?php

use Illuminate\Database\Seeder;

class TipoEstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = array('Pendiente', 'Aprobado', 'Cancelado') ; ;

    	for ($i=0; $i < count($data) ; $i++)
    	{
    		 DB::table('tipo_estado')->insert(array(
					'descripcion' => $data[$i],
					'estado'      => 1
				)
	        );
    	}
    }
}
