<?php

use Illuminate\Database\Seeder;

class TipoDireccionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array('Dirección Fiscal', 'Dirección Domicilio') ;

    	for ($i=0; $i < count($data) ; $i++)
    	{
    		 DB::table('tipo_direccion')->insert(array(
					'nombre'=> $data[$i],
					'estado'=> 1
				)
	        );
    	}
    }
}
