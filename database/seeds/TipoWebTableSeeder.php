<?php

use Illuminate\Database\Seeder;

class TipoWebTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = array('Web', 'Facebook' ,'Twitter', 'Gmail', 'Linkedin', 'Git Hub' ) ;

    	for ($i=0; $i < count($data) ; $i++)
    	{
    		 DB::table('tipo_web')->insert(array(
					'nombre'=> $data[$i],
					'estado'=> 1
				)
	        );
    	}


    }
}
