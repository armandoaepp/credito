<?php

use Illuminate\Database\Seeder;

class TipoRelacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	# Usuario Receptor : usuario que se registra(via web) para recibir informacion
    	# Usuario Emisor : Usuario que se registra como empresa que enviara infomacion(RPP )
        # Usuario: Usuario del sistema(del Usuario Receptor/Usuario Emisor/Cliente)
    	# Contacto : Contacto de la empresa o Usuario Emisor
    	# Cliente : consumidor de la informacion(cliente de alert sms Usuario Receptor / Usuario Emisor)


        $data = array('Usuaraio', 'Persona  - Empresa' ,'Cliente', 'Empleado','Aval', 'Contacto' ) ;

    	for ($i=0; $i < count($data) ; $i++)
    	{
    		DB::table('tipo_relacion')->insert(array(
					'nombre'=> $data[$i],
					'estado'=> 1
				)
	        );
    	}
    }
}
