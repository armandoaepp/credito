<?php

use Illuminate\Database\Seeder;

class SuperAdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	#====== EMPRESA SUDO
	        $nombre    = "Sudo - Planeatec" ;
	        $ruc       = "00000000000" ;
	        $create    = date('Y-m-d H:m:s') ;

		    $persona_id_padre = DB::table('persona')->insertGetId(array(
	                            // 'id' => 1 ,
	    	                    'per_nombre' => $nombre,
	    	                    'per_apellidos' => "",
	    	                    'per_fecha_nac' => $create,
	    	                    'per_tipo' => 0, # tipo sudo
	                        )
	                    );

		    // $persona_id_padre = 1 ;

	        DB::table('per_juridica')->insert(array(
				'persona_id'   => $persona_id_padre,
				'rubro_id'     => 0,
				'ruc'          => $ruc,
				'razon_social' => $nombre,
				'estado'       => 1,
	            )
	        );

	        DB::table('per_documento')->insert(array(
	            'persona_id' => $persona_id_padre,
	            'tipo_documento_id' => 2,
	            'numero' => $ruc,
	            'caducidad' => $create,
	            'imagen' => '',
	            'estado' => 1,
	            )
	        );

        #==== user SUDO ===================================================
	        $nombre    = '@armandoaepp' ;
	        $apellidos = '@sudo' ;
	        $dni       = "4x3x8x8x" ;
	        $mail      = "armandoaepp@gmail.com" ;
	        $telefono  = '996393414' ;
	        $create    = date('Y-m-d H:m:s') ;

	        $persona_id = DB::table('persona')->insertGetId(array(
	                                'per_nombre' => $nombre,
	                                'per_apellidos' => $apellidos,
	                                'per_fecha_nac' => '1986-11-11',
	                                'per_tipo' => 1,
	                        )
	                    );


	        DB::table('users')->insert(array(
	                'persona_id'       => $persona_id ,
	                'rol_id'           => $persona_id_padre,
	                'email'            => $mail,
	                'password'         => Hash::make('armando'),
	                'alias'            => 'armandoaepp',
	                'persona_id_padre' => 1,
	                'estado'           => 1,
	                // 'created_at'       => $create,
	                // 'updated_at'       => $create,
				)
	        );

	        DB::table('per_natural')->insert(array(
	                'persona_id'   => $persona_id ,
	                'dni'          => $dni,
	                'apellidos'    => $apellidos,
	                'nombres'      => $nombre,
	                'sexo'         => 1,
	                'estado_civil' => 1,
	                'estado'       => 1,
	            )
	        );

	        DB::table('per_mail')->insert(array(
	                'persona_id' => $persona_id ,
	                'mail' => $mail,
	                'item' => 1,
	                'estado' => 1,
	            )
	        );

	        DB::table('per_telefono')->insert(array(
	                'persona_id' => $persona_id ,
	                'tipo_telefono_id' => 1,
	                'telefono' => $telefono,
	                'item' => 1,
	                'estado' => 1,
	            )
	        );

	        DB::table('per_documento')->insert(array(
	                'persona_id' => $persona_id ,
	                'tipo_documento_id' => 1,
	                'numero' => $dni,
	                'caducidad' => $create,
	                'imagen' => '',
	                'estado' => 1,
	            )
	        );

	        DB::table('per_imagen')->insert(array(
	                'persona_id' => $persona_id ,
	                'url' => 'img_avatars/armandoaepp.png',
	                'tipo' => 1,
	                'estado' => 1,
	            )
	        );

	         DB::table('per_relacion')->insert(array(
	                'persona_id_padre' => $persona_id_padre,
	                'tipo_relacion_id' => 1,
	                'persona_id' => $persona_id,
	                'referencia' => 'SUDO',
	                'created_at' => $create,
	                'estado' =>1,
	            )
	        );
    	# ==================================================================

    }
}
