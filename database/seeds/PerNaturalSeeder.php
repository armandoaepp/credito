<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class PerNaturalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

        for($i = 0; $i < 500; $i ++)
        {
            $firstName = $faker->firstName;
            $lastName = $faker->lastName;

            $persona_id = \DB::table('persona')->insertGetId(array (
					'per_nombre'    => $faker->firstName,
					'per_apellidos' => $faker->lastName,
					'per_fecha_nac' => $faker->dateTimeBetween($startDate = '-45 years', $endDate = '-15 years'  )->format('Y-m-d'),
					'per_tipo'      => $faker->numberBetween($min = 1, $max = 2),
					'estado'        => 1,
            ));

            $dni = $faker->numerify('########') ;

            \DB::table('per_natural')->insert(array (
				'persona_id'   => $persona_id ,
				'dni'          => $dni ,
				'apellidos'    => $lastName,
				'nombres'      => $firstName ,
				'sexo'         => $faker->numberBetween($min = 1, $max = 2),
				'estado_civil' => $faker->numberBetween($min = 1, $max = 2),
            ));

            \DB::table('per_documento')->insert(array (
				'persona_id'        => $persona_id ,
				'tipo_documento_id' => 1,
				'numero'            => $dni,
				'caducidad'         => $faker->dateTimeBetween($startDate = '-3 years', $endDate = '+5 years'  )->format('Y-m-d'),
				'imagen'            =>'',
			));

			\DB::table('per_mail')->insert(array (
				'persona_id'   => $persona_id ,
				'mail'          => $faker->unique()->email,
				'item'    => 1,
			));

			\DB::table('per_telefono')->insert(array (
				'persona_id'       => $persona_id ,
				'tipo_telefono_id' => 1,
				'telefono'         => $faker->numerify('9########'),
				'item'             => 1,
			));

    	}
    }
}
