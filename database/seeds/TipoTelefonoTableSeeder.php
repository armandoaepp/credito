<?php

use Illuminate\Database\Seeder;

class TipoTelefonoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array('Telefono Fijo', 'Celular') ;

    	for ($i=0; $i < count($data) ; $i++)
    	{
    		 DB::table('tipo_telefono')->insert(array(
					'nombre'=> $data[$i],
					'estado'=> 1
				)
	        );
    	}
    }
}
