<?php

use Illuminate\Database\Seeder;

class TipoDocumentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array('Dni', 'Ruc', 'Pasaporte') ; ;

    	for ($i=0; $i < count($data) ; $i++)
    	{
    		 DB::table('tipo_documento')->insert(array(
					'nombre'=> $data[$i],
					'estado'=> 1
				)
	        );
    	}
    }
}
