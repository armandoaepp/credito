<?php

use Illuminate\Database\Seeder;

class OrganizacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	# ======= Organizacion ===========================
	        $nombre    = "Prestamos del Norte" ;
	        $ruc       = "20487950484" ;
	        $create    = date('Y-m-d H:m:s') ;

		    $persona_id_padre = DB::table('persona')->insertGetId(array(
	                            // 'id' => 1 ,
	    	                    'per_nombre' => $nombre,
	    	                    'per_apellidos' => "",
	    	                    'per_fecha_nac' => '2015-09-05',
	    	                    'per_tipo' => 2,
	                        )
	                    );


	        DB::table('per_juridica')->insert(array(
				'persona_id'   => $persona_id_padre,
				'rubro_id'     => 0,
				'ruc'          => $ruc,
				'razon_social' => $nombre,
				'estado'       => 1,
	            )
	        );

	        DB::table('per_documento')->insert(array(
				'persona_id'        => $persona_id_padre,
				'tipo_documento_id' => 2,
				'numero'            => $ruc,
				'caducidad'         => $create,
				'imagen'            => '',
				'estado'            => 1,
	            )
	        );

	        DB::table('per_relacion')->insert(array(
	                'persona_id_padre' => 1, # empresa (SUDO) developer
	                'tipo_relacion_id' => 3, # viene a ser un cliendte de SUDO
	                'persona_id'       => $persona_id_padre,
	                'referencia'       => 'Cliente',
	                'created_at'       => $create,
	                'estado'           => 1,
	            )
	        );

	    # ======= user super admin organizacion =

			$nombre    = "Adminstrador" ;
			$apellidos = 'Super Admin ' ;
			$dni       = "123456789" ;
			$mail      = "admin@admin.com" ;
			$password  = 'admin123' ;
			$telefono  = '900000000' ;
			$create    = date('Y-m-d H:m:s') ;

	        $persona_id = DB::table('persona')->insertGetId(array(
	                                'per_nombre' => $nombre,
	                                'per_apellidos' => $apellidos,
	                                'per_fecha_nac' => '1986-11-11',
	                                'per_tipo' => 1,
	                        )
	                    );


	        $user_id = DB::table('users')->insertGetId(array(
			                'persona_id'       => $persona_id ,
			                'rol_id'           => 2, # super admin
			                'email'            => $mail,
			                'password'         => Hash::make($password),
			                'alias'            => 'armandoaepp',
			                'persona_id_padre' => $persona_id_padre,
			                'created_at'       => $create,
			                'updated_at'       => $create,
			                'estado'           => 1,
						)
			        );

	        DB::table('per_natural')->insert(array(
	                'persona_id'   => $persona_id ,
	                'dni'          => $dni,
	                'apellidos'    => $apellidos,
	                'nombres'      => $nombre,
	                'sexo'         => 1,
	                'estado_civil' => 1,
	                'estado'       => 1,
	            )
	        );

	        DB::table('per_mail')->insert(array(
	                'persona_id' => $persona_id ,
	                'mail' => $mail,
	                'item' => 1,
	                'estado' => 1,
	            )
	        );

	        DB::table('per_telefono')->insert(array(
	                'persona_id'       => $persona_id ,
	                'tipo_telefono_id' => 1,
	                'telefono'         => $telefono,
	                'item'             => 1,
	                'estado'           => 1,
	            )
	        );

	        DB::table('per_documento')->insert(array(
	                'persona_id'        => $persona_id ,
	                'tipo_documento_id' => 1,
	                'numero'            => $dni,
	                'caducidad'         => $create,
	                'imagen'            => '',
	                'estado'            => 1,
	            )
	        );

	        DB::table('per_imagen')->insert(array(
	                'persona_id' => $persona_id ,
	                'url'        => 'img_avatars/avatar_admin.png',
	                'tipo'       => 1,
	                'estado'     => 1,
	            )
	        );

	        DB::table('per_relacion')->insert(array(
	                'persona_id_padre' => $persona_id_padre,
	                'tipo_relacion_id' => 2, # persona - empresa
	                'persona_id'       => $persona_id,
	                'referencia'       => 'Persona - Empresa ',
	                'created_at'       => $create,
	                'estado'           => 1,
	            )
	        );

	        DB::table('per_relacion')->insert(array(
	                'persona_id_padre' => $persona_id_padre,
	                'tipo_relacion_id' => 1, # usuario
	                'persona_id'       => $persona_id,
	                'referencia'       => 'Usuario',
	                'created_at'       => $create,
	                'estado'           => 1,
	            )
	        );
	    # generamos los accesos para el usuaraio administrador ====================
	        // $user_id = 1 ;
			$data_accesos = array() ;

			$controles = App\Models\Control::where('estado',1)->get() ;
	        if (count($controles) > 0)
	        {
	            foreach ($controles as $row)
	            {
	                $fill = array(
	                                'user_id' => $user_id ,
	                                'control_id' => $row->id,
	                                'referencia' => "",
	                             ) ;
	                array_push($data_accesos, $fill) ;
	            }
	            # insertarmos los accesso
	            App\Models\Acceso::insert( $data_accesos) ;
	        }
    }
}
