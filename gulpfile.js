var gulp = require('gulp') ;
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var cssmin = require('gulp-cssmin');


// carpeta app : concatenar todos los archivos  (no coge subcarpetas)
gulp.task('min-script', function() {
 return gulp.src('./resources/assets/js/script/**/*.js')
    .pipe(concat('script.js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('./public/js/'));
});

// minificar todos los archivos JS
gulp.task('min-app', function() {
    return gulp.src(['./resources/assets/js/angular/*.js',
                      './resources/assets/js/angular/controllers/**/*.js', // caperta y sub carpetas
                      './resources/assets/js/angular/modules/**/*.js',
                      './resources/assets/js/angular/services/**/*.js',
                      './resources/assets/js/angular/factories/**/*.js',
                      './resources/assets/js/angular/directives/**/*.js',
                      './resources/assets/js/angular/filters/*.js',
                      './resources/assets/js/angular/constants/*.js',
                      './resources/assets/js/angular/routers/**/*.js',
                    ])
        .pipe(concat('app.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
       .pipe(gulp.dest('./public/js/'));
});

gulp.task('min-js-plugin', function() {
    return gulp.src([
                      './resources/assets/js/angular/plugin/**/*.js',
                    ])
        .pipe(concat('app.plugin.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
       .pipe(gulp.dest('./public/js/'));
});

gulp.task('min-css-plugin', function() {
    return gulp.src([
                      './resources/assets/js/angular/plugin/**/*.css',
                    ])
        .pipe(concat('app.plugin.css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(cssmin())
        .pipe(gulp.dest('./public/css/'));
});

gulp.task('min-app-plugin', ['min-css-plugin', 'min-js-plugin']) ;



gulp.task('min-css', function () {
    gulp.src([
                './resources/assets/css/**/*.css',
                // './resources/assets/css/note.tpl/**/*.css',
                // './resources/assets/css/styles/**/*.css',
              ])

        .pipe(concat('app.css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(cssmin())
        .pipe(gulp.dest('./public/css/'));
});

//  cambiar de nombre para poner a produccion
gulp.task('prod-plataforma', function() {
    // rename via string
    gulp.src("./resources/views/plataforma-prod.blade.php")
      .pipe(rename("plataforma.blade.php"))
      .pipe(gulp.dest("./resources/views/"));
});


//  minificar y poner en produccion todo
gulp.task('prod', ['min-script', 'min-app' ,'min-css','min-app-plugin']) ;


// desarrollo

// carpeta app : concatenar todos los archivos  (no coge subcarpetas)
gulp.task('script', function() {
 return gulp.src('./resources/assets/js/script/**/*.js')
    .pipe(concat('script.js'))
    .pipe(gulp.dest('./public/js/'));
});


// minificar todos los archivos JS
gulp.task('angular', function() {
    return gulp.src(['./resources/assets/js/angular/*.js',
                      './resources/assets/js/angular/controllers/**/*.js', // caperta y sub carpetas
                      './resources/assets/js/angular/modules/**/*.js',
                      './resources/assets/js/angular/services/**/*.js',
                      './resources/assets/js/angular/factories/**/*.js',
                      './resources/assets/js/angular/directives/**/*.js',
                      './resources/assets/js/angular/filters/*.js',
                      './resources/assets/js/angular/constants/*.js',
                      './resources/assets/js/angular/routers/**/*.js',

                    ])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./public/js/'));
});

gulp.task('css', function () {
    gulp.src([
                 './resources/assets/css/**/*.css',
              ])

        .pipe(concat('app.css'))
        .pipe(gulp.dest('./public/css/'));
});


gulp.task('app-plugin', function() {
    return gulp.src([
                      './resources/assets/js/angular/plugin/**/*.js',
                    ])
        .pipe(concat('app.plugin.js'))
        .pipe(gulp.dest('./public/js/'));
});